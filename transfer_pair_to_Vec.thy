theory transfer_pair_to_Vec
imports
  "HOL-Analysis.Analysis"
begin

subsection \<open>Parametricity rules for constants in analysis\<close>

context includes lifting_syntax begin

lemma Sigma_transfer[transfer_rule]:
  "(rel_set A ===> (A ===> rel_set B) ===> rel_set (rel_prod A B)) Sigma Sigma"
  unfolding Sigma_def
  by transfer_prover

lemma less_transfer[transfer_rule]:
  "(A ===> A ===> (=)) less less"
  if [transfer_rule]: "bi_unique A" "(A ===> A ===> (=)) less_eq less_eq"
  for A::"'c::order \<Rightarrow> 'd::order \<Rightarrow> bool"
  unfolding order.strict_iff_order[abs_def]
  by transfer_prover

lemma norm_transfer[transfer_rule]:
  "(A ===> (=)) norm norm"
  if [transfer_rule]: "(A ===> A ===> (=)) inner inner"
  unfolding norm_eq_sqrt_inner
  by transfer_prover

lemma dist_transfer[transfer_rule]:
  "(A ===> A ===> (=)) dist dist"
  if [transfer_rule]: "(A ===> (=)) norm norm" "(A ===> A ===> A) (-) (-)"
  unfolding dist_norm
  by transfer_prover

lemma open_transfer[transfer_rule]:
  "(rel_set A ===> (=)) open open"
  if [transfer_rule]: "bi_unique A" "bi_total A" "(A ===> A ===> (=)) dist dist"
  unfolding open_dist
  by transfer_prover

lemma closed_transfer[transfer_rule]:
  "(rel_set A ===> (=)) closed closed"
  if [transfer_rule]: "bi_unique A" "bi_total A" "(rel_set A ===> (=)) open open"
  unfolding closed_def
  by transfer_prover

lemma sgn_transfer[transfer_rule]:
  "(A ===> A) sgn sgn"
  if [transfer_rule]: "(A ===> (=)) norm norm" "((=) ===> A ===> A) scaleR scaleR"
  unfolding sgn_div_norm
  by transfer_prover

lemma uniformity_transfer[transfer_rule]:
  "(rel_filter (rel_prod A A)) uniformity uniformity"
  if [transfer_rule]: "bi_total A"  "bi_unique A" "(A ===> A ===> (=)) dist dist"
  unfolding uniformity_dist
  by transfer_prover

lemma cball_transfer[transfer_rule]:
  "(A ===> (=) ===> rel_set A) cball cball"
  if [transfer_rule]: "bi_total A" "(A ===> A ===> (=)) dist dist"
  unfolding cball_def by transfer_prover

lemma ball_transfer[transfer_rule]:
  "(A ===> (=) ===> rel_set A) ball ball"
  if [transfer_rule]: "bi_total A" "(A ===> A ===> (=)) dist dist"
  unfolding ball_def by transfer_prover

lemma filterlim_transfer[transfer_rule]:
  "((A ===> B) ===> rel_filter B ===> rel_filter A ===> (=)) filterlim filterlim"
  if [transfer_rule]: "bi_unique B"
  unfolding filterlim_iff
  by transfer_prover

lemma nhds_transfer[transfer_rule]:
  "(A ===> rel_filter A) nhds nhds"
  if [transfer_rule]: "bi_unique A" "bi_total A" "(rel_set A ===> (=)) open open"
  unfolding nhds_def
  by transfer_prover

lemma at_within_transfer[transfer_rule]:
  "(A ===> rel_set A ===> rel_filter A) at_within at_within"
  if [transfer_rule]: "bi_unique A" "bi_total A" "(rel_set A ===> (=)) open open"
  unfolding at_within_def
  by transfer_prover

lemma continuous_on_transfer[transfer_rule]:
  "(rel_set A ===> (A ===> B) ===> (=)) continuous_on continuous_on"
  if [transfer_rule]: "bi_unique A" "bi_total A" "(rel_set A ===> (=)) open open"
    "bi_unique B" "bi_total B" "(rel_set B ===> (=)) open open"
  unfolding continuous_on_def
  by transfer_prover

lemma is_interval_transfer[transfer_rule]: "(rel_set A ===> (=)) is_interval is_interval"
  if [transfer_rule]: "bi_unique A" "bi_total A" "(A ===> A ===> (=)) inner inner" "(rel_set A) Basis Basis"
  unfolding is_interval_def
  by transfer_prover

lemma additive_transfer[transfer_rule]:
  "((B ===> A) ===> (=)) Modules.additive Modules.additive"
  if [transfer_rule]:
    "bi_unique A"
    "bi_total B"
    "(A ===> A ===> A) (+) (+)"
    "(B ===> B ===> B) (+) (+)"
  unfolding Modules.additive_def
  by transfer_prover

lemma linear_transfer[transfer_rule]: "((B ===> A) ===> (=)) linear linear"
  if [transfer_rule]:
    "bi_unique A"
    "bi_total B"
    "(A ===> A ===> A) (+) (+)"
    "((=) ===> A ===> A) (%x y. x *\<^sub>R y) (%x y. x *\<^sub>R y)"
    "(B ===> B ===> B) (+) (+)"
    "((=) ===> B ===> B) (%x y. x *\<^sub>R y) (%x y. x *\<^sub>R y)"
  unfolding linear_iff
  by transfer_prover

lemma bounded_linear_transfer[transfer_rule]: "((B ===> A) ===> (=)) bounded_linear bounded_linear"
  if [transfer_rule]:
    "bi_unique A"
    "bi_total B"
    "(A ===> A ===> A) (+) (+)"
    "((=) ===> A ===> A) (%x y. x *\<^sub>R y) (%x y. x *\<^sub>R y)"
    "(B ===> B ===> B) (+) (+)"
    "((=) ===> B ===> B) (%x y. x *\<^sub>R y) (%x y. x *\<^sub>R y)"
    "(A ===> (=)) norm norm"
    "(B ===> (=)) norm norm"
  unfolding bounded_linear_def bounded_linear_axioms_def
  by transfer_prover

definition "has_derivative_at_within f f' a X = ( has_derivative ) f f' (at a within X)"

lemma has_derivative_at_within[transfer_rule]:
  "((A ===> B) ===> (A ===> B) ===> A ===> rel_set A ===> (=)) has_derivative_at_within has_derivative_at_within"
  if [transfer_rule]:
    "bi_total A" 
    "((=) ===> A ===> A) (%x y. x *\<^sub>R y) (%x y. x *\<^sub>R y)"
    "(A ===> A ===> A) (+) (+)"
    "(A ===> A ===> A) (-) (-)"
    "(A ===> (=)) norm norm"
    "bi_unique B"
    "((=) ===> B ===> B) (%x y. x *\<^sub>R y) (%x y. x *\<^sub>R y)"
    "(B ===> B ===> B) (+) (+)"
    "(B ===> B ===> B) (-) (-)"
    "(B ===> (=)) norm norm"
  unfolding has_derivative_within' has_derivative_at_within_def linear_linear
  by transfer_prover

lemma Finite_Set_fold_comp_fun_commute_transfer:
  "B (Finite_Set.fold f x0 X) (Finite_Set.fold g y0 Y)"
  if [transfer_rule]:
    "bi_total A" 
    "bi_unique A" 
    "rel_set A X Y"
    "(A ===> B ===> B) f g"
    "B x0 y0"
    and "comp_fun_commute f"
    and "comp_fun_commute g"
proof cases
  assume "finite X"
  then show ?thesis
    using \<open>rel_set A X Y\<close>
  proof (induction arbitrary: Y)
    case [transfer_rule]: empty
    have "Y = {}"
      by transfer simp
    then show ?case using \<open>B x0 y0\<close>
      by simp
  next
    case (insert x X')
    note [transfer_rule] = \<open>rel_set A (insert x X') Y\<close>
    obtain y where [transfer_rule]: "A x y"
      by (meson \<open>bi_total A\<close> bi_total_def)
    have "y \<in> Y" by transfer simp
    define Y' where "Y' = Y - {y}"
    have Y_def: "Y = insert y Y'" by (auto simp: Y'_def \<open>y \<in> Y\<close>)
    have "y \<notin> Y'" by (auto simp: Y'_def)
    define fX' where "fX' = Finite_Set.fold f x0 X'"
    define fY' where "fY' = Finite_Set.fold g y0 Y'"
    have "rel_set A (insert x X' - {x}) Y'"
      unfolding Y'_def
      by transfer_prover
    then have [transfer_rule]: "rel_set A X' Y'" "B fX' fY'"
      unfolding fX'_def fY'_def
      using insert.hyps insert.IH by auto
    have "finite Y'"
      by transfer fact
    interpret comp_fun_commute f by fact
    interpret comp_fun_commute g by fact
    show ?case
      unfolding Y_def
      using \<open>x \<notin> X'\<close> \<open>y \<notin> Y'\<close> \<open>finite X'\<close> \<open>finite Y'\<close>
      apply (simp add: fX'_def[symmetric] fY'_def[symmetric])
      subgoal premises prems
        by transfer_prover
      done
  qed
next
  assume [simp]: "infinite X"
  then have [simp]: "infinite Y"
    by transfer
  show "B (Finite_Set.fold f x0 X) (Finite_Set.fold g y0 Y)"
    using \<open>B x0 y0\<close>
    by simp
qed

lemma comm_monoid_set_F_transfer:
  "((B ===> A) ===> rel_set B ===> A)
    (comm_monoid_set.F f z) (comm_monoid_set.F f' z')"
  if [transfer_rule]:
    "bi_total B" 
    "bi_unique B"
    "(A ===> A ===> A) f f'"
    "A z z'"
    and csf: "comm_monoid_set f z" and csf': "comm_monoid_set f' z'"
proof -
  interpret f: comm_monoid_set f z by fact
  interpret f': comm_monoid_set f' z' by fact
  show ?thesis
    apply (safe intro!: rel_funI)
    unfolding f.eq_fold f'.eq_fold
    apply (rule Finite_Set_fold_comp_fun_commute_transfer[where B=A and A=B])
    subgoal by (rule that)
    subgoal by (rule that)
    subgoal by assumption
    subgoal premises prems
      supply [transfer_rule] = prems
      by transfer_prover
    subgoal by (rule that)
    subgoal
      by standard (simp add: fun_eq_iff f.left_commute)
    subgoal
      by standard (simp add: fun_eq_iff f'.left_commute)
    done
qed

lemma sum_transfer[transfer_rule]:
  "((A ===> B) ===> rel_set A ===> B) sum sum"
  if tr[transfer_rule]:
    "bi_total A" "bi_unique A" 
    "(B ===> B ===> B) (+) (+)"
    "B 0 0"
  for A::"'a \<Rightarrow> 'b::comm_monoid_add \<Rightarrow> bool"
  unfolding sum_def
  by (rule comm_monoid_set_F_transfer; (fact | rule sum.comm_monoid_set_axioms))

lemma prod_transfer[transfer_rule]:
  "((A ===> B) ===> rel_set A ===> B) prod prod"
  if tr[transfer_rule]:
    "bi_total A" "bi_unique A" 
    "bi_total B" "bi_unique B" 
    "(B ===> B ===> B) (*) (*)"
    "B 1 1"
  for A::"'a \<Rightarrow> 'b::comm_monoid_mult \<Rightarrow> bool"
  unfolding prod_def
  by (rule comm_monoid_set_F_transfer; (fact | rule prod.comm_monoid_set_axioms))

context includes lifting_syntax begin

lemma cbox_transfer[transfer_rule]: "(A ===> A ===> rel_set A) cbox cbox"
  if [transfer_rule]: "(A ===> A ===> (=)) (\<bullet>) (\<bullet>)"
    "(rel_set A) Basis Basis"
    "bi_total A"
  unfolding cbox_def
  by transfer_prover

lemma gauge_transfer[transfer_rule]: "((A ===> rel_set A) ===> (=)) gauge gauge"
  if [transfer_rule]:
    "(rel_set A ===> (=)) open open"
    "bi_total A" 
    "bi_unique A" 
  unfolding gauge_def
  by (transfer_prover)

lemma fine_transfer[transfer_rule]: "((A ===> rel_set B) ===> rel_set (rel_prod A (rel_set B)) ===> (=)) (fine) (fine)"
  if [transfer_rule]: "bi_unique B" 
  unfolding fine_def
  by (transfer_prover)

lemma interior_transfer[transfer_rule]:
  "(rel_set A ===> rel_set A) interior interior"
  if [transfer_rule]:
    "bi_unique A" 
    "bi_total A"
    "(rel_set A ===> (=)) open open"
  unfolding interior_def
  by transfer_prover

lemma tagged_partial_division_of_transfer[transfer_rule]:
  "(rel_set (rel_prod A (rel_set A)) ===> rel_set A ===> (=))
     (tagged_partial_division_of)
     (tagged_partial_division_of)"
  if [transfer_rule]:
    "bi_total A" 
    "bi_unique A" 
    "(rel_set A ===> (=)) open open"
    "(A ===> A ===> (=)) inner inner"
    "rel_set A Basis Basis"
  unfolding tagged_partial_division_of_def
  by transfer_prover

lemma tagged_division_of_transfer[transfer_rule]:
  "(rel_set (rel_prod A (rel_set A)) ===> rel_set A ===> (=)) (tagged_division_of) (tagged_division_of)"
  if [transfer_rule]:
    "bi_total A" 
    "bi_unique A" 
    "(rel_set A ===> (=)) open open"
    "(A ===> A ===> (=)) inner inner"
    "rel_set A Basis Basis"
  unfolding tagged_division_of_def
  by transfer_prover

lemma division_filter_transfer[transfer_rule]:
  "(rel_set A ===> rel_filter(rel_set (rel_prod A (rel_set A)))) division_filter division_filter"
  if [transfer_rule]:
    "bi_total A" 
    "bi_unique A" 
    "(rel_set A ===> (=)) open open"
    "(A ===> A ===> (=)) inner inner"
    "rel_set A Basis Basis"
  unfolding division_filter_def
  by transfer_prover

definition "content_cbox X = (if \<exists>a b. X = cbox a b then content X else 0)"

lemma content_cbox: "D tagged_division_of cbox a b \<Longrightarrow> X \<in> D \<Longrightarrow> content_cbox (snd X) = content (snd X)"
  by (cases X) (auto dest!: tagged_division_ofD(4) simp: content_cbox_def)

lemma content_cbox_transfer[transfer_rule]:
  "(rel_set A ===> (=)) content_cbox content_cbox"
  if [transfer_rule]:
    "(A ===> A ===> (=)) (\<bullet>) (\<bullet>)"
    "(rel_set A) Basis Basis"
    "bi_total A" "bi_unique A" 
proof (safe intro!: rel_funI, goal_cases)
  case [transfer_rule]: (1 x y)
  show ?case
  proof cases
    assume x: "(\<exists>a b. x = cbox a b)"
    then have y: "(\<exists>a b. y = cbox a b)" by transfer
    from x obtain a b where xc: "x = cbox a b" by auto
    then obtain a' b' where yc: "y = cbox a' b'"
      by transfer auto
    show ?thesis
      using x y
      unfolding content_cbox_def
      apply (auto simp: content_cbox_if)
      subgoal by transfer auto
      subgoal by transfer auto
      subgoal
        apply transfer
        apply auto
        by (metis empty_iff eq_cbox prod.cong)
      done
  next
    assume 1: "\<nexists>a b. x = cbox a b"
    then have 2: "\<nexists>a b. y = cbox a b"
      by transfer
    show ?thesis
      using 1 2
      by (auto simp: content_cbox_def)
  qed
qed

lemma has_integral_content_cbox:
  "(f has_integral y) (cbox a b) \<longleftrightarrow>
    (\<forall>e>0. \<exists>\<gamma>. gauge \<gamma> \<and>
      (\<forall>\<D>. \<D> tagged_division_of (cbox a b) \<and> \<gamma> fine \<D> \<longrightarrow>
        norm (sum (\<lambda>(x,k). content_cbox(k) *\<^sub>R f x) \<D> - y) < e))"
  unfolding has_integral
  apply (auto simp: has_integral content_cbox split_beta' cong: sum.cong imp_cong all_cong)
   apply (subst all_cong)
    apply (subst sum.cong[OF refl])
     apply (subst content_cbox)
       apply force
      apply assumption
     apply (rule refl)
    apply (rule refl)
   apply force
  apply (subst all_cong)
   apply (subst sum.cong[OF refl])
    apply (subst content_cbox[symmetric])
      apply force
     apply assumption
    apply (rule refl)
   apply (rule refl)
  apply force
  done

lemma diff_transfer[transfer_rule]:
  "(A ===> A ===> A) (-) (-)"
  if [transfer_rule]:
    "(A ===> A) uminus uminus"
    "(A ===> A ===> A) (+) (+)"
  for A::"'a::group_add \<Rightarrow> 'b::group_add \<Rightarrow> bool"
  unfolding diff_conv_add_uminus
  by transfer_prover

lemma uminus_transfer[transfer_rule]: 
  "(A ===> A) uminus uminus"
  if [transfer_rule]:
    "((=) ===> A ===> A) (%x y. x *\<^sub>R y) (%x y. x *\<^sub>R y)"
  for A::"'a::real_vector \<Rightarrow> 'b::real_vector \<Rightarrow> bool"
  by (subst (1 2) scaleR_minus1_left[symmetric, abs_def]) transfer_prover
  
lemma zero_euclidean_transfer[transfer_rule]:
  "A 0 0"
  if [transfer_rule]:
    "bi_unique A" "bi_total A"
    "rel_set A Basis Basis"
    "((=) ===> A ===> A) (%x y. x *\<^sub>R y) (%x y. x *\<^sub>R y)"
proof -
  from nonempty_Basis obtain x::'a where x: "x \<in> Basis" by blast
  obtain y where [transfer_rule]: "A x y"
    by (meson \<open>bi_total A\<close> bi_total_def)
  from x have y: "y \<in> Basis" by transfer
  have "A (0 *\<^sub>R x) (0 *\<^sub>R y)"
    by transfer_prover
  then show ?thesis by simp
qed

lemma has_integral_transfer[transfer_rule]:
  "((A ===> B) ===> B ===> rel_set A ===> (=)) (has_integral) (has_integral)"
  if [transfer_rule]:
    "bi_unique A" "bi_total A"
    "rel_set A Basis Basis"
    "(A ===> A ===> (=)) (\<bullet>) (\<bullet>)"
    "(A ===> A ===> A) (+) (+)"
    "((=) ===> A ===> A) (%x y. x *\<^sub>R y) (%x y. x *\<^sub>R y)"
    "bi_unique B" "bi_total B"
    "((=) ===> B ===> B) (%x y. x *\<^sub>R y) (%x y. x *\<^sub>R y)"
    "(B ===> B ===> B) (+) (+)"
    "(B ===> (=)) norm norm"
    "B (0::'a::banach) (0::'b::banach)"
  using [[show_sorts, show_consts]]
  apply (subst has_integral'[abs_def])
  apply (subst (2) has_integral'[abs_def])
  unfolding has_integral_content_cbox
  by (transfer_prover)

end

end

context includes lifting_syntax begin


definition "embed_vec a = ((fst a) *\<^sub>R axis (1::2) (1::real) + (snd a) *\<^sub>R axis 2 1)"

definition "embed_pair a = (a \<bullet> axis (1::2) (1::real), a \<bullet> axis 2 1)"

lemma Vec_2_Basis: "Basis = {axis (1::2) (1::real), axis (2::2) (1::real)}"
  by (auto simp add: UNIV_2 Basis_vec_def )

lemma sum_on_base_2:
  assumes "a *\<^sub>R v1 + b *\<^sub>R v2 = a' *\<^sub>R v1 + b' *\<^sub>R v2"
      "v1 \<bullet> v2 = 0" "v1 \<bullet> v1 = 1" "v2 \<bullet> v2 = 1"
    shows "a = a'" (is ?g1) "b = b'" (is ?g2)
proof- 
  have i: "\<And>v. (a *\<^sub>R v1 + b *\<^sub>R v2) \<bullet> v = (a' *\<^sub>R v1 + b' *\<^sub>R v2) \<bullet> v" 
    using assms(1) by auto
  show ?g1 ?g2
    using assms(2-4) i[of v1] i[of v2]
    by(auto simp add: algebra_simps inner_commute)
qed


definition vec_rel(*::"real*real \<Rightarrow> (real \<times> real) ^2 \<Rightarrow> bool"*)
  where "vec_rel a b \<longleftrightarrow> b = (embed_vec a)"

named_theorems vec_rel_transfer

lemma [vec_rel_transfer,transfer_rule]:
  shows "bi_unique vec_rel" "bi_total vec_rel"
  "rel_set vec_rel Basis Basis"\<comment>\<open>generalize assms of @{thm has_integral_transfer}\<close>
  "(vec_rel ===> vec_rel ===> (=)) (\<bullet>) (\<bullet>)"
  "(vec_rel ===> vec_rel ===> vec_rel) (+) (+)"
  "((=) ===> vec_rel ===> vec_rel) (%x y. x *\<^sub>R y) (%x y. x *\<^sub>R y)"
  subgoal
    by (auto simp: vec_rel_def bi_unique_def embed_vec_def sum_on_base_2 axis_eq_axis inner_not_same_Basis) +
  subgoal
    apply (auto simp: vec_rel_def bi_total_def embed_vec_def)
    subgoal for y using euclidean_representation[of y] apply(auto simp add: Vec_2_Basis axis_eq_axis) by metis
  done
  subgoal
    by (auto simp: vec_rel_def bi_unique_def embed_vec_def rel_set_def Basis_prod_def Vec_2_Basis)
  subgoal
    by (auto simp: vec_rel_def bi_unique_def embed_vec_def rel_fun_def
      inner_add_left inner_add_right inner_commute vector_def fst_def snd_def inner_axis_axis)
  subgoal
    by (auto simp: vec_rel_def bi_unique_def embed_vec_def
        algebra_simps
        intro!: rel_funI)
  subgoal
    by (auto simp: vec_rel_def bi_unique_def embed_vec_def algebra_simps
        intro!: rel_funI)
  done

end

bundle vec_rel_bundle begin
lemmas [transfer_rule] = vec_rel_transfer
end

context includes lifting_syntax begin

lemma [vec_rel_transfer,transfer_rule]: "((=) ===> vec_rel) (\<lambda>x. x) embed_vec"
  by (auto intro!: rel_funI simp: vec_rel_def)

lemma has_integral_eq_pair_Vec[vec_rel_transfer,transfer_rule]:
  fixes I::"'b::banach"  assumes [transfer_rule]: "(vec_rel ===> (=)) g g'" "rel_set vec_rel S S'"
  notes [transfer_rule] = vec_rel_transfer
  shows "(g has_integral I) S \<longleftrightarrow> (g' has_integral I) S'"
  by transfer simp

lemma has_derivative_within_eq_pair_Vec: "has_derivative_at_within f f' a S \<longleftrightarrow> has_derivative_at_within g g' b T"
  if [transfer_rule]: "(vec_rel ===> (=)) f' g'" "(vec_rel ===> (=)) f g" "rel_set vec_rel S T" "vec_rel a b" 
  for I::"'b::banach"
  by transfer simp

lemma eq_continuous_on_pair_Vec: "continuous_on (embed_vec ` S) g \<longleftrightarrow> continuous_on S (g o embed_vec)"
  for I::"'b::banach"
  by transfer (simp add: o_def)

lemma pre_eq_continuous_on_pair_Vec: "continuous_on S g \<longleftrightarrow> continuous_on S (embed_vec o g)"
  for I::"'b::banach"
  by transfer (simp add: o_def)

lemma eq_integrals_pair_Vec: "(g has_integral I) (embed_vec ` S) \<longleftrightarrow> (g o embed_vec has_integral I) S"
  for I::"'b::banach"
  by transfer (simp add: o_def)

lemma pre_eq_integrals_pair_Vec: "((embed_vec o g) has_integral (embed_vec I)) (S) \<longleftrightarrow> (g has_integral I) S"
  by transfer (simp add: o_def)

lemma pre_has_derivative_within_eq_pair_Vec': "has_derivative_at_within (embed_vec o f) (embed_vec o f') a S \<longleftrightarrow> has_derivative_at_within f f' a S"
  by transfer (simp add: o_def)

lemma has_derivative_within_eq_pair_Vec': "has_derivative_at_within f f' (embed_vec a) (embed_vec ` S) \<longleftrightarrow> has_derivative_at_within (f o embed_vec) (f' o embed_vec) a S"
  by transfer (simp add: o_def)

lemma has_derivative_within_eq_pair_Vec'': "(f has_derivative f') (at (embed_vec a) within (embed_vec ` S)) \<longleftrightarrow> ((f o embed_vec) has_derivative (f' o embed_vec)) (at a within S)"
  using has_derivative_within_eq_pair_Vec'
  unfolding has_derivative_at_within_def by blast

lemma pre_has_derivative_within_eq_pair_Vec'': "((embed_vec o f) has_derivative (embed_vec o f')) (at a within S) \<longleftrightarrow> (f has_derivative f') (at a within S)"
  using pre_has_derivative_within_eq_pair_Vec'
  unfolding has_derivative_at_within_def by blast

lemma eq_integrable_on_pair_Vec: "(g:: (real, 2) vec \<Rightarrow> 'b::banach) integrable_on (embed_vec ` S) \<longleftrightarrow> (g o embed_vec) integrable_on S"
  unfolding integrable_on_def
  apply auto
  using eq_integrals_pair_Vec[where g = g and S = S]  by blast+

lemma eq_integral_pair_Vec:
  shows "(integral (embed_vec ` S)  (g:: (real, 2) vec \<Rightarrow> 'b::banach)) = (integral S (g o embed_vec))"
proof-
  have "g integrable_on (embed_vec ` S) \<Longrightarrow> (integral (embed_vec ` S)  (g:: (real, 2) vec \<Rightarrow> 'b::banach)) = (integral S (g o embed_vec))"
    using has_integral_eq_pair_Vec eq_integrable_on_pair_Vec eq_integrals_pair_Vec not_integrable_integral
    by fastforce
  moreover have "(g o embed_vec) integrable_on S \<Longrightarrow> (integral (embed_vec ` S)  (g:: (real, 2) vec \<Rightarrow> 'b::banach)) = (integral S (g o embed_vec))"
    using has_integral_eq_pair_Vec eq_integrable_on_pair_Vec eq_integrals_pair_Vec not_integrable_integral
    by fastforce
  moreover have "\<not> g integrable_on (embed_vec ` S) \<Longrightarrow> \<not> (g o embed_vec) integrable_on S \<Longrightarrow> (integral (embed_vec ` S)  (g:: (real, 2) vec \<Rightarrow> 'b::banach)) = (integral S (g o embed_vec))"
    using not_integrable_integral
    by metis
  ultimately show ?thesis by auto
qed

lemma rw: "(\<lambda>x. norm (g x)) o embed_vec = (\<lambda>x. norm ((g \<circ> embed_vec) x))" by auto

lemma eq_absolutely_integrable_on_pair_Vec: " (g:: (real, 2) vec \<Rightarrow> 'b::{euclidean_space,banach,second_countable_topology}) absolutely_integrable_on (embed_vec ` S) \<longleftrightarrow> (g o embed_vec) absolutely_integrable_on S"
  unfolding absolutely_integrable_on_def
  apply auto
  using eq_integrals_pair_Vec rw by (force simp add: o_def)+

lemma [transfer_rule]: "((=)===>vec_rel) embed_pair id "
  unfolding rel_fun_def vec_rel_def embed_vec_def embed_pair_def
  apply auto subgoal for x using euclidean_representation[of x, symmetric] unfolding Vec_2_Basis
    by (simp add: axis_eq_axis sum.insert)
  done

lemma pre_has_derivative_within_eq_Vec_pair': "((embed_pair o f) has_derivative (embed_pair o f')) (at a within S) \<longleftrightarrow> (f has_derivative f') (at a within S)"
proof -
  have "has_derivative_at_within (id o f) (id o f') a S \<longleftrightarrow> has_derivative_at_within f f' a S"
    by (simp add: o_def)
  from this[untransferred] have "has_derivative_at_within (embed_pair o f) (embed_pair o f') a S \<longleftrightarrow> has_derivative_at_within f f' a S" .
  then show ?thesis
    unfolding has_derivative_at_within_def by auto
qed

definition pair_rel
  where "pair_rel a b \<longleftrightarrow> b = (embed_pair a)"

lemma [transfer_rule]:
  shows "bi_unique pair_rel" "bi_total pair_rel"
  "rel_set pair_rel Basis Basis"\<comment>\<open>generalize assms of @{thm has_integral_transfer}\<close>
  "(pair_rel ===> pair_rel ===> (=)) (\<bullet>) (\<bullet>)"
  "(pair_rel ===> pair_rel ===> pair_rel) (+) (+)"
  "((=) ===> pair_rel ===> pair_rel) (%x y. x *\<^sub>R y) (%x y. x *\<^sub>R y)"
  subgoal
    apply (auto simp: pair_rel_def bi_unique_def embed_pair_def)
    by (metis (mono_tags, hide_lams) cart_eq_inner_axis exhaust_2 vec_eq_iff)
  subgoal
    apply (auto simp: pair_rel_def bi_total_def embed_pair_def) subgoal for a b
    proof-
      have "(a *\<^sub>R axis (1::2) (1::real) + b *\<^sub>R axis (2::2) (1::real)) \<bullet> axis (1::2) 1 = a" "(a *\<^sub>R axis (1::2) (1::real) + b *\<^sub>R axis (2::2) (1::real)) \<bullet> axis (2::2) 1 = b"
        by (simp add: Vec_2_Basis inner_add_left inner_axis_axis)+
      then show ?thesis by metis
    qed
  done
  subgoal
    by (auto simp: pair_rel_def bi_unique_def embed_pair_def rel_set_def Basis_prod_def Vec_2_Basis inner_axis_axis)
  subgoal
    apply (auto simp: pair_rel_def bi_unique_def embed_pair_def rel_fun_def)
      by (smt cart_eq_inner_axis inner_real_def inner_vec_def sum_2 euclidean_inner Vec_2_Basis)
  subgoal
    by (auto simp: pair_rel_def bi_unique_def embed_pair_def
        algebra_simps
        intro!: rel_funI)
  subgoal
    by (auto simp: pair_rel_def bi_unique_def embed_pair_def algebra_simps
        intro!: rel_funI)
  done


lemma [transfer_rule]: "((=) ===> pair_rel) (\<lambda>x. x) embed_pair"
  by (auto intro!: rel_funI simp: pair_rel_def)

lemma has_integral_eq_Vec_pair: "(g has_integral I) S \<longleftrightarrow> (g' has_integral I) S'"
  if [transfer_rule]: "(pair_rel ===> (=)) g g'" "rel_set pair_rel S S'"
  for I::"'b::banach"
  by transfer simp

lemma eq_integrals_Vec_pair: "(g has_integral I) (embed_pair ` S) \<longleftrightarrow> (g o embed_pair has_integral I) S"
  for I::"'b::banach"
  by transfer (simp add: o_def)

lemma eq_integrable_on_Vec_pair: "(g:: (real\<times>real) \<Rightarrow> 'b::banach) integrable_on (embed_pair ` S) \<longleftrightarrow> (g o embed_pair) integrable_on S"
  unfolding integrable_on_def
  apply auto
  using eq_integrals_Vec_pair[where g = g and S = S]  by blast+

lemma eq_integral_Vec_pair:
  shows "(integral (embed_pair ` S)  (g:: (real\<times>real) \<Rightarrow> 'b::banach)) = (integral S (g o embed_pair))"
proof-
  have "g integrable_on (embed_pair ` S) \<Longrightarrow> (integral (embed_pair ` S)  g) = (integral S (g o embed_pair))"
    using has_integral_eq_Vec_pair eq_integrable_on_Vec_pair eq_integrals_Vec_pair not_integrable_integral
    by fastforce
  moreover have "(g o embed_pair) integrable_on S \<Longrightarrow> (integral (embed_pair ` S)  g) = (integral S (g o embed_pair))"
    using has_integral_eq_Vec_pair eq_integrable_on_Vec_pair eq_integrals_Vec_pair not_integrable_integral
    by fastforce
  moreover have "\<not> g integrable_on (embed_pair ` S) \<Longrightarrow> \<not> (g o embed_pair) integrable_on S \<Longrightarrow> (integral (embed_pair ` S)  g) = (integral S (g o embed_pair))"
    using not_integrable_integral
    by metis
  ultimately show ?thesis by auto
qed

lemma eq_absolutely_integrable_on_Vec_pair: " (g:: (real\<times>real) \<Rightarrow> 'b::{euclidean_space,banach,second_countable_topology}) absolutely_integrable_on (embed_pair ` S) \<longleftrightarrow> (g o embed_pair) absolutely_integrable_on S"
  unfolding absolutely_integrable_on_def
  apply auto
  using eq_integrals_Vec_pair rw by (force simp add: o_def)+

lemma has_derivative_within_eq_Vec_pair: "has_derivative_at_within f f' a S \<longleftrightarrow> has_derivative_at_within g g' b T"
  if [transfer_rule]: "(pair_rel ===> (=)) f' g'" "(pair_rel ===> (=)) f g" "rel_set pair_rel S T" "pair_rel a b" 
  for I::"'b::banach"
  apply transfer
  by simp


lemma has_derivative_within_eq_Vec_pair': "has_derivative_at_within f f' (embed_pair a) (embed_pair ` S) \<longleftrightarrow> has_derivative_at_within (f o embed_pair) (f' o embed_pair) a S"
  by transfer (simp add: o_def)


lemma has_derivative_within_eq_Vec_pair'': "(f has_derivative f') (at (embed_pair a) within (embed_pair ` S)) \<longleftrightarrow> ((f o embed_pair) has_derivative (f' o embed_pair)) (at a within S)"
  using has_derivative_within_eq_Vec_pair'
  unfolding has_derivative_at_within_def by auto

lemma eq_continuous_on_Vec_pair: "continuous_on (embed_pair ` S) g \<longleftrightarrow> continuous_on S (g o embed_pair)"
  for I::"'b::banach"
  by transfer (simp add: o_def)

lemma pre_eq_continuous_on_Vec_pair: "continuous_on S g \<longleftrightarrow> continuous_on S (embed_pair o g)"
  for I::"'b::banach"
  by transfer (simp add: o_def)

lemma pre_eq_integrals_Vec_pair: "((embed_pair o g) has_integral (embed_pair I)) (S) \<longleftrightarrow> (g has_integral I) S"
 by transfer (simp add: o_def)

end


lemma embed_vec_pair_id:
  "embed_vec o embed_pair = id" (is ?g1)
  "embed_pair o (embed_vec::((real\<times>real) \<Rightarrow> (real,2) vec)) = id" (is ?g2)
proof-
  {
    fix x
    have "axis (1::2) (1::real) \<noteq> axis (2::2) (1::real)"
      by (simp add: axis_eq_axis)
    hence "(x \<bullet> axis (1::2) (1::real)) *\<^sub>R axis (1::2) (1::real) + (x \<bullet> axis 2 1) *\<^sub>R axis 2 1 = (\<Sum>b\<in>Basis. (x \<bullet> b) *\<^sub>R b)"
      unfolding Vec_2_Basis
      by auto
    also have "... = x"
      using euclidean_representation[simplified Vec_2_Basis] .
    finally have "(x \<bullet> axis (1::2) (1::real)) *\<^sub>R axis (1::2) (1::real) + (x \<bullet> axis 2 1) *\<^sub>R axis 2 1 = x"
      .
  }
  thus ?g1
    by (auto intro!: HOL.ext simp add: embed_pair_def embed_vec_def)
  show ?g2
    by (auto intro!: HOL.ext
             simp add: embed_pair_def embed_vec_def axis_eq_axis algebra_simps inner_axis_axis)
qed

end
