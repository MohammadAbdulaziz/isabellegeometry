theory General_Utils
  imports "HOL-Analysis.Analysis"
begin

lemma lambda_skolem_gen: "(\<forall>i. \<exists>f'::('a ^ 'n) \<Rightarrow> 'a. P i f') \<longleftrightarrow>
   (\<exists>f'::('a ^ 'n) \<Rightarrow> ('a ^ 'n). \<forall>i. P i ((\<lambda>x. (f' x) $ i)))" (is "?lhs \<longleftrightarrow> ?rhs")
  (*using choice_iff lambda_skolem*)
proof -
  { assume H: "?rhs"
    then have ?lhs by auto }
  moreover
  { assume H: "?lhs"
    then obtain f'' where f'':"\<forall>i. P i (f'' i)" unfolding choice_iff
      by metis
    let ?x = "(\<lambda>x. (\<chi> i. (f''  i) x))"
    { fix i
      from f'' have "P i (f'' i)" by metis
      then have "P i (\<lambda>x.(?x x) $ i)" by auto
    }
    hence "\<forall>i. P i (\<lambda>x.(?x x) $ i)" by metis
    hence ?rhs by metis}
  ultimately show ?thesis by metis
qed

lemma lambda_skolem_euclidean: "(\<forall>i\<in>Basis. \<exists>f'::('a::{euclidean_space}\<Rightarrow>real). P i f') \<longleftrightarrow>
   (\<exists>f'::('a::euclidean_space\<Rightarrow>'b::euclidean_space). \<forall>i\<in>Basis. P i ((\<lambda>x. (f' x) \<bullet> i)))" (is "?lhs \<longleftrightarrow> ?rhs")
  (*using choice_iff lambda_skolem*)
proof -
  { assume H: "?rhs"
    then have ?lhs by auto }
  moreover
  { assume H: "?lhs"
    then obtain f'' where f'':"\<forall>i::('b::euclidean_space)\<in>Basis. P i (f'' i)" unfolding choice_iff
      by metis
    let ?x = "(\<lambda>x. (\<Sum>i\<in>Basis. ((f''  i) x) *\<^sub>R i))"
    { fix i::"'b::euclidean_space"
      assume ass: "i\<in>Basis"
      then have "P i (f'' i)"
        using f''
        by metis
      then have "P i (\<lambda>x.(?x x) \<bullet> i)" using ass by auto
    }
    hence *: "\<forall>i\<in>Basis. P i (\<lambda>x.(?x x) \<bullet> i)" by auto
    then have ?rhs
      apply auto
    proof
      let ?f'6 = ?x
      show " \<forall>i\<in>Basis. P i (\<lambda>x. ?f'6 x \<bullet> i)" using * by auto
    qed}
  ultimately show ?thesis by metis
qed

lemma lambda_skolem_euclidean_explicit: "(\<forall>i\<in>Basis. \<exists>f'::('a::{euclidean_space}\<Rightarrow>real). P i f') \<longleftrightarrow>
   (\<exists>f'::('a::{euclidean_space}\<Rightarrow>'a). \<forall>i\<in>Basis. P i ((\<lambda>x. (f' x) \<bullet> i)))" (is "?lhs \<longleftrightarrow> ?rhs")
  (*using choice_iff lambda_skolem*)
proof -
  { assume H: "?rhs"
    then have ?lhs by auto }
  moreover
  { assume H: "?lhs"
    then obtain f'' where f'':"\<forall>i::('a::euclidean_space)\<in>Basis. P i (f'' i)" unfolding choice_iff
      by metis
    let ?x = "(\<lambda>x. (\<Sum>i\<in>Basis. ((f''  i) x) *\<^sub>R i))"
    { fix i::"'a::euclidean_space"
      assume ass: "i\<in>Basis"
      then have "P i (f'' i)"
        using f''
        by metis
      then have "P i (\<lambda>x.(?x x) \<bullet> i)" using ass by auto
    }
    hence *: "\<forall>i\<in>Basis. P i (\<lambda>x.(?x x) \<bullet> i)" by auto
    then have ?rhs
      apply auto
    proof
      let ?f'6 = ?x
      show " \<forall>i\<in>Basis. P i (\<lambda>x. ?f'6 x \<bullet> i)" using * by auto
    qed}
  ultimately show ?thesis by metis
qed

lemma indic_ident:
  "\<And> (f::'a \<Rightarrow> real) s. (\<lambda>x. (f x) * indicator s x) = (\<lambda>x. if x \<in> s then f x else 0)"
proof
  fix f::"'a \<Rightarrow> real"
  fix s::"'a set"
  fix x:: 'a
  show " f x * indicator s x = (if x \<in> s then f x else 0)"
    by (simp add: indicator_def)
qed

lemma real_pair_basis: "Basis = {(1::real,0::real), (0::real,1::real)}"
  by (simp add: Basis_prod_def insert_commute)


(*AE_measure_singleton*)

lemma real_singleton_in_borel:
  shows "{a::real} \<in> sets borel"
  using Borel_Space.cbox_borel[of "a" "a"]
  apply auto
  done

lemma real_singleton_in_lborel:
  shows "{a::real} \<in> sets lborel"
  using real_singleton_in_borel
  apply auto
  done

lemma cbox_diff:
  shows "{0::real..1} - {0,1} = box 0 1"
  by (auto simp add: cbox_def)

lemma sum_bij:
  assumes "bij F"
    "\<forall>x\<in>s. f x = g (F x)"
  shows "\<And>t. F ` s =  t \<Longrightarrow> sum f s = sum g t"
  by (metis assms bij_betw_def bij_betw_subset subset_UNIV sum.reindex_cong)

lemma sum_inj_on:
  assumes "inj_on F s"
    "\<forall>x\<in>s. f x = g (F x)"
  shows "\<And>t. F ` s =  t \<Longrightarrow> sum f s = sum g t"
  by (metis assms sum.reindex_cong)

abbreviation surj_on where
  "surj_on s f \<equiv> s \<subseteq> range f"

lemma surj_on_image_vimage_eq: "surj_on s f \<Longrightarrow> f ` (f -` s) = s"
  by fastforce

lemma sum_zero_set:
  assumes "\<forall>x \<in> s. f x = 0" "finite s" "finite t"
  shows "sum f (s \<union> t) = sum f t"
  using assms
  by (simp add: IntE sum.union_inter_neutral sup_commute)


lemma continuous_on_product_then_coordinatewise':
  assumes "continuous_on s f"
  shows "continuous_on s (\<lambda>x. f x a)"
  by (simp add: assms continuous_on_product_then_coordinatewise)
  
lemma continuous_on_coordinatewise_then_product' [intro, continuous_intros]:
  assumes "\<And>i. continuous_on s (\<lambda>x. f x i)"
  shows "continuous_on s f"
  by (simp add: assms continuous_on_coordinatewise_then_product)

lemma continuous_on_product_coordinatewise_iff:
  "continuous_on s f \<longleftrightarrow> (\<forall>i. continuous_on s (\<lambda>x. f x i))"
  by (auto intro: continuous_on_product_then_coordinatewise')

lemma bounded_linearE:
  assumes "bounded_linear f"
  obtains g where "blinfun_apply g = f"
  using assms bounded_linear_Blinfun_apply by blast

lemma frontier_Un_subset_Un_frontier:
     "frontier (s \<union> t) \<subseteq> (frontier s) \<union> (frontier t)"
  by (simp add: frontier_def Un_Diff) (auto simp add: closure_def interior_def islimpt_def)

lemma continuous_add_imp_continuous:
  assumes "continuous_on s (\<lambda>x.(c::'a::euclidean_space) + f x )"
  shows "continuous_on s (\<lambda>x. f x)" 
proof-
  have rw: "(\<lambda>x. f x) = (\<lambda>x. ((\<lambda>x. c + f x) x) + (- c))" by auto
  show ?thesis
    apply(subst rw)
    using assms
    by(rule continuous_on_add, auto intro!: continuous_intros)
qed

lemma continuous_add_imp_continuous':
  assumes "continuous_on s (\<lambda>x::'a::topological_space. ((g::'a \<Rightarrow> ('b::euclidean_space)) x) + f x )" "continuous_on s g" 
  shows "continuous_on s (\<lambda>x. f x)" 
proof-
  have rw: "(\<lambda>x. f x) = (\<lambda>x. ((\<lambda>x. g x + f x) x) - (g x))" by auto
  show ?thesis
    apply(subst rw)
    using assms
    by(intro continuous_on_diff, auto intro!: continuous_intros)
qed


lemma continuous_scaleR_imp_continuous:
  assumes "continuous_on s (\<lambda>x. f x *\<^sub>R (c::'a::euclidean_space))" "c \<bullet> c = 1"
  shows "continuous_on s (\<lambda>x. f x)" 
proof-
  have rw: "(\<lambda>x. f x) = (\<lambda>x. ((\<lambda>x. f x *\<^sub>R c) x) \<bullet> c)" using assms by auto
  show ?thesis
    apply(subst rw)
    using assms
    by(intro continuous_on_inner, auto intro!: continuous_intros)
qed

lemma linepath_vimage_singleton: "(linepath a b) -` {x} = {(x - a)/(b -a)}" if "a <b"
proof-
  have "(\<lambda>x. (a + (b - a) * x)) = linepath a b" by (auto simp add: linepath_def algebra_simps)
  moreover have "\<forall>x. (\<lambda>x. (a + (b - a) * x)) -` {x} = {(x - a)/(b -a)}"
    using that by auto
  ultimately show ?thesis by auto
qed

lemma linepath_vimage_singleton': "(linepath a b) -` {x} = {(x - a)/(b -a)}" if "a \<noteq> b"
proof-
  have "(\<lambda>x. (a + (b - a) * x)) = linepath a b" by (auto simp add: linepath_def algebra_simps)
  moreover have "\<forall>x. (\<lambda>x. (a + (b - a) * x)) -` {x} = {(x - a)/(b -a)}"
    using that by auto
  ultimately show ?thesis by auto
qed

lemma add_scale_img:
  assumes "a < b" shows "(\<lambda>x::real. a + (b - a) * x) ` {0 .. 1} = {a .. b}"
  using assms
  apply (auto simp: algebra_simps affine_ineq image_iff)
  using less_eq_real_def apply force
  apply (rule_tac x="(x-a)/(b-a)" in bexI)
   apply (auto simp: field_simps)
  done

lemma add_scale_img'':
  assumes "a \<noteq> b" shows "(\<lambda>x::real. a + (b - a) * x) ` {0 .. 1} = ({a .. b} \<union> {b..a})"
  using assms[unfolded neq_iff]
  apply (auto simp: algebra_simps affine_ineq image_iff)
  using less_eq_real_def apply force
  apply (simp add: mult_left_mono)
  apply (rule_tac x="(x-a)/(b-a)" in bexI)
   apply (auto simp: field_simps)
  apply (rule_tac x="(x-a)/(b-a)" in bexI)
   apply (auto simp: field_simps)
  done

lemma linepath_img:
  assumes "(a::real) < (b::real)" shows "(linepath a b) ` {0::real .. 1} = {a .. b}"
proof- 
  have "(\<lambda>x. a + (b - a) * x) = (linepath a b)" unfolding linepath_def
    by (auto simp add: algebra_simps)
  then show ?thesis
    using add_scale_img[OF assms]
    by presburger
qed

lemma linepath_img':
  assumes "(a::real) \<noteq> (b::real)" shows "(linepath a b) ` {0::real .. 1} = ({a .. b} \<union> {b..a})"
proof- 
  have "(\<lambda>x. a + (b - a) * x) = (linepath a b)" unfolding linepath_def
    by (auto simp add: algebra_simps)
  then show ?thesis
    using add_scale_img''[OF assms]
    by presburger
qed

lemma add_scale_img':
  assumes "a \<le> b"
  shows "(\<lambda>x::real. a + (b - a) * x) ` {0 .. 1} = {a .. b}"
proof (cases "a = b")
  case True
  then show ?thesis by force
next
  case False
  then show ?thesis
    using add_scale_img assms by auto
qed

lemma continuous_on_linepath_compose:
  assumes a_lt_b: "a < b" and f_cont: "continuous_on {0..1} (f o(linepath a b))"
  shows "continuous_on (cbox (a::real) b) f"
proof-
  have shift_scale_cont: "continuous_on {a..b} (\<lambda>x. (x - a)*(1/(b-a)))"
    by (intro continuous_intros)
  have "(\<lambda>x. a + (b - a) * x) \<circ> (\<lambda>x. (x - a)*(1/(b-a))) = id"
    using a_lt_b by (auto simp add: o_def)
  then have linepath_inv: "(linepath a b) \<circ> (\<lambda>x. (x - a)*(1/(b-a))) = id"
    unfolding linepath_def
    by (auto simp add: algebra_simps)
  have img_shift_scale: "(\<lambda>x. (x - a)*(1/(b-a)))`{a..b} = {0..1}"
    using a_lt_b apply (auto simp: divide_simps image_iff)
    apply (rule_tac x="x * (b - a) + a" in bexI)
    using le_diff_eq by fastforce+
  have gamma1_y_component: "(\<lambda>x. f(linepath a b x)) = f \<circ> (linepath a b)"
    by auto
  have "continuous_on {a..b} ((\<lambda>x. f(linepath a b x)) \<circ> (\<lambda>x. (x - a)*(1/(b-a))))"
    using img_shift_scale continuous_on_compose shift_scale_cont f_cont
    by force
  then have "continuous_on {a..b} (f \<circ> ((linepath a b) \<circ> (\<lambda>x. (x - a)*(1/(b-a)))))"
    using gamma1_y_component by auto
  then show "continuous_on (cbox a b) f"
    unfolding linepath_inv
    using a_lt_b by simp
qed

lemma continuous_on_linepath_compose_2:
  assumes a_lt_b: "b < a" and f_cont: "continuous_on {0..1} (f o(linepath a b))"
  shows "continuous_on (cbox (b::real) a) f"
proof-
  have shift_scale_cont: "continuous_on {b..a} (\<lambda>x. (x - a)*(1/(b-a)))"
    by (intro continuous_intros)
  have "(\<lambda>x. a + (b - a) * x) \<circ> (\<lambda>x. (x - a)*(1/(b-a))) = id"
    using a_lt_b by (auto simp add: o_def)
  then have linepath_inv: "(linepath a b) \<circ> (\<lambda>x. (x - a)*(1/(b-a))) = id"
    unfolding linepath_def
    by (auto simp add: algebra_simps)
  have img_shift_scale: "(\<lambda>x. (x - a)*(1/(b-a)))`{b..a} = {0..1}"
    using a_lt_b apply (auto simp: divide_simps image_iff)
    apply (rule_tac x="x * (b - a) + a" in bexI)
    using le_diff_eq apply fastforce
    apply( auto simp: algebra_simps)
     apply (simp add: affine_ineq)
    by (simp add: mult_left_mono)
  have gamma1_y_component: "(\<lambda>x. f(linepath a b x)) = f \<circ> (linepath a b)"
    by auto
  have "continuous_on {b..a} ((\<lambda>x. f(linepath a b x)) \<circ> (\<lambda>x. (x - a)*(1/(b-a))))"
    using img_shift_scale continuous_on_compose shift_scale_cont f_cont
    by fastforce
  then have "continuous_on {b..a} (f \<circ> ((linepath a b) \<circ> (\<lambda>x. (x - a)*(1/(b-a)))))"
    using gamma1_y_component by auto
  then show "continuous_on (cbox b a) f"
    unfolding linepath_inv
    using a_lt_b by simp
qed

lemma continuous_on_linepath_compose':
  assumes a_lt_b: "a \<noteq> b" and f_cont: "continuous_on {0..1} (f o(linepath a b))"
  shows "continuous_on ((cbox (a::real) b) \<union> (cbox (b::real) a)) f"
proof-
  have shift_scale_cont: "continuous_on ({a..b} \<union> {b..a}) (\<lambda>x. (x - a)*(1/(b-a)))"
    by (intro continuous_intros)
  have "(\<lambda>x. a + (b - a) * x) \<circ> (\<lambda>x. (x - a)*(1/(b-a))) = id"
    using a_lt_b by (auto simp add: o_def)
  then have linepath_inv: "(linepath a b) \<circ> (\<lambda>x. (x - a)*(1/(b-a))) = id"
    unfolding linepath_def
    by (auto simp add: algebra_simps)
  have img_shift_scale: "(\<lambda>x. (x - a)*(1/(b-a)))`({a..b} \<union> {b..a}) = {0..1}"
  proof(cases "a <b")
    case True
    then show ?thesis
    apply (auto simp: divide_simps image_iff)
    apply (rule_tac x="x * (b - a) + a" in bexI)
    using le_diff_eq by fastforce+
  next
    case False
    then have "b < a" using a_lt_b by auto
    then show ?thesis
      apply (auto simp: divide_simps image_iff)
      apply (rule_tac x="x * (b - a) + a" in bexI)
    using le_diff_eq apply fastforce
    apply( auto simp: algebra_simps)
     apply (simp add: affine_ineq)
    by (simp add: mult_left_mono)
qed 
  have gamma1_y_component: "(\<lambda>x. f(linepath a b x)) = f \<circ> (linepath a b)"
    by auto
  have "continuous_on ({a..b} \<union> {b..a}) ((\<lambda>x. f(linepath a b x)) \<circ> (\<lambda>x. (x - a)*(1/(b-a))))"
    using img_shift_scale continuous_on_compose shift_scale_cont f_cont
    by force
  then have "continuous_on ({a..b} \<union> {b..a}) (f \<circ> ((linepath a b) \<circ> (\<lambda>x. (x - a)*(1/(b-a)))))"
    using gamma1_y_component by auto
  then show ?thesis
    unfolding linepath_inv
    using a_lt_b by simp
qed

lemma ball_pair: "(\<forall>x\<in>t. foo (fst x) (snd x)) \<longleftrightarrow> (\<forall>(x,y)\<in>t. foo x y)" for t
  by auto

lemma continous_at_cong:
  assumes "\<And>x. x\<in>s \<Longrightarrow> f x  = g x"
      "continuous (at (x::'a::metric_space) within s) f"
      "x \<in> s"
  shows "continuous (at x within s) g"
  using continuous_transform_within_openin[OF assms(2) _ assms(3) assms(1)]
  using assms
  by auto

end
