section \<open>The Diamond Example\<close>

theory DiamExample
  imports Green SymmetricR2Shapes
begin

locale diamond = R2 +
  fixes d::real
  assumes d_gt_0: "0 < d"
begin

definition diamond_x where
  "diamond_x \<equiv> \<lambda>t. (t - 1/2) * d"

definition diamond_y where
  "diamond_y \<equiv> \<lambda>t. d/2 - \<bar>t\<bar>"

definition diamond_cube where
  "diamond_cube = (\<lambda>(t1,t2). let x = (t1 - 1/2) * d in (x, (2 * t2 - 1) * (d/2 - \<bar>x\<bar>)))"

lemma diamond_cube_is_type_I:
  shows "typeI_twoCube (1::int, diamond_cube) i j"
  unfolding typeI_twoCube.simps
proof (intro exI conjI ballI)
  show "-d/2 \<noteq> d/2"
    using d_gt_0 by auto
  show "(\<forall>x\<in>{- d / 2..d / 2} \<union> {d / 2..- d / 2}. (\<lambda>x. - diamond_y x) x \<le> diamond_y x) \<or> (\<forall>x\<in>{- d / 2..d / 2} \<union> {d / 2..- d / 2}. diamond_y x \<le> (\<lambda>x. - diamond_y x) x)"
    using diamond_y_def by auto
  have f0: "finite {0}"
    by simp
  show "diamond_y piecewise_C1_differentiable_on {- d / 2..d / 2} \<union> {d / 2..- d / 2}"
       "(\<lambda>x. - diamond_y x) piecewise_C1_differentiable_on {- d / 2..d / 2} \<union> {d / 2..- d / 2}"
    unfolding diamond_y_def piecewise_C1_differentiable_on_def
    by (rule conjI exI f0 continuous_intros derivative_intros | force)+
  show "diamond_cube = (\<lambda>(t1, t2). let c = linepath (- d / 2) (d / 2) t1 in c *\<^sub>R i + linepath (- diamond_y c) (diamond_y c) t2 *\<^sub>R j)"
    by (auto simp: linepath_def Let_def diamond_cube_def diamond_x_def diamond_y_def divide_simps algebra_simps i_is_x_axis j_is_y_axis)
  show "(1::int) = (if - d / 2 < d / 2 then (1::int) else - 1) * (if \<forall>x\<in>{- d / 2..d / 2} \<union> {d / 2..- d / 2}. - diamond_y x \<le> diamond_y x then (1::int) else - 1)"
    by (auto simp: d_gt_0 linepath_def Let_def diamond_cube_def diamond_x_def diamond_y_def divide_simps algebra_simps i_is_x_axis j_is_y_axis)
qed

lemma diamond_cube_valid_two_cube:
  shows "valid_two_cube diamond_cube"
  apply (auto simp add: valid_two_cube_def boundary_def diamond_cube_def diamond_x_def card_insert_if)
  apply (metis (no_types, hide_lams) add_diff_cancel_left' d_gt_0 dbl_simps(3) diff_divide_distrib fst_conv inner_real_def mult.commute mult_zero_left mult_zero_right order_less_irrefl real_mult_less_iff1 right_minus_eq zero_less_one)
  apply (metis (no_types, hide_lams) add_diff_cancel_right' d_gt_0 mult_cancel_left mult_zero_right order_less_irrefl prod.inject)
  done

definition diamond_top_left_edge where
  "diamond_top_left_edge = (- 1::int, (\<lambda>x. (diamond_x (1/2 * x),  (diamond_x (1/2 * x)) + d/2)))"

definition diamond_top_right_edge where
  "diamond_top_right_edge = (- 1::int, (\<lambda>x. (diamond_x (1/2 * x + 1/2),  (-(diamond_x (1/2 * x + 1/2)) + d/2))))"

definition diamond_bot_left_edge where
  "diamond_bot_left_edge = (1::int, (\<lambda>x. (diamond_x (1/2 * x), - (diamond_x (1/2 * x) + d/2))))"

definition diamond_bot_right_edge where
  "diamond_bot_right_edge = (1::int, (\<lambda>x. (diamond_x (1/2 * x + 1/2), - (-(diamond_x (1/2 * x + 1/2)) + d/2))))"

lemma diamond_edges_are_valid:
   "valid_path (snd (diamond_top_left_edge))"
    "valid_path (snd (diamond_top_right_edge))"
    "valid_path (snd (diamond_bot_left_edge))"
    "valid_path (snd (diamond_bot_right_edge))"
  by (auto simp add: valid_path_def diamond_top_left_edge_def diamond_bot_right_edge_def diamond_bot_left_edge_def diamond_top_right_edge_def diamond_x_def)

definition diamond_cube_boundary_to_subdiv where
  "diamond_cube_boundary_to_subdiv gamma = 
 (if (gamma = top_edge diamond_cube) then
       {diamond_top_left_edge, diamond_top_right_edge}
 else if (gamma = bot_edge diamond_cube) then
   {diamond_bot_left_edge, diamond_bot_right_edge}
 else {})"

definition rot_diamond_cube_boundary_to_subdiv where
  "rot_diamond_cube_boundary_to_subdiv (gamma::(int \<times> (real \<Rightarrow> real \<times> real))) \<equiv>
     if (gamma = top_edge (typeI_to_typeII diamond_cube) ) then {diamond_bot_right_edge, diamond_top_right_edge}
     else if (gamma = bot_edge (typeI_to_typeII diamond_cube)) then {diamond_bot_left_edge, diamond_top_left_edge}
     else {}"

lemma diamond_top_left_edge_neq_diamond_top_right_edge:
     "diamond_top_left_edge \<noteq> diamond_top_right_edge"
     "diamond_top_left_edge \<noteq> diamond_bot_right_edge"
     "diamond_top_left_edge \<noteq> diamond_bot_right_edge"
     "diamond_bot_left_edge \<noteq> diamond_bot_right_edge"
     "diamond_bot_left_edge \<noteq> diamond_top_left_edge"
     "diamond_bot_left_edge \<noteq> diamond_top_right_edge"
     "diamond_bot_right_edge \<noteq> diamond_top_right_edge"
     "diamond_bot_right_edge \<noteq> diamond_top_right_edge"
     "diamond_top_left_edge \<noteq> diamond_top_right_edge"
  unfolding diamond_bot_left_edge_def diamond_bot_right_edge_def diamond_x_def diamond_y_def diamond_top_left_edge_def diamond_top_right_edge_def
    using d_gt_0
    apply (auto simp add: algebra_simps divide_simps)
    by (smt divide_cancel_right prod.inject)+

lemma diamond_bot_edges_neq_diamond_top_edges:
  "bot_edge diamond_cube \<noteq>  top_edge diamond_cube"
  "right_edge diamond_cube \<noteq>  top_edge diamond_cube"
  "left_edge diamond_cube \<noteq>  top_edge diamond_cube"
  "bot_edge diamond_cube \<noteq>  left_edge diamond_cube"
  "right_edge diamond_cube \<noteq>  left_edge diamond_cube"
  "bot_edge diamond_cube \<noteq>  right_edge diamond_cube"
  unfolding diamond_cube_def
  using d_gt_0
       apply(auto simp add: diamond_x_def diamond_y_def fun_eq_iff bot_edge_def right_edge_def left_edge_def top_edge_def i_is_x_axis j_is_y_axis Let_def)
  by (metis add.inverse_inverse cancel_comm_monoid_add_class.diff_cancel divide_eq_0_iff mult.commute mult_zero_right neg_equal_zero order_less_irrefl zero_neq_numeral)+

lemma diamond_bot_edges_neq_diamond_top_edges_typeII'':
  "bot_edge (typeI_to_typeII diamond_cube) \<noteq>  top_edge (typeI_to_typeII diamond_cube)"
  "right_edge (typeI_to_typeII diamond_cube) \<noteq>  top_edge (typeI_to_typeII diamond_cube)"
  "left_edge (typeI_to_typeII diamond_cube) \<noteq>  top_edge (typeI_to_typeII diamond_cube)"
  "bot_edge (typeI_to_typeII diamond_cube) \<noteq>  left_edge (typeI_to_typeII diamond_cube)"
  "right_edge (typeI_to_typeII diamond_cube) \<noteq>  left_edge (typeI_to_typeII diamond_cube)"
  "bot_edge (typeI_to_typeII diamond_cube) \<noteq>  right_edge (typeI_to_typeII diamond_cube)"
  unfolding diamond_cube_def
  using d_gt_0 apply(auto simp add: diamond_x_def diamond_y_def fun_eq_iff bot_edge_def right_edge_def left_edge_def top_edge_def i_is_x_axis j_is_y_axis typeI_to_typeII_def Let_def)
  by (metis add.inverse_inverse cancel_comm_monoid_add_class.diff_cancel divide_eq_0_iff mult.commute mult_zero_right neg_equal_zero order_less_irrefl)

lemma diamond_cube_is_only_vertical_div_of_rot:
  shows "only_vertical_division' (\<partial>diamond_cube) {(1, typeI_to_typeII diamond_cube)}"
  unfolding only_vertical_division'_def
proof (rule exI [of _ "{}"], simp add: case_prod_unfold, intro conjI ballI)
  let ?\<V> = "(\<partial>diamond_cube)"
  show 0: "finite ?\<V>"
    by (simp add: finite_boundary)
  show "boundary_chain (\<partial>diamond_cube)"
    by (simp add: two_cube_boundary_is_boundary)
  show "\<And>x. x \<in> \<partial>diamond_cube \<Longrightarrow> valid_path (snd x)"
    using typeI_edges_are_valid_paths[OF diamond_cube_is_type_I] by auto
  have 1: "boundary_chain ?\<V>" using two_cube_boundary_is_boundary by auto
  let ?subdiv = "{diamond_top_left_edge, diamond_top_right_edge, diamond_bot_left_edge, diamond_bot_right_edge}"
  let ?pi = "{left_edge diamond_cube, right_edge diamond_cube}"
  show "common_subdiv_exists (two_chain_horizontal_boundary {(1, typeI_to_typeII diamond_cube)}) (\<partial> diamond_cube) \<or>
               common_reparam_exists (\<partial> diamond_cube) (two_chain_horizontal_boundary {(1, typeI_to_typeII diamond_cube)})"
    unfolding common_subdiv_exists_def
  proof (intro exI conjI HOL.disjI1)
    show "chain_subdiv_chain (\<partial>diamond_cube - ?pi) ?subdiv"
      unfolding chain_subdiv_chain_character
    proof (intro exI conjI)
      have 1: "(\<partial>diamond_cube) - ?pi = {top_edge diamond_cube, bot_edge diamond_cube}"
        unfolding boundary_edges using diamond_bot_edges_neq_diamond_top_edges by auto
      let ?f = "diamond_cube_boundary_to_subdiv"
      show "UNION (\<partial>diamond_cube - ?pi) ?f = ?subdiv"
        unfolding 1
        by (auto simp add: diamond_bot_edges_neq_diamond_top_edges diamond_cube_boundary_to_subdiv_def)
      show "\<forall>twoC\<in>\<partial> diamond_cube - {left_edge diamond_cube, right_edge diamond_cube}. chain_subdiv_path (coeff_cube_to_path twoC) (?f twoC)" 
        unfolding 1 diamond_cube_boundary_to_subdiv_def
      proof (simp, intro conjI impI)
        have rw: "{x, y} = {y, x}" for x y by auto
        show "chain_subdiv_path (coeff_cube_to_path (top_edge diamond_cube)) {diamond_top_left_edge, diamond_top_right_edge}"
        proof(subst rw, rule chain_subdiv_path_cong_intros)
          show "(coeff_cube_to_path (top_edge diamond_cube)) = ((coeff_cube_to_path diamond_top_right_edge) +++ (coeff_cube_to_path diamond_top_left_edge))"
            unfolding top_edge_def diamond_top_right_edge_def diamond_top_left_edge_def diamond_cube_def diamond_x_def diamond_y_def
            using d_gt_0
            by (auto simp add: reversepath_def joinpaths_def algebra_simps divide_simps)
          show "pathfinish (coeff_cube_to_path diamond_top_right_edge) = pathstart (coeff_cube_to_path diamond_top_left_edge)"
            unfolding diamond_x_def reversepath_def coeff_cube_to_path.simps pathfinish_def diamond_top_right_edge_def
              diamond_top_left_edge_def pathstart_def
            by auto
        qed(auto intro: chain_subdiv_path.intros, metis diamond_top_left_edge_neq_diamond_top_right_edge)
        then show "chain_subdiv_path (coeff_cube_to_path (top_edge diamond_cube)) {diamond_top_left_edge, diamond_top_right_edge}" .
        show "chain_subdiv_path (coeff_cube_to_path (bot_edge diamond_cube)) {diamond_bot_left_edge, diamond_bot_right_edge}"
        proof(rule chain_subdiv_path_cong_intros)
          show "(coeff_cube_to_path (bot_edge diamond_cube)) = ((coeff_cube_to_path diamond_bot_left_edge) +++ (coeff_cube_to_path diamond_bot_right_edge))"
            unfolding bot_edge_def diamond_bot_right_edge_def diamond_bot_left_edge_def diamond_cube_def diamond_x_def diamond_y_def
            using d_gt_0
            by (auto simp add: reversepath_def joinpaths_def algebra_simps divide_simps)
          show "pathfinish (coeff_cube_to_path diamond_bot_left_edge) = pathstart (coeff_cube_to_path diamond_bot_right_edge)"
            unfolding diamond_x_def reversepath_def coeff_cube_to_path.simps pathfinish_def diamond_bot_right_edge_def
              diamond_bot_left_edge_def pathstart_def
            by auto
        qed(auto intro: chain_subdiv_path.intros, metis diamond_top_left_edge_neq_diamond_top_right_edge)
      qed
    qed  (auto simp add: diamond_cube_boundary_to_subdiv_def boundary_edges; metis diamond_top_left_edge_neq_diamond_top_right_edge)+
    let ?pj = "{left_edge (typeI_to_typeII diamond_cube),
                right_edge (typeI_to_typeII diamond_cube)}"
    show "chain_subdiv_chain (two_chain_horizontal_boundary {(1, typeI_to_typeII diamond_cube)} - ?pj) {diamond_top_left_edge, diamond_top_right_edge, diamond_bot_left_edge, diamond_bot_right_edge}"
      unfolding chain_subdiv_chain_character
    proof (intro exI conjI)
      have 1: "two_chain_horizontal_boundary {(1, typeI_to_typeII diamond_cube)} - ?pj = {top_edge (typeI_to_typeII diamond_cube), bot_edge (typeI_to_typeII diamond_cube)}"
        unfolding two_chain_horizontal_boundary_def horizontal_boundary_edges
        using diamond_bot_edges_neq_diamond_top_edges_typeII''
        by (auto simp add:  rot_diamond_cube_boundary_to_subdiv_def)
      let ?f = "rot_diamond_cube_boundary_to_subdiv"
      show "UNION (two_chain_horizontal_boundary {(1, typeI_to_typeII diamond_cube)} - ?pj) ?f = ?subdiv"
        unfolding 1
        using diamond_bot_edges_neq_diamond_top_edges_typeII''
        by (auto simp add:  rot_diamond_cube_boundary_to_subdiv_def)
      show "\<forall>twoC\<in>two_chain_horizontal_boundary {(1, typeI_to_typeII diamond_cube)} - {left_edge (typeI_to_typeII diamond_cube), right_edge (typeI_to_typeII diamond_cube)}.
                               chain_subdiv_path (coeff_cube_to_path twoC) (rot_diamond_cube_boundary_to_subdiv twoC)" 
        unfolding 1 rot_diamond_cube_boundary_to_subdiv_def
      proof (simp, intro conjI impI)
        show "chain_subdiv_path (coeff_cube_to_path (top_edge (typeI_to_typeII diamond_cube))) {diamond_bot_right_edge, diamond_top_right_edge}"
        proof(rule chain_subdiv_path_cong_intros)
          show "coeff_cube_to_path (top_edge (typeI_to_typeII diamond_cube)) = coeff_cube_to_path diamond_bot_right_edge +++ coeff_cube_to_path diamond_top_right_edge"
            unfolding top_edge_def diamond_bot_right_edge_def diamond_top_right_edge_def diamond_bot_left_edge_def diamond_top_left_edge_def diamond_top_left_edge_def diamond_cube_def diamond_x_def diamond_y_def typeI_to_typeII_def
            using d_gt_0
            by (auto simp add: reversepath_def joinpaths_def algebra_simps divide_simps)
          show "pathfinish (coeff_cube_to_path diamond_bot_right_edge) = pathstart (coeff_cube_to_path diamond_top_right_edge)"
            unfolding diamond_x_def reversepath_def coeff_cube_to_path.simps pathfinish_def diamond_bot_right_edge_def diamond_top_right_edge_def pathstart_def 
            by auto
        qed(auto intro: chain_subdiv_path.intros, metis diamond_top_left_edge_neq_diamond_top_right_edge)
        then show " chain_subdiv_path (coeff_cube_to_path (top_edge (typeI_to_typeII diamond_cube))) {diamond_bot_right_edge, diamond_top_right_edge}" .
        have rw: "{x, y} = {y, x}" for x y by auto
        show "chain_subdiv_path (coeff_cube_to_path (bot_edge (typeI_to_typeII diamond_cube))) {diamond_bot_left_edge, diamond_top_left_edge}"
        proof(subst rw, rule chain_subdiv_path_cong_intros)
          show "coeff_cube_to_path (bot_edge (typeI_to_typeII diamond_cube)) = coeff_cube_to_path diamond_top_left_edge +++ coeff_cube_to_path diamond_bot_left_edge"
            unfolding bot_edge_def typeI_to_typeII_def prod.swap_def o_def diamond_bot_left_edge_def diamond_top_left_edge_def diamond_cube_def diamond_x_def diamond_y_def
            using d_gt_0
            by (auto simp add: reversepath_def joinpaths_def algebra_simps divide_simps)
          show "pathfinish (coeff_cube_to_path diamond_top_left_edge) = pathstart (coeff_cube_to_path diamond_bot_left_edge)"
            unfolding diamond_x_def reversepath_def  coeff_cube_to_path.simps pathfinish_def diamond_top_left_edge_def
              diamond_bot_left_edge_def pathstart_def
            by auto
        qed (auto intro: chain_subdiv_path.intros, metis diamond_top_left_edge_neq_diamond_top_right_edge)
      qed
    qed  (auto simp add: rot_diamond_cube_boundary_to_subdiv_def; metis diamond_top_left_edge_neq_diamond_top_right_edge)+
    show "\<forall>(k, \<gamma>)\<in>{diamond_top_left_edge, diamond_top_right_edge, diamond_bot_left_edge, diamond_bot_right_edge}. valid_path \<gamma>"
      using diamond_edges_are_valid
      by auto
    show "\<forall>(k, \<gamma>)\<in>{left_edge (typeI_to_typeII diamond_cube), right_edge (typeI_to_typeII diamond_cube)}. point_path \<gamma>"
      "\<forall>(k, \<gamma>)\<in>{left_edge diamond_cube, right_edge diamond_cube}. point_path \<gamma>"
      unfolding point_path_def top_edge_def typeI_to_typeII_def diamond_cube_def diamond_y_def diamond_x_def d_gt_0 prod.swap_def bot_edge_def left_edge_def right_edge_def
      using d_gt_0 eq_divide_eq_numeral1(1) zero_neq_numeral
      by force+
  qed (auto simp add: boundary_chain_def diamond_top_left_edge_def diamond_top_right_edge_def diamond_bot_left_edge_def diamond_bot_right_edge_def)
qed

lemma diamond_swap_eq_img: "cubeImage (diamond_cube) = cubeImage (prod.swap o diamond_cube)"
proof-
  have "(prod.swap \<circ> (\<lambda>(x, y). (diamond_x x, (2 * y - 1) * diamond_y (diamond_x x)))) ` unit_cube =
                (\<lambda>(x, y). (diamond_x x, (2 * y - 1) * diamond_y (diamond_x x))) ` unit_cube"
    unfolding prod.swap_def diamond_x_def diamond_y_def image_def
    apply (auto simp add: case_prod_unfold)
  proof-
    fix a b::real
    assume ass: "0 \<le> a" "a \<le> 1" "0 \<le> b" "b \<le> 1"
      define x where "x = (2 * b - 1) * (1 / 2 - \<bar>(a - 1 / 2) \<bar>) + 1/2"
      have x_sound: "(2 * b - 1) * (1 / 2 - \<bar>(a - 1 / 2)\<bar>) = (x - 1 / 2)"
        unfolding x_def
        by(cases "\<bar>(a - 1 / 2)\<bar> = (a - 1 / 2)"; auto)
      define y where "y = (((a - 1 / 2)) /  (1 / 2 - \<bar>(x - 1 / 2) \<bar>) + 1) / 2"
      have y_sound: "(a - 1 / 2) = (2 * y - 1) * (1 / 2 - \<bar>(x - 1 / 2)\<bar>)"
          unfolding y_def unfolding x_def
          using ass
          by sos
      have x: "x \<in> {0..1}"
        unfolding x_def
        using ass
        by (auto simp add: algebra_simps divide_simps; sos)
      then have y: "y \<in> {0..1}"
        unfolding y_def
        apply (auto simp add: algebra_simps divide_simps)
      proof-
        show "\<bar>x * 2 - 1\<bar> \<le> a * 2"
          using ass x
          unfolding x_def
          apply simp by sos
        then show "a * 4 + 2 * \<bar>x * 2 - 1\<bar> \<le> 4"
          using ass x
          unfolding x_def
          apply simp by sos
      qed
      have rw: "(x - 1 / 2) * d = d * (x - 1 / 2)"
               "(d / 2 - d * x) = d * (1 / 2 - x)" for x
        by (auto simp add: algebra_simps)
      have "(2 * b - 1) * (d / 2 - \<bar>(a - 1 / 2) * d\<bar>) = (x - 1 / 2) * d" "(a - 1 / 2) * d = (2 * y - 1) * (d / 2 - \<bar>(x- 1 / 2) * d\<bar>)"
        unfolding rw abs_mult[where a = d, unfolded abs_of_pos[OF d_gt_0]]
        using x_sound[symmetric] y_sound
        by auto
      then show "\<exists>x\<in>Green.unit_cube. (2 * b - 1) * (d / 2 - \<bar>(a - 1 / 2) * d\<bar>) = (fst x - 1 / 2) * d \<and> (a - 1 / 2) * d = (2 * snd x - 1) * (d / 2 - \<bar>(fst x - 1 / 2) * d\<bar>)"
                "\<exists>x\<in>Green.unit_cube. (a - 1 / 2) * d = (2 * snd x - 1) * (d / 2 - \<bar>(fst x - 1 / 2) * d\<bar>) \<and> (2 * b - 1) * (d / 2 - \<bar>(a - 1 / 2) * d\<bar>) = (fst x - 1 / 2) * d"
        using x y
        by (metis box_real(2) cbox_Pair_iff fst_conv snd_conv)+
    qed
    then show ?thesis
      unfolding cubeImage_def diamond_cube_def typeI_to_typeII_def Let_def diamond_x_def diamond_y_def
      by metis
  qed

lemma GreenThm_diamond:
  assumes "analytically_valid (cubeImage (diamond_cube)) (F\<^bsub>i\<^esub>) j i"
    "analytically_valid (cubeImage (diamond_cube)) (F\<^bsub>j\<^esub>) i j"
  shows "integral (cubeImage (diamond_cube)) (\<lambda>x. \<partial> F\<^bsub>j\<^esub> / \<partial> i |\<^bsub>x\<^esub> - \<partial> F\<^bsub>i\<^esub> / \<partial> j |\<^bsub>x\<^esub> )
                    =  \<^sup>H\<integral>\<^bsub>\<partial>diamond_cube\<^esub> F \<downharpoonright>\<^bsub>{i, j}\<^esub>"
proof -
  have "integral (cubeImage (diamond_cube)) (\<lambda>x. real_of_int(chain_orientation {(1, typeI_to_typeII diamond_cube)}) * \<partial> F\<^bsub>j\<^esub> / \<partial> i |\<^bsub>x\<^esub> - real_of_int(chain_orientation {(1, diamond_cube)}) * \<partial> F\<^bsub>i\<^esub> / \<partial> j |\<^bsub>x\<^esub> )
                    =  \<^sup>H\<integral>\<^bsub>\<partial>diamond_cube\<^esub> F \<downharpoonright>\<^bsub>{i, j}\<^esub>"
  proof(intro green_typeI_typeII_chain.GreenThm_typeI_typeII_divisible_region_2'(1))
    show "green_typeI_typeII_chain (cubeImage diamond_cube) i j F {(1, diamond_cube)} {(1, typeI_to_typeII diamond_cube)}"
      unfolding green_typeI_typeII_chain_def
    proof(intro conjI)
      show *: " i_j_orthonorm i j" using R2.i_j_orthonorm[OF R2_axioms] by auto
      show "green_typeI_chain i j F {(1, diamond_cube)} (cubeImage diamond_cube)"
        unfolding green_typeI_chain_def
      proof(intro conjI * )
        show "green_typeI_chain_axioms i j F {(1, diamond_cube)} (cubeImage diamond_cube)"
          unfolding green_typeI_chain_axioms_def gen_division_def valid_two_chain_def
          by(auto intro: conjI assms(1) diamond_cube_is_type_I diamond_cube_valid_two_cube)
      qed
      show "green_typeI_chain (- j) i F {(1, typeI_to_typeII diamond_cube)} (cubeImage diamond_cube)"
        unfolding green_typeI_chain_def
      proof(intro conjI R2.neg_j_i_orthonorm'[OF R2_axioms])
        show "green_typeI_chain_axioms (- j) i F {(1, typeI_to_typeII diamond_cube)} (cubeImage diamond_cube)"
          unfolding green_typeI_chain_axioms_def gen_division_def valid_two_chain_def
        proof(intro conjI)
          show "\<forall>twoCube\<in>{(1, typeI_to_typeII diamond_cube)}. typeI_twoCube twoCube (- j) i"
            using typeI_to_typeII_works[OF diamond_cube_is_type_I] by auto
          show "UNION {(1, typeI_to_typeII diamond_cube)} (cubeImage \<circ> snd) = cubeImage diamond_cube"
            using diamond_swap_eq_img rev_orient_same_img[OF swap_neg_typeI_is_typeII[OF diamond_cube_is_type_I]]
            unfolding typeI_to_typeII_def by auto
          show "\<forall>(orient, C)\<in>{(1, typeI_to_typeII diamond_cube)}. valid_two_cube C"
            unfolding valid_two_cube_def boundary_edges
            using diamond_bot_edges_neq_diamond_top_edges_typeII''
            using [[simp_trace, simp_trace_depth_limit=3]] by simp
          show "\<forall>(orient, C)\<in>{(1, typeI_to_typeII diamond_cube)}. analytically_valid (cubeImage C) ((F\<^bsub>- j\<^esub>)) i (- j)"
            using anal_valid_j_neg_j[OF assms(2)] diamond_swap_eq_img
            using rev_orient_same_img[OF swap_typeI_is_typeII[OF diamond_cube_is_type_I]]
            unfolding typeI_to_typeII_def
            by auto
        qed auto
      qed
    qed
    show "only_vertical_division' (\<partial>diamond_cube) {(1, diamond_cube)}"
      apply(rule twoChainVertDiv_of_itself'[where C = "{(1, diamond_cube)}", unfolded two_chain_boundary_def , simplified, OF _ diamond_cube_is_type_I])
      using typeI_edges_are_valid_paths[OF diamond_cube_is_type_I]
      by auto
    show "only_vertical_division' (\<partial>diamond_cube) {(1, typeI_to_typeII diamond_cube)}"
      using diamond_cube_is_only_vertical_div_of_rot
      by auto
    show "common_subdiv_exists (\<partial>diamond_cube) (\<partial>diamond_cube)"
      using typeI_edges_are_valid_paths_prod[OF diamond_cube_is_type_I] gen_common_boundary_sudivision_exists_refl_twochain_boundary[OF _ two_cube_boundary_is_boundary]
      by blast
  qed (auto simp add: two_cube_boundary_is_boundary)
  moreover have "chain_orientation {(1::int, R2.typeI_to_typeII diamond_cube)} = (1::int)"
    apply(rule valid_two_chain_orient'[symmetric], auto)
    unfolding valid_two_chain_def valid_two_cube_def boundary_edges
    using diamond_bot_edges_neq_diamond_top_edges_typeII''
    by auto
  moreover have "chain_orientation {(1::int, diamond_cube)} = (1::int)"
    apply(rule valid_two_chain_orient'[symmetric], auto)
    using diamond_cube_valid_two_cube
    unfolding valid_two_chain_def R2.typeI_to_typeII_def by (auto simp add: o_def)
  ultimately show ?thesis by auto
qed

end
end