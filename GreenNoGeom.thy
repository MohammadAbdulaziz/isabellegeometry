theory GreenNoGeom
  imports RectExample "Smooth_Manifolds.Smooth"
begin

lemma C1_diff'_imp_C1_diff'_boundaries:
      assumes "C1_differentiable_on' (C::(real\<times>real) \<Rightarrow> (real\<times>real)) (cbox 0 (1,1))"
              "(k,\<gamma>) \<in> \<partial>C"
      shows "C1_differentiable_on' \<gamma> {0..1}"
proof-
  obtain a b where a_b: "b \<in> Basis" "\<And>x. \<gamma> x = C(a + x *\<^sub>R b)" "a \<bullet> b = 0" "a \<in> (cbox 0 (1,1))"
    using assms(2)
    unfolding cbox_def real_pair_basis boundary_def horizontal_boundary.simps vertical_boundary.simps
    by (auto; metis add.left_neutral mult.right_neutral mult_zero_right order_refl zero_le_one add.left_neutral add.right_neutral mult.right_neutral mult_zero_right order_refl zero_le_one)
  then have rw1: "\<gamma> = (%x. C(a + x *\<^sub>R b))" by auto
  have i: "\<forall>x\<in>(\<lambda>x. x \<bullet> b) ` cbox 0 (1, 1). a + x *\<^sub>R b \<in> cbox 0 (1, 1)"
    using a_b(1) a_b(3) a_b(4)
    unfolding real_pair_basis
    by (auto simp add: cbox_def real_pair_basis inner_left_distrib)    
  have rw2: "{0..1} =  (%p. p \<bullet> b) ` (cbox 0 (1,1))" using a_b(1) unfolding real_pair_basis by (auto simp add: image_def cbox_def; metis add.left_neutral add.right_neutral empty_iff inner_Pair inner_zero_right insert_iff real_inner_1_right real_pair_basis)
  show ?thesis 
    unfolding rw1 rw2
    apply(auto intro!: C1_smooth'_imp_C1_smooth_boundaries i assms a_b)
    using a_b by auto
qed

lemma C1_diff'_imp_C1_diff_boundaries:
      assumes "C1_differentiable_on' (C::(real\<times>real) \<Rightarrow> (real\<times>real)) (cbox 0 (1,1))"
              "(k,\<gamma>) \<in> \<partial>C"
      shows "\<gamma> C1_differentiable_on {0..1}"
  using C1_diff'_imp_C1_diff'_boundaries
  unfolding C1_diff'_path_eq_C1_diff_old
  using assms by auto

lemma line_integral_exists_on_boundary:
  assumes "C1_differentiable_on' (C::(real\<times>real) \<Rightarrow> (real\<times>real)) (cbox 0 (1,1))"
          "continuous_on (cubeImage C) (F\<^bsub>b\<^esub>)"
  shows "one_chain_line_integral_exists F {b} (\<partial>C)"
  unfolding one_chain_line_integral_exists_def
  using field_cont_on_region_cont_on_edges[OF assms(2)] line_integral_exists_smooth_one_base C1_diff'_imp_C1_diff_boundaries[OF assms(1)]
  by blast

(*lemma assumes "compact s" "continuous_on s F" "C1_partially_differentiable_on F s i" "Basis = {i,j}" "i \<noteq> j"
  shows "analytically_valid s F i j"
  unfolding analytically_valid_def   using assms C1_partially_diff_imp_partially_diff   apply auto
proof-
  obtain F_i where F_i: "\<forall>x\<in> s. (has_partial_vector_derivative F i (F_i x) x)" "continuous_on s F_i"
    using assms(3) unfolding C1_partially_differentiable_on_def by auto  
  have cont_F_i: "continuous_on s (\<partial>F/ \<partial>i)"
    apply(rule continuous_on_eq[where f = F_i], rule F_i)
    using F_i partial_vector_derivative_works_2
    by force
  then show "set_integrable lborel s ((\<partial> F / \<partial> i))"
    using borel_integrable_compact assms(1) 
    by auto
  have rw: "(\<lambda>(x, t). (\<partial> F / \<partial> i |\<^bsub>t *\<^sub>R i + x *\<^sub>R j\<^esub>)) = ( (\<partial> F / \<partial> i) o  (\<lambda>pr. (snd pr) *\<^sub>R i + (fst pr) *\<^sub>R j))" by auto
  obtain a b where a_b: "\<And>x. ((\<lambda>p. p \<bullet> i) ` (s \<inter> {p. p \<bullet> j = x})) = cbox a b" sorry
  then have rw2: "(\<lambda>pr. snd pr *\<^sub>R i + fst pr *\<^sub>R j) ` ((\<lambda>p. p \<bullet> j) ` s \<times> {a..b}) = s"
    sorry

  have "(\<lambda>x. integral UNIV (\<lambda>y. indicator s (y *\<^sub>R i + x *\<^sub>R j) *\<^sub>R (\<partial> F / \<partial> i |\<^bsub>y *\<^sub>R i + x *\<^sub>R j\<^esub>))) \<in> borel_measurable lborel"
    apply(intro Bochner_Integration.borel_measurable_integrable)
    apply (simp only: indicator_2d_to_1d[OF assms(4,5)] a_b)
    apply(rule continuous_on_eq[where ?f= "(\<lambda>x. integral (cbox a b) (\<lambda>y. (\<partial> F / \<partial> i |\<^bsub>y *\<^sub>R i + x *\<^sub>R j\<^esub>)))" and ?g = "(\<lambda>x. integral UNIV (\<lambda>y. indicator (cbox a b) y *\<^sub>R (\<partial> F / \<partial> i |\<^bsub>y *\<^sub>R i + x *\<^sub>R j\<^esub>)))"])     
    subgoal apply(rule integral_continuous_on_param, simp only: rw) by(rule continuous_on_compose, auto intro!: continuous_intros cont_F_i simp add: rw2)
    by (auto simp add: integral_indicator_Int[where T = UNIV])

  have "(\<lambda>x. integral UNIV (\<lambda>y. indicator s (y *\<^sub>R i + x *\<^sub>R j) *\<^sub>R (\<partial> F / \<partial> i |\<^bsub>y *\<^sub>R i + x *\<^sub>R j\<^esub>))) \<in> borel_measurable lborel"
    apply(intro Bochner_Integration.borel_measurable_integrable)
    apply (simp only: indicator_2d_to_1d[OF assms(4,5)] a_b)
    apply(rule continuous_on_eq[where ?f= "(\<lambda>x. integral (cbox a b) (\<lambda>y. (\<partial> F / \<partial> i |\<^bsub>y *\<^sub>R i + x *\<^sub>R j\<^esub>)))" and ?g = "(\<lambda>x. integral UNIV (\<lambda>y. indicator (cbox a b) y *\<^sub>R (\<partial> F / \<partial> i |\<^bsub>y *\<^sub>R i + x *\<^sub>R j\<^esub>)))"])     
    subgoal apply(rule integral_continuous_on_param, simp only: rw) by(rule continuous_on_compose, auto intro!: continuous_intros cont_F_i simp add: rw2)
    by (auto simp add: integral_indicator_Int[where T = UNIV])

  have "(\<lambda>x. integral UNIV (\<lambda>y. indicator s (y *\<^sub>R i + x *\<^sub>R j) *\<^sub>R (\<partial> F / \<partial> i |\<^bsub>y *\<^sub>R i + x *\<^sub>R j\<^esub>))) \<in> borel_measurable (restrict_space borel ((\<lambda>p. p \<bullet> j) ` s))"
    apply(intro borel_measurable_continuous_on_restrict)
    apply (simp only: indicator_2d_to_1d[OF assms(4,5)] a_b)
    apply(rule continuous_on_eq[where ?f= "(\<lambda>x. integral (cbox a b) (\<lambda>y. (\<partial> F / \<partial> i |\<^bsub>y *\<^sub>R i + x *\<^sub>R j\<^esub>)))" and ?g = "(\<lambda>x. integral UNIV (\<lambda>y. indicator (cbox a b) y *\<^sub>R (\<partial> F / \<partial> i |\<^bsub>y *\<^sub>R i + x *\<^sub>R j\<^esub>)))"])     
    subgoal apply(rule integral_continuous_on_param, simp only: rw) by(rule continuous_on_compose, auto intro!: continuous_intros cont_F_i simp add: rw2)
    by (auto simp add: integral_indicator_Int[where T = UNIV])
*)

context R2
begin
  definition "jac_det f \<equiv> \<lambda>x. (\<partial> f\<^bsub>i\<^esub> / \<partial> i |\<^bsub>x\<^esub>) * (\<partial> f\<^bsub>j\<^esub> / \<partial> j |\<^bsub>x\<^esub>) - (\<partial> f\<^bsub>i\<^esub> / \<partial> j |\<^bsub>x\<^esub>) * (\<partial> f\<^bsub>j\<^esub> / \<partial> i |\<^bsub>x\<^esub>)"
end

lemma card_4_intro:  "\<lbrakk>a \<noteq> b; a \<noteq> c; a \<noteq> d; b \<noteq> c; b \<noteq> d; c \<noteq> d\<rbrakk> \<Longrightarrow> card {a, b, c, d} = 4"
  by auto

lemma bAllE: "(\<forall>x\<in>s. R x) \<Longrightarrow> ((\<And>x. x \<in> s \<Longrightarrow> R x) \<Longrightarrow> Q) \<Longrightarrow> Q"
  by simp

locale Green_No_Geom = R2 +
  fixes C and F ::"(real \<times> real) \<Rightarrow> (real \<times> real)"
  assumes 
    Field_analytically_valid: "C1_differentiable_on' F (cubeImage C)"

locale Green_No_Geom_Cube = Green_No_Geom + 
  fixes C_inv::"(real \<times> real) \<Rightarrow> (real \<times> real)" and S
  assumes cube_is_C2_diff: 
    "C1_differentiable_on' C UNIV"
    "C1_differentiable_on' ((\<partial> C\<^bsub>j\<^esub> / \<partial> j)) UNIV" 
    "C1_differentiable_on' ((\<partial> C\<^bsub>j\<^esub> / \<partial> i)) UNIV"
    "C1_differentiable_on' ((\<partial> C\<^bsub>i\<^esub> / \<partial> i)) UNIV"
    "C1_differentiable_on' ((\<partial> C\<^bsub>i\<^esub> / \<partial> j)) UNIV" and 
    cube_has_four_sides: "valid_two_cube C" and 
    cube_has_an_inverse:
    "\<And>x. x \<in> (unit_cube - S) \<Longrightarrow> C_inv (C x) = x"
    "continuous_on (C ` (unit_cube - S)) C_inv"
    "negligible S" and
  orientation_preserving_cube:
    "\<And>x. x \<in> unit_cube \<Longrightarrow> jac_det C x \<noteq> 0"
begin

lemma partial_deriv_on_y: "\<partial> f / \<partial> j |\<^bsub>(c, x)\<^esub> = (vector_derivative (\<lambda>x. f(c,x))  (at x))"
  unfolding partial_vector_derivative_def by(auto simp add: algebra_simps i_is_x_axis j_is_y_axis)

lemma partial_deriv_on_x: "\<partial> f / \<partial> i |\<^bsub>(x, c)\<^esub> = (vector_derivative (\<lambda>x. f(x, c))  (at x))"
  unfolding partial_vector_derivative_def by(auto simp add: algebra_simps i_is_x_axis j_is_y_axis)

lemma derivative_Pair_1: "vector_derivative (Pair c) (at x) = (0,1)"
  apply(rule vector_derivative_at)
  unfolding  vector_derivative_def
  by (auto intro!: derivative_eq_intros vector_derivative_at)
  
lemma derivative_Pair_2: "vector_derivative (%x. (x,c)) (at x) = (1,0)"
  apply(rule vector_derivative_at)
  unfolding  vector_derivative_def
  by (auto intro!: derivative_eq_intros vector_derivative_at)

lemma mult_cong: "b = c \<Longrightarrow> a * b = a * c" by auto

lemma lem1: assumes syntax_abbrev[simp] : "\<gamma> = (%y. c *\<^sub>R i + y *\<^sub>R j)"
  shows "(\<integral>\<^bsub>\<gamma>\<^esub> (\<lambda>x. (F (C x) \<bullet> j * \<partial> C\<^bsub>j\<^esub> / \<partial> i |\<^bsub>x\<^esub> ) *\<^sub>R i + (F (C x) \<bullet> j * \<partial> C\<^bsub>j\<^esub> / \<partial> j |\<^bsub>x\<^esub> ) *\<^sub>R j) \<downharpoonright>\<^bsub>{i}\<^esub> ) = 0"
        "line_integral_exists (\<lambda>x. (F (C x) \<bullet> j * \<partial> C\<^bsub>j\<^esub> / \<partial> i |\<^bsub>x\<^esub> ) *\<^sub>R i + (F (C x) \<bullet> j * \<partial> C\<^bsub>j\<^esub> / \<partial> j |\<^bsub>x\<^esub> ) *\<^sub>R j) {i} \<gamma>"
        "(\<integral>\<^bsub>\<gamma>\<^esub> (\<lambda>x. ((F\<^bsub>i\<^esub>) (C x) * (\<partial> C\<^bsub>i\<^esub> / \<partial> i |\<^bsub>x\<^esub>)) *\<^sub>R i + ((F\<^bsub>i\<^esub>) (C x) * (\<partial> C\<^bsub>i\<^esub> / \<partial> j |\<^bsub>x\<^esub>)) *\<^sub>R j) \<downharpoonright>\<^bsub>{i}\<^esub> ) = 0"
        "line_integral_exists (\<lambda>x. ((F\<^bsub>i\<^esub>) (C x) * (\<partial> C\<^bsub>i\<^esub> / \<partial> i |\<^bsub>x\<^esub>)) *\<^sub>R i + ((F\<^bsub>i\<^esub>) (C x) * (\<partial> C\<^bsub>i\<^esub> / \<partial> j |\<^bsub>x\<^esub>)) *\<^sub>R j) {i} \<gamma>"
  by (auto intro: line_integral_on_pair_straight_path simp add: i_is_x_axis j_is_y_axis)

lemma lem1': assumes syntax_abbrev[simp] : "\<gamma> = (%x. x *\<^sub>R i + c *\<^sub>R j)"
  shows "\<integral>\<^bsub>\<gamma>\<^esub> (\<lambda>x. ((F\<^bsub>j\<^esub>) (C x) * (\<partial> C\<^bsub>j\<^esub> / \<partial> i |\<^bsub>x\<^esub>)) *\<^sub>R i + ((F\<^bsub>j\<^esub>) (C x) * (\<partial> C\<^bsub>j\<^esub> / \<partial> j |\<^bsub>x\<^esub>)) *\<^sub>R j) \<downharpoonright>\<^bsub>{j}\<^esub> = 0"
        "line_integral_exists (\<lambda>x. (F (C x) \<bullet> j * \<partial> C\<^bsub>j\<^esub> / \<partial> i |\<^bsub>x\<^esub> ) *\<^sub>R i + (F (C x) \<bullet> j * \<partial> C\<^bsub>j\<^esub> / \<partial> j |\<^bsub>x\<^esub> ) *\<^sub>R j) {j} \<gamma>"
        "\<integral>\<^bsub>\<gamma>\<^esub> (\<lambda>x. ((F\<^bsub>i\<^esub>) (C x) * (\<partial> C\<^bsub>i\<^esub> / \<partial> i |\<^bsub>x\<^esub>)) *\<^sub>R i + ((F\<^bsub>i\<^esub>) (C x) * (\<partial> C\<^bsub>i\<^esub> / \<partial> j |\<^bsub>x\<^esub>)) *\<^sub>R j) \<downharpoonright>\<^bsub>{j}\<^esub> = 0"
        "line_integral_exists (\<lambda>x. ((F\<^bsub>i\<^esub>) (C x) * (\<partial> C\<^bsub>i\<^esub> / \<partial> i |\<^bsub>x\<^esub>)) *\<^sub>R i + ((F\<^bsub>i\<^esub>) (C x) * (\<partial> C\<^bsub>i\<^esub> / \<partial> j |\<^bsub>x\<^esub>)) *\<^sub>R j) {j} \<gamma>" 
  by (auto intro: line_integral_on_pair_straight_path simp add: i_is_x_axis j_is_y_axis)

lemma differentiable_then_continuous_within:
  assumes "\<And>x. x \<in> s \<Longrightarrow> f differentiable (at x within s)"
  shows "continuous_on s f"
proof-
  obtain f' where "(f has_derivative (f' x)) (at x within s)" if "x \<in> s" for x
    using assms
    unfolding differentiable_def
    by metis
  then show ?thesis
    apply(intro has_derivative_continuous_on)
    by auto
qed

lemma differentiable_then_continuous:
  assumes "\<And>x. x \<in> s \<Longrightarrow> f differentiable (at x)"
  shows "continuous_on s f"
  apply(rule differentiable_then_continuous_within)
  using assms differentiable_at_withinI
  by blast

lemma lem2:
  assumes syntax_abbrev[simp] : "\<gamma> = (%y. c *\<^sub>R i + y *\<^sub>R j)" and
  c_ivl: "0 \<le> c" "c \<le> 1" and
  nice_cube: "\<forall>x\<in>unit_cube. C differentiable (at x)"
             "continuous_on unit_cube ((\<partial> C\<^bsub>j\<^esub> / \<partial> i))"
             "continuous_on unit_cube ((\<partial> C\<^bsub>j\<^esub> / \<partial> j))" 
             "continuous_on unit_cube ((\<partial> C\<^bsub>i\<^esub> / \<partial> i))"
             "continuous_on unit_cube ((\<partial> C\<^bsub>i\<^esub> / \<partial> j))" and 
  nice_field: "continuous_on (cubeImage C) F"
  shows "\<integral>\<^bsub>\<gamma>\<^esub> (\<lambda>x. (F (C x) \<bullet> j * \<partial> \<lambda>x. C x \<bullet> j / \<partial> i |\<^bsub>x\<^esub> ) *\<^sub>R i + (F (C x) \<bullet> j * \<partial> \<lambda>x. C x \<bullet> j / \<partial> j |\<^bsub>x\<^esub> ) *\<^sub>R j) \<downharpoonright>\<^bsub>{j}\<^esub> = \<integral>\<^bsub>C o \<gamma>\<^esub> F \<downharpoonright>\<^bsub>{j}\<^esub>"
    "line_integral_exists (\<lambda>x. (F (C x) \<bullet> j * \<partial> \<lambda>x. C x \<bullet> j / \<partial> i |\<^bsub>x\<^esub> ) *\<^sub>R i + (F (C x) \<bullet> j * \<partial> \<lambda>x. C x \<bullet> j / \<partial> j |\<^bsub>x\<^esub> ) *\<^sub>R j) {j} \<gamma>"
    "\<integral>\<^bsub>\<gamma>\<^esub> (\<lambda>x. (F (C x) \<bullet> i * \<partial> \<lambda>x. C x \<bullet> i / \<partial> i |\<^bsub>x\<^esub> ) *\<^sub>R i + (F (C x) \<bullet> i * \<partial> \<lambda>x. C x \<bullet> i / \<partial> j |\<^bsub>x\<^esub> ) *\<^sub>R j) \<downharpoonright>\<^bsub>{j}\<^esub> = \<integral>\<^bsub>C o \<gamma>\<^esub> F \<downharpoonright>\<^bsub>{i}\<^esub>"
    "line_integral_exists (\<lambda>x. (F (C x) \<bullet> i * \<partial> \<lambda>x. C x \<bullet> i / \<partial> i |\<^bsub>x\<^esub> ) *\<^sub>R i + (F (C x) \<bullet> i * \<partial> \<lambda>x. C x \<bullet> i / \<partial> j |\<^bsub>x\<^esub> ) *\<^sub>R j) {j} \<gamma>"
proof-
  {fix x::real
    assume ass: "0 \<le> x" "x \<le> 1"
    then have c_comp_diff: "(C \<circ> Pair c) differentiable (at x)"
      using assms
      by (auto intro: differentiable_chain_at simp add: cbox_def real_pair_basis)
    then have i: "vector_derivative (C \<circ> Pair c) (at x within {0..1}) = (vector_derivative (C \<circ> Pair c) (at x))"
      apply(auto simp add:  vector_derivative_works)
      by (simp add: ass(1) ass(2) vector_derivative_at_within_ivl)
    have c_comp_diff: "(Pair c) differentiable (at x)" by auto
    then have ii: "vector_derivative (Pair c) (at x) = vector_derivative (Pair c) (at x within {0..1})"
      using ass vector_derivative_at_within_ivl
      by (smt c_comp_diff vector_derivative_works)
    have "(C \<circ> Pair c) differentiable (at x)"
      using assms ass
      by (auto intro!: differentiable_chain_at simp add: cbox_def real_pair_basis)
    then have "(vector_derivative (C \<circ> (\<lambda>t. (c, t))) (at x) \<bullet> j) = (\<partial> \<lambda>x. C x \<bullet> j / \<partial> j |\<^bsub>(c,x)\<^esub>) * (vector_derivative (\<lambda>t. (c, t)) (at x) \<bullet> j)"
              "(vector_derivative (C \<circ> (\<lambda>t. (c, t))) (at x) \<bullet> i) = (\<partial> \<lambda>x. C x \<bullet> i / \<partial> j |\<^bsub>(c,x)\<^esub>) * (vector_derivative (\<lambda>t. (c, t)) (at x) \<bullet> j)"
      apply(auto simp add: partial_deriv_on_y j_is_y_axis[symmetric] derivative_component_fun_component derivative_Pair_1)
      by (simp add: j_is_y_axis)+
    then have "(vector_derivative (C \<circ> (\<lambda>t. (c, t))) (at x within {0..1}) \<bullet> j) = 
                   (\<partial> \<lambda>x. C x \<bullet> j / \<partial> j|\<^bsub>(c,x)\<^esub>) * (vector_derivative (\<lambda>t. (c, t)) (at x within {0..1}) \<bullet> j)"
              "(vector_derivative (C \<circ> (\<lambda>t. (c, t))) (at x within {0..1}) \<bullet> i) = 
                   (\<partial> \<lambda>x. C x \<bullet> i / \<partial> j|\<^bsub>(c,x)\<^esub>) * (vector_derivative (\<lambda>t. (c, t)) (at x within {0..1}) \<bullet> j)"
      by (auto simp add: i ii)
  }
  then show "\<integral>\<^bsub>\<gamma>\<^esub> (\<lambda>x. (F (C x) \<bullet> j * \<partial> \<lambda>x. C x \<bullet> j / \<partial> i |\<^bsub>x\<^esub> ) *\<^sub>R i + (F (C x) \<bullet> j * \<partial> \<lambda>x. C x \<bullet> j / \<partial> j |\<^bsub>x\<^esub> ) *\<^sub>R j) \<downharpoonright>\<^bsub>{j}\<^esub> = \<integral>\<^bsub>C o \<gamma>\<^esub> F \<downharpoonright>\<^bsub>{j}\<^esub>"
       "\<integral>\<^bsub>\<gamma>\<^esub> (\<lambda>x. (F (C x) \<bullet> i * \<partial> \<lambda>x. C x \<bullet> i / \<partial> i |\<^bsub>x\<^esub> ) *\<^sub>R i + (F (C x) \<bullet> i * \<partial> \<lambda>x. C x \<bullet> i / \<partial> j |\<^bsub>x\<^esub> ) *\<^sub>R j) \<downharpoonright>\<^bsub>{j}\<^esub> = \<integral>\<^bsub>C o \<gamma>\<^esub> F \<downharpoonright>\<^bsub>{i}\<^esub>"
    unfolding line_integral_def
    by(auto intro: integral_cong simp add: i_is_x_axis j_is_y_axis)
  have "continuous_on unit_cube C"
    apply(intro differentiable_then_continuous assms)
    using nice_cube(1)
    by (simp add: zero_prod_def)
  then have "continuous_on unit_cube (F o C)"
    apply(intro continuous_on_compose)
    using nice_field
    by (auto simp add: cubeImage_def)
  then have "line_integral_exists (\<lambda>x. (F (C x) \<bullet> k * \<partial>(C\<^bsub>k\<^esub>) / \<partial> i |\<^bsub>x\<^esub> ) *\<^sub>R i + (F (C x) \<bullet> k * \<partial>(C\<^bsub>k\<^esub>) / \<partial> j |\<^bsub>x\<^esub> ) *\<^sub>R j) {j} \<gamma>"
    if "continuous_on unit_cube ((\<partial> C\<^bsub>k\<^esub> / \<partial> i))" "continuous_on unit_cube ((\<partial> C\<^bsub>k\<^esub> / \<partial> j))"
    for k
    apply(intro line_integral_exists_smooth_one_base)
    subgoal by auto
    subgoal apply(rule continuous_on_subset[where s = "unit_cube"])
      subgoal apply(intro continuous_intros)
        subgoal by(simp add: o_def)
        subgoal using that by simp
        subgoal by(simp add: o_def)
        subgoal using that by simp
        done
      subgoal using c_ivl by(auto simp add: path_image_def i_is_x_axis j_is_y_axis)
      done
    done
  then show "line_integral_exists (\<lambda>x. (F (C x) \<bullet> j * \<partial> \<lambda>x. C x \<bullet> j / \<partial> i |\<^bsub>x\<^esub> ) *\<^sub>R i + (F (C x) \<bullet> j * \<partial> \<lambda>x. C x \<bullet> j / \<partial> j |\<^bsub>x\<^esub> ) *\<^sub>R j) {j} \<gamma>"
            "line_integral_exists (\<lambda>x. (F (C x) \<bullet> i * \<partial> \<lambda>x. C x \<bullet> i / \<partial> i |\<^bsub>x\<^esub> ) *\<^sub>R i + (F (C x) \<bullet> i * \<partial> \<lambda>x. C x \<bullet> i / \<partial> j |\<^bsub>x\<^esub> ) *\<^sub>R j) {j} \<gamma>"
    using nice_cube
    by auto
qed

lemma lem2':
  assumes syntax_abbrev[simp] : "\<gamma> = (%x. x *\<^sub>R i + c *\<^sub>R j)" and
  c_ivl: "0 \<le> c" "c \<le> 1" and
  nice_cube: "\<forall>x\<in>unit_cube. C differentiable (at x)"
             "continuous_on unit_cube ((\<partial> C\<^bsub>j\<^esub> / \<partial> i))"
             "continuous_on unit_cube ((\<partial> C\<^bsub>j\<^esub> / \<partial> j))" 
             "continuous_on unit_cube ((\<partial> C\<^bsub>i\<^esub> / \<partial> i))"
             "continuous_on unit_cube ((\<partial> C\<^bsub>i\<^esub> / \<partial> j))" and 
  nice_field: "continuous_on (cubeImage C) F"
  shows "\<integral>\<^bsub>\<gamma>\<^esub> (\<lambda>x. ((F\<^bsub>j\<^esub>) (C x) * (\<partial> C\<^bsub>j\<^esub> / \<partial> i |\<^bsub>x\<^esub>)) *\<^sub>R i + ((F\<^bsub>j\<^esub>) (C x) * (\<partial> C\<^bsub>j\<^esub> / \<partial> j |\<^bsub>x\<^esub>)) *\<^sub>R j) \<downharpoonright>\<^bsub>{i}\<^esub> = \<integral>\<^bsub>C \<circ> \<gamma>\<^esub> F \<downharpoonright>\<^bsub>{j}\<^esub>"
    "line_integral_exists (\<lambda>x. ((F\<^bsub>j\<^esub>) (C x) * (\<partial> C\<^bsub>j\<^esub> / \<partial> i |\<^bsub>x\<^esub>)) *\<^sub>R i + ((F\<^bsub>j\<^esub>) (C x) * (\<partial> C\<^bsub>j\<^esub> / \<partial> j |\<^bsub>x\<^esub>)) *\<^sub>R j) {i} \<gamma>" 
    "\<integral>\<^bsub>\<gamma>\<^esub> (\<lambda>x. ((F\<^bsub>i\<^esub>) (C x) * (\<partial> C\<^bsub>i\<^esub> / \<partial> i |\<^bsub>x\<^esub>)) *\<^sub>R i + ((F\<^bsub>i\<^esub>) (C x) * (\<partial> C\<^bsub>i\<^esub> / \<partial> j |\<^bsub>x\<^esub>)) *\<^sub>R j) \<downharpoonright>\<^bsub>{i}\<^esub> = \<integral>\<^bsub>C \<circ> \<gamma>\<^esub> F \<downharpoonright>\<^bsub>{i}\<^esub>"
    "line_integral_exists (\<lambda>x. ((F\<^bsub>i\<^esub>) (C x) * (\<partial> C\<^bsub>i\<^esub> / \<partial> i |\<^bsub>x\<^esub>)) *\<^sub>R i + ((F\<^bsub>i\<^esub>) (C x) * (\<partial> C\<^bsub>i\<^esub> / \<partial> j |\<^bsub>x\<^esub>)) *\<^sub>R j) {i} \<gamma>"
proof-
  {fix x::real
    assume ass: "0 \<le> x" "x \<le> 1"
    then have c_comp_diff: "(C \<circ> (%x. (x, c))) differentiable (at x)"
      using assms
      by (auto intro!: differentiable_chain_at simp add: cbox_def real_pair_basis)
    then have i: "vector_derivative (C \<circ> (%x. (x, c))) (at x within {0..1}) = (vector_derivative (C \<circ> (%x. (x, c))) (at x))"
      apply(auto simp add:  vector_derivative_works)
      by (simp add: ass(1) ass(2) vector_derivative_at_within_ivl)
    have c_comp_diff: "((%x. (x, c))) differentiable (at x)" by auto
    then have ii: "vector_derivative ((%x. (x, c))) (at x) = vector_derivative ((%x. (x, c))) (at x within {0..1})"
      using ass vector_derivative_at_within_ivl
      by (smt c_comp_diff vector_derivative_works)
    have "(C \<circ> (%x. (x, c))) differentiable (at x)"
      using assms ass
      by (auto intro!: differentiable_chain_at simp add: cbox_def real_pair_basis)
    then have "(vector_derivative (C \<circ> (\<lambda>x. (x, c)))\<^bsub>j\<^esub>) (at x) = (\<partial> C\<^bsub>j\<^esub> / \<partial> i |\<^bsub>(x, c)\<^esub>) * (vector_derivative (\<lambda>x. (x, c))\<^bsub>i\<^esub>) (at x)"
      "(vector_derivative (C \<circ> (\<lambda>x. (x, c)))\<^bsub>i\<^esub>) (at x) = (\<partial> C\<^bsub>i\<^esub> / \<partial> i |\<^bsub>(x, c)\<^esub>) * (vector_derivative (\<lambda>x. (x, c))\<^bsub>i\<^esub>) (at x)"
       apply(auto simp add: partial_deriv_on_x i_is_x_axis[symmetric] j_is_y_axis[symmetric] derivative_component_fun_component derivative_Pair_2)
      by (simp add: i_is_x_axis)+
    then have "(vector_derivative (C \<circ> (\<lambda>x. (x, c)))\<^bsub>j\<^esub>) (at x within {0..1}) = (\<partial> C\<^bsub>j\<^esub> / \<partial> i |\<^bsub>(x, c)\<^esub>) * (vector_derivative (\<lambda>x. (x, c))\<^bsub>i\<^esub>) (at x within {0..1})"
              "(vector_derivative (C \<circ> (\<lambda>x. (x, c)))\<^bsub>i\<^esub>) (at x within {0..1}) = (\<partial> C\<^bsub>i\<^esub> / \<partial> i |\<^bsub>(x, c)\<^esub>) * (vector_derivative (\<lambda>x. (x, c))\<^bsub>i\<^esub>) (at x within {0..1})"
      by (auto simp add: i ii)}
  then show "\<integral>\<^bsub>\<gamma>\<^esub> \<lambda>x. ((F\<^bsub>j\<^esub>) (C x) * (\<partial> C\<^bsub>j\<^esub> / \<partial> i |\<^bsub>x\<^esub>)) *\<^sub>R i + ((F\<^bsub>j\<^esub>) (C x) * (\<partial> C\<^bsub>j\<^esub> / \<partial> j |\<^bsub>x\<^esub>)) *\<^sub>R j \<downharpoonright>\<^bsub>{i}\<^esub> = \<integral>\<^bsub>C \<circ> \<gamma>\<^esub> F \<downharpoonright>\<^bsub>{j}\<^esub>"
       "\<integral>\<^bsub>\<gamma>\<^esub> (\<lambda>x. ((F\<^bsub>i\<^esub>) (C x) * (\<partial> C\<^bsub>i\<^esub> / \<partial> i |\<^bsub>x\<^esub>)) *\<^sub>R i + ((F\<^bsub>i\<^esub>) (C x) * (\<partial> C\<^bsub>i\<^esub> / \<partial> j |\<^bsub>x\<^esub>)) *\<^sub>R j) \<downharpoonright>\<^bsub>{i}\<^esub> = \<integral>\<^bsub>C \<circ> \<gamma>\<^esub> F \<downharpoonright>\<^bsub>{i}\<^esub>"
    unfolding line_integral_def
    by(auto intro: integral_cong simp add: i_is_x_axis j_is_y_axis)
  have "continuous_on unit_cube C"
    apply(intro differentiable_then_continuous assms)
    using nice_cube(1)
    by (simp add: zero_prod_def)
  then have "continuous_on unit_cube (F o C)"
    apply(intro continuous_on_compose)
    using nice_field
    by (auto simp add: cubeImage_def)
  then have "line_integral_exists (\<lambda>x. (F (C x) \<bullet> k * \<partial>(C\<^bsub>k\<^esub>) / \<partial> i |\<^bsub>x\<^esub> ) *\<^sub>R i + (F (C x) \<bullet> k * \<partial>(C\<^bsub>k\<^esub>) / \<partial> j |\<^bsub>x\<^esub> ) *\<^sub>R j) {i} \<gamma>"
    if "continuous_on unit_cube ((\<partial> C\<^bsub>k\<^esub> / \<partial> i))" "continuous_on unit_cube ((\<partial> C\<^bsub>k\<^esub> / \<partial> j))"
    for k i
    apply(intro line_integral_exists_smooth_one_base)
    subgoal by auto
    subgoal apply(rule continuous_on_subset[where s = "unit_cube"])
      subgoal apply(intro continuous_intros)
        subgoal by(simp add: o_def)
        subgoal using that by simp
        subgoal by(simp add: o_def)
        subgoal using that by simp
        done
      subgoal using c_ivl by(auto simp add: path_image_def i_is_x_axis j_is_y_axis)
      done
    done
  then show  "line_integral_exists (\<lambda>x. ((F\<^bsub>j\<^esub>) (C x) * (\<partial> C\<^bsub>j\<^esub> / \<partial> i |\<^bsub>x\<^esub>)) *\<^sub>R i + ((F\<^bsub>j\<^esub>) (C x) * (\<partial> C\<^bsub>j\<^esub> / \<partial> j |\<^bsub>x\<^esub>)) *\<^sub>R j) {i} \<gamma>" 
    "line_integral_exists (\<lambda>x. ((F\<^bsub>i\<^esub>) (C x) * (\<partial> C\<^bsub>i\<^esub> / \<partial> i |\<^bsub>x\<^esub>)) *\<^sub>R i + ((F\<^bsub>i\<^esub>) (C x) * (\<partial> C\<^bsub>i\<^esub> / \<partial> j |\<^bsub>x\<^esub>)) *\<^sub>R j) {i} \<gamma>"
    using nice_cube
    by auto
qed

lemma card_le_3: "card {a,b,c} \<le> 3"
  by(auto simp add: eval_nat_numeral insert_absorb card_insert_if)

lemma unit_cube_eq: "unit_cube = cbox 0 (1,1)"
  by (auto simp add: zero_prod_def)

lemma green_no_geom:
  assumes cube_is_nice: 
          "C1_differentiable_on' C unit_cube"
          "\<And>x. ((C\<^bsub>j\<^esub>) has_derivative C_j' x) (at x)"
          (*This and other similar assumptions should be made into 
            \<And>x. ((\<lambda>xa. C_j' xa i) has_derivative C_j'i x) (at x),
            and then has_derivative_transform can be used. Note:
            partial derivative also has to be change be within some
            argument "s". *)
          "\<And>x. ((\<lambda>xa. C_j' xa i) has_derivative C_j'i x) (at x)"
          "\<And>x. ((\<lambda>xa. C_i' xa j) has_derivative C_i'j x) (at x)"
          "\<And>x. ((\<lambda>xa. C_i' xa i) has_derivative C_i'i x) (at x)"
          "\<And>x. ((\<lambda>xa. C_j' xa j) has_derivative C_j'j x) (at x)"
          "valid_two_cube C"
          "C1_differentiable_on' ((\<partial> C\<^bsub>j\<^esub> / \<partial> j)) unit_cube" 
          "C1_differentiable_on' ((\<partial> C\<^bsub>j\<^esub> / \<partial> i)) unit_cube"
          "C1_differentiable_on' ((\<partial> C\<^bsub>i\<^esub> / \<partial> i)) unit_cube"
          "C1_differentiable_on' ((\<partial> C\<^bsub>i\<^esub> / \<partial> j)) unit_cube" and 
          T_cube_is_nice: 
          "C1_differentiable_on' C unit_cube"
          "\<And>x. ((C\<^bsub>i\<^esub>) has_derivative C_i' x) (at x)"
      shows "integral unit_cube (\<lambda>p. (\<partial> F \<circ> C\<^bsub>j\<^esub> / \<partial> i |\<^bsub>p\<^esub>) * (\<partial> C\<^bsub>j\<^esub> / \<partial> j |\<^bsub>p\<^esub>) - (\<partial> F \<circ> C\<^bsub>j\<^esub> / \<partial> j |\<^bsub>p\<^esub>) * (\<partial> C\<^bsub>j\<^esub> / \<partial> i |\<^bsub>p\<^esub>) + 
                                                 (\<partial> F \<circ> C\<^bsub>i\<^esub> / \<partial> i |\<^bsub>p\<^esub>) * (\<partial> C\<^bsub>i\<^esub> / \<partial> j |\<^bsub>p\<^esub>) - (\<partial> F \<circ> C\<^bsub>i\<^esub> / \<partial> j |\<^bsub>p\<^esub>) * (\<partial> C\<^bsub>i\<^esub> / \<partial> i |\<^bsub>p\<^esub>))
                   = \<^sup>H\<integral>\<^bsub>\<partial>C\<^esub> F \<downharpoonright>\<^bsub>{i,j}\<^esub>"
proof-  
  interpret rect i j 1 1 unfolding rect_def R2_def rect_axioms_def
    using i_is_x_axis j_is_y_axis by auto
  let ?FoC = "%i j. \<partial> (F o C)\<^bsub>i\<^esub> / \<partial> j"
  let ?C = "%i j. (\<partial> C\<^bsub>i\<^esub> / \<partial> j)"
  obtain F' where F_is_diff: "\<forall>x\<in>(cubeImage C). (F has_derivative F' x) (at x)"
    using Field_analytically_valid(1)
    by (auto simp: C1_differentiable_on'_def)
  have F': "\<And>x. x\<in>unit_cube \<Longrightarrow> (F has_derivative F' (C x)) (at (C x))" using F_is_diff unfolding cubeImage_def image_def
    by (simp add: F_is_diff cubeImage_def)
  obtain C' where C': "(\<And>x. x\<in>unit_cube \<Longrightarrow> (C has_derivative (C' x)) (at x))" "continuous_on unit_cube C'"
    using cube_is_nice (1) unfolding C1_differentiable_on'_def by auto
  then have C'_within: "(\<And>x. x\<in>unit_cube \<Longrightarrow> (C has_derivative (C' x)) (at x within unit_cube))"
    using has_derivative_at_withinI by blast
  have FoC': "(((F o C)) has_derivative ((F' o C) x o C' x)) (at x)" if "x \<in> unit_cube" for x
    using diff_chain_at[OF C'(1) F' ] that
    by auto
  have has_derivative_to_has_partial_deriv'': 
    "b \<bullet> b = 1 \<Longrightarrow> ((F::'a::euclidean_space \<Rightarrow> 'b::euclidean_space) has_derivative (F')) (at a) \<Longrightarrow> (has_partial_vector_derivative F b (F' b) a)" for a b F F'
    using has_derivative_to_has_partial_deriv'' .
  have "\<exists>F_C_j'.  \<forall>x\<in>unit_cube. (((F o C)\<^bsub>j\<^esub>) has_derivative F_C_j' x) (at x)"
    using has_derivative_inner_left FoC'
    by (fastforce simp: zero_prod_def[symmetric])
  moreover have "\<exists>C_j_j'. \<forall>x\<in>unit_cube. ((?C j j) has_derivative (C_j_j' x)) (at x)"
    using cube_is_nice(8)
    by (auto simp add: C1_differentiable_on'_def)
  ultimately have 
    iv: "\<And>x. x\<in> unit_cube \<Longrightarrow> partially_vector_differentiable ((F o C)\<^bsub>j\<^esub>) i x"
        "\<And>x. x\<in>unit_cube \<Longrightarrow> partially_vector_differentiable (?C j j) i x" 
    unfolding partially_vector_differentiable_def
    using has_derivative_to_has_partial_deriv i_is_x_axis
     apply auto
    by (smt R2.i_j_orthonorm R2_def cbox_Pair_iff has_derivative_to_has_partial_deriv'' i_j_orthonorm.i_inner_unit mem_box_real(2))+
  have "\<exists>F_C_i'.  \<forall>x\<in> unit_cube. (((F o C)\<^bsub>i\<^esub>) has_derivative F_C_i' x) (at x)"
       "\<exists>C_i_i'. \<forall>x\<in> unit_cube. ((?C i i) has_derivative (C_i_i' x)) (at x)"
    apply clarsimp
    subgoal using has_derivative_inner_left FoC' unfolding zero_prod_def[symmetric] by fastforce
    subgoal using cube_is_nice(10) unfolding C1_differentiable_on'_def by fastforce
    done
  then have T_iv: "\<And>x. x\<in>unit_cube \<Longrightarrow> partially_vector_differentiable (F o C\<^bsub>i\<^esub>) j x"
       "\<And>x. x\<in>unit_cube \<Longrightarrow> partially_vector_differentiable (?C i i) j x"
    unfolding partially_vector_differentiable_def
    using has_derivative_to_has_partial_deriv j_is_y_axis i_is_x_axis
    apply (auto simp add: unit_cube_eq)
    apply (smt has_derivative_to_has_partial_deriv'' norm_Pair1  norm_eq_1 norm_one)
    by (smt has_derivative_to_has_partial_deriv'' norm_Pair1 norm_eq_1 norm_one)
  have "\<exists>F_C_j'.  \<forall>x\<in>unit_cube. (((F o C)\<^bsub>j\<^esub>) has_derivative F_C_j' x) (at x)" "\<exists>C_j_i'. \<forall>x\<in>unit_cube. ((?C j i) has_derivative (C_j_i' x)) (at x)"
    apply clarsimp
    subgoal using has_derivative_inner_left FoC'
      unfolding unit_cube_eq
      by fastforce
    subgoal using cube_is_nice(9) unfolding C1_differentiable_on'_def by fastforce
    done
  then have v: "\<And>x. x\<in>unit_cube \<Longrightarrow> partially_vector_differentiable (F o C\<^bsub>j\<^esub>) j x" "\<And>x. x\<in>(cbox 0 (1,1)) \<Longrightarrow> partially_vector_differentiable (?C j i) j x"
    unfolding partially_vector_differentiable_def
    using has_derivative_to_has_partial_deriv j_is_y_axis i_is_x_axis
    apply (auto simp add: unit_cube_eq)
    apply (smt has_derivative_to_has_partial_deriv'' norm_Pair1  norm_eq_1 norm_one)
    by (smt has_derivative_to_has_partial_deriv'' norm_Pair1 norm_eq_1 norm_one)
  have "\<exists>F_C_i'.  \<forall>x\<in>unit_cube. (((F o C)\<^bsub>i\<^esub>) has_derivative F_C_i' x) (at x)" "\<exists>C_i_j'. \<forall>x\<in>unit_cube. ((?C i j) has_derivative (C_i_j' x)) (at x)"
    apply clarsimp
    subgoal using has_derivative_inner_left FoC'
      unfolding unit_cube_eq
      by fastforce
    subgoal using cube_is_nice(11) unfolding C1_differentiable_on'_def by fastforce
    done
  then have T_v:  "\<And>x. x\<in>unit_cube \<Longrightarrow> partially_vector_differentiable (F o C\<^bsub>i\<^esub>) i x"
                  "\<And>x. x\<in>unit_cube \<Longrightarrow> partially_vector_differentiable (?C i j) i x"
    unfolding partially_vector_differentiable_def
    using has_derivative_to_has_partial_deriv j_is_y_axis i_is_x_axis
    apply (auto simp add: unit_cube_eq)
    apply (smt has_derivative_to_has_partial_deriv'' norm_Pair2  norm_eq_1 norm_one)
    by (smt has_derivative_to_has_partial_deriv'' norm_Pair2 norm_eq_1 norm_one)
  have "C1_differentiable_on' (F o C) unit_cube"
    using Field_analytically_valid(1) cube_is_nice(1) C1_differentiable'_compose[OF ]
    unfolding cubeImage_def
    by simp
  then have CoF_component_diff: "C1_differentiable_on' ((F o C)\<^bsub>j\<^esub>) unit_cube"
            "C1_differentiable_on' ((F o C)\<^bsub>i\<^esub>) unit_cube"
    using C1_diff'_imp_components by blast+
  then have FoC_partial_diff_all_directions: "C1_partially_differentiable_on (\<lambda>x. (F\<^bsub>j\<^esub>) (C x)) unit_cube j"
            "C1_partially_differentiable_on (\<lambda>x. (F\<^bsub>i\<^esub>) (C x)) unit_cube j"
            "C1_partially_differentiable_on (\<lambda>x. (F\<^bsub>i\<^esub>) (C x)) unit_cube i"
            "C1_partially_differentiable_on (\<lambda>x. (F\<^bsub>j\<^esub>) (C x)) unit_cube i"
    unfolding o_def
    by (simp add: C1_smooth_gen_C1_smooth_old' j_is_y_axis i_is_x_axis)+
  have F_cont: "continuous_on (cubeImage C) (F\<^bsub>i\<^esub>)" "continuous_on (cubeImage C) (F\<^bsub>j\<^esub>)" "continuous_on (cubeImage C) F"
    using Field_analytically_valid(1) C_diff'_imp_cont C1_diff'_imp_components by (blast)+
  define proof_field1 where "proof_field1 = (\<lambda>p. ((F o C\<^bsub>j\<^esub>) p * (?C j i p)) *\<^sub>R i + ((F o C\<^bsub>j\<^esub>) p * (?C j j p)) *\<^sub>R j)"
  have cbox_Times: "unit_cube = (cbox 0 1) \<times> (cbox 0 1)"
    by (auto simp add: cbox_def real_pair_basis)
  have "C1_partially_differentiable_on (\<lambda>x. (F o C\<^bsub>j\<^esub>) x * (?C j i x)) unit_cube j"
       "C1_partially_differentiable_on (\<lambda>x. (F o C\<^bsub>j\<^esub>) x * (?C j j x)) unit_cube i" 
    apply(auto intro!: C1_partially_differentiable_on_mult FoC_partial_diff_all_directions)
    subgoal by (force intro: C1_smooth_gen_C1_smooth_old' cube_is_nice(9))
    subgoal using C_diff'_imp_cont CoF_component_diff by force
    subgoal using cube_is_nice(9) C_diff'_imp_cont by force
    subgoal using C1_smooth_gen_C1_smooth_old' cube_is_nice(8) by force
    subgoal using C_diff'_imp_cont CoF_component_diff by force
    subgoal using cube_is_nice(8) C_diff'_imp_cont by force
    done
  then have "analytically_valid unit_cube (\<lambda>x. (F o C\<^bsub>j\<^esub>) x * (?C j i x)) j i"
            "analytically_valid unit_cube (\<lambda>x. (F o C\<^bsub>j\<^esub>) x * (?C j j x)) i j"
    using CoF_component_diff cbox_Times cube_is_nice(9) cbox_Times cube_is_nice(8)
    by (auto intro!: C1_imp_analytically_valid_ji C1_imp_analytically_valid_ij C_diff'_imp_cont C1_differentiable'_mult simp add: cbox_Times)
  then have analytically_valid: "analytically_valid unit_cube (proof_field1\<^bsub>i\<^esub>) j i"
                                "analytically_valid unit_cube (proof_field1\<^bsub>j\<^esub>) i j"
    unfolding proof_field1_def
    by(auto simp add: algebra_simps i_is_x_axis j_is_y_axis)
  define proof_field2 where "proof_field2 = (\<lambda>p. ((F o C\<^bsub>i\<^esub>) p * (?C i i p)) *\<^sub>R i + ((F o C\<^bsub>i\<^esub>) p * (?C i j p)) *\<^sub>R j)"
  have cbox_Times: "unit_cube = (cbox 0 1) \<times> (cbox 0 1)" apply (auto simp add: cbox_def) unfolding real_pair_basis by auto
  have "C1_partially_differentiable_on (\<lambda>x. (F o C\<^bsub>i\<^esub>) x * (?C i j x)) unit_cube i"
       "C1_partially_differentiable_on (\<lambda>x. (F o C\<^bsub>i\<^esub>) x * (?C i i x)) unit_cube j" 
    apply(auto intro!: C1_partially_differentiable_on_mult FoC_partial_diff_all_directions)
    subgoal using C1_smooth_gen_C1_smooth_old' i_is_x_axis cube_is_nice(11) by force
    subgoal using C_diff'_imp_cont CoF_component_diff by force
    subgoal using cube_is_nice(11) C_diff'_imp_cont by force
    subgoal using C1_smooth_gen_C1_smooth_old' j_is_y_axis cube_is_nice(10) by force
    subgoal using C_diff'_imp_cont CoF_component_diff by force
    subgoal using cube_is_nice(10) C_diff'_imp_cont by force
    done
  then have "analytically_valid unit_cube (\<lambda>x. (F o C\<^bsub>i\<^esub>) x * (?C i i x)) j i"
            "analytically_valid unit_cube (\<lambda>x. (F o C\<^bsub>i\<^esub>) x * (?C i j x)) i j"
    using CoF_component_diff cbox_Times cube_is_nice(10) cbox_Times cube_is_nice(11)
    by (auto intro!: C1_imp_analytically_valid_ji C1_imp_analytically_valid_ij C_diff'_imp_cont C1_differentiable'_mult simp add: cbox_Times)
  then have T_analytically_valid: "analytically_valid unit_cube (proof_field2\<^bsub>i\<^esub>) j i"
            "analytically_valid unit_cube (proof_field2\<^bsub>j\<^esub>) i j"
    unfolding proof_field2_def
    by(auto simp add: algebra_simps i_is_x_axis j_is_y_axis)
  have more_anal_conds: "(\<lambda>x. (?FoC i i x ) * (?C i j x) - (?FoC i j x ) * (?C i i x)) integrable_on unit_cube"
       "(\<lambda>x. (?FoC j i x ) * (?C j j x) - (?FoC j j x ) * (?C j i x)) integrable_on unit_cube"
    by(auto intro!: set_borel_integral_eq_integral[unfolded set_integrable_def]
                    borel_integrable_compact continuous_intros
                    C1_partially_differentiable_on_imp_cont_part_deriv 
                    FoC_partial_diff_all_directions C1_smooth_gen_C1_smooth_old'
                    C1_diff'_imp_components cube_is_nice(1))
  have C_diff: "\<forall>x\<in> unit_cube. C differentiable at x" using C_diff'_imp_differntiable(1)[OF cube_is_nice(1)] by auto
  have diff_auto: "\<And>a b c d. a = c \<and> b = d \<Longrightarrow> a - b = c - d" by auto
  have partial_derivs_symm: "(\<partial> (?C i j) / \<partial> i |\<^bsub>x\<^esub>) = (\<partial> (?C i i) / \<partial> j |\<^bsub>x\<^esub>)  \<and> (\<partial> (?C j i) / \<partial> j |\<^bsub>x\<^esub>) = (\<partial> (?C j j) / \<partial> i |\<^bsub>x\<^esub>)" for x
    apply (auto intro!: Youngs_thm cube_is_nice(2-6) T_cube_is_nice(2))
    by (auto simp add: i_neq_j i_is_x_axis j_is_y_axis)
  have lem2_result:
    "\<integral>\<^bsub>\<gamma>\<^esub> \<lambda>x. ((F\<^bsub>j\<^esub>) (C x) * (\<partial> C\<^bsub>j\<^esub> / \<partial> i |\<^bsub>x\<^esub>)) *\<^sub>R i + ((F\<^bsub>j\<^esub>) (C x) * (\<partial> C\<^bsub>j\<^esub> / \<partial> j |\<^bsub>x\<^esub>)) *\<^sub>R j \<downharpoonright>\<^bsub>{j}\<^esub> = \<integral>\<^bsub>C \<circ> \<gamma>\<^esub> F \<downharpoonright>\<^bsub>{j}\<^esub>"
    "line_integral_exists (\<lambda>x. ((F\<^bsub>j\<^esub>) (C x) * (\<partial> C\<^bsub>j\<^esub> / \<partial> i |\<^bsub>x\<^esub>)) *\<^sub>R i + ((F\<^bsub>j\<^esub>) (C x) * (\<partial> C\<^bsub>j\<^esub> / \<partial> j |\<^bsub>x\<^esub>)) *\<^sub>R j) {j} \<gamma>"
    "\<integral>\<^bsub>\<gamma>\<^esub> \<lambda>x. ((F\<^bsub>i\<^esub>) (C x) * (\<partial> C\<^bsub>i\<^esub> / \<partial> i |\<^bsub>x\<^esub>)) *\<^sub>R i + ((F\<^bsub>i\<^esub>) (C x) * (\<partial> C\<^bsub>i\<^esub> / \<partial> j |\<^bsub>x\<^esub>)) *\<^sub>R j \<downharpoonright>\<^bsub>{j}\<^esub> = \<integral>\<^bsub>C \<circ> \<gamma>\<^esub> F \<downharpoonright>\<^bsub>{i}\<^esub>"
    "line_integral_exists (\<lambda>x. ((F\<^bsub>i\<^esub>) (C x) * (\<partial> C\<^bsub>i\<^esub> / \<partial> i |\<^bsub>x\<^esub>)) *\<^sub>R i + ((F\<^bsub>i\<^esub>) (C x) * (\<partial> C\<^bsub>i\<^esub> / \<partial> j |\<^bsub>x\<^esub>)) *\<^sub>R j) {j} \<gamma>"
    if "\<gamma> = (\<lambda>y. c *\<^sub>R i + y *\<^sub>R j)" "0 \<le> c" "c \<le> 1" for \<gamma> c
    using lem2[OF that C_diff C_diff'_imp_cont[OF cube_is_nice(9)] C_diff'_imp_cont[OF cube_is_nice(8)]
        C_diff'_imp_cont[OF cube_is_nice(10)] C_diff'_imp_cont[OF cube_is_nice(11)] C_diff'_imp_cont[OF Field_analytically_valid]]
    by auto
  have lem2'_result:
    "\<integral>\<^bsub>\<gamma>\<^esub> \<lambda>x. ((F\<^bsub>j\<^esub>) (C x) * (\<partial> C\<^bsub>j\<^esub> / \<partial> i |\<^bsub>x\<^esub>)) *\<^sub>R i + ((F\<^bsub>j\<^esub>) (C x) * (\<partial> C\<^bsub>j\<^esub> / \<partial> j |\<^bsub>x\<^esub>)) *\<^sub>R j \<downharpoonright>\<^bsub>{i}\<^esub> = \<integral>\<^bsub>C \<circ> \<gamma>\<^esub> F \<downharpoonright>\<^bsub>{j}\<^esub>"
    "line_integral_exists (\<lambda>x. ((F\<^bsub>j\<^esub>) (C x) * (\<partial> C\<^bsub>j\<^esub> / \<partial> i |\<^bsub>x\<^esub>)) *\<^sub>R i + ((F\<^bsub>j\<^esub>) (C x) * (\<partial> C\<^bsub>j\<^esub> / \<partial> j |\<^bsub>x\<^esub>)) *\<^sub>R j) {i} \<gamma>"
    "\<integral>\<^bsub>\<gamma>\<^esub> \<lambda>x. ((F\<^bsub>i\<^esub>) (C x) * (\<partial> C\<^bsub>i\<^esub> / \<partial> i |\<^bsub>x\<^esub>)) *\<^sub>R i + ((F\<^bsub>i\<^esub>) (C x) * (\<partial> C\<^bsub>i\<^esub> / \<partial> j |\<^bsub>x\<^esub>)) *\<^sub>R j \<downharpoonright>\<^bsub>{i}\<^esub> = \<integral>\<^bsub>C \<circ> \<gamma>\<^esub> F \<downharpoonright>\<^bsub>{i}\<^esub>"
    "line_integral_exists (\<lambda>x. ((F\<^bsub>i\<^esub>) (C x) * (\<partial> C\<^bsub>i\<^esub> / \<partial> i |\<^bsub>x\<^esub>)) *\<^sub>R i + ((F\<^bsub>i\<^esub>) (C x) * (\<partial> C\<^bsub>i\<^esub> / \<partial> j |\<^bsub>x\<^esub>)) *\<^sub>R j) {i} \<gamma>"
    if "\<gamma> = (\<lambda>y. y *\<^sub>R i + c *\<^sub>R j)" "0 \<le> c" "c \<le> 1" for \<gamma> c
    by(intro lem2'[OF that C_diff C_diff'_imp_cont[OF cube_is_nice(9)] C_diff'_imp_cont[OF cube_is_nice(8)]
                   C_diff'_imp_cont[OF cube_is_nice(10)] C_diff'_imp_cont[OF cube_is_nice(11)]
                   C_diff'_imp_cont[OF Field_analytically_valid]])+
  have "integral unit_cube (\<lambda>x. (?FoC i i x ) * (?C i j x) - (?FoC i j x ) * (?C i i x)) = \<^sup>H\<integral>\<^bsub>\<partial>C\<^esub> F \<downharpoonright>\<^bsub>{i}\<^esub>"
  proof-
    have "integral unit_cube (\<lambda>x. (?FoC i i x ) * (?C i j x) - (?FoC i j x ) * (?C i i x)) =
           integral unit_cube (\<lambda>x. (\<lambda>x. (?FoC i i x ) * (?C i j x) + (F \<circ> C\<^bsub>i\<^esub>) x * (\<partial> (?C i j) / \<partial> i |\<^bsub>x\<^esub>)) x
                                      - (\<lambda>x. (F o C\<^bsub>i\<^esub>) x * (\<partial> (?C i i) / \<partial> j |\<^bsub>x\<^esub>) + (?FoC i j x ) * (?C i i x)) x)"
      apply(rule integral_cong)
      using partial_derivs_symm
      by auto
    also have "... = integral unit_cube (\<lambda>x. \<partial> \<lambda>x. (F o C\<^bsub>i\<^esub>) x * (?C i j x) / \<partial> i |\<^bsub>x\<^esub> 
                                                 - \<partial> \<lambda>x. (F o C\<^bsub>i\<^esub>) x * (?C i i x) / \<partial> j |\<^bsub>x\<^esub> )"
      apply(intro diff_auto integral_cong)
      using partial_vector_derivative_mult_at[OF T_iv(1-2)] partial_vector_derivative_mult_at[OF T_v(1-2)] by auto
    also have "... = integral unit_cube (\<lambda>x. \<partial> proof_field2\<^bsub>j\<^esub> / \<partial> i |\<^bsub>x\<^esub>
                                                   - \<partial> proof_field2\<^bsub>i\<^esub> / \<partial> j |\<^bsub>x\<^esub>)"
      unfolding proof_field2_def
      by(auto intro!: diff_auto integral_cong partial_vector_derivative_cong simp add: algebra_simps i_is_x_axis j_is_y_axis)    
    also have "... = \<^sup>H\<integral>\<^bsub>\<partial>rect_cube\<^esub> proof_field2"
      unfolding real_pair_basis i_is_x_axis[symmetric] j_is_y_axis[symmetric] unit_cube_eq
      apply(intro green_for_rect.GreenThm_rect[OF _ T_analytically_valid[unfolded unit_cube_eq]])
      by(simp add: green_for_rect_def green_for_rect_axioms_def rect_def R2_def rect_axioms_def i_is_x_axis j_is_y_axis)
    also have "... = \<^sup>H\<integral>\<^bsub>\<partial>C\<^esub> F \<downharpoonright>\<^bsub>{i}\<^esub>"
      unfolding one_chain_line_integral_def
      apply(rule sum_inj_on[where F = "\<lambda>(k,\<gamma>). (k, C o \<gamma>)"]; clarsimp?)
    proof goal_cases
      show "inj_on (\<lambda>(k, \<gamma>). (k, C \<circ> \<gamma>)) (\<partial>rect_cube)"
        using cube_is_nice(7)
        unfolding valid_two_cube_def boundary_def rect_cube_def
        apply(auto simp add: o_def i_is_x_axis j_is_y_axis)
        subgoal apply (auto simp add: eval_nat_numeral  )
          by (smt card_Suc_eq insert_absorb insert_not_empty nat.inject)
        subgoal apply (auto simp add: eval_nat_numeral)
          by (smt Suc_inject Zero_not_Suc card.insert card_Suc_eq finite.intros(1) insert_absorb insert_absorb2 insert_commute insert_not_empty)
        done
    next
      case ass: (2 k \<gamma>)
      {fix k \<gamma>
        assume "(k, \<gamma>) \<in> \<partial>rect_cube"
        then have "(\<integral>\<^bsub>\<gamma>\<^esub> proof_field2) = (\<integral>\<^bsub>C \<circ> \<gamma>\<^esub> F \<downharpoonright>\<^bsub>{i}\<^esub>)"              
          unfolding real_pair_basis i_is_x_axis[symmetric] j_is_y_axis[symmetric] boundary_edges 
          apply safe
        proof goal_cases
          case ass: 1
          have i: "snd (top_edge rect_cube) = (\<lambda>x. x *\<^sub>R i + 1 *\<^sub>R j)" by (auto simp add: top_edge_def rect_cube_def)
          have "(\<integral>\<^bsub>snd (top_edge rect_cube)\<^esub> proof_field2 \<downharpoonright>\<^bsub>{i,j}\<^esub>) = (\<Sum>b\<in>{i,j}. (\<integral>\<^bsub>snd (top_edge rect_cube)\<^esub> proof_field2 \<downharpoonright>\<^bsub>{b}\<^esub>))"
            unfolding proof_field2_def 
            apply(rule line_integral_summation(1))
            using lem1'(4)[OF i] lem2'_result[OF i]
            by auto
          moreover have "... = \<integral>\<^bsub>C \<circ> snd (top_edge rect_cube)\<^esub> F \<downharpoonright>\<^bsub>{i}\<^esub>" using i_neq_j
            lem1'(3)[OF i] lem2'_result[OF i]
            unfolding proof_field2_def
            by auto
          ultimately show ?case using ass
            by (metis (no_types, lifting) snd_conv)
        next
          case ass: 2
          have i: "snd (bot_edge rect_cube) = (\<lambda>x. x *\<^sub>R i + 0 *\<^sub>R j)" by (auto simp add: bot_edge_def rect_cube_def)
          have "(\<integral>\<^bsub>snd (bot_edge rect_cube)\<^esub> proof_field2 \<downharpoonright>\<^bsub>{i,j}\<^esub>) = (\<Sum>b\<in>{i,j}. (\<integral>\<^bsub>snd (bot_edge rect_cube)\<^esub> proof_field2 \<downharpoonright>\<^bsub>{b}\<^esub>))"
            unfolding proof_field2_def
            apply(rule line_integral_summation(1), auto)
            using lem1'(4)[OF i] lem2'_result[OF i]
            by auto
          moreover have "... = \<integral>\<^bsub>C \<circ> snd (bot_edge rect_cube)\<^esub> F \<downharpoonright>\<^bsub>{i}\<^esub>" using i_neq_j lem1'(3)[OF i] lem2'_result[OF i]
            unfolding proof_field2_def
            by auto
          ultimately show ?case using ass
            by (metis (no_types, lifting) snd_conv)
        next
          case ass: 3
          have i: "snd (right_edge rect_cube) = (\<lambda>y. 1 *\<^sub>R i + y *\<^sub>R j)" by (auto simp add: right_edge_def rect_cube_def)
          have "(\<integral>\<^bsub>snd (right_edge rect_cube)\<^esub> proof_field2 \<downharpoonright>\<^bsub>{i,j}\<^esub>) = (\<Sum>b\<in>{i,j}. (\<integral>\<^bsub>snd (right_edge rect_cube)\<^esub> proof_field2 \<downharpoonright>\<^bsub>{b}\<^esub>))"
            unfolding proof_field2_def
            apply(rule line_integral_summation(1))
            using lem1(4)[OF i] lem2_result(4)[OF i]
            by auto
          moreover have "... = \<integral>\<^bsub>C \<circ> snd (right_edge rect_cube)\<^esub> F \<downharpoonright>\<^bsub>{i}\<^esub>"
            using i_neq_j lem1(3)[OF i] lem2_result[OF i] 
            unfolding proof_field2_def
            by auto
          ultimately show ?case using ass
            by (metis (no_types, lifting) snd_conv)
        next
          case ass: 4
          have i: "snd (left_edge rect_cube) = (\<lambda>y. 0 *\<^sub>R i + y *\<^sub>R j)" by (auto simp add: left_edge_def rect_cube_def)
          have "(\<integral>\<^bsub>snd (left_edge rect_cube)\<^esub> proof_field2 \<downharpoonright>\<^bsub>{i,j}\<^esub>) = (\<Sum>b\<in>{i,j}. (\<integral>\<^bsub>snd (left_edge rect_cube)\<^esub> proof_field2 \<downharpoonright>\<^bsub>{b}\<^esub>))"
            unfolding proof_field2_def
            apply(rule line_integral_summation(1))
            using lem1(4)[OF i]
                  lem2_result[OF i]
            by auto
          moreover have "... = \<integral>\<^bsub>C \<circ> snd (left_edge rect_cube)\<^esub> F \<downharpoonright>\<^bsub>{i}\<^esub>"
            unfolding proof_field2_def
            using i_neq_j lem1(3)[OF i] lem2_result[OF i]
            by auto
          ultimately show ?case using ass
            by (metis (no_types, lifting) snd_conv)
        qed
      }
      then show ?case using ass by auto
    next
      show "(\<lambda>(k, \<gamma>). (k, C \<circ> \<gamma>)) ` \<partial>rect_cube = \<partial>C"
        unfolding rect_cube_def
        unfolding boundary_def horizontal_boundary.simps vertical_boundary.simps i_is_x_axis j_is_y_axis
        by auto
    qed
    finally show ?thesis by auto
  qed
  moreover have "integral unit_cube (\<lambda>x. (?FoC j i x ) * (?C j j x) - (?FoC j j x ) * (?C j i x)) = \<^sup>H\<integral>\<^bsub>\<partial>C\<^esub> F \<downharpoonright>\<^bsub>{j}\<^esub>"
  proof-
    have "integral unit_cube (\<lambda>x. (?FoC j i x ) * (?C j j x) - (?FoC j j x ) * (?C j i x)) =
         integral unit_cube (\<lambda>x. (\<lambda>x. (?FoC j i x ) * (?C j j x) + ((F o C) x \<bullet> j) * (\<partial> (?C j j) / \<partial> i |\<^bsub>x\<^esub>)) x
                                      - (\<lambda>x. ((F o C) x \<bullet> j) * (\<partial> (?C j i) / \<partial> j |\<^bsub>x\<^esub>) + (?FoC j j x ) * (?C j i x)) x)"
      apply(rule integral_cong)
      using partial_derivs_symm
      by auto
    also have "... = integral unit_cube (\<lambda>x. \<partial> \<lambda>x. (F o C) x \<bullet> j * (?C j j x) / \<partial> i |\<^bsub>x\<^esub> 
                                                   - \<partial> \<lambda>x. (F o C) x \<bullet> j * (?C j i x) / \<partial> j |\<^bsub>x\<^esub> )"
      apply(auto intro!: diff_auto integral_cong)
      using partial_vector_derivative_mult_at[OF iv]
            partial_vector_derivative_mult_at[OF v]
      by (auto simp add: unit_cube_eq[symmetric])
    also have "... = integral unit_cube (\<lambda>x. \<partial> proof_field1\<^bsub>j\<^esub> / \<partial> i |\<^bsub>x\<^esub>
                                                   - \<partial> proof_field1\<^bsub>i\<^esub> / \<partial> j |\<^bsub>x\<^esub>)"
      unfolding proof_field1_def
      by(auto intro!: diff_auto integral_cong partial_vector_derivative_cong simp add: algebra_simps i_is_x_axis j_is_y_axis)    
    also have "... = \<^sup>H\<integral>\<^bsub>\<partial>rect_cube\<^esub> proof_field1"
      unfolding real_pair_basis i_is_x_axis[symmetric] j_is_y_axis[symmetric] unit_cube_eq
      apply(intro green_for_rect.GreenThm_rect[OF _ analytically_valid[unfolded unit_cube_eq]])
      by(simp add: green_for_rect_def green_for_rect_axioms_def rect_def R2_def rect_axioms_def i_is_x_axis j_is_y_axis)
    also have "... = \<^sup>H\<integral>\<^bsub>\<partial>C\<^esub> F \<downharpoonright>\<^bsub>{j}\<^esub>"
      unfolding one_chain_line_integral_def
      apply(rule sum_inj_on[where F = "\<lambda>(k,\<gamma>). (k, C o \<gamma>)"]; clarsimp?)
    proof goal_cases
      show "inj_on (\<lambda>(k, \<gamma>). (k, C \<circ> \<gamma>)) (\<partial>rect_cube)"
        using cube_is_nice(7)
        unfolding valid_two_cube_def boundary_def rect_cube_def
        apply(auto simp add: o_def i_is_x_axis j_is_y_axis)
        subgoal apply (auto simp add: eval_nat_numeral)
          by (smt Suc_inject Zero_not_Suc card_Suc_eq insert_absorb)
        subgoal apply (auto simp add: eval_nat_numeral)
          by (smt Suc_inject card_Suc_eq insert_absorb insert_iff insert_not_empty)
        done
    next
      case ass: (2 k \<gamma>)
      {fix k \<gamma>
        assume "(k, \<gamma>) \<in> \<partial>rect_cube"
        then have "(\<integral>\<^bsub>\<gamma>\<^esub> proof_field1) = (\<integral>\<^bsub>C \<circ> \<gamma>\<^esub> F \<downharpoonright>\<^bsub>{j}\<^esub>)"
          unfolding real_pair_basis i_is_x_axis[symmetric] j_is_y_axis[symmetric] boundary_edges 
          apply safe
        proof goal_cases
          case ass: 1
          have i: "snd (top_edge rect_cube) = (\<lambda>x. x *\<^sub>R i + 1 *\<^sub>R j)" by (auto simp add: top_edge_def rect_cube_def)
          have "(\<integral>\<^bsub>snd (top_edge rect_cube)\<^esub> proof_field1 \<downharpoonright>\<^bsub>{i,j}\<^esub>) = (\<Sum>b\<in>{i,j}. (\<integral>\<^bsub>snd (top_edge rect_cube)\<^esub> proof_field1 \<downharpoonright>\<^bsub>{b}\<^esub>))"
            unfolding proof_field1_def
            apply(rule line_integral_summation(1))
            using lem1'(2)[OF i] lem2'_result[OF i]
            by auto
          moreover have "... = \<integral>\<^bsub>C \<circ> snd (top_edge rect_cube)\<^esub> F \<downharpoonright>\<^bsub>{j}\<^esub>"
            unfolding proof_field1_def
            using i_neq_j lem1'(1)[OF i] lem2'_result[OF i]
            by auto
          ultimately show ?case using ass
            by (metis (no_types, lifting) snd_conv)
        next
          case ass: 2
          have i: "snd (bot_edge rect_cube) = (\<lambda>x. x *\<^sub>R i + 0 *\<^sub>R j)" by (auto simp add: bot_edge_def rect_cube_def)
          have "(\<integral>\<^bsub>snd (bot_edge rect_cube)\<^esub> proof_field1 \<downharpoonright>\<^bsub>{i,j}\<^esub>) = (\<Sum>b\<in>{i,j}. (\<integral>\<^bsub>snd (bot_edge rect_cube)\<^esub> proof_field1 \<downharpoonright>\<^bsub>{b}\<^esub>))"
            unfolding proof_field1_def
            apply(rule line_integral_summation(1))
            using lem1'(2)[OF i] lem2'_result[OF i]
            by auto
          moreover have "... = \<integral>\<^bsub>C \<circ> snd (bot_edge rect_cube)\<^esub> F \<downharpoonright>\<^bsub>{j}\<^esub>"
            unfolding proof_field1_def
            using i_neq_j lem1'(1)[OF i] lem2'_result[OF i]
            by auto
          ultimately show ?case using ass
            by (metis (no_types, lifting) snd_conv)
        next
          case ass: 3
          have i: "snd (right_edge rect_cube) =  (\<lambda>y. 1 *\<^sub>R i + y *\<^sub>R j)" by (auto simp add: right_edge_def rect_cube_def)
          have "(\<integral>\<^bsub>snd (right_edge rect_cube)\<^esub> proof_field1 \<downharpoonright>\<^bsub>{i,j}\<^esub>) = (\<Sum>b\<in>{i,j}. (\<integral>\<^bsub>snd (right_edge rect_cube)\<^esub> proof_field1 \<downharpoonright>\<^bsub>{b}\<^esub>))"
            unfolding proof_field1_def
            apply(rule line_integral_summation(1))
            using lem1(2)[OF i] lem2_result[OF i]
            by auto
          moreover have "... = \<integral>\<^bsub>C \<circ> snd (right_edge rect_cube)\<^esub> F \<downharpoonright>\<^bsub>{j}\<^esub>"
            unfolding proof_field1_def
            using i_neq_j lem1(1)[OF i] lem2_result[OF i]
            by auto
          ultimately show ?case using ass
            by (metis (no_types, lifting) snd_conv)
        next
          case ass: 4
          have i: "snd (left_edge rect_cube) = (\<lambda>y. 0 *\<^sub>R i + y *\<^sub>R j)" by (auto simp add: left_edge_def rect_cube_def)
          have "(\<integral>\<^bsub>snd (left_edge rect_cube)\<^esub> proof_field1 \<downharpoonright>\<^bsub>{i,j}\<^esub>) = (\<Sum>b\<in>{i,j}. (\<integral>\<^bsub>snd (left_edge rect_cube)\<^esub> proof_field1 \<downharpoonright>\<^bsub>{b}\<^esub>))"
            unfolding proof_field1_def
            apply(rule line_integral_summation(1))
            using lem1(2)[OF i] lem2_result[OF i]
            by auto
          moreover have "... = \<integral>\<^bsub>C \<circ> snd (left_edge rect_cube)\<^esub> F \<downharpoonright>\<^bsub>{j}\<^esub>"
            unfolding proof_field1_def
            using i_neq_j lem1(1)[OF i] lem2_result[OF i]
            by auto
          ultimately show ?case using ass
            by (metis (no_types, lifting) snd_conv)
        qed
      }
      then show ?case using ass by auto
    next
      show "(\<lambda>(k, \<gamma>). (k, C \<circ> \<gamma>)) ` \<partial>rect_cube = \<partial>C"
        unfolding rect_cube_def
        unfolding boundary_def horizontal_boundary.simps vertical_boundary.simps i_is_x_axis j_is_y_axis
        by auto
    qed
    finally show ?thesis by auto
  qed
  moreover have "integral unit_cube (\<lambda>x. (\<partial> F \<circ> C\<^bsub>j\<^esub> / \<partial> i |\<^bsub>x\<^esub>) * (\<partial> C\<^bsub>j\<^esub> / \<partial> j |\<^bsub>x\<^esub>) - (\<partial> F \<circ> C\<^bsub>j\<^esub> / \<partial> j |\<^bsub>x\<^esub>) * (\<partial> C\<^bsub>j\<^esub> / \<partial> i |\<^bsub>x\<^esub>)
                                               + ((\<partial> F \<circ> C\<^bsub>i\<^esub> / \<partial> i |\<^bsub>x\<^esub>) * (\<partial> C\<^bsub>i\<^esub> / \<partial> j |\<^bsub>x\<^esub>) - (\<partial> F \<circ> C\<^bsub>i\<^esub> / \<partial> j |\<^bsub>x\<^esub>) * (\<partial> C\<^bsub>i\<^esub> / \<partial> i |\<^bsub>x\<^esub>)))
                         = integral unit_cube (\<lambda>x. (\<partial> F \<circ> C\<^bsub>j\<^esub> / \<partial> i |\<^bsub>x\<^esub>) * (\<partial> C\<^bsub>j\<^esub> / \<partial> j |\<^bsub>x\<^esub>) - (\<partial> F \<circ> C\<^bsub>j\<^esub> / \<partial> j |\<^bsub>x\<^esub>) * (\<partial> C\<^bsub>j\<^esub> / \<partial> i |\<^bsub>x\<^esub>) ) +
                           integral unit_cube (\<lambda>x. (\<partial> F \<circ> C\<^bsub>i\<^esub> / \<partial> i |\<^bsub>x\<^esub>) * (\<partial> C\<^bsub>i\<^esub> / \<partial> j |\<^bsub>x\<^esub>) - (\<partial> F \<circ> C\<^bsub>i\<^esub> / \<partial> j |\<^bsub>x\<^esub>) * (\<partial> C\<^bsub>i\<^esub> / \<partial> i |\<^bsub>x\<^esub>))"
    apply(rule integral_add)
    using more_anal_conds by auto
  moreover have "\<^sup>H\<integral>\<^bsub>\<partial>C\<^esub> F \<downharpoonright>\<^bsub>{i,j}\<^esub> = \<^sup>H\<integral>\<^bsub>\<partial>C\<^esub> F \<downharpoonright>\<^bsub>{i}\<^esub> + \<^sup>H\<integral>\<^bsub>\<partial>C\<^esub> F \<downharpoonright>\<^bsub>{j}\<^esub>"
  proof-
    have "\<^sup>H\<integral>\<^bsub>\<partial>C\<^esub> F \<downharpoonright>\<^bsub>{i,j}\<^esub> = (\<Sum>b\<in>{i,j}. \<^sup>H\<integral>\<^bsub>\<partial>C\<^esub> F \<downharpoonright>\<^bsub>{b}\<^esub>)"
      apply(rule one_chain_line_integral_summation(1)[OF finite_boundary])
      using line_integral_exists_on_boundary[OF cube_is_nice(1)[unfolded unit_cube_eq]] F_cont
      by auto 
    then show ?thesis
      by (auto simp add: i_neq_j)
  qed
  ultimately show "integral unit_cube (\<lambda>p. (\<partial> F \<circ> C\<^bsub>j\<^esub> / \<partial> i |\<^bsub>p\<^esub>) * (\<partial> C\<^bsub>j\<^esub> / \<partial> j |\<^bsub>p\<^esub>) - (\<partial> F \<circ> C\<^bsub>j\<^esub> / \<partial> j |\<^bsub>p\<^esub>) * (\<partial> C\<^bsub>j\<^esub> / \<partial> i |\<^bsub>p\<^esub>) + 
                                                 (\<partial> F \<circ> C\<^bsub>i\<^esub> / \<partial> i |\<^bsub>p\<^esub>) * (\<partial> C\<^bsub>i\<^esub> / \<partial> j |\<^bsub>p\<^esub>) - (\<partial> F \<circ> C\<^bsub>i\<^esub> / \<partial> j |\<^bsub>p\<^esub>) * (\<partial> C\<^bsub>i\<^esub> / \<partial> i |\<^bsub>p\<^esub>))
                   = \<^sup>H\<integral>\<^bsub>\<partial>C\<^esub> F \<downharpoonright>\<^bsub>{i,j}\<^esub>"
    by (smt Henstock_Kurzweil_Integration.integral_cong)
qed

lemma green_no_geom_less_analytic_assms:
      shows "integral unit_cube (\<lambda>p. (\<partial> F \<circ> C\<^bsub>j\<^esub> / \<partial> i |\<^bsub>p\<^esub>) * (\<partial> C\<^bsub>j\<^esub> / \<partial> j |\<^bsub>p\<^esub>) - (\<partial> F \<circ> C\<^bsub>j\<^esub> / \<partial> j |\<^bsub>p\<^esub>) * (\<partial> C\<^bsub>j\<^esub> / \<partial> i |\<^bsub>p\<^esub>) + 
                                                 (\<partial> F \<circ> C\<^bsub>i\<^esub> / \<partial> i |\<^bsub>p\<^esub>) * (\<partial> C\<^bsub>i\<^esub> / \<partial> j |\<^bsub>p\<^esub>) - (\<partial> F \<circ> C\<^bsub>i\<^esub> / \<partial> j |\<^bsub>p\<^esub>) * (\<partial> C\<^bsub>i\<^esub> / \<partial> i |\<^bsub>p\<^esub>))
                   = \<^sup>H\<integral>\<^bsub>\<partial>C\<^esub> F \<downharpoonright>\<^bsub>{i,j}\<^esub>"
proof-
  have i: "C1_differentiable_on' (C\<^bsub>i\<^esub>) UNIV"  "C1_differentiable_on' (C\<^bsub>j\<^esub>) UNIV"
    using cube_is_C2_diff(1)
    by (simp add: C1_diff'_imp_components)+
  then obtain C_i' C_j' where ii: "\<And>x. ((C\<^bsub>j\<^esub>) has_derivative C_j' x) (at x)" "\<And>x. ((C\<^bsub>i\<^esub>) has_derivative C_i' x) (at x)"
    unfolding C1_differentiable_on'_def
    by auto
  then have part_diff: "has_partial_vector_derivative (C\<^bsub>i\<^esub>) i (C_i' p i) p"
                       "has_partial_vector_derivative (C\<^bsub>i\<^esub>) j (C_i' p j) p"
                       "has_partial_vector_derivative (C\<^bsub>j\<^esub>) i (C_j' p i) p"
                       "has_partial_vector_derivative (C\<^bsub>j\<^esub>) j (C_j' p j) p"  for p
    by (simp add: has_derivative_to_has_partial_deriv i_is_x_axis j_is_y_axis)+
  then have iv: "C1_differentiable_on' (\<lambda>p. C_j' p i) UNIV"
                "C1_differentiable_on' (\<lambda>p. C_j' p j) UNIV"
                "C1_differentiable_on' (\<lambda>p. C_i' p j) UNIV"
                "C1_differentiable_on' (\<lambda>p. C_i' p i) UNIV"
    subgoal by (smt cube_is_C2_diff(3) C1_differentiable_on'_def has_derivative_transform partial_vector_derivative_works_2)
    subgoal by (smt C1_differentiable_on'_def part_diff cube_is_C2_diff(2) has_derivative_transform partial_vector_derivative_works_2)
    subgoal by (smt C1_differentiable_on'_def part_diff cube_is_C2_diff(5) has_derivative_transform partial_vector_derivative_works_2)
    subgoal by (smt C1_differentiable_on'_def part_diff cube_is_C2_diff(4) has_derivative_transform partial_vector_derivative_works_2)
    done
  then obtain C_j'i  C_j'j C_i'i  C_i'j where v: "\<And>x. ((\<lambda>xa. C_j' xa i) has_derivative C_j'i x) (at x)"
                                                 "\<And>x. ((\<lambda>xa. C_j' xa j) has_derivative C_j'j x) (at x)"
                                                 "\<And>x. ((\<lambda>xa. C_i' xa j) has_derivative C_i'j x) (at x)"
                                                 "\<And>x. ((\<lambda>xa. C_i' xa i) has_derivative C_i'i x) (at x)"
    unfolding C1_differentiable_on'_def
    by auto
  show ?thesis
    apply(intro green_no_geom[where C_i' = C_i' and C_j' = C_j' and C_j'i = C_j'i and C_j'j = C_j'j and C_i'i = C_i'i and C_i'j = C_i'j ])
    using cube_is_C2_diff cube_has_four_sides Field_analytically_valid
          i ii part_diff iv v C1_diff'_subset by blast+
qed

lemma green_no_geom_less_analytic_assms'':
  "integral unit_cube (\<lambda>p. jac_det C p *\<^sub>R ((\<partial> F\<^bsub>j\<^esub> / \<partial>i |\<^bsub>(C p)\<^esub>) - \<partial> F\<^bsub>i\<^esub> / \<partial>j |\<^bsub>(C p)\<^esub>))
                   = \<^sup>H\<integral>\<^bsub>\<partial>C\<^esub> F \<downharpoonright>\<^bsub>{i,j}\<^esub>"
proof-
  have *: "((F \<circ> C)\<^bsub>i\<^esub>) = (F\<^bsub>i\<^esub>) \<circ> C" for i
    by auto
  obtain C' where C': "(C has_derivative C' x) (at x)" for x
    using C1_differentiable_on'_has_derivative[OF cube_is_C2_diff(1)]
    by auto
  obtain F'i where F'i: "((F\<^bsub>i\<^esub>) has_derivative F'i x) (at x)" if "x \<in> cubeImage C" for x
    using C1_differentiable_on'_has_derivative[OF C1_diff'_imp_components[OF Field_analytically_valid]]
    using that 
    by metis
  have i: "(\<partial> (F \<circ> C)\<^bsub>i\<^esub> / \<partial> j |\<^bsub>p\<^esub>) = \<partial>C\<^bsub>i\<^esub> / \<partial>j |\<^bsub>p\<^esub> *\<^sub>R  ( \<partial> F\<^bsub>i\<^esub> / \<partial>i |\<^bsub>C p\<^esub>) + \<partial>C\<^bsub>j\<^esub> / \<partial>j |\<^bsub>p\<^esub> *\<^sub>R  ( \<partial> F\<^bsub>i\<^esub> / \<partial>j |\<^bsub>C p\<^esub>)" if "p\<in>unit_cube" for p
    unfolding *
    apply(intro partial_derivative_chain_pair[OF F'i C' _ _ i_is_x_axis j_is_y_axis, where S = "cbox (0,0) (1,1)" and T = "cubeImage C"])
    by (auto simp add: cubeImage_def j_is_y_axis that)
    
  have ii: "(\<partial> (F \<circ> C)\<^bsub>i\<^esub> / \<partial> i |\<^bsub>p\<^esub>) = \<partial>C\<^bsub>i\<^esub> / \<partial>i |\<^bsub>p\<^esub> *\<^sub>R  ( \<partial> F\<^bsub>i\<^esub> / \<partial>i |\<^bsub>C p\<^esub>) + \<partial>C\<^bsub>j\<^esub> / \<partial>i |\<^bsub>p\<^esub> *\<^sub>R  ( \<partial> F\<^bsub>i\<^esub> / \<partial>j |\<^bsub>C p\<^esub>)" if "p\<in>unit_cube" for p
    unfolding *
    apply(intro partial_derivative_chain_pair[OF F'i C' _ _ i_is_x_axis j_is_y_axis, where S = "cbox (0,0) (1,1)" and T = "cubeImage C"])
    by (auto simp add: cubeImage_def i_is_x_axis that)
  obtain F'j where F'j: "((F\<^bsub>j\<^esub>) has_derivative F'j x) (at x)" if "x \<in> cubeImage C" for x
    using C1_differentiable_on'_has_derivative[OF C1_diff'_imp_components[OF Field_analytically_valid]]
    using that 
    by metis
  have iii: "(\<partial> (F \<circ> C)\<^bsub>j\<^esub> / \<partial> i |\<^bsub>p\<^esub>) = \<partial>C\<^bsub>i\<^esub> / \<partial>i |\<^bsub>p\<^esub> *\<^sub>R  ( \<partial> F\<^bsub>j\<^esub> / \<partial>i |\<^bsub>C p\<^esub>) + \<partial>C\<^bsub>j\<^esub> / \<partial>i |\<^bsub>p\<^esub> *\<^sub>R  ( \<partial> F\<^bsub>j\<^esub> / \<partial>j |\<^bsub>C p\<^esub>)"  if "p\<in>unit_cube" for p
    unfolding *
    apply(intro partial_derivative_chain_pair[OF F'j C' _ _ i_is_x_axis j_is_y_axis, where S = "cbox (0,0) (1,1)" and T = "cubeImage C"])
    by (auto simp add: cubeImage_def i_is_x_axis that)
  have iv: "(\<partial> (F \<circ> C)\<^bsub>j\<^esub> / \<partial> j |\<^bsub>p\<^esub>) = \<partial>C\<^bsub>i\<^esub> / \<partial>j |\<^bsub>p\<^esub> *\<^sub>R  ( \<partial> F\<^bsub>j\<^esub> / \<partial>i |\<^bsub>C p\<^esub>) + \<partial>C\<^bsub>j\<^esub> / \<partial>j |\<^bsub>p\<^esub> *\<^sub>R  ( \<partial> F\<^bsub>j\<^esub> / \<partial>j |\<^bsub>C p\<^esub>)"  if "p\<in>unit_cube" for p
    unfolding *
    apply(intro partial_derivative_chain_pair[OF F'j C' _ _ i_is_x_axis j_is_y_axis, where S = "cbox (0,0) (1,1)" and T = "cubeImage C"])
    by (auto simp add: cubeImage_def j_is_y_axis that)
  have "integral (cbox (0,0) (1,1)) (\<lambda>p. jac_det C p *\<^sub>R  ((\<partial> F\<^bsub>j\<^esub> / \<partial>i |\<^bsub>(C p)\<^esub>) - \<partial> F\<^bsub>i\<^esub> / \<partial>j |\<^bsub>(C p)\<^esub>)) =
             integral (cbox (0,0) (1,1)) (\<lambda>p. (\<partial> F \<circ> C\<^bsub>j\<^esub> / \<partial> i |\<^bsub>p\<^esub>) * (\<partial> C\<^bsub>j\<^esub> / \<partial> j |\<^bsub>p\<^esub>) - (\<partial> F \<circ> C\<^bsub>j\<^esub> / \<partial> j |\<^bsub>p\<^esub>) * (\<partial> C\<^bsub>j\<^esub> / \<partial> i |\<^bsub>p\<^esub>) + 
                                                 (\<partial> F \<circ> C\<^bsub>i\<^esub> / \<partial> i |\<^bsub>p\<^esub>) * (\<partial> C\<^bsub>i\<^esub> / \<partial> j |\<^bsub>p\<^esub>) - (\<partial> F \<circ> C\<^bsub>i\<^esub> / \<partial> j |\<^bsub>p\<^esub>) * (\<partial> C\<^bsub>i\<^esub> / \<partial> i |\<^bsub>p\<^esub>))"
    apply(intro integral_cong)
    subgoal for p using i[of p] ii[of p] iii[of p] iv[of p]
      by(auto simp add: algebra_simps jac_det_def)
    done
  then show ?thesis
    using green_no_geom_less_analytic_assms
    by (auto simp add: zero_prod_def)
qed

end

(*
find_theorems "(blinfun_apply (f' x))"

find_theorems name: inverse name: "function"

lemma "{m. det (matrix m) > 0} \<inter> {m. det (matrix m) < 0} = {}"
  

lemma "continuous_on S f \<Longrightarrow> x \<in> S \<Longrightarrow> det (matrix (f x)) \<ge> 0 \<or> det (matrix (f x)) \<le> 0"
*)  

context R2
begin

lemma det_expanded:
  assumes der_g: "(g has_derivative g') (at x)"
  shows "jac_det g x = det (matrix (embed_vec \<circ> g' \<circ> embed_pair))"
proof- 
  have *: "(\<partial> (g\<^bsub>v\<^esub>) / \<partial> b |\<^bsub>x\<^esub>) = (g' b) \<bullet> v"   for b v
     using partial_vector_derivative_inner[OF has_derivative_to_has_partial_deriv''[OF  der_g]]
           partial_vector_derivative_works_2[OF has_derivative_to_has_partial_deriv''[OF  der_g]]
     by simp
  have "axis (i::2) (1::real) $ j = 0" if "i \<noteq> j" for i j
    using that
    by (simp add: cart_eq_inner_axis inner_axis_axis)
  moreover have "fst x = x \<bullet> i" "snd x = x \<bullet> j" for x
    unfolding i_is_x_axis j_is_y_axis
    by (auto simp add: inner_Pair_0)
  moreover have "i \<in> Basis" "j \<in> Basis"
    by(auto simp add: i_is_x_axis j_is_y_axis real_pair_basis)
  ultimately show ?thesis
    apply (simp add: jac_det_def det_2 o_def embed_pair_def embed_vec_def matrix_def inner_axis_axis
                     i_is_x_axis[symmetric] j_is_y_axis[symmetric])
    by(auto intro!: arg_cong2[where f = "(*)"] arg_cong2[where f = "(-)"] *
            simp: real_pair_basis)
qed

lemma linear_embed_vec: "linear embed_vec"
  by (simp add: linear_def embed_vec_def Vector_Spaces.linear_def module_hom_def
                module_hom_axioms_def axis_def vector_space_axioms module_axioms
                algebra_simps)

lemma linear_embed_pair: "linear embed_pair"
  by (simp add: linear_def embed_pair_def Vector_Spaces.linear_def module_hom_def
                module_hom_axioms_def axis_def vector_space_axioms module_axioms
                algebra_simps)

lemma det_compose:
  assumes der_f[intro]: "(f has_derivative f') (at (g x))" and der_g[intro]: "(g has_derivative g') (at x)"
  shows "jac_det (f o g) x = jac_det f (g x) * jac_det g x"
proof- 
  have *: "((f o g) has_derivative (f' o g')) (at x)"
    by(auto intro!: derivative_eq_intros)
  hence "jac_det (f o g) x = det (matrix (embed_vec \<circ> (f' \<circ> g') \<circ> embed_pair))"
    using det_expanded
    by auto
  also have "... = det (matrix ((embed_vec \<circ> f') \<circ> (g' \<circ> embed_pair)))"
    by (simp add: comp_assoc)
  also have "... = det (matrix ((embed_vec \<circ> f') o (embed_pair \<circ> embed_vec) o (g' \<circ> embed_pair)))"
    by (simp add: embed_vec_pair_id)
  also have "... = det (matrix ((embed_vec \<circ> f' o embed_pair) \<circ> (embed_vec o g' \<circ> embed_pair)))"
    by (simp add: comp_assoc)
  also have "... = det ((matrix (embed_vec \<circ> f' o embed_pair)) ** (matrix (embed_vec o g' \<circ> embed_pair)))"
    apply(subst matrix_compose)
    by (auto intro!: linear_compose linear_embed_pair linear_embed_vec
                     has_derivative_linear[OF der_f] has_derivative_linear[OF der_g])
  also have "... = det (matrix (embed_vec \<circ> f' o embed_pair)) * det (matrix (embed_vec o g' \<circ> embed_pair))"
    by (simp add: det_mul)
  finally have "jac_det (f o g) x = ..."
    .
  thus ?thesis
    using der_f der_g det_expanded by auto
qed

proposition has_absolute_integral_change_of_variables_invertible:
  fixes f :: "(real\<times>real) \<Rightarrow> (real)" and g :: "(real\<times>real) \<Rightarrow> (real\<times>real)"
  assumes der_g: "\<And>x. x \<in> S \<Longrightarrow> (g has_derivative g' x) (at x)"
      and hg: "\<And>x. x \<in> S \<Longrightarrow> h(g x) = x"
      and conth: "continuous_on (g ` S) h"
      and orientation_pres: "\<And>x. x \<in> S \<Longrightarrow> jac_det g x \<ge> 0"
      and f_int: "f absolutely_integrable_on (g ` S)"
    shows "(\<lambda>x. jac_det g x *\<^sub>R f(g x)) absolutely_integrable_on S" (is ?g1)
          "integral (g ` S) f =
                 integral S (\<lambda>x. jac_det g x *\<^sub>R f(g x))" (is ?g2)
proof-
  have der_g_within: "\<And>x. x \<in> S \<Longrightarrow> (g has_derivative g' x) (at x within S)"
    using der_g
    using has_derivative_at_withinI by blast
  have eq: "jac_det g x = \<bar> det (matrix (embed_vec \<circ> g' x \<circ> embed_pair)) \<bar>" if "x \<in> S" for x
    using orientation_pres[OF that] that det_expanded[OF der_g]
    by (fastforce simp add: jac_det_def)
  moreover have "(\<lambda>x. \<bar>det (matrix (embed_vec \<circ> g' x \<circ> embed_pair))\<bar> *\<^sub>R f (g x)) absolutely_integrable_on S"
    apply(intro conjunct1[OF iffD2[OF has_absolute_integral_change_of_variables_invertible_pair(1)[OF der_g_within hg conth]]])
    using assms
    by auto
  ultimately show ?g1
    apply(subst absolutely_integrable_spike[where S = "{}", unfolded Diff_empty])
    by auto
  have det_int: "integral S (\<lambda>x. \<bar>det (matrix (embed_vec \<circ> g' x \<circ> embed_pair))\<bar> *\<^sub>R f (g x)) = integral (g ` S) f"
    apply(intro conjunct2[OF iffD2[OF has_absolute_integral_change_of_variables_invertible_pair(1)[OF der_g_within hg conth]]])
    using assms
    by auto
  show ?g2
    apply (subst det_int[symmetric])
    apply(rule integral_cong)
    using eq by (auto simp: jac_det_def)
qed

proposition has_absolute_integral_change_of_variables_invertible_2:
  fixes f :: "(real\<times>real) \<Rightarrow> (real)" and g :: "(real\<times>real) \<Rightarrow> (real\<times>real)"
  assumes
    der_g: "\<And>x. x \<in> S \<Longrightarrow> (g has_derivative g' x) (at x)"
    and hg: "\<And>x. x \<in> S \<Longrightarrow> h(g x) = x"
    and conth: "continuous_on (g ` S) h"
    (*and orientation_pres: "\<And>x. x \<in> S \<Longrightarrow> (\<partial>g\<^bsub>i\<^esub>/ \<partial>i |\<^bsub>x\<^esub> * \<partial>g\<^bsub>j\<^esub>/ \<partial>j |\<^bsub>x\<^esub> - \<partial>g\<^bsub>i\<^esub>/ \<partial>j |\<^bsub>x\<^esub> * \<partial>g\<^bsub>j\<^esub>/ \<partial>i |\<^bsub>x\<^esub>) \<ge> 0"*)
    and f_int: "(\<lambda>x. \<bar> jac_det g x \<bar> *\<^sub>R f(g x))
                    absolutely_integrable_on S"
  shows "f absolutely_integrable_on (g ` S)" (is ?g1)
          "integral (g ` S) f =
                 integral S (\<lambda>x. \<bar> jac_det g x \<bar> *\<^sub>R f(g x))" (is ?g2)
proof-
  have der_g_within: "\<And>x. x \<in> S \<Longrightarrow> (g has_derivative g' x) (at x within S)"
    using der_g
    using has_derivative_at_withinI by blast
  have eq: "det (matrix (embed_vec \<circ> g' x \<circ> embed_pair)) = 
              jac_det g x"
     if "x \<in> S" for x
    using that det_expanded[OF der_g]
    by (simp add: jac_det_def)
  hence det_int: 
    "(\<lambda>x. \<bar> det (matrix (embed_vec \<circ> g' x \<circ> embed_pair)) \<bar> *\<^sub>R f (g x)) absolutely_integrable_on S"
    by(auto intro: absolutely_integrable_spike[OF f_int, where S = "{}", unfolded Diff_empty eq])
  then show ?g1                           
    by (auto intro: conjunct1[OF iffD1[OF has_absolute_integral_change_of_variables_invertible_pair(1)[OF der_g_within hg conth]]])
  have det_int: "integral (g ` S) f = integral S (\<lambda>x. \<bar>det (matrix (embed_vec \<circ> g' x \<circ> embed_pair))\<bar> *\<^sub>R f (g x))"
    apply(rule conjunct2[OF iffD1[OF has_absolute_integral_change_of_variables_invertible_pair(1)[OF der_g_within hg conth]]])
    using assms det_int
    by auto
  show ?g2
    using eq
    by (auto simp: det_int jac_det_def intro!: integral_cong)
qed

proposition has_absolute_integral_change_of_variables_invertible_2_spike_finite:
  fixes f :: "(real\<times>real) \<Rightarrow> (real)" and g :: "(real\<times>real) \<Rightarrow> (real\<times>real)"
  assumes negligible_T: "finite T" 
      and der_g: "\<And>x. x \<in> S - T \<Longrightarrow> (g has_derivative g' x) (at x)"
      and hg: "\<And>x. x \<in> S - T \<Longrightarrow> h(g x) = x"
      and conth: "continuous_on (g ` (S - T)) h"
      (*and orientation_pres: "\<And>x. x \<in> S -T \<Longrightarrow> (\<partial>g\<^bsub>i\<^esub>/ \<partial>i |\<^bsub>x\<^esub> * \<partial>g\<^bsub>j\<^esub>/ \<partial>j |\<^bsub>x\<^esub> - \<partial>g\<^bsub>i\<^esub>/ \<partial>j |\<^bsub>x\<^esub> * \<partial>g\<^bsub>j\<^esub>/ \<partial>i |\<^bsub>x\<^esub>) \<ge> 0"*)
      and f_int: "(\<lambda>x. \<bar> jac_det g x \<bar> *\<^sub>R f(g x)) 
                  absolutely_integrable_on (S - T)"
    shows "f absolutely_integrable_on (g ` S)" (is ?g1)
          "integral (g ` S) f =
                 integral S (\<lambda>x. \<bar> jac_det g x \<bar> *\<^sub>R f(g x))" (is ?g2)
proof-
  have "f absolutely_integrable_on (g ` (S - T))"
    by (auto intro: has_absolute_integral_change_of_variables_invertible_2[OF assms(2-)])
  moreover have neg_g_img: "negligible (g ` T)"
    using negligible_T
    by (simp add: negligible_finite)
  ultimately show ?g1
    apply (subst absolutely_integrable_spike_set_eq[where T = "g ` (S - T)"])
    by(fastforce intro: negligible_subset[of "g ` T"] simp add: image_def)+
  have "integral (g ` (S - T)) f =
                 integral (S - T) (\<lambda>x. \<bar> jac_det g x \<bar> *\<^sub>R f(g x))"
    by (fastforce intro: has_absolute_integral_change_of_variables_invertible_2[OF assms(2-)])
  moreover have "integral (g ` S) f = integral (g ` (S - T)) f"
    by(fastforce intro: integral_spike_set[where T = "g ` (S - T)"] negligible_subset[of "g ` T"]
                 simp add: image_def negligible_T)+
  moreover have "integral S (\<lambda>x. \<bar> jac_det g x\<bar> *\<^sub>R f(g x))
                  = integral (S - T) (\<lambda>x. \<bar> jac_det g x \<bar> *\<^sub>R f(g x))"
    by(fastforce intro: negligible_subset[of "T"] integral_spike_set[where T = "(S - T)"]
                 simp add: image_def negligible_finite[OF negligible_T])+
  ultimately show ?g2
    by simp
qed

proposition has_absolute_integral_change_of_variables_invertible_2_spike:
  fixes f :: "(real\<times>real) \<Rightarrow> (real)" and g :: "(real\<times>real) \<Rightarrow> (real\<times>real)"
  assumes negligible_T: "negligible T" "negligible (g ` T)" 
      and der_g: "\<And>x. x \<in> S - T \<Longrightarrow> (g has_derivative g' x) (at x)"
      and hg: "\<And>x. x \<in> S - T \<Longrightarrow> h(g x) = x"
      and conth: "continuous_on (g ` (S - T)) h"
      (*and orientation_pres: "\<And>x. x \<in> S -T \<Longrightarrow> (\<partial>g\<^bsub>i\<^esub>/ \<partial>i |\<^bsub>x\<^esub> * \<partial>g\<^bsub>j\<^esub>/ \<partial>j |\<^bsub>x\<^esub> - \<partial>g\<^bsub>i\<^esub>/ \<partial>j |\<^bsub>x\<^esub> * \<partial>g\<^bsub>j\<^esub>/ \<partial>i |\<^bsub>x\<^esub>) \<ge> 0"*)
      and f_int: "(\<lambda>x. \<bar> jac_det g x \<bar> *\<^sub>R 
                    f(g x)) absolutely_integrable_on (S - T)"
    shows "f absolutely_integrable_on (g ` S)" (is ?g1)
          "integral (g ` S) f =
             integral S (\<lambda>x. \<bar> jac_det g x \<bar> *\<^sub>R f(g x))" (is ?g2)
proof-
  have "f absolutely_integrable_on (g ` (S - T))"
    by (auto intro: has_absolute_integral_change_of_variables_invertible_2[OF assms(3-)])
  then show ?g1
    using negligible_T
    apply (subst absolutely_integrable_spike_set_eq[where T = "g ` (S - T)"])
    by(fastforce simp add: image_def intro: negligible_subset[of "g ` T"])+
  have "integral (g ` (S - T)) f =
          integral (S - T) (\<lambda>x. \<bar> jac_det g x \<bar> *\<^sub>R f(g x))"
    by (fastforce intro: has_absolute_integral_change_of_variables_invertible_2[OF assms(3-)])
  moreover have "integral (g ` S) f = integral (g ` (S - T)) f"
    using negligible_T 
    by (auto intro:  integral_spike_set[where T = "g ` (S - T)"] negligible_subset[of "g ` T"])
  moreover have "integral S (\<lambda>x. \<bar> jac_det g x \<bar> *\<^sub>R f(g x))
                   = integral (S - T) (\<lambda>x. \<bar> jac_det g x \<bar> *\<^sub>R f(g x))"
    using negligible_T
    by (auto intro: integral_spike_set[where T = "(S - T)"] negligible_subset[of "T"])
  ultimately show ?g2
    by simp
qed

proposition has_absolute_integral_change_of_variables_invertible_2_spike':
  fixes f :: "(real\<times>real) \<Rightarrow> (real)" and g :: "(real\<times>real) \<Rightarrow> (real\<times>real)"
  assumes negligible_T: "negligible T" "T \<subseteq> S" 
      and der_g: "\<And>x. x \<in> S \<Longrightarrow> (g has_derivative g' x) (at x)"
      and hg: "\<And>x. x \<in> S - T \<Longrightarrow> h(g x) = x"
      and conth: "continuous_on (g ` (S - T)) h"
      (*and orientation_pres: "\<And>x. x \<in> S -T \<Longrightarrow> (\<partial>g\<^bsub>i\<^esub>/ \<partial>i |\<^bsub>x\<^esub> * \<partial>g\<^bsub>j\<^esub>/ \<partial>j |\<^bsub>x\<^esub> - \<partial>g\<^bsub>i\<^esub>/ \<partial>j |\<^bsub>x\<^esub> * \<partial>g\<^bsub>j\<^esub>/ \<partial>i |\<^bsub>x\<^esub>) \<ge> 0"*)
      and f_int: "(\<lambda>x. \<bar> jac_det g x \<bar> *\<^sub>R f(g x)) absolutely_integrable_on (S - T)"
    shows "f absolutely_integrable_on (g ` S)" (is ?g1)
          "integral (g ` S) f =
                 integral S (\<lambda>x. \<bar> jac_det g x \<bar> *\<^sub>R f(g x))" (is ?g2)
proof-
  from negligible_T(1) have "negligible (g ` T)"
    apply (intro negligible_differentiable_image_negligible)
    using der_g negligible_T has_derivative_at_withinI
    by (fastforce simp add: differentiable_on_def differentiable_def)+
  moreover have "f absolutely_integrable_on (g ` (S - T))"
    by (auto intro: has_absolute_integral_change_of_variables_invertible_2[OF assms(3-)])
  ultimately show ?g1
    apply (subst absolutely_integrable_spike_set_eq[where T = "g ` (S - T)"])
    by(fastforce simp add: negligible_T(1) image_def intro: negligible_subset[of "g ` T"])+
  have "integral (g ` (S - T)) f =
                 integral (S - T) (\<lambda>x. \<bar>  jac_det g x \<bar> *\<^sub>R f(g x))"
    by (fastforce intro: has_absolute_integral_change_of_variables_invertible_2[OF assms(3-)])
  moreover have "integral (g ` S) f = integral (g ` (S - T)) f"
    using negligible_T(1) \<open>negligible (g ` T)\<close> 
    by (auto intro:  integral_spike_set[where T = "g ` (S - T)"] negligible_subset[of "g ` T"])
  moreover have "integral S (\<lambda>x. \<bar> jac_det g x \<bar> *\<^sub>R f(g x))
                = integral (S - T) (\<lambda>x. \<bar> jac_det g x \<bar> *\<^sub>R f(g x))"
    using negligible_T
    by (auto intro: integral_spike_set[where T = "(S - T)"] negligible_subset[of "T"])
  ultimately show ?g2
    by simp
qed

lemma basis: "Basis = {i, j}" "i \<in> Basis" "j \<in> Basis" "i \<bullet> i = 1" "j \<bullet> j = 1"
  by (simp add: i_is_x_axis j_is_y_axis real_pair_basis)+

end

lemma IVT_topological_space:
  fixes f ::"'a::topological_space \<Rightarrow> real"
  assumes "continuous_on s f" "connected s" "compact s" "(\<And>x. x \<in> s \<Longrightarrow> f x \<noteq> c)"
  shows "(\<forall>x\<in> s. f x > c) \<or> (\<forall>x\<in>s. f x < c)"
proof(rule ccontr)
  have "connected (f ` s)"
    using \<open>connected s\<close> \<open>continuous_on s f\<close>
    by (simp add: connected_continuous_image)
  moreover have "compact (f ` s)"
    using \<open>compact s\<close> \<open>continuous_on s f\<close>
    by (simp add: compact_continuous_image)
  ultimately obtain a b where "f ` s = {a..b}"
    by (force simp add: connected_compact_interval_1[symmetric])
  hence "c \<notin> {a..b}"
    using assms(4) 
    by (force dest!: equalityD2 HOL.spec[where x = c] simp: subset_iff)
  moreover assume "\<not> ((\<forall>x\<in>s. c < f x) \<or> (\<forall>x\<in>s. f x < c))"
  then obtain x y where "f x < c" "c < f y" "x\<in>s" "y\<in>s"
    using assms(4) less_linear
    by blast
  hence "c \<in> {f x .. f y}" "{f x .. f y} \<subseteq> {a .. b}"
    using \<open>f ` s = {a..b}\<close>
    by auto
  ultimately show False
    by auto
qed

lemma IVT_cbox:
  fixes f ::"'a::euclidean_space \<Rightarrow> real"
  shows "\<lbrakk>continuous_on (cbox a b) f; (\<And>x. x \<in> cbox a b \<Longrightarrow> f x \<noteq> c)\<rbrakk>
         \<Longrightarrow> (\<forall>x\<in>cbox a b. f x > c) \<or> (\<forall>x\<in>cbox a b. f x < c)"
  by (rule IVT_topological_space, auto simp: is_interval_connected)

context Green_No_Geom_Cube
begin

lemma jac_det_cont:
  "continuous_on unit_cube (jac_det C)"
    unfolding  jac_det_def
    by(fastforce simp add: i_is_x_axis j_is_y_axis
                 intro: continuous_intros C1_partially_differentiable_on_imp_cont_part_deriv
                        C1_smooth_gen_C1_smooth_old' C1_diff'_imp_components
                        C1_diff'_subset[where s = UNIV and C = C and t = unit_cube] cube_is_C2_diff)

lemma unit_cube_nempty: "unit_cube \<noteq> {}"
  by (fastforce simp: cbox_def basis i_is_x_axis j_is_y_axis )

lemma green_no_geom_less_analytic_assms''':
  defines "orient \<equiv> if (\<forall>x \<in> unit_cube. jac_det C x > 0) then 1 else -1"
  shows
   "orient * integral (cubeImage C) (\<lambda>p. \<partial> F\<^bsub>j\<^esub> / \<partial>i |\<^bsub>p\<^esub> - \<partial> F\<^bsub>i\<^esub> / \<partial>j |\<^bsub>p\<^esub> ) = \<^sup>H\<integral>\<^bsub>\<partial>C\<^esub> F" (is ?g1)
   "(\<lambda>p. \<partial> F\<^bsub>j\<^esub> / \<partial>i |\<^bsub>p\<^esub> - \<partial> F\<^bsub>i\<^esub> / \<partial>j |\<^bsub>p\<^esub> )  absolutely_integrable_on (cubeImage C)" (is ?g2)
proof-
  let ?S' = "S \<inter> unit_cube"
  have diff_inter: "s - t \<inter> s = s - t" for t s
    by auto
  have cube_has_an_inverse': 
    "\<And>x. x \<in> (unit_cube - ?S') \<Longrightarrow> C_inv (C x) = x"
    "continuous_on (C ` (unit_cube - ?S')) C_inv"
    "negligible ?S'"
    "?S' \<subseteq> S"
    using cube_has_an_inverse
    by (auto simp add: diff_inter intro: negligible_subset)
  obtain C' where C': "(\<And>x. x\<in>unit_cube \<Longrightarrow> (C has_derivative (C' x)) (at x))" "continuous_on unit_cube C'"
    using cube_is_C2_diff unfolding C1_differentiable_on'_def
    by (metis UNIV_I continuous_on_subset top_greatest)
  define jac_det' where "jac_det' \<equiv> jac_det C"
  define abs_jac_det where "abs_jac_det \<equiv> \<lambda>x. \<bar> jac_det' x \<bar>"
  have "continuous_on (cubeImage C) (\<lambda>p. \<partial> F\<^bsub>j\<^esub> / \<partial>i |\<^bsub>p\<^esub> - \<partial> F\<^bsub>i\<^esub> / \<partial>j |\<^bsub>p\<^esub> )"
    by(fastforce simp add: i_is_x_axis j_is_y_axis
                 intro: continuous_intros C1_partially_differentiable_on_imp_cont_part_deriv
                        C1_smooth_gen_C1_smooth_old' C1_diff'_imp_components Field_analytically_valid)
  then have "continuous_on unit_cube ((\<lambda>p. \<partial> F\<^bsub>j\<^esub> / \<partial>i |\<^bsub>p\<^esub> - \<partial> F\<^bsub>i\<^esub> / \<partial>j |\<^bsub>p\<^esub> ) o C)"
    by (fastforce intro: continuous_on_compose
                         C_diff'_imp_cont C1_diff'_subset[where s = UNIV and C = C and t = unit_cube]
                         cube_is_C2_diff simp add: cubeImage_def)
  moreover have "continuous_on unit_cube abs_jac_det"
    unfolding abs_jac_det_def jac_det'_def 
    by(fastforce intro: continuous_intros jac_det_cont)
  ultimately have
    "(\<lambda>x.  abs_jac_det x *\<^sub>R ((\<partial> F\<^bsub>j\<^esub> / \<partial> i |\<^bsub>C x\<^esub>) - (\<partial> F\<^bsub>i\<^esub> / \<partial> j |\<^bsub>C x\<^esub>)))
     absolutely_integrable_on unit_cube"
    by (fastforce intro!: absolutely_integrable_continuous continuous_on_scaleR)
  moreover have "C absolutely_integrable_on unit_cube - ?S'"
    if "C absolutely_integrable_on unit_cube"
    for C::"(real \<times> real) \<Rightarrow> 'b::euclidean_space"
    by(fastforce intro: absolutely_integrable_spike_set[OF that] negligible_subset[of "S"]
                 simp add: cube_has_an_inverse)+
  ultimately have "integral (cubeImage C) (\<lambda>p. (\<partial> F\<^bsub>j\<^esub> / \<partial>i |\<^bsub>p\<^esub>) - \<partial> F\<^bsub>i\<^esub> / \<partial>j |\<^bsub>p\<^esub>) =
                    integral unit_cube (\<lambda>p. abs_jac_det p *\<^sub>R ((\<partial> F\<^bsub>j\<^esub> / \<partial> i |\<^bsub>C p\<^esub>) - (\<partial> F\<^bsub>i\<^esub> / \<partial> j |\<^bsub>C p\<^esub>)))"
    and g2: ?g2
    unfolding cubeImage_def abs_jac_det_def jac_det'_def
    by (fastforce simp add: C' C1_differentiable_on'_def cbox_def real_pair_basis  
                  intro: has_absolute_integral_change_of_variables_invertible_2_spike'
                           [where g' = C', OF cube_has_an_inverse'(3) _ _ cube_has_an_inverse'(1,2)])+
  hence "(\<And>p. p \<in> unit_cube \<Longrightarrow> 0 \<le> jac_det' p) \<Longrightarrow>
          integral (cubeImage C) (\<lambda>p. (\<partial> F\<^bsub>j\<^esub> / \<partial>i |\<^bsub>p\<^esub>) - \<partial> F\<^bsub>i\<^esub> / \<partial>j |\<^bsub>p\<^esub>) =
                    integral unit_cube (\<lambda>p. jac_det' p *\<^sub>R ((\<partial> F\<^bsub>j\<^esub> / \<partial> i |\<^bsub>C p\<^esub>) - (\<partial> F\<^bsub>i\<^esub> / \<partial> j |\<^bsub>C p\<^esub>)))"
        "(\<And>p. p \<in> unit_cube \<Longrightarrow> jac_det' p \<le> 0) \<Longrightarrow>
          integral (cubeImage C) (\<lambda>p. (\<partial> F\<^bsub>j\<^esub> / \<partial>i |\<^bsub>p\<^esub>) - \<partial> F\<^bsub>i\<^esub> / \<partial>j |\<^bsub>p\<^esub>) =
                    - integral unit_cube (\<lambda>p. jac_det' p *\<^sub>R ((\<partial> F\<^bsub>j\<^esub> / \<partial> i |\<^bsub>C p\<^esub>) - (\<partial> F\<^bsub>i\<^esub> / \<partial> j |\<^bsub>C p\<^esub>)))"
    using minus_mult_left
    by (auto intro!: integral_cong simp flip: integral_neg simp: abs_jac_det_def)
  moreover have "(\<And>p. p \<in> unit_cube \<Longrightarrow> jac_det' p \<ge> 0 \<longleftrightarrow> 0 < jac_det' p)"
                "(\<And>p. p \<in> unit_cube \<Longrightarrow> jac_det' p \<le> 0 \<longleftrightarrow> jac_det' p < 0)"
    using orientation_preserving_cube
    by(fastforce simp: jac_det'_def)+
  moreover have "orient = 1 \<longleftrightarrow> (\<forall>x \<in> unit_cube. jac_det C x > 0)"
                "orient = -1 \<longleftrightarrow> (\<forall>x \<in> unit_cube. jac_det C x < 0)"
    using IVT_cbox[OF jac_det_cont orientation_preserving_cube, simplified]
    by (force simp add: orient_def)+      
  ultimately show ?g1
    using green_no_geom_less_analytic_assms''
          IVT_cbox[OF jac_det_cont orientation_preserving_cube, simplified]
    by(auto simp: jac_det'_def basis(1))

  show ?g2
    using g2 .
qed

lemma green_no_geom_less_analytic_assms_abs:
   "\<bar> integral (cubeImage C) (\<lambda>p. \<partial> F\<^bsub>j\<^esub> / \<partial>i |\<^bsub>p\<^esub> - \<partial> F\<^bsub>i\<^esub> / \<partial>j |\<^bsub>p\<^esub> ) \<bar> = \<bar> \<^sup>H\<integral>\<^bsub>\<partial>C\<^esub> F \<bar>"
  using green_no_geom_less_analytic_assms'''
  by (smt (z3) minus_mult_minus mult_cancel_right1)

end

(*Below, the orientation is 1 since all the cubes are orientation preserving, and they all start
  at the unit box which has a +ve orientation.*)

context R2
begin

abbreviation "valid_division s twoChain \<equiv>
  (\<forall>(orient::int, C)\<in>twoChain. orient = (if (\<forall>x\<in>unit_cube. jac_det C x > 0) then (1::int) else -1)) \<and>
  gen_division s ((cubeImage o snd) ` twoChain) \<and>
  valid_two_chain twoChain"

lemma valid_division_orients: 
  "\<lbrakk>valid_division s two_chain; (orient, C)\<in>two_chain\<rbrakk> \<Longrightarrow>
         orient = (if (\<forall>x\<in>unit_cube. jac_det C x > 0) then 1 else -1)"
  by fastforce

lemma valid_division_then_inj: "valid_division s two_chain \<Longrightarrow> inj_on (cubeImage o snd) two_chain"
  by (auto simp: valid_two_chain_def inj_on_def split: prod.splits)

lemma valid_division_then_finite: "valid_division s two_chain \<Longrightarrow>
  finite ((cubeImage o snd) ` two_chain)"
  by (auto simp: gen_division_def)

lemma valid_division_then_pw_neglig: "valid_division s two_chain \<Longrightarrow>
  pairwise (\<lambda>u t. negligible (u \<inter> t)) ((cubeImage o snd) ` two_chain)"
  by (auto simp: gen_division_def)

end

lemma conjI': "\<lbrakk>\<And>x. x \<in> s \<Longrightarrow> Q; \<And>x. x \<in> s \<Longrightarrow> R\<rbrakk> \<Longrightarrow> x \<in> s \<Longrightarrow> Q \<and> R"
  by auto

locale Green_No_Geom_Chain = Green_No_Geom +
  fixes two_chain ::"(int \<times> ((real \<times> real) \<Rightarrow> (real \<times> real))) set" and
        S (*S can be easily generalised to be a different S for every one cube in the chain, via
            a function that does such a mapping.*)
  assumes
    two_chain_valid:
      "\<And>orient C. (orient, C) \<in> two_chain \<Longrightarrow> \<exists>C_inv. Green_No_Geom_Cube i j C F C_inv S" and
    valid_div: "valid_division s two_chain"
begin

lemma GreenThm_No_Geom_twoChain:
  shows "two_chain_integral two_chain (\<lambda>p. (\<partial> F\<^bsub>j\<^esub> / \<partial> i |\<^bsub>p\<^esub>) - (\<partial> F\<^bsub>i\<^esub> / \<partial> j |\<^bsub>p\<^esub>)) = \<^sup>H\<ointegral>\<^bsub>(\<partial>\<^sup>H two_chain)\<^esub> F" (is "?LHS = ?RHS")
        "(orient,cube) \<in> two_chain \<Longrightarrow> 
           (\<lambda>p. (\<partial> F\<^bsub>j\<^esub> / \<partial> i |\<^bsub>p\<^esub>) - (\<partial> F\<^bsub>i\<^esub> / \<partial> j |\<^bsub>p\<^esub>)) absolutely_integrable_on (cubeImage cube)" (is "?P \<Longrightarrow> ?Q")
proof-
  have pairwise_disjoint_boundaries:
    "\<forall>x\<in> ((boundary o snd) ` two_chain). (\<forall>y\<in> ((boundary o snd) ` two_chain). (x \<noteq> y \<longrightarrow> (x \<inter> y = {})))"
    using valid_div by (fastforce simp add:  image_def valid_two_chain_def pairwise_def)
  have finite_boundaries: "\<forall>B \<in> ((boundary o snd)` two_chain). finite B"
    using valid_div image_iff by (fastforce simp add: valid_two_cube_def valid_two_chain_def)
  have boundary_inj: "inj_on (boundary o snd) two_chain"
    using valid_div
    unfolding valid_two_cube_def valid_two_chain_def pairwise_def inj_on_def
    by (metis (mono_tags) card_0_eq comp_apply finite.emptyI inf.idem prod.case_eq_if zero_neq_numeral)
  have "?LHS =
          (\<Sum>(orient, C)\<in>two_chain. 
             orient * integral (cubeImage C) (\<lambda>p. (\<partial> F\<^bsub>j\<^esub> / \<partial> i |\<^bsub>p\<^esub>) - (\<partial> F\<^bsub>i\<^esub> / \<partial> j |\<^bsub>p\<^esub>)))"
    using valid_div
    by (fastforce intro: Finite_Cartesian_Product.sum_cong_aux simp: two_chain_integral_def)
  also have
    "... = (\<Sum>(orient,C) \<in> two_chain.\<^sup>H\<integral>\<^bsub>\<partial> C\<^esub> F)"
  proof(intro Finite_Cartesian_Product.sum_cong_aux, safe)
    fix orient c
    assume "(orient, c) \<in> two_chain"
    obtain C_inv where "Green_No_Geom_Cube i j c F C_inv S"
      using two_chain_valid[OF \<open>(orient, c) \<in> two_chain\<close>]
      by rule
    thus "real_of_int orient * integral (cubeImage c) (\<lambda>p. (\<partial> F\<^bsub>j\<^esub> / \<partial> i |\<^bsub>p\<^esub>) - (\<partial> F\<^bsub>i\<^esub> / \<partial> j |\<^bsub>p\<^esub>)) = \<^sup>H\<integral>\<^bsub>\<partial> c\<^esub> F"
      using Green_No_Geom_Cube.green_no_geom_less_analytic_assms'''(1)[OF \<open>Green_No_Geom_Cube i j c F C_inv S\<close>]
      by (auto simp add: basis valid_division_orients[OF valid_div \<open>(orient, c) \<in> two_chain\<close>] split: if_splits)
  qed      
  also have "... = sum ((\<lambda>C. \<^sup>H\<integral>\<^bsub>\<partial> C\<^esub> F) o snd) two_chain"
    by(simp add: prod.case_eq_if)
  also have "... = sum (\<lambda>c. \<^sup>H\<integral>\<^bsub>c\<^esub> F) ((boundary o snd) ` two_chain)"
    using sum.reindex[OF boundary_inj,  of "(\<lambda>c. \<^sup>H\<integral>\<^bsub>c\<^esub> F)", symmetric]
    by simp
  also have "... = \<^sup>H\<integral>\<^bsub>\<Union> ((boundary \<circ> snd) ` two_chain)\<^esub> F"
    using sum.Union_disjoint[OF finite_boundaries pairwise_disjoint_boundaries, symmetric]
    by (simp add: one_chain_line_integral_def)
  finally show "?LHS = ?RHS"
    by(auto simp: one_chain_line_integral_def two_chain_boundary_def)
  show "?P \<Longrightarrow> ?Q"
    by(auto simp: basis
            dest!: two_chain_valid Green_No_Geom_Cube.green_no_geom_less_analytic_assms'''(2))
qed

end

lemma integral_Union:
  fixes f :: "'a::euclidean_space \<Rightarrow> 'b::banach"
  assumes "finite (\<T>::'a::euclidean_space set set)"
          "(\<And>S. S \<in> \<T> \<Longrightarrow> f integrable_on S)"
          "pairwise (\<lambda>S S'. negligible (S \<inter> S')) \<T>"
  shows "(integral (\<Union> \<T>) f) = (\<Sum>S\<in>\<T>. integral S f)"
proof-
  have "(f has_integral (integral S f)) S" if "S \<in> \<T>" for S
    using assms(2)[OF that]
    by (simp add: integrable_integral)
  hence "(f has_integral sum (\<lambda>S. integral S f) \<T>) (\<Union> \<T>)"
    using has_integral_Union[where i = "\<lambda>S. integral S f", OF assms(1) _ assms(3)]
    by auto
  thus ?thesis
    by auto
qed

lemma inj_on_snd: "inj_on (f o snd) s \<Longrightarrow> inj_on (\<lambda>(x, y). (g x, f y)) s"
  by (simp add: inj_on_def prod.case_eq_if)

locale Green_No_Geom_div = Green_No_Geom_Chain +
  assumes pos_orient: "\<forall>(orient::int, C)\<in>two_chain. orient = 1"
begin

lemma GreenThm_No_Geom_divisible:
  "integral s (\<lambda>p. (\<partial> F\<^bsub>j\<^esub> / \<partial> i |\<^bsub>p\<^esub>) - (\<partial> F\<^bsub>i\<^esub> / \<partial> j |\<^bsub>p\<^esub>)) = \<^sup>H\<ointegral>\<^bsub>(\<partial>\<^sup>H two_chain)\<^esub> F" (is "_ = ?RHS")
proof -
  let ?F = " (\<lambda>p. (\<partial> F\<^bsub>j\<^esub> / \<partial> i |\<^bsub>p\<^esub>) - (\<partial> F\<^bsub>i\<^esub> / \<partial> j |\<^bsub>p\<^esub>))"
  have "?RHS = two_chain_integral two_chain ?F"
    using GreenThm_No_Geom_twoChain(1)[symmetric] .
  also have "... = (\<Sum>(orient, C)\<in>two_chain. orient * integral (cubeImage C) ?F)"
    using valid_div
    by (fastforce intro: Finite_Cartesian_Product.sum_cong_aux simp: two_chain_integral_def)
  also have "... = (\<Sum>(orient, C)\<in>two_chain. integral (cubeImage C) ?F)"
    using pos_orient
    by (auto intro!: Finite_Cartesian_Product.sum_cong_aux)
  also have "... = sum ((\<lambda>imgC. integral imgC ?F) o (cubeImage o snd)) two_chain"
    by (simp add: prod.case_eq_if)
  also have "... = (\<Sum>imgC\<in>((cubeImage o snd) ` two_chain). integral imgC ?F)"
    using sum.reindex[OF valid_division_then_inj[OF valid_div], symmetric]
    by simp
  also have "... = integral (\<Union>C\<in>((cubeImage o snd) ` two_chain). C) ?F"
    using valid_division_then_pw_neglig[OF valid_div] valid_division_then_finite[OF valid_div]
    by(auto intro!: set_lebesgue_integral_eq_integral GreenThm_No_Geom_twoChain(2)
                    integral_Union[symmetric])
  also have "... = integral s ?F"
    using valid_div 
    unfolding gen_division_def
    by smt
  finally show ?thesis
    by simp
qed

end

lemma higher_differentiable_on_then_C1_differentiable_on':
  assumes "higher_differentiable_on S f 1"
  shows "C1_differentiable_on' f S"
proof-
  have "\<And>x. x\<in>S \<Longrightarrow> f differentiable (at x)"
    using assms(1)
    by (auto simp: eval_nat_numeral higher_differentiable_on.simps)
  hence "\<And>x. x\<in>S \<Longrightarrow> (f has_derivative (frechet_derivative f (at x))) (at x)"
    by (simp add: frechet_derivative_works)
  moreover have "(\<And>v. continuous_on S (\<lambda>x. frechet_derivative f (at x) v))"
    using assms(1)
    by (auto simp: eval_nat_numeral higher_differentiable_on.simps
             intro: higher_differentiable_on_imp_continuous_on[where n = 1])
  hence "continuous_on S (\<lambda>x v. frechet_derivative f (at x) v)"
    by (simp add: continuous_on_coordinatewise_then_product')
  ultimately show ?thesis
    by(fastforce simp add: C1_differentiable_on'_def)
qed

lemma higher_differentiable_on_then_partial_C1_differentiable_on':
  assumes "higher_differentiable_on UNIV f 2" "v \<bullet> v = 1"
  shows "C1_differentiable_on' (\<partial>f / \<partial>v) UNIV"
proof-
  have "\<forall>v. (\<forall>x. (\<lambda>x. frechet_derivative f (at x) v) differentiable at x)"
    using assms(1)
    by (auto simp: eval_nat_numeral higher_differentiable_on.simps)
  hence "\<forall>v. \<forall>x. ((\<lambda>x. frechet_derivative f (at x) v) has_derivative 
                   (frechet_derivative (\<lambda>x. frechet_derivative f (at x) v) (at x))) (at x)"
    by (simp add: frechet_derivative_works)
  moreover have "(\<forall>v v'. \<forall>x. continuous_on UNIV
                              (\<lambda>x. frechet_derivative (\<lambda>x. frechet_derivative f (at x) v) (at x) v'))"
    using assms(1)
    by (auto simp: eval_nat_numeral higher_differentiable_on.simps
             intro: higher_differentiable_on_imp_continuous_on)
  hence "\<And>v. \<forall>x. continuous_on UNIV
                              (\<lambda>x. frechet_derivative (\<lambda>x. frechet_derivative f (at x) v) (at x))"
    by (simp add: continuous_on_coordinatewise_then_product')
  ultimately have "C1_differentiable_on' (\<lambda>x. frechet_derivative f (at x) v) UNIV"
    by(force simp add: C1_differentiable_on'_def)
  moreover have "has_partial_vector_derivative f v (frechet_derivative f (at x) v) x"
    for x
    using assms
    by(auto simp: higher_differentiable_on.simps eval_nat_numeral frechet_derivative_works
            dest: has_derivative_to_has_partial_deriv'')
  from partial_vector_derivative_works_2[OF this]
  have "(\<lambda>x. frechet_derivative f (at x) v) = (\<partial> f / \<partial> v)"
    by(auto intro: HOL.ext)
  ultimately show ?thesis
    by simp
qed

lemma C1_differentiable_on'_partial_deriv_inner: 
  fixes f :: "'a::euclidean_space \<Rightarrow> 'b::euclidean_space" and
        b :: 'b
  assumes "b \<in> Basis" "C1_differentiable_on' f S" "p \<in> S"
  shows  "(\<partial> f\<^bsub>b\<^esub> / \<partial> v |\<^bsub>p\<^esub>) = ((\<partial> f / \<partial> v)\<^bsub>b\<^esub>) p"
proof-
  obtain f' where "(f has_derivative f') (at p)"
    using assms
    by (auto simp add: C1_differentiable_on'_def)

  hence "has_partial_vector_derivative f v (f' v) p"
    by(simp add: has_derivative_to_has_partial_deriv'')

  thus ?thesis
    by(auto dest: partial_vector_derivative_inner[OF _])
qed

context R2
begin

lemma higher_differentiable_on_then_C1_differentiable_on'_ij:
  assumes "higher_differentiable_on UNIV f 2"
  shows "C1_differentiable_on' f UNIV" (is ?g1)
    "C1_differentiable_on' ((\<partial> f\<^bsub>j\<^esub> / \<partial> j)) UNIV" (is ?g2)
    "C1_differentiable_on' ((\<partial> f\<^bsub>j\<^esub> / \<partial> i)) UNIV" (is ?g3)
    "C1_differentiable_on' ((\<partial> f\<^bsub>i\<^esub> / \<partial> i)) UNIV" (is ?g4)
    "C1_differentiable_on' ((\<partial> f\<^bsub>i\<^esub> / \<partial> j)) UNIV" (is ?g5)
proof-
  show ?g1
    using assms
    by (auto simp: eval_nat_numeral dest: higher_differentiable_on_SucD
             intro!: higher_differentiable_on_then_C1_differentiable_on')
  hence "(\<partial> f\<^bsub>b\<^esub> / \<partial> v) = ((\<partial> f / \<partial> v)\<^bsub>b\<^esub>)" if "b \<in> Basis" for b v
    using C1_differentiable_on'_partial_deriv_inner[OF that]
    by fastforce
  then show ?g2 ?g3 ?g4 ?g5
    using basis assms
    by (auto intro!: higher_differentiable_on_then_partial_C1_differentiable_on'
                     C1_diff'_imp_components)
qed

lemma smooth_then_C1_differentiable_on'_ij:
  assumes "2-smooth_on UNIV f"
  shows "C1_differentiable_on' f UNIV" (is ?g1)
    "C1_differentiable_on' ((\<partial> f\<^bsub>j\<^esub> / \<partial> j)) UNIV" (is ?g2)
    "C1_differentiable_on' ((\<partial> f\<^bsub>j\<^esub> / \<partial> i)) UNIV" (is ?g3)
    "C1_differentiable_on' ((\<partial> f\<^bsub>i\<^esub> / \<partial> i)) UNIV" (is ?g4)
    "C1_differentiable_on' ((\<partial> f\<^bsub>i\<^esub> / \<partial> j)) UNIV" (is ?g5)
  by (auto simp add: numeral_eq_enat 
           intro!: higher_differentiable_on_then_C1_differentiable_on'_ij
                   smooth_onD assms)

lemma jac_det_id:
  "jac_det id x = 1"
  unfolding jac_det_def
  by (auto simp add: i_is_x_axis j_is_y_axis id_def)

lemma jac_det_eq:
  assumes  "(\<And>u v. (\<partial>(f\<^bsub>u\<^esub>) / \<partial>v|\<^bsub>x\<^esub>) = (\<partial>(g\<^bsub>u\<^esub>) / \<partial>v|\<^bsub>x\<^esub>))"
  shows "jac_det f x = jac_det g x"
proof-
  have *: "a = l \<Longrightarrow> b = m \<Longrightarrow> a - b = l - m" for a b l m
    by auto
  have **: "a = l \<Longrightarrow> b = m \<Longrightarrow> a * b = l * m" for a b l m
    by auto
  show ?thesis
    unfolding jac_det_def
    apply(intro * **)
    using assms
    by auto
qed

lemma jac_det_nzero:
  assumes
      [intro]: "(f has_derivative f') (at x)" and
      [intro]: "(f_inv has_derivative f_inv') (at (f x))" and
      "\<And>x. f_inv (f x) = x"
  shows "jac_det f x \<noteq> 0"
proof-
  have *: "((f_inv o f) has_derivative (f_inv' o f')) (at x)"
    by(auto intro!: derivative_eq_intros)
  moreover hence **: "(id has_derivative (f_inv' o f')) (at x)"
    using assms(3)
    apply(intro has_derivative_transform[where f = "(f_inv o f)" and g = id])
    by(auto intro!: derivative_eq_intros)
  ultimately have "\<partial> (f_inv o f) / \<partial>v |\<^bsub>x\<^esub> = \<partial> id / \<partial>v |\<^bsub>x\<^esub>" for v
    by(intro partial_derivative_eq[where F_u = "(f_inv' o f') v"] partial_derivative_intros)+
  have "\<partial> ((f_inv o f)\<^bsub>u\<^esub>) / \<partial>v |\<^bsub>x\<^esub> = \<partial> (id\<^bsub>u\<^esub>) / \<partial>v |\<^bsub>x\<^esub>" for v u
    apply(intro partial_derivative_eq_inner[where F_u = "(f_inv' o f') v"]
                partial_derivative_intros * **)
    .
  hence "jac_det (f_inv o f) x = 1"
    unfolding jac_det_id[of x, symmetric]
    apply(intro jac_det_eq)
    by auto
  thus ?thesis
    by (auto simp: det_compose[OF assms(2,1)])
qed

end

locale Green_No_Geom_Cube_C2 = Green_No_Geom + 
  fixes C_inv::"(real \<times> real) \<Rightarrow> (real \<times> real)" and S
  assumes cube_is_C2: 
    "2-smooth_on UNIV C" and
    cube_has_four_sides: "valid_two_cube C" and 
    cube_has_an_inverse:
    "\<And>x. x \<in> (unit_cube - S) \<Longrightarrow> C_inv (C x) = x"
    "continuous_on (C ` (unit_cube - S)) C_inv"
    "negligible S" and
  orientation_preserving_cube:
    "\<And>x. x \<in> unit_cube \<Longrightarrow> jac_det C x \<noteq> 0"
begin

sublocale Green_No_Geom_Cube
  apply unfold_locales
  using smooth_then_C1_differentiable_on'_ij[OF cube_is_C2] cube_has_four_sides cube_has_an_inverse
        orientation_preserving_cube
  by auto

end


(*lemma "continuous_on UNIV f \<Longrightarrow> homeomorphism UNIV s f g \<Longrightarrow> s = UNIV"
  using iffD1[OF injective_into_1d_eq_homeomorphism[OF _ path_connected_UNIV, symmetric]]
  by auto
*)

lemma inj_on_valid_cube:
  assumes "inj_on f unit_cube"
  shows "valid_two_cube f"
proof-
  have "f (1, 0.5) \<noteq> f (0.5, 0)" "f (0, 0.5) \<noteq> f (0.5, 1)"
    using assms
    by (fastforce simp: inj_on_def)+
  thus ?thesis
    using assms
    by (auto simp: inj_on_def valid_two_cube_def boundary_def fun_eq_iff
             intro!: card_4_intro)
qed

lemma path_connected_unit_cube: "path_connected unit_cube"
  by (simp add: convex_imp_path_connected)

lemma homeomorphism_inj_on:
  "\<lbrakk>continuous_on s f; path_connected s; homeomorphism s t f f_inv\<rbrakk> \<Longrightarrow> inj_on f s"
  by (metis homeomorphism_def inj_onI)

find_theorems homeomorphism diffeomorphism

locale Green_No_Geom_Cube_diffeomorphism = Green_No_Geom +
  fixes C_inv::"(real \<times> real) \<Rightarrow> (real \<times> real)"
  assumes cube_is_diffeo:
    "diffeomorphism 2 UNIV UNIV C C_inv"
begin

lemma valid_two_cube: "valid_two_cube C"
  using diffeomorphism_imp_homeomorphism[OF cube_is_diffeo]
        inj_on_valid_cube cube_is_diffeo
  by (metis UNIV_I homeomorphism_apply1 inj_on_inverseI)

lemma cont_C_inv: "continuous_on (C ` s) C_inv"
proof-
  have "continuous_on (C ` UNIV) C_inv"
    using diffeomorphismD[OF cube_is_diffeo]
    by (auto intro!: smooth_on_imp_continuous_on)
  moreover have "C ` s \<subseteq> (C ` UNIV)"
    by auto
  ultimately show ?thesis
    by (rule continuous_on_subset)
qed

lemma jac_det_C_zero: assumes "x \<in> range C " shows " jac_det C x \<noteq> 0"
proof-
  obtain C' where "(C has_derivative C') (at x)"
    using diffeomorphismD[OF cube_is_diffeo]
    by (meson C1_differentiable_on'_has_derivative UNIV_I smooth_then_C1_differentiable_on'_ij(1))
  moreover have "C x \<in> range C"
    by auto    
  then obtain C_inv' where "(C_inv has_derivative C_inv') (at (C x))"
    using diffeomorphismD[OF cube_is_diffeo] C1_differentiable_on'_has_derivative UNIV_I
          smooth_then_C1_differentiable_on'_ij
    by metis    
  ultimately show ?thesis
    apply (intro jac_det_nzero[where f' = C' and f_inv' = C_inv' and f_inv = C_inv])
    using diffeomorphismD[OF cube_is_diffeo]
    by auto
qed

sublocale Green_No_Geom_Cube_C2 i j C F C_inv "{}"
  apply unfold_locales
  using smooth_then_C1_differentiable_on'_ij diffeomorphismD[OF cube_is_diffeo]
        cont_C_inv jac_det_C_zero valid_two_cube
  by auto

end


end