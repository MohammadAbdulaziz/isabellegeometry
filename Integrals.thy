theory Integrals
  imports General_Utils transfer_pair_to_Vec "HOL-Library.Rewrite"
begin

lemma gauge_integral_Fubini_universe_x_set:
  fixes f :: "('a::euclidean_space * 'b::euclidean_space) \<Rightarrow> 'c::euclidean_space"
  assumes fun_lesbegue_integrable: "set_integrable lborel (s \<times> t) f" and
    x_axis_integral_measurable: "set_integrable lborel s (\<lambda>x. LBINT y:t. f (x, y))" "\<forall>x\<in>s. set_integrable lborel t (\<lambda>xa. f (x, xa))"
  shows "integral (s \<times> t) f = integral s (\<lambda>x. integral t (\<lambda>y. f(x,y)))"
    "(\<lambda>x. integral t (\<lambda>y. f(x,y))) integrable_on s"
proof -
  have fun_lborel_prod_integrable:
    "integrable lborel (%pr. indicator (s \<times> t) pr *\<^sub>R f pr)"
    using fun_lesbegue_integrable[unfolded set_integrable_def]
    using lborel_prod
    by blast
  then have fun_lborel_prod_integrable:
    "integrable (lborel \<Otimes>\<^sub>M lborel) (%pr. indicator (s \<times> t) pr *\<^sub>R f pr)"
    unfolding lborel_prod by auto
  then have region_integral_is_one_twoD_integral:
    "LBINT x. LBINT y. indicator (s \<times> t) (x,y) *\<^sub>R f (x, y) = integral\<^sup>L (lborel \<Otimes>\<^sub>M lborel) (%pr. indicator (s \<times> t) pr *\<^sub>R f pr)"
    using lborel_pair.integral_fst'
    by auto
  then have AE_one_D_integrals_eq: "AE x in lborel. LBINT y. indicator (s \<times> t) (x,y) *\<^sub>R f (x, y) = integral UNIV (\<lambda>y. indicator (s \<times> t) (x,y) *\<^sub>R f (x, y))"
  proof -
    have "AE x in lborel. integrable lborel (\<lambda>y. indicator (s \<times> t) (x,y) *\<^sub>R f (x, y))"
      using lborel_pair.AE_integrable_fst' and fun_lborel_prod_integrable
      by blast
    then show ?thesis
      using integral_lborel and always_eventually
        and AE_mp
      by fastforce
  qed
  have integral_indic_to_times: "LBINT x:s. (LBINT y:t. f (x, y)) = LBINT x. LBINT y. indicat_real (s \<times> t) (x, y) *\<^sub>R f (x, y)"
    unfolding set_lebesgue_integral_def
    apply(rule Bochner_Integration.integral_cong)
    subgoal by simp
    subgoal for x
      unfolding integral_scaleR_right[symmetric]
      apply(rule Bochner_Integration.integral_cong)
      by (simp add: indicator_def)+
    done
  have i: "integrable lborel (\<lambda>x. LBINT y.  indicator (s \<times> t) (x,y) *\<^sub>R f (x, y))"
    using fun_lborel_prod_integrable and lborel_pair.integrable_fst'
    by auto
  have ii: "integrable (lborel \<Otimes>\<^sub>M lborel) (%(x,y). indicator (s \<times> t) (x,y) *\<^sub>R f (x,y))"
    by (simp add: fun_lesbegue_integrable[unfolded set_integrable_def] integrable_cong lborel_prod)
  have "set_integrable lborel s (\<lambda>x. (LBINT y:t. f (x, y))) = set_integrable lborel s (\<lambda>x. integral t (\<lambda>y. f (x, y)))"
    using x_axis_integral_measurable(2)
    by(auto intro!: Bochner_Integration.integrable_cong set_borel_integral_eq_integral(2) simp add: set_integrable_def)+
  then show one_D_gauge_integral_integrable:
    "(\<lambda>x. integral t (\<lambda>y. f(x,y))) integrable_on s"
    apply(auto intro!: set_borel_integral_eq_integral(1))
    using x_axis_integral_measurable by auto
  have "LBINT x:s. (LBINT y:t. f (x, y)) = integral s (\<lambda>x. (LBINT y:t. f (x, y)))"
    using x_axis_integral_measurable 
    by (simp add: set_borel_integral_eq_integral(2))
  moreover have "integral s (\<lambda>x. (LBINT y:t. f (x, y))) = integral s (\<lambda>x. integral t (\<lambda>y. f(x, y)))"
    apply(rule integral_cong, rule set_borel_integral_eq_integral)
    using x_axis_integral_measurable
    by auto
  ultimately have twoD_lesbeuge_eq_twoD_gauge:
    "LBINT x:s. (LBINT y:t. f (x, y)) = integral s (\<lambda>x. integral t (\<lambda>y. f(x, y)))"
    by auto
  show "integral (s \<times> t) f = integral s (\<lambda>x. integral t (\<lambda>y. f(x,y)))"
    apply(subst set_borel_integral_eq_integral(2)[symmetric])
    subgoal using fun_lesbegue_integrable .
    subgoal using twoD_lesbeuge_eq_twoD_gauge integral_indic_to_times region_integral_is_one_twoD_integral
      unfolding set_lebesgue_integral_def
      by (simp add: lborel_prod)
    done
qed

lemma pair_integrable_to_single_integrable:
      assumes "set_integrable (lborel) (s \<times> t) (f::('a::{euclidean_space,second_countable_topology}*'b::{euclidean_space,second_countable_topology}) \<Rightarrow> 'c::{banach,second_countable_topology})"
              "s \<in> sets lborel"
      shows "set_integrable lborel s (\<lambda>x. LBINT y:t. f (x, y))" 
            "AE x \<in>s in lborel. integrable lborel (\<lambda>y. indicat_real t y *\<^sub>R f (x, y))" (*Can this be made into "\<forall> x \<in>s. integrable lborel (\<lambda>y. indicat_real t y *\<^sub>R f (x, y))"*)
proof-
  have "\<And>s x. indicat_real s x * indicat_real s x = indicat_real s x" by (auto simp add: indicator_def)
  have i: "integrable (lborel \<Otimes>\<^sub>M lborel) (%pr. indicat_real (s \<times> t) pr *\<^sub>R f pr)"
    by (simp add: assms[unfolded set_integrable_def] integrable_cong lborel_prod)
  have "integrable lborel (\<lambda>x. LBINT y. indicat_real s x *\<^sub>R (indicat_real (s \<times> t) (x, y) *\<^sub>R f (x, y)))"
    using integrable_mult_indicator[OF assms(2) lborel_pair.integrable_fst'[OF i]]
    by (smt Bochner_Integration.integral_cong integrable_cong integral_scaleR_right)
  moreover have "integrable lborel (\<lambda>x. LBINT y. indicat_real s x *\<^sub>R (indicat_real (s \<times> t) (x, y) *\<^sub>R f (x, y))) = set_integrable lborel s (\<lambda>x. LBINT y:t. (f (x, y)))"
    unfolding set_integrable_def set_lebesgue_integral_def
    apply(auto intro!: Bochner_Integration.integrable_cong)
    by (simp add: indicator_def)
  ultimately show "set_integrable lborel s (\<lambda>x. LBINT y:t. f (x, y))"
    by auto
  have ii: "integrable (lborel \<Otimes>\<^sub>M lborel) (%(x,y). indicator (s \<times> t) (x,y) *\<^sub>R f (x,y))"
    by (simp add: assms(1)[unfolded set_integrable_def] integrable_cong lborel_prod)
  have iii: "AE x in lborel. integrable lborel (%y. indicat_real (s \<times> t) (x, y) *\<^sub>R f (x, y))"
    using lborel_pair.AE_integrable_fst[OF ii]
    by (metis (no_types, lifting) AE_I2 ii lborel_prod)
  have "\<And>x. integrable lborel (\<lambda>y. indicat_real (s \<times> t) (x, y) *\<^sub>R f (x, y)) \<Longrightarrow> (x\<in>s \<longrightarrow> (integrable lborel (\<lambda>y. indicat_real s x *\<^sub>R indicat_real t y *\<^sub>R f (x, y))))"
    apply(auto intro!: Bochner_Integration.integrable_cong)
    by (simp add: indicator_def)
  then show "AE x \<in>s in lborel. integrable lborel (\<lambda>y. indicat_real t y *\<^sub>R f (x, y))"
    by(auto intro: Measure_Space.AE_mp[OF iii])
qed

lemma gauge_integral_Fubini_universe_x_set':
  fixes f :: "('a::euclidean_space * 'b::euclidean_space) \<Rightarrow> 'c::euclidean_space"
  assumes "set_integrable lborel (s \<times> t) f"  "s \<in> sets lborel"
          "\<forall>x\<in>s. set_integrable lborel t (\<lambda>xa. f (x, xa))"
  shows "integral (s \<times> t) f = integral s (\<lambda>x. integral t (\<lambda>y. f(x,y)))"
    "(\<lambda>x. integral t (\<lambda>y. f(x,y))) integrable_on s"
  using gauge_integral_Fubini_universe_x_set[OF assms(1) _ assms(3)] pair_integrable_to_single_integrable(1)[OF assms(1,2)]
  by blast+

lemma gauge_integral_Fubini_universe_x:
  fixes f :: "('a::euclidean_space * 'b::euclidean_space) \<Rightarrow> 'c::euclidean_space"
  assumes fun_lesbegue_integrable: "integrable lborel f" and
    x_axis_integral_measurable: "(\<lambda>x. integral UNIV (\<lambda>y. f(x, y))) \<in> borel_measurable lborel"
  shows "integral UNIV f = integral UNIV (\<lambda>x. integral UNIV (\<lambda>y. f(x,y)))"
    "(\<lambda>x. integral UNIV (\<lambda>y. f(x,y))) integrable_on UNIV"
proof -
  have f_is_measurable: "f \<in> borel_measurable lborel"
    using fun_lesbegue_integrable and borel_measurable_integrable
    by auto
  have fun_lborel_prod_integrable:
    "integrable (lborel \<Otimes>\<^sub>M lborel) f"
    using fun_lesbegue_integrable
    by (simp add: lborel_prod)
  then have region_integral_is_one_twoD_integral:
    "LBINT x. LBINT y. f (x, y) = integral\<^sup>L (lborel \<Otimes>\<^sub>M lborel) f"
    using lborel_pair.integral_fst'
    by auto
  then have AE_one_D_integrals_eq: "AE x in lborel. LBINT y. f (x, y) = integral UNIV (\<lambda>y. f(x,y))"
  proof -
    have "AE x in lborel. integrable lborel (\<lambda>y. f(x,y))"
      using lborel_pair.AE_integrable_fst' and fun_lborel_prod_integrable
      by blast
    then show ?thesis
      using integral_lborel  and always_eventually
        and AE_mp
      by fastforce
  qed
  have one_D_integral_measurable:
    "(\<lambda>x. LBINT y. f (x, y)) \<in> borel_measurable lborel"
    using f_is_measurable and lborel.borel_measurable_lebesgue_integral
    by auto
  then have second_lesbegue_integral_eq:
    "LBINT x. LBINT y. f (x, y) = LBINT x. (integral UNIV (\<lambda>y. f(x,y)))"
    using x_axis_integral_measurable and integral_cong_AE and AE_one_D_integrals_eq
    by blast
  have "integrable lborel (\<lambda>x. LBINT y. f (x, y))"
    using fun_lborel_prod_integrable and lborel_pair.integrable_fst'
    by auto
  then have oneD_gauge_integral_lesbegue_integrable:
    "integrable lborel (\<lambda>x. integral UNIV (\<lambda>y. f(x,y)))"
    using x_axis_integral_measurable and AE_one_D_integrals_eq and integrable_cong_AE_imp
    by blast
  then show one_D_gauge_integral_integrable:
    "(\<lambda>x. integral UNIV (\<lambda>y. f(x,y))) integrable_on UNIV"
    using integrable_on_lborel
    by auto
  have "LBINT x. (integral UNIV (\<lambda>y. f(x,y))) = integral UNIV (\<lambda>x. integral UNIV (\<lambda>y. f(x, y)))"
    using integral_lborel oneD_gauge_integral_lesbegue_integrable
    by fastforce
  then have twoD_lesbeuge_eq_twoD_gauge:
    "LBINT x. LBINT y. f (x, y) = integral UNIV (\<lambda>x. integral UNIV (\<lambda>y. f(x, y)))"
    using second_lesbegue_integral_eq
    by auto
  then show "integral UNIV f = integral UNIV (\<lambda>x. integral UNIV (\<lambda>y. f(x,y)))"
    using fun_lesbegue_integrable and integral_lborel and region_integral_is_one_twoD_integral
    by (metis lborel_prod)
qed

lemma gauge_integral_Fubini_universe_y:
  fixes f :: "('a::euclidean_space * 'b::euclidean_space) \<Rightarrow> 'c::euclidean_space"
  assumes fun_lesbegue_integrable: "integrable lborel f" and
    y_axis_integral_measurable: "(\<lambda>x. integral UNIV (\<lambda>y. f(y, x))) \<in> borel_measurable lborel"
  shows "integral UNIV f = integral UNIV (\<lambda>x. integral UNIV (\<lambda>y. f(y, x)))"
    "(\<lambda>x. integral UNIV (\<lambda>y. f(y, x))) integrable_on UNIV"
proof -
  have f_is_measurable: "f \<in> borel_measurable lborel"
    using fun_lesbegue_integrable and borel_measurable_integrable
    by auto
  have fun_lborel_prod_integrable:
    "integrable (lborel \<Otimes>\<^sub>M lborel) f"
    using fun_lesbegue_integrable
    by (simp add: lborel_prod)
  then have region_integral_is_one_twoD_integral:
    "LBINT x. LBINT y. f (y, x) = integral\<^sup>L (lborel \<Otimes>\<^sub>M lborel) f"
    using lborel_pair.integral_fst'
      f_is_measurable lborel_pair.integrable_product_swap lborel_pair.integral_fst lborel_pair.integral_product_swap lborel_prod
    by force
  then have AE_one_D_integrals_eq: "AE x in lborel. LBINT y. f (y, x) = integral UNIV (\<lambda>y. f(y,x))"
  proof -
    have "AE x in lborel. integrable lborel (\<lambda>y. f(y,x))"
      using lborel_pair.AE_integrable_fst' and fun_lborel_prod_integrable
        lborel_pair.AE_integrable_fst lborel_pair.integrable_product_swap
      by blast
    then show ?thesis
      using integral_lborel  and always_eventually
        and AE_mp
      by fastforce
  qed
  have one_D_integral_measurable:
    "(\<lambda>x. LBINT y. f (y, x)) \<in> borel_measurable lborel"
    using f_is_measurable and lborel.borel_measurable_lebesgue_integral
    by auto
  then have second_lesbegue_integral_eq:
    "LBINT x. LBINT y. f (y, x) = LBINT x. (integral UNIV (\<lambda>y. f(y, x)))"
    using y_axis_integral_measurable and integral_cong_AE and AE_one_D_integrals_eq
    by blast
  have "integrable lborel (\<lambda>x. LBINT y. f (y, x))"
    using fun_lborel_prod_integrable and lborel_pair.integrable_fst'
    by (simp add: lborel_pair.integrable_fst lborel_pair.integrable_product_swap)
  then have oneD_gauge_integral_lesbegue_integrable:
    "integrable lborel (\<lambda>x. integral UNIV (\<lambda>y. f(y, x)))"
    using y_axis_integral_measurable and AE_one_D_integrals_eq and integrable_cong_AE_imp
    by blast
  then show one_D_gauge_integral_integrable:
    "(\<lambda>x. integral UNIV (\<lambda>y. f(y, x))) integrable_on UNIV"
    using integrable_on_lborel
    by auto
  have "LBINT x. (integral UNIV (\<lambda>y. f(y, x))) = integral UNIV (\<lambda>x. integral UNIV (\<lambda>y. f(y, x)))"
    using integral_lborel oneD_gauge_integral_lesbegue_integrable
    by fastforce
  then have twoD_lesbeuge_eq_twoD_gauge:
    "LBINT x. LBINT y. f (y, x) = integral UNIV (\<lambda>x. integral UNIV (\<lambda>y. f(y, x)))"
    using second_lesbegue_integral_eq
    by auto
  then show "integral UNIV f = integral UNIV (\<lambda>x. integral UNIV (\<lambda>y. f(y, x)))"
    using fun_lesbegue_integrable and integral_lborel and region_integral_is_one_twoD_integral
    by (metis lborel_prod)
qed

lemma gauge_integral_Fubini_curve_bounded_region_x:
  fixes f g :: "('a::euclidean_space * 'b::euclidean_space) \<Rightarrow> 'c::euclidean_space" and
    g1 g2:: "'a \<Rightarrow> 'b" and
    s:: "('a * 'b) set"
  assumes fun_lesbegue_integrable: "integrable lborel f" and
    x_axis_gauge_integrable: "\<And>x. (\<lambda>y. f(x, y)) integrable_on UNIV" and
    (*IS THIS redundant? NO IT IS NOT*)
    x_axis_integral_measurable: "(\<lambda>x. integral UNIV (\<lambda>y. f(x, y))) \<in> borel_measurable lborel" and
    f_is_g_indicator: "f = (\<lambda>x. if x \<in> s then g x else 0)" and
    s_is_bounded_by_g1_and_g2: "s = {(x,y). (\<forall>i\<in>Basis. a \<bullet> i \<le> x \<bullet> i \<and> x \<bullet> i \<le> b \<bullet> i) \<and>
                                      (\<forall>i\<in>Basis. (g1 x) \<bullet> i \<le> y \<bullet> i \<and> y \<bullet> i \<le> (g2 x) \<bullet> i)}"
  shows "integral s g = integral (cbox a b) (\<lambda>x. integral (cbox (g1 x) (g2 x)) (\<lambda>y. g(x,y)))"
proof -
  have two_D_integral_to_one_D: "integral UNIV f = integral UNIV (\<lambda>x. integral UNIV (\<lambda>y. f(x,y)))"
    using gauge_integral_Fubini_universe_x and fun_lesbegue_integrable and x_axis_integral_measurable
    by auto
  have one_d_integral_integrable: "(\<lambda>x. integral UNIV (\<lambda>y. f(x,y))) integrable_on UNIV"
    using gauge_integral_Fubini_universe_x(2) and assms
    by blast
  have case_x_in_range:
    "\<forall> x \<in> cbox a b. integral (cbox (g1 x) (g2 x)) (\<lambda>y. g(x,y)) = integral UNIV (\<lambda>y. f(x,y))"
  proof
    fix x:: 'a
    assume within_range: "x \<in> (cbox a b)"
    let ?f_one_D_spec = "(\<lambda>y. if y \<in> (cbox (g1 x) (g2 x)) then g(x,y) else 0)"
    have f_one_D_region: "(\<lambda>y. f(x,y)) = (\<lambda>y. if y \<in> cbox (g1 x) (g2 x) then g(x,y) else 0)"
    proof
      fix y::'b
      show "f (x, y) = (if y \<in> (cbox (g1 x) (g2 x)) then g (x, y) else 0)"
        apply (simp add: f_is_g_indicator s_is_bounded_by_g1_and_g2)
        using within_range
        apply (simp add: cbox_def)
        by smt
    qed
    have zero_out_of_bound: "\<forall> y.  y \<notin> cbox (g1 x) (g2 x) \<longrightarrow> f (x,y) = 0"
      using f_is_g_indicator and s_is_bounded_by_g1_and_g2
      by (auto simp add: cbox_def)
    have "(\<lambda>y. f(x,y)) integrable_on cbox (g1 x)  (g2 x)"
    proof -
      have "?f_one_D_spec integrable_on UNIV"
        using f_one_D_region and x_axis_gauge_integrable
        by metis
      then have "?f_one_D_spec integrable_on cbox(g1 x) (g2 x)"
        using integrable_on_subcbox
        by blast
      then show ?thesis using f_one_D_region  by auto
    qed
    then have f_integrale_x: "((\<lambda>y. f(x,y)) has_integral (integral (cbox (g1 x) (g2 x)) (\<lambda>y. f(x,y)))) (cbox (g1 x) (g2 x))"
      using integrable_integral and within_range and x_axis_gauge_integrable
      by auto
    have "integral (cbox (g1 x)  (g2 x)) (\<lambda>y. f (x, y)) = integral UNIV (\<lambda>y. f (x, y))"
      using has_integral_on_superset[OF f_integrale_x _ Set.subset_UNIV] zero_out_of_bound
      by (simp add: integral_unique)
    then have "((\<lambda>y. f(x, y)) has_integral integral UNIV (\<lambda>y. f (x, y))) (cbox (g1 x) (g2 x))"
      using f_integrale_x
      by simp
    then have "((\<lambda>y. g(x, y)) has_integral integral UNIV (\<lambda>y. f (x, y))) (cbox (g1 x)(g2 x))"
      using  Henstock_Kurzweil_Integration.has_integral_restrict [OF subset_refl ] and
        f_one_D_region
      by (smt has_integral_eq)
    then show "integral (cbox (g1 x) (g2 x)) (\<lambda>y. g (x, y)) = integral UNIV (\<lambda>y. f (x, y))"
      by auto
  qed
  have case_x_not_in_range:
    "\<forall> x. x \<notin> cbox a  b \<longrightarrow> integral UNIV (\<lambda>y. f(x,y)) = 0"
  proof
    fix x::'a
    have "x \<notin> (cbox a b) \<longrightarrow> (\<forall>y. f(x,y) = 0)"
      apply  (simp add: s_is_bounded_by_g1_and_g2 f_is_g_indicator cbox_def)
      by auto
    then show "x \<notin> cbox a b \<longrightarrow> integral UNIV (\<lambda>y. f (x, y)) = 0"
      by (simp add: integral_0)
  qed
  have RHS: "integral UNIV (\<lambda>x. integral UNIV (\<lambda>y. f(x,y))) = integral (cbox a b) (\<lambda>x. integral (cbox (g1 x) (g2 x)) (\<lambda>y. g(x,y)))"
  proof -
    let ?first_integral = "(\<lambda>x. integral (cbox (g1 x) (g2 x)) (\<lambda>y. g(x,y)))"
    let ?x_integral_cases = "(\<lambda>x. if x \<in> cbox a b then  ?first_integral x else 0)"
    have x_integral_cases_integral: "(\<lambda>x. integral UNIV (\<lambda>y. f(x,y))) = ?x_integral_cases"
      using case_x_in_range and case_x_not_in_range
      by auto
    have "((\<lambda>x. integral UNIV (\<lambda>y. f(x,y))) has_integral (integral UNIV f)) UNIV"
      using two_D_integral_to_one_D
        one_d_integral_integrable
      by auto
    then have "(?x_integral_cases has_integral (integral UNIV f)) UNIV"
      using x_integral_cases_integral by auto
    then have "(?first_integral has_integral (integral UNIV f)) (cbox a b)"
      using  has_integral_restrict_UNIV[of "cbox a b" "?first_integral" "integral UNIV f"]
      by auto
    then show ?thesis
      using two_D_integral_to_one_D
      by (simp add: integral_unique)
  qed
  have f_integrable:"f integrable_on UNIV"
    using fun_lesbegue_integrable and integrable_on_lborel
    by auto
  then have LHS: "integral UNIV f = integral s g"
    apply (simp add: f_is_g_indicator)
    using integrable_restrict_UNIV
      integral_restrict_UNIV
    by auto
  then show ?thesis
    using RHS and two_D_integral_to_one_D
    by auto
qed

(*lemma gauge_integral_Fubini_universe_x_euclid:
  fixes f :: "'a:: euclidean_space \<Rightarrow> 'b::euclidean_space"
  assumes fun_lesbegue_integrable: "integrable lborel f" and
    x_axis_integral_measurable: "(\<lambda>x. integral UNIV (\<lambda>y. f(x *\<^sub>R i + y *\<^sub>R j))) \<in> borel_measurable lborel"
  shows "integral UNIV f = integral UNIV (\<lambda>x. integral UNIV (\<lambda>y. f(x *\<^sub>R i + y *\<^sub>R j)))"
    "(\<lambda>x. integral UNIV (\<lambda>y. f(x *\<^sub>R i + y *\<^sub>R j))) integrable_on UNIV"
  sorry

lemma gauge_integral_Fubini_curve_bounded_region_x_euclid:
  fixes f g :: "('a::euclidean_space \<Rightarrow> 'b::euclidean_space)" and
    g1 g2:: "real \<Rightarrow> real" and
    s:: "'a set"
  assumes 
    i_j: "i \<bullet> j = 0" "i \<bullet> i = 1" "j \<bullet> j = 1" and
    fun_lesbegue_integrable: "integrable lborel f" and
    x_axis_gauge_integrable: "\<And>x. (\<lambda>y. f(x *\<^sub>R i + y *\<^sub>R j)) integrable_on UNIV" and
    (*IS THIS redundant? NO IT IS NOT*)
    x_axis_integral_measurable: "(\<lambda>x. integral UNIV (\<lambda>y. f(x *\<^sub>R i + y *\<^sub>R j))) \<in> borel_measurable lborel" and
    f_is_g_indicator: "f = (\<lambda>x. if x \<in> s then g x else 0)" and
    s_is_bounded_by_g1_and_g2: "s = {p. \<exists>x y. p = x *\<^sub>R i + y *\<^sub>R j \<and> a \<le> x \<and> x \<le> b \<and>
                                      (g1 x) \<le> y \<and> y \<le> (g2 x)}"
  shows "integral s g = integral (cbox a b) (\<lambda>x. integral (cbox (g1 x) (g2 x)) (\<lambda>y. g(x *\<^sub>R i + y *\<^sub>R j)))"
proof- 
  have two_D_integral_to_one_D: "integral UNIV f = integral UNIV (\<lambda>x. integral UNIV (\<lambda>y. f(x *\<^sub>R i + y *\<^sub>R j)))"
    using gauge_integral_Fubini_universe_x_euclid[OF fun_lesbegue_integrable] x_axis_integral_measurable
    by auto
  have one_d_integral_integrable: "(\<lambda>x. integral UNIV (\<lambda>y. f(x *\<^sub>R i + y *\<^sub>R j))) integrable_on UNIV"
    using gauge_integral_Fubini_universe_x_euclid(2) and assms
    by blast
  have in_s_iff: "(x *\<^sub>R i + y *\<^sub>R j) \<in> s \<longleftrightarrow> (x \<in> (cbox a b) \<and> y \<in> (cbox (g1 x) (g2 x)))" for x y
    apply (simp add: s_is_bounded_by_g1_and_g2)
    using sum_on_base_2[OF _ i_j]
    by fastforce
  have case_x_in_range:
    "\<forall> x \<in> cbox a b. integral (cbox (g1 x) (g2 x)) (\<lambda>y. g(x *\<^sub>R i + y *\<^sub>R j)) = integral UNIV (\<lambda>y. f(x *\<^sub>R i + y *\<^sub>R j))"
  proof
    fix x:: real
    assume within_range: "x \<in> (cbox a b)"
    let ?f_one_D_spec = "(\<lambda>y. if y \<in> (cbox (g1 x) (g2 x)) then g(x *\<^sub>R i + y *\<^sub>R j) else 0)"
    have f_one_D_region: "(\<lambda>y. f(x *\<^sub>R i + y *\<^sub>R j)) = (\<lambda>y. if y \<in> cbox (g1 x) (g2 x) then g(x *\<^sub>R i + y *\<^sub>R j) else 0)"
    proof
      fix y::real
      show "f (x *\<^sub>R i + y *\<^sub>R j) = (if y \<in> (cbox (g1 x) (g2 x)) then g (x *\<^sub>R i + y *\<^sub>R j) else 0)"
        using in_s_iff within_range
        by (simp add: f_is_g_indicator)        
    qed
    have "(\<lambda>y. f(x *\<^sub>R i + y *\<^sub>R j)) integrable_on cbox (g1 x)  (g2 x)"
    proof -
      have "?f_one_D_spec integrable_on UNIV"
        using f_one_D_region and x_axis_gauge_integrable
        by metis
      then have "?f_one_D_spec integrable_on cbox(g1 x) (g2 x)"
        using integrable_on_subcbox
        by blast
      then show ?thesis using f_one_D_region  by auto
    qed
    then have f_integrale_x: "((\<lambda>y. f(x *\<^sub>R i + y *\<^sub>R j)) has_integral (integral (cbox (g1 x) (g2 x)) (\<lambda>y. f(x *\<^sub>R i + y *\<^sub>R j)))) (cbox (g1 x) (g2 x))"
      using integrable_integral and within_range and x_axis_gauge_integrable
      by auto
    have "\<forall> y.  y \<notin> cbox (g1 x) (g2 x) \<longrightarrow> f (x *\<^sub>R i + y *\<^sub>R j) = 0"
      using f_is_g_indicator and s_is_bounded_by_g1_and_g2 in_s_iff
      by (auto simp add: cbox_def)
    then have "integral (cbox (g1 x)  (g2 x)) (\<lambda>y. f (x *\<^sub>R i + y *\<^sub>R j)) = integral UNIV (\<lambda>y. f (x *\<^sub>R i + y *\<^sub>R j))"
      using has_integral_on_superset[OF f_integrale_x _ Set.subset_UNIV]
      by (simp add: integral_unique)
    then have "((\<lambda>y. f(x *\<^sub>R i + y *\<^sub>R j)) has_integral integral UNIV (\<lambda>y. f (x *\<^sub>R i + y *\<^sub>R j))) (cbox (g1 x) (g2 x))"
      using f_integrale_x
      by simp
    then have "((\<lambda>y. g(x *\<^sub>R i + y *\<^sub>R j)) has_integral integral UNIV (\<lambda>y. f (x *\<^sub>R i + y *\<^sub>R j))) (cbox (g1 x)(g2 x))"
      using  Henstock_Kurzweil_Integration.has_integral_restrict [OF subset_refl ] and
        f_one_D_region
      by (smt has_integral_eq)
    then show "integral (cbox (g1 x) (g2 x)) (\<lambda>y. g (x *\<^sub>R i + y *\<^sub>R j)) = integral UNIV (\<lambda>y. f (x *\<^sub>R i + y *\<^sub>R j))"
      by auto
  qed
  have case_x_not_in_range:
    "\<forall> x. x \<notin> cbox a  b \<longrightarrow> integral UNIV (\<lambda>y. f(x *\<^sub>R i + y *\<^sub>R j)) = 0"
  proof
    fix x::real
    have "x \<notin> (cbox a b) \<longrightarrow> (\<forall>y. f(x *\<^sub>R i + y *\<^sub>R j) = 0)"
      apply  (simp add: f_is_g_indicator)
      using in_s_iff
      by auto
    then show "x \<notin> cbox a b \<longrightarrow> integral UNIV (\<lambda>y. f (x *\<^sub>R i + y *\<^sub>R j)) = 0"
      by simp
  qed
  have RHS: "integral UNIV (\<lambda>x. integral UNIV (\<lambda>y. f(x *\<^sub>R i + y *\<^sub>R j))) = integral (cbox a b) (\<lambda>x. integral (cbox (g1 x) (g2 x)) (\<lambda>y. g(x *\<^sub>R i + y *\<^sub>R j)))"
  proof -
    let ?first_integral = "(\<lambda>x. integral (cbox (g1 x) (g2 x)) (\<lambda>y. g(x *\<^sub>R i + y *\<^sub>R j)))"
    let ?x_integral_cases = "(\<lambda>x. if x \<in> cbox a b then  ?first_integral x else 0)"
    have x_integral_cases_integral: "(\<lambda>x. integral UNIV (\<lambda>y. f(x *\<^sub>R i + y *\<^sub>R j))) = ?x_integral_cases"
      using case_x_in_range and case_x_not_in_range
      by auto
    have "((\<lambda>x. integral UNIV (\<lambda>y. f(x *\<^sub>R i + y *\<^sub>R j))) has_integral (integral UNIV f)) UNIV"
      using two_D_integral_to_one_D
        one_d_integral_integrable
      by auto
    then have "(?x_integral_cases has_integral (integral UNIV f)) UNIV"
      using x_integral_cases_integral by auto
    then have "(?first_integral has_integral (integral UNIV f)) (cbox a b)"
      using  has_integral_restrict_UNIV[of "cbox a b" "?first_integral" "integral UNIV f"]
      by auto
    then show ?thesis
      using two_D_integral_to_one_D
      by (simp add: integral_unique)
  qed
  have f_integrable:"f integrable_on UNIV"
    using fun_lesbegue_integrable and integrable_on_lborel
    by auto
  then have LHS: "integral UNIV f = integral s g"
    apply (simp add: f_is_g_indicator)
    using integrable_restrict_UNIV
      integral_restrict_UNIV
    by auto
  then show ?thesis
    using RHS and two_D_integral_to_one_D
    by auto
qed
*)

lemma gauge_integral_Fubini_curve_bounded_region_x_set:
  fixes f g :: "('a::euclidean_space * 'b::euclidean_space) \<Rightarrow> 'c::euclidean_space" and
    g1 g2:: "'a \<Rightarrow> 'b" and
    s:: "('a * 'b) set"
  assumes fun_lesbegue_integrable: "integrable lborel f" and
    x_axis_gauge_integrable: "\<And>x. (\<lambda>y. f(x, y)) integrable_on UNIV" and
    (*IS THIS redundant? NO IT IS NOT*)
    x_axis_integral_measurable: "(\<lambda>x. integral UNIV (\<lambda>y. f(x, y))) \<in> borel_measurable lborel" and
    f_is_g_indicator: "f = (\<lambda>x. if x \<in> s then g x else 0)" and
    s_is_bounded_by_g1_and_g2: "s = {(x,y). (\<forall>i\<in>Basis. a \<bullet> i \<le> x \<bullet> i \<and> x \<bullet> i \<le> b \<bullet> i) \<and>
                                      (\<forall>i\<in>Basis. (g1 x) \<bullet> i \<le> y \<bullet> i \<and> y \<bullet> i \<le> (g2 x) \<bullet> i)}"
  shows "integral s g = integral (cbox a b) (\<lambda>x. integral (cbox (g1 x) (g2 x)) (\<lambda>y. g(x,y)))"
proof -
  have two_D_integral_to_one_D: "integral UNIV f = integral UNIV (\<lambda>x. integral UNIV (\<lambda>y. f(x,y)))"
    using gauge_integral_Fubini_universe_x(1)[OF fun_lesbegue_integrable] and x_axis_integral_measurable
    by auto
  have one_d_integral_integrable: "(\<lambda>x. integral UNIV (\<lambda>y. f(x,y))) integrable_on UNIV"
    using gauge_integral_Fubini_universe_x(2) and assms
    by blast
  have case_x_in_range:
    "\<forall> x \<in> cbox a b. integral (cbox (g1 x) (g2 x)) (\<lambda>y. g(x,y)) = integral UNIV (\<lambda>y. f(x,y))"
  proof
    fix x:: 'a
    assume within_range: "x \<in> (cbox a b)"
    let ?f_one_D_spec = "(\<lambda>y. if y \<in> (cbox (g1 x) (g2 x)) then g(x,y) else 0)"
    have f_one_D_region: "(\<lambda>y. f(x,y)) = (\<lambda>y. if y \<in> cbox (g1 x) (g2 x) then g(x,y) else 0)"
    proof
      fix y::'b
      show "f (x, y) = (if y \<in> (cbox (g1 x) (g2 x)) then g (x, y) else 0)"
        apply (simp add: f_is_g_indicator s_is_bounded_by_g1_and_g2)
        using within_range
        apply (simp add: cbox_def)
        by smt
    qed
    have zero_out_of_bound: "\<forall> y.  y \<notin> cbox (g1 x) (g2 x) \<longrightarrow> f (x,y) = 0"
      using f_is_g_indicator and s_is_bounded_by_g1_and_g2
      by (auto simp add: cbox_def)
    have "(\<lambda>y. f(x,y)) integrable_on cbox (g1 x)  (g2 x)"
    proof -
      have "?f_one_D_spec integrable_on UNIV"
        using f_one_D_region and x_axis_gauge_integrable
        by metis
      then have "?f_one_D_spec integrable_on cbox(g1 x) (g2 x)"
        using integrable_on_subcbox
        by blast
      then show ?thesis using f_one_D_region  by auto
    qed
    then have f_integrale_x: "((\<lambda>y. f(x,y)) has_integral (integral (cbox (g1 x) (g2 x)) (\<lambda>y. f(x,y)))) (cbox (g1 x) (g2 x))"
      using integrable_integral and within_range and x_axis_gauge_integrable
      by auto
    have "integral (cbox (g1 x)  (g2 x)) (\<lambda>y. f (x, y)) = integral UNIV (\<lambda>y. f (x, y))"
      using has_integral_on_superset[OF f_integrale_x _ Set.subset_UNIV] zero_out_of_bound
      by (simp add: integral_unique)
    then have "((\<lambda>y. f(x, y)) has_integral integral UNIV (\<lambda>y. f (x, y))) (cbox (g1 x) (g2 x))"
      using f_integrale_x
      by simp
    then have "((\<lambda>y. g(x, y)) has_integral integral UNIV (\<lambda>y. f (x, y))) (cbox (g1 x)(g2 x))"
      using  Henstock_Kurzweil_Integration.has_integral_restrict [OF subset_refl ] and
        f_one_D_region
      by (smt has_integral_eq)
    then show "integral (cbox (g1 x) (g2 x)) (\<lambda>y. g (x, y)) = integral UNIV (\<lambda>y. f (x, y))"
      by auto
  qed
  have case_x_not_in_range:
    "\<forall> x. x \<notin> cbox a  b \<longrightarrow> integral UNIV (\<lambda>y. f(x,y)) = 0"
  proof
    fix x::'a
    have "x \<notin> (cbox a b) \<longrightarrow> (\<forall>y. f(x,y) = 0)"
      apply  (simp add: s_is_bounded_by_g1_and_g2 f_is_g_indicator cbox_def)
      by auto
    then show "x \<notin> cbox a b \<longrightarrow> integral UNIV (\<lambda>y. f (x, y)) = 0"
      by (simp add: integral_0)
  qed
  have RHS: "integral UNIV (\<lambda>x. integral UNIV (\<lambda>y. f(x,y))) = integral (cbox a b) (\<lambda>x. integral (cbox (g1 x) (g2 x)) (\<lambda>y. g(x,y)))"
  proof -
    let ?first_integral = "(\<lambda>x. integral (cbox (g1 x) (g2 x)) (\<lambda>y. g(x,y)))"
    let ?x_integral_cases = "(\<lambda>x. if x \<in> cbox a b then  ?first_integral x else 0)"
    have x_integral_cases_integral: "(\<lambda>x. integral UNIV (\<lambda>y. f(x,y))) = ?x_integral_cases"
      using case_x_in_range and case_x_not_in_range
      by auto
    have "((\<lambda>x. integral UNIV (\<lambda>y. f(x,y))) has_integral (integral UNIV f)) UNIV"
      using two_D_integral_to_one_D
        one_d_integral_integrable
      by auto
    then have "(?x_integral_cases has_integral (integral UNIV f)) UNIV"
      using x_integral_cases_integral by auto
    then have "(?first_integral has_integral (integral UNIV f)) (cbox a b)"
      using  has_integral_restrict_UNIV[of "cbox a b" "?first_integral" "integral UNIV f"]
      by auto
    then show ?thesis
      using two_D_integral_to_one_D
      by (simp add: integral_unique)
  qed
  have f_integrable:"f integrable_on UNIV"
    using fun_lesbegue_integrable and integrable_on_lborel
    by auto
  then have LHS: "integral UNIV f = integral s g"
    apply (simp add: f_is_g_indicator)
    using integrable_restrict_UNIV
      integral_restrict_UNIV
    by auto
  then show ?thesis
    using RHS and two_D_integral_to_one_D
    by auto
qed

lemma gauge_integral_Fubini_curve_bounded_region_y:
  fixes f g :: "('a::euclidean_space * 'b::euclidean_space) \<Rightarrow> 'c::euclidean_space" and
    g1 g2:: "'b \<Rightarrow> 'a" and
    s:: "('a * 'b) set"
  assumes fun_lesbegue_integrable: "integrable lborel f" and
    y_axis_gauge_integrable: "\<And>x. (\<lambda>y. f(y, x)) integrable_on UNIV" and
    (*IS THIS redundant? NO IT IS NOT*)
    y_axis_integral_measurable: "(\<lambda>x. integral UNIV (\<lambda>y. f(y, x))) \<in> borel_measurable lborel" and
    f_is_g_indicator: "f = (\<lambda>x. if x \<in> s then g x else 0)" and
    s_is_bounded_by_g1_and_g2: "s = {(y, x). (\<forall>i\<in>Basis. a \<bullet> i \<le> x \<bullet> i \<and> x \<bullet> i \<le> b \<bullet> i) \<and>
                                                   (\<forall>i\<in>Basis. (g1 x) \<bullet> i \<le> y \<bullet> i \<and> y \<bullet> i \<le> (g2 x) \<bullet> i)}"
  shows "integral s g = integral (cbox a b) (\<lambda>x. integral (cbox (g1 x) (g2 x)) (\<lambda>y. g(y, x)))" (is ?g1)
        "g integrable_on s" (is ?g2)
proof -
  have two_D_integral_to_one_D: "integral UNIV f = integral UNIV (\<lambda>x. integral UNIV (\<lambda>y. f(y, x)))"
    using gauge_integral_Fubini_universe_y and fun_lesbegue_integrable and y_axis_integral_measurable
    by auto
  have one_d_integral_integrable: "(\<lambda>x. integral UNIV (\<lambda>y. f(y, x))) integrable_on UNIV"
    using gauge_integral_Fubini_universe_y(2) and assms
    by blast
  have case_y_in_range:
    "\<forall> x \<in> cbox a b. integral (cbox (g1 x) (g2 x)) (\<lambda>y. g(y, x)) = integral UNIV (\<lambda>y. f(y, x))"
  proof
    fix x:: 'b
    assume within_range: "x \<in> (cbox a b)"
    let ?f_one_D_spec = "(\<lambda>y. if y \<in> (cbox (g1 x) (g2 x)) then g(y, x) else 0)"
    have f_one_D_region: "(\<lambda>y. f(y, x)) = (\<lambda>y. if y \<in> cbox (g1 x) (g2 x) then g(y, x) else 0)"
    proof
      fix y::'a
      show "f (y, x) = (if y \<in> (cbox (g1 x) (g2 x)) then g (y, x) else 0)"
        apply (simp add: f_is_g_indicator s_is_bounded_by_g1_and_g2)
        using within_range
        apply (simp add: cbox_def)
        by smt
    qed
    have zero_out_of_bound: "\<forall> y.  y \<notin> cbox (g1 x) (g2 x) \<longrightarrow> f (y, x) = 0"
      using f_is_g_indicator and s_is_bounded_by_g1_and_g2
      by (auto simp add: cbox_def)
    have "(\<lambda>y. f(y, x)) integrable_on cbox (g1 x)  (g2 x)"
    proof -
      have "?f_one_D_spec integrable_on UNIV"
        using f_one_D_region and y_axis_gauge_integrable
        by metis
      then have "?f_one_D_spec integrable_on cbox(g1 x) (g2 x)"
        using integrable_on_subcbox
        by blast
      then show ?thesis using f_one_D_region  by auto
    qed
    then have f_integrale_y: "((\<lambda>y. f(y, x)) has_integral (integral (cbox (g1 x) (g2 x)) (\<lambda>y. f(y,x)))) (cbox (g1 x) (g2 x))"
      using integrable_integral and within_range and y_axis_gauge_integrable
      by auto
    have "integral (cbox (g1 x)  (g2 x)) (\<lambda>y. f (y, x)) = integral UNIV (\<lambda>y. f (y, x))"
      using has_integral_on_superset[OF f_integrale_y _ Set.subset_UNIV] zero_out_of_bound
      by (simp add: integral_unique)
    then have "((\<lambda>y. f(y, x)) has_integral integral UNIV (\<lambda>y. f (y, x))) (cbox (g1 x) (g2 x))"
      using f_integrale_y
      by simp
    then have "((\<lambda>y. g(y, x)) has_integral integral UNIV (\<lambda>y. f (y, x))) (cbox (g1 x)(g2 x))"
      using  Henstock_Kurzweil_Integration.has_integral_restrict [OF subset_refl ] and
        f_one_D_region
      by (smt has_integral_eq)
    then show "integral (cbox (g1 x) (g2 x)) (\<lambda>y. g (y, x)) = integral UNIV (\<lambda>y. f (y, x))"
      by auto
  qed
  have case_y_not_in_range:
    "\<forall> x. x \<notin> cbox a  b \<longrightarrow> integral UNIV (\<lambda>y. f(y, x)) = 0"
  proof
    fix x::'b
    have "x \<notin> (cbox a b) \<longrightarrow> (\<forall>y. f(y, x) = 0)"
      apply  (simp add: s_is_bounded_by_g1_and_g2 f_is_g_indicator cbox_def)
      by auto
    then show "x \<notin> cbox a b \<longrightarrow> integral UNIV (\<lambda>y. f (y, x)) = 0"
      by (simp add: integral_0)
  qed
  have RHS: "integral UNIV (\<lambda>x. integral UNIV (\<lambda>y. f(y, x))) = integral (cbox a b) (\<lambda>x. integral (cbox (g1 x) (g2 x)) (\<lambda>y. g(y, x)))"
  proof -
    let ?first_integral = "(\<lambda>x. integral (cbox (g1 x) (g2 x)) (\<lambda>y. g(y, x)))"
    let ?x_integral_cases = "(\<lambda>x. if x \<in> cbox a b then  ?first_integral x else 0)"
    have y_integral_cases_integral: "(\<lambda>x. integral UNIV (\<lambda>y. f(y, x))) = ?x_integral_cases"
      using case_y_in_range and case_y_not_in_range
      by auto
    have "((\<lambda>x. integral UNIV (\<lambda>y. f(y, x))) has_integral (integral UNIV f)) UNIV"
      using two_D_integral_to_one_D
        one_d_integral_integrable
      by auto
    then have "(?x_integral_cases has_integral (integral UNIV f)) UNIV"
      using y_integral_cases_integral by auto
    then have "(?first_integral has_integral (integral UNIV f)) (cbox a b)"
      using has_integral_restrict_UNIV[of "cbox a b" "?first_integral" "integral UNIV f"]
      by auto
    then show ?thesis
      using two_D_integral_to_one_D
      by (simp add: integral_unique)
  qed
  have f_integrable:"f integrable_on UNIV"
    using fun_lesbegue_integrable and integrable_on_lborel
    by auto
  then have LHS: "integral UNIV f = integral s g"
    apply (simp add: f_is_g_indicator)
    using integrable_restrict_UNIV
      integral_restrict_UNIV
    by auto
  then show ?g1
    using RHS and two_D_integral_to_one_D
    by auto
  show ?g2
    using f_integrable
    apply (simp add: f_is_g_indicator)
    using integrable_restrict_UNIV
      integral_restrict_UNIV
    by auto
qed

lemma gauge_integral_by_substitution:
  fixes f::"(real \<Rightarrow> real)" and
    g::"(real \<Rightarrow> real)" and
    g'::"real \<Rightarrow> real" and
    a::"real" and
    b::"real"
  assumes a_le_b: "a \<le> b" and
    ga_le_gb: "g a \<le> g b" and
    g'_derivative: "\<forall>x \<in> {a..b}. (g has_vector_derivative (g' x)) (at x within {a..b})" and
    g'_continuous: "continuous_on {a..b} g'" and
    f_continuous: "continuous_on (g ` {a..b}) f"
  shows "integral {g a..g b} (f) = integral {a..b} (\<lambda>x. f(g x) * (g' x))"
proof -
  have "\<forall>x \<in> {a..b}. (g has_real_derivative (g' x)) (at x within {a..b})"
    using has_field_derivative_iff_has_vector_derivative[of "g"] and g'_derivative
    by auto
  then have 2: "interval_lebesgue_integral lborel (ereal (a)) (ereal (b)) (\<lambda>x. g' x *\<^sub>R f (g x))
                    = interval_lebesgue_integral lborel (ereal (g a)) (ereal (g b)) f"
    using interval_integral_substitution_finite[of "a" "b" "g" "g'" "f"] and g'_continuous and a_le_b and f_continuous
    by auto
  have g_continuous: "continuous_on {a .. b}  g"
    using Derivative.differentiable_imp_continuous_on
    apply (simp add: differentiable_on_def differentiable_def)
    by (metis continuous_on_vector_derivative g'_derivative)
  have "set_integrable lborel {a .. b} (\<lambda>x. g' x *\<^sub>R f (g x))"
  proof -
    have "continuous_on {a .. b} (\<lambda>x. g' x *\<^sub>R f (g x))"
    proof -
      have "continuous_on {a .. b} (\<lambda>x. f (g x))"
      proof -
        show ?thesis
          using Topological_Spaces.continuous_on_compose f_continuous g_continuous
          by auto
      qed
      then show ?thesis
        using Limits.continuous_on_mult g'_continuous
        by auto
    qed
    then show ?thesis
      using borel_integrable_atLeastAtMost' by blast
  qed
  then have 0: "interval_lebesgue_integral lborel (ereal (a)) (ereal (b)) (\<lambda>x. g' x *\<^sub>R f (g x))
                      = integral {a .. b} (\<lambda>x. g' x *\<^sub>R f (g x))"
    using a_le_b and interval_integral_eq_integral
    by (metis (no_types))
  have "set_integrable lborel {g a .. g b} f"
  proof -
    have "continuous_on {g a .. g b} f"
    proof -
      have "{g a .. g b} \<subseteq> g ` {a .. b}"
        using g_continuous
        by (metis a_le_b atLeastAtMost_iff atLeastatMost_subset_iff continuous_image_closed_interval imageI order_refl)
      then show "continuous_on {g a .. g b} f"
        using f_continuous continuous_on_subset
        by blast
    qed
    then show ?thesis
      using borel_integrable_atLeastAtMost'
      by blast
  qed
  then have 1: "interval_lebesgue_integral lborel (ereal (g a)) (ereal (g b)) f
                      = integral {g a .. g b} f"
    using ga_le_gb and interval_integral_eq_integral
    by (metis (no_types))
  then show ?thesis
    using 0 and 1 and 2
    by (metis (no_types, lifting)  Henstock_Kurzweil_Integration.integral_cong mult.commute real_scaleR_def)
qed

lemma integral_cbox_box:
  shows "((f::('a::euclidean_space) \<Rightarrow> ('a)) has_integral g) (cbox a b) = (f has_integral g) (box a b)"
  using negligible_frontier_interval  box_subset_cbox[of "a" "b"]  has_integral_spike_set_eq[where ?T = "box a b", where ?S ="cbox a b"]
  by (auto simp only: Diff_eq_empty_iff[symmetric] Un_empty_right has_integral_open_interval)

lemma integrable_cbox_box:
  shows "((f::('a::euclidean_space) \<Rightarrow> ('a)) integrable_on (cbox a b)) = (f integrable_on (box a b))"
  using negligible_frontier_interval  box_subset_cbox[of "a" "b"]  integrable_spike_set_eq[where ?T = "box a b", where ?S ="cbox a b"]
  by (auto simp only: Diff_eq_empty_iff[symmetric] Un_empty_right)

lemma integral_on_spikes:
  assumes "finite t"
  assumes "((f::('a::euclidean_space) \<Rightarrow> ('a)) has_integral g) s"
  shows "(f has_integral g) (s - t)"
  by (metis Diff_eq_empty_iff assms assms diff_zero empty_iff finite_Diff has_integral_negligible has_integral_setdiff negligible_finite order_refl)

(*The idea now is how to convert a tagged division to another one whose tags are not in s, and show that it will have the same content*)
lemma rsum_bound_:
  assumes p: "p tagged_division_of (cbox a b)"
    and "\<forall>x\<in>(cbox a b) -s. norm (f x) \<le> e"
    and "(cbox a b) -s \<noteq> {}"
    and "\<forall>(x,K)\<in> p. x \<notin> s"
  shows "norm (sum (\<lambda>(x,k). content k *\<^sub>R f x) p) \<le> e * content (cbox a b)"
proof (cases "(cbox a b) = {}")
  case True show ?thesis
    using p unfolding True tagged_division_of_trivial by auto
next
  case False
  then have e: "e \<ge> 0"
    by (meson ex_in_conv assms(2) assms(3) norm_ge_zero order_trans)
  have sum_le: "sum (content \<circ> snd) p \<le> content (cbox a b)"
    unfolding additive_content_tagged_division[OF p, symmetric] split_def
    by (auto intro: eq_refl)
  have con: "\<And>xk. xk \<in> p \<Longrightarrow> 0 \<le> content (snd xk)"
    using tagged_division_ofD(4) [OF p] content_pos_le
    by force
  have norm: "\<And>xk. xk \<in> p \<Longrightarrow> norm (f (fst xk)) \<le> e"
  proof-
    fix xk
    assume ass: "xk \<in> p"
    then obtain x K where xk: "(x,K)\<in>p" "xk = (x, K)" "K \<subseteq> cbox a b" "x \<in> K"
      using tagged_division_ofD(2,3)[OF p] prod.collapse by metis
    then have "x \<in> (cbox a b) - s" using assms by auto
    then have "norm (f x) \<le> e" using assms by auto
    then show "norm (f (fst xk)) \<le> e" using xk by auto
  qed
  have "norm (sum (\<lambda>(x,k). content k *\<^sub>R f x) p) \<le> (\<Sum>i\<in>p. norm (case i of (x, k) \<Rightarrow> content k *\<^sub>R f x))"
    by (rule norm_sum)
  also have "...  \<le> e * content (cbox a b)"
    unfolding split_def norm_scaleR
    apply (rule order_trans[OF sum_mono])
     apply (rule mult_left_mono[OF _ abs_ge_zero, of _ e])
     apply (metis norm)
    unfolding sum_distrib_right[symmetric]
    using con sum_le
    apply (auto simp: mult.commute intro: mult_left_mono [OF _ e])
    done
  finally show ?thesis .
qed

lemma rsum_bound_'(*Larry's proof*):
  assumes p: "p tagged_division_of (cbox a b)"
    and "\<forall>x\<in>(cbox a b) -s. norm (f x) \<le> e"
    and "(cbox a b) -s \<noteq> {}"
    and "\<forall>(x,K)\<in> p. x \<notin> s"
  shows "norm (sum (\<lambda>(x,k). content k *\<^sub>R f x) p) \<le> e * content (cbox a b)"
proof (cases "(cbox a b) = {}")
  case True show ?thesis
    using p unfolding True tagged_division_of_trivial by auto
next
  case False
  then have e: "e \<ge> 0"
    by (meson ex_in_conv assms(2) assms(3) norm_ge_zero order_trans)
  have sum_le: "sum (content \<circ> snd) p \<le> content (cbox a b)"
    unfolding additive_content_tagged_division[OF p, symmetric] split_def
    by (auto intro: eq_refl)
  have con: "\<And>xk. xk \<in> p \<Longrightarrow> 0 \<le> content (snd xk)"
    using tagged_division_ofD(4) [OF p] content_pos_le
    by force
  have norm: "\<And>xk. xk \<in> p \<Longrightarrow> norm (f (fst xk)) \<le> e"
  proof-
    fix xk
    assume ass: "xk \<in> p"
    then obtain x K where xk: "(x,K)\<in>p" "xk = (x, K)" "K \<subseteq> cbox a b" "x \<in> K"
      using tagged_division_ofD(2,3)[OF p] prod.collapse by metis
    then have "x \<in> (cbox a b) - s" using assms by auto
    then have "norm (f x) \<le> e" using assms by auto
    then show "norm (f (fst xk)) \<le> e" using xk by auto
  qed
  have "norm (sum (\<lambda>(x,k). content k *\<^sub>R f x) p) \<le> (\<Sum>i\<in>p. norm (case i of (x, k) \<Rightarrow> content k *\<^sub>R f x))"
    by (rule norm_sum)
  also have "...  \<le> e * content (cbox a b)"
    unfolding split_def norm_scaleR
    apply (rule order_trans[OF sum_mono])
     apply (rule mult_left_mono[OF _ abs_ge_zero, of _ e])
     apply (metis norm)
    unfolding sum_distrib_right[symmetric]
    using con sum_le
    apply (auto simp: mult.commute intro: mult_left_mono [OF _ e])
    done
  finally show ?thesis .
qed


lemma frontier_ic:
  assumes "a < (b::real)"
  shows "frontier {a<..b}  = {a,b}"
  apply(simp add: frontier_def)
  using assms
  by auto

lemma frontier_ci:
  assumes "a < (b::real)"
  shows "frontier {a<..<b}  = {a,b}"
  apply(simp add: frontier_def)
  using assms
  by auto

lemma ic_not_closed:
  assumes "a < (b::real)"
  shows "\<not> closed {a<..b}"
  using assms frontier_subset_eq frontier_ic greaterThanAtMost_iff by blast

lemma closure_ic_union_ci:
  assumes "a < (b::real)" "b < c"
  shows "closure ({a..<b} \<union> {b<..c})  = {a .. c}"
  using frontier_ic[OF assms(1)] frontier_ci[OF assms(2)] closure_Un assms
  apply(simp add: frontier_def)
  by auto

lemma interior_ic_ci_union:
  assumes "a < (b::real)" "b < c"
  shows "b \<notin> (interior ({a..<b} \<union> {b<..c}))"
proof-
  have "b \<notin> ({a..<b} \<union> {b<..c})" by auto
  then show ?thesis
    using interior_subset by blast
qed

lemma frontier_ic_union_ci:
  assumes "a < (b::real)" "b < c"
  shows "b \<in> frontier ({a..<b} \<union> {b<..c})"
  using closure_ic_union_ci assms interior_ic_ci_union
  by(simp add: frontier_def)

lemma ic_union_ci_not_closed:
  assumes "a < (b::real)" "b < c"
  shows "\<not> closed ({a..<b} \<union> {b<..c})"
proof-
  have "b \<notin> ({a..<b} \<union> {b<..c})" by auto
  then show ?thesis
    using assms frontier_subset_eq frontier_ic_union_ci[OF assms]
    by (auto simp only: subset_iff)
qed

lemma integrable_continuous_:
  fixes f :: "'b::euclidean_space \<Rightarrow> 'a::banach"
  assumes "continuous_on (cbox a b) f"
  shows "f integrable_on cbox a b"
  by (simp add: assms integrable_continuous)

lemma removing_singletons_from_div:
  assumes   "\<forall>t\<in>S. \<exists>c d::real. c < d \<and> {c..d} = t"
    "{x} \<union> \<Union>S = {a..b}" "a < x" "x < b"
    "finite S"
  shows "\<exists>t\<in>S. x \<in> t"
proof(rule ccontr)
  assume "\<not>(\<exists>t\<in>S. x \<in> t)"
  then have "\<forall>t\<in>S. x \<notin> t" by auto
  then have "x \<notin> \<Union>S" by auto
  then have i: "\<Union>S = {a..b} - {x}" using assms (2) by auto
  have "x \<in> {a..b}" using assms by auto
  then have "{a .. b} - {x} = {a..<x} \<union> {x<..b}" by auto
  then have 0: "\<Union>S = {a..<x} \<union> {x<..b}" using i by auto
  have 1:"closed (\<Union>S)"
    apply(rule closed_Union)
  proof-
    show "finite S"
      using assms by auto
    show "\<forall>T\<in>S. closed T" using assms  by auto
  qed
  show False using 0 1 ic_union_ci_not_closed assms by auto
qed

lemma remove_singleton_from_division_of:(*By Manuel Eberl*)
  assumes "A division_of {a::real..b}" "a < b"
  assumes "x \<in> {a..b}"
  shows   "\<exists>c d. c < d \<and> {c..d} \<in> A \<and> x \<in> {c..d}"
proof -
  from assms have "x islimpt {a..b}"
    by (intro connected_imp_perfect) auto
  also have "{a..b} = {x. {x..x} \<in> A} \<union> ({a..b} - {x. {x..x} \<in> A})"
    using assms by auto
  also have "x islimpt \<dots> \<longleftrightarrow> x islimpt {a..b} - {x. {x..x} \<in> A}"
  proof (intro islimpt_Un_finite)
    have "{x. {x..x} \<in> A} \<subseteq> Inf ` A"
    proof safe
      fix x assume "{x..x} \<in> A"
      thus "x \<in> Inf ` A"
        by (auto intro!: bexI[of _ "{x}"] simp: image_iff)
    qed
    moreover from assms have "finite A" by (auto simp: division_of_def)
    hence "finite (Inf ` A)" by auto
    ultimately show "finite {x. {x..x} \<in> A}" by (rule finite_subset)
  qed
  also have "{a..b} = \<Union>A"
    using assms by (auto simp: division_of_def)
  finally have "x islimpt \<Union>(A - range (\<lambda>x. {x..x}))"
    by (rule islimpt_subset) auto
  moreover have "closed (\<Union>(A - range (\<lambda>x. {x..x})))"
    using assms by (intro closed_Union) auto
  ultimately have "x \<in> (\<Union>(A - range (\<lambda>x. {x..x})))"
    by (auto simp: closed_limpt)
  then obtain X where "x \<in> X" "X \<in> A" "X \<notin> range (\<lambda>x. {x..x})"
    by blast
  moreover from division_ofD(2)[OF assms(1) this(2)] division_ofD(3)[OF assms(1) this(2)]
    division_ofD(4)[OF assms(1) this(2)]
  obtain c d where "X = cbox c d" "X \<subseteq> {a..b}" "X \<noteq> {}" by blast
  ultimately have "c \<le> d" by auto
  have "c \<noteq> d"
  proof
    assume "c = d"
    with \<open>X = cbox c d\<close> have "X = {c..c}" by auto
    hence "X \<in> range (\<lambda>x. {x..x})" by blast
    with \<open>X \<notin> range (\<lambda>x. {x..x})\<close> show False by contradiction
  qed
  with \<open>c \<le> d\<close> have "c < d" by simp
  with \<open>X = cbox c d\<close> and \<open>x \<in> X\<close> and \<open>X \<in> A\<close> show ?thesis
    by auto
qed

lemma remove_singleton_from_tagged_division_of:
  assumes "A tagged_division_of {a::real..b}" "a < b"
  assumes "x \<in> {a..b}"
  shows   "\<exists>k c d. c < d \<and> (k, {c..d}) \<in> A \<and> x \<in> {c..d}"
  using remove_singleton_from_division_of[OF division_of_tagged_division[OF assms(1)] assms(2)]
    (*sledgehammer*)
  using assms(3) by fastforce

lemma tagged_div_wo_singlestons:
  assumes "p tagged_division_of {a::real..b}" "a < b"
  shows "(p - {xk. \<exists>x y. xk = (x,{y})}) tagged_division_of cbox a b"
  using remove_singleton_from_tagged_division_of[OF assms] assms
  apply(auto simp add: tagged_division_of_def tagged_partial_division_of_def)
     apply blast
    apply blast
   apply blast
  by fastforce

lemma tagged_div_wo_empty:
  assumes "p tagged_division_of {a::real..b}" "a < b"
  shows "(p - {xk. \<exists>x. xk = (x,{})}) tagged_division_of cbox a b"
  using remove_singleton_from_tagged_division_of[OF assms] assms
  apply(auto simp add: tagged_division_of_def tagged_partial_division_of_def)
     apply blast
    apply blast
   apply blast
  by fastforce

lemma fine_diff:
  assumes "\<gamma> fine p"
  shows "\<gamma> fine (p - s)"
  apply (auto simp add: fine_def)
  using assms by auto

lemma tagged_div_tage_notin_set:
  assumes "finite (s::real set)"
    "p tagged_division_of {a..b}"
    "\<gamma> fine p" "(\<forall>(x, K)\<in>p. \<exists>c d::real. c < d \<and> K = {c..d})" "gauge \<gamma>"
  shows "\<exists>p' \<gamma>'. p' tagged_division_of {a..b} \<and>
                  \<gamma>' fine p' \<and> (\<forall>(x, K)\<in>p'. x \<notin> s) \<and> gauge \<gamma>'"
proof-
  have "(\<forall>(x::real, K)\<in>p. \<exists>x'. x' \<notin> s \<and> x'\<in> interior K)"
  proof-
    {fix x::real
      fix K
      assume ass: "(x::real,K) \<in> p"
      have "(\<forall>(x, K)\<in>p. infinite (interior K))"
        using assms(4) infinite_Ioo interior_atLeastAtMost_real
        by (smt split_beta)
      then have i: "infinite (interior K)" using ass by auto
      have "\<exists>x'. x' \<notin> s \<and> x'\<in> interior K"
        using infinite_imp_nonempty[OF Diff_infinite_finite[OF assms(1) i]] by auto}
    then show ?thesis by auto
  qed
  then obtain f where f: "(\<forall>(x::real, K)\<in>p. (f (x,K)) \<notin> s \<and> (f (x,K)) \<in> interior K)"
    using choice_iff[where ?Q = "\<lambda>(x,K) x'. (x::real, K)\<in>p \<longrightarrow> x' \<notin> s \<and> x' \<in> interior K"]
    apply (auto simp add: case_prod_beta)
    by metis
  have f': "(\<forall>(x::real, K)\<in>p. (f (x,K)) \<notin> s \<and> (f (x,K)) \<in> K)"
    using f interior_subset
    by (auto simp add: case_prod_beta subset_iff)
  let ?p' = "{m. (\<exists>xK. m = ((f xK), snd xK) \<and> xK \<in> p)}"
  have 0: "(\<forall>(x, K)\<in>?p'. x \<notin> s)"
    using f
    by (auto simp add: case_prod_beta)
  have i: "finite {(f (a, b), b) |a b. (a, b) \<in> p}"
  proof-
    have a: "{(f (a, b), b) |a b. (a, b) \<in> p} = (%(a,b). (f(a,b),b)) ` p"  by auto
    have b: "finite p" using assms(2) by auto
    show ?thesis using a b by auto
  qed
  have 1: "?p' tagged_division_of {a..b}"
    using assms(2) f'
    apply(auto simp add: tagged_division_of_def tagged_partial_division_of_def case_prod_beta)
       apply(metis i)
      apply blast
     apply blast
    by fastforce
      (*f is injective becuase interiors of different K's are disjoint and f is in interior*)
  have f_inj: "inj_on f p"
    apply(simp add: inj_on_def)
  proof-
    {fix x y
      assume "x \<in> p" "y \<in> p"
        "f x = f y"
      then have "x = y"
        using f
          tagged_division_ofD(5)[OF assms(2)]
          (*sledgehammer*)
        by (smt case_prodE disjoint_insert(2) mk_disjoint_insert)}note * = this
    show "\<forall>x\<in>p. \<forall>y\<in>p. f x = f y \<longrightarrow> x = y"  using * by auto
  qed
  let ?\<gamma>' = "\<lambda>x. if (\<exists>xK \<in> p. f xK  = x) then (\<gamma> o fst o  the_inv_into p f) x else \<gamma> x"
  have 2: "?\<gamma>' fine ?p'" using assms(3)
    apply(auto simp add: fine_def case_prod_beta the_inv_into_f_f[OF f_inj])
    by force
  have 3: "gauge ?\<gamma>'"
    using assms(5) assms(3) f'
    apply(auto simp add: fine_def gauge_def case_prod_beta the_inv_into_f_f[OF f_inj])
    by force
  have "?p' tagged_division_of {a..b} \<and> ?\<gamma>' fine ?p' \<and> (\<forall>(x, K)\<in>?p'. x \<notin> s) \<and> gauge ?\<gamma>'"
    using 0 1 2 3 by auto
  then show ?thesis by smt
qed

lemma has_integral_bound_spike_finite:
  fixes f :: "'a::euclidean_space \<Rightarrow> 'b::real_normed_vector"
  assumes "0 \<le> B" and "finite S"
    and f: "(f has_integral i) (cbox a b)"
    and leB: "\<And>x. x \<in> cbox a b - S \<Longrightarrow> norm (f x) \<le> B"
  shows "norm i \<le> B * content (cbox a b)"
proof -
  define g where "g \<equiv> (\<lambda>x. if x \<in> S then 0 else f x)"
  then have "\<And>x. x \<in> cbox a b - S \<Longrightarrow> norm (g x) \<le> B"
    using leB by simp
  moreover have "(g has_integral i) (cbox a b)"
    using has_integral_spike_finite [OF \<open>finite S\<close> _ f]
    by (simp add: g_def)
  ultimately show ?thesis
    by (simp add: \<open>0 \<le> B\<close> g_def has_integral_bound)
qed

lemma has_integral_bound_:
  fixes f :: "real \<Rightarrow> 'a::real_normed_vector"
  assumes "a < b"
    and "0 \<le> B"
    and f: "(f has_integral i) (cbox a b)"
    and "finite s"
    and "\<forall>x\<in>(cbox a b)-s. norm (f x) \<le> B"
  shows "norm i \<le> B * content (cbox a b)"
  using has_integral_bound_spike_finite assms by blast

corollary has_integral_bound_real':
  fixes f :: "real \<Rightarrow> 'b::real_normed_vector"
  assumes "0 \<le> B"
    and f: "(f has_integral i) (cbox a b)"
    and "finite s"
    and "\<forall>x\<in>(cbox a b)-s. norm (f x) \<le> B"
  shows "norm i \<le> B * content {a..b}"
    (*sledgehammer*)
  by (metis assms(1) assms(3) assms(4) box_real(2) f has_integral_bound_spike_finite)

lemma integral_has_vector_derivative_continuous_at':
  fixes f :: "real \<Rightarrow> 'a::banach"
  assumes "finite s"
    and f: "f integrable_on {a..b}"
    and x: "x \<in> {a..b} - s"
    and fx: "continuous (at x within ({a..b} - s)) f"
  shows "((\<lambda>u. integral {a..u} f) has_vector_derivative f x) (at x within ({a..b} - s))"
proof -
  let ?I = "\<lambda>a b. integral {a..b} f"
  { fix e::real
    assume "e > 0"
    obtain d where "d>0" and d: "\<And>x'. \<lbrakk>x' \<in> {a..b} - s; \<bar>x' - x\<bar> < d\<rbrakk> \<Longrightarrow> norm(f x' - f x) \<le> e"
      using \<open>e>0\<close> fx by (auto simp: continuous_within_eps_delta dist_norm less_imp_le)
    have "norm (integral {a..y} f - integral {a..x} f - (y-x) *\<^sub>R f x) \<le> e * \<bar>y - x\<bar>"
      if y: "y \<in> {a..b} - s" and yx: "\<bar>y - x\<bar> < d" for y
    proof (cases "y < x")
      case False
      have "f integrable_on {a..y}"
        using f y by (simp add: integrable_subinterval_real)
      then have Idiff: "?I a y - ?I a x = ?I x y"
        using False x by (simp add: algebra_simps Henstock_Kurzweil_Integration.integral_combine)
      have fux_int: "((\<lambda>u. f u - f x) has_integral integral {x..y} f - (y-x) *\<^sub>R f x) {x..y}"
        apply (rule has_integral_diff)
        using x y apply (auto intro: integrable_integral [OF integrable_subinterval_real [OF f]])
        using has_integral_const_real [of "f x" x y] False
        apply (simp add: )
        done
      show ?thesis
        using False
        apply (simp add: abs_eq_content del: content_real_if measure_lborel_Icc)
        apply (rule has_integral_bound_real'[where f="(\<lambda>u. f u - f x)"])
        using yx False d x y \<open>e>0\<close> apply (auto simp add: Idiff fux_int)
      proof-
        let ?M48= "mset_set s"
        show "\<And>z. y - x < d \<Longrightarrow> (\<And>x'. a \<le> x' \<and> x' \<le> b \<and> x' \<notin> s \<Longrightarrow> \<bar>x' - x\<bar> < d \<Longrightarrow> norm (f x' - f x) \<le> e) \<Longrightarrow> 0 < e \<Longrightarrow> z \<notin># ?M48 \<Longrightarrow> a \<le> x \<Longrightarrow> x \<notin> s \<Longrightarrow> y \<le> b \<Longrightarrow> y \<notin> s \<Longrightarrow> x \<le> z \<Longrightarrow> z \<le> y \<Longrightarrow> norm (f z - f x) \<le> e"
          using assms by auto
      qed
    next
      case True
      have "f integrable_on {a..x}"
        using f x by (simp add: integrable_subinterval_real)
      then have Idiff: "?I a x - ?I a y = ?I y x"
        using True x y by (simp add: algebra_simps Henstock_Kurzweil_Integration.integral_combine)
      have fux_int: "((\<lambda>u. f u - f x) has_integral integral {y..x} f - (x - y) *\<^sub>R f x) {y..x}"
        apply (rule has_integral_diff)
        using x y apply (auto intro: integrable_integral [OF integrable_subinterval_real [OF f]])
        using has_integral_const_real [of "f x" y x] True
        apply (simp add: )
        done
      have "norm (integral {a..x} f - integral {a..y} f - (x - y) *\<^sub>R f x) \<le> e * \<bar>y - x\<bar>"
        using True
        apply (simp add: abs_eq_content del: content_real_if measure_lborel_Icc)
        apply (rule has_integral_bound_real'[where f="(\<lambda>u. f u - f x)"])
        using yx True d x y \<open>e>0\<close> apply (auto simp add: Idiff fux_int)
      proof-
        let ?M44= "mset_set s"
        show " \<And>xa. x - y < d \<Longrightarrow> y < x \<Longrightarrow> (\<And>x'. a \<le> x' \<and> x' \<le> b \<and> x' \<notin> s \<Longrightarrow> \<bar>x' - x\<bar> < d \<Longrightarrow> norm (f x' - f x) \<le> e) \<Longrightarrow> 0 < e \<Longrightarrow> xa \<notin># ?M44 \<Longrightarrow> x \<le> b \<Longrightarrow> x \<notin> s \<Longrightarrow> a \<le> y \<Longrightarrow> y \<notin> s \<Longrightarrow> y \<le> xa \<Longrightarrow> xa \<le> x \<Longrightarrow> norm (f xa - f x) \<le> e"
          using assms by auto
      qed
      then show ?thesis
        by (simp add: algebra_simps norm_minus_commute)
    qed
    then have "\<exists>d>0. \<forall>y\<in>{a..b} - s. \<bar>y - x\<bar> < d \<longrightarrow> norm (integral {a..y} f - integral {a..x} f - (y-x) *\<^sub>R f x) \<le> e * \<bar>y - x\<bar>"
      using \<open>d>0\<close> by blast
  }
  then show ?thesis
    by (simp add: has_vector_derivative_def has_derivative_within_alt bounded_linear_scaleR_left)
qed

lemma integral_has_vector_derivative':
  fixes f :: "real \<Rightarrow> 'a::banach"
  assumes "finite s"
    "f integrable_on {a..b}"
    "x \<in> {a..b} - s"
    "continuous (at x within {a..b} - s) f"
  shows "((\<lambda>u. integral {a .. u} f) has_vector_derivative f(x)) (at x within {a .. b} - s)"
  apply (rule integral_has_vector_derivative_continuous_at')
  using assms
     apply (auto simp: continuous_on_eq_continuous_within)
  done

lemma fundamental_theorem_of_calculus_interior_stronger:
  fixes f :: "real \<Rightarrow> 'a::banach"
  assumes "finite S"
    and "a \<le> b" "\<And>x. x \<in> {a <..< b} - S \<Longrightarrow> (f has_vector_derivative f'(x)) (at x)"
    and "continuous_on {a .. b} f"
  shows "(f' has_integral (f b - f a)) {a .. b}"
  using assms
proof (induction arbitrary: a b)
  case empty
  then show ?case
    using fundamental_theorem_of_calculus_interior by force
next
  case (insert x S)
  show ?case
  proof (cases "x \<in> {a<..<b}")
    case False then show ?thesis
      using insert by blast
  next
    case True then have "a < x" "x < b"
      by auto
    have "(f' has_integral f x - f a) {a..x}"
      apply (rule insert)
      using \<open>a < x\<close> \<open>x < b\<close> insert.prems continuous_on_subset by force+
    moreover have "(f' has_integral f b - f x) {x..b}"
      apply (rule insert)
      using \<open>x < b\<close> \<open>a < x\<close> insert.prems continuous_on_subset by force+
    ultimately show ?thesis
      by (meson finite_insert fundamental_theorem_of_calculus_interior_strong insert.hyps(1) insert.prems(1) insert.prems(2) insert.prems(3))
  qed
qed

lemma at_within_closed_interval_finite:
  fixes x::real
  assumes "a < x" "x < b" "x \<notin> S" "finite S"
  shows "(at x within {a..b} - S) = at x"
proof -
  have "interior ({a..b} - S) = {a<..<b} - S"
    using \<open>finite S\<close>
    by (simp add: interior_diff finite_imp_closed)
  then show ?thesis
    using at_within_interior assms by fastforce
qed

lemma at_within_cbox_finite:
  assumes "x \<in> box a b" "x \<notin> S" "finite S"
  shows "(at x within cbox a b - S) = at x"
proof -
  have "interior (cbox a b - S) = box a b - S"
    using \<open>finite S\<close> by (simp add: interior_diff finite_imp_closed)
  then show ?thesis
    using at_within_interior assms by fastforce
qed

lemma fundamental_theorem_of_calculus_interior_stronger':
  fixes f :: "real \<Rightarrow> 'a::banach"
  assumes "finite S"
    and "a \<le> b" "\<And>x. x \<in> {a <..< b} - S \<Longrightarrow> (f has_vector_derivative f'(x)) (at x within {a..b} - S)"
    and "continuous_on {a .. b} f"
  shows "(f' has_integral (f b - f a)) {a .. b}"
  using assms fundamental_theorem_of_calculus_interior_strong at_within_cbox_finite
    (*sledgehammer*)
  by (metis DiffD1 DiffD2 interior_atLeastAtMost_real interior_cbox interval_cbox)

lemma has_integral_substitution_general_:
  fixes f :: "real \<Rightarrow> 'a::euclidean_space" and g :: "real \<Rightarrow> real"
  assumes s: "finite s" and le: "a \<le> b"
    and subset: "g ` {a..b} \<subseteq> {c..d}"
    and f: "f integrable_on {c..d}" "continuous_on ({c..d} - (g ` s)) f"
    (*and f [continuous_intros]: "continuous_on {c..d} f"*)
    and g : "continuous_on {a..b} g" "inj_on g ({a..b} \<union> s)"
    and deriv [derivative_intros]:
    "\<And>x. x \<in> {a..b} - s \<Longrightarrow> (g has_field_derivative g' x) (at x within {a..b})"
  shows "((\<lambda>x. g' x *\<^sub>R f (g x)) has_integral (integral {g a..g b} f - integral {g b..g a} f)) {a..b}"
proof -
  let ?F = "\<lambda>x. integral {c..g x} f"
  have cont_int: "continuous_on {a..b} ?F"
    by (rule continuous_on_compose2[OF _ g(1) subset] indefinite_integral_continuous_1
        f)+
  have deriv: "\<And>x. x \<in> {a..b} - s \<Longrightarrow> (((\<lambda>x. integral {c..x} f) \<circ> g) has_vector_derivative g' x *\<^sub>R f (g x))
                 (at x within ({a..b} - s))"
    apply (rule has_vector_derivative_eq_rhs)
     apply (rule vector_diff_chain_within)
      apply (subst has_field_derivative_iff_has_vector_derivative [symmetric])
  proof-
    fix x::real
    assume ass: "x \<in> {a..b} - s"
    let ?f'3 = "g' x"
    have i:"{a..b} - s \<subseteq> {a..b}" by auto
    have ii: " (g has_vector_derivative g' x) (at x within {a..b})" using deriv[OF ass]
      by (simp only: has_field_derivative_iff_has_vector_derivative)
    show "(g has_real_derivative ?f'3) (at x within {a..b} - s)"
      using has_vector_derivative_within_subset[OF ii i]
      by (simp only: has_field_derivative_iff_has_vector_derivative)
  next
    let ?g'3 = "f o g"
    show "\<And>x. x \<in> {a..b} - s \<Longrightarrow> ((\<lambda>x. integral {c..x} f) has_vector_derivative ?g'3 x) (at (g x) within g ` ({a..b} - s))"
    proof-
      fix x::real
      assume ass: "x \<in> {a..b} - s"
      have "finite (g ` s)" using s by auto
      then have i: "((\<lambda>x. integral {c..x} f) has_vector_derivative f(g x)) (at (g x) within ({c..d} - g ` s))"
        apply (rule integral_has_vector_derivative')
      proof-
        show " f integrable_on {c..d}" using f by auto
        show "g x \<in> {c..d} - g ` s" using ass subset
            (*sledgehammer*)
          by (smt Diff_iff Un_upper1 Un_upper2 g(2) imageE image_subset_iff inj_onD subsetCE)
        show "continuous (at (g x) within {c..d} - g ` s) f"
          (*sledgehammer*)
          using \<open>g x \<in> {c..d} - g ` s\<close> continuous_on_eq_continuous_within f(2) by blast
      qed
      have ii: "g ` ({a..b} - s) \<subseteq> ({c..d} - g ` s)"
        using subset g(2)
          (*sledgehammer*)
        by (smt Diff_subset Un_Diff Un_commute Un_upper2 inj_on_image_set_diff subset_trans sup.order_iff)
      then show "((\<lambda>x. integral {c..x} f) has_vector_derivative ?g'3 x) (at (g x) within g ` ({a..b} - s))"
        (*sledgehammer*)
        by (smt Diff_subset has_vector_derivative_weaken Un_upper1 Un_upper2 \<open>finite (g ` s)\<close> ass comp_def continuous_on_eq_continuous_within f(1) f(2) g(2) image_diff_subset image_subset_iff inj_on_image_set_diff integral_has_vector_derivative_continuous_at' subset_trans)
    qed
    show "\<And>x. x \<in> {a..b} - s \<Longrightarrow> g' x *\<^sub>R ?g'3 x = g' x *\<^sub>R f (g x)" by auto
  qed
  have deriv: "(?F has_vector_derivative g' x *\<^sub>R f (g x))
                  (at x within {a..b} - s)" if "x \<in> {a<..<b} - (s)" for x
    using deriv[of x] that by (simp add: at_within_Icc_at o_def)
  have "((\<lambda>x. g' x *\<^sub>R f (g x)) has_integral (?F b - ?F a)) {a..b}"
    using cont_int
    using fundamental_theorem_of_calculus_interior_stronger'[OF s le deriv]
    by blast
  also
  from subset have "g x \<in> {c..d}" if "x \<in> {a..b}" for x using that by blast
  from this[of a] this[of b] le have cd: "c \<le> g a" "g b \<le> d" "c \<le> g b" "g a \<le> d" by auto
  have "integral {c..g b} f - integral {c..g a} f = integral {g a..g b} f - integral {g b..g a} f"
  proof cases
    assume "g a \<le> g b"
    note le = le this
    from cd have "integral {c..g a} f + integral {g a..g b} f = integral {c..g b} f"
      by (meson Henstock_Kurzweil_Integration.integral_combine atLeastatMost_subset_iff f(1) integrable_on_subinterval le(2) order_refl)
    with le show ?thesis
      by (cases "g a = g b") (simp_all add: algebra_simps)
  next
    assume less: "\<not>g a \<le> g b"
    then have "g a \<ge> g b" by simp
    note le = le this
    from cd have "integral {c..g b} f + integral {g b..g a} f = integral {c..g a} f"
      by (meson Henstock_Kurzweil_Integration.integral_combine atLeastatMost_subset_iff f(1) integrable_on_subinterval le(2) order_refl)
    with less show ?thesis
      by (simp_all add: algebra_simps)
  qed
  finally show ?thesis .
qed

lemma has_integral_substitution_general__:
  fixes f :: "real \<Rightarrow> 'a::euclidean_space" and g :: "real \<Rightarrow> real"
  assumes s: "finite s" and le: "a \<le> b" and s_subset: "s \<subseteq> {a..b}"
    and subset: "g ` {a..b} \<subseteq> {c..d}"
    and f: "f integrable_on {c..d}" "continuous_on ({c..d} - (g ` s)) f"
    (*and f [continuous_intros]: "continuous_on {c..d} f"*)
    and g : "continuous_on {a..b} g" "inj_on g {a..b}"
    and deriv [derivative_intros]:
    "\<And>x. x \<in> {a..b} - s \<Longrightarrow> (g has_field_derivative g' x) (at x within {a..b})"
  shows "((\<lambda>x. g' x *\<^sub>R f (g x)) has_integral (integral {g a..g b} f - integral {g b..g a} f)) {a..b}"
  using s_subset has_integral_substitution_general_[OF s le subset f g(1) _ deriv]
  by (simp add: g(2) sup_absorb1)

lemma has_integral_substitution_general_':
  fixes f :: "real \<Rightarrow> 'a::euclidean_space" and g :: "real \<Rightarrow> real"
  assumes s: "finite s" and le: "a \<le> b" and s': "finite s'"
    and subset: "g ` {a..b} \<subseteq> {c..d}"
    and f: "f integrable_on {c..d}" "continuous_on ({c..d} - s') f"
    and g : "continuous_on {a..b} g" "\<forall>x\<in>s'. finite ({c..d} \<inter> g -` {x})" "surj_on s' g" "inj_on g ({a..b} \<union> ((s \<union> ({c..d} \<inter> g -` s'))))"
    and deriv:
    "\<And>x. x \<in> {a..b} - s \<Longrightarrow> (g has_field_derivative g' x) (at x within {a..b})"
    and bk_img_subset: "g -` s' \<subseteq> {c..d}"
  shows "((\<lambda>x. g' x *\<^sub>R f (g x)) has_integral (integral {g a..g b} f - integral {g b..g a} f)) {a..b}"
proof-
  have a: "{c..d} \<inter> g -` s' = \<Union>{t. \<exists>x. t = {c..d} \<inter> g -` {x} \<and> x \<in> s'}"
    using s s' by blast
  have "finite (\<Union>{t. \<exists>x. t = {c..d} \<inter> g -` {x} \<and> x \<in> s'})" using s'
    using g(2) by auto
  then have 0: "finite (s \<union> {c..d} \<inter> (g -` s'))"
    using a s
    by simp
  have "continuous_on ({c..d} - g ` (s \<union> g -` s')) f"
    using f(2) surj_on_image_vimage_eq
    by (metis Diff_mono Un_upper2 continuous_on_subset equalityE g(3) image_Un)
  then have 1: "continuous_on ({c..d} - (g ` (s \<union> ({c..d} \<inter> g -` s')))) f"
    using f(2) surj_on_image_vimage_eq bk_img_subset
    by (simp add: Int_absorb1)
  have 2: " (\<And>x. x \<in> {a..b} - (s \<union> g -` s') \<Longrightarrow> (g has_real_derivative g' x) (at x within {a..b}))"
    using deriv by auto
  have 3: "inj_on g ({a..b} \<union> ((s \<union> ({c..d} \<inter> g -` s'))))"
    using g(4)
    unfolding inj_on_def
    by auto
  show ?thesis
    apply(rule has_integral_substitution_general_[OF 0 assms(2) subset f(1) 1 g(1) 3 2])
    using bk_img_subset
    by auto
qed


lemma has_integral_substitution_general_'':
  fixes f :: "real \<Rightarrow> 'a::euclidean_space" and g :: "real \<Rightarrow> real"
  assumes s: "finite s" and le: "a \<le> b" and s': "finite s'"
    and subset: "g ` {a..b} \<subseteq> {c..d}"
    and f: "f integrable_on {c..d}" "continuous_on ({c..d} - s') f"
    and g : "continuous_on {a..b} g" "\<forall>x\<in>s'. finite (g -` {x})" "surj_on s' g" "inj_on g ({a..b} \<union> ((s \<union> g -` s')))"
    and deriv [derivative_intros]:
    "\<And>x. x \<in> {a..b} - s \<Longrightarrow> (g has_field_derivative g' x) (at x within {a..b})"
  shows "((\<lambda>x. g' x *\<^sub>R f (g x)) has_integral (integral {g a..g b} f - integral {g b..g a} f)) {a..b}"
proof-
  have a: "g -` s' = \<Union>{t. \<exists>x. t = g -` {x} \<and> x \<in> s'}"
    using s s' by blast
  have "finite (\<Union>{t. \<exists>x. t = g -` {x} \<and> x \<in> s'})" using s'
    by (metis (no_types, lifting) \<open>g -` s' = \<Union>{g -` {x} |x. x \<in> s'}\<close> finite_UN_I g(2) vimage_eq_UN)
  then have 0: "finite (s \<union> (g -` s'))"
    using a s
    by simp
  have 1: "continuous_on ({c..d} - g ` (s \<union> g -` s')) f"
    using f(2) surj_on_image_vimage_eq
    by (metis Diff_mono Un_upper2 continuous_on_subset equalityE g(3) image_Un)
  have 2: " (\<And>x. x \<in> {a..b} - (s \<union> g -` s') \<Longrightarrow> (g has_real_derivative g' x) (at x within {a..b}))"
    using deriv by auto
  show ?thesis using has_integral_substitution_general_[OF 0 assms(2) subset f(1) 1 g(1) g(4) 2]
    by auto
qed

lemma has_integral_substitution_general_'''':
  fixes f :: "real \<Rightarrow> 'a::euclidean_space" and g :: "real \<Rightarrow> real"
  assumes s: "finite s" and le: "a \<le> b" and s': "finite s'"
    and subset: "g ` {a..b} \<subseteq> {c..d}"
    and f: "f integrable_on {c..d}" "continuous_on ({c..d} - g ` ({a..b} \<inter> g -` s')) f"
    and g : "continuous_on {a..b} g" "\<forall>x\<in>s'. finite (g -` {x})" "surj_on s' g" "inj_on g ({a..b} \<union> (({c..d} \<inter> s) \<union> (g -` s')))"
    and deriv:
    "\<And>x. x \<in> {a..b} - ({c..d} \<inter> s) \<Longrightarrow> (g has_field_derivative g' x) (at x within {a..b})"
    (*and bk_img_subset: "g -` s' \<subseteq> {c..d}"*)
  shows "((\<lambda>x. g' x *\<^sub>R f (g x)) has_integral (integral {g a..g b} f - integral {g b..g a} f)) {a..b}"
proof-
  have a: "g -` s' = \<Union>{t. \<exists>x. t = g -` {x} \<and> x \<in> s'}"
    using s s' by blast
  have "finite (\<Union>{t. \<exists>x. t = g -` {x} \<and> x \<in> s'})" using s'
    using g(2) by auto
  then have 0: "finite ((({c..d} \<inter> s) \<union> ({a..b} \<inter> g -` s')))"
    using a s
    by simp
  have 1: "continuous_on ({c..d} - (g ` (({c..d} \<inter> s) \<union> ({a..b} \<inter> g -` s')))) f"
    using f(2) surj_on_image_vimage_eq
  by (smt Diff_mono Un_upper2 a box_real(2) continuous_on_subset inf.commute subset_image_iff subset_refl)
  have 2: " (\<And>x. x \<in> {a..b} - ({c..d} \<inter> s \<union> {a..b} \<inter> g -` s')  \<Longrightarrow> (g has_real_derivative g' x) (at x within {a..b}))"
    using deriv by auto
  have 3: "inj_on g ({a..b} \<union> ({c..d} \<inter> s \<union> {a..b} \<inter> g -` s'))"
    using g(4)
    unfolding inj_on_def
    by auto
  show ?thesis
    apply(rule has_integral_substitution_general_[OF 0 assms(2) subset f(1) 1 g(1) 3 2])
    by auto
qed

lemma has_integral_indicator:
  fixes f :: "'a :: euclidean_space \<Rightarrow> 'b :: banach"
  assumes "S \<subseteq> T"
  shows "((\<lambda>x.  indicator S x *\<^sub>R f x) has_integral i) T \<longleftrightarrow> (f has_integral i) S"
  unfolding indicator_def
proof -
  have *: "\<And>x. (if x \<in> T then (if x \<in> S then 1 else 0) *\<^sub>R f x else 0) =  (if x\<in>S then f x else 0)"
    using assms by auto
  show "((\<lambda>x. (if x \<in> S then 1 else 0) *\<^sub>R f x) has_integral i) T = (f has_integral i) S"
    apply (subst(2) has_integral')
    apply (subst has_integral')
      apply (simp add: *)
    done
qed

corollary has_integral_indicator_UNIV:
  fixes f :: "'n::euclidean_space \<Rightarrow> 'a::banach"
  shows "((\<lambda>x.  indicator s x *\<^sub>R f x) has_integral i) UNIV \<longleftrightarrow> (f has_integral i) s"
  by (auto simp add: has_integral_indicator)

lemma has_integral_restrict_Int:
  fixes f :: "'a :: euclidean_space \<Rightarrow> 'b :: banach"
  shows "((\<lambda>x. if x \<in> S then f x else 0) has_integral i) T \<longleftrightarrow> (f has_integral i) (S \<inter> T)"
proof -
  have "((\<lambda>x. if x \<in> T then if x \<in> S then f x else 0 else 0) has_integral i) UNIV =
        ((\<lambda>x. if x \<in> S \<inter> T then f x else 0) has_integral i) UNIV"
    by (rule has_integral_cong) auto
  then show ?thesis
    using has_integral_restrict_UNIV by fastforce
qed

lemma has_integral_indicator_Int:
  fixes f :: "'a :: euclidean_space \<Rightarrow> 'b :: banach"
  shows "((\<lambda>x.  indicator S x *\<^sub>R f x) has_integral i) T \<longleftrightarrow> (f has_integral i) (S \<inter> T)"
proof -
  have "((\<lambda>x. indicator (S \<inter> T) x *\<^sub>R f x) has_integral i) UNIV =
        ((\<lambda>x. indicator T x *\<^sub>R (indicator S x *\<^sub>R f x)) has_integral i) UNIV"
    by (rule has_integral_cong) (auto simp add: indicator_def)
  then show ?thesis
    using has_integral_indicator_UNIV by fastforce
qed

lemma integral_indicator_Int:
  fixes f :: "'a :: euclidean_space \<Rightarrow> 'b :: banach"
  shows "integral T (\<lambda>x.  indicator S x *\<^sub>R f x) = integral (S \<inter> T) f"
  by (metis (mono_tags) has_integral_iff has_integral_indicator_Int not_integrable_integral)

proposition has_absolute_integral_change_of_variables_invertible_1:
  fixes f :: "real^'m::{finite,wellorder} \<Rightarrow> real^'n" and g :: "real^'m::_ \<Rightarrow> real^'m::_"
  assumes der_g: "\<And>x. x \<in> S \<Longrightarrow> (g has_derivative g' x) (at x within S)"
      and hg: "\<And>x. x \<in> S \<Longrightarrow> h(g x) = x"
      and conth: "continuous_on (g ` S) h"
      and "(\<lambda>x. \<bar>det (matrix (g' x))\<bar> *\<^sub>R f(g x)) absolutely_integrable_on S"
      and "integral S (\<lambda>x. \<bar>det (matrix (g' x))\<bar> *\<^sub>R f(g x)) = b"
  shows "f absolutely_integrable_on (g ` S) \<and> integral (g ` S) f = b"
  using has_absolute_integral_change_of_variables_invertible[OF assms(1-3)] assms by auto

proposition has_absolute_integral_change_of_variables_invertible_2:
  fixes f :: "real^'m::{finite,wellorder} \<Rightarrow> real^'n" and g :: "real^'m::_ \<Rightarrow> real^'m::_"
  assumes der_g: "\<And>x. x \<in> S \<Longrightarrow> (g has_derivative g' x) (at x within S)"
      and hg: "\<And>x. x \<in> S \<Longrightarrow> h(g x) = x"
      and conth: "continuous_on (g ` S) h"
      and "f absolutely_integrable_on (g ` S)"
      and "integral (g ` S) f = b"
  shows "(\<lambda>x. \<bar>det (matrix (g' x))\<bar> *\<^sub>R f(g x)) absolutely_integrable_on S \<and> integral S (\<lambda>x. \<bar>det (matrix (g' x))\<bar> *\<^sub>R f(g x)) = b"
  using has_absolute_integral_change_of_variables_invertible[OF assms(1-3)] assms by auto

lemma integral_on_1_eq:
  fixes f :: "'a::euclidean_space \<Rightarrow> real^1"
  shows "integral S f = axis (1::1) (integral S (\<lambda>x. f x $ 1))" 
proof (cases "f integrable_on S")
  case True
  with integrable_on_1_iff show ?thesis
    by (simp add: vec_eq_iff)
next
  case False
  with integrable_on_1_iff show ?thesis
    by (simp add: integrable_on_1_iff not_integrable_integral)
qed

lemma absolutely_integrable_on_1_iff':
  fixes f :: "'a::euclidean_space \<Rightarrow> real"
  shows "f absolutely_integrable_on S \<longleftrightarrow> (\<lambda>x. axis (1::1) (f x)) absolutely_integrable_on S"
  unfolding absolutely_integrable_on_def
  by (auto simp: integrable_on_1_iff norm_real)

lemma integral_on_1_eq':
  fixes f :: "'a::euclidean_space \<Rightarrow> real^1"
  shows "(integral S f) $1 = (integral S (\<lambda>x. f x $ 1))"
proof (cases "f integrable_on S")
  case True
  with integrable_on_1_iff show ?thesis
    by (simp add: vec_eq_iff)
next
  case False
  with integrable_on_1_iff show ?thesis
    by (simp add: integrable_on_1_iff not_integrable_integral)
qed

lemma integral_on_1_eq'':
  fixes f :: "'a::euclidean_space \<Rightarrow> real"
  shows "(integral S (\<lambda>x. axis (1::1) (f x))) = axis (1::1) (integral S f)"
proof (cases "f integrable_on S")
  case True
  with integrable_on_1_iff show ?thesis
    by (metis (mono_tags, lifting) Henstock_Kurzweil_Integration.integral_cong Integrals.integral_on_1_eq axis_nth)
next
  case False
  with integrable_on_1_iff show ?thesis
    by (simp add: integrable_on_1_iff not_integrable_integral)
qed

proposition has_absolute_integral_change_of_variables_invertible_pair_1:
  fixes f :: "(real\<times>real) \<Rightarrow> real" and g :: "(real\<times>real) \<Rightarrow> (real\<times>real)"
  assumes der_g: "\<And>x. x \<in> S \<Longrightarrow> (g has_derivative g' x) (at x within S)"
    and hg: "\<And>x. x \<in> S \<Longrightarrow> h(g x) = x"
    and conth: "continuous_on (g ` S) h"
    and abs_integ: "(\<lambda>x. \<bar>det (matrix (embed_vec \<circ> g' x \<circ> embed_pair))\<bar> *\<^sub>R f(g x)) absolutely_integrable_on S"
    and integral_eq_b: "integral S (\<lambda>x. \<bar>det (matrix (embed_vec \<circ> g' x \<circ> embed_pair))\<bar> *\<^sub>R f(g x)) = b"
  shows "f absolutely_integrable_on (g ` S)" (is ?g1) "integral (g ` S) f = b" (is ?g2)
proof-
  (*have det_eq: "det (matrix (embed_vec \<circ> g' x \<circ> embed_pair)) = (Det g' x)" for x sorry*)
  have "embed_pair ` embed_vec ` S = S"
    unfolding embed_pair_def embed_vec_def
    by (auto simp add: inner_add_left inner_axis_axis image_def)
  then have i: "(embed_vec \<circ> g \<circ> embed_pair) ` embed_vec ` S = embed_vec ` g ` S" apply (auto simp add: o_def)
    by (metis imageI image_image)
  have ii: "\<And>f. f = f o embed_pair o embed_vec" unfolding embed_pair_def embed_vec_def by (auto simp add: o_def inner_add_left inner_axis_axis)
  have a: "(axis (1::1) o f \<circ> embed_pair absolutely_integrable_on (embed_vec \<circ> g \<circ> embed_pair) ` embed_vec ` S) = ((axis (1::1) o f) absolutely_integrable_on (g ` S))"
    apply(simp only: i, rewrite in "_ = \<hole>" ii[where f = "axis (1::1) o  f"]) using eq_absolutely_integrable_on_pair_Vec[where g = "axis (1::1) \<circ> f \<circ> embed_pair" and S = "g ` S"] by force
      (*apply - supply[[unify_trace_failure]] apply assumption apply (erule asm_rl)*)
  have b: "integral ((embed_vec \<circ> g \<circ> embed_pair) ` embed_vec ` S) (axis (1::1) o f \<circ> embed_pair) = (integral (g ` S) (axis (1::1) o f))"
    using i ii eq_integral_pair_Vec integral_on_1_eq
    by metis
  let ?det_fun = "\<lambda>x. \<bar>det (matrix (embed_vec \<circ> g' x \<circ> embed_pair))\<bar>"
  have int_axis: "integral S (\<lambda>x. axis (1::1) (?det_fun x *\<^sub>R f (g x))) = axis (1::1) b"
    using integral_eq_b integral_on_1_eq integral_on_1_eq'' by auto
  have "(axis (1::1) o f) absolutely_integrable_on (g ` S) \<and> integral (g ` S) (axis (1::1) o f) = (axis (1::1) b)" 
    apply(rule has_absolute_integral_change_of_variables_invertible_1[where f = "axis (1::1) o f o embed_pair" and g = "embed_vec o g o embed_pair" and
          h = "embed_vec o h o embed_pair" and S = "embed_vec ` S" and g' = "\<lambda>x. (embed_vec o (g' (embed_pair x)) o embed_pair)", unfolded a b])
  proof-
    show "(embed_vec \<circ> h \<circ> embed_pair) ((embed_vec \<circ> g \<circ> embed_pair) x) = x" if "x \<in> embed_vec ` S" for x
      using hg that
      by(auto simp add: embed_vec_def embed_pair_def o_def inner_add_left inner_axis_axis)
    show "(embed_vec \<circ> g \<circ> embed_pair has_derivative embed_vec \<circ> g' (embed_pair x) \<circ> embed_pair) (at x within embed_vec ` S)" if "x \<in> embed_vec ` S" for x
    proof-
      obtain p where p: "x = embed_vec p" "p \<in> S"
        using \<open>x \<in> embed_vec ` S\<close> by auto
      have rw: "embed_vec \<circ> g' (embed_pair (embed_vec p)) \<circ> embed_pair \<circ> embed_vec = embed_vec \<circ> (g' p)"
        "embed_vec \<circ> g \<circ> embed_pair \<circ> embed_vec = embed_vec \<circ> g" "(embed_pair (embed_vec p)) = p"
        unfolding embed_pair_def embed_vec_def by(auto simp add: o_def inner_add_left inner_axis_axis)
      show ?thesis
        unfolding p(1)
        using has_derivative_within_eq_pair_Vec'' der_g[OF p(2)] rw
        by (metis ii pre_has_derivative_within_eq_pair_Vec'')
    qed
    show "continuous_on ((embed_vec \<circ> g \<circ> embed_pair) ` embed_vec ` S) (embed_vec \<circ> h \<circ> embed_pair)"
      using conth pre_eq_continuous_on_pair_Vec eq_continuous_on_pair_Vec
      by (metis ii image_comp)
    have pair_vec_id: "embed_pair (embed_vec x) = x" for x
      unfolding embed_pair_def embed_vec_def
      by (auto simp add: inner_add_left inner_axis_axis)
    have "x *\<^sub>R (axis 1 b) = (axis 1 (x *\<^sub>R b))" for x and b::"real"
      by (simp add: cart_eq_inner_axis inner_axis_axis vec_eq_iff)
    then have axis_scale: "(\<lambda>x. \<bar>det (matrix (embed_vec \<circ> g' (embed_pair x) \<circ> embed_pair)) \<bar> *\<^sub>R 
                                                (axis (1::1) \<circ> f \<circ> embed_pair) ((embed_vec \<circ> g \<circ> embed_pair) x)) =
                                     (\<lambda>x. (axis (1::1) (\<bar>det (matrix (embed_vec \<circ> g' (embed_pair x) \<circ> embed_pair))\<bar> *\<^sub>R (f \<circ> g \<circ> embed_pair) x)))"
      using pair_vec_id unfolding embed_pair_def embed_vec_def by(auto simp add: o_def inner_add_left inner_axis_axis)
    show "(\<lambda>x. \<bar>det (matrix (embed_vec \<circ> g' (embed_pair x) \<circ> embed_pair))\<bar> *\<^sub>R (axis (1::1) \<circ> f \<circ> embed_pair) ((embed_vec \<circ> g \<circ> embed_pair) x)) absolutely_integrable_on embed_vec ` S"
    proof-
      have i: "(\<lambda>x. (axis (1::1) (\<bar>det (matrix (embed_vec \<circ> g' (embed_pair x) \<circ> embed_pair))\<bar> *\<^sub>R (f \<circ> g \<circ> embed_pair) x))) absolutely_integrable_on embed_vec ` S"
        using pair_vec_id conth eq_absolutely_integrable_on_pair_Vec[where g = "(\<lambda>x. axis 1 (?det_fun (embed_pair x) *\<^sub>R (f \<circ> g \<circ> embed_pair) x))" and S = S] abs_integ
          absolutely_integrable_on_1_iff'[where f = "(\<lambda>x. (?det_fun (embed_pair x) *\<^sub>R (f \<circ> g \<circ> embed_pair) x)) o embed_vec"]
        by (auto simp add: o_def)
      show ?thesis
        using i axis_scale by metis
    qed
    show "integral (embed_vec ` S) (\<lambda>x. \<bar>det (matrix (embed_vec \<circ> g' (embed_pair x) \<circ> embed_pair))\<bar> *\<^sub>R (axis (1::1) \<circ> f \<circ> embed_pair) ((embed_vec \<circ> g \<circ> embed_pair) x)) = axis (1::1) b"
      unfolding axis_scale int_axis[symmetric]
      using  integral_on_1_eq eq_integral_pair_Vec[where g = " (\<lambda>x. axis 1 (?det_fun (embed_pair x) *\<^sub>R (f \<circ> g \<circ> embed_pair) x))"]
      by(auto simp add: o_def pair_vec_id)
  qed
  then show ?g1 ?g2
    using absolutely_integrable_on_1_iff[where f = "axis (1::1) \<circ> f"] integral_on_1_eq'[where  f = "axis (1::1) \<circ> f"] apply (auto simp add: o_def) using axis_nth by metis
qed

proposition has_absolute_integral_change_of_variables_invertible_pair_2:
  fixes f :: "(real\<times>real) \<Rightarrow> real" and g :: "(real\<times>real) \<Rightarrow> (real\<times>real)"
  assumes der_g: "\<And>x. x \<in> S \<Longrightarrow> (g has_derivative g' x) (at x within S)"
    and hg: "\<And>x. x \<in> S \<Longrightarrow> h(g x) = x"
    and conth: "continuous_on (g ` S) h"
    and abs_integ: "f absolutely_integrable_on (g ` S)"
    and integral_eq_b: "integral (g ` S) f = b"
  shows "(\<lambda>x. \<bar>det (matrix (embed_vec \<circ> g' x \<circ> embed_pair))\<bar> *\<^sub>R f(g x)) absolutely_integrable_on S" (is ?g1) "integral S (\<lambda>x. \<bar>det (matrix (embed_vec \<circ> g' x \<circ> embed_pair))\<bar> *\<^sub>R f(g x)) = b" (is ?g2)
proof-
  (*have det_eq: "det (matrix (embed_vec \<circ> g' x \<circ> embed_pair)) = (Det g' x)" for x sorry*)
  have "embed_pair ` embed_vec ` S = S"
    unfolding embed_pair_def embed_vec_def
    by (auto simp add: inner_add_left inner_axis_axis image_def)
  then have i: "embed_vec ` (g \<circ> embed_pair) ` embed_vec ` S = embed_vec ` g ` S"
    "(g \<circ> embed_pair) ` embed_vec ` S = g ` S"
    "(embed_vec \<circ> g \<circ> embed_pair) ` embed_vec ` S = embed_vec ` g ` S"
      apply (auto simp add: o_def)
    by (metis imageI image_image)+
  have ii: "embed_pair o embed_vec = id" unfolding embed_pair_def embed_vec_def by (auto simp add: o_def inner_add_left inner_axis_axis)
  let ?f_embed = "(\<lambda>x. \<bar>det (matrix (embed_vec \<circ> g' (embed_pair x) \<circ> embed_pair))\<bar> *\<^sub>R (axis (1::1) \<circ> f \<circ> embed_pair) ((embed_vec \<circ> g \<circ> embed_pair) x))"
  let ?det_fun = "(\<lambda>x. \<bar>det (matrix (embed_vec \<circ> g' x \<circ> embed_pair))\<bar>)"
  let ?det_fun_2 = "\<lambda>x. \<bar>det (matrix (embed_vec \<circ> g' (embed_pair x) \<circ> embed_pair))\<bar>"
  let ?vec_g_pair = "(embed_vec \<circ> g \<circ> embed_pair)"
  have scale_axis: "x *\<^sub>R (axis (1::1) b) = (axis 1 (x *\<^sub>R b))" for x and b::"real"
    by (simp add: cart_eq_inner_axis inner_axis_axis vec_eq_iff)
  moreover have "((\<lambda>x. (?det_fun x *\<^sub>R axis (1::1)  (f( g x)))) absolutely_integrable_on S) = (?f_embed absolutely_integrable_on embed_vec ` S)"
    using eq_absolutely_integrable_on_pair_Vec[where g = ?f_embed and S = "S"]
    by(simp add: o_def embed_pair_def embed_vec_def inner_add_left inner_axis_axis)
  ultimately have a: "... = (axis (1::1) o (\<lambda>x. (?det_fun x *\<^sub>R (f( g x)))) absolutely_integrable_on S)"
    unfolding o_def
    by simp
  have b: "integral (embed_vec ` S) (\<lambda>x. ?det_fun_2 x *\<^sub>R (axis (1::1) \<circ> f \<circ> embed_pair) (?vec_g_pair x)) =
                   integral S (axis (1::1) o (\<lambda>x. (?det_fun x *\<^sub>R  (f (g x)))))"
    using eq_integral_pair_Vec[where g = "(\<lambda>x. ?det_fun_2 x *\<^sub>R (axis (1::1) \<circ> f \<circ> embed_pair) (?vec_g_pair x))"]
    by(simp add: o_def embed_pair_def embed_vec_def inner_add_left inner_axis_axis scale_axis)
  let ?vec_h_pair = "(embed_vec \<circ> h \<circ> embed_pair)"
  let ?vec_g'_pair = "\<lambda>x. (embed_vec o (g' (embed_pair x)) o embed_pair)"
  have "(axis (1::1) o (\<lambda>x. ?det_fun x *\<^sub>R f(g x))) absolutely_integrable_on S \<and>
         integral S (axis (1::1) o  (\<lambda>x. ?det_fun x *\<^sub>R f(g x))) = axis (1::1) b"
    apply(rule has_absolute_integral_change_of_variables_invertible_2[where f = "axis (1::1) o f o embed_pair" and g = ?vec_g_pair and
          h = ?vec_h_pair and S = "embed_vec ` S" and g' = ?vec_g'_pair, unfolded a b])
  proof-
    show "?vec_h_pair (?vec_g_pair x) = x" if "x \<in> embed_vec ` S" for x
      using hg that
      by(auto simp add: embed_vec_def embed_pair_def o_def inner_add_left inner_axis_axis)
    show "(?vec_g_pair has_derivative embed_vec \<circ> g' (embed_pair x) \<circ> embed_pair) (at x within embed_vec ` S)" if "x \<in> embed_vec ` S" for x
    proof-
      obtain p where p: "x = embed_vec p" "p \<in> S"
        using \<open>x \<in> embed_vec ` S\<close> by auto
      have rw: "embed_vec \<circ> g' (embed_pair (embed_vec p)) \<circ> embed_pair \<circ> embed_vec = embed_vec \<circ> (g' p)"
        "?vec_g_pair \<circ> embed_vec = embed_vec \<circ> g" "(embed_pair (embed_vec p)) = p"
        unfolding embed_pair_def embed_vec_def by(auto simp add: o_def inner_add_left inner_axis_axis)
      show ?thesis
        unfolding p(1)
        using has_derivative_within_eq_pair_Vec'' der_g[OF p(2)] rw
        by (metis ii pre_has_derivative_within_eq_pair_Vec'')
    qed
    show "continuous_on (?vec_g_pair ` embed_vec ` S) (embed_vec \<circ> h \<circ> embed_pair)"
      using conth pre_eq_continuous_on_pair_Vec   using ii image_comp 
      by (metis eq_continuous_on_Vec_pair i(3) id_comp)
    have pair_vec_id: "embed_pair (embed_vec x) = x" for x
      unfolding embed_pair_def embed_vec_def
      by (auto simp add: inner_add_left inner_axis_axis)
    have "x *\<^sub>R (axis 1 b) = (axis 1 (x *\<^sub>R b))" for x and b::"real"
      by (simp add: cart_eq_inner_axis inner_axis_axis vec_eq_iff)
    then have axis_scale: "(\<lambda>x. ?det_fun_2 x *\<^sub>R (axis (1::1) \<circ> f \<circ> embed_pair) (?vec_g_pair x)) =
                                  (\<lambda>x. (axis (1::1) (?det_fun_2 x *\<^sub>R (f \<circ> g \<circ> embed_pair) x)))"
      using pair_vec_id unfolding embed_pair_def embed_vec_def by(auto simp add: o_def inner_add_left inner_axis_axis)
    show "axis (1::1) \<circ> f \<circ> embed_pair absolutely_integrable_on (embed_vec \<circ> g \<circ> embed_pair) ` embed_vec ` S"
    proof-
      have rw: "(\<lambda>x. f (g (embed_pair x))) ` embed_vec ` S  =  f ` g ` S" for f
        unfolding embed_pair_def embed_vec_def
        by (auto simp add: inner_add_left inner_axis_axis image_def, metis old.prod.exhaust)
      have "axis (1::1) \<circ> f \<circ> embed_pair absolutely_integrable_on ?vec_g_pair ` embed_vec ` S"
        using eq_absolutely_integrable_on_pair_Vec[where g = "axis (1::1) \<circ> f \<circ> embed_pair" and S = "(g \<circ> embed_pair) ` embed_vec ` S", unfolded i]
        unfolding o_def absolutely_integrable_on_1_iff'[symmetric] pair_vec_id rw
        using abs_integ
        by simp
      then show ?thesis
        using axis_scale by metis
    qed
    show "integral (?vec_g_pair ` embed_vec ` S) (axis (1::1) \<circ> f \<circ> embed_pair) = axis (1::1) b"
      unfolding axis_scale i integral_eq_b[symmetric] eq_integral_pair_Vec[where g = "(axis 1 \<circ> f \<circ> embed_pair)"]
      apply(simp add: id_def comp_assoc ii)
      apply(simp add:   o_def) using  integral_on_1_eq'' by blast
  qed
  then show ?g1 ?g2
    using absolutely_integrable_on_1_iff[where f = "axis (1::1) \<circ> (\<lambda>x. ?det_fun x *\<^sub>R f (g x))"]
          integral_on_1_eq'[where  f = "axis (1::1) \<circ> (\<lambda>x. ?det_fun x *\<^sub>R f (g x))"] 
     apply (auto simp add: o_def) using axis_nth
    by (metis (mono_tags, lifting))
qed

proposition has_absolute_integral_change_of_variables_invertible_pair:
  fixes f :: "(real\<times>real) \<Rightarrow> real" and g :: "(real\<times>real) \<Rightarrow> (real\<times>real)"
  assumes der_g: "\<And>x. x \<in> S \<Longrightarrow> (g has_derivative g' x) (at x within S)"
    and hg: "\<And>x. x \<in> S \<Longrightarrow> h(g x) = x"
    and conth: "continuous_on (g ` S) h"
  shows "(\<lambda>x. \<bar>det (matrix (embed_vec \<circ> g' x \<circ> embed_pair))\<bar> *\<^sub>R f(g x)) absolutely_integrable_on S \<and>
         integral S (\<lambda>x. \<bar>det (matrix (embed_vec \<circ> g' x \<circ> embed_pair))\<bar> *\<^sub>R f(g x)) = b \<longleftrightarrow>
           f absolutely_integrable_on (g ` S)\<and> integral (g ` S) f = b"
  using has_absolute_integral_change_of_variables_invertible_pair_1[OF assms] has_absolute_integral_change_of_variables_invertible_pair_2[OF assms]
  by meson

proposition has_absolute_integral_change_of_variables_invertible_pair':
  fixes f :: "(real\<times>real) \<Rightarrow> real" and g :: "(real\<times>real) \<Rightarrow> (real\<times>real)"
  assumes der_g: "\<And>x. x \<in> S \<Longrightarrow> (g has_derivative g' x) (at x within S)"
    and hg: "\<And>x. x \<in> S \<Longrightarrow> h(g x) = x"
    and conth: "continuous_on (g ` S) h"
  shows "(\<lambda>x. \<bar>det (matrix (embed_vec \<circ> g' x \<circ> embed_pair))\<bar> *\<^sub>R f(g x)) absolutely_integrable_on S \<longleftrightarrow> f absolutely_integrable_on (g ` S)"
  using has_absolute_integral_change_of_variables_invertible_pair_1[OF assms] has_absolute_integral_change_of_variables_invertible_pair_2[OF assms]
  by meson

lemma absolutely_integrable_imp_lborel_integrable: "f \<in> borel_measurable lborel \<Longrightarrow> (f absolutely_integrable_on UNIV) \<Longrightarrow> integrable lborel f"
  apply(auto simp add: indicator_def set_integrable_def)
  using  integrable_completion measurable_lborel2 by blast

lemma lborel_integrable_imp_absolutely_integrable: "integrable lborel f \<Longrightarrow> (f absolutely_integrable_on UNIV)"
  apply(auto simp add: indicator_def set_integrable_def)
  using borel_measurable_integrable integrable_completion integrable_iff_bounded by blast

lemma absolutely_integrable_eq_lborel_integrable: "f \<in> borel_measurable lborel \<Longrightarrow> integrable lborel f = (f absolutely_integrable_on UNIV)"
  using absolutely_integrable_imp_lborel_integrable lborel_integrable_imp_absolutely_integrable by auto

lemma orthogonal_embed_vec_compose: "orthogonal_transformation f \<Longrightarrow> orthogonal_transformation (embed_vec o f)"
    unfolding orthogonal_transformation_def Modules.additive_def linear_iff embed_vec_def embed_pair_def
    apply (auto simp add: o_def scaleR_add_right scaleR_add_left inner_add_right inner_add_left algebra_simps inner_axis_axis)
    subgoal for v w apply(cases "f v", cases "f w", auto) 
      by (metis inner_Pair inner_real_def)
    done

lemma orthogonal_compose_embed_pair: "orthogonal_transformation f \<Longrightarrow> orthogonal_transformation (f o embed_pair)"
    unfolding orthogonal_transformation_def linear_iff Modules.additive_def embed_vec_def embed_pair_def
    apply (auto simp add: o_def scaleR_add_right scaleR_add_left inner_add_right inner_add_left algebra_simps inner_axis_axis)
    by (metis (mono_tags, lifting) cart_eq_inner_axis inner_real_def inner_vec_def sum_2)

lemma a_leq_q_int_0: "integral {b::real..a} f = 0" if "a \<le> b" 
proof-
  have "{b..a} = {} \<or> {b..a} = {a}" using that by auto
  then show ?thesis
  by auto
qed

end
