\section{Usability of the Formalised Statement}

%The generality of a statement of Green's theorem can be judged by the analytical and geometric assumptions of the statement.
%The analytical assumptions we make are as general as the most general statement of the theorem in recent work~\cite{jurkat1990general}.
%However, the geometric assumptions that we formalised are marginally less general than assumptions that are usually present in text book presentations of the theorem: we assume that the type~I and type~II divisions are obtained by only vertical and horizontal divisions, respectively.
In this section we investigate the applicability of the statement that we formalised, and the \textit{practical} impact of the extra geometric assumptions.
We first derive the following special case of the theorem.

\begin{isabelle}
\isacommand{lemma}\isamarkupfalse%
\ GreenThm\_typeI\_typeII\_divisible\_region\_finite\_holes:\isanewline
\ \isakeyword{assumes}\ valid\_cube\_boundary:\ "{\isasymforall}(k,{\isasymgamma}){\isasymin}boundary\ C.\ valid\_path\ {\isasymgamma}"\ \isakeyword{and}\isanewline
\ \ \ \ only\_vertical\_division:\isanewline
\ \ \ \ "only\_vertical\_division\ ({\isasympartial}C)\ two\_chain\_typeI"\ \isakeyword{and}\isanewline
\ \ \ \ only\_horizontal\_division:\isanewline
\ \ \ \ "only\_vertical\_division\ ({\isasympartial}C)\ two\_chain\_typeII"\ \isakeyword{and}\isanewline
\ \ \ \ s\_is\_oneCube:\ "s\ =\ cubeImage\ (C)"\ \isakeyword{and}\isanewline
\ \ \ \ "two\_chain\_typeI\ =\ {(orient,\ C)}"\ "two\_chain\_typeII\ =\ {(orient,\ C)}"\isanewline
\isakeyword{shows}\isanewline
"\isactrlsup H{\isasymointegral}\isactrlbsub {\isasympartial}C\isactrlesub \ F\ {\isasymdownharpoonright}\isactrlbsub \{i,\ j\}\isactrlesub\ =\isanewline\isanewline
\ \ \ orient\ *\isanewline
\ \ \ \ \ \  integral\ (cubeImage\ C)\ ({\isasymlambda}x.\ ({\isasympartial}\ F\isactrlbsub j\isactrlesub \ /\ {\isasympartial}\ i\ {\isacharbar}\isactrlbsub x\isactrlesub )\ -\ ({\isasympartial}\ F\isactrlbsub i\isactrlesub \ /\ {\isasympartial}\ j\ {\isacharbar}\isactrlbsub x\isactrlesub ))"
\end{isabelle}

We derived it based on the reflexivity properties of the different subdivision and reparameterisation constructs that we defined earlier.
Except for the extra geometric assumptions, the statement above resembles the statement of Green's theorem as it would be rendered in classical textbook treatments.
It directly relates the double integral on a region to the line integral on the region's boundary, without using an intermediate subdivision like we did in the more general statement \isa{GreenThm\_typeI\_} \isa{typeII\_divisible\_region}, and with minimal reference to the homological concepts of chains and cubes.
This special case can be used primarily for regions without holes, like the regions considered by Harrison in his formalisation of Cauchy's integral theorem~\cite{harrison2007formalizing}.
According to Protter~\cite{protter2006basic}, the first and second geometric assumptions are equivalent to assuming that the region has piecewise-smooth boundaries.
%Accordingly, if Conjecture~\ref{conj:typeIvertical} is true (e.g. by a homologous assumption that excludes holes), then the above statement can be used to derive a statement of Cauchy's integral theorem that is at least as general as Harrison's~\cite{harrison2007formalizing}.

\subsection{Two Small Examples}

To demonstrate the practical utility of our formalised statement we apply it to two regions: a diamond and a disk.
In particular, we aim to study the complexity of proving that the geometric assumptions apply to realistic examples.
The reason motivating our focus on the geometric assumptions is that this is where the statement that we formalised is different with respect to other mainstream statements of the theorem.
%We start by describing the application to the diamond and then move on to the disk.
The first step is to formulate the diamond and the disk as 2-cubes.
We do so as follows, where the real number \isa{d} is the diameter for both shapes.
\begin{isabelle}
\isacommand{definition}\isamarkupfalse%
\ diamond\_cube\isanewline
\isakeyword{where}\isanewline
\ "diamond\_cube\ =\isanewline
\ \ \ \ ({\isasymlambda}(t{\isadigit{1}},t{\isadigit{2}}).\ let\ x\ =\ (t{\isadigit{1}}\ -\ {\isadigit{1}}/{\isadigit{2}})\ *\ d\ in\isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ (x,\ ({\isadigit{2}}\ *\ t{\isadigit{2}}\ -\ {\isadigit{1}})\ *\ (d/{\isadigit{2}}\ -\ {\isasymbar}x{\isasymbar})))"\isanewline

\isacommand{definition}\isamarkupfalse%
\ disk\_cube\isanewline
\isakeyword{where}\isanewline
\ "disk\_cube\ =\isanewline
\ \ \ \ ({\isasymlambda}(t{\isadigit{1}},t{\isadigit{2}}).\ let\ x\ =\ (t{\isadigit{1}}\ -\ {\isadigit{1}}/{\isadigit{2}})\ in\isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ (x\ *\ d,\ ({\isadigit{2}}\ *\ t{\isadigit{2}}\ -\ {\isadigit{1}})\ *\ d\ *\ sqrt\ ({\isadigit{1}}/{\isadigit{4}}\ -\ x\ *\ x)))"
\end{isabelle}
%The function \isa{x\_coord} returns the $x$ coordinate for both the diamond and the disk, while \isa{diamond\_y} and \isa{circle\_y} return $y$ coordinates for the diamond and the disk, respectively.
It should be clear that both of those parameterisations are type~I, because the $y$-coordinates for both are functions in the $x$-coordinate.
For the diamond, the four boundaries are shown in Figure~\ref{fig:diamEg}.
The top and bottom edges are each comprised of two line segments and the right and left edges are point paths (i.e. degenerate lines) as shown in Figures~\ref{fig:diam1}-\ref{fig:diam4}.
For the disk, the top and bottom edges are arcs instead of line segments, while the right and left edges are points too.

\begin{figure}
    \centering
    \begin{subfigure}[b]{0.15\textwidth}
           \includegraphics[width=1\textwidth]{diamond1}
           \caption[]{\label{fig:diam1}}
    \end{subfigure}
    \begin{subfigure}[b]{0.15\textwidth}
           \includegraphics[width=1\textwidth]{diamond2}
           \caption[]{\label{fig:diam2}}
    \end{subfigure}
    \begin{subfigure}[b]{0.15\textwidth}
           \includegraphics[width=1\textwidth]{diamond3}
           \caption[]{\label{fig:diam3}}
    \end{subfigure}
    \begin{subfigure}[b]{0.15\textwidth}
           \includegraphics[width=1\textwidth]{diamond4}
           \caption[]{\label{fig:diam4}}
    \end{subfigure}
    \begin{subfigure}[b]{0.15\textwidth}
           \includegraphics[width=1\textwidth]{diamond5}
           \caption[]{\label{fig:diam5}}
    \end{subfigure}
    \begin{subfigure}[b]{0.15\textwidth}
           \includegraphics[width=1\textwidth]{diamond6}
           \caption[]{\label{fig:diam6}}
    \end{subfigure}
    \begin{subfigure}[b]{0.15\textwidth}
           \includegraphics[width=1\textwidth]{diamond7}
           \caption[]{\label{fig:diam7}}
    \end{subfigure}
    \begin{subfigure}[b]{0.15\textwidth}
           \includegraphics[width=1\textwidth]{diamond8}
           \caption[]{\label{fig:diam8}}
    \end{subfigure}
    \begin{subfigure}[b]{0.15\textwidth}
           \includegraphics[width=1\textwidth]{diamond9}
           \caption[]{\label{fig:diam9}}
    \end{subfigure}
    \begin{subfigure}[b]{0.15\textwidth}
           \includegraphics[width=1\textwidth]{diamond10}
           \caption[]{\label{fig:diam10}}
    \end{subfigure}
    \begin{subfigure}[b]{0.15\textwidth}
           \includegraphics[width=1\textwidth]{diamond11}
           \caption[]{\label{fig:diam11}}
    \end{subfigure}
    \begin{subfigure}[b]{0.15\textwidth}
           \includegraphics[width=1\textwidth]{diamond12}
           \caption[]{\label{fig:diam12}}
    \end{subfigure}
\caption[]{\label{fig:diamEg} (a), (b), (c), and (d) show the left, top, bottom boundaries of the type~I diamond.
The left and right edges are degenerate straight lines i.e. points, thus qualifying it to be a type~I 2-cube.
(e), (f), (g), and (h) show the left, top, bottom boundaries of the type~II diamond.
The top and bottom edges are degenerate straight lines i.e. points, thus qualifying it to be a type~II 2-cube.
(i), (j), (k), and (l) show four 1-cubes that are a common subdivision between the non-degenerate part of the type~I diamond's boundary (i.e. (c) and (d)) and the vertical part of the type~II diamond's boundary.
Note that their orientations are different, but this is accounted for by the integer coefficients in the chain representation.}
\end{figure}
The first geometric assumption we need the diamond and the disk to conform to is that each of their four boundaries is piecewise differentiable.
Proving this was straightforward for the diamond since the four boundaries are comprised of straight lines and points, while for the disk it was slightly more tedious because of the presence of the square roots.

The second geometric assumption that we need to show for the regions is that they admit a type~I division obtained by only vertical slicing.
The type~I division that we use is a singleton: it is the parameterisation itself, i.e. \isa{\{diamond\_cube d\}} for the diamond and \isa{\{disk\_cube d\}} for the disk, which are both type I.
%There are no conceptual challenges in proving that, in the way we have formulated it, is a type~I region.
Proving that these type~I parameterisations are obtained only via introducing only vertical boundaries is easy, since we add no boundaries.

Next, we need to show the existence of a type~II subdivision that can be obtained by introducing \emph{only} horizontal boundaries.
Since both the diamond and disk are symmetric, the type II parameterisations can be the type I parameterisations but after i) reversing their orientation and ii) reflecting them on the identity line.
The way to obtain type~II parameterisations is shown below.
\begin{isabelle}
\isacommand{definition}\ typeI\_to\_typeII\isanewline
\isakeyword{where}\isanewline
\ "typeI\_to\_typeII\ C\ =\ prod.swap\ o\ C\ o\ ({\isasymlambda}(x,y).\ ({\isadigit{1}}\ -\ x,\ y))"
\end{isabelle}
In the above definition, the function \isa{prod.swap} takes an ordered pair and swaps its members and thus does the reflection on the identity line.
The function \isa{{\isasymlambda}(x,y).\ ({\isadigit{1}}\ -\ x,\ y)} inverts the orientation so that the resulting cube is type II (i.e. the resulting cube is type I w.r.t. \isa{-j} and \isa{i}), which is the right form for applying Green's theorem.
We show that the resulting cubes are type~II regions and that all of their boundaries are piecewise-smooth based on the following general result.
\begin{isabelle}
\isacommand{lemma}\isamarkupfalse%
\ typeI\_to\_typeII\_works:\isanewline
\ \ \ \ \ \isakeyword{assumes}\ "typeI\_twoCube\ (orient,\ C)\ i\ j"\isanewline
\ \ \ \ \ \isakeyword{shows}\ "\ typeI\_twoCube\ (orient,\ typeI\_to\_typeII\ C)\ (-j)\ i"
\end{isabelle}


Next we show that the type~II parameterisations are divisions of the type~I parameterisations: i.e. they represent the same domain in $\mathbb{R}^2$.
We first show that inverting the orientation of a cube preserves its image as follows.
\begin{isabelle}
\isacommand{lemma}\isamarkupfalse%
\ rev\_orient\_same\_img:\isanewline
\ \ \isakeyword{assumes}\ "typeI\_twoCube\ (orient,\ C)\ i\ j"\isanewline
\ \ \isakeyword{shows}\ "cubeImage\ C\ =\ cubeImage\ (C\ o\ \ ({\isacharpercent}(x,y).\ ({\isadigit{1}}\ -\ x,\ y)))"
\end{isabelle}
Then we show that reflecting the diamond (resp. the disk) on the identity line preserves its image.
\begin{isabelle}
\isacommand{lemma}\isamarkupfalse%
\ diamond\_swap\_eq\_img:\isanewline
\ \ \ \ "cubeImage\ (diamond\_cube)\ =\ cubeImage\ (prod.swap\ o\ diamond\_cube)"\isanewline

\isacommand{lemma}\isamarkupfalse%
\ disk\_swap\_eq\_img:\isanewline
\ \ \ \ \ \ \  "cubeImage\ (disk\_cube)\ =\ cubeImage\ (prod.swap\ o\ disk\_cube)"
\end{isabelle}

Proving this depends on mapping every point in \isa{unit\_cube} to another point in \isa{unit\_cube} such that the type~I parameterisation maps the first point to the same value to which the type~II parameterisation maps the second point, and vice versa.
Again, to prove this, there are no conceptual challenges, the only issue is that we need to manually identify bijections that do the aformentioned mapping for the disk and the diamond.

%% Below are the transformations that map the x and y coordinates of those points from the unit square to each other and the properties of such transformations.
%% \begin{isabelle}
%% \isacommand{abbreviation}\ "rot\_x\ d\ t1\ t2\ \isasymequiv\isanewline
%% \ \ \ \ \ \ \ \ \ \ \ \ \ (if\ (t1\ -\ 1/2)\ *\ d\ \isasymle \ 0\ then\ (2\ *\ t2\ -\ 1)\ *\ t1\ +\ 1/2\isanewline
%% \ \ \ \ \ \ \ \ \ \ \ \ \  else\ 2\ *\ t2\ -\ 2\ *\ t1\ *\ t2\ +t1\ -1/2)"\isanewline
%% \isacommand{lemma}\ rot\_x\_ivl:\isanewline
%% \ \ \isakeyword{assumes}\ "0\ <\ d"\ "0\ \isasymle \ x"\ "x\ \isasymle \ 1"\ "0\ \isasymle \ y"\ "y\ \isasymle \ 1"\ \isanewline
%% \ \ \isakeyword{shows}\ "0\ \isasymle \ rot\_x\ d\ x\ y\ \isasymand \ rot\_x\ d\ x\ y\ \isasymle \ 1"\isanewline
%% \isanewline
%% \isacommand{abbreviation}\ "rot\_y\ d\ t1\ t2\ \isasymequiv\isanewline
%% \ \ \ \ \ \ \ \ \ \ \ (t1 - 1/2) * d /\isanewline
%% \ \ \ \ \ \ \ \ \ \ \ (2\ *\ diamond\_y\ d\ (diamond\_x\ d\ (rot\_x\ d\ t1\ t2)))\ +\ 1/2"\isanewline
%% \isacommand{lemma}\ rot\_y\_ivl:\isanewline
%% \ \ \isakeyword{assumes}\ "0\ <\ d"\ "0\ \isasymle \ x"\ "x\ \isasymle \ 1"\ "0\ \isasymle \ y"\ "y\ \isasymle \ 1"\ \isanewline
%% \ \ \isakeyword{shows}\ "0\ \isasymle \ rot\_y\ d\ x\ y\ \isasymand \ rot\_y\ d\ x\ y\ \isasymle \ 1"\isanewline
%% \isanewline
%% \isacommand{lemma}\ diamond\_eq\_rot\_diamond:\isanewline
%% \ \ \isakeyword{assumes}\ "0\ <\ d"\ "0\ \isasymle \ x"\ "x\ \isasymle \ 1"\ "0\ \isasymle \ y"\ "y\ \isasymle \ 1"\ \isanewline
%% \ \ \isakeyword{shows}\ "(diamond\_cube\ d\ (x,\ y))\ =\isanewline
%% \ \ \ \ \ \ \ (rot\_diamond\_cube\ d\ (rot\_y\ d\ x\ y,\ rot\_x\ d\ x\ y))"\isanewline
%% \isacommand{lemma}\ rot\_diamond\_eq\_diamond:\isanewline
%% \ \ \isakeyword{assumes}\ "0\ <\ d"\ "0\ \isasymle \ x"\ "x\ \isasymle \ 1"\ "0\ \isasymle \ y"\ "y\ \isasymle \ 1"\isanewline
%% \ \ \isakeyword{shows}\ "(rot\_diamond\_cube\ d\ (x,\ y))\ =\isanewline
%% \ \ \ \ \ \ \ \ (diamond\_cube\ d\ (rot\_x\ d\ y\ x,\ rot\_y\ d\ y\ x))"
%% \end{isabelle}


A last and a relatively challenging goal to prove is that the type~I cube can be obtained by adding only vertical boundaries to the type~II reparameterisation.
For the diamond, formally this goal is shown in the next statement.

\begin{isabelle}
\isacommand{lemma}\isamarkupfalse%
\ diamond\_cube\_is\_only\_vertical\_div\_of\_rot:\isanewline
\ \ \isakeyword{shows}\ "only\_vertical\_division\ ({\isasympartial}diamond\_cube)\isanewline
 \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \  {({\isadigit{1}},\ typeI\_to\_typeII\ diamond\_cube)}" 
\end{isabelle}

To prove this we show that some subset of the 1-chain that forms the boundary of the type~I parameterisation has a common subdivision with \emph{all} the horizontal boundaries of the type~II parameterisation.
Every 1-cube in the rest of the type~I diamond's boundary has to either be i) a degenerate path (i.e. a path that maps $[0,1]$ to one point on the plane) or ii) a subpath of a 1-cube that is in the horizontal boundary of the type~II diamond.
As shown in Figure~\ref{fig:diamEg}, the boundary of the type~I diamond is made of two line segments and two degenerate paths.
We show that the 1-chain constituting those two line segments has a common 1-chain subdivision with the horizontal boundary of the type~II diamond, which is also made of two line segments, that are rotated nonetheless.
That common subdivision is shown in Figure~\ref{fig:diam9}-\ref{fig:diam12}.
Finally, the statement of Green's theorem for a diamond follows.

\begin{isabelle}
\isacommand{lemma}\isamarkupfalse%
\ GreenThm\_diamond:\isanewline
\ \ \isakeyword{assumes}\ "analytically\_valid\ (cubeImage\ (diamond\_cube))\ (F\isactrlbsub i\isactrlesub )\ j\ i"\isanewline
\ \ \ \ "analytically\_valid\ (cubeImage\ (diamond\_cube))\ (F\isactrlbsub j\isactrlesub )\ i\ j"\isanewline
\ \ \isakeyword{shows}\ "\isactrlsup H{\isasymintegral}\isactrlbsub {\isasympartial}diamond\_cube\isactrlesub \ F\ {\isasymdownharpoonright}\isactrlbsub \{i,\ j\}\isactrlesub =\isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ integral\ (cubeImage\ (diamond\_cube))\isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ ({\isasymlambda}x.\ {\isasympartial}\ F\isactrlbsub j\isactrlesub \ /\ {\isasympartial}\ i\ {\isacharbar}\isactrlbsub x\isactrlesub \ -\ {\isasympartial}\ F\isactrlbsub i\isactrlesub \ /\ {\isasympartial}\ j\ {\isacharbar}\isactrlbsub x\isactrlesub \ )"
\end{isabelle}

For the disk, showing the existence of a common subdivision between the boundary of its type~I parameterisation and the horizontal boundaries of the type II parameterisation is not possible.
This is because the boundaries of the two rotated parameterisations have different \emph{curve speeds}.%
\footnote{Rutter \cite{rutter2000geometry} explains curve speeds and velocities.}
Accordingly, instead of showing the existence of a common subdivision, we show the existence of a common reparameterisation between the two boundaries.
Since that common reparameterisation needs to be piecewise-smooth, we choose it to be an angular parameterisation since the \emph{velocity} of the Cartesian parameterisation of the disk arcs has a singularity.
The challenge in proving this is manually devising the mapping $\phi$ used to show the reparameterisation relation.

Another aspect of applying the theorem to the diamond and the disk that is worth mentioning is instantiating the vectors \isa{i} and \isa{j} on which the locale \isa{green\_typeI\_} \isa{typeII\_chain} and accordingly Green's theorem are parameterised.
We instantiate \isa{i} and \isa{j} to the concrete Cartesian basis \isa{(1,0)} and \isa{(0,1)}, respectively.
The most challenging part of this instantiation is proving that the vectors used for instantiation satisfy the assumptions of the locale \isa{i\_j\_orthonorm}.
The assumptions of \isa{i\_j\_} \isa{orthonorm\ } are all straightforward to prove, except for the assumption stating that Fubini's theorem applies to the vectors \isa{i} and \isa{j}.
We need to prove this twice, once for the vectors \isa{(1,0)} and \isa{(0,1)} (to satisfy the assumptions of the instantiation \isa{T1} in the locale \isa{green\_typeI\_typeII\_chain}) and another for the vectors \isa{-(0,1)} and \isa{(1,0)} (to satisfy the assumptions of \isa{T2}).
The proof for the vectors \isa{(1,0)} and \isa{(0,1)} is, albeit being tedious, a straightforward application of Fubini's theorem.
On the other hand, for the vectors \isa{-(0,1)} and \isa{(1,0)} the proof is trickier and it can be seen as proving Fubini's theorem on rotated basis vectors.
The way we do this rotation is using the multivariate change of variables theorem.
Overall, the proofs for this instantiation are 200 lines long, which is not too bad given that the instantiation using the Cartesian base vectors is quite reusable.
