#!/bin/bash
cp ../*.tex .
cp ../*.bib .
cp ../*.sty .
cp ../*.png .
cp ../*.cls .
cp ../*.clo .
cp ../*.bst .
awk 'FNR==1{print ""}1' ../*_snd.txt > response.txt
perl latexpand/latexpand paper.tex > out
rm *.tex
rm *~
mv out paper.tex
pdflatex paper.tex
bibtex paper
pdflatex paper.tex
pdflatex paper.tex
mkdir sources
./aaai_script.sh paper.tex
cp sources/*.sty .
rm -rf sources
rm __tmp*
rm JARFig*.png
rm *.blg
rm *.log
rm *.pdf
# gcc flatex.c -o flatten
# ./flatten diamBoundingFormalisationJAR.tex
#rm *.tex
#mv diamBoundingFormalisationJAR.flt diamBoundingFormalisationJAR.tex
# # copy used figs
#pdflatex diamBoundingFormalisationJAR.tex
#bibtex diamBoundingFormalisationJAR
#pdflatex diamBoundingFormalisationJAR.tex
#pdflatex diamBoundingFormalisationJAR.tex

# rm -rf /tmp/LaTeX_Source_Files
# mkdir /tmp/LaTeX_Source_Files
# cp -rf * /tmp/LaTeX_Source_Files
# cp full.pdf /tmp/abdulaziz.pdf
# cd /tmp
# rm LaTeX_Source_Files.zip
# zip -r LaTeX_Source_Files LaTeX_Source_Files
