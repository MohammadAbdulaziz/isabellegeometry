\section{Isabelle Prerequisites}
\label{sec:isabelle}

Before we proceed with explaining our formalisation, we first give a brief overview of some basic features of Isabelle.

\subsection{Axiomatic Type Classes}

Axiomatic type classes \cite{DBLP:conf/tphol/Wenzel97} are a powerful refinement of polymorphism, supporting principled overloading of notation and inheritance of common structures and their properties.

A \textit{type class} denotes a collection of types; a \textit{sort}  is a list of type classes and denotes their intersection. 
Each type variable has a sort and can be instantiated by any  type that belongs to all of the listed type classes.
An \textit{axiomatic type class} is a type class augmented  with axioms constraining the constants. We can refer to these  axioms in proofs, obtaining theorems specific to the type class.  To show that a type~$\tau$ is an
instance of a particular axiomatic type class, we verify the corresponding axioms.
These typically refer to overloaded constants, which we define for type~$\tau$ with the objective of satisfying the axioms.
Verifying the axioms for type~$\tau$ makes all the theorems proved for the type class immediately available for type~$\tau$. 

Isabelle/HOL uses type classes to organise the various numeric types (integers, rationals, complex numbers, etc.) and also a variety of topological concepts, such as metric spaces, topological spaces of various kinds and Euclidean spaces~\cite{isabelleanalysislib}. Most of the operators shown in the sequel, from the humble arithmetic operators to the various differentiation and integration operators, are overloaded through type classes.

\subsection{The Isabelle/HOL Analysis Library}

Isabelle/HOL is distributed with a comprehensive library covering topology, Euclidean spaces, complex analysis and much other material, mostly ported from the HOL Light multivariate analysis library~\cite{harrison2007formalizing,harrison-hol-light-euclidean}. Below we briefly introduce a few of the mathematical concepts used in the rest of the paper.
\begin{itemize}
\item In Isabelle/HOL Euclidean spaces are formalised as a type class.
  Most notably, it fixes the operators \isa{norm} and \isa{\isasymbullet} for the norm of a vector and the inner product of two vectors, respectively.
  \item A \isa{path} $\gamma$ is a continuous function over the closed interval $[0,1]$.
  A \isa{valid\_path} is also piecewise-continuously differentiable.
  The operator \isa{+++} joins two paths, yielding another path provided the endpoints meet.
  \item Functions \isa{path\_start} and \isa{path\_finish} return a path's start and finish, simply $\gamma(0)$ and $\gamma(1)$.
  \item The rectangle bounded by vectors \isa{a} and \isa{b} is written \isa{cbox a b}, which for $\mathbb R^n$ is the set $\{x \mid a_i \le x_i \le b_i \}$, where $v_i$ denotes the i-th component of a vector $v$.
  In the special case when the two vectors are real numbers \isa{a} and \isa{b}, the interval between them cab be written as \isa{\{a..b\}}.
  \item Isabelle/HOL's analysis library has formalisations of the Lebesgue integral and the Henstock-Kurzweil gauge integral.
\footnote{The gauge integral \cite{swartz-gauge} is a generalisation of the well-known Riemann integral.}
The predicate \isa{set\_integrable} takes a measure, a set, and a function and returns true if the function is Lebesgue integrable under the given measure on the given set. 
The function \isa{borel\_measurable} takes a measure and returns the set of functions that are measurable under that measure.
The predicate \isa{integrable\_on} takes a set and a function and returns true if the Henstock-Kurzweil gauge integral of the function on the given set exists.
The function \isa{integral} takes a set and a function and returns the Henstock-Kurzweil gauge integral of the function on the given set, assuming that such integral exists.
For more information on those constants please refer to \cite{holzl2011three,isabelleanalysislib}.
\end{itemize}


\section{Basic Concepts and Lemmas}
\label{sec:defs}
In this section we discuss the basic lemmas that we need to prove Green's theorem.
However, we first need to discuss two basic definitions needed to state the theorem statement: \emph{line integrals} and \emph{partial derivatives}.
Adapting these well-known concepts to Isabelle's analysis library required some thought and effort.

\subsection{Line Integrals}


For a vector field $F $ defined on a Euclidean space, a parameterised path~$\gamma$ in the same Euclidean space, and a set of vectors~$B$ in the same Euclidean space, we define the line integral of $F$ on $\gamma$ as follows:
% it is usually defined in the literature  \cite[chapter X]{zorich2004mathematical}.
\begin{mydef}{Line Integral}
\[\work{F}\gamma {B} = \int_{0}^{1} \sum_{b \in B} (F(\gamma(t)) \cdot b) (\gamma'(t) \cdot b) dt  \]
\end{mydef}
Above, $b$ is a base vector from $B$, the symbol~$\cdot$ denotes the inner product of two vectors, $\proj{F}{B}$ is the projection of $F$ on the basis $B$, and the integral sign on the right hand side is the Henstock-Kurzweil gauge integral.
A difference in our definition is that we add the argument $B$, a set of vectors, to which $F$ and $\gamma$, and accordingly the line integral are projected.
The reason for adding $B$ is that we often refer to line integrals along a subset of base vectors, e.g. the integral of the $x$-component of a field along the $x$-component of a path.
If we use the traditional formulation of line integrals (e.g. \cite[p.\ts 212]{zorich2004mathematical}), we would need to pass the projections of both the field and the path, which is more cumbersome than passing the vectors on which we project once, as $B$.
Formally, we need two definitions (one for the existence of the line integral):
\begin{isabelle}
\isacommand{definition}\isamarkupfalse%
\ line\_integral::\isanewline
\ "({\isacharprime}a::euclidean\_space\ {\isasymRightarrow}\ {\isacharprime}a)\ {\isasymRightarrow}\ {\isacharprime}a\ set\ {\isasymRightarrow}\ (real\ {\isasymRightarrow}\ {\isacharprime}a)\ {\isasymRightarrow}\ real"\isanewline
\isakeyword{where}\isanewline
\ "line\_integral\ F\ basis\ {\isasymgamma}\ {\isasymequiv}\isanewline
\ \ \ \ integral\ \{{\isadigit{0}}..{\isadigit{1}}\} ({\isasymlambda}x.\ {\isasymSum}b{\isasymin}basis.\ (F({\isasymgamma}\ (x))\ {\isasymbullet}\ b)\isanewline
\ \ \ \ \ \ \ \ \ \ \ *\ (vector\_derivative\ {\isasymgamma}\ (at\ x\ within\ \{{\isadigit{0}}..{\isadigit{1}}\})\ {\isasymbullet}\ b))"\isanewline
\end{isabelle}
\begin{isabelle}
\isacommand{definition}\isamarkupfalse%
\ line\_integral\_exists\isanewline
\isakeyword{where}\isanewline
\ "line\_integral\_exists\ F\ basis\ {\isasymgamma}\ {\isasymequiv}\isanewline
\ \ \ \ ({\isasymlambda}x.\ {\isasymSum}b{\isasymin}basis.\ F({\isasymgamma}\ x)\ {\isasymbullet}\ b\isanewline
\ \ \ \ \ \ \ \ \ \ \ *\ (vector\_derivative\ {\isasymgamma}\ (at\ x\ within\ \{{\isadigit{0}}..{\isadigit{1}}\})\ {\isasymbullet}\ b))\isanewline
\ \ \ \ integrable\_on\ \{{\isadigit{0}}..{\isadigit{1}}\}"
\end{isabelle}
\noindent In the above definition \isa{(at\ x\ within\ s)} is an Isabelle/HOL mix infix notation for a filter (for more information on filters in Isabelle/HOL please consult \cite{isabelleanalysislib}), where \isa{x} is a point and \isa{s} is a set.
We use Isabelle's syntax capabilities to have \isa{{\isasymintegral}\isactrlbsub {\isasymgamma}\isactrlesub \ F{\isasymdownharpoonright}\isactrlbsub basis\isactrlesub} as well as \isa{{\isasymointegral}\isactrlbsub {\isasymgamma}\isactrlesub \ F{\isasymdownharpoonright}\isactrlbsub basis\isactrlesub} denote \isa{line\_integral\ F\ basis\ {\isasymgamma}}.
For our definition of line integrals, the fundamental theorem of line integrals is as follows.
\begin{isabelle}
\isacommand{lemma}\isamarkupfalse%
\ fundamental\_theorem\_of\_line\_integrals\_gen:\isanewline
\ \ \isakeyword{fixes}\ f::"{\isacharprime}a::euclidean\_space\ {\isasymRightarrow}\ real"\ \isakeyword{and}\ g::"real\ {\isasymRightarrow}\ {\isacharprime}a"\isanewline
\ \ \isakeyword{assumes}\ "{\isasymforall}a\ {\isasymin}\ s.\ (has\_gradient\ f\ f{\isacharprime}\ a)"\isanewline
\ \ \ \ \isakeyword{and}\ "{\isasymforall}x\ {\isasymin}\ \{{\isadigit{0}}..{\isadigit{1}}\}.\ {\isasymgamma}\ x\ {\isasymin}\ s"\isanewline
\ \ \ \ \isakeyword{and}\ "{\isasymforall}x\ {\isasymin}\ \{{\isadigit{0}}..{\isadigit{1}}\}.\isanewline
\ \ \ \ \ \ \ \ \ \ \ ({\isasymgamma}\ has\_vector\_derivative\ ({\isasymgamma}{\isacharprime}\ x))\ (at\ x\ within\ \{{\isadigit{0}}..{\isadigit{1}}\})"\isanewline
\ \ \isakeyword{shows}\ "{\isasymintegral}\isactrlbsub {\isasymgamma}\isactrlesub \ f{\isacharprime}\ {\isasymdownharpoonright}\isactrlbsub Basis\isactrlesub \ =\ f({\isasymgamma}\ {\isadigit{1}})\ -\ f({\isasymgamma}\ {\isadigit{0}})"\isanewline
\end{isabelle}
In the statement above the constant \isa{has\_gradient} indicates that \isa{f} has the function \isa{f'} as its gradient.
Also, as one would expect, the line integral distributes over unions of disjoint sets of vectors and path joins as shown in the following statements.

\begin{isabelle}
\isacommand{lemma}\isamarkupfalse%
\ line\_integral\_sum\_gen:\isanewline
\ \ \isakeyword{assumes}\ finite\_basis:\isanewline
\ \ \ \ "finite\ basis"\ \isakeyword{and}\isanewline
\ \ \ \ line\_integral\_exists:\isanewline
\ \ \ \ "line\_integral\_exists\ F\ basis{\isadigit{1}}\ {\isasymgamma}"\isanewline
\ \ \ \ "line\_integral\_exists\ F\ basis{\isadigit{2}}\ {\isasymgamma}"\ \isakeyword{and}\isanewline
\ \ \ \ basis\_partition:\isanewline
\ \ \ \ "basis{\isadigit{1}}\ {\isasymunion}\ basis{\isadigit{2}}\ =\ basis"\ "basis{\isadigit{1}}\ {\isasyminter}\ basis{\isadigit{2}}\ =\ \{\}"\isanewline
\ \ \isakeyword{shows}\ "{\isasymintegral}\isactrlbsub {\isasymgamma}\isactrlesub \ F{\isasymdownharpoonright}\isactrlbsub basis\isactrlesub \ =\ {\isasymintegral}\isactrlbsub {\isasymgamma}\isactrlesub \ F{\isasymdownharpoonright}\isactrlbsub basis{\isadigit{1}}\isactrlesub \ +\ {\isasymintegral}\isactrlbsub {\isasymgamma}\isactrlesub \ F{\isasymdownharpoonright}\isactrlbsub basis{\isadigit{2}}\isactrlesub "\isanewline
\ \ \ \ "line\_integral\_exists\ F\ basis\ {\isasymgamma}"\isanewline

\isacommand{lemma}\isamarkupfalse%
\ line\_integral\_distrib:\isanewline
\ \ \isakeyword{assumes}\ "line\_integral\_exists\ f\ basis\ {\isasymgamma}{\isadigit{1}}"\isanewline
\ \ \ \ "line\_integral\_exists\ f\ basis\ {\isasymgamma}{\isadigit{2}}"\isanewline
\ \ \ \ "valid\_path\ {\isasymgamma}{\isadigit{1}}"\ "valid\_path\ {\isasymgamma}{\isadigit{2}}"\isanewline
\ \ \isakeyword{shows}\ "{\isasymintegral}\isactrlbsub {\isasymgamma}{\isadigit{1}}\ +++\ {\isasymgamma}{\isadigit{2}}\isactrlesub \ f{\isasymdownharpoonright}\isactrlbsub basis\isactrlesub \ =\ \ {\isasymintegral}\isactrlbsub {\isasymgamma}{\isadigit{1}}\isactrlesub \ f{\isasymdownharpoonright}\isactrlbsub basis\isactrlesub \ +\ {\isasymintegral}\isactrlbsub {\isasymgamma}{\isadigit{2}}\isactrlesub \ f{\isasymdownharpoonright}\isactrlbsub basis\isactrlesub "\isanewline
\ \ \ \ "line\_integral\_exists\ f\ basis\ ({\isasymgamma}{\isadigit{1}}\ +++\ {\isasymgamma}{\isadigit{2}})"
\end{isabelle}


\noindent Line integrals also admit a transformation analogous to integration by substitution.

\begin{isabelle}
\isacommand{lemma}\isamarkupfalse%
\ line\_integral\_on\_pair\_path:\isanewline
\ \ \isakeyword{fixes}\ F::"{\isacharprime}a::euclidean\_space\ {\isasymRightarrow}\ {\isacharprime}a"\ \isakeyword{and}\ g::"real\ {\isasymRightarrow}\ {\isacharprime}a"\ \isakeyword{and}\isanewline
\ \ \ \ \ \ {\isasymgamma}::"real\ {\isasymRightarrow}\ {\isacharprime}a"\ \isakeyword{and}\ i::{\isacharprime}a\ \isanewline
\ \ \isakeyword{assumes}\ i\_norm\_{\isadigit{1}}:\ "norm\ i\ =\ {\isadigit{1}}"\ \isakeyword{and}\isanewline
\ \ \ g\_orthogonal\_to\_i:\ "{\isasymforall}x.\ g(x)\ {\isasymbullet}\ i\ =\ {\isadigit{0}}"\ \isakeyword{and}\ \ \isanewline
\ \ \ gamma\_is\_in\_terms\_of\_i:\ "{\isasymgamma}\ =\ ({\isasymlambda}x.\ f(x)\ *\isactrlsub R\ i\ +\ g(f(x)))"\ \isakeyword{and}\isanewline
\ \ \ gamma\_smooth:\ "{\isasymgamma}\ C{\isadigit{1}}\_differentiable\_on\ \{{\isadigit{0}}..{\isadigit{1}}\}"\ \isakeyword{and}\isanewline
\ \ \ g\_continuous\_on\_f:\ "continuous\_on\ (f\ {\isacharbackquote}\ \{{\isadigit{0}}..{\isadigit{1}}\})\ g"\ \isakeyword{and}\isanewline
\ \ \ path\_start\_le\_path\_end:\ "(pathstart\ {\isasymgamma})\ {\isasymbullet}\ i\ {\isasymle}\ (pathfinish\ {\isasymgamma})\ {\isasymbullet}\ i"\ \isakeyword{and}\isanewline
\ \ \ field\_i\_comp\_cont:\ "continuous\_on\ (path\_image\ {\isasymgamma})\ ({\isasymlambda}x.\ F\ x\ {\isasymbullet}\ i)"\isanewline
\ \ \isakeyword{shows}\ "{\isasymintegral}\isactrlbsub {\isasymgamma}\isactrlesub \ F\ {\isasymdownharpoonright}\isactrlbsub \{i\}\isactrlesub\ = \isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ integral\ \{((pathstart\ {\isasymgamma})\ {\isasymbullet}\ i)..((pathfinish\ {\isasymgamma})\ {\isasymbullet}\ i)\}\isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ ({\isasymlambda}f\_var.\ (F\ (f\_var\ *\isactrlsub R\ i\ +\ g(f\_var))\ {\isasymbullet}\ i))"
\end{isabelle}
The lemma above applies to any path with all orthogonal components, but one (call it~\isa{i}), defined as a function~\isa{g} in terms of~\isa{i}.


\subsection{Partial Derivatives}

\emph{Partial derivatives} are defined on the Euclidean space type class implemented in Isabelle/HOL.
For a vector field $F$ that maps a Euclidean space to another Euclidean space, we define its partial derivative to be w.r.t. the change in the magnitude of a component vector $b$ of its input.
At a point $a$, the partial derivative is defined as%
\begin{mydef}{Partial Derivative}
\[\left. \frac{\partial F(v)}{\partial b}\right\rvert_{v = a} = \left. \frac{dF(a + (x - a \cdot b) b)}{dx}\right \rvert_{x = a \cdot b}\]
\end{mydef}
In the definition above, for a function $f$ mapping reals to vectors, $\frac{df(x)}{dx} \rvert_{x = c}$ denotes the vector derivative of $f$ at the point where $x = c$.
The Isabelle version of that definition above is as follows:
\begin{isabelle}
\isacommand{definition}\isamarkupfalse%
\ has\_partial\_vector\_derivative::\isanewline
\ "({\isacharprime}a::euclidean\_space\ {\isasymRightarrow}\ {\isacharprime}b::euclidean\_space)\ {\isasymRightarrow}\ {\isacharprime}a\ {\isasymRightarrow}\ {\isacharprime}b\ {\isasymRightarrow}\ {\isacharprime}a\isanewline
\ \  {\isasymRightarrow}\ bool"\isanewline
\isakeyword{where}\isanewline
\ "has\_partial\_vector\_derivative\ F\ b\ F{\isacharprime}\ a\isanewline
\ \ \ \ \ \ \ \ {\isasymequiv}\ (({\isasymlambda}x.\ F(a\ -\ (a\ {\isasymbullet}\ b)\ *\isactrlsub R\ b\ +\ x\ *\isactrlsub R\ b\ ))\isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ has\_vector\_derivative\ F{\isacharprime})\ (at\ (a\ {\isasymbullet}\ b))"\isanewline
\isanewline
\isacommand{definition}\isamarkupfalse%
\ partially\_vector\_differentiable\isanewline
\isakeyword{where}\isanewline
\ "partially\_vector\_differentiable\ F\ b\ p\ {\isasymequiv}\isanewline
\ \ \ \ \ \ \ \ \ \ ({\isasymexists}F{\isacharprime}.\ has\_partial\_vector\_derivative\ F\ b\ F{\isacharprime}\ p)"\isanewline
\isanewline
\isacommand{definition}\isamarkupfalse%
\ partial\_vector\_derivative::\isanewline
\ "({\isacharprime}a::euclidean\_space\ {\isasymRightarrow}\ {\isacharprime}b::euclidean\_space)\ {\isasymRightarrow}\ {\isacharprime}a\ {\isasymRightarrow}\ {\isacharprime}a\ {\isasymRightarrow}\ {\isacharprime}b"\isanewline
\isakeyword{where}\isanewline
\ "partial\_vector\_derivative\ F\ b\ a\isanewline
\ \ \ \ \ \ \ \ {\isasymequiv}\ (vector\_derivative\ ({\isasymlambda}x.\ F((a\ -\ ((a\ {\isasymbullet}\ b)\ *\isactrlsub R\ b))\ +\ x\ *\isactrlsub R\ b))\isanewline
\ \ \ \ \ \ \ \ \ \  (at\ (a\ {\isasymbullet}\ b)))"\isanewline
\end{isabelle}
We also use Isabelle's syntax capabilities to have \isa{{\isasympartial}\ F /\ {\isasympartial}i\ {\isacharbar}\isactrlbsub a\isactrlesub} denote \isa{partial\_} \isa{vector\_derivative F\ i\ a\ } and \isa{{\isasympartial}\ F /\ {\isasympartial}i\ } denote \isa{partial\_vector\_derivative F\ i}.

The definition that we use above resembles the \emph{directional derivative}, which is a generalisation of the partial derivative.
It is different from the partial derivative in that it is the change in the value of a function w.r.t. changes of its input in the direction of a given vector rather than the change in one of the variables on which $F$ is defined.
However, it is equivalent to the classical definition of a partial derivative when $b$ is a base vector.
This more general notion of derivative frequently allows us to remove the assumption that the given vector is a base vector.
We also note that the following is an equivalent characterisation of that notion of derivatives, which can simplify some proofs.

\begin{isabelle}
\isacommand{lemma}\isamarkupfalse%
\ has\_partial\_vector\_derivative\_def\_{\isadigit{2}}:\isanewline
"has\_partial\_vector\_derivative\ F\ b\ F{\isacharprime}\ a\ =\isanewline
\ \ \ \ \ \ \ (({\isasymlambda}x.\ F(a\ +\ x\ *\isactrlsub R\ b\ ))\ has\_vector\_derivative\ F{\isacharprime})\ (at\ {\isadigit{0}})"
\end{isabelle}


\noindent The following result for the partial derivative follows from the fundamental theorem of calculus (FTC) for the vector derivative,  proved in Isabelle/HOL analysis library.
\begin{isabelle}
\isacommand{lemma}\isamarkupfalse%
\ fundamental\_theorem\_of\_calculus\_partial\_vector\_gen:\isanewline
\ \ \isakeyword{fixes}\ k{\isadigit{1}}\ k{\isadigit{2}}::"real"\ \isakeyword{and}\isanewline
\ \ \ \ F::"{\isacharprime}a::euclidean\_space\ {\isasymRightarrow}\ {\isacharprime}b::euclidean\_space"\ \isakeyword{and}\isanewline
\ \ \ \ i::"{\isacharprime}a"\ \isakeyword{and}\isanewline
\ \ \ \ F\_i::"{\isacharprime}a\ {\isasymRightarrow}\ {\isacharprime}b"\isanewline
\ \ \isakeyword{assumes}\ a\_leq\_b:\ "k{\isadigit{1}}\ {\isasymle}\ k{\isadigit{2}}"\ \isakeyword{and}\isanewline
\ \ \ \ unit\_len:\ "i\ {\isasymbullet}\ i\ =\ {\isadigit{1}}"\ \isakeyword{and}\isanewline
\ \ \ \ no\_i\_component:\ "c\ {\isasymbullet}\ i\ =\ {\isadigit{0}}\ "\ \isakeyword{and}\isanewline
\ \ \ \ has\_partial\_deriv:\isanewline
\ \ \ \ \ \ "{\isasymforall}p\ {\isasymin}\ D.\ has\_partial\_vector\_derivative\ F\ i\ (F\_i\ p)\ p"\ \isakeyword{and}\isanewline
\ \ \ \ domain\_subset\_of\_D:\isanewline
\ \ \ \ \ \ "\{v.\ {\isasymexists}x.\ k{\isadigit{1}}\ {\isasymle}\ x\ {\isasymand}\ x\ {\isasymle}\ k{\isadigit{2}}\ {\isasymand}\ v\ =\ x\ *\isactrlsub R\ i\ +\ c\}\ {\isasymsubseteq}\ D"\isanewline
\ \ \isakeyword{shows}\ "(({\isasymlambda}x.\ F\_i(\ x\ *\isactrlsub R\ i\ +\ c))\ has\_integral\isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ F(k{\isadigit{2}}\ *\isactrlsub R\ i\ +\ c)\ -\ F(k{\isadigit{1}}\ *\isactrlsub R\ i\ +\ c))\ \{k{\isadigit{1}}..k{\isadigit{2}}\}"\isanewline
\end{isabelle}

\subsection{Green's Theorem for Elementary Regions}

Given these definitions and basic lemmas, we can now start elaborating on our formalisation of Green's theorem.
All proofs of Green's theorem that we encountered (e.g.\ Zorich and Cooke~\cite{zorich2004mathematical}) start by proving ``half'' of the theorem statement for every type of ``elementary region'' in $\mathbb{R}^2$.
These regions are referred to as Type~I or Type~II regions, defined below.


\begin{mydef}{Elementary Regions}

\noindent A region $D$ (modelled as a set of $\isa{real}$ pairs) is Type I iff there are piecewise-$C^1$ smooth functions $g_1$ and $g_2$ such that for two constants $a$ and $b$
\[D_x = \{(x,y) \mid a \leq x \leq b \wedge g_2(x) \leq y \leq g_1(x) \}.\]
Similarly D would be called type II iff there are $g_1$, $g_2$, $a$ and $b$
\[D_y = \{(x,y) \mid a \leq y \leq b \wedge g_2(y) \leq x \leq g_1(y) \}.\]
\end{mydef}



To prove Green's theorem, the typical approach is to prove the following two separate cases, for any regions $D_x$ and $D_y$ that are type I and type II, respectively, and their positively oriented boundaries:
\[ \underset{ D_x }{\int } - \frac{\partial(F_i)}{\partial j}\ dxdy = \work{ F}{\partial D_x}{\{i\}},\]and
\[ \underset{ D_y }{\int } \frac{\partial(F_j)}{\partial i}\ dxdy = \work{ F}{\partial D_y}{\{j\}}.\]
Here $i$ and $j$ are the base vectors while $F_i$ and $F_j$ are the $x$-axis and $y$-axis components, respectively, of $F$.
The difference in the expressions for the type I and type II regions is because of the skew symmetry of the $x$-axis and the $y$-axis w.r.t. the orientation.
We refer to the top expression as the \emph{$x$-axis} Green theorem, and the bottom one as the \textit{$y$-axis} Green theorem.

To avoid having near-duplicate proofs, one for the $x$-axis and another for the $y$-axis, we formulate a locale \isa{i\_j\_orthonorm} that treats arbitrary orthonormal unit vectors \isa{i} and \isa{j}, for which there is a Fubini like theorem (i.e. \isa{i} and \isa{j} commute under iterated integration).
A \textit{locale} is a named context: definitions and theorems proved within locale \isa{i\_j\_orthonorm} can refer to the variables and assumptions declared there.
Within that locale, we prove the following Isabelle/HOL statement, which can be seen as the $x$-axis Green theorem for type I regions (if \isa{i} is assigned to be the $x$-axis and \isa{j} is assigned to be the $y$-axis).
For the boundary, we model its paths explicitly as functions of type \isa{real \isasymRightarrow\ 'a::euclidean\_space}, where \isa{\isasymgamma 1}, \isa{\isasymgamma 2}, \isa{\isasymgamma 3} and \isa{\isasymgamma 4} are the bottom, right, top and left sides, respectively.
Also, from now on we let \isa{(F\isactrlbsub i\isactrlesub )} denote \isa{({\isasymlambda}a. F a . i)}, i.e. the \isa{i}th component of the field \isa{F}.
\begin{isabelle}
\isacommand{lemma}\isamarkupfalse%
\ GreenThm\_type\_I:\isanewline
\ \ \isakeyword{fixes}\ F\ \isakeyword{and}\isanewline
\ \ \ \ {\isasymgamma}{\isadigit{1}}\ {\isasymgamma}{\isadigit{2}}\ {\isasymgamma}{\isadigit{3}}\ {\isasymgamma}{\isadigit{4}}\ \isakeyword{and}\isanewline
\ \ \ \ a::"real"\ \isakeyword{and}\ b::"real"\ \isakeyword{and}\isanewline
\ \ \ \ g{\isadigit{1}}::"real\ {\isasymRightarrow}\ real"\ \isakeyword{and}\ g{\isadigit{2}}::"real\ {\isasymRightarrow}\ real"\isanewline
\ \ \isakeyword{assumes}\isanewline
\ \ \ \ "Dy\_pair\ =\ \{p.\ {\isasymexists}x\ y.\ p\ =\ x\ *\isactrlsub R\ i\ +\ y\ *\isactrlsub R\ j\ {\isasymand}\isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ x\ {\isasymin}\ \{a..b\}\ {\isasymunion}\ \{b..a\}\ {\isasymand}\isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ y\ {\isasymin}\ \{(g{\isadigit{2}}\ x)..(g{\isadigit{1}}\ x)\}\ {\isasymunion}\ \{(g{\isadigit{1}}\ x)..(g{\isadigit{2}}\ x)\}\}"\isanewline
\ \ \ \ "{\isasymgamma}{\isadigit{1}}\ =\ ({\isasymlambda}x.\ let\ c\ =\ (linepath\ a\ b\ x)\ in\ c\ *\isactrlsub R\ i\ +\ g{\isadigit{2}}\ c\ *\isactrlsub R\ j)"\isanewline
\ \ \ \ "{\isasymgamma}{\isadigit{1}}\ piecewise\_C{\isadigit{1}}\_differentiable\_on\ \{{\isadigit{0}}..{\isadigit{1}}\}"\ \isanewline
\ \ \ \ "{\isasymgamma}{\isadigit{2}}\ =\ ({\isasymlambda}x.\ b\ *\isactrlsub R\ i\ +\ (linepath\ (g{\isadigit{2}}\ b)\ (g{\isadigit{1}}\ b)\ x)\ *\isactrlsub R\ j)"\isanewline
\ \ \ \ "{\isasymgamma}{\isadigit{3}}\ =\ ({\isasymlambda}x.\ let\ c\ =\ (linepath\ a\ b\ x)\ in\ c\ *\isactrlsub R\ i\ +\ g{\isadigit{1}}\ c\ *\isactrlsub R\ j)"\isanewline
\ \ \ \ "{\isasymgamma}{\isadigit{3}}\ piecewise\_C{\isadigit{1}}\_differentiable\_on\ \{{\isadigit{0}}..{\isadigit{1}}\}"\isanewline
\ \ \ \ "{\isasymgamma}{\isadigit{4}}\ =\ ({\isasymlambda}x.\ a\ *\isactrlsub R\ i\ +\ (linepath\ (g{\isadigit{2}}\ a)\ (g{\isadigit{1}}\ a)\ x)\ *\isactrlsub R\ j)"\isanewline
\ \ \ \ "analytically\_valid\ Dy\_pair\ (F\isactrlbsub i\isactrlesub )\ j\ i"\isanewline
\ \ \ \ "({\isasymforall}x\ {\isasymin}\ \{a..b\}\ {\isasymunion}\ \{b..a\}.\isanewline
\ \ \ \ \ \ \ \ (g{\isadigit{2}}\ x)\ {\isasymle}\ (g{\isadigit{1}}\ x))\ {\isasymor}\ ({\isasymforall}x\ {\isasymin}\ \{a..b\}\ {\isasymunion}\ \{b..a\}.\ (g{\isadigit{1}}\ x)\ {\isasymle}\ (g{\isadigit{2}}\ x))"\ \isanewline
\ \ \ \ "a\ {\isasymnoteq}\ b"\isanewline
\ \ \isakeyword{shows}\ "{\isasymintegral}\isactrlbsub {\isasymgamma}{\isadigit{1}}\isactrlesub \ F\ {\isasymdownharpoonright}\isactrlbsub \{i\}\isactrlesub \ +\ {\isasymintegral}\isactrlbsub {\isasymgamma}{\isadigit{2}}\isactrlesub \ F\ {\isasymdownharpoonright}\isactrlbsub \{i\}\isactrlesub \ -\ {\isasymintegral}\isactrlbsub {\isasymgamma}{\isadigit{3}}\isactrlesub \ F\ {\isasymdownharpoonright}\isactrlbsub \{i\}\isactrlesub \ -\ {\isasymintegral}\isactrlbsub {\isasymgamma}{\isadigit{4}}\isactrlesub \ F\ {\isasymdownharpoonright}\isactrlbsub \{i\}\isactrlesub \isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ \ =\ (if\ a\ {\isacharless}\ b\ then\ {\isadigit{1}}::int\ else\ -{\isadigit{1}})\ *\isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ (if\ ({\isasymforall}x\ {\isasymin}\ \{a..b\}\ {\isasymunion}\ \{b..a\}.\ (g{\isadigit{2}}\ x)\ {\isasymle}\ (g{\isadigit{1}}\ x))\isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ then\ {\isadigit{1}}::int\isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ else\ -{\isadigit{1}})\ *\isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ integral\ Dy\_pair\ ({\isasymlambda}a.\ -\ ({\isasympartial}\ (F\isactrlbsub i\isactrlesub )/\ {\isasympartial}j\ {\isacharbar}\isactrlbsub a\isactrlesub ))"
\end{isabelle}
Proving the lemma above depends on the observation that for a path $\gamma$ (e.g. \isa{\isasymgamma 1} above) that is orthogonal to a vector $i$, $\work{ F}\gamma {\{i\}} = 0$, for an $F$ continuous on $\gamma$.%
\footnote{Formally, this observation follows immediately from theorem \isa{line\_integral\_on\_pair\_path}.}
%
The rest of the proof boils down to an application of Fubini's theorem (which is assumed to hold for \isa{i} and \isa{j}) and the FTC to the double integral, the integral by substitution to the line integrals and some algebraic manipulation \cite[p.\ts 238]{zorich2004mathematical}.
Nonetheless, this algebraic manipulation proved to be quite tedious when done formally in Isabelle/HOL\@.

\subsection{Our Analytic Assumptions}

The predicate \isa{analytically\_valid} in the last lemma represents the analytic assumptions of our statement of Green's theorem, to which an ``appropriate'' field has to conform.
Firstly let $1_s$ be the indicator function for a set $s$.
Then, for a type I region $D_x$ our analytic assumptions for the $x$-axis Green theorem are that
\begin{enumerate}
  \item $F_i$ is continuous on $D_x$
  \item $\frac{\partial(F_i)}{\partial j}$ exists everywhere in $D_x$
  \item the product $1_{D_x}(x,y) \frac{\partial(F_i)}{\partial j}(x,y)$ is Lebesgue integrable
  \item the product $1_{[a,b]}(x) \int_{g_1(x)}^{g_2(x)} F(x,y) dy$ is a Borel measurable function, where the integral in that function is a Henstock-Kurzweil gauge integral.
\end{enumerate}
These assumptions vary symmetrically for the $y$-axis Green theorem, so to avoid having two symmetrical definitions, we define the predicate \isa{analytically\_valid} to take the axes as arguments.
\begin{isabelle}
\isacommand{definition}\isamarkupfalse%
\ analytically\_valid\isanewline
\isakeyword{where}\isanewline
\ "analytically\_valid\ s\ F\ i\ j\ {\isasymequiv}\isanewline
\ \ \ \ \ \ \ continuous\_on\ s\ F\ {\isasymand}\ \isanewline
\ \ \ \ \ \ \ ({\isasymforall}a\ {\isasymin}\ s.\ partially\_vector\_differentiable\ F\ i\ a)\ {\isasymand}\isanewline
\ \ \ \ \ \ \ set\_integrable\ lborel\ s\ ({\isasympartial}\ F\ /\ {\isasympartial}\ i)\ {\isasymand}\isanewline
\ \ \ \ \ \ \ (let\ p\ =\ ({\isasymlambda}x\ y.\ (y\ *\isactrlsub R\ i\ +\ x\ *\isactrlsub R\ j))\ in\isanewline
\ \ \ \ \ \ \ \ \ \ \ \ (({\isasymlambda}x.\ integral\ UNIV\isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ ({\isasymlambda}y.\ (indicator\ s\ (p\ x\ y))\ *\isactrlsub R\ ({\isasympartial}\ F\ /\ {\isasympartial}\ i\ {\isacharbar}\isactrlbsub p\ x\ y\isactrlesub )))\isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ {\isasymin}\ borel\_measurable\ lborel))"\isanewline
\end{isabelle}

%Although these assumptions are not the most general possible for Green's theorem, they are still more general than the assumptions of the most basic theorem statement, which requires the field and all of its partial derivatives to be continuous in the region.
These conditions refer to Lebesgue integrability and to measurability because we use Fubini's theorem for the Lebesgue integral in Isabelle/HOL's Analysis library to derive a Fubini like result for the Henstock-Kurzweil integral.
Note that proving Fubini's theorem for the gauge integral would allow for more general analytic assumptions, and we hope to do this eventually.

