Reviewer #3: The authors have substantially revised their text. In particular, they
have taken into account many of the comments/questions of the
reviewers, although not all of them. They describe one more instance
of shape of region (a disk) the theorem can apply to, beside the case
of a diamond shape, already present in the previous version.

This version of the paper should be accepted, if only
because it improves on the conference version by clarifying some
assumptions, like the generalized equivalence relation on paths, that
make the statement of the theorem usable for real.

However, in my opinion, there is still room for significant
improvements (see my comments below), both in the formalization and in
its presentation.

If possible for the editors'agenda, I would recommend another round of
revisions/review, hence my assessment that a "major revision is
needed".

Remarks:

>> + It is nice to the reviewers to proof-read your text before
>> submitting it, in particular at this stage of the reviewing
>> process. E.g. (I will not list them all):
>> - p1: missing dot at the end of the 1st paragraph
>> - p2: in Isabelle'a analysis -> Isabelle's
>> - p4: a set of basis B -> ?
>> - p7: a the predicate
>> - p16: to substantially -> to be substantially
>> - misplaced footnote 5 p20
>> -...

Having re-read the entire manuscript carefully, we can't regard this comment as fair. At any stage in the reviewing process one may have to beat a deadline, but one error per several pages is hardly unusual.

>> I understand that ultimately the layout of the camera ready version
>> will be different, but please pay attention to the inlined code:
>> several lines are too long (p9, p16).

>> - abstract: "The theorem statement that we formalise is enough
>> for most applications, especially in physics and engineering." : the
>> is slightly misleading, as you did not formalize any such
>> application. For instance, the previous version of your text featured
>> the same sentence, but in order to deal with one more application, you
>> had to modify your definition of path-equivalence.

What we mean is that since the statement that we formalised is supposedly applicable to regions that can be divided in both, type I and type II regions, then it can be applied to many practical examples. On page 10, we cite the book by Spivak to justify the claim that such regions are enough for applications, when we say "Practically, those regions are enough for most applications, especially in physics and engineering [17, p. 240]." We removed the claim that you pointed out from the abstract, nonetheless.

>> - p2 "In mathematics, Green's theorem is a fundamental result. (period) ": I do
>>   agree that it is an important result, which makes possible to derive
>>   very useful formulas, etc. But this sentence, as such is
>>   really not informative. I think that many researchers in mathematics
>>   never make use of this equation for their own work.

Surely this could be said of almost any deep mathematical result.
However, we changed this to say that "in many areas of mathematics, Green's theorem is a fundamental result" and gave examples.

>> + A few sentences in the paper are claims without a real
>> justification. E.g.:

>> + Some other wordings are too vague/clumsy. E.g.:
>>
>> - p1: I still do not like the use of a "theorem" LaTeX environment for
>>   a vague statement, although I understand that the purpose of this is
>>   to represent a class of statements.
>>
>> -p2: "Many statements", "geometrically sophisticated", "prevalent textbook form"
>> -p3: "ubiquitous in the literature"
>> -p4: :"traditional formulation of line integrals"
>> -p8: "more general structures": what are the structures here?
>> -p8: "a lot of the textbook proofs"

We wonder how else to remark that our formulation of the theorem statement differs from usual lines.
We removed or changed those sentences in the current draft, and added citations in some cases.

>> -p8: "more general structures": what are the structures here?

We replaced that with "more general regions".

>> -p3 and later: I would use 'circle' for a boundary and 'disk' for
>> a region.

We changed circle to disk.

>> -p8 I do not understand why this 'conjecture' should be stated
>>   here. Nothing in the development depend on the provability of this
>>   claim. I think it would be more appropriate to state and discuss it
>>   in conclusion.

We moved it to after the last statement of Green's theorem was obtained.

>> - p20 conclusion: "We formalised a statement of Green's theorem that
>>   is general enough for a lot of practical applications." Same comment
>>   as for the abstract. What is 'a lot' ?

Reworded slightly. But we would be surprised to see a real world example that fell outside our current formulation.

  >> Again in the conjecture statement: what does
  >> "divided" mean? Is it the same as "partitioning"? 

Yes it means divisions. We have been using the terms division and partition interchangeably in our paper. Now we only use division.

  >> Also is it about
  >> finite partitions only or do you want to address more general ones
  >> in this conjecture?

It is only about finite divisions. We now state that assumption explicitly.

  >> And finally, what is the property of the region
  >> you start with? Connected? piece-wise-smooth boundary? piece-wise C1?
  >> In its current shape, I am not sure I understand the statement of
  >> this conjecture, so as such I cannot say if it is trivial or
  >> difficult or false.

The only property of the region is that it can be divided into finitely many type I (resp. type II) regions.
This implies, however, that its boundaries are piece-wise C1.

>> -p17: "Practical Examples"? Practical really?

We rather mean practically relevant, since there are some applications that need circles, for instance. Additionally, the way we prove the theorem for the examples can be used a guide in other more practical applications. However, we now changed the text to "small examples" to reflect your feedback.


>> + Some questions by me but also by other reviewers, were answered in
>> the response, but not in the article itself. For instance, asking
>> "what is a rectifiable cycle?" means that the reader needs an
>> indication in the paper, not that we expect to learn the definition in
>> the response.

We clarified those questions in the paper.

>> + Section 2.1 has been added to provide the Isabelle/HOL
>> prerequisites, but it is a bit short. For instance, you explain what a
>> locale is, but not what a type class is. You do not explain the
>> different definitions of integrals, the related predicates and
>> notations, and the relations between them, although the latter plays
>> an important role, e.g. p8.

We added to section 2 a description of the different constants and predicates that we use that are related to integration in the Isabelle's analysis library.

>> Another example: it was not obvious to me
>> that definitions like the ones p4 mix infix (at ... within...) and
>> applicative notations. 

We added a note to indicate that after the first occurrence of (at .. within ..)

>> Finally, one thing still not clear to me: is
>> the definition of line integrals a contribution of the paper?

Yes and we finally say so, thanks!

>> +p6: I am still no convinced by the locale R2 and by the treatment of
>> the i, j vectors, hardcoded to be (1,0) and (0,1)

We now fixed that and have a locale that assumes that i and j are orthonormal unit vectors, thus removing all symmetries from our proofs.

>> and Definition 3 still
>> looks half-baked.

Definition 3 is a standard definition, see page 246 in Protter's book or page 237 in Zorich's book, for instance.

>> + Section 2.1 has been added to provide the Isabelle/HOL
>> prerequisites, but it is a bit short. For instance, you explain what a
>> locale is, but not what a type class is.

DONE

Other remarks:

>>-p3: what is a primitive?

Changed to "mathematical concepts"

>> -p5: In the formal statement of the lemma line_integral_on_pair_path_strong,
>>   why keeping g in the right hand-side? Hypothesis 2 says that it is
>>  useless since g(f_var).i = 0. I must say that this was already
>>  present in the previous version of the paper, but I did not notice it.

The term on this line should be read as "(F (f_var * R i + g(f_var))) · i", i.e. the inner product is with the value of F, not its argument "(f_var * R i + g(f_var))". Thus we cannot remove "g(f_var)".

>> -p7-8, Lebesgue vs Gauge integral: I am not entirely convinced about
>>   the answer provided in this case. I could have asked instead "why
>>  don't you use Lebesgue only from the beginning?", but the real
>>  question is about the usability and intelligibility of the statement.

For usability, we believe that showing that the field is continuously partially differentiable is the easiest way to use the theorem, and from that all the analytic assumptions would follow.
We added a note describing that, as well as a theorem showing that continuous partial differentiability implies analytic validity after the final statement of the formalised Green's theorem.

>> -conclusion: any suggestion to ease the definition of suitable
>>  partitions, and/or automate the proofs of the corresponding conditions?

We suspect that choosing divisions manually is a skill that would be gained by experience. This is similar to applying Green's theorem in pen-and-paper examples, where for instance choosing the right parameterisation needs some experience. We believe that if there are sufficiently many applications one can deduce the common features between parameterisations that occur in practice and accordingly devise infrastructure for automation, especially for finding subdivisions and/or reparameterisations. We added a note to the conclusion summarising our view about that.
