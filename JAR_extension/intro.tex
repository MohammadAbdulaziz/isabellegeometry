\section{Introduction}
\label{sec:introduction}
The \emph{Fundamental Theorem of Calculus} (FTC) is a theorem of immense importance in differential calculus and its applications, relating a function's derivative to its integral.
Having been conceived in the seventeenth century in parallel with the development of the infinitesimal calculus, more general forms of the FTC have been developed, the most general of which is the \emph{General Stokes Theorem}.

A generalisation of the FTC (and a special case of the General Stokes Theorem in $\mathbb{R}^2$) was published in 1828 by George Green \cite{green1828essay}, with applications to electromagnetism in mind.
Green's Theorem is the main topic of this work.
In modern terms, it can be stated as follows:
\begin{mythm}
\label{thm:grexen}
Given a region $D\subseteq \mathbb{R}^2$ with an ``appropriate'' positively oriented boundary $\partial D$, and a field ``appropriately'' defined on $D$ as $F (a) = (F_x (a), F_y (a))$, for every $a\in D$, the following identity holds:
\[ \underset{ D }{\int } \frac{\partial F_y}{\partial x} - \frac{\partial F_x}{\partial y}\ dxdy = \underset{ \partial D }{\oint } F_x dx + F_y dy,\]

\noindent where the left hand side is a double integral and the right hand side is a line integral
in $\mathbb{R}^2$.
\end{mythm}

The term under double integral can be interpreted as the curl of the field $F$ in the plane which, for instance, physically represents the vorticity of a physical field.
The line integral of the field is on the region's boundary, and it can be physically interpreted as the work done by the field $F$ on a particle moving along the path $\partial D$ in the plane, for instance.
Thus, this statement is a special case of the 3-dimensional Kelvin-Stokes theorem \cite[p.\ts438]{apostol-calculus}.
Also note that one can obtain the 2-dimensional divergence theorem for the field $F$ and a region $D$ if the statement above is applied to $(-F_x(a), F_y(a))$ instead of $(F_x(a), F_y(a))$.
This is because in that case the line integral will be $\underset{ \partial D }{\oint } F_y dy - F_x dx$, which is the flux of $F$ through $\partial D$, and the double integral will be $\underset{ D }{\int } \frac{\partial F_x}{\partial y} + \frac{\partial F_y}{\partial x}\ dxdy$, which is the divergence of $F$ through $D$.

Many statements of Green's theorem define, with varying degrees of generality,
\begin{itemize}
  \item \textit{the geometrical assumptions}: what is an appropriate boundary
  \item \textit{the analytic assumptions}: what is an appropriate field.
\end{itemize}
%This mainly depends on how geometrically sophisticated the proof is, and the type of integral used (Riemann, Lebesgue, etc).
The prevalent textbook form of Green's theorem asserts that, geometrically, the region can be divided into \emph{elementary regions} and that, analytically, the field has smooth partial derivatives throughout the region.
Also, the underlying integral is a Riemann integral in most textbooks.

Despite this being enough for most applications, more general forms of the theorem have appeared in the analysis literature. Michael~\cite{michael1955approximation} proves a statement of the theorem that generalises the geometrical assumptions, only assuming that the region has a rectifiable boundary (i.e. a boundary with finite length).
Jurkat et al.~\cite{jurkat1990general} prove a statement of the theorem with exceptionally general analytic assumptions: 
they only assume that the field is continuous in the region, and that the \emph{total} derivative of the field exists in the region except for a $\sigma_{1}$-finite set of points in the region.
Then, they use that statement of Green's theorem to derive a general form of Cauchy's integral theorem.

Green's theorem has innumerable applications.
In physics, these include electrodynamics and mechanics; in engineering these include deriving moments of inertia, hydrodynamics and the basis of the planimeter.
Furthermore, Green's theorem is a fundamental result for a multitude of branches in mathematics, e.g. it can be used to derive Cauchy's integral theorem, and to justify efficient numerical solution methods for partial differential equations describing dynamical systems.

We formalise a statement of Green's theorem for Henstock-Kurzweil gauge integrals in the interactive theorem prover Isabelle/HOL \cite{nipkow2002isabelle}.
Our work builds on the work of H{\"o}lzl et~al.~\cite{holzl2011three} and the Isabelle/HOL analysis library \cite{isabelleanalysislib}.

Existing proofs of Green's theorem all have one fundamental argument in common: showing that if a region is divided into subregions, the line integral of a field on the region's boundary is equal to the sum of the line integrals of the field around the subregions' boundaries.
Crucially, line integrals on boundaries of neighbouring subregions cancel each other out.
Formalising this argument depends on formalising orientations and region boundaries explicitly using for instance, an outward-pointing vector as used in Federer~\cite{federer2014geometric}.
This can be hard, especially for regions with holes.

We have developed a novel proof.
Avoiding the usual line cancellation argument, we use a homological argument that characterises equivalences between paths.
Accordingly our formalisation does not strictly follow any published proof, but for reference we used the proofs by Zorich and Cooke \cite{zorich2004mathematical}, Spivak~\cite{spivak1981comprehensive}, and Protter~\cite{protter2006basic}.

\subsection*{Contributions}
This paper extends our prior work~\cite{abdulaziz2016isabelle}, which presented a mechanisation of  line integrals, partial derivatives and Green's theorem.
One major difference between this paper and the conference version is that we provide a more general formalisation that removes all symmetric definitions and proofs.
In particular, in our original formalisation we had separate definitions, theorems and proofs that were (almost) symmetric: they were once stated for the $x$-axis, and a second time for the $y$-axis with some modifications to accommodate the skew symmetry between the two axes.
In the current formalisation, symmetric objects are replaced by objects parameterised by arbitrary orthonormal bases, thus eliminating most symmetries.
A key component which made this generalisation possible was porting the multivariate change of variables theorem from HOL Light to Isabelle, which is a substantial formalisation in its own right (around 12K lines of proof script).


Another major difference is that we apply our statement of Green's theorem to simple regions (a diamond and a disk) as a tutorial showing how to use the theorem that we formalised.
Moreover, we have substantially generalised the theorem statement as follows to simplify the treatment of applications.
\begin{itemize}
  \item We have weakened the geometric assumptions of our theorem in two ways:
  \begin{enumerate}
    \item generalising our definition of the equivalence of two paths from requiring that they have equal parameterisations to requiring that one of them is the reparameterisation of the other through a piecewise-smooth map~$\phi$
    \item generalising our definition of 1-chains having a common sub-division to exclude a finite number of points from each 1-chain
  \end{enumerate}
  \item We have generalised our definition of elementary regions to have piecewise-smooth edges instead of smooth edges.
  \item And we have generalised our formalisation to accept parameterisations of regions that are either clockwise or anticlockwise orientations, versus our original restriction to only anticlockwise parameterisations.
\end{itemize}

We also elaborate more on the intuitions behind the concepts related to Green's theorem and our proof.
