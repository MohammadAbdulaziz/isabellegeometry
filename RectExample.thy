section \<open>The Rectangle Example\<close>

theory RectExample
  imports Green SymmetricR2Shapes
begin

locale rect = R2 +
  fixes a b::real
  assumes a_b_gt_0: "0 < a" "0 < b"
begin

definition "rect_x_coord t = a * t"

definition "rect_y_coord x = b"

definition rect_cube:: "((real * real) \<Rightarrow> (real * real))" where
  "rect_cube = (\<lambda>(t1::real,t2::real). ( (a * t1) *\<^sub>R i +  (b * t2) *\<^sub>R j))"

lemma rect_y_valid:
  shows "rect_y_coord piecewise_C1_differentiable_on {0..a}"         (is ?P)
        "(\<lambda>x. rect_y_coord x) piecewise_C1_differentiable_on {0..a}" (is ?Q)
proof -
  have f0: "finite {0}"
    by simp
  show ?P ?Q
    unfolding piecewise_C1_differentiable_on_def rect_y_coord_def
    by (fastforce intro!: f0 continuous_intros derivative_intros)+
qed

lemma rect_cube_is_type_I:
  shows "typeI_twoCube (1,rect_cube) i j"
proof (simp only: typeI_twoCube.simps, intro exI conjI ballI impI)
  show "0 \<noteq> a"
    using a_b_gt_0 by auto
  show "(\<forall>x\<in>{0..a} \<union> {a..0}. (%x. 0) x \<le> (%x. b) x) \<or> (\<forall>x\<in>{0..a} \<union> {a..0}. (%x. b) x \<le> (%x. 0) x)"
    using a_b_gt_0
    by (auto simp add: rect_y_coord_def)
  show "rect_cube = (\<lambda>(t1, t2). let c = linepath 0 a t1 in c *\<^sub>R i + linepath 0 b t2 *\<^sub>R j)"
    by (auto simp: rect_cube_def rect_x_coord_def rect_y_coord_def divide_simps algebra_simps)
    
  show "(%x. b) piecewise_C1_differentiable_on  {0..a} \<union> {a..0}"
       "(\<lambda>x. 0) piecewise_C1_differentiable_on  {0..a} \<union> {a..0}"
    unfolding rect_y_coord_def piecewise_C1_differentiable_on_def
    by (rule conjI exI continuous_intros derivative_intros | force)+
  show "(1::int) = (if 0 < a then 1 else - 1) * (if \<forall>x\<in>{0..a} \<union> {a..0}. 0 \<le> b then 1 else - 1)"
    using a_b_gt_0 by auto
qed

lemma rect_cube_boundary_valid:
  assumes "(k,\<gamma>)\<in>\<partial>rect_cube"
  shows "valid_path \<gamma>"
  using assms typeI_edges_are_valid_paths_prod rect_cube_is_type_I by blast

lemma rect_cube_is_type_II:
  shows "typeI_twoCube (1, prod.swap o rect_cube) (j) i"
  using swap_typeI_is_typeII rect_cube_is_type_I by auto

lemma rect_cube_valid_two_cube:
  shows "valid_two_cube rect_cube"
  unfolding rect_cube_def
  apply (auto simp add: valid_two_cube_def boundary_def rect_x_coord_def card_insert_if i_is_x_axis j_is_y_axis)   
   apply (metis a_b_gt_0(1) mult_zero_right not_square_less_zero old.prod.inject)
  apply (metis (no_types, hide_lams) add_diff_cancel_right' a_b_gt_0 mult_cancel_left mult_zero_right order_less_irrefl prod.inject)
  done

definition rect_cube_boundary_to_subdiv where
  "rect_cube_boundary_to_subdiv (gamma::(int \<times> (real \<Rightarrow> real \<times> real))) \<equiv> id"

definition rot_rect_cube_boundary_to_subdiv where
  "rot_rect_cube_boundary_to_subdiv (gamma::(int \<times> (real \<Rightarrow> real \<times> real))) \<equiv> id"

definition rect_boundaries_reparam_map where
  "rect_boundaries_reparam_map \<equiv> id"

lemma rect_boundaries_reparam_map_bij:
     "bij (rect_boundaries_reparam_map)"
  by(auto simp add: bij_def full_SetCompr_eq[symmetric] rect_boundaries_reparam_map_def)

lemma rect_bot_edges_neq_rect_top_edges:
     "bot_edge rect_cube \<noteq>  top_edge rect_cube"
     "right_edge rect_cube \<noteq>  top_edge rect_cube"
     "left_edge rect_cube \<noteq>  top_edge rect_cube"
     "bot_edge rect_cube \<noteq>  left_edge rect_cube"
     "right_edge rect_cube \<noteq>  left_edge rect_cube"
     "bot_edge rect_cube \<noteq>  right_edge rect_cube"
  unfolding rect_cube_def
  using a_b_gt_0
  by(auto simp add: fun_eq_iff bot_edge_def right_edge_def left_edge_def rect_cube_def top_edge_def i_is_x_axis j_is_y_axis)

lemma rect_bot_edges_neq_rect_top_edges_typeII'':
     "bot_edge (R2.typeI_to_typeII rect_cube) \<noteq>  top_edge (R2.typeI_to_typeII rect_cube)"
     "right_edge (R2.typeI_to_typeII rect_cube) \<noteq>  top_edge (R2.typeI_to_typeII rect_cube)"
     "left_edge (R2.typeI_to_typeII rect_cube) \<noteq>  top_edge (R2.typeI_to_typeII rect_cube)"
     "bot_edge (R2.typeI_to_typeII rect_cube) \<noteq>  left_edge (R2.typeI_to_typeII rect_cube)"
     "right_edge (R2.typeI_to_typeII rect_cube) \<noteq>  left_edge (R2.typeI_to_typeII rect_cube)"
     "bot_edge (R2.typeI_to_typeII rect_cube) \<noteq>  right_edge (R2.typeI_to_typeII rect_cube)"
  unfolding rect_cube_def
  using a_b_gt_0
  by(auto simp add: fun_eq_iff bot_edge_def right_edge_def left_edge_def rect_cube_def top_edge_def i_is_x_axis j_is_y_axis typeI_to_typeII_def)

lemma rect_cube_to_cbox: "(cubeImage (rect_cube)) = cbox 0 (a,b)"
  unfolding cbox_def rect_cube_def apply (auto simp add: real_pair_basis)
  using a_b_gt_0
  apply(auto simp add: cubeImage_def image_def i_is_x_axis j_is_y_axis)
  subgoal for x y by (rule bexI [of _ "(x/a, y/b)"], cases "(x/a, y/b)", auto)
  done

lemma valid_cube_inv_orient:
  assumes "valid_two_cube (rect_cube)"
  shows "valid_two_cube (rect_cube o  (%(x,y). (1 - x, y)))"
  using assms unfolding valid_two_cube_def boundary_def rect_cube_def i_is_x_axis j_is_y_axis
  apply (auto simp: card_insert_if split: if_split_asm)
  by (smt a_b_gt_0(1) add_diff_cancel_right' case_prod_conv i_is_x_axis inner_real_def inner_zero_right j_is_y_axis prod.sel(1) real_inner_1_right real_scaleR_def rect.rect_cube_def rect_axioms scaleR_Pair)+

lemma rect_cube_inj: "inj_on rect_cube s"
  apply(subst rect_cube_def)
  unfolding inj_on_def  i_is_x_axis j_is_y_axis
  apply auto
  using a_b_gt_0 by blast+

end

locale green_for_rect = r1: rect i j a b + r2: rect i j b a for i j a b +
  assumes i_is_x_axis: "i = (1::real,0::real)" and
    j_is_y_axis: "j = (0::real, 1::real)"
begin

lemma r1_img_eq_r2_img: "cubeImage (prod.swap \<circ> r2.rect_cube) = cubeImage r1.rect_cube"
  unfolding cubeImage_def prod.swap_def r1.rect_cube_def r2.rect_cube_def image_def
  apply (auto simp add: i_is_x_axis j_is_y_axis)
  by force+

definition r1_boundary_to_r2_boundary where
"r1_boundary_to_r2_boundary one_cube =
                                (if one_cube = top_edge (R2.typeI_to_typeII r2.rect_cube)
                                   then {right_edge r1.rect_cube}
                                 else if one_cube = bot_edge (R2.typeI_to_typeII r2.rect_cube)
                                   then {left_edge r1.rect_cube}
                                 else if one_cube = right_edge (R2.typeI_to_typeII r2.rect_cube)
                                   then {bot_edge r1.rect_cube}
                                 else if one_cube = left_edge (R2.typeI_to_typeII r2.rect_cube)
                                   then {top_edge r1.rect_cube}
                                 else {})"

lemma result_mapping_is_boundary:
  "boundary_chain (r1_boundary_to_r2_boundary C)"
  unfolding boundary_chain_def r1_boundary_to_r2_boundary_def
  by(auto simp add:  bot_edge_def top_edge_def right_edge_def left_edge_def)

lemma GreenThm_rect:
  assumes "analytically_valid (cbox 0 (a,b)) (F\<^bsub>i\<^esub>) j i"
    "analytically_valid (cbox 0 (a,b)) (F\<^bsub>j\<^esub>) i j"
  shows "integral (cbox 0 (a,b)) (\<lambda>x. \<partial> F\<^bsub>j\<^esub> / \<partial> i |\<^bsub>x\<^esub> - \<partial> F\<^bsub>i\<^esub> / \<partial> j |\<^bsub>x\<^esub> )
                    =  \<^sup>H\<integral>\<^bsub>(\<partial> r1.rect_cube)\<^esub> F \<downharpoonright>\<^bsub>{i, j}\<^esub>"
proof-
  have "integral (cbox 0 (a,b)) (\<lambda>x. real_of_int(chain_orientation {(1, R2.typeI_to_typeII r2.rect_cube)}) * \<partial> F\<^bsub>j\<^esub> / \<partial> i |\<^bsub>x\<^esub> - real_of_int(chain_orientation {(1, r1.rect_cube)}) * \<partial> F\<^bsub>i\<^esub> / \<partial> j |\<^bsub>x\<^esub> )
                    =  \<^sup>H\<integral>\<^bsub>(\<partial>r1.rect_cube)\<^esub> F \<downharpoonright>\<^bsub>{i, j}\<^esub>"
    unfolding r1.rect_cube_to_cbox[symmetric]
  proof(intro green_typeI_typeII_chain.GreenThm_typeI_typeII_divisible_region_2'(1))
    show "green_typeI_typeII_chain (cubeImage r1.rect_cube) i j F ({(1, r1.rect_cube)}) ({(1, R2.typeI_to_typeII r2.rect_cube)})"
      unfolding green_typeI_typeII_chain_def
    proof(intro conjI)
      show *: " i_j_orthonorm i j" using R2.i_j_orthonorm[OF r1.R2_axioms] by auto
      show "green_typeI_chain i j F {(1, r1.rect_cube)} (cubeImage r1.rect_cube)"
        unfolding green_typeI_chain_def
      proof(intro conjI * )
        show "green_typeI_chain_axioms i j F {(1, r1.rect_cube)} (cubeImage r1.rect_cube)"
          unfolding green_typeI_chain_axioms_def gen_division_def valid_two_chain_def
          by(auto intro: conjI assms(1)[unfolded r1.rect_cube_to_cbox[symmetric]] r1.rect_cube_is_type_I r1.rect_cube_valid_two_cube)
      qed
      show "green_typeI_chain (- j) i F {(1, R2.typeI_to_typeII r2.rect_cube)} (cubeImage r1.rect_cube)"
        unfolding green_typeI_chain_def
      proof(intro conjI R2.neg_j_i_orthonorm'[OF r1.R2_axioms])
        show "green_typeI_chain_axioms (- j) i F {(1, R2.typeI_to_typeII r2.rect_cube)} (cubeImage r1.rect_cube)"
          unfolding green_typeI_chain_axioms_def gen_division_def valid_two_chain_def
        proof(intro conjI)
          show "\<forall>twoCube\<in>{(1, R2.typeI_to_typeII r2.rect_cube)}. typeI_twoCube twoCube (- j) i"
            using r1.typeI_to_typeII_works[OF r2.rect_cube_is_type_I] by auto
          show "\<Union> ((cubeImage \<circ> snd) ` {(1, R2.typeI_to_typeII r2.rect_cube)}) = cubeImage r1.rect_cube"
            using r1_img_eq_r2_img rev_orient_same_img[OF r2.rect_cube_is_type_II] unfolding r1.typeI_to_typeII_def by auto
          show "\<forall>(orient, C)\<in>{(1, R2.typeI_to_typeII r2.rect_cube)}. valid_two_cube C"
            using valid_cube_valid_swap'[OF rev_orient_valid[OF r2.rect_cube_valid_two_cube r2.rect_cube_inj]] unfolding r1.typeI_to_typeII_def by (auto simp add: o_def)
          show "\<forall>(orient, C)\<in>{(1, R2.typeI_to_typeII r2.rect_cube)}. analytically_valid (cubeImage C) ((F\<^bsub>- j\<^esub>)) i (- j)"
            using R2.anal_valid_j_neg_j[OF r1.R2_axioms assms(2)] r2.rect_cube_valid_two_cube
            using r1.rect_cube_to_cbox r1_img_eq_r2_img r2.rect_cube_is_type_II rev_orient_same_img unfolding r1.typeI_to_typeII_def by fastforce
        qed auto
      qed
    qed
    show "only_vertical_division' (\<partial> r1.rect_cube) {(1, r1.rect_cube)}"
      apply(rule twoChainVertDiv_of_itself'[where C = "{(1, r1.rect_cube)}", unfolded two_chain_boundary_def , simplified, OF _ r1.rect_cube_is_type_I])
      using typeI_edges_are_valid_paths[OF r1.rect_cube_is_type_I]
      by auto
    show "only_vertical_division' (\<partial> (R2.typeI_to_typeII r2.rect_cube)) {(1, r1.typeI_to_typeII r2.rect_cube)}"
      using twoChainVertDiv_of_itself'[where C = "{(1, R2.typeI_to_typeII r2.rect_cube)}", unfolded two_chain_boundary_def , simplified, OF _ r1.typeI_to_typeII_works[OF r2.rect_cube_is_type_I]]
      using typeI_edges_are_valid_paths[OF r1.typeI_to_typeII_works[OF r2.rect_cube_is_type_I]]
      by auto
    show "common_subdiv_exists (\<partial> r1.rect_cube) (\<partial> (R2.typeI_to_typeII r2.rect_cube))"
      unfolding common_subdiv_exists_def
    proof(intro exI conjI)
      show "chain_subdiv_chain (\<partial>(R2.typeI_to_typeII r2.rect_cube) - {}) (\<partial>r1.rect_cube)"
        unfolding chain_subdiv_chain_def two_chain_boundary_def
      proof(intro exI conjI; subst Diff_empty)
        show "\<Union> (r1_boundary_to_r2_boundary ` (\<partial>(R2.typeI_to_typeII r2.rect_cube))) = \<partial>r1.rect_cube"
          unfolding boundary_edges r1_boundary_to_r2_boundary_def
          using r2.rect_bot_edges_neq_rect_top_edges_typeII'' by auto
        show "\<forall>c\<in>\<partial>(R2.typeI_to_typeII r2.rect_cube). chain_subdiv_path (coeff_cube_to_path c) (r1_boundary_to_r2_boundary c)"
          unfolding boundary_edges
          using r1.rect_bot_edges_neq_rect_top_edges r2.rect_bot_edges_neq_rect_top_edges_typeII''
        proof(auto simp add: r1_boundary_to_r2_boundary_def)
          have "(coeff_cube_to_path (bot_edge (R2.typeI_to_typeII r2.rect_cube))) = coeff_cube_to_path (left_edge r1.rect_cube)"
              unfolding coeff_cube_to_path.simps reversepath_def r1.typeI_to_typeII_def prod.swap_def r1.rect_cube_def r2.rect_cube_def left_edge_def bot_edge_def 
              by (auto simp add: i_is_x_axis j_is_y_axis)
          then show "chain_subdiv_path (coeff_cube_to_path (bot_edge (R2.typeI_to_typeII r2.rect_cube))) {left_edge r1.rect_cube}"
            using chain_subdiv_path.intros by auto
          have "(coeff_cube_to_path (top_edge (R2.typeI_to_typeII r2.rect_cube))) = coeff_cube_to_path (right_edge r1.rect_cube)"
              unfolding coeff_cube_to_path.simps reversepath_def r1.typeI_to_typeII_def prod.swap_def r1.rect_cube_def r2.rect_cube_def right_edge_def top_edge_def 
              by (auto simp add: i_is_x_axis j_is_y_axis)
          then show "chain_subdiv_path (coeff_cube_to_path (top_edge (R2.typeI_to_typeII r2.rect_cube))) {right_edge r1.rect_cube}"
            using chain_subdiv_path.intros by auto
          have "(coeff_cube_to_path (right_edge (R2.typeI_to_typeII r2.rect_cube))) = coeff_cube_to_path (bot_edge r1.rect_cube)"
              unfolding coeff_cube_to_path.simps reversepath_def r1.typeI_to_typeII_def prod.swap_def r1.rect_cube_def r2.rect_cube_def right_edge_def bot_edge_def 
              by (auto simp add: i_is_x_axis j_is_y_axis)
          then show "chain_subdiv_path (coeff_cube_to_path (right_edge (R2.typeI_to_typeII r2.rect_cube))) {bot_edge r1.rect_cube}"  
            using chain_subdiv_path.intros by auto
          have "(coeff_cube_to_path (left_edge (R2.typeI_to_typeII r2.rect_cube))) = coeff_cube_to_path (top_edge r1.rect_cube)"
              unfolding coeff_cube_to_path.simps reversepath_def r1.typeI_to_typeII_def prod.swap_def r1.rect_cube_def r2.rect_cube_def left_edge_def top_edge_def 
              by (auto simp add: i_is_x_axis j_is_y_axis)
          then show "chain_subdiv_path (coeff_cube_to_path (left_edge (R2.typeI_to_typeII r2.rect_cube))) {top_edge r1.rect_cube}"  
            using chain_subdiv_path.intros by auto
        qed
        show "pairwise (\<lambda>p p'. r1_boundary_to_r2_boundary p \<inter> r1_boundary_to_r2_boundary p' = {}) (\<partial>(R2.typeI_to_typeII r2.rect_cube))"
          unfolding pairwise_def boundary_edges r1_boundary_to_r2_boundary_def
          using r1.rect_bot_edges_neq_rect_top_edges by auto
      qed
      show "chain_subdiv_chain (\<partial>r1.rect_cube - {}) (\<partial>r1.rect_cube)"
        using chain_subdiv_chain_refl[OF two_cube_boundary_is_boundary] by auto
      show "\<forall>(k, \<gamma>)\<in>\<partial>r1.rect_cube. valid_path \<gamma>"
        using typeI_edges_are_valid_paths[OF r1.rect_cube_is_type_I] by auto
      show "boundary_chain (\<partial>r1.rect_cube)"
        using two_cube_boundary_is_boundary by auto
    qed  (auto simp add: two_cube_boundary_is_boundary)
  qed  (auto simp add: two_cube_boundary_is_boundary)
  moreover have "chain_orientation {(1::int, R2.typeI_to_typeII r2.rect_cube)} = (1::int)"
    apply(rule valid_two_chain_orient'[symmetric], auto)
    using valid_cube_valid_swap'[OF rev_orient_valid[OF r2.rect_cube_valid_two_cube r2.rect_cube_inj]]
    unfolding valid_two_chain_def r1.typeI_to_typeII_def by (auto simp add: o_def)
  moreover have "chain_orientation {(1::int, r1.rect_cube)} = (1::int)"
    apply(rule valid_two_chain_orient'[symmetric], auto)
    using r1.rect_cube_valid_two_cube
    unfolding valid_two_chain_def R2.typeI_to_typeII_def by (auto simp add: o_def)
  ultimately show ?thesis by auto
qed

end

end