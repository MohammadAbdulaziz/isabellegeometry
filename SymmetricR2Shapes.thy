theory SymmetricR2Shapes
  imports Green
begin 

lemma valid_cube_valid_swap:
  assumes "valid_two_cube C"
  shows "valid_two_cube (prod.swap o C o prod.swap)"
  using assms unfolding valid_two_cube_def boundary_def
  apply (auto simp: card_insert_if  split: if_split_asm)
   apply (metis swap_swap)+
  done

lemma valid_cube_valid_swap':
  assumes "valid_two_cube C"
  shows "valid_two_cube (prod.swap o C)"
  using assms unfolding valid_two_cube_def boundary_def
  apply (auto simp: card_insert_if  split: if_split_asm)
   apply (metis swap_swap)+
  done

lemma twoChainVertDiv_of_itself':
  assumes "finite C"
    "\<forall>(k, \<gamma>)\<in>(two_chain_boundary C). valid_path \<gamma>"
    "\<forall>twoCube\<in>C. typeI_twoCube twoCube i j"
  shows "only_vertical_division' (two_chain_boundary C) C"
proof(clarsimp simp add: only_vertical_division'_def, intro exI conjI, goal_cases)
  let ?\<H> = "two_chain_horizontal_boundary C"
  show 0: "\<forall>(k,\<gamma>)\<in>?\<H>. valid_path \<gamma>" using assms(2)
    by (auto simp add: two_chain_horizontal_boundary_def two_chain_boundary_def boundary_def)
  let ?\<V> = "(two_chain_vertical_boundary C)"
  show "finite ?\<H> " "finite ?\<V> "
    by (auto simp add: finite_two_chain_horizontal_boundary[OF assms(1)] finite_two_chain_vertical_boundary[OF assms(1)])
  have "\<And>a b. (a, b) \<in> two_chain_vertical_boundary C \<Longrightarrow>
                  \<exists>x\<in>two_chain_vertical_boundary C. case x of (k', \<gamma>') \<Rightarrow> \<exists>a\<in>{0..1}. \<exists>c\<in>{0..1}. a \<le> c \<and> subpath a c \<gamma>' = b"
    by (metis (mono_tags, lifting) atLeastAtMost_iff case_prod_conv le_numeral_extra(1) order_refl subpath_trivial)
  then show "\<forall>x\<in>two_chain_vertical_boundary C. case x of (k, \<gamma>) \<Rightarrow> \<exists>x\<in>two_chain_vertical_boundary C. case x of (k', \<gamma>') \<Rightarrow> \<exists>a\<in>{0..1}. \<exists>b\<in>{0..1}. a \<le> b \<and> subpath a b \<gamma>' = \<gamma>"
    by auto
  have "common_subdiv_exists ?\<H> ?\<H>"
    using gen_common_boundary_sudivision_exists_refl_twochain_boundary[OF 0 two_chain_horizontal_boundary_is_boundary_chain] assms(3)
    by auto
  then show "common_subdiv_exists (two_chain_horizontal_boundary C) (two_chain_horizontal_boundary C) \<or> common_reparam_exists (two_chain_horizontal_boundary C) (two_chain_horizontal_boundary C)"
    by auto
  show "boundary_chain ?\<H>"
    using two_chain_horizontal_boundary_is_boundary_chain assms(3) by auto
  have "\<And>a b. (a, b) \<in> two_chain_boundary C \<Longrightarrow> (a, b) \<notin> ?\<H> \<Longrightarrow> (a, b) \<in> two_chain_vertical_boundary C"
    by (auto simp add: two_chain_boundary_def two_chain_horizontal_boundary_def two_chain_vertical_boundary_def boundary_def)
  moreover have "\<And>a b. (a, b) \<in> two_chain_vertical_boundary C \<Longrightarrow> (a, b) \<in> two_chain_boundary C"
    "\<And>a b. (a, b) \<in> ?\<H> \<Longrightarrow> (a, b) \<in> two_chain_boundary C"
    by (auto simp add: two_chain_boundary_def two_chain_horizontal_boundary_def two_chain_vertical_boundary_def boundary_def)
  ultimately show "two_chain_boundary C = two_chain_vertical_boundary C \<union> two_chain_horizontal_boundary C" by auto
qed

(*lemma valid_path_valid_swap:
  assumes "valid_path (\<lambda>x::real. ((f x)::real, (g x)::real))"
  shows "valid_path (prod.swap o (\<lambda>x. (f x, g x)))"
  unfolding o_def valid_path_def piecewise_C1_differentiable_on_def swap_simp
proof (intro conjI)
  show "continuous_on {0..1} (\<lambda>x. (g x, f x))"
    using assms
    using continuous_on_Pair continuous_on_componentwise[where f = "(\<lambda>x. (f x, g x))"]
    by (auto simp add: real_pair_basis valid_path_def piecewise_C1_differentiable_on_def)
  show "\<exists>s. finite s \<and> (\<lambda>x. (g x, f x)) C1_differentiable_on {0..1} - s"
  proof -
    obtain s where s: "finite s" "(\<lambda>x. (f x, g x)) C1_differentiable_on {0..1} - s"
      using assms
      by (auto simp add: real_pair_basis valid_path_def piecewise_C1_differentiable_on_def)
    have 0: "f C1_differentiable_on {0..1} - s" using s(2) assms
      using C1_diff_components_2[where f = "(\<lambda>x. (f x, g x))"] 
      apply(auto simp add: real_pair_basis algebra_simps)
      using real_pair_basis by fastforce
    have 1: "g C1_differentiable_on {0..1} - s" using s(2) assms
      using C1_diff_components_2 real_pair_basis sledgehammer
    have *: "(\<lambda>x. (g x, f x)) C1_differentiable_on {0..1} - s"
      using 0 1 C1_differentiable_on_components[where f = "(\<lambda>x. (g x, f x))"]
      by (auto simp add: real_pair_basis valid_path_def piecewise_C1_differentiable_on_def)
    then show ?thesis using s(1) by auto
  qed
qed*)

lemma prod_comp_eq: 
  assumes "f = prod.swap o g"
  shows "prod.swap o f  = g"
  using swap_comp_swap assms
  by fastforce

context R2
begin

lemma pair_fun_components: "C = (\<lambda>x. (C x \<bullet> i, C x \<bullet> j))"
  by (simp add: i_is_x_axis inner_Pair_0 j_is_y_axis)

lemma swap_pair_fun: "(\<lambda>y. prod.swap (C (y, 0))) =  (\<lambda>x. (C (x, 0) \<bullet> j, C (x, 0) \<bullet> i))"
  by (simp add: prod.swap_def i_is_x_axis inner_Pair_0 j_is_y_axis)

lemma swap_pair_fun': "(\<lambda>y. prod.swap (C (y, 1))) =  (\<lambda>x. (C (x, 1) \<bullet> j, C (x, 1) \<bullet> i))"
  by (simp add: prod.swap_def i_is_x_axis inner_Pair_0 j_is_y_axis)

lemma swap_pair_fun'': "(\<lambda>y. prod.swap (C (0, y))) =  (\<lambda>x. (C (0,x) \<bullet> j, C (0,x) \<bullet> i))"
  by (simp add: prod.swap_def i_is_x_axis inner_Pair_0 j_is_y_axis)

lemma swap_pair_fun''': "(\<lambda>y. prod.swap (C (1, y))) =  (\<lambda>x. (C (1,x) \<bullet> j, C (1,x) \<bullet> i))"
  by (simp add: prod.swap_def i_is_x_axis inner_Pair_0 j_is_y_axis)

lemma swap_valid_boundaries:
  assumes "\<forall>(k,\<gamma>)\<in>\<partial>C. valid_path \<gamma>"
  assumes "(k,\<gamma>)\<in>\<partial>(C o prod.swap)"
  shows "valid_path \<gamma>"
  using assms
  by (auto simp add: boundary_def o_def real_pair_basis swap_pair_fun swap_pair_fun' swap_pair_fun'' swap_pair_fun''')

lemma swap_typeI_is_typeII:
  assumes "typeI_twoCube (orient, C) i j"
  shows "typeI_twoCube (orient, (prod.swap o C)) j i"
proof-
  obtain a b g1 g2 where C: "a \<noteq> b"
    "((\<forall>x\<in>{a..b} \<union> {b..a}. g2 x \<le> g1 x) \<or> (\<forall>x\<in>{a..b} \<union> {b..a}. g1 x \<le> g2 x))"
    "cubeImage C = {x *\<^sub>R i + y *\<^sub>R j |x y. x \<in> {a..b} \<union> {b..a} \<and> y \<in> {g2 x..g1 x} \<union> {g1 x..g2 x}}"
    "C = (\<lambda>(x, y). ((1 - x) * a + x * b) *\<^sub>R i + ((1 - y) * g2 ((1 - x) * a + x * b) + y * g1 ((1 - x) * a + x * b)) *\<^sub>R j)"
    "g1 piecewise_C1_differentiable_on {a..b} \<union> {b..a}"
    "g2 piecewise_C1_differentiable_on {a..b} \<union> {b..a}"
    "orient = (if a < b then 1 else - 1) * (if \<forall>x\<in>{a..b} \<union> {b..a}. g2 x \<le> g1 x then 1 else - 1)"
    using typeI_cube_explicit_spec'(1)[OF assms]
    by (auto simp add: linepath_def Let_def)
  show ?thesis
    unfolding typeI_twoCube.simps
  proof(intro exI conjI)
    show "a \<noteq> b" using C(1) by auto
    show "((\<forall>x\<in>{a..b} \<union> {b..a}. g2 x \<le> g1 x) \<or> (\<forall>x\<in>{a..b} \<union> {b..a}. g1 x \<le> g2 x))" using C(2) by auto
    show "prod.swap \<circ> C = (\<lambda>(t1, t2). let c = linepath a b t1 in c *\<^sub>R j + linepath (g2 c) (g1 c) t2 *\<^sub>R i)"    
      by (auto simp add: i_is_x_axis j_is_y_axis typeI_twoCube.simps prod.swap_def o_def linepath_def Let_def C(4))
  qed (force simp add: C(5-7))+
qed

lemma swap_neg_typeI_is_typeII:
  assumes "typeI_twoCube (orient, C) i j"
  shows "typeI_twoCube (-orient, prod.swap o C) (-j) i"
proof-
  obtain a b g1 g2 where C: "a \<noteq> b"
     "((\<forall>x\<in>{a..b} \<union> {b..a}. g2 x \<le> g1 x) \<or> (\<forall>x\<in>{a..b} \<union> {b..a}. g1 x \<le> g2 x))"
     "cubeImage C = {x *\<^sub>R i + y *\<^sub>R j |x y. x \<in>  {a..b} \<union> {b..a} \<and> y \<in> {g2 x..g1 x} \<union> {g1 x..g2 x}}"
     "C = (\<lambda>(x, y). ((1 - x) * a + x * b) *\<^sub>R i + ((1 - y) * g2 ((1 - x) * a + x * b) + y * g1 ((1 - x) * a + x * b)) *\<^sub>R j)"
     "g1 piecewise_C1_differentiable_on  {a..b} \<union> {b..a}"
     "g2 piecewise_C1_differentiable_on  {a..b} \<union> {b..a}"
     "orient = (if a < b then 1 else - 1) * (if \<forall>x\<in>{a..b} \<union> {b..a}. g2 x \<le> g1 x then 1 else - 1)"
    using typeI_twoCubeImg'[OF assms]
    by (auto simp add: linepath_def Let_def)
  show ?thesis
    apply (simp only: i_is_x_axis j_is_y_axis prod.swap_def o_def linepath_def Let_def typeI_twoCube.simps; intro exI conjI)
  proof-
    show "-a \<noteq> -b" using C(1) by auto
    show "(\<forall>x\<in>{- a..- b} \<union> {- b..- a}. (g2 o uminus) x \<le> (g1 o uminus) x) \<or> ((\<forall>x\<in>{- a..- b} \<union> {- b..- a}. (g1 o uminus) x \<le> (g2 o uminus) x))"
      using C(2) by auto
    show "(\<lambda>x. (snd (C x), fst (C x))) =
            (\<lambda>(t1, t2). ((1 - t1) *\<^sub>R - a + t1 *\<^sub>R - b) *\<^sub>R - (0, 1) + ((1 - t2) *\<^sub>R (g2 \<circ> uminus) ((1 - t1) *\<^sub>R - a + t1 *\<^sub>R - b) + t2 *\<^sub>R (g1 \<circ> uminus) ((1 - t1) *\<^sub>R - a + t1 *\<^sub>R - b)) *\<^sub>R (1, 0))"
      by(auto simp: C(4) algebra_simps fun_eq_iff i_is_x_axis j_is_y_axis)
    show "g1 o uminus piecewise_C1_differentiable_on {- a..- b} \<union> {- b..- a}"
         "g2 o uminus piecewise_C1_differentiable_on {- a..- b} \<union> {- b..- a}"
      apply(auto intro!: derivative_eq_intros piecewise_C1_differentiable_compose)
    proof-
      have " uminus ` ({- a..- b} \<union> {- b..- a}) = {a..b} \<union> {b..a}" using C(1) by auto
      then show "g1 piecewise_C1_differentiable_on uminus ` ({- a..- b} \<union> {- b..- a})"
                "g2 piecewise_C1_differentiable_on uminus ` ({- a..- b} \<union> {- b..- a})"
         using C(5,6) by auto
    next
      fix x::real
      have "uminus -` {x} = {- x}" by auto
      then show "finite (({- a..- b} \<union> {- b..- a}) \<inter> uminus -` {x})"
                "finite (({- a..- b} \<union> {- b..- a}) \<inter> uminus -` {x})"
        by auto
    qed
    show "- orient = (if - a < - b then 1 else - 1) * (if \<forall>x\<in>{- a..- b} \<union> {- b..- a}. (g2 \<circ> uminus) x \<le> (g1 \<circ> uminus) x then 1 else - 1)" using C(1,2) C(7)
      apply (auto simp add: o_def split: if_splits)
      by (smt atLeastAtMost_iff)+
  qed
qed

end

(*Reversing the orientation of a region*)

lemma inv_linepath: "linepath a b x = linepath b a (1 - x)"
  unfolding linepath_def by auto

lemma rev_orient_works:
  assumes "typeI_twoCube (orient, C) i j"
  shows "typeI_twoCube (-orient, C o (%(x,y). (1 - x, y))) i j"
proof-
  obtain a b g1 g2 where C: "a \<noteq> b"
     "((\<forall>x\<in>{a..b} \<union> {b..a}. g2 x \<le> g1 x) \<or> (\<forall>x\<in>{a..b} \<union> {b..a}. g1 x \<le> g2 x))"
     "cubeImage C = {x *\<^sub>R i + y *\<^sub>R j |x y. x \<in>  {a..b} \<union> {b..a} \<and> y \<in> {g2 x..g1 x} \<union> {g1 x..g2 x}}"
     "C = (\<lambda>(t1, t2). let c = linepath a b t1 in c *\<^sub>R i + linepath (g2 c) (g1 c) t2 *\<^sub>R j)"
     "g1 piecewise_C1_differentiable_on  {a..b} \<union> {b..a}"
     "g2 piecewise_C1_differentiable_on  {a..b} \<union> {b..a}"
     "orient = (if a < b then 1 else - 1) * (if \<forall>x\<in>{a..b} \<union> {b..a}. g2 x \<le> g1 x then 1 else - 1)"
    using typeI_cube_explicit_spec'[OF assms]
    by auto
  show ?thesis
    unfolding typeI_twoCube.simps o_def
  proof(intro exI conjI)
    show "b \<noteq> a" using C(1) by auto
    show "((\<forall>x\<in>{b..a} \<union> {a..b}. g2 x \<le> g1 x) \<or> (\<forall>x\<in>{b..a} \<union> {a..b}. g1 x \<le> g2 x))" using C(2) by auto
    show "(\<lambda>x. C (case x of (x, xa) \<Rightarrow> (1 - x, xa))) = (\<lambda>(t1, t2). let c = linepath b a t1 in c *\<^sub>R i + linepath (g2 c) (g1 c) t2 *\<^sub>R j)"
      unfolding C(4)
      by (subst inv_linepath, auto simp add: Let_def)
    show "g1 piecewise_C1_differentiable_on {b..a} \<union> {a..b}"
         "g2 piecewise_C1_differentiable_on {b..a} \<union> {a..b}"
      using C using Un_commute by metis+
    show "- orient = (if b < a then 1 else - 1) * (if \<forall>x\<in>{b..a} \<union> {a..b}. g2 x \<le> g1 x then 1 else - 1)" using C(1,7) by auto
  qed
qed

lemma case_prod: "(\<And>p1 p2. f (p1,p2)) \<Longrightarrow> (\<And>x. (case x of (x, y) \<Rightarrow> f (x,y)))" by auto
  
(*lemma rev_orient_works:
  assumes "typeII_twoCube (orient, C) i j"
  shows "typeII_twoCube (-orient, C o (%(x,y). (x, 1 - y))) i j"
proof-
  obtain a b g1 g2 where C: "a \<noteq> b"
     "((\<forall>x\<in>{a..b} \<union> {b..a}. g2 x < g1 x) \<or> (\<forall>x\<in>{a..b} \<union> {b..a}. g1 x < g2 x))"
     "cubeImage C = {x *\<^sub>R i + y *\<^sub>R j |x y. x \<in>  {a..b} \<union> {b..a} \<and> y \<in> {g2 x..g1 x} \<union> {g1 x..g2 x}}"
     "C = (\<lambda>(t1, t2). let c = linepath a b t1 in c *\<^sub>R i + linepath (g2 c) (g1 c) t2 *\<^sub>R j)"
     "g1 piecewise_C1_differentiable_on  {a..b} \<union> {b..a}"
     "g2 piecewise_C1_differentiable_on  {a..b} \<union> {b..a}"
     "orient = (if a < b then 1 else - 1) * (if \<forall>x\<in>{a..b} \<union> {b..a}. g2 x \<le> g1 x then 1 else - 1)"
    using typeII_cube_explicit_spec'[OF assms]
    sorry
  show ?thesis
    unfolding typeII_twoCube.simps o_def
  proof(intro exI conjI)
    show "a \<noteq> b" using C(1) by auto
    show "((\<forall>x\<in>{a..b} \<union> {b..a}. g1 x \<le> g2 x) \<or> (\<forall>x\<in>{a..b} \<union> {b..a}. g2 x \<le> g1 x))" using C(2) sorry
    show "(\<lambda>x. C (case x of (x, y) \<Rightarrow> (x, 1 - y))) = (\<lambda>(t1, t2). let c = linepath a b t1 in c *\<^sub>R i + linepath (g1 c) (g2 c) t2 *\<^sub>R j)"
      unfolding C(4)
      apply(intro HOL.ext, auto simp add: Let_def case_prod, subst inv_linepath[symmetric])
      by auto
    show "g1 piecewise_C1_differentiable_on {a..b} \<union> {b..a}"
         "g2 piecewise_C1_differentiable_on {a..b} \<union> {b..a}"
      using C using Un_commute by metis+
    have "-orient = (if a < b then 1 else - 1) * (if \<forall>x\<in>{a..b} \<union> {b..a}. g1 x \<le> g2 x then 1 else - 1)" 
      apply(split if_splits, intro conjI)
      subgoal using C(1,7) sledgehammer
      using C(1,2,7)
      apply (auto simp add: o_def split: if_splits)

      by (smt atLeastAtMost_iff)+

  qed
qed
*)

lemma rev_orient_same_img:
  assumes "typeI_twoCube (orient, C) i j"
  shows "cubeImage C = cubeImage (C o  (%(x,y). (1 - x, y)))"
proof-
  obtain a b g1 g2 where C: "a \<noteq> b"
     "((\<forall>x\<in>{a..b} \<union> {b..a}. g2 x \<le> g1 x) \<or> (\<forall>x\<in>{a..b} \<union> {b..a}. g1 x \<le> g2 x))"
     "cubeImage C = {x *\<^sub>R i + y *\<^sub>R j |x y. x \<in>  {a..b} \<union> {b..a} \<and> y \<in> {g2 x..g1 x} \<union> {g1 x..g2 x}}"
     "C = (\<lambda>(t1, t2). let c = linepath a b t1 in c *\<^sub>R i + linepath (g2 c) (g1 c) t2 *\<^sub>R j)"
     "g1 piecewise_C1_differentiable_on  {a..b} \<union> {b..a}"
     "g2 piecewise_C1_differentiable_on  {a..b} \<union> {b..a}"
     "orient = (if a < b then 1 else - 1) * (if \<forall>x\<in>{a..b} \<union> {b..a}. g2 x \<le> g1 x then 1 else - 1)"
    using typeI_cube_explicit_spec'[OF assms]
    by auto
  show ?thesis
  proof-
    have i: "(1 - a, b) \<in> unit_cube" if "0 \<le> a "" a \<le> 1 "" 0 \<le> b "" b \<le> 1" for a b
      using that by auto
    then show ?thesis
      unfolding cubeImage_def image_def
      apply (auto)
      by (smt i case_prod_conv)
  qed
qed


(*We need g1 and g2 to be different everywhere for injectivity
lemma pairI: "(\<forall>x\<in>s. f x) \<longleftrightarrow> (\<forall>(a,b)\<in>s. f (a,b))" by auto

lemma assumes 
  "typeII_twoCube (orient, C) i j"
  "i \<bullet> j = 0" "i \<bullet> i = 1" "j \<bullet> j = 1"
  shows "inj_on C unit_cube"
proof-
  obtain a b g1 g2 where C: "a \<noteq> b"
     "C = (\<lambda>(t1, t2). let c = linepath a b t1 in c *\<^sub>R i + linepath (g2 c) (g1 c) t2 *\<^sub>R j)"
    using typeII_cube_explicit_spec'[OF assms(1)]
    by auto
  show ?thesis
    unfolding C(2) inj_on_def
  proof(auto)
    fix a' a'' b' b''::real
    assume ass:"0 \<le> a'" "a' \<le> 1" "0 \<le> a''" "a'' \<le> 1" "0 \<le> b'" "0 \<le> b''" "b' \<le> 1" "b''\<le> 1"
    assume ass2: "(let c = linepath a b a' in c *\<^sub>R i + linepath (g2 c) (g1 c) b' *\<^sub>R j) = (let c = linepath a b a'' in c *\<^sub>R i + linepath (g2 c) (g1 c) b'' *\<^sub>R j)"
    then have "linepath a b a' = linepath a b a''" 
      apply(intro sum_on_base_2[OF _ assms(2-4)])
      unfolding Let_def .
    then show *: "a' = a''"
      apply(rule inj_onD[OF inj_on_linepath[OF C(1)]])
      using ass by simp+
    define c1 where "c1 =  linepath a b a'" 
    define c2 where "c2 =  linepath a b a''"
    with ass2 have b': "linepath (g2 c1) (g1 c1) b' = linepath (g2 c2) (g1 c2) b''"
      apply(intro sum_on_base_2(2)[OF _ assms(2-4)])
      unfolding Let_def c1_def by auto 
    show "b' = b''"
    proof(cases "(g2 c1) = (g1 c1)")
      case True
      have c1: "c2 = c1" using * unfolding c1_def c2_def by auto
      show ?thesis
        using b'
        unfolding linepath_def True c1
        apply (auto simp add: algebra_simps)
    next
      case False
      then show ?thesis sorry
    qed
      
      apply(rule inj_onD[OF inj_on_linepath[]])
      
      using ass by simp+*)

lemma rev_orient_valid:
  assumes "valid_two_cube C" "inj_on C unit_cube"
  shows "valid_two_cube (C o  (%(x,y). (1 - x, y)))"
  using assms unfolding valid_two_cube_def boundary_def
  apply (auto simp: card_insert_if o_def fun_eq_iff split: if_split_asm)
  using assms(2)
  by (smt atLeastAtMost_iff box_real(2) cbox_Pair_iff inj_onD)+

definition x_coord where "x_coord \<equiv> (\<lambda>t::real. t - 1/2)"

lemma x_coord_smooth: "x_coord C1_differentiable_on {a..b}"
  by (simp add: x_coord_def)

lemma x_coord_bounds:
  assumes "(0::real) \<le> x" "x \<le> 1"
  shows "-1/2 \<le> x_coord x \<and> x_coord x \<le> 1/2"
  using assms by(auto simp add: x_coord_def)

lemma x_coord_img: "x_coord ` {(0::real)..1} = {-1/2 .. 1/2}"
  by (auto simp add: x_coord_def image_def algebra_simps)

lemma x_coord_back_img: "finite ({0..1} \<inter> x_coord -` {x::real})"
  by (simp add: finite_vimageI inj_on_def x_coord_def)

abbreviation "rot_x t1 t2 \<equiv> (if (t1 - 1/2) \<le> 0 then (2 * t2 - 1) * t1 + 1/2 ::real else 2 * t2 - 2 * t1 * t2 +t1 -1/2::real)"

lemma rot_x_ivl:
  assumes "0 \<le> x"
    "x \<le> 1"
    "0 \<le> y"
    "y \<le> 1" 
  shows "0 \<le> rot_x x y \<and> rot_x x y \<le> 1"
proof - 
  have i: "\<And>a::real. a \<le> 0 \<Longrightarrow> 0 \<le> y \<Longrightarrow> y \<le> 1  \<Longrightarrow> -1/2 < a \<Longrightarrow> (a * (1 - 2*y) \<le> 1/2)"
  proof -
    have 0: "\<And>a::real. a \<le> 0 \<Longrightarrow> 0 \<le> y \<Longrightarrow> y \<le> 1  \<Longrightarrow> -1/2 < a \<Longrightarrow> (-a \<le> 1/2)"
      by (sos "((((A<0 * A<1) * R<1) + (R<1 * (R<1/4 * [2*a + 1]^2))))")
    have 1: "\<And>a. a \<le> 0 \<Longrightarrow> 0 \<le> y \<Longrightarrow> y \<le> 1  \<Longrightarrow> -1/2 < a \<Longrightarrow> (a * (1 - 2*y) \<le> -a)"
      by (sos "((((A<0 * A<1) * R<1) + (((A<=0 * (A<1 * R<1)) * (R<2/3 * [1]^2)) + (((A<=0 * (A<=2 * R<1)) * (R<2/3 * [1]^2)) + ((A<=0 * (A<=2 * (A<0 * R<1))) * (R<2/3 * [1]^2))))))")
    show "\<And>a::real. a \<le> 0 \<Longrightarrow> 0 \<le> y \<Longrightarrow> y \<le> 1 \<Longrightarrow> -1/2 < a \<Longrightarrow> (a * (1 - 2*y) \<le> 1/2)" using 0 1 by force
  qed
  have *: "(x * 2 + y * 4 \<le> 3 + x * (y * 4)) =  ( (x-1) \<le> 1/2+ (x-1) * (y * 2))"
    by (sos "((((A<0 * R<1) + ((A<=0 * R<1) * (R<2 * [1]^2)))) & (((A<0 * R<1) + ((A<=0 * R<1) * (R<1/2 * [1]^2)))))")
  show ?thesis
    using assms
    apply(auto simp add: algebra_simps divide_simps linorder_class.not_le)
       apply (sos "(((A<0 * R<1) + (((A<=2 * (A<=3 * R<1)) * (R<1 * [1]^2)) + (((A<=1 * R<1) * (R<1 * [1]^2)) + ((A<=0 * (A<=1 * R<1)) * (R<2 * [1]^2))))))")
      apply (sos "(((A<0 * R<1) + (((A<=2 * R<1) * (R<1 * [1]^2)) + (((A<=1 * (A<=3 * R<1)) * (R<1 * [1]^2)) + ((A<=0 * (A<=2 * R<1)) * (R<2 * [1]^2))))))")
    using i[of "(x::real) - 1"] affine_ineq
     apply (fastforce simp: algebra_simps *)+
    done
qed

definition "right_edge C = (1::int, \<lambda>t. C(1, t))"
definition "left_edge C = (-1::int, \<lambda>t. C(0, t))"
definition "top_edge C = (-1::int, \<lambda>t. C(t, 1))"
definition "bot_edge C = (1::int, \<lambda>t. C(t, 0))"

lemma boundary_edges:
    "boundary C =
           {top_edge C, bot_edge C, right_edge C, left_edge C}"
  unfolding top_edge_def bot_edge_def right_edge_def left_edge_def
  by (auto simp add:  valid_two_cube_def boundary_def Un_iff algebra_simps)+

lemma vertical_boundary_edges:
    "vertical_boundary C =
           {right_edge C, left_edge C}"
  unfolding top_edge_def bot_edge_def right_edge_def left_edge_def
  by (auto simp add:  valid_two_cube_def boundary_def Un_iff algebra_simps)+

lemma horizontal_boundary_edges:
    "horizontal_boundary C =
           {bot_edge C, top_edge C}"
  unfolding top_edge_def bot_edge_def right_edge_def left_edge_def
  by (auto simp add:  valid_two_cube_def boundary_def Un_iff algebra_simps)+

context R2
begin

definition "typeI_to_typeII C = prod.swap o C o (\<lambda>(x,y). (1 - x, y))"

lemma typeI_to_typeII_works:
     assumes "typeI_twoCube (orient, C) i j"
     shows " typeI_twoCube (orient, typeI_to_typeII C) (-j) i"
  unfolding typeI_to_typeII_def
  using rev_orient_works[OF swap_neg_typeI_is_typeII[OF assms]]
  by auto

end

end