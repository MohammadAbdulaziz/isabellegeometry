theory Paths
  imports Derivs General_Utils Integrals PartialDerivs "HOL-Library.Rewrite"
begin
(*This has everything related to paths purely*)

lemma reverse_subpaths_join:
  shows " subpath 1 (1 / 2) p +++ subpath (1 / 2) 0 p = reversepath p"
  using reversepath_subpath join_subpaths_middle pathfinish_subpath pathstart_subpath reversepath_joinpaths
  by (metis (no_types, lifting))

(*Below F cannot be from 'a \<Rightarrow> 'b because the dot product won't work.
  We have that g returns 'a and then F takes the output of g, so F should start from 'a
  Then we have to compute the dot product of the vector b with both the derivative of g, and F.
  Since the derivative of g returns the same type as g, accordingly F should return the same type as g, i.e. 'a.
 *)

definition line_integral:: "('a::euclidean_space \<Rightarrow> 'a) \<Rightarrow> 'a set \<Rightarrow> (real \<Rightarrow> 'a) \<Rightarrow> real" where
"line_integral F basis \<gamma> \<equiv> integral {0 .. 1} (\<lambda>x. \<Sum>b\<in>basis. (F(\<gamma> (x)) \<bullet> b) * (vector_derivative \<gamma> (at x within {0..1}) \<bullet> b))"

abbreviation "fancy_integral c a b \<equiv> line_integral a b c"

notation fancy_integral ("\<integral>\<^bsub>_\<^esub> _ \<downharpoonright>\<^bsub>_\<^esub>" 400)

abbreviation "fancy_integral2 c a \<equiv> line_integral a Basis c"

notation fancy_integral2 ("\<integral>\<^bsub>_\<^esub> _" 200)

lemma line_integral_empty_basis[simp]: "\<integral>\<^bsub>g\<^esub> F \<downharpoonright>\<^bsub>{}\<^esub> = 0"
  unfolding line_integral_def
  by auto

lemma line_integral_cong: "(\<forall>x\<in>path_image \<gamma>. F x = G x) \<Longrightarrow>  \<integral>\<^bsub>\<gamma>\<^esub> F \<downharpoonright>\<^bsub>B\<^esub> = \<integral>\<^bsub>\<gamma>\<^esub> G \<downharpoonright>\<^bsub>B\<^esub>"
  unfolding line_integral_def path_image_def apply(rule integral_cong)
  by auto

lemma line_integral_Basis: "(\<integral>\<^bsub>g\<^esub> F) = integral {0 .. 1} (\<lambda>x. F(g x) \<bullet> (vector_derivative g (at x within {0..1})))"
      unfolding line_integral_def apply (rule integral_cong)
      using euclidean_inner by metis

definition line_integral_exists where
  "line_integral_exists F basis \<gamma> \<equiv> (\<lambda>x. \<Sum>b\<in>basis. F(\<gamma> x) \<bullet> b * (vector_derivative \<gamma> (at x within {0..1}) \<bullet> b)) integrable_on {0..1}"

lemma scale_inner: "((*\<^sub>R) c\<^bsub>b\<^esub>) (F (\<gamma> x)) = c * (F\<^bsub>b\<^esub>) (\<gamma> x)" by auto

lemma "(\<Sum>b\<in>B. ((*\<^sub>R) c\<^bsub>b\<^esub>) (F (\<gamma> x)) * (vector_derivative \<gamma>\<^bsub>b\<^esub>) (at x within {0..1})) = c * (\<Sum>b\<in>B. (F\<^bsub>b\<^esub>) (\<gamma> x) * (vector_derivative \<gamma>\<^bsub>b\<^esub>) (at x within {0..1}))"
  by (simp add: scale_inner sum_distrib_left mult.assoc)

lemma line_integral_mult_left: "(c::real) * \<integral>\<^bsub>\<gamma>\<^esub> F \<downharpoonright>\<^bsub>B\<^esub> = \<integral>\<^bsub>\<gamma>\<^esub> %x. c *\<^sub>R (F x) \<downharpoonright>\<^bsub>B\<^esub>" "line_integral_exists F B \<gamma> \<Longrightarrow> line_integral_exists (%x. c *\<^sub>R (F x)) B \<gamma>"
  unfolding line_integral_def integral_mult_right[symmetric] apply(rule integral_cong, simp add: sum_distrib_left mult.assoc)
  unfolding line_integral_exists_def apply simp
proof-
  assume "(\<lambda>x. \<Sum>b\<in>B. (F\<^bsub>b\<^esub>) (\<gamma> x) * (vector_derivative \<gamma>\<^bsub>b\<^esub>) (at x within {0..1})) integrable_on {0..1}"
  then have "(\<lambda>x. c * (\<Sum>b\<in>B. (F\<^bsub>b\<^esub>) (\<gamma> x) * (vector_derivative \<gamma>\<^bsub>b\<^esub>) (at x within {0..1}))) integrable_on {0..1}"
    using integrable_cmul by force
  then obtain i where i: "((\<lambda>x. c * (\<Sum>b\<in>B. (F\<^bsub>b\<^esub>) (\<gamma> x) * (vector_derivative \<gamma>\<^bsub>b\<^esub>) (at x within {0..1}))) has_integral i) {0..1}"
    by auto
  have "((\<lambda>x. (\<Sum>b\<in>B. c * (F\<^bsub>b\<^esub>) (\<gamma> x) * (vector_derivative \<gamma>\<^bsub>b\<^esub>) (at x within {0..1}))) has_integral i) {0..1}"
    apply(rule has_integral_eq[OF _ i])
    by (simp add: sum_distrib_left mult.assoc)
  then show "(\<lambda>x. (\<Sum>b\<in>B. c * (F\<^bsub>b\<^esub>) (\<gamma> x) * (vector_derivative \<gamma>\<^bsub>b\<^esub>) (at x within {0..1}))) integrable_on {0..1}"
    by auto
qed

lemma line_integrable_Basis: "line_integral_exists F Basis g = ((\<lambda>x. F(g x) \<bullet> (vector_derivative g (at x within {0..1}))) integrable_on {0 .. 1})"
  unfolding line_integral_exists_def integrable_on_def
proof-
  have "((\<lambda>x. \<Sum>b\<in>Basis. (F\<^bsub>b\<^esub>) (g x) * (vector_derivative g\<^bsub>b\<^esub>) (at x within {0..1})) has_integral y) {0..1} =
    ((\<lambda>x. (F\<^bsub>vector_derivative g (at x within {0..1})\<^esub>) (g x)) has_integral y) {0..1}" for y
    apply(rule has_integral_cong)
    using  euclidean_inner euclidean_representation
    by (metis (full_types))
  then show "(\<exists>y. ((\<lambda>x. \<Sum>b\<in>Basis. (F\<^bsub>b\<^esub>) (g x) * (vector_derivative g\<^bsub>b\<^esub>) (at x within {0..1})) has_integral y) {0..1}) =
                            (\<exists>y. ((\<lambda>x. (F\<^bsub>vector_derivative g (at x within {0..1})\<^esub>) (g x)) has_integral y) {0..1})"
    by auto
qed

lemma line_integral_on_pair_straight_path:
  fixes F::"('a::euclidean_space) \<Rightarrow> 'a" and g :: "real \<Rightarrow> real" and \<gamma>
  assumes gamma_const: "\<forall>x. \<gamma>(x)\<bullet> i = a"
      and gamma_smooth: "\<forall>x \<in> {0 .. 1}. \<gamma> differentiable at x"          
  shows "\<integral>\<^bsub>\<gamma>\<^esub> F \<downharpoonright>\<^bsub>{i}\<^esub> = 0" "(line_integral_exists F {i} \<gamma>)"
proof (simp add: line_integral_def)
  have *: "F (\<gamma> x) \<bullet> i * (vector_derivative \<gamma> (at x within {0..1}) \<bullet> i) = 0"
    if "0 \<le> x \<and> x \<le> 1" for x
  proof -
    have "((\<lambda>x. \<gamma>(x)\<bullet> i) has_vector_derivative 0) (at x)"
      using vector_derivative_const_at[of "a" "x"] and gamma_const
      by auto
    then have "(vector_derivative \<gamma> (at x) \<bullet> i) = 0"
      using derivative_component_fun_component[ of "\<gamma>" "x" "i"]
        and gamma_smooth and that
      by (simp add: vector_derivative_at)
    then have "(vector_derivative \<gamma> (at x within {0 .. 1}) \<bullet> i) = 0"
      using has_vector_derivative_at_within vector_derivative_at_within_ivl that
      by (metis atLeastAtMost_iff gamma_smooth vector_derivative_works zero_less_one)
    then show ?thesis
      by auto
  qed
  then have "((\<lambda>x. F (\<gamma> x) \<bullet> i * (vector_derivative \<gamma> (at x within {0..1}) \<bullet> i)) has_integral 0) {0..1}"
    using has_integral_is_0[of "{0 .. 1}" "(\<lambda>x. F (\<gamma> x) \<bullet> i * (vector_derivative \<gamma> (at x within {0..1}) \<bullet> i))"]
    by auto
  then have "((\<lambda>x. F (\<gamma> x) \<bullet> i * (vector_derivative \<gamma> (at x within {0..1}) \<bullet> i)) integrable_on {0..1})"
    by auto
  then show "line_integral_exists F {i} \<gamma>" by (auto simp add:line_integral_exists_def)
  show "integral {0..1} (\<lambda>x. F (\<gamma> x) \<bullet> i * (vector_derivative \<gamma> (at x within {0..1}) \<bullet> i)) = 0"
    using * has_integral_is_0[of "{0 .. 1}" "(\<lambda>x. F (\<gamma> x) \<bullet> i * (vector_derivative \<gamma> (at x within {0..1}) \<bullet> i))"]
    by auto
qed

lemma line_integral_on_pair_path_strong:
  fixes F::"'a::euclidean_space \<Rightarrow> 'a" and
    g::"real \<Rightarrow> 'a" and
    \<gamma>::"real \<Rightarrow> 'a" and
    i::'a 
  assumes i_norm_1: "norm i = 1" and
    g_orthogonal_to_i: "\<forall>x. g(x) \<bullet> i = 0" and  
    gamma_is_in_terms_of_i: "\<gamma> = (\<lambda>x. f(x) *\<^sub>R i + g(f(x)))" and
    gamma_smooth: "\<gamma> piecewise_C1_differentiable_on {0 .. 1}" and
    g_continuous_on_f: "continuous_on (f ` {0..1}) g" and
   path_start_le_path_end: "(pathstart \<gamma>) \<bullet> i \<le> (pathfinish \<gamma>) \<bullet> i" and
    field_i_comp_cont: "continuous_on (path_image \<gamma>) (F\<^bsub>i\<^esub>)"
  shows "\<integral>\<^bsub>\<gamma>\<^esub> F \<downharpoonright>\<^bsub>{i}\<^esub>
         = integral {((pathstart \<gamma>) \<bullet> i) .. ((pathfinish \<gamma>) \<bullet> i)} (\<lambda>f_var. (F (f_var *\<^sub>R i + g(f_var)) \<bullet> i))"
    "line_integral_exists F {i} \<gamma>"
proof (simp add: line_integral_def)
  obtain s where gamma_differentiable: "finite s" "(\<forall>x \<in> {0 .. 1} - s. \<gamma> differentiable at x)"
    using gamma_smooth
    by (auto simp add: C1_differentiable_on_eq piecewise_C1_differentiable_on_def)
  then have gamma_i_component_smooth: "\<forall>x \<in> {0 .. 1} - s. (\<lambda>x. \<gamma> x \<bullet> i) differentiable at x"
    by auto
  have field_cont_on_path: "continuous_on ((\<lambda>x. \<gamma> x \<bullet> i) ` {0..1}) (\<lambda>f_var. F (f_var *\<^sub>R i + g f_var) \<bullet> i)"
  proof - 
    have 0: "(\<lambda>x. \<gamma> x \<bullet> i) = f"
    proof 
      fix x
      show "\<gamma> x \<bullet> i = f x"
        using g_orthogonal_to_i i_norm_1
        by (simp only: gamma_is_in_terms_of_i  real_inner_class.inner_add_left g_orthogonal_to_i inner_scaleR_left inner_same_Basis norm_eq_1)
    qed
    show ?thesis 
      unfolding 0 
      apply (rule continuous_on_compose2 [of _ "(\<lambda>x. F(x)  \<bullet> i)" "f ` { 0..1}" "(\<lambda>x. x *\<^sub>R i + g x)"]
          field_i_comp_cont g_continuous_on_f field_i_comp_cont continuous_intros)+
      by (auto simp add: gamma_is_in_terms_of_i path_image_def)
  qed
  have path_start_le_path_end': "\<gamma> 0 \<bullet> i \<le> \<gamma> 1 \<bullet> i" using path_start_le_path_end by (auto simp add: pathstart_def pathfinish_def)
  have gamm_cont: "continuous_on {0..1} (\<lambda>a. \<gamma> a \<bullet> i)"
    apply(rule continuous_on_inner)
    using gamma_smooth 
     apply (simp add: piecewise_C1_differentiable_on_def)
    using continuous_on_const by auto
  then obtain c d where cd: "c \<le> d" "(\<lambda>a. \<gamma> a \<bullet> i) ` {0..1} = {c..d}"
    by (meson continuous_image_closed_interval zero_le_one)
  then have subset_cd: "(\<lambda>a. \<gamma> a \<bullet> i) ` {0..1} \<subseteq> {c..d}" by auto
  have field_cont_on_path_cd:
    "continuous_on {c..d} (\<lambda>f_var. F (f_var *\<^sub>R i + g f_var) \<bullet> i)"
    using field_cont_on_path cd by auto
  have path_vector_deriv_line_integrals:
    "\<forall>x\<in>{0..1} - s. ((\<lambda>x. \<gamma> x \<bullet> i) has_vector_derivative 
                                          vector_derivative (\<lambda>x. \<gamma> x \<bullet> i) (at x))
                                                  (at x)"
    using gamma_i_component_smooth and derivative_component_fun_component and
      vector_derivative_works
    by blast       
  then have *: "\<And>x. x\<in>{0..1} - s \<Longrightarrow> ((\<lambda>x. \<gamma> x \<bullet> i) has_real_derivative 
                                          vector_derivative (\<lambda>x. \<gamma> x \<bullet> i) (at x within ({0..1})))
                                                  (at x within ({0..1}))"
    using has_vector_derivative_at_within vector_derivative_at_within_ivl has_field_derivative_iff_has_vector_derivative
    by fastforce
  have has_int:"((\<lambda>x. vector_derivative (\<lambda>x. \<gamma> x \<bullet> i) (at x within {0..1}) *\<^sub>R (F ((\<gamma> x \<bullet> i) *\<^sub>R i + g (\<gamma> x \<bullet> i)) \<bullet> i)) has_integral
           integral {\<gamma> 0 \<bullet> i..\<gamma> 1 \<bullet> i} (\<lambda>f_var. F (f_var *\<^sub>R i + g f_var) \<bullet> i)) {0..1}"
    using has_integral_substitution_strong[OF gamma_differentiable(1) rel_simps(44)
        path_start_le_path_end' subset_cd field_cont_on_path_cd gamm_cont *]
      gamma_is_in_terms_of_i
    by (auto simp only: has_field_derivative_iff_has_vector_derivative)
  then have has_int':"((\<lambda>x. (F(\<gamma>(x)) \<bullet> i)*(vector_derivative (\<lambda>x. \<gamma>(x) \<bullet> i) (at x within ({0..1})))) has_integral
           integral {((pathstart \<gamma>) \<bullet> i)..((pathfinish \<gamma>) \<bullet> i)} (\<lambda>f_var. F (f_var *\<^sub>R i + g f_var) \<bullet> i)) {0..1}"
    using  gamma_is_in_terms_of_i i_norm_1
    apply (auto simp add: pathstart_def pathfinish_def)
    apply (simp only:  real_inner_class.inner_add_left inner_not_same_Basis g_orthogonal_to_i inner_scaleR_left norm_eq_1)
    by (auto simp add: algebra_simps)
  have substitute:
    "integral ({((pathstart \<gamma>) \<bullet> i)..((pathfinish \<gamma>) \<bullet> i)}) (\<lambda>f_var. (F (f_var *\<^sub>R i + g(f_var)) \<bullet> i)) 
                 = integral ({0..1}) (\<lambda>x. (F(\<gamma>(x)) \<bullet> i)*(vector_derivative (\<lambda>x. \<gamma>(x) \<bullet> i) (at x within ({0..1}))))"
    using gamma_is_in_terms_of_i integral_unique[OF has_int] i_norm_1
    apply (auto simp add: pathstart_def pathfinish_def)
    apply (simp only:  real_inner_class.inner_add_left inner_not_same_Basis g_orthogonal_to_i inner_scaleR_left norm_eq_1)
    by (auto simp add: algebra_simps)
  have comp_in_eq_comp_out: "\<forall>x \<in> {0..1} - s.
            (vector_derivative (\<lambda>x. \<gamma>(x) \<bullet> i) (at x within {0..1}))
                = (vector_derivative \<gamma> (at x within {0..1})) \<bullet> i"
  proof
    fix x:: real
    assume ass:"x \<in> {0..1} -s"
    then have x_bounds:"x \<in> {0..1}" by auto
    have "\<gamma> differentiable at x" using ass gamma_differentiable by auto
    then have dotprod_in_is_out:
      "vector_derivative (\<lambda>x. \<gamma>(x) \<bullet> i) (at x)
                         = (vector_derivative \<gamma> (at x)) \<bullet> i"
      using derivative_component_fun_component 
      by force
    then have 0: "(vector_derivative \<gamma> (at x)) \<bullet> i = (vector_derivative \<gamma> (at x within {0..1})) \<bullet> i"
    proof -
      have "(\<gamma> has_vector_derivative vector_derivative \<gamma> (at x)) (at x)"
        using \<open>\<gamma> differentiable at x\<close> vector_derivative_works by blast
      moreover have "0 \<le> x \<and> x \<le> 1"
        using x_bounds by presburger
      ultimately show ?thesis
        by (simp add: vector_derivative_at_within_ivl)
    qed
    have 1: "vector_derivative (\<lambda>x. \<gamma>(x) \<bullet> i) (at x) = vector_derivative (\<lambda>x. \<gamma>(x) \<bullet> i) (at x within {0..1})"
      using path_vector_deriv_line_integrals and vector_derivative_at_within_ivl and
        x_bounds
      by (metis ass atLeastAtMost_iff zero_less_one)
    show "vector_derivative (\<lambda>x. \<gamma> x \<bullet> i) (at x within {0..1}) = vector_derivative \<gamma> (at x within {0..1}) \<bullet> i"
      using 0 and 1 and dotprod_in_is_out
      by auto
  qed
  show "integral {0..1} (\<lambda>x. F (\<gamma> x) \<bullet> i * (vector_derivative \<gamma> (at x within {0..1}) \<bullet> i)) =
                   integral {pathstart \<gamma> \<bullet> i..pathfinish \<gamma> \<bullet> i} (\<lambda>f_var. F (f_var *\<^sub>R i + g f_var) \<bullet> i)"
    using substitute and comp_in_eq_comp_out and negligible_finite
      Henstock_Kurzweil_Integration.integral_spike
      [of "s" "{0..1}" "(\<lambda>x. F (\<gamma> x) \<bullet> i * (vector_derivative \<gamma> (at x within {0..1}) \<bullet> i))"
        "(\<lambda>x. F (\<gamma> x) \<bullet> i * vector_derivative (\<lambda>x. \<gamma> x \<bullet> i) (at x within {0..1}))"]
    by (metis (no_types, lifting) gamma_differentiable(1))
  have "((\<lambda>x. F (\<gamma> x) \<bullet> i * (vector_derivative \<gamma> (at x within {0..1}) \<bullet> i)) has_integral
                   integral {pathstart \<gamma> \<bullet> i..pathfinish \<gamma> \<bullet> i} (\<lambda>f_var. F (f_var *\<^sub>R i + g f_var) \<bullet> i)) {0..1}"
    using has_int' and comp_in_eq_comp_out and negligible_finite
      Henstock_Kurzweil_Integration.has_integral_spike
      [of "s" "{0..1}" "(\<lambda>x. F (\<gamma> x) \<bullet> i * (vector_derivative \<gamma> (at x within {0..1}) \<bullet> i))"
        "(\<lambda>x. F (\<gamma> x) \<bullet> i * vector_derivative (\<lambda>x. \<gamma> x \<bullet> i) (at x within {0..1}))"
        "integral {pathstart \<gamma> \<bullet> i..pathfinish \<gamma> \<bullet> i} (\<lambda>f_var. F (f_var *\<^sub>R i + g f_var) \<bullet> i)"]
    by (metis (no_types, lifting) gamma_differentiable(1))
  then have "(\<lambda>x. F (\<gamma> x) \<bullet> i * (vector_derivative \<gamma> (at x within {0..1}) \<bullet> i)) integrable_on {0..1}"
    using integrable_on_def by auto
  then show "line_integral_exists F {i} \<gamma>"
    by (auto simp add: line_integral_exists_def)
qed

lemma line_integral_on_pair_path_strong':
  fixes F::"'a::euclidean_space \<Rightarrow> 'a" and
    g::"real \<Rightarrow> 'a" and
    \<gamma>::"real \<Rightarrow> 'a" and
    i::'a 
  assumes i_norm_1: "norm i = 1" and
    g_orthogonal_to_i: "\<forall>x. g(x) \<bullet> i = 0" and  
    gamma_is_in_terms_of_i: "\<gamma> = (\<lambda>x. f(x) *\<^sub>R i + g(f(x)))" and
    gamma_smooth: "\<gamma> piecewise_C1_differentiable_on {0 .. 1}" and
    g_continuous_on_f: "continuous_on (f ` {0..1}) g" and
  (*path_start_le_path_end: "(pathstart \<gamma>) \<bullet> i \<le> (pathfinish \<gamma>) \<bullet> i" and*)
    field_i_comp_cont: "continuous_on (path_image \<gamma>) (F\<^bsub>i\<^esub>)"
  shows "\<integral>\<^bsub>\<gamma>\<^esub> F \<downharpoonright>\<^bsub>{i}\<^esub>
         = integral {((pathstart \<gamma>) \<bullet> i) .. ((pathfinish \<gamma>) \<bullet> i)} (\<lambda>f_var. (F (f_var *\<^sub>R i + g(f_var)) \<bullet> i))
           - integral {((pathfinish \<gamma>) \<bullet> i) .. ((pathstart \<gamma>) \<bullet> i)} (\<lambda>f_var. (F (f_var *\<^sub>R i + g(f_var)) \<bullet> i))" (is ?g1)
    "line_integral_exists F {i} \<gamma>" (is ?g2)
proof-
  obtain s where gamma_differentiable: "finite s" "(\<forall>x \<in> {0 .. 1} - s. \<gamma> differentiable at x)"
    using gamma_smooth
    by (auto simp add: C1_differentiable_on_eq piecewise_C1_differentiable_on_def)
  then have gamma_i_component_smooth: "\<forall>x \<in> {0 .. 1} - s. (\<lambda>x. \<gamma> x \<bullet> i) differentiable at x"
    by auto
  have field_cont_on_path: "continuous_on ((\<lambda>x. \<gamma> x \<bullet> i) ` {0..1}) (\<lambda>f_var. F (f_var *\<^sub>R i + g f_var) \<bullet> i)"
  proof - 
    have 0: "(\<lambda>x. \<gamma> x \<bullet> i) = f"
    proof 
      fix x
      show "\<gamma> x \<bullet> i = f x"
        using g_orthogonal_to_i i_norm_1
        by (simp only: gamma_is_in_terms_of_i  real_inner_class.inner_add_left g_orthogonal_to_i inner_scaleR_left inner_same_Basis norm_eq_1)
    qed
    show ?thesis 
      unfolding 0 
      apply (rule continuous_on_compose2 [of _ "(\<lambda>x. F(x)  \<bullet> i)" "f ` { 0..1}" "(\<lambda>x. x *\<^sub>R i + g x)"]
          field_i_comp_cont g_continuous_on_f field_i_comp_cont continuous_intros)+
      by (auto simp add: gamma_is_in_terms_of_i path_image_def)
  qed
  (*have path_start_le_path_end': "\<gamma> 0 \<bullet> i \<le> \<gamma> 1 \<bullet> i" using path_start_le_path_end by (auto simp add: pathstart_def pathfinish_def)*)
  have gamm_cont: "continuous_on {0..1} (\<lambda>a. \<gamma> a \<bullet> i)"
    apply(rule continuous_on_inner)
    using gamma_smooth 
     apply (simp add: piecewise_C1_differentiable_on_def)
    using continuous_on_const by auto
  then obtain c d where cd: "c \<le> d" "(\<lambda>a. \<gamma> a \<bullet> i) ` {0..1} = {c..d}"
    by (meson continuous_image_closed_interval zero_le_one)
  then have subset_cd: "(\<lambda>a. \<gamma> a \<bullet> i) ` {0..1} \<subseteq> {c..d}" by auto
  have field_cont_on_path_cd:
    "continuous_on {c..d} (\<lambda>f_var. F (f_var *\<^sub>R i + g f_var) \<bullet> i)"
    using field_cont_on_path cd by auto
  have path_vector_deriv_line_integrals:
    "\<forall>x\<in>{0..1} - s. ((\<lambda>x. \<gamma> x \<bullet> i) has_vector_derivative 
                                          vector_derivative (\<lambda>x. \<gamma> x \<bullet> i) (at x))
                                                  (at x)"
    using gamma_i_component_smooth and derivative_component_fun_component and
      vector_derivative_works
    by blast       
  then have "\<forall>x\<in>{0..1} - s. ((\<lambda>x. \<gamma> x \<bullet> i) has_vector_derivative 
                                          vector_derivative (\<lambda>x. \<gamma> x \<bullet> i) (at x within ({0..1})))
                                                  (at x within ({0..1}))"
    using has_vector_derivative_at_within vector_derivative_at_within_ivl
    by fastforce
  then have *: "\<And>x. x\<in>{0..1} - s \<Longrightarrow> ((\<lambda>x. \<gamma> x \<bullet> i) has_real_derivative 
                                          vector_derivative (\<lambda>x. \<gamma> x \<bullet> i) (at x within ({0..1})))
                                                  (at x within ({0..1}))"
    using has_vector_derivative_at_within vector_derivative_at_within_ivl has_field_derivative_iff_has_vector_derivative
    by fastforce
  then have has_int:"((\<lambda>x. vector_derivative (\<lambda>x. \<gamma> x \<bullet> i) (at x within {0..1}) *\<^sub>R (F ((\<gamma> x \<bullet> i) *\<^sub>R i + g (\<gamma> x \<bullet> i)) \<bullet> i)) has_integral
           integral ({\<gamma> 0 \<bullet> i..\<gamma> 1 \<bullet> i}) (\<lambda>f_var. F (f_var *\<^sub>R i + g f_var) \<bullet> i) - integral ({\<gamma> 1 \<bullet> i..\<gamma> 0 \<bullet> i}) (\<lambda>f_var. F (f_var *\<^sub>R i + g f_var) \<bullet> i)) {0..1}"
      using has_integral_substitution_general[OF gamma_differentiable(1) _ subset_cd field_cont_on_path_cd gamm_cont *]
        gamma_is_in_terms_of_i
      by (auto simp only: has_field_derivative_iff_has_vector_derivative)
  then have has_int':"((\<lambda>x. (F(\<gamma>(x)) \<bullet> i)*(vector_derivative (\<lambda>x. \<gamma>(x) \<bullet> i) (at x within ({0..1})))) has_integral
           integral {((pathstart \<gamma>) \<bullet> i)..((pathfinish \<gamma>) \<bullet> i)} (\<lambda>f_var. F (f_var *\<^sub>R i + g f_var) \<bullet> i)
           - integral {((pathfinish \<gamma>) \<bullet> i)..((pathstart \<gamma>) \<bullet> i)} (\<lambda>f_var. F (f_var *\<^sub>R i + g f_var) \<bullet> i)) {0..1}"
    using  gamma_is_in_terms_of_i i_norm_1
    apply (auto simp add: pathstart_def pathfinish_def)
    apply (simp only:  real_inner_class.inner_add_left inner_not_same_Basis g_orthogonal_to_i inner_scaleR_left norm_eq_1)
    by (auto simp add: algebra_simps)
  have substitute:
    "integral {((pathstart \<gamma>) \<bullet> i)..((pathfinish \<gamma>) \<bullet> i)} (\<lambda>f_var. F (f_var *\<^sub>R i + g f_var) \<bullet> i)
           - integral {((pathfinish \<gamma>) \<bullet> i)..((pathstart \<gamma>) \<bullet> i)} (\<lambda>f_var. F (f_var *\<^sub>R i + g f_var) \<bullet> i) 
                 = integral ({0..1}) (\<lambda>x. (F(\<gamma>(x)) \<bullet> i)*(vector_derivative (\<lambda>x. \<gamma>(x) \<bullet> i) (at x within ({0..1}))))"
    using gamma_is_in_terms_of_i integral_unique[OF has_int] i_norm_1
    apply (auto simp add: pathstart_def pathfinish_def)
    apply (simp only:  real_inner_class.inner_add_left inner_not_same_Basis g_orthogonal_to_i inner_scaleR_left norm_eq_1)
    by (auto simp add: algebra_simps)
  have comp_in_eq_comp_out: "\<forall>x \<in> {0..1} - s.
            (vector_derivative (\<lambda>x. \<gamma>(x) \<bullet> i) (at x within {0..1}))
                = (vector_derivative \<gamma> (at x within {0..1})) \<bullet> i"
  proof
    fix x:: real
    assume ass:"x \<in> {0..1} -s"
    then have x_bounds:"x \<in> {0..1}" by auto
    have "\<gamma> differentiable at x" using ass gamma_differentiable by auto
    then have dotprod_in_is_out:
      "vector_derivative (\<lambda>x. \<gamma>(x) \<bullet> i) (at x)
                         = (vector_derivative \<gamma> (at x)) \<bullet> i"
      using derivative_component_fun_component 
      by force
    then have 0: "(vector_derivative \<gamma> (at x)) \<bullet> i = (vector_derivative \<gamma> (at x within {0..1})) \<bullet> i"
    proof -
      have "(\<gamma> has_vector_derivative vector_derivative \<gamma> (at x)) (at x)"
        using \<open>\<gamma> differentiable at x\<close> vector_derivative_works by blast
      moreover have "0 \<le> x \<and> x \<le> 1"
        using x_bounds by presburger
      ultimately show ?thesis
        by (simp add: vector_derivative_at_within_ivl)
    qed
    have 1: "vector_derivative (\<lambda>x. \<gamma>(x) \<bullet> i) (at x) = vector_derivative (\<lambda>x. \<gamma>(x) \<bullet> i) (at x within {0..1})"
      using path_vector_deriv_line_integrals and vector_derivative_at_within_ivl and
        x_bounds
      by (metis ass atLeastAtMost_iff zero_less_one)
    show "vector_derivative (\<lambda>x. \<gamma> x \<bullet> i) (at x within {0..1}) = vector_derivative \<gamma> (at x within {0..1}) \<bullet> i"
      using 0 and 1 and dotprod_in_is_out
      by auto
  qed
  show ?g1
    apply (simp add: line_integral_def)
    using substitute and comp_in_eq_comp_out and negligible_finite
      Henstock_Kurzweil_Integration.integral_spike
      [of "s" "{0..1}" "(\<lambda>x. F (\<gamma> x) \<bullet> i * (vector_derivative \<gamma> (at x within {0..1}) \<bullet> i))"
        "(\<lambda>x. F (\<gamma> x) \<bullet> i * vector_derivative (\<lambda>x. \<gamma> x \<bullet> i) (at x within {0..1}))"]
    by (metis (no_types, lifting) gamma_differentiable(1))
  have "((\<lambda>x. F (\<gamma> x) \<bullet> i * (vector_derivative \<gamma> (at x within {0..1}) \<bullet> i)) has_integral
                   integral {pathstart \<gamma> \<bullet> i..pathfinish \<gamma> \<bullet> i} (\<lambda>f_var. F (f_var *\<^sub>R i + g f_var) \<bullet> i)
                   - integral {pathfinish \<gamma> \<bullet> i..pathstart \<gamma> \<bullet> i} (\<lambda>f_var. F (f_var *\<^sub>R i + g f_var) \<bullet> i)) {0..1}"
    using has_int' and comp_in_eq_comp_out and negligible_finite
      Henstock_Kurzweil_Integration.has_integral_spike
      [of "s" "{0..1}" "(\<lambda>x. F (\<gamma> x) \<bullet> i * (vector_derivative \<gamma> (at x within {0..1}) \<bullet> i))"
        "(\<lambda>x. F (\<gamma> x) \<bullet> i * vector_derivative (\<lambda>x. \<gamma> x \<bullet> i) (at x within {0..1}))"
        "integral {pathstart \<gamma> \<bullet> i..pathfinish \<gamma> \<bullet> i} (\<lambda>f_var. F (f_var *\<^sub>R i + g f_var) \<bullet> i) - integral {pathfinish \<gamma> \<bullet> i..pathstart \<gamma> \<bullet> i} (\<lambda>f_var. F (f_var *\<^sub>R i + g f_var) \<bullet> i)"]
    by (metis (no_types, lifting) gamma_differentiable(1))
  then have "(\<lambda>x. F (\<gamma> x) \<bullet> i * (vector_derivative \<gamma> (at x within {0..1}) \<bullet> i)) integrable_on {0..1}"
    using integrable_on_def by auto
  then show "line_integral_exists F {i} \<gamma>"
    by (auto simp add: line_integral_exists_def)
qed

lemma line_integral_on_pair_path:
  fixes F::"'a::euclidean_space \<Rightarrow> 'a" and
    g::"real \<Rightarrow> 'a" and
    \<gamma>::"real \<Rightarrow> 'a" and
    i::'a 
  assumes i_norm_1: "norm i = 1" and
    g_orthogonal_to_i: "\<forall>x. g(x) \<bullet> i = 0" and  
    gamma_is_in_terms_of_i: "\<gamma> = (\<lambda>x. f(x) *\<^sub>R i + g(f(x)))" and
    gamma_smooth: "\<gamma> C1_differentiable_on {0 .. 1}" and
    g_continuous_on_f: "continuous_on (f ` {0..1}) g" and
    path_start_le_path_end: "(pathstart \<gamma>) \<bullet> i \<le> (pathfinish \<gamma>) \<bullet> i" and
    field_i_comp_cont: "continuous_on (path_image \<gamma>) (\<lambda>x. F x \<bullet> i)"
  shows "\<integral>\<^bsub>\<gamma>\<^esub> F \<downharpoonright>\<^bsub>{i}\<^esub>
                 = integral {((pathstart \<gamma>) \<bullet> i)..((pathfinish \<gamma>) \<bullet> i)} (\<lambda>f_var. (F (f_var *\<^sub>R i + g(f_var)) \<bullet> i))"
proof (simp add: line_integral_def)
  have gamma_differentiable: "\<forall>x \<in> {0 .. 1}. \<gamma> differentiable at x"
    using gamma_smooth  C1_differentiable_on_eq by blast
  then have gamma_i_component_smooth:
    "\<forall>x \<in> {0 .. 1}. (\<lambda>x. \<gamma> x \<bullet> i) differentiable at x"
    by auto
  have vec_deriv_i_comp_cont:
    "continuous_on {0..1} (\<lambda>x. vector_derivative (\<lambda>x. \<gamma> x \<bullet> i) (at x within {0..1}))"
  proof -
    have "continuous_on {0..1} (\<lambda>x. vector_derivative (\<lambda>x. \<gamma> x) (at x within {0..1}))"
      using gamma_smooth C1_differentiable_on_eq
      by (smt C1_differentiable_on_def atLeastAtMost_iff continuous_on_eq vector_derivative_at_within_ivl)
    then have deriv_comp_cont:
      "continuous_on {0..1} (\<lambda>x. vector_derivative (\<lambda>x. \<gamma> x) (at x within {0..1}) \<bullet> i)"
      by (simp add: continuous_intros)
    show ?thesis
      using derivative_component_fun_component_at_within[OF gamma_differentiable, of "i"]                    
        continuous_on_eq[OF deriv_comp_cont, of "(\<lambda>x. vector_derivative (\<lambda>x. \<gamma> x \<bullet> i) (at x within {0..1}))"]
      by fastforce
  qed
  have field_cont_on_path:
    "continuous_on ((\<lambda>x. \<gamma> x \<bullet> i) ` {0..1}) (\<lambda>f_var. F (f_var *\<^sub>R i + g f_var) \<bullet> i)"
  proof - 
    have 0: "(\<lambda>x. \<gamma> x \<bullet> i) = f"
    proof 
      fix x
      show "\<gamma> x \<bullet> i = f x"
        using g_orthogonal_to_i i_norm_1
        by (simp only: gamma_is_in_terms_of_i  real_inner_class.inner_add_left g_orthogonal_to_i inner_scaleR_left inner_same_Basis norm_eq_1)
    qed
    show ?thesis 
      unfolding 0 
      apply (rule continuous_on_compose2 [of _ "(\<lambda>x. F(x)  \<bullet> i)" "f ` { 0..1}" "(\<lambda>x. x *\<^sub>R i + g x)"]
          field_i_comp_cont g_continuous_on_f field_i_comp_cont continuous_intros)+
      by (auto simp add: gamma_is_in_terms_of_i path_image_def)
  qed
  have path_vector_deriv_line_integrals:
    "\<forall>x\<in>{0..1}. ((\<lambda>x. \<gamma> x \<bullet> i) has_vector_derivative 
                                          vector_derivative (\<lambda>x. \<gamma> x \<bullet> i) (at x))
                                                  (at x)"
    using gamma_i_component_smooth and derivative_component_fun_component and
      vector_derivative_works
    by blast       
  then have "\<forall>x\<in>{0..1}. ((\<lambda>x. \<gamma> x \<bullet> i) has_vector_derivative 
                                          vector_derivative (\<lambda>x. \<gamma> x \<bullet> i) (at x within {0..1}))
                                                  (at x within {0..1})"
    using has_vector_derivative_at_within vector_derivative_at_within_ivl
    by fastforce
  then have substitute:
    "integral (cbox ((pathstart \<gamma>) \<bullet> i) ((pathfinish \<gamma>) \<bullet> i)) (\<lambda>f_var. (F (f_var *\<^sub>R i + g(f_var)) \<bullet> i)) 
                 = integral (cbox 0 1) (\<lambda>x. (F(\<gamma>(x)) \<bullet> i)*(vector_derivative (\<lambda>x. \<gamma>(x) \<bullet> i) (at x within {0..1})))"
    using gauge_integral_by_substitution
      [of "0" "1" "(\<lambda>x. (\<gamma> x) \<bullet> i)"
        "(\<lambda>x. vector_derivative (\<lambda>x. \<gamma>(x) \<bullet> i) (at x within {0..1}))"
        "(\<lambda>f_var. (F (f_var *\<^sub>R i + g(f_var)) \<bullet> i))"] and
      path_start_le_path_end and vec_deriv_i_comp_cont and field_cont_on_path and 
      gamma_is_in_terms_of_i i_norm_1
    apply (auto simp add: pathstart_def pathfinish_def)
    apply (simp only:  real_inner_class.inner_add_left inner_not_same_Basis g_orthogonal_to_i inner_scaleR_left norm_eq_1)
    by (auto)
      (*integration by substitution*)
  have comp_in_eq_comp_out: "\<forall>x \<in> {0..1}.
            (vector_derivative (\<lambda>x. \<gamma>(x) \<bullet> i) (at x within {0..1}))
                = (vector_derivative \<gamma> (at x within {0..1})) \<bullet> i"
  proof
    fix x:: real
    assume x_bounds: "x \<in> {0..1}"
    then have "\<gamma> differentiable at x" using gamma_differentiable by auto
    then have dotprod_in_is_out:
      "vector_derivative (\<lambda>x. \<gamma>(x) \<bullet> i) (at x)
                         = (vector_derivative \<gamma> (at x)) \<bullet> i"
      using derivative_component_fun_component 
      by force
    then have 0: "(vector_derivative \<gamma> (at x)) \<bullet> i
                         = (vector_derivative \<gamma> (at x within {0..1})) \<bullet> i"
      using has_vector_derivative_at_within and x_bounds and vector_derivative_at_within_ivl
      by (smt atLeastAtMost_iff gamma_differentiable inner_commute vector_derivative_works)
    have 1: "vector_derivative (\<lambda>x. \<gamma>(x) \<bullet> i) (at x) = vector_derivative (\<lambda>x. \<gamma>(x) \<bullet> i) (at x within {0..1})"
      using path_vector_deriv_line_integrals and vector_derivative_at_within_ivl and
        x_bounds
      by fastforce
    show "vector_derivative (\<lambda>x. \<gamma> x \<bullet> i) (at x within {0..1}) = vector_derivative \<gamma> (at x within {0..1}) \<bullet> i"
      using 0 and 1 and dotprod_in_is_out
      by auto
  qed
  show "integral {0..1} (\<lambda>x. F (\<gamma> x) \<bullet> i * (vector_derivative \<gamma> (at x within {0..1}) \<bullet> i)) =
                   integral {pathstart \<gamma> \<bullet> i..pathfinish \<gamma> \<bullet> i} (\<lambda>f_var. F (f_var *\<^sub>R i + g f_var) \<bullet> i)"
    using substitute and comp_in_eq_comp_out and 
      Henstock_Kurzweil_Integration.integral_cong
      [of "{0..1}" "(\<lambda>x. F (\<gamma> x) \<bullet> i * (vector_derivative \<gamma> (at x within {0..1}) \<bullet> i))"
        "(\<lambda>x. F (\<gamma> x) \<bullet> i * vector_derivative (\<lambda>x. \<gamma> x \<bullet> i) (at x within {0..1}))"]
    by auto
qed

lemma content_box_cases:
  "content (box a b) = (if \<forall>i\<in>Basis. a\<bullet>i \<le> b\<bullet>i then prod (\<lambda>i. b\<bullet>i - a\<bullet>i) Basis else 0)"
  by (simp add: measure_lborel_box_eq inner_diff)

lemma content_box_cbox:
  shows "content (box a b) = content (cbox a b)"
  by (simp add: content_box_cases content_cbox_cases)

lemma content_eq_0: "content (box a b) = 0 \<longleftrightarrow> (\<exists>i\<in>Basis. b\<bullet>i \<le> a\<bullet>i)"
  by (auto simp: content_box_cases not_le intro: less_imp_le antisym eq_refl)

lemma content_pos_lt_eq: "0 < content (cbox a (b::'a::euclidean_space)) \<longleftrightarrow> (\<forall>i\<in>Basis. a\<bullet>i < b\<bullet>i)"
  by (auto simp add: content_cbox_cases less_le prod_nonneg)

lemma content_lt_nz: "0 < content (box a b) \<longleftrightarrow> content (box a b) \<noteq> 0"
  using Paths.content_eq_0 zero_less_measure_iff by blast

lemma content_subset: "cbox a b \<subseteq> box c d \<Longrightarrow> content (cbox a b) \<le> content (box c d)"
  unfolding measure_def
  by (intro enn2real_mono emeasure_mono) (auto simp: emeasure_lborel_cbox_eq emeasure_lborel_box_eq)

lemma sum_content_null:
  assumes "content (box a b) = 0"
    and "p tagged_division_of (box a b)"
  shows "sum (\<lambda>(x,k). content k *\<^sub>R f x) p = (0::'a::real_normed_vector)"
proof (rule sum.neutral, rule)
  fix y
  assume y: "y \<in> p"
  obtain x k where xk: "y = (x, k)"
    using surj_pair[of y] by blast
  note assm = tagged_division_ofD(3-4)[OF assms(2) y[unfolded xk]]
  from this(2) obtain c d where k: "k = cbox c d" by blast
  have "(\<lambda>(x, k). content k *\<^sub>R f x) y = content k *\<^sub>R f x"
    unfolding xk by auto
  also have "\<dots> = 0"
    using content_subset[OF assm(1)[unfolded k]] content_pos_le[of "cbox c d"]
    unfolding assms(1) k
    by auto
  finally show "(\<lambda>(x, k). content k *\<^sub>R f x) y = 0" .
qed


lemma has_integral_null [intro]: "content(box a b) = 0 \<Longrightarrow> (f has_integral 0) (box a b)"
  by (simp add: content_box_cbox content_eq_0_interior)

declare[[smt_timeout=400]]

lemma line_integral_distrib:
  assumes "line_integral_exists f basis \<gamma>1"
    "line_integral_exists f basis \<gamma>2"
    "valid_path \<gamma>1" "valid_path \<gamma>2"
  shows "\<integral>\<^bsub>\<gamma>1 +++ \<gamma>2\<^esub> f\<downharpoonright>\<^bsub>basis\<^esub> =  \<integral>\<^bsub>\<gamma>1\<^esub> f\<downharpoonright>\<^bsub>basis\<^esub> + \<integral>\<^bsub>\<gamma>2\<^esub> f\<downharpoonright>\<^bsub>basis\<^esub>"
    "line_integral_exists f basis (\<gamma>1 +++ \<gamma>2)"
proof -
  obtain s1 s2
    where s1: "finite s1" "\<forall>x\<in>{0..1} - s1. \<gamma>1 differentiable at x"
      and s2: "finite s2" "\<forall>x\<in>{0..1} - s2. \<gamma>2 differentiable at x"
    using assms
    by (auto simp: valid_path_def piecewise_C1_differentiable_on_def C1_differentiable_on_eq)
  obtain i1 i2
    where 1: "((\<lambda>x. \<Sum>b\<in>basis. f (\<gamma>1 x) \<bullet> b * (vector_derivative \<gamma>1 (at x within {0..1}) \<bullet> b)) has_integral i1) {0..1}"
      and 2: "((\<lambda>x. \<Sum>b\<in>basis. f (\<gamma>2 x) \<bullet> b * (vector_derivative \<gamma>2 (at x within {0..1}) \<bullet> b)) has_integral i2) {0..1}"
    using assms
    by (auto simp: line_integral_exists_def)
  have i1: "((\<lambda>x. 2 * (\<Sum>b\<in>basis. f (\<gamma>1 (2 * x)) \<bullet> b * (vector_derivative \<gamma>1 (at (2 * x) within {0..1}) \<bullet> b))) has_integral i1) {0..1/2}"
   and i2: "((\<lambda>x. 2 * (\<Sum>b\<in>basis. f (\<gamma>2 (2 * x - 1)) \<bullet> b * (vector_derivative \<gamma>2 (at ((2 * x) - 1) within {0..1}) \<bullet> b))) has_integral i2) {1/2..1}"
    using has_integral_affinity01 [OF 1, where m= 2 and c=0, THEN has_integral_cmul [where c=2]]
      has_integral_affinity01 [OF 2, where m= 2 and c="-1", THEN has_integral_cmul [where c=2]]
    by (simp_all only: image_affinity_atLeastAtMost_div_diff, simp_all add: scaleR_conv_of_real mult_ac)
  have \<gamma>1: "\<lbrakk>0 \<le> z; z*2 < 1; z*2 \<notin> s1\<rbrakk> \<Longrightarrow>
            vector_derivative (\<lambda>x. if x*2 \<le> 1 then \<gamma>1 (2*x) else \<gamma>2 (2*x - 1)) (at z within {0..1}) =
            2 *\<^sub>R vector_derivative \<gamma>1 (at (z*2) within {0..1})" for z
  proof -
    have i:"\<lbrakk>0 \<le> z; z*2 < 1; z*2 \<notin> s1\<rbrakk> \<Longrightarrow>
              vector_derivative (\<lambda>x. if x * 2 \<le> 1 then \<gamma>1 (2 * x) else \<gamma>2 (2 * x - 1)) (at z within {0..1}) = 2 *\<^sub>R vector_derivative \<gamma>1 (at (z * 2))" for z
    proof-
      have \<gamma>1_at_z:"\<lbrakk>0 \<le> z; z*2 < 1; z*2 \<notin> s1\<rbrakk> \<Longrightarrow>
                         ((\<lambda>x. if x*2 \<le> 1 then \<gamma>1 (2*x) else \<gamma>2 (2*x - 1)) has_vector_derivative
                         2 *\<^sub>R vector_derivative \<gamma>1 (at (z*2))) (at z)" for z
        apply (rule has_vector_derivative_transform_at [of "\<bar>z - 1/2\<bar>" _ "(\<lambda>x. \<gamma>1(2*x))"])
          apply (simp_all add: dist_real_def abs_if split: if_split_asm)
        apply (rule vector_diff_chain_at [of "\<lambda>x. 2*x" 2 _ \<gamma>1, simplified o_def])
         apply (simp add: has_vector_derivative_def has_derivative_def bounded_linear_mult_left)
        using s1
        apply (auto simp: algebra_simps vector_derivative_works)
        done
      assume ass: "0 \<le> z" "z*2 < 1" "z*2 \<notin> s1"
      then have z_ge: "z\<le> 1" by auto
      show "vector_derivative (\<lambda>x. if x * 2 \<le> 1 then \<gamma>1 (2 * x) else \<gamma>2 (2 * x - 1)) (at z within {0..1}) = 2 *\<^sub>R vector_derivative \<gamma>1 (at (z * 2))"
        using Derivative.vector_derivative_at_within_ivl[OF \<gamma>1_at_z[OF ass] ass(1) z_ge]
        by auto
    qed
    assume ass: "0 \<le> z" "z*2 < 1" "z*2 \<notin> s1"
    then have "(\<gamma>1 has_vector_derivative ((vector_derivative \<gamma>1 (at (z*2))))) (at (z*2))"
      using s1 by (auto simp: algebra_simps vector_derivative_works)
    then have ii: "(vector_derivative \<gamma>1 (at (z*2) within {0..1})) = (vector_derivative \<gamma>1 (at (z*2)))"
      using Derivative.vector_derivative_at_within_ivl ass
      by force
    show "vector_derivative (\<lambda>x. if x * 2 \<le> 1 then \<gamma>1 (2 * x) else \<gamma>2 (2 * x - 1)) (at z within {0..1}) = 2 *\<^sub>R vector_derivative \<gamma>1 (at (z * 2) within {0..1})"
      using i[OF ass] ii
      by auto
  qed
  have \<gamma>2: "\<lbrakk>1 < z*2; z \<le> 1; z*2 - 1 \<notin> s2\<rbrakk> \<Longrightarrow>
            vector_derivative (\<lambda>x. if x*2 \<le> 1 then \<gamma>1 (2*x) else \<gamma>2 (2*x - 1)) (at z within {0..1}) =
            2 *\<^sub>R vector_derivative \<gamma>2 (at (z*2 - 1) within {0..1})" for z       proof -
    have i:"\<lbrakk>1 < z*2; z \<le> 1; z*2 - 1 \<notin> s2\<rbrakk> \<Longrightarrow>
              vector_derivative (\<lambda>x. if x * 2 \<le> 1 then \<gamma>1 (2 * x) else \<gamma>2 (2 * x - 1)) (at z within {0..1})
                   = 2 *\<^sub>R vector_derivative \<gamma>2 (at (z * 2 - 1))" for z
    proof-
      have \<gamma>2_at_z:"\<lbrakk>1 < z*2; z \<le> 1; z*2 - 1 \<notin> s2\<rbrakk> \<Longrightarrow>
                      ((\<lambda>x. if x*2 \<le> 1 then \<gamma>1 (2*x) else \<gamma>2 (2*x - 1)) has_vector_derivative 2 *\<^sub>R vector_derivative \<gamma>2 (at (z*2 - 1))) (at z)" for z
        apply (rule has_vector_derivative_transform_at [of "\<bar>z - 1/2\<bar>" _ "(\<lambda>x. \<gamma>2 (2*x - 1))"])
          apply (simp_all add: dist_real_def abs_if split: if_split_asm)
        apply (rule vector_diff_chain_at [of "\<lambda>x. 2*x - 1" 2 _ \<gamma>2, simplified o_def])
         apply (simp add: has_vector_derivative_def has_derivative_def bounded_linear_mult_left)
        using s2
        apply (auto simp: algebra_simps vector_derivative_works)
        done
      assume ass: "1 < z*2" "z \<le> 1" "z*2 - 1 \<notin> s2"
      then have z_le: "z\<le> 1" by auto
      have z_ge: "0 \<le> z" using ass by auto
      show "vector_derivative (\<lambda>x. if x * 2 \<le> 1 then \<gamma>1 (2 * x) else \<gamma>2 (2 * x - 1)) (at z within {0..1})
                                = 2 *\<^sub>R vector_derivative \<gamma>2 (at (z * 2 - 1))"
        using Derivative.vector_derivative_at_within_ivl[OF \<gamma>2_at_z[OF ass] z_ge z_le]
        by auto
    qed
    assume ass: "1 < z*2" "z \<le> 1" "z*2 - 1 \<notin> s2"
    then have "(\<gamma>2 has_vector_derivative ((vector_derivative \<gamma>2 (at (z*2 - 1))))) (at (z*2 - 1))"
      using s2 by (auto simp: algebra_simps vector_derivative_works)
    then have ii: "(vector_derivative \<gamma>2 (at (z*2 - 1) within {0..1})) = (vector_derivative \<gamma>2 (at (z*2 - 1)))"
      using Derivative.vector_derivative_at_within_ivl ass
      by force
    show "vector_derivative (\<lambda>x. if x * 2 \<le> 1 then \<gamma>1 (2 * x) else \<gamma>2 (2 * x - 1)) (at z within {0..1}) = 2 *\<^sub>R vector_derivative \<gamma>2 (at (z * 2 - 1) within {0..1})"
      using i[OF ass] ii
      by auto
  qed
  have lem1: "((\<lambda>x. \<Sum>b\<in>basis. f ((\<gamma>1+++\<gamma>2) x) \<bullet> b * (vector_derivative (\<gamma>1+++\<gamma>2) (at x within {0..1}) \<bullet> b)) has_integral i1) {0..1/2}"
    apply (rule has_integral_spike_finite [OF _ _ i1, of "insert (1/2) ((*)2 -` s1)"])
    using s1
     apply (force intro: finite_vimageI [where h = "(*)2"] inj_onI)
    apply (clarsimp simp add: joinpaths_def scaleR_conv_of_real mult_ac \<gamma>1)
    by (simp add: sum_distrib_left)
  moreover have lem2: "((\<lambda>x. \<Sum>b\<in>basis. f ((\<gamma>1+++\<gamma>2) x) \<bullet> b * (vector_derivative (\<gamma>1+++\<gamma>2) (at x within {0..1}) \<bullet> b)) has_integral i2) {1/2..1}"
    apply (rule has_integral_spike_finite [OF _ _ i2, of "insert (1/2) ((\<lambda>x. 2*x-1) -` s2)"])
    using s2
     apply (force intro: finite_vimageI [where h = "\<lambda>x. 2*x-1"] inj_onI)
    apply (clarsimp simp add: joinpaths_def scaleR_conv_of_real mult_ac \<gamma>2)
    by (simp add: sum_distrib_left)
  ultimately
  show "line_integral f basis (\<gamma>1 +++ \<gamma>2) = line_integral f basis \<gamma>1 + line_integral f basis \<gamma>2"
    apply (simp add: line_integral_def)
    apply (rule integral_unique [OF has_integral_combine [where c = "1/2"]])
    using 1 2 integral_unique apply auto
    done
  show "line_integral_exists f basis (\<gamma>1 +++ \<gamma>2)"
  proof (simp add: line_integral_exists_def integrable_on_def)
    have "(1::real) \<le> 1 * 2 \<and> (0::real) \<le> 1 / 2"
      by simp
    then show "\<exists>r. ((\<lambda>r. \<Sum>a\<in>basis. f ((\<gamma>1 +++ \<gamma>2) r) \<bullet> a * (vector_derivative (\<gamma>1 +++ \<gamma>2) (at r within {0..1}) \<bullet> a)) has_integral r) {0..1}"
    using has_integral_combine [where c = "1/2"] 1 2 divide_le_eq_numeral1(1) lem1 lem2 by blast
  qed
qed

lemma line_integral_exists_joinD1:
  assumes "line_integral_exists f basis (g1 +++ g2)" "valid_path g1"
  shows "line_integral_exists f basis g1"
proof -
  obtain s1
    where s1: "finite s1" "\<forall>x\<in>{0..1} - s1. g1 differentiable at x"
    using assms by (auto simp: valid_path_def piecewise_C1_differentiable_on_def C1_differentiable_on_eq)
  have "(\<lambda>x. \<Sum>b\<in>basis. f ((g1 +++ g2) (x/2)) \<bullet> b * (vector_derivative (g1 +++ g2) (at (x/2) within {0..1}) \<bullet> b)) integrable_on {0..1}"
    using assms
    apply (auto simp: line_integral_exists_def)
    apply (drule integrable_on_subcbox [where a=0 and b="1/2"])
     apply (auto intro: integrable_affinity [of _ 0 "1/2::real" "1/2" 0, simplified])
    done
  then have *:"(\<lambda>x. \<Sum>b\<in>basis. ((f ((g1 +++ g2) (x/2)) \<bullet> b) / 2)* (vector_derivative (g1 +++ g2) (at (x/2) within {0..1}) \<bullet> b)) integrable_on {0..1}"
    by (auto simp: Groups_Big.sum_distrib_left dest: integrable_cmul [where c="1/2"] simp: scaleR_conv_of_real)
  have g1: "\<lbrakk>0 \<le> z; z*2 < 1; z*2 \<notin> s1\<rbrakk> \<Longrightarrow>
            vector_derivative (\<lambda>x. if x*2 \<le> 1 then g1 (2*x) else g2 (2*x - 1)) (at z within {0..1}) =
            2 *\<^sub>R vector_derivative g1 (at (z*2) within {0..1})" for z
  proof -
    have i:"\<lbrakk>0 \<le> z; z*2 < 1; z*2 \<notin> s1\<rbrakk> \<Longrightarrow>
              vector_derivative (\<lambda>x. if x * 2 \<le> 1 then g1 (2 * x) else g2 (2 * x - 1)) (at z within {0..1}) = 2 *\<^sub>R vector_derivative g1 (at (z * 2))" for z
    proof-
      have g1_at_z:"\<lbrakk>0 \<le> z; z*2 < 1; z*2 \<notin> s1\<rbrakk> \<Longrightarrow>
                         ((\<lambda>x. if x*2 \<le> 1 then g1 (2*x) else g2 (2*x - 1)) has_vector_derivative
                         2 *\<^sub>R vector_derivative g1 (at (z*2))) (at z)" for z
        apply (rule has_vector_derivative_transform_at [of "\<bar>z - 1/2\<bar>" _ "(\<lambda>x. g1(2*x))"])
          apply (simp_all add: dist_real_def abs_if split: if_split_asm)
        apply (rule vector_diff_chain_at [of "\<lambda>x. 2*x" 2 _ g1, simplified o_def])
         apply (simp add: has_vector_derivative_def has_derivative_def bounded_linear_mult_left)
        using s1
        apply (auto simp: algebra_simps vector_derivative_works)
        done
      assume ass: "0 \<le> z" "z*2 < 1" "z*2 \<notin> s1"
      then have z_ge: "z\<le> 1" by auto
      show "vector_derivative (\<lambda>x. if x * 2 \<le> 1 then g1 (2 * x) else g2 (2 * x - 1)) (at z within {0..1}) = 2 *\<^sub>R vector_derivative g1 (at (z * 2))"
        using Derivative.vector_derivative_at_within_ivl[OF g1_at_z[OF ass] ass(1) z_ge]
        by auto
    qed
    assume ass: "0 \<le> z" "z*2 < 1" "z*2 \<notin> s1"
    then have "(g1 has_vector_derivative ((vector_derivative g1 (at (z*2))))) (at (z*2))"
      using s1 by (auto simp: algebra_simps vector_derivative_works)
    then have ii: "(vector_derivative g1 (at (z*2) within {0..1})) = (vector_derivative g1 (at (z*2)))"
      using Derivative.vector_derivative_at_within_ivl ass by force
    show "vector_derivative (\<lambda>x. if x * 2 \<le> 1 then g1 (2 * x) else g2 (2 * x - 1)) (at z within {0..1}) = 2 *\<^sub>R vector_derivative g1 (at (z * 2) within {0..1})"
      using i[OF ass] ii by auto
  qed
  show ?thesis
    using s1
    apply (auto simp: line_integral_exists_def)
    apply (rule integrable_spike_finite [of "{0,1} \<union> s1", OF _ _ *])
     apply (auto simp: joinpaths_def scaleR_conv_of_real g1)
    done
qed

lemma line_integral_exists_joinD2:
  assumes "line_integral_exists f basis (g1 +++ g2)" "valid_path g2"
  shows "line_integral_exists f basis g2"
proof -
  obtain s2
    where s2: "finite s2" "\<forall>x\<in>{0..1} - s2. g2 differentiable at x"
    using assms by (auto simp: valid_path_def piecewise_C1_differentiable_on_def C1_differentiable_on_eq)
  have "(\<lambda>x. \<Sum>b\<in>basis. f ((g1 +++ g2) (x/2 + 1/2)) \<bullet> b * (vector_derivative (g1 +++ g2) (at (x/2 + 1/2) within {0..1}) \<bullet> b)) integrable_on {0..1}"
    using assms
    apply (auto simp: line_integral_exists_def)
    apply (drule integrable_on_subcbox [where a="1/2" and b=1], auto)
    apply (drule integrable_affinity [of _ "1/2::real" 1 "1/2" "1/2", simplified])
    apply (simp add: image_affinity_atLeastAtMost_diff)
    done
  then have *:"(\<lambda>x. \<Sum>b\<in>basis. ((f ((g1 +++ g2) (x/2 + 1/2)) \<bullet> b) / 2)* (vector_derivative (g1 +++ g2) (at (x/2 + 1/2) within {0..1}) \<bullet> b)) integrable_on {0..1}"
    by (auto simp: Groups_Big.sum_distrib_left dest: integrable_cmul [where c="1/2"] simp: scaleR_conv_of_real)
  have g2: "\<lbrakk>1 < z*2; z \<le> 1; z*2 - 1 \<notin> s2\<rbrakk> \<Longrightarrow>
            vector_derivative (\<lambda>x. if x*2 \<le> 1 then g1 (2*x) else g2 (2*x - 1)) (at z within {0..1}) =
            2 *\<^sub>R vector_derivative g2 (at (z*2 - 1) within {0..1})" for z       proof -
    have i:"\<lbrakk>1 < z*2; z \<le> 1; z*2 - 1 \<notin> s2\<rbrakk> \<Longrightarrow>
              vector_derivative (\<lambda>x. if x * 2 \<le> 1 then g1 (2 * x) else g2 (2 * x - 1)) (at z within {0..1})
                   = 2 *\<^sub>R vector_derivative g2 (at (z * 2 - 1))" for z
    proof-
      have g2_at_z:"\<lbrakk>1 < z*2; z \<le> 1; z*2 - 1 \<notin> s2\<rbrakk> \<Longrightarrow>
                      ((\<lambda>x. if x*2 \<le> 1 then g1 (2*x) else g2 (2*x - 1)) has_vector_derivative 2 *\<^sub>R vector_derivative g2 (at (z*2 - 1))) (at z)" for z
        apply (rule has_vector_derivative_transform_at [of "\<bar>z - 1/2\<bar>" _ "(\<lambda>x. g2 (2*x - 1))"])
          apply (simp_all add: dist_real_def abs_if split: if_split_asm)
        apply (rule vector_diff_chain_at [of "\<lambda>x. 2*x - 1" 2 _ g2, simplified o_def])
         apply (simp add: has_vector_derivative_def has_derivative_def bounded_linear_mult_left)
        using s2
        apply (auto simp: algebra_simps vector_derivative_works)
        done
      assume ass: "1 < z*2" "z \<le> 1" "z*2 - 1 \<notin> s2"
      then have z_le: "z\<le> 1" by auto
      have z_ge: "0 \<le> z" using ass by auto
      show "vector_derivative (\<lambda>x. if x * 2 \<le> 1 then g1 (2 * x) else g2 (2 * x - 1)) (at z within {0..1})
                                = 2 *\<^sub>R vector_derivative g2 (at (z * 2 - 1))"
        using Derivative.vector_derivative_at_within_ivl[OF g2_at_z[OF ass] z_ge z_le]
        by auto
    qed
    assume ass: "1 < z*2" "z \<le> 1" "z*2 - 1 \<notin> s2"
    then have "(g2 has_vector_derivative ((vector_derivative g2 (at (z*2 - 1))))) (at (z*2 - 1))"
      using s2 by (auto simp: algebra_simps vector_derivative_works)
    then have ii: "(vector_derivative g2 (at (z*2 - 1) within {0..1})) = (vector_derivative g2 (at (z*2 - 1)))"
      using Derivative.vector_derivative_at_within_ivl ass
      by force
    show "vector_derivative (\<lambda>x. if x * 2 \<le> 1 then g1 (2 * x) else g2 (2 * x - 1)) (at z within {0..1}) = 2 *\<^sub>R vector_derivative g2 (at (z * 2 - 1) within {0..1})"
      using i[OF ass] ii
      by auto
  qed
  show ?thesis
    using s2
    apply (auto simp: line_integral_exists_def)
    apply (rule integrable_spike_finite [of "{0,1} \<union> s2", OF _ _ *])
     apply (auto simp: joinpaths_def scaleR_conv_of_real g2)
    done
qed

lemma has_line_integral_on_reverse_path:
  assumes "valid_path (g::real \<Rightarrow> 'a::{real_inner, euclidean_space})"
    "((\<lambda>x. \<Sum>b\<in>basis. F (g x) \<bullet> b * (vector_derivative g (at x within {0..1}) \<bullet> b)) has_integral c){0..1}"
  shows  "((\<lambda>x. \<Sum>b\<in>basis. F ((reversepath g) x) \<bullet> b * (vector_derivative (reversepath g) (at x within {0..1}) \<bullet> b)) has_integral -c){0..1}"
proof -
  { fix s x
    assume xs: "g C1_differentiable_on ({0..1} - s)" "x \<notin> (-) 1 ` s" "0 \<le> x" "x \<le> 1"
    have "vector_derivative (\<lambda>x. g (1 - x)) (at x within {0..1}) =
                - vector_derivative g (at (1 - x) within {0..1})"
    proof -
      obtain f' where f': "(g has_vector_derivative f') (at (1 - x))"
        using xs
        by (force simp: has_vector_derivative_def C1_differentiable_on_def)
      have "(g o (\<lambda>x. 1 - x) has_vector_derivative -1 *\<^sub>R f') (at x)"
        apply (rule vector_diff_chain_within)
         apply (intro vector_diff_chain_within derivative_eq_intros | simp)+
        apply (rule has_vector_derivative_at_within [OF f'])
        done
      then have mf': "((\<lambda>x. g (1 - x)) has_vector_derivative -f') (at x)"
        by (simp add: o_def)
      show ?thesis
        using xs
        by (auto simp: vector_derivative_at_within_ivl [OF mf'] vector_derivative_at_within_ivl [OF f'])
    qed
  } note * = this
  have 01: "{0..1::real} = cbox 0 1"
    by simp
  show ?thesis using assms
    apply (auto simp add: valid_path_def)
    apply (drule has_integral_affinity01 [where m= "-1" and c=1])
     apply (auto simp: reversepath_def piecewise_C1_differentiable_on_def)
    apply (drule has_integral_neg)
    subgoal for s apply (rule_tac S = "(\<lambda>x. 1 - x) ` s" in has_integral_spike_finite)
      apply (auto simp: * Groups_Big.sum_negf)
      done
    done
qed

lemma line_integral_on_reverse_path:
  assumes "valid_path \<gamma>" "line_integral_exists F basis \<gamma>"
  shows "line_integral F basis \<gamma> = - (line_integral F basis (reversepath \<gamma>))"
        "line_integral_exists F basis (reversepath \<gamma>)"
proof -
  obtain i where
    0: "((\<lambda>x. \<Sum>b\<in>basis. F (\<gamma> x) \<bullet> b * (vector_derivative \<gamma> (at x within {0..1}) \<bullet> b)) has_integral i){0..1}"
    using assms unfolding integrable_on_def line_integral_exists_def by auto
  then have 1: "((\<lambda>x. \<Sum>b\<in>basis. F ((reversepath \<gamma>) x) \<bullet> b * (vector_derivative (reversepath \<gamma>) (at x within {0..1}) \<bullet> b)) has_integral -i){0..1}"
    using has_line_integral_on_reverse_path assms
    by auto
  then have rev_line_integral:"line_integral F basis (reversepath \<gamma>) = -i"
    using line_integral_def Henstock_Kurzweil_Integration.integral_unique
    by (metis (no_types))
  have line_integral: "line_integral F basis \<gamma> = i"
    using line_integral_def 0 Henstock_Kurzweil_Integration.integral_unique
    by blast
  show "line_integral F basis \<gamma> = - (line_integral F basis (reversepath \<gamma>))"
    using line_integral rev_line_integral
    by auto
  show "line_integral_exists F basis (reversepath \<gamma>)"
    using 1 line_integral_exists_def
    by auto
qed

lemma line_integral_exists_on_degenerate_path:
  assumes "finite basis"
  shows "line_integral_exists F basis (\<lambda>x. c)"
proof-
  have every_component_integrable:
    "\<forall>b\<in>basis. (\<lambda>x. F ((\<lambda>x. c) x) \<bullet> b * (vector_derivative (\<lambda>x. c) (at x within {0..1}) \<bullet> b)) integrable_on {0..1}"
  proof
    fix b
    assume b_in_basis: "b \<in> basis"
    have cont_field_zero_one: "continuous_on {0..1} (\<lambda>x. F ((\<lambda>x. c) x) \<bullet> b)"
      using continuous_on_const by fastforce
    have cont_path_zero_one:
      "continuous_on {0..1} (\<lambda>x. (vector_derivative (\<lambda>x. c) (at x within {0..1})) \<bullet> b)"
    proof -
      have "((vector_derivative (\<lambda>x. c) (at x within {0..1})) \<bullet> b) = 0" if "x \<in> {0..1}" for x
      proof -
        have "vector_derivative (\<lambda>x. c) (at x within {0..1}) = 0"
          using that gamma_deriv_at_within[of "0" "1"] differentiable_const vector_derivative_const_at
          by fastforce
        then show "vector_derivative (\<lambda>x. c) (at x within {0..1}) \<bullet> b = 0"
          by auto
      qed
      then show "continuous_on {0..1} (\<lambda>x. (vector_derivative (\<lambda>x. c) (at x within {0..1})) \<bullet> b)"
        using continuous_on_const[of "{0..1}" "0"] continuous_on_eq[of "{0..1}" "\<lambda>x. 0" "(\<lambda>x. (vector_derivative (\<lambda>x. c) (at x within {0..1})) \<bullet> b)"]
        by auto
    qed
    show "(\<lambda>x. F (c) \<bullet> b * (vector_derivative (\<lambda>x. c) (at x within {0..1}) \<bullet> b)) integrable_on {0..1}"
      using cont_field_zero_one cont_path_zero_one continuous_on_mult integrable_continuous_real
      by blast
  qed
  have integrable_sum': "\<And>t f s. finite t \<Longrightarrow>
                \<forall>a\<in>t. f a integrable_on s \<Longrightarrow> (\<lambda>x. \<Sum>a\<in>t. f a x) integrable_on s"
    using integrable_sum by metis
  have field_integrable_on_basis:
    "(\<lambda>x. \<Sum>b\<in>basis. F (c) \<bullet> b * (vector_derivative (\<lambda>x. c) (at x within {0..1}) \<bullet> b)) integrable_on {0..1}"
    using integrable_sum'[OF assms(1) every_component_integrable]
    by auto
  then show ?thesis
    using line_integral_exists_def by auto
qed

lemma degenerate_path_is_valid_path: "valid_path (\<lambda>x. c)"
  by (auto simp add: valid_path_def piecewise_C1_differentiable_on_def continuous_on_const)

lemma line_integral_degenerate_path:
  assumes "finite basis"
  shows "line_integral F basis (\<lambda>x. c) = 0"
proof (simp add: line_integral_def)
  have "((vector_derivative (\<lambda>x. c) (at x within {0..1})) \<bullet> b) = 0" if "x \<in> {0..1}" for x b
  proof -
    have "vector_derivative (\<lambda>x. c) (at x within {0..1}) = 0"
      using that gamma_deriv_at_within[of "0" "1"] differentiable_const vector_derivative_const_at
      by fastforce
    then show "vector_derivative (\<lambda>x. c) (at x within {0..1}) \<bullet> b = 0"
      by auto
  qed
  then have 0: "\<And>x. x \<in> {0..1} \<Longrightarrow> (\<lambda>x. \<Sum>b\<in>basis. F c \<bullet> b * (vector_derivative (\<lambda>x. c) (at x within {0..1}) \<bullet> b)) x = (\<lambda>x. 0) x"
    by auto
  then show "integral {0..1} (\<lambda>x. \<Sum>b\<in>basis. F c \<bullet> b * (vector_derivative (\<lambda>x. c) (at x within {0..1}) \<bullet> b)) = 0"
    using integral_cong[of "{0..1}", OF 0] integral_0 by auto
qed

definition point_path where
    "point_path \<gamma> \<equiv> \<exists>c. \<gamma> = (\<lambda>x. c)"

lemma line_integral_point_path:
  assumes "point_path \<gamma>"
  assumes "finite basis"
  shows "line_integral F basis \<gamma> = 0"
  using assms(1) point_path_def line_integral_degenerate_path[OF assms(2)]
  by force

lemma line_integral_exists_point_path:
  assumes "finite basis" "point_path \<gamma>"
  shows "line_integral_exists F basis \<gamma>"
  using assms
  apply(simp add: point_path_def)
  using line_integral_exists_on_degenerate_path by auto

lemma line_integral_exists_subpath:
  assumes f: "line_integral_exists f basis g" and g: "valid_path g"
    and uv: "u \<in> {0..1}" "v \<in> {0..1}" "u \<le> v"
  shows "(line_integral_exists f basis (subpath u v g))"
proof (cases "v=u")
  case tr: True
  have zero: "(\<Sum>b\<in>basis. f (g u) \<bullet> b * (vector_derivative (\<lambda>x. g u) (at x within {0..1}) \<bullet> b)) = 0" if "x \<in> {0..1}" for x::real
  proof -
    have "(vector_derivative (\<lambda>x. g u) (at x within {0..1})) = 0"
      using Deriv.has_vector_derivative_const that Derivative.vector_derivative_at_within_ivl
      by fastforce
    then show "(\<Sum>b\<in>basis. f (g u) \<bullet> b * (vector_derivative (\<lambda>x. g u) (at x within {0..1}) \<bullet> b)) = 0"
      by auto
  qed
  then have "((\<lambda>x. \<Sum>b\<in>basis. f (g u)  \<bullet> b * (vector_derivative (\<lambda>x. g u) (at x within {0..1})  \<bullet> b)) has_integral 0) {0..1}"
    by (meson has_integral_is_0)
  then show ?thesis
    using f tr by (auto simp add: line_integral_def line_integral_exists_def subpath_def)
next
  case False
  obtain s where s: "\<And>x. x \<in> {0..1} - s \<Longrightarrow> g differentiable at x" and fs: "finite s"
    using g unfolding piecewise_C1_differentiable_on_def C1_differentiable_on_eq valid_path_def by blast
  have *: "((\<lambda>x. \<Sum>b\<in>basis. f (g ((v - u) * x + u))  \<bullet> b * (vector_derivative g (at ((v - u) * x + u) within {0..1})  \<bullet> b))
            has_integral (1 / (v - u)) * integral {u..v} (\<lambda>x. \<Sum>b\<in>basis. f (g (x))  \<bullet> b * (vector_derivative g (at x within {0..1})  \<bullet> b)))
           {0..1}"
    using f uv
    apply (simp add: line_integral_exists_def subpath_def)
    apply (drule integrable_on_subcbox [where a=u and b=v, simplified])
     apply (simp_all add: has_integral_integral)
    apply (drule has_integral_affinity [where m="v-u" and c=u, simplified])
     apply (simp_all add: False image_affinity_atLeastAtMost_div_diff scaleR_conv_of_real)
    apply (simp add: divide_simps False)
    done
  have vd:"\<And>x. x \<in> {0..1} \<Longrightarrow>
           x \<notin> (\<lambda>t. (v-u) *\<^sub>R t + u) -` s \<Longrightarrow>
           vector_derivative (\<lambda>x. g ((v-u) * x + u)) (at x within {0..1}) = (v-u) *\<^sub>R vector_derivative g (at ((v-u) * x + u) within {0..1})"
    using test2[OF s fs uv]
    by auto
  have arg:"\<And>x. (\<Sum>n\<in>basis. (v - u) * (f (g ((v - u) * x + u)) \<bullet> n) * (vector_derivative g (at ((v - u) * x + u) within {0..1}) \<bullet> n))
              =    (\<Sum>b\<in>basis. f (g ((v - u) * x + u)) \<bullet> b * (v - u) * (vector_derivative g (at ((v - u) * x + u) within {0..1}) \<bullet> b))"
    by (simp add: mult.commute)
  have"((\<lambda>x. \<Sum>b\<in>basis. f (g ((v - u) * x + u))  \<bullet> b * (vector_derivative (\<lambda>x. g ((v - u) * x + u)) (at x within {0..1})  \<bullet> b)) has_integral
          (integral {u..v} (\<lambda>x. \<Sum>b\<in>basis. f (g (x))  \<bullet> b * (vector_derivative g (at x within {0..1}) \<bullet> b)))) {0..1}"
    apply (cut_tac Henstock_Kurzweil_Integration.has_integral_mult_right [OF *, where c = "v-u"])
    using fs assms
    apply (simp add: False subpath_def line_integral_exists_def)
    apply (rule_tac S = "(\<lambda>t. ((v-u) *\<^sub>R t + u)) -` s" in has_integral_spike_finite)
      apply (auto simp: inj_on_def False vd finite_vimageI scaleR_conv_of_real Groups_Big.sum_distrib_left
        mult.assoc[symmetric] arg)
    done
  then show "(line_integral_exists f basis (subpath u v g))"
    by(auto simp add: line_integral_exists_def subpath_def integrable_on_def)
qed

(*This should have everything that has to do with onecubes*)

(* Chain line integrals should be generalsied as follows

   datatype 'a path = path "real \<Rightarrow> 'a"*)

   type_synonym path = "(real \<Rightarrow> (real * real))"   
   type_synonym one_cube = "(real \<Rightarrow> (real * real))"
   type_synonym one_chain = "(int * path) set"

definition one_chain_line_integral :: "(real * real \<Rightarrow> real * real) => (real * real) set \<Rightarrow> one_chain \<Rightarrow> real" where
    "one_chain_line_integral F b C \<equiv> (\<Sum>(k,\<gamma>)\<in>C. k * (line_integral F b \<gamma>))"

abbreviation "one_chain_line_integral_fancy C F b \<equiv> one_chain_line_integral F b C"

notation one_chain_line_integral_fancy ("\<^sup>H\<ointegral>\<^bsub>_\<^esub> _ \<downharpoonright>\<^bsub>_\<^esub>" 400)

notation one_chain_line_integral_fancy ("\<^sup>H\<integral>\<^bsub>_\<^esub> _ \<downharpoonright>\<^bsub>_\<^esub>" 400)

abbreviation "one_chain_line_integral_fancy2 C F \<equiv> one_chain_line_integral F Basis C"

notation one_chain_line_integral_fancy2 ("\<^sup>H\<ointegral>\<^bsub>_\<^esub> _" 200)

notation one_chain_line_integral_fancy2 ("\<^sup>H\<integral>\<^bsub>_\<^esub> _" 200)

definition one_chain_line_integral_exists :: "((real * real) \<Rightarrow> (real * real)) => ((real*real) set) \<Rightarrow> one_chain \<Rightarrow> bool" where
    "one_chain_line_integral_exists F b C \<equiv> (\<forall>(k,\<gamma>)\<in>C. (line_integral_exists F b \<gamma>))"

lemma one_chain_line_integral_empty_basis[simp]: "\<^sup>H\<integral>\<^bsub>C\<^esub> F \<downharpoonright>\<^bsub>{}\<^esub> = 0" "one_chain_line_integral_exists F {} C"
  unfolding one_chain_line_integral_def one_chain_line_integral_exists_def line_integral_exists_def by auto

lemma one_chain_line_integral_empty_chain[simp]: "\<^sup>H\<integral>\<^bsub>{}\<^esub> F \<downharpoonright>\<^bsub>B\<^esub> = 0"  "one_chain_line_integral_exists F B {}"
  unfolding one_chain_line_integral_def one_chain_line_integral_exists_def line_integral_exists_def by auto

lemma one_chain_line_integral_mult_left: "k * \<^sup>H\<integral>\<^bsub>C\<^esub> F \<downharpoonright>\<^bsub>B\<^esub> = \<^sup>H\<integral>\<^bsub>C\<^esub> %x. k *\<^sub>R (F x) \<downharpoonright>\<^bsub>B\<^esub>" "one_chain_line_integral_exists F B C \<Longrightarrow> one_chain_line_integral_exists (%x. c *\<^sub>R F x) B C"
  unfolding one_chain_line_integral_def apply(simp add: sum_distrib_left, rule Finite_Cartesian_Product.sum_cong_aux, auto simp add: mult.commute)
  unfolding one_chain_line_integral_exists_def using line_integral_mult_left(2) by (auto simp add: line_integral_mult_left)

lemma one_chain_line_integral_cong: "(\<forall>(k,\<gamma>)\<in>C. \<forall>x\<in>path_image \<gamma>. F x = G x) \<Longrightarrow>  \<^sup>H\<integral>\<^bsub>C\<^esub> F \<downharpoonright>\<^bsub>B\<^esub> = \<^sup>H\<integral>\<^bsub>C\<^esub> G \<downharpoonright>\<^bsub>B\<^esub>"
  unfolding one_chain_line_integral_def
 by(auto intro!: Finite_Cartesian_Product.sum_cong_aux line_integral_cong)

lemma one_chain_line_integral_subset: "C2 \<subseteq> C1 \<Longrightarrow> one_chain_line_integral_exists F B C1 \<Longrightarrow> one_chain_line_integral_exists F B C2"
  unfolding one_chain_line_integral_exists_def
  by auto

definition boundary_chain where
    "boundary_chain s \<equiv> (\<forall>(k, \<gamma>) \<in> s. k = 1 \<or> k = -1)"

lemma subset_boundary_is_boundary: "t\<subseteq>s \<Longrightarrow> boundary_chain s \<Longrightarrow> boundary_chain t" unfolding boundary_chain_def by auto

fun coeff_cube_to_path::"(int * one_cube) \<Rightarrow> path" 
  where "coeff_cube_to_path (k, \<gamma>) = (if k = 1 then \<gamma> else (reversepath \<gamma>))"

lemma valid_coeff_cube_to_path: assumes "valid_path \<gamma>"
  shows " valid_path (coeff_cube_to_path (1, \<gamma>))" " valid_path (coeff_cube_to_path (-1, \<gamma>))"
  using assms
  by auto

inductive chain_subdiv_path where
   (*empty: "chain_subdiv_path (\<lambda>x. c) {}" |*)
   singleton: "chain_subdiv_path (coeff_cube_to_path \<gamma>) {\<gamma>}" |
   insert: "\<gamma> \<notin> s \<Longrightarrow> pathfinish (coeff_cube_to_path \<gamma>) = pathstart \<gamma>0 \<Longrightarrow> chain_subdiv_path \<gamma>0 s \<Longrightarrow>
       chain_subdiv_path (coeff_cube_to_path \<gamma> +++ \<gamma>0) (insert \<gamma> s)"

lemma chain_subdiv_path_cong_intros:
   shows  singleton: "\<And>\<gamma>'. \<gamma>' = \<gamma> \<Longrightarrow> chain_subdiv_path (coeff_cube_to_path \<gamma>') {\<gamma>}" and
          insert_1: "\<And>\<gamma>'. \<gamma>' = \<gamma> \<Longrightarrow> \<gamma> \<notin> s  \<Longrightarrow> pathfinish (coeff_cube_to_path \<gamma>) = pathstart \<gamma>0 \<Longrightarrow> chain_subdiv_path \<gamma>0 s \<Longrightarrow>
            chain_subdiv_path (coeff_cube_to_path \<gamma>' +++ \<gamma>0) (insert \<gamma> s)" and
          insert_2: "\<And>\<gamma>'. (\<gamma>' = (coeff_cube_to_path \<gamma> +++ \<gamma>0)) \<Longrightarrow> \<gamma> \<notin> s  \<Longrightarrow> pathfinish (coeff_cube_to_path \<gamma>) = pathstart \<gamma>0 \<Longrightarrow> chain_subdiv_path \<gamma>0 s \<Longrightarrow>
            chain_subdiv_path \<gamma>' (insert \<gamma> s)"
  using chain_subdiv_path.intros by auto

lemma valid_path_equiv_valid_chain_list':
  assumes path_eq_chain: "chain_subdiv_path \<gamma> one_chain" 
    and "boundary_chain one_chain" "\<forall>(k, \<gamma>) \<in> one_chain. valid_path \<gamma>"
  shows "valid_path \<gamma>"
  using assms
proof(induct rule: chain_subdiv_path.induct)
  case (singleton \<gamma>)
  then show ?case using valid_coeff_cube_to_path unfolding boundary_chain_def by auto
next
  case (insert \<gamma> s \<gamma>0)
  then show ?case by (auto simp add: boundary_chain_def valid_path_join)+
qed

lemma chain_subdiv_path_finite:
  assumes "chain_subdiv_path \<gamma> one_chain"
  shows "finite one_chain"
  using assms
  by(induct rule: chain_subdiv_path.induct; auto)

lemma coeff_cube_to_path_line_integral: 
  assumes "valid_path \<gamma>" "line_integral_exists F basis \<gamma>" "k = 1 \<or> k = -1"
  shows  "\<integral>\<^bsub>coeff_cube_to_path (k,\<gamma>)\<^esub> F \<downharpoonright>\<^bsub>basis\<^esub> = k * \<integral>\<^bsub>\<gamma>\<^esub> F \<downharpoonright>\<^bsub>basis\<^esub>"
  "line_integral_exists F basis (coeff_cube_to_path (k,\<gamma>))"
  using line_integral_on_reverse_path[OF assms(1,2)] assms 
  by auto

lemma coeff_cube_to_path_line_integral_2:
  assumes "valid_path \<gamma>" "line_integral_exists F basis (coeff_cube_to_path (k,\<gamma>))" "k = 1 \<or> k = -1"
  shows  "\<integral>\<^bsub>coeff_cube_to_path (k,\<gamma>)\<^esub> F \<downharpoonright>\<^bsub>basis\<^esub> = k * \<integral>\<^bsub>\<gamma>\<^esub> F \<downharpoonright>\<^bsub>basis\<^esub>"
  "line_integral_exists F basis \<gamma>"
  using assms(2,3) apply auto
  subgoal apply(rewrite in "_ = \<hole>" reversepath_reversepath[of \<gamma>, symmetric])
    apply (rule line_integral_on_reverse_path)
    using assms(1) valid_path_reversepath
    by auto
  subgoal apply(subst reversepath_reversepath[of \<gamma>, symmetric])
    apply(rule line_integral_on_reverse_path)
    using assms(1) valid_path_reversepath
    by auto
  done

lemma line_integral_on_path_eq_line_integral_on_equiv_chain_1':
  assumes path_eq_chain: "chain_subdiv_path \<gamma> one_chain" and
    boundary_chain: "boundary_chain one_chain" and
    line_integral_exists: "\<forall>(k::int, \<gamma>) \<in> one_chain. line_integral_exists F basis \<gamma>" and
    valid_path: "\<forall>(k::int, \<gamma>) \<in> one_chain. valid_path \<gamma>" and
    finite_basis: "finite basis"
  shows "one_chain_line_integral F basis one_chain = line_integral F basis \<gamma>"
    "line_integral_exists F basis \<gamma>"
    "valid_path \<gamma>"
  using assms
proof(induct rule: chain_subdiv_path.induct)
  fix twoC
  assume ass: "boundary_chain {twoC}" "\<forall>(k::int, y)\<in>{twoC}. line_integral_exists F basis y" "\<forall>(k, y)\<in>{twoC}. valid_path y"
  then have *: "valid_path (snd twoC)" "line_integral_exists F basis (snd twoC)" "fst twoC = 1 \<or> fst twoC = - 1"
    unfolding boundary_chain_def
    by auto
  then show " \<^sup>H\<integral>\<^bsub>{twoC}\<^esub> F \<downharpoonright>\<^bsub>basis\<^esub> = \<integral>\<^bsub>coeff_cube_to_path twoC\<^esub> F \<downharpoonright>\<^bsub>basis\<^esub>" "line_integral_exists F basis (coeff_cube_to_path twoC)"
    unfolding one_chain_line_integral_def case_prod_beta'
    using coeff_cube_to_path_line_integral[OF *(1,2,3)]
    by auto
  show "valid_path (coeff_cube_to_path twoC)"
    using valid_coeff_cube_to_path * ass(3)
    by auto
next
  fix \<gamma> \<gamma>0 s
  assume ass: "\<gamma> \<notin> s" "chain_subdiv_path \<gamma>0 s"
              "boundary_chain (insert \<gamma> s)"
              "\<forall>(k, y)\<in>insert \<gamma> s. line_integral_exists F basis y"
              "\<forall>(k, y)\<in>insert \<gamma> s. valid_path y"
              "finite basis"
              "pathfinish (coeff_cube_to_path \<gamma>) = pathstart \<gamma>0"
              "(boundary_chain s \<Longrightarrow> \<forall>(k, y)\<in>s. line_integral_exists F basis y \<Longrightarrow> \<forall>(k, y)\<in>s. valid_path y \<Longrightarrow> finite basis \<Longrightarrow> \<^sup>H\<integral>\<^bsub>s\<^esub> F \<downharpoonright>\<^bsub>basis\<^esub> = \<integral>\<^bsub>\<gamma>0\<^esub> F \<downharpoonright>\<^bsub>basis\<^esub>)"
              "(boundary_chain s \<Longrightarrow> \<forall>(k, y)\<in>s. line_integral_exists F basis y \<Longrightarrow> \<forall>(k, y)\<in>s. valid_path y \<Longrightarrow> finite basis \<Longrightarrow> valid_path \<gamma>0)"
              "(boundary_chain s \<Longrightarrow> \<forall>(k, y)\<in>s. line_integral_exists F basis y \<Longrightarrow> \<forall>(k, y)\<in>s. valid_path y \<Longrightarrow> finite basis \<Longrightarrow> line_integral_exists F basis \<gamma>0)"
  have *: "boundary_chain s" "\<forall>(k, y)\<in>s. line_integral_exists F basis y" "\<forall>(k, y)\<in>s. valid_path y" "valid_path \<gamma>0" "valid_path (snd \<gamma>)" "line_integral_exists F basis (snd \<gamma>)" "fst \<gamma> = 1 \<or> fst \<gamma> = -1" 
    using ass unfolding boundary_chain_def by auto
  then have a: "valid_path (coeff_cube_to_path \<gamma>)" using valid_coeff_cube_to_path 
        by (metis (no_types, lifting) case_prod_beta' prod.collapse)
  moreover have b: "line_integral_exists F basis (coeff_cube_to_path \<gamma>)"
        using coeff_cube_to_path_line_integral *
        by (metis (no_types, lifting) case_prod_beta' prod.collapse)
  moreover have c: "line_integral_exists F basis \<gamma>0"
        using ass(10)[OF *(1-3) finite_basis] by auto
  moreover have d: "valid_path \<gamma>0" using * by auto
  ultimately show "\<^sup>H\<integral>\<^bsub>insert \<gamma> s\<^esub> F \<downharpoonright>\<^bsub>basis\<^esub> = \<integral>\<^bsub>coeff_cube_to_path \<gamma> +++ \<gamma>0\<^esub> F \<downharpoonright>\<^bsub>basis\<^esub>"
    unfolding one_chain_line_integral_def
    apply(subst comm_monoid_add_class.sum.insert )
    subgoal by (auto intro: ass chain_subdiv_path_finite)
    subgoal using ass(1) .
    subgoal apply(subst line_integral_distrib(1))
    proof-
      show "(case \<gamma> of (k, \<gamma>) \<Rightarrow> real_of_int k * \<integral>\<^bsub>\<gamma>\<^esub> F \<downharpoonright>\<^bsub>basis\<^esub>) + (\<Sum>(k, \<gamma>)\<in>s. real_of_int k * \<integral>\<^bsub>\<gamma>\<^esub> F \<downharpoonright>\<^bsub>basis\<^esub>) = \<integral>\<^bsub>coeff_cube_to_path \<gamma>\<^esub> F \<downharpoonright>\<^bsub>basis\<^esub> + \<integral>\<^bsub>\<gamma>0\<^esub> F \<downharpoonright>\<^bsub>basis\<^esub>"
        using coeff_cube_to_path_line_integral(1)[OF *(5-7), symmetric]
        unfolding  ass(8)[OF *(1-3) finite_basis, symmetric] one_chain_line_integral_def
        by (metis (no_types, lifting) case_prod_beta' prod.collapse)
    qed
    done
   show "line_integral_exists F basis (coeff_cube_to_path \<gamma> +++ \<gamma>0)"
    apply(rule line_integral_distrib(2))
     using a b c d by auto
  show "valid_path (coeff_cube_to_path \<gamma> +++ \<gamma>0)"
    apply(rule valid_path_join)
     using a b c d ass(7) by auto
 qed

lemma chain_subdiv_path_valid_path:
      assumes "chain_subdiv_path \<gamma>0 s"
              "\<forall>(k, y)\<in>s. valid_path y"
      shows "valid_path \<gamma>0"
  using assms
proof(induct rule: chain_subdiv_path.induct)
  case (singleton \<gamma>)
  then show ?case by auto
next
  case (insert \<gamma> s \<gamma>0)
  then show ?case
    apply(intro valid_path_join)
    by auto
qed

lemma line_integral_on_path_eq_line_integral_on_equiv_chain_2':
  assumes path_eq_chain: "chain_subdiv_path \<gamma> one_chain" and
    boundary_chain: "boundary_chain one_chain" and
    line_integral_exists: "line_integral_exists F basis \<gamma>" and
    valid_path: "\<forall>(k, \<gamma>) \<in> one_chain. valid_path \<gamma>" and
    finite_basis: "finite basis"
  shows "one_chain_line_integral F basis one_chain = line_integral F basis \<gamma>"
    "\<forall>(k, \<gamma>) \<in> one_chain. line_integral_exists F basis \<gamma>"
  using assms
proof(induct rule: chain_subdiv_path.induct)
  fix twoC
  assume ass: "boundary_chain {twoC}" "line_integral_exists F basis (coeff_cube_to_path twoC)" "\<forall>(k, y)\<in>{twoC}. valid_path y"
  then have *: "valid_path (snd twoC)" "fst twoC = 1 \<or> fst twoC = - 1"
    unfolding boundary_chain_def
    by auto
  then show " \<^sup>H\<integral>\<^bsub>{twoC}\<^esub> F \<downharpoonright>\<^bsub>basis\<^esub> = \<integral>\<^bsub>coeff_cube_to_path twoC\<^esub> F \<downharpoonright>\<^bsub>basis\<^esub>" "\<forall>(k, y)\<in>{twoC}. line_integral_exists F basis y"
    unfolding one_chain_line_integral_def case_prod_beta'
    using coeff_cube_to_path_line_integral_2[OF *(1) _ *(2)] ass(2)
    by auto
next
  fix \<gamma> \<gamma>0 s
  assume ass: "\<gamma> \<notin> s" "chain_subdiv_path \<gamma>0 s"
              "boundary_chain (insert \<gamma> s)"
              "line_integral_exists F basis (coeff_cube_to_path \<gamma> +++ \<gamma>0)"
              "\<forall>(k, y)\<in>insert \<gamma> s. valid_path y"
              "finite basis"
              "pathfinish (coeff_cube_to_path \<gamma>) = pathstart \<gamma>0"
              "(boundary_chain s \<Longrightarrow> line_integral_exists F basis \<gamma>0 \<Longrightarrow> \<forall>(k, y)\<in>s. valid_path y \<Longrightarrow> finite basis \<Longrightarrow> \<^sup>H\<integral>\<^bsub>s\<^esub> F \<downharpoonright>\<^bsub>basis\<^esub> = \<integral>\<^bsub>\<gamma>0\<^esub> F \<downharpoonright>\<^bsub>basis\<^esub>)"
              "(boundary_chain s \<Longrightarrow> line_integral_exists F basis \<gamma>0 \<Longrightarrow> \<forall>(k, y)\<in>s. valid_path y \<Longrightarrow> finite basis \<Longrightarrow> \<forall>(k, y)\<in>s. line_integral_exists F basis y)"
  have *: "boundary_chain s" "\<forall>(k, y)\<in>s. valid_path y" "valid_path (snd \<gamma>)"  "fst \<gamma> = 1 \<or> fst \<gamma> = -1" 
    using ass unfolding boundary_chain_def by auto
  have a: "line_integral_exists F basis \<gamma>0"
    apply(rule line_integral_exists_joinD2)
    using ass chain_subdiv_path_valid_path by auto
  moreover have b: "line_integral_exists F basis (coeff_cube_to_path \<gamma>)"
    apply(rule line_integral_exists_joinD1)
    using ass by auto
  moreover have c: "line_integral_exists F basis (snd \<gamma>)"
    apply(rule coeff_cube_to_path_line_integral_2)
  proof-
    show "line_integral_exists F basis (coeff_cube_to_path (fst \<gamma>, snd \<gamma>))"
      apply(rule line_integral_exists_joinD1)
      using ass by auto
  qed (auto simp add: b *)
  ultimately show "\<^sup>H\<integral>\<^bsub>insert \<gamma> s\<^esub> F \<downharpoonright>\<^bsub>basis\<^esub> = \<integral>\<^bsub>coeff_cube_to_path \<gamma> +++ \<gamma>0\<^esub> F \<downharpoonright>\<^bsub>basis\<^esub>"
    unfolding one_chain_line_integral_def
    apply(subst comm_monoid_add_class.sum.insert )
    subgoal by (auto intro: ass chain_subdiv_path_finite)
    subgoal using ass(1) .
    subgoal apply(subst line_integral_distrib(1))
    proof-
      show "(case \<gamma> of (k, \<gamma>) \<Rightarrow> real_of_int k * \<integral>\<^bsub>\<gamma>\<^esub> F \<downharpoonright>\<^bsub>basis\<^esub>) + (\<Sum>(k, \<gamma>)\<in>s. real_of_int k * \<integral>\<^bsub>\<gamma>\<^esub> F \<downharpoonright>\<^bsub>basis\<^esub>) = \<integral>\<^bsub>coeff_cube_to_path \<gamma>\<^esub> F \<downharpoonright>\<^bsub>basis\<^esub> + \<integral>\<^bsub>\<gamma>0\<^esub> F \<downharpoonright>\<^bsub>basis\<^esub>"
        using coeff_cube_to_path_line_integral(1)[OF *(3) c *(4), symmetric]
          ass(8)[OF *(1) a *(2) finite_basis, symmetric] one_chain_line_integral_def
        by (metis (no_types, lifting) case_prod_beta' prod.collapse)
      show "valid_path (coeff_cube_to_path \<gamma>)"
        using *(3,4) valid_coeff_cube_to_path[OF *(3)]
        by (metis prod.collapse)
      show "valid_path \<gamma>0"
        using ass chain_subdiv_path_valid_path by auto
    qed
    done
  show " \<forall>(k, y)\<in>insert \<gamma> s. line_integral_exists F basis y"
    using ass(9)[OF *(1) a *(2) finite_basis] c
    by auto
 qed

definition chain_subdiv_chain where
  "chain_subdiv_chain one_chain1 subdiv
        \<equiv> \<exists>f. (\<Union>(f ` one_chain1)) = subdiv \<and>
              (\<forall>c\<in>one_chain1. chain_subdiv_path (coeff_cube_to_path c) (f c)) \<and>
              pairwise (\<lambda> p p'. f p \<inter> f p' = {}) one_chain1"

lemma chain_subdiv_chain_character:
  shows "chain_subdiv_chain one_chain1 subdiv \<longleftrightarrow>
        (\<exists>f. \<Union>(f ` one_chain1) = subdiv \<and>
             (\<forall>twoC\<in>one_chain1. chain_subdiv_path (coeff_cube_to_path twoC) (f twoC)) \<and>
             (\<forall>p\<in>one_chain1.
                 \<forall>p'\<in>one_chain1. p \<noteq> p' \<longrightarrow> f p \<inter> f p' = {}) \<and>
             (\<forall>x\<in>one_chain1. finite (f x)))"  
  unfolding chain_subdiv_chain_def
  apply (safe; intro exI conjI iffI)
  by (fastforce simp add: pairwise_def chain_subdiv_path_finite)+

lemma valid_subdiv_imp_valid_one_chain':
  assumes chain1_eq_chain2: "chain_subdiv_chain one_chain1 subdiv" and
    boundary_chain1: "boundary_chain one_chain1" and
    boundary_chain2: "boundary_chain subdiv" and
    valid_path: "\<forall>(k, \<gamma>) \<in> subdiv. valid_path \<gamma>"
  shows "\<forall>(k, \<gamma>) \<in> one_chain1. valid_path \<gamma>"
proof -
  obtain f where f_props:
    "((\<Union>(f ` one_chain1)) = subdiv)"
    "(\<forall>twoC\<in>one_chain1. chain_subdiv_path (coeff_cube_to_path twoC) (f twoC))"
    "(\<forall>p\<in>one_chain1. \<forall>p'\<in>one_chain1. p \<noteq> p' \<longrightarrow> f p \<inter> f p' = {})"
    using chain1_eq_chain2 chain_subdiv_chain_character by auto
  have "\<And> k \<gamma>. (k,\<gamma>) \<in> one_chain1\<Longrightarrow> valid_path \<gamma>"
  proof-
    fix k \<gamma>
    assume ass: "(k,\<gamma>) \<in> one_chain1"
    show "valid_path \<gamma>"
    proof (cases "k = 1")
      assume k_eq_1: "k = 1"
      then have i:"chain_subdiv_path \<gamma> (f(k,\<gamma>))"
        using f_props(2) ass by auto
      have ii:"boundary_chain (f(k,\<gamma>))"
        using f_props(1) ass assms
        apply (simp add: boundary_chain_def)
        by blast
      have "iv": " \<forall>(k, \<gamma>)\<in>f (k, \<gamma>). valid_path \<gamma>"
        using f_props(1) ass assms
        by blast
      show ?thesis
        using valid_path_equiv_valid_chain_list'[OF i ii iv]
        by auto
    next
      assume "k \<noteq> 1"
      then have k_eq_neg1: "k = -1"
        using ass boundary_chain1
        by (auto simp add: boundary_chain_def)
      then have i:"chain_subdiv_path (reversepath \<gamma>) (f(k,\<gamma>))"
        using f_props(2) ass using \<open>k \<noteq> 1\<close> by auto
      have ii:"boundary_chain (f(k,\<gamma>))"
        using f_props(1) ass assms by (auto simp add: boundary_chain_def)
      have "iv": " \<forall>(k, \<gamma>)\<in>f (k, \<gamma>). valid_path \<gamma>"
        using f_props(1) ass assms
        by blast
      have "valid_path (reversepath \<gamma>)"
        using valid_path_equiv_valid_chain_list'[OF i ii iv]
        by auto
      then show ?thesis
        using  reversepath_reversepath valid_path_imp_reverse
        by force
    qed
  qed
  then show valid_path1: "\<forall>(k, \<gamma>) \<in> one_chain1. valid_path \<gamma>"
    by auto
qed

lemma one_chain_line_integral_eq_line_integral_on_sudivision_1':
  assumes chain1_eq_chain2: "chain_subdiv_chain one_chain1 subdiv" and
    boundary_chain1: "boundary_chain one_chain1" and
    boundary_chain2: "boundary_chain subdiv" and
    line_integral_exists_on_chain2: "\<forall>(k, \<gamma>) \<in> subdiv. line_integral_exists F basis \<gamma>" and
    valid_path: "\<forall>(k, \<gamma>) \<in> subdiv. valid_path \<gamma>" and
    finite_chain1: "finite one_chain1" and
    finite_basis: "finite basis"
  shows "one_chain_line_integral F basis one_chain1 = one_chain_line_integral F basis subdiv"
    "\<forall>(k, \<gamma>) \<in> one_chain1. line_integral_exists F basis \<gamma>"
proof -
  obtain f where f_props:
    "((\<Union>(f ` one_chain1)) = subdiv)"
    "(\<forall>twoC\<in>one_chain1. chain_subdiv_path (coeff_cube_to_path twoC) (f twoC))"
    "(\<forall>p\<in>one_chain1. \<forall>p'\<in>one_chain1. p \<noteq> p' \<longrightarrow> f p \<inter> f p' = {})"
    "(\<forall>x \<in> one_chain1. finite (f x))"
    using chain1_eq_chain2 chain_subdiv_chain_character by (auto simp add: pairwise_def)
  then have 0: "one_chain_line_integral F basis subdiv = one_chain_line_integral F basis (\<Union>(f ` one_chain1))"
    by auto
  have finite_chain2: "finite subdiv"
    using finite_chain1 f_props(1) f_props(4)
    apply (simp add: image_def)
    using f_props(1) by auto
  have "\<And> k \<gamma>. (k,\<gamma>) \<in> one_chain1\<Longrightarrow> line_integral_exists F basis \<gamma>"
  proof-
    fix k \<gamma>
    assume ass: "(k,\<gamma>) \<in> one_chain1"
    show "line_integral_exists F basis \<gamma>"
    proof (cases "k = 1")
      assume k_eq_1: "k = 1"
      then have i:"chain_subdiv_path \<gamma> (f(k,\<gamma>))"
        using f_props(2) ass by auto
      have ii:"boundary_chain (f(k,\<gamma>))"
        using f_props(1) ass assms by (auto simp add: boundary_chain_def)
      have iii:"\<forall>(k, \<gamma>)\<in>f (k, \<gamma>). line_integral_exists F basis \<gamma>"
        using f_props(1) ass assms
        by blast
      have "iv": " \<forall>(k, \<gamma>)\<in>f (k, \<gamma>). valid_path \<gamma>"
        using f_props(1) ass assms
        by blast
      show ?thesis
        using line_integral_on_path_eq_line_integral_on_equiv_chain_1'(2)[OF i ii iii iv finite_basis]
        by auto
    next
      assume "k \<noteq> 1"
      then have k_eq_neg1: "k = -1"
        using ass boundary_chain1
        by (auto simp add: boundary_chain_def)
      then have i:"chain_subdiv_path (reversepath \<gamma>) (f(k,\<gamma>))"
        using f_props(2) ass by auto
      have ii:"boundary_chain (f(k,\<gamma>))"
        using f_props(1) ass assms by (auto simp add: boundary_chain_def)
      have iii:"\<forall>(k, \<gamma>)\<in>f (k, \<gamma>). line_integral_exists F basis \<gamma>"
        using f_props(1) ass assms
        by blast
      have "iv": " \<forall>(k, \<gamma>)\<in>f (k, \<gamma>). valid_path \<gamma>"
        using f_props(1) ass assms
        by blast
      have "valid_path (reversepath \<gamma>)" "line_integral_exists F basis (reversepath \<gamma>)"
        using line_integral_on_path_eq_line_integral_on_equiv_chain_1'(2,3)[OF i ii iii iv finite_basis]
        by auto
      then show ?thesis
        using line_integral_on_reverse_path(2) reversepath_reversepath
        by fastforce
    qed
  qed
  then show line_integral_exists_on_chain1: "\<forall>(k, \<gamma>) \<in> one_chain1. line_integral_exists F basis \<gamma>"
    by auto
  have 1: "one_chain_line_integral F basis (\<Union>(f ` one_chain1)) = one_chain_line_integral F basis one_chain1"
  proof -
    have 0:"one_chain_line_integral F basis (\<Union>(f ` one_chain1)) =
                           (\<Sum>one_chain \<in> (f ` one_chain1). one_chain_line_integral F basis one_chain)"
    proof -
      have finite: "\<forall>chain \<in> (f ` one_chain1). finite chain"
        using f_props(1) finite_chain2
        by (meson Sup_upper finite_subset)
      have disj: " \<forall>A\<in>f ` one_chain1. \<forall>B\<in>f ` one_chain1. A \<noteq> B \<longrightarrow> A \<inter> B = {}"
        by (metis (no_types, hide_lams) f_props(3) image_iff)
      show "one_chain_line_integral F basis (\<Union>(f ` one_chain1)) =
                                (\<Sum>one_chain \<in> (f ` one_chain1). one_chain_line_integral F basis one_chain)"
        using Groups_Big.comm_monoid_add_class.sum.Union_disjoint[OF finite disj]
          one_chain_line_integral_def
        by auto
    qed
    have 1:"(\<Sum>one_chain \<in> (f ` one_chain1). one_chain_line_integral F basis one_chain) =
                            one_chain_line_integral F basis one_chain1"
    proof -
      have "(\<Sum>one_chain \<in> (f ` one_chain1). one_chain_line_integral F basis one_chain) =
                                     (\<Sum>(k,\<gamma>)\<in>one_chain1. k*(line_integral F basis \<gamma>))"
      proof -
        have i:"(\<Sum>one_chain \<in> (f ` (one_chain1 - {p. f p = {}})). one_chain_line_integral F basis one_chain) =
                                       (\<Sum>(k,\<gamma>)\<in>one_chain1 - {p. f p = {}}. k*(line_integral F basis \<gamma>))"
        proof -
          have "inj_on f (one_chain1 - {p. f p = {}})"
            unfolding inj_on_def using f_props(3) by blast
          then have 0: "(\<Sum>one_chain \<in> (f ` (one_chain1 - {p. f p = {}})). one_chain_line_integral F basis one_chain)
                                                       = (\<Sum>(k, \<gamma>) \<in> (one_chain1 - {p. f p = {}}). one_chain_line_integral F basis (f (k, \<gamma>)))"
            using Groups_Big.comm_monoid_add_class.sum.reindex
            by auto
          have "\<And> k \<gamma>. (k, \<gamma>) \<in> (one_chain1 - {p. f p = {}}) \<Longrightarrow>
                        one_chain_line_integral F basis (f(k, \<gamma>)) = k* (line_integral F basis \<gamma>)"
          proof-
            fix k \<gamma>
            assume ass: "(k, \<gamma>) \<in> (one_chain1 - {p. f p = {}})"
            have bchain: "boundary_chain (f(k,\<gamma>))"
              using f_props(1) boundary_chain2 ass
              by (auto simp add: boundary_chain_def)
            have wexist: "\<forall>(k, \<gamma>)\<in>(f(k,\<gamma>)). line_integral_exists F basis \<gamma>"
              using f_props(1) line_integral_exists_on_chain2 ass
              by blast
            have vpath: " \<forall>(k, \<gamma>)\<in>(f(k, \<gamma>)). valid_path \<gamma>"
              using f_props(1) assms ass
              by blast
            show "one_chain_line_integral F basis (f (k, \<gamma>)) = k * line_integral F basis \<gamma>"
            proof(cases "k = 1")
              assume k_eq_1: "k = 1"
              have "one_chain_line_integral F basis (f (k, \<gamma>)) = line_integral F basis \<gamma>"
                using f_props(2) k_eq_1 line_integral_on_path_eq_line_integral_on_equiv_chain_1' bchain wexist vpath ass finite_basis
                by auto
              then show "one_chain_line_integral F basis (f (k, \<gamma>)) = k * line_integral F basis \<gamma>"
                using k_eq_1  by auto
            next
              assume "k \<noteq> 1"
              then have k_eq_neg1: "k = -1"
                using ass boundary_chain1
                by (auto simp add: boundary_chain_def)
              have "one_chain_line_integral F basis (f (k, \<gamma>)) = line_integral F basis (reversepath \<gamma>)"
                using f_props(2) k_eq_neg1 line_integral_on_path_eq_line_integral_on_equiv_chain_1' bchain wexist vpath ass finite_basis
                by auto
              then have "one_chain_line_integral F basis (f (k, \<gamma>)) = - (line_integral F basis \<gamma>)"
                using line_integral_on_reverse_path(1) ass line_integral_exists_on_chain1
                  valid_subdiv_imp_valid_one_chain'[OF chain1_eq_chain2 boundary_chain1 boundary_chain2 valid_path]
                by force
              then show "one_chain_line_integral F basis (f (k, \<gamma>)) = k * line_integral F basis \<gamma>"
                using k_eq_neg1 by (auto  simp del: one_chain_line_integral_mult_left line_integral_mult_left)
            qed
          qed
          then have "(\<Sum>(k, \<gamma>) \<in> (one_chain1 - {p. f p = {}}). one_chain_line_integral F basis (f (k, \<gamma>)))
                   = (\<Sum>(k, \<gamma>) \<in> (one_chain1 - {p. f p = {}}). k* (line_integral F basis \<gamma>))"
            by (auto intro!: Finite_Cartesian_Product.sum_cong_aux)
          then show "(\<Sum>one_chain \<in> (f ` (one_chain1 - {p. f p = {}})). one_chain_line_integral F basis one_chain) =
                                                     (\<Sum>(k, \<gamma>) \<in> (one_chain1 - {p. f p = {}}). k* (line_integral F basis \<gamma>))"
            using 0  by auto
        qed
        have "\<And> (k::int) \<gamma>. (k, \<gamma>) \<in> one_chain1 \<Longrightarrow> (f (k, \<gamma>) = {}) \<Longrightarrow> (k, \<gamma>) \<in> {(k',\<gamma>'). k'* (line_integral F basis \<gamma>') = 0}"
        proof-
          fix k::"int"
          fix \<gamma>::"one_cube"
          assume ass:"(k, \<gamma>) \<in> one_chain1"
            "(f (k, \<gamma>) = {})"
          then have zero_line_integral:"one_chain_line_integral F basis (f (k, \<gamma>)) = 0"
            using one_chain_line_integral_def
            by auto
          have bchain: "boundary_chain (f(k,\<gamma>))"
            using f_props(1) boundary_chain2 ass
            by (auto simp add: boundary_chain_def)
          have wexist: "\<forall>(k, \<gamma>)\<in>(f(k,\<gamma>)). line_integral_exists F basis \<gamma>"
            using f_props(1) line_integral_exists_on_chain2 ass
            by blast
          have vpath: " \<forall>(k, \<gamma>)\<in>(f(k, \<gamma>)). valid_path \<gamma>"
            using f_props(1) assms ass by blast
          have "one_chain_line_integral F basis (f (k, \<gamma>)) = k * line_integral F basis \<gamma>"
          proof(cases "k = 1")
            assume k_eq_1: "k = 1"
            have "one_chain_line_integral F basis (f (k, \<gamma>)) = line_integral F basis \<gamma>"
              using f_props(2) k_eq_1 line_integral_on_path_eq_line_integral_on_equiv_chain_1' bchain wexist vpath ass finite_basis
              by (auto simp del: one_chain_line_integral_empty_chain)
            then show "one_chain_line_integral F basis (f (k, \<gamma>)) = k * line_integral F basis \<gamma>"
              using k_eq_1
              by auto
          next
            assume "k \<noteq> 1"
            then have k_eq_neg1: "k = -1"
              using ass boundary_chain1
              by (auto simp add: boundary_chain_def)
            have "one_chain_line_integral F basis (f (k, \<gamma>)) = line_integral F basis (reversepath \<gamma>)"
              using f_props(2) k_eq_neg1 line_integral_on_path_eq_line_integral_on_equiv_chain_1' bchain wexist vpath ass finite_basis
              by (auto simp del: one_chain_line_integral_empty_chain)
            then have "one_chain_line_integral F basis (f (k, \<gamma>)) = - (line_integral F basis \<gamma>)"
              using line_integral_on_reverse_path(1)  ass line_integral_exists_on_chain1
                valid_subdiv_imp_valid_one_chain'[OF chain1_eq_chain2 boundary_chain1 boundary_chain2 valid_path]
              by force
            then show "one_chain_line_integral F basis (f (k::int, \<gamma>)) = k * line_integral F basis \<gamma>"
              using k_eq_neg1  by (auto simp del: one_chain_line_integral_mult_left line_integral_mult_left line_integral_empty_basis one_chain_line_integral_empty_basis one_chain_line_integral_empty_chain)
          qed
          then show "(k, \<gamma>) \<in> {(k'::int, \<gamma>'). k' * line_integral F basis \<gamma>' = 0}"
            using zero_line_integral by auto
        qed
        then have ii:"(\<Sum>one_chain \<in> (f ` (one_chain1 - {p. f p = {}})). one_chain_line_integral F basis one_chain) =
                                                          (\<Sum>one_chain \<in> (f ` (one_chain1)). one_chain_line_integral F basis one_chain)"
        proof -
          have "\<And>one_chain. one_chain \<in> (f ` (one_chain1)) - (f ` (one_chain1 - {p. f p = {}})) \<Longrightarrow>
                                                         one_chain_line_integral F basis one_chain = 0"
          proof -
            fix one_chain
            assume "one_chain \<in> (f ` (one_chain1)) - (f ` (one_chain1 - {p. f p = {}}))"
            then show "one_chain_line_integral F basis one_chain = 0"
              by (auto simp add: one_chain_line_integral_def)
          qed
          then have 0:"(\<Sum>one_chain \<in> f ` (one_chain1) - (f ` (one_chain1 - {p. f p = {}})). one_chain_line_integral F basis one_chain)
                                                       = 0"
            using comm_monoid_add_class.sum.neutral by auto
          then have "(\<Sum>one_chain \<in> f ` (one_chain1). one_chain_line_integral F basis one_chain)
                                                      - (\<Sum>one_chain \<in> (f ` (one_chain1 - {p. f p = {}})). one_chain_line_integral F basis one_chain)
                                                       = 0"
          proof -
            have finte: "finite (f ` one_chain1)" using finite_chain1 by auto
            show ?thesis
              using Groups_Big.sum_diff[OF finte, of " (f ` (one_chain1 - {p. f p = {}}))"
                  "one_chain_line_integral F basis"]
                0
              by auto
          qed
          then show "(\<Sum>one_chain \<in> (f ` (one_chain1 - {p. f p = {}})). one_chain_line_integral F basis one_chain) =
                                                          (\<Sum>one_chain \<in> (f ` (one_chain1)). one_chain_line_integral F basis one_chain)"
            by auto
        qed
        have "\<And> (k::int) \<gamma>. (k, \<gamma>) \<in> one_chain1 \<Longrightarrow> (f (k, \<gamma>) = {}) \<Longrightarrow> f (k, \<gamma>) \<in> {chain. one_chain_line_integral F basis chain = 0}"
        proof-
          fix k::"int"
          fix \<gamma>::"one_cube"
          assume ass:"(k, \<gamma>) \<in> one_chain1" "(f (k, \<gamma>) = {})"
          then have "one_chain_line_integral F basis (f (k, \<gamma>)) = 0"
            using one_chain_line_integral_def  by auto
          then show "f (k, \<gamma>) \<in> {p'. (one_chain_line_integral F basis p' = 0)}"
            by auto
        qed
        then have iii:"(\<Sum>(k::int,\<gamma>)\<in>one_chain1 - {p. f p = {}}. k*(line_integral F basis \<gamma>))
                                                 = (\<Sum>(k::int,\<gamma>)\<in>one_chain1. k*(line_integral F basis \<gamma>))"
        proof-
          have "\<And> k \<gamma>. (k,\<gamma>)\<in>one_chain1 - (one_chain1 - {p. f p = {}})
                                                    \<Longrightarrow> k* (line_integral F basis \<gamma>) = 0"
          proof-
            fix k \<gamma>
            assume ass: "(k,\<gamma>)\<in>one_chain1 - (one_chain1 - {p. f p = {}})"
            then have "f(k, \<gamma>) = {}"
              by auto
            then have "one_chain_line_integral F basis (f(k, \<gamma>)) = 0"
              by (auto simp add: one_chain_line_integral_def)
            then have zero_line_integral:"one_chain_line_integral F basis (f (k, \<gamma>)) = 0"
              using one_chain_line_integral_def by auto
            have bchain: "boundary_chain (f(k,\<gamma>))"
              using f_props(1) boundary_chain2 ass
              by (auto simp add: boundary_chain_def)
            have wexist: "\<forall>(k, \<gamma>)\<in>(f(k,\<gamma>)). line_integral_exists F basis \<gamma>"
              using f_props(1) line_integral_exists_on_chain2 ass
              by blast
            have vpath: " \<forall>(k, \<gamma>)\<in>(f(k, \<gamma>)). valid_path \<gamma>"
              using f_props(1) assms ass
              by blast
            have "one_chain_line_integral F basis (f (k, \<gamma>)) = k * line_integral F basis \<gamma>"
            proof(cases "k = 1")
              assume k_eq_1: "k = 1"
              have "one_chain_line_integral F basis (f (k, \<gamma>)) = line_integral F basis \<gamma>"
                using f_props(2) k_eq_1 line_integral_on_path_eq_line_integral_on_equiv_chain_1' bchain wexist vpath ass finite_basis
                by (auto  simp del: one_chain_line_integral_mult_left line_integral_mult_left line_integral_empty_basis one_chain_line_integral_empty_basis one_chain_line_integral_empty_chain)
              then show "one_chain_line_integral F basis (f (k, \<gamma>)) = k * line_integral F basis \<gamma>"
                using k_eq_1
                by auto
            next
              assume "k \<noteq> 1"
              then have k_eq_neg1: "k = -1"
                using ass boundary_chain1
                by (auto simp add: boundary_chain_def)
              have "one_chain_line_integral F basis (f (k, \<gamma>)) = line_integral F basis (reversepath \<gamma>)"
                using f_props(2) k_eq_neg1 line_integral_on_path_eq_line_integral_on_equiv_chain_1' bchain wexist vpath ass finite_basis
                by (auto  simp del: one_chain_line_integral_mult_left line_integral_mult_left line_integral_empty_basis one_chain_line_integral_empty_basis one_chain_line_integral_empty_chain)
              then have "one_chain_line_integral F basis (f (k, \<gamma>)) = - (line_integral F basis \<gamma>)"
                using line_integral_on_reverse_path(1)  ass line_integral_exists_on_chain1
                  valid_subdiv_imp_valid_one_chain'[OF chain1_eq_chain2 boundary_chain1 boundary_chain2 valid_path]
                by force
              then show "one_chain_line_integral F basis (f (k, \<gamma>)) = k * line_integral F basis \<gamma>"
                using k_eq_neg1
                by (auto  simp del: one_chain_line_integral_mult_left line_integral_mult_left line_integral_empty_basis one_chain_line_integral_empty_basis one_chain_line_integral_empty_chain)
            qed
            then show "k* (line_integral F basis \<gamma>) = 0"
              using zero_line_integral
              by auto
          qed
          then have "\<forall>(k::int,\<gamma>)\<in>one_chain1 - (one_chain1 - {p. f p = {}}).
                                                      k* (line_integral F basis \<gamma>) = 0" by auto
          then have "(\<Sum>(k::int,\<gamma>)\<in>one_chain1 - (one_chain1 - {p. f p = {}}). k*(line_integral F basis \<gamma>)) = 0"
            using Groups_Big.comm_monoid_add_class.sum.neutral
              [of "one_chain1 - (one_chain1 - {p. f p = {}})" "(\<lambda>(k::int,\<gamma>). k* (line_integral F basis \<gamma>))"]
            by (simp add: split_beta)
          then have "(\<Sum>(k::int,\<gamma>)\<in>one_chain1. k*(line_integral F basis \<gamma>)) -
                     (\<Sum>(k::int,\<gamma>)\<in> (one_chain1 - {p. f p = {}}). k*(line_integral F basis \<gamma>)) = 0"
            using Groups_Big.sum_diff[OF finite_chain1]
            by (metis (no_types) Diff_subset \<open>(\<Sum>(k, \<gamma>)\<in>one_chain1 - (one_chain1 - {p. f p = {}}). k * line_integral F basis \<gamma>) = 0\<close> \<open>\<And>f B. B \<subseteq> one_chain1 \<Longrightarrow> sum f (one_chain1 - B) = sum f one_chain1 - sum f B\<close>)
          then show ?thesis by auto
        qed
        show ?thesis using i ii iii by auto
      qed
      then show ?thesis using one_chain_line_integral_def by auto
    qed
    show ?thesis using 0 1 by auto
  qed
  show "one_chain_line_integral F basis one_chain1 = one_chain_line_integral F basis subdiv" using 0 1 by auto
qed

lemma one_chain_line_integral_eq_line_integral_on_sudivision_2':
  assumes chain1_eq_chain2: "chain_subdiv_chain one_chain1 subdiv" and
    boundary_chain1: "boundary_chain one_chain1" and
    boundary_chain2: "boundary_chain subdiv" and
    line_integral_exists_on_chain1: "\<forall>(k, \<gamma>) \<in> one_chain1. line_integral_exists F basis \<gamma>" and
    valid_path: "\<forall>(k, \<gamma>) \<in> subdiv. valid_path \<gamma>" and
    finite_chain1: "finite one_chain1" and
    finite_basis: "finite basis"
  shows "one_chain_line_integral F basis one_chain1 = one_chain_line_integral F basis subdiv"
    "\<forall>(k, \<gamma>) \<in> subdiv. line_integral_exists F basis \<gamma>"
proof -
  obtain f where f_props:
    "((\<Union>(f ` one_chain1)) = subdiv)"
    "(\<forall>twoC\<in>one_chain1. chain_subdiv_path (coeff_cube_to_path twoC) (f twoC))"
    "(\<forall>p\<in>one_chain1. \<forall>p'\<in>one_chain1. p \<noteq> p' \<longrightarrow> f p \<inter> f p' = {})"
    "(\<forall>x \<in> one_chain1. finite (f x))"
    using chain1_eq_chain2 chain_subdiv_chain_character by (auto simp add: pairwise_def)
  have finite_chain2: "finite subdiv"
    using finite_chain1 f_props(1) f_props(4) by blast
  have "\<And>k \<gamma>. (k, \<gamma>) \<in> subdiv \<Longrightarrow> line_integral_exists F basis \<gamma>"
  proof -
    fix k \<gamma>
    assume ass: "(k, \<gamma>) \<in> subdiv"
    then obtain k' \<gamma>' where kp_gammap: "(k',\<gamma>') \<in> one_chain1" "(k,\<gamma>) \<in> f(k',\<gamma>')"
      using f_props(1) by fastforce
    show "line_integral_exists F basis \<gamma>"
    proof (cases "k' = 1")
      assume k_eq_1: "k' = 1"
      then have i:"chain_subdiv_path \<gamma>' (f(k',\<gamma>'))"
        using f_props(2) kp_gammap ass  by auto
      have ii:"boundary_chain (f(k',\<gamma>'))"
        using f_props(1) ass assms kp_gammap  by (meson UN_I boundary_chain_def)
      have iii:"line_integral_exists F basis \<gamma>'"
        using assms kp_gammap by blast
      have "iv": " \<forall>(k, \<gamma>)\<in>f (k', \<gamma>'). valid_path \<gamma>"
        using f_props(1) ass assms kp_gammap by blast
      show ?thesis
        using line_integral_on_path_eq_line_integral_on_equiv_chain_2'(2)[OF i ii iii iv finite_basis] kp_gammap
        by auto
    next
      assume "k' \<noteq> 1"
      then have k_eq_neg1: "k' = -1"
        using boundary_chain1 kp_gammap
        by (auto simp add: boundary_chain_def)
      then have i:"chain_subdiv_path (reversepath \<gamma>') (f(k',\<gamma>'))"
        using f_props(2) kp_gammap by auto
      have ii:"boundary_chain (f(k',\<gamma>'))"
        using f_props(1) assms kp_gammap  by (meson UN_I boundary_chain_def)
      have iii: " \<forall>(k, \<gamma>)\<in>f (k', \<gamma>'). valid_path \<gamma>"
        using f_props(1) ass assms kp_gammap by blast
      have iv: "valid_path (reversepath \<gamma>')"
        using valid_path_equiv_valid_chain_list'[OF i ii iii]
        by force
      have "line_integral_exists F basis \<gamma>'"
        using assms kp_gammap  by blast
      then have x: "line_integral_exists F basis (reversepath \<gamma>')"
        using iv line_integral_on_reverse_path(2) valid_path_reversepath
        by fastforce
      show ?thesis
        using line_integral_on_path_eq_line_integral_on_equiv_chain_2'(2)[OF i ii x iii finite_basis] kp_gammap
        by auto
    qed
  qed
  then show "\<forall>(k, \<gamma>)\<in>subdiv. line_integral_exists F basis \<gamma>" by auto
  then show "one_chain_line_integral F basis one_chain1 = one_chain_line_integral F basis subdiv"
    using one_chain_line_integral_eq_line_integral_on_sudivision_1'(1) assms
    by auto
qed

lemma line_integral_sum_gen:
  assumes finite_basis:
    "finite basis" and
    line_integral_exists:
    "line_integral_exists F basis1 \<gamma>"
    "line_integral_exists F basis2 \<gamma>" and
    basis_partition:
    "basis1 \<union> basis2 = basis" "basis1 \<inter> basis2 = {}"
  shows "\<integral>\<^bsub>\<gamma>\<^esub> F\<downharpoonright>\<^bsub>basis\<^esub> = \<integral>\<^bsub>\<gamma>\<^esub> F\<downharpoonright>\<^bsub>basis1\<^esub> + \<integral>\<^bsub>\<gamma>\<^esub> F\<downharpoonright>\<^bsub>basis2\<^esub>"
    "line_integral_exists F basis \<gamma>"
   apply (simp add: line_integral_def)
proof -
  have 0: "integral {0..1} (\<lambda>x. (\<Sum>b\<in>basis1. F (\<gamma> x) \<bullet> b * (vector_derivative \<gamma> (at x within {0..1}) \<bullet> b)) +
                                       (\<Sum>b\<in>basis2. F (\<gamma> x) \<bullet> b * (vector_derivative \<gamma> (at x within {0..1}) \<bullet> b))) =
                    integral {0..1} (\<lambda>x. \<Sum>b\<in>basis1. F (\<gamma> x) \<bullet> b * (vector_derivative \<gamma> (at x within {0..1}) \<bullet> b)) +
                           integral {0..1} (\<lambda>x. \<Sum>b\<in>basis2. F (\<gamma> x) \<bullet> b * (vector_derivative \<gamma> (at x within {0..1}) \<bullet> b))"
    using Henstock_Kurzweil_Integration.integral_add line_integral_exists
    by (auto simp add: line_integral_exists_def)
  have 1: "integral {0..1} (\<lambda>x. \<Sum>b\<in>basis. F (\<gamma> x) \<bullet> b * (vector_derivative \<gamma> (at x within {0..1}) \<bullet> b)) =
                       integral {0..1} (\<lambda>x. (\<Sum>b\<in>basis1. F (\<gamma> x) \<bullet> b * (vector_derivative \<gamma> (at x within {0..1}) \<bullet> b)) +
                                            (\<Sum>b\<in>basis2. F (\<gamma> x) \<bullet> b * (vector_derivative \<gamma> (at x within {0..1}) \<bullet> b)))"
    by (metis (mono_tags, lifting) basis_partition finite_Un finite_basis sum.union_disjoint)
  show "integral {0..1} (\<lambda>x. \<Sum>b\<in>basis. F (\<gamma> x) \<bullet> b * (vector_derivative \<gamma> (at x within {0..1}) \<bullet> b)) =
                  integral {0..1} (\<lambda>x. \<Sum>b\<in>basis1. F (\<gamma> x) \<bullet> b * (vector_derivative \<gamma> (at x within {0..1}) \<bullet> b)) +
                  integral {0..1} (\<lambda>x. \<Sum>b\<in>basis2. F (\<gamma> x) \<bullet> b * (vector_derivative \<gamma> (at x within {0..1}) \<bullet> b))"
    using 0 1
    by auto
  have 2: "((\<lambda>x. (\<Sum>b\<in>basis1. F (\<gamma> x) \<bullet> b * (vector_derivative \<gamma> (at x within {0..1}) \<bullet> b)) +
                                       (\<Sum>b\<in>basis2. F (\<gamma> x) \<bullet> b * (vector_derivative \<gamma> (at x within {0..1}) \<bullet> b))) has_integral
                    integral {0..1} (\<lambda>x. \<Sum>b\<in>basis1. F (\<gamma> x) \<bullet> b * (vector_derivative \<gamma> (at x within {0..1}) \<bullet> b)) +
                           integral {0..1} (\<lambda>x. \<Sum>b\<in>basis2. F (\<gamma> x) \<bullet> b * (vector_derivative \<gamma> (at x within {0..1}) \<bullet> b))) {0..1}"
    using Henstock_Kurzweil_Integration.has_integral_add line_integral_exists has_integral_integral
    apply (auto simp add: line_integral_exists_def)
    by blast
  have 3: "(\<lambda>x. \<Sum>b\<in>basis. F (\<gamma> x) \<bullet> b * (vector_derivative \<gamma> (at x within {0..1}) \<bullet> b)) =
                       (\<lambda>x. (\<Sum>b\<in>basis1. F (\<gamma> x) \<bullet> b * (vector_derivative \<gamma> (at x within {0..1}) \<bullet> b)) +
                                            (\<Sum>b\<in>basis2. F (\<gamma> x) \<bullet> b * (vector_derivative \<gamma> (at x within {0..1}) \<bullet> b)))"
    by (metis (mono_tags, lifting) basis_partition finite_Un finite_basis sum.union_disjoint)
  show "line_integral_exists F basis \<gamma>"
    apply(auto simp add: line_integral_exists_def has_integral_integral)
    using 2 3
    using has_integral_integrable_integral by fastforce
qed

lemma line_integral_summation:
  assumes finite_basis:
    "finite B" and
    line_integral_exists:
    "\<forall>b\<in>B. line_integral_exists F {b} \<gamma>"
  shows "\<integral>\<^bsub>\<gamma>\<^esub> F\<downharpoonright>\<^bsub>B\<^esub> = (\<Sum>b\<in>B. \<integral>\<^bsub>\<gamma>\<^esub> F\<downharpoonright>\<^bsub>{b}\<^esub>)"
        "line_integral_exists F B \<gamma>"
  using assms
proof(induction)
  case empty
  then show "line_integral F {} \<gamma> = (\<Sum>b\<in>{}. line_integral F {b} \<gamma>)"
            "\<forall>b\<in>{}. line_integral_exists F {b} \<gamma> \<Longrightarrow> line_integral_exists F {} \<gamma>"
    unfolding line_integral_def line_integral_exists_def by auto
next
  case (insert x B)
  assume ass: "\<forall>b\<in>insert x B. line_integral_exists F {b} \<gamma>"
  then show "line_integral_exists F (insert x B) \<gamma>"
    by (metis Diff_disjoint Diff_insert_absorb finite.insertI insert insert_iff insert_is_Un line_integral_sum_gen(2))
  show "line_integral F (insert x B) \<gamma> = (\<Sum>b\<in>insert x B. line_integral F {b} \<gamma>)"
    by (smt ass Un_insert_left disjoint_insert(2) finite.insertI inf_bot_right inf_commute insert insertI1 line_integral_sum_gen(1) sum.insert sup_bot.left_neutral)
qed

lemma one_chain_line_integral_summation:
  assumes finite_basis:
    "finite C" and
    "finite B" and
    line_integral_exists:
    "\<forall>b\<in>B. one_chain_line_integral_exists F {b} C"
  shows "one_chain_line_integral F B C = (\<Sum>b\<in>B. one_chain_line_integral F {b} C)"
        "one_chain_line_integral_exists F B C"
  using assms
proof(induction C)
  case empty
  then show "\<^sup>H\<integral>\<^bsub>{}\<^esub> F \<downharpoonright>\<^bsub>B\<^esub> = (\<Sum>b\<in>B. \<^sup>H\<integral>\<^bsub>{}\<^esub> F \<downharpoonright>\<^bsub>{b}\<^esub>)" "one_chain_line_integral_exists F B {}" by auto
next
  case case_ass: (insert c C)
  assume ass: "finite B" "\<forall>b\<in>B. one_chain_line_integral_exists F {b} (insert c C)"
  then show int_exists: "one_chain_line_integral_exists F B (insert c C)"
    unfolding one_chain_line_integral_exists_def apply auto
    apply (metis case_ass line_integral_summation)
    using line_integral_summation(2) by fastforce
  have "(\<^sup>H\<integral>\<^bsub>{c}\<^esub> F \<downharpoonright>\<^bsub>B\<^esub>) = (\<Sum>b\<in>B. \<^sup>H\<integral>\<^bsub>{c}\<^esub> F \<downharpoonright>\<^bsub>{b}\<^esub>)"
    unfolding one_chain_line_integral_def
    apply (cases c)
    subgoal for k \<gamma> apply (auto simp add: integral_mult_right[symmetric] line_integral_mult_left[where F = F])
      apply(rule line_integral_summation)
      using ass case_ass apply blast
      using ass case_ass line_integral_mult_left(2)
      by (simp add: one_chain_line_integral_exists_def line_integral_mult_left)
    done
  moreover have "(\<^sup>H\<integral>\<^bsub>C\<^esub> F \<downharpoonright>\<^bsub>B\<^esub>) = (\<Sum>b\<in>B. \<^sup>H\<integral>\<^bsub>C\<^esub> F \<downharpoonright>\<^bsub>{b}\<^esub>)"
    using case_ass ass one_chain_line_integral_subset
    by blast
  moreover have "(\<Sum>b\<in>B. \<^sup>H\<integral>\<^bsub>insert c C\<^esub> F \<downharpoonright>\<^bsub>{b}\<^esub>) = (\<Sum>b\<in>B. \<^sup>H\<integral>\<^bsub>{c}\<^esub> F \<downharpoonright>\<^bsub>{b} \<^esub>+ \<^sup>H\<integral>\<^bsub>C\<^esub> F \<downharpoonright>\<^bsub>{b}\<^esub>)"
    apply(rule Finite_Cartesian_Product.sum_cong_aux Groups_Big.comm_monoid_add_class.sum.distrib)
    using case_ass(1-2)
    unfolding one_chain_line_integral_def one_chain_line_integral_exists_def
    by auto
  moreover have "... = (\<Sum>b\<in>B. \<^sup>H\<integral>\<^bsub>{c}\<^esub> F \<downharpoonright>\<^bsub>{b}\<^esub>) + (\<Sum>b\<in>B. \<^sup>H\<integral>\<^bsub>C\<^esub> F \<downharpoonright>\<^bsub>{b}\<^esub>)" using comm_monoid_add_class.sum.distrib by force
  moreover have "\<^sup>H\<integral>\<^bsub>insert c C\<^esub> F \<downharpoonright>\<^bsub>B\<^esub> = (\<^sup>H\<integral>\<^bsub>{c}\<^esub> F \<downharpoonright>\<^bsub>B\<^esub>) + \<^sup>H\<integral>\<^bsub>C\<^esub> F \<downharpoonright>\<^bsub>B\<^esub>"
    using case_ass(1-2)
    unfolding one_chain_line_integral_def one_chain_line_integral_exists_def
    by auto
  ultimately show "\<^sup>H\<integral>\<^bsub>insert c C\<^esub> F \<downharpoonright>\<^bsub>B\<^esub> = (\<Sum>b\<in>B. \<^sup>H\<integral>\<^bsub>insert c C\<^esub> F \<downharpoonright>\<^bsub>{b}\<^esub>)" by auto
qed

definition common_boundary_sudivision_exists where
    "common_boundary_sudivision_exists one_chain1 one_chain2 \<equiv>
             \<exists>subdiv. chain_subdiv_chain one_chain1 subdiv \<and>
                      chain_subdiv_chain one_chain2 subdiv \<and>
                      (\<forall>(k, \<gamma>) \<in> subdiv. valid_path \<gamma>)\<and>
                      boundary_chain subdiv"

lemma common_boundary_sudivision_commutative':
  "(common_boundary_sudivision_exists one_chain1 one_chain2) = (common_boundary_sudivision_exists one_chain2 one_chain1)"
  apply (simp add: common_boundary_sudivision_exists_def)
  by blast

lemma common_subdivision_imp_eq_line_integral':
  assumes "(common_boundary_sudivision_exists one_chain1 one_chain2)"
    "boundary_chain one_chain1"
    "boundary_chain one_chain2"
    "\<forall>(k, \<gamma>)\<in>one_chain1. line_integral_exists F basis \<gamma>"
    "finite one_chain1"
    "finite one_chain2"
    "finite basis"
  shows "one_chain_line_integral F basis one_chain1 = one_chain_line_integral F basis one_chain2"
    "\<forall>(k, \<gamma>)\<in>one_chain2. line_integral_exists F basis \<gamma>"
proof -
  obtain subdiv where subdiv_props:
    "chain_subdiv_chain one_chain1 subdiv"
    "chain_subdiv_chain one_chain2 subdiv"
    "(\<forall>(k, \<gamma>) \<in> subdiv. valid_path \<gamma>)"
    "(boundary_chain subdiv)"
    using assms
    by (auto simp add: common_boundary_sudivision_exists_def)
  have i: "\<forall>(k, \<gamma>)\<in>subdiv. line_integral_exists F basis \<gamma>"
    using one_chain_line_integral_eq_line_integral_on_sudivision_2'(2)[OF subdiv_props(1) assms(2) subdiv_props(4) assms(4) subdiv_props(3) assms(5) assms(7)]
    by auto
  show "one_chain_line_integral F basis one_chain1 = one_chain_line_integral F basis one_chain2"
    using one_chain_line_integral_eq_line_integral_on_sudivision_2'(1)[OF subdiv_props(1) assms(2) subdiv_props(4) assms(4) subdiv_props(3) assms(5) assms(7)]
      one_chain_line_integral_eq_line_integral_on_sudivision_1'(1)[OF subdiv_props(2) assms(3) subdiv_props(4) i subdiv_props(3) assms(6) assms(7)]
    by auto
  show "\<forall>(k, \<gamma>)\<in>one_chain2. line_integral_exists F basis \<gamma>"
    using one_chain_line_integral_eq_line_integral_on_sudivision_1'(2)[OF subdiv_props(2) assms(3) subdiv_props(4) i subdiv_props(3) assms(6) assms(7)]
    by auto
qed

definition common_subdiv_exists where
    "common_subdiv_exists one_chain1 one_chain2 \<equiv>
        \<exists>subdiv ps1 ps2. chain_subdiv_chain (one_chain1 - ps1) subdiv \<and>
                  chain_subdiv_chain (one_chain2 - ps2) subdiv \<and>
                  (\<forall>(k, \<gamma>) \<in> subdiv. valid_path \<gamma>) \<and>
                  (boundary_chain subdiv) \<and>
                  (\<forall>(k, \<gamma>) \<in> ps1. point_path \<gamma>) \<and>
                  (\<forall>(k, \<gamma>) \<in> ps2. point_path \<gamma>)"

lemma common_subdiv_exists_comm':
  shows "common_subdiv_exists C1 C2 = common_subdiv_exists C2 C1"
  by (auto simp add: common_subdiv_exists_def)

lemma line_integral_degenerate_chain:
  assumes "(\<forall>(k, \<gamma>) \<in> chain. point_path \<gamma>)"
  assumes "finite basis"
  shows "one_chain_line_integral F basis chain = 0"
proof (simp add: one_chain_line_integral_def del: line_integral_mult_left)
  have "\<forall>(k,g)\<in>chain. line_integral F basis g = 0"
    using assms line_integral_point_path
    by blast
  then have "\<forall>(k,g)\<in>chain. real_of_int k * line_integral F basis g = 0" by (auto simp del: line_integral_mult_left)
  then have "\<And>p. p \<in> chain \<Longrightarrow> (case p of (i, f) \<Rightarrow> real_of_int i * line_integral F basis f) = 0"
    by fastforce
  then show "(\<Sum>x\<in>chain. case x of (k, g) \<Rightarrow> real_of_int k * line_integral F basis g) = 0"
    by simp
qed

lemma gen_common_subdiv_imp_common_subdiv':
  shows "(common_subdiv_exists one_chain1 one_chain2) = (\<exists>ps1 ps2. (common_boundary_sudivision_exists (one_chain1 - ps1) (one_chain2 - ps2)) \<and> (\<forall>(k, \<gamma>)\<in>ps1. point_path \<gamma>) \<and> (\<forall>(k, \<gamma>)\<in>ps2. point_path \<gamma>))"
  by (auto simp add: common_subdiv_exists_def common_boundary_sudivision_exists_def)

lemma common_subdiv_imp_gen_common_subdiv':
  assumes "(common_boundary_sudivision_exists one_chain1 one_chain2)"
  shows "(common_subdiv_exists one_chain1 one_chain2)"
  using assms
  apply (auto simp add: common_subdiv_exists_def common_boundary_sudivision_exists_def)
  by (metis Diff_empty all_not_in_conv)

lemma one_chain_line_integral_point_paths:
  assumes "finite one_chain"
  assumes "finite basis"
  assumes "(\<forall>(k, \<gamma>)\<in>ps. point_path \<gamma>)"
  shows "one_chain_line_integral F basis (one_chain - ps) = one_chain_line_integral F basis (one_chain)"
proof-
  have 0:"(\<forall>x\<in>ps. case x of (k, g) \<Rightarrow>  (real_of_int k * line_integral F basis g) = 0)"
    using line_integral_point_path assms
    by force
  show "one_chain_line_integral F basis (one_chain - ps) = one_chain_line_integral F basis one_chain"
    unfolding one_chain_line_integral_def using 0 \<open>finite one_chain\<close>
    by (force simp add:  intro: comm_monoid_add_class.sum.mono_neutral_left)
qed

lemma boundary_chain_diff:
  assumes "boundary_chain one_chain"
  shows "boundary_chain (one_chain - s)"
  using assms
  by (auto simp add: boundary_chain_def)

lemma gen_common_subdivision_imp_eq_line_integral:
  assumes "(common_subdiv_exists one_chain1 one_chain2)"
    "boundary_chain one_chain1"
    "boundary_chain one_chain2"
    "\<forall>(k, \<gamma>)\<in>one_chain1. line_integral_exists F basis \<gamma>"
    "finite one_chain1"
    "finite one_chain2"
    "finite basis"
  shows "\<^sup>H\<integral>\<^bsub>one_chain1\<^esub> F \<downharpoonright>\<^bsub>basis\<^esub>  = \<^sup>H\<integral>\<^bsub>one_chain2\<^esub> F \<downharpoonright>\<^bsub>basis\<^esub> "
    "(k, \<gamma>)\<in>one_chain2 \<Longrightarrow> line_integral_exists F basis \<gamma>"
proof -
  obtain ps1 ps2 where gen_subdiv: "(common_boundary_sudivision_exists (one_chain1 - ps1) (one_chain2 - ps2))" "(\<forall>(k, \<gamma>)\<in>ps1. point_path \<gamma>)" " (\<forall>(k, \<gamma>)\<in>ps2. point_path \<gamma>)"
    using assms(1) gen_common_subdiv_imp_common_subdiv'
    by blast
  show "one_chain_line_integral F basis one_chain1 = one_chain_line_integral F basis one_chain2"
    using one_chain_line_integral_point_paths
      assms(2-7) gen_subdiv
      common_subdivision_imp_eq_line_integral'(1)[OF gen_subdiv(1) boundary_chain_diff[OF assms(2)] boundary_chain_diff[OF assms(3)]]
    by auto
  show "line_integral_exists F basis \<gamma>" if "(k, \<gamma>)\<in>one_chain2"
  proof-
    obtain subdiv where subdiv_props:
      "chain_subdiv_chain (one_chain1-ps1) subdiv"
      "chain_subdiv_chain (one_chain2-ps2) subdiv"
      "(\<forall>(k, \<gamma>) \<in> subdiv. valid_path \<gamma>)"
      "(boundary_chain subdiv)"
      using gen_subdiv(1)
      by (auto simp add: common_boundary_sudivision_exists_def)
    have "\<forall>(k, \<gamma>)\<in>subdiv. line_integral_exists F basis \<gamma>"
      using one_chain_line_integral_eq_line_integral_on_sudivision_2'(2)[OF subdiv_props(1) boundary_chain_diff[OF assms(2)] subdiv_props(4)] assms(4) subdiv_props(3) assms(5) assms(7)
      by blast
    then have i: "\<forall>(k, \<gamma>)\<in>one_chain2-ps2. line_integral_exists F basis \<gamma>"
      using one_chain_line_integral_eq_line_integral_on_sudivision_1'(2)[OF subdiv_props(2) boundary_chain_diff[OF assms(3)] subdiv_props(4)] subdiv_props(3) assms(6) assms(7)
      by blast
    then show ?thesis
      using gen_subdiv(3) line_integral_exists_point_path[OF assms(7)] that
      by blast
  qed      
qed

lemma common_subdiv_exists_refl':
      assumes "common_subdiv_exists C1 C2"
      shows "common_subdiv_exists C2 C1"
      using assms
      apply(simp add: common_subdiv_exists_def)
      by auto

lemma chain_subdiv_path_singleton:
  shows "chain_subdiv_path \<gamma> {(1,\<gamma>)}" 
  using chain_subdiv_path.intros
  by (metis coeff_cube_to_path.simps)

lemma chain_subdiv_path_singleton_reverse:
  shows "chain_subdiv_path (reversepath \<gamma>) {(-1,\<gamma>)}"
  using chain_subdiv_path.intros
  by (metis coeff_cube_to_path.simps one_neq_neg_one)

lemma chain_subdiv_chain_refl:
  assumes "boundary_chain C"
  shows "chain_subdiv_chain C C"
  using chain_subdiv_path_singleton chain_subdiv_path_singleton_reverse assms
  unfolding chain_subdiv_chain_def boundary_chain_def pairwise_def using case_prodI2 coeff_cube_to_path.simps 
  by (rule_tac x="\<lambda>x. {x}" in exI) auto

lemma line_integral_exists_smooth_one_base:
  assumes "\<gamma> C1_differentiable_on {0..1}" (*To generalise this to valid_path we need a version of has_integral_substitution_strong that allows finite discontinuities of f*)
    "continuous_on (path_image \<gamma>) (\<lambda>x. F x \<bullet> b)"
  shows "line_integral_exists F {b} \<gamma>"
proof-
  have gamma2_differentiable: "(\<forall>x \<in> {0 .. 1}. \<gamma> differentiable at x)"
    using assms(1) 
    by (auto simp add: valid_path_def C1_differentiable_on_eq)
  then have gamma2_b_component_differentiable: "(\<forall>x \<in> {0 .. 1}. (\<lambda>x. (\<gamma> x) \<bullet> b) differentiable at x)"
    by auto
  then have "(\<lambda>x. (\<gamma> x) \<bullet> b) differentiable_on {0..1}"
    using differentiable_at_withinI
    by (auto simp add: differentiable_on_def)
  then have gama2_cont_comp: "continuous_on {0..1} (\<lambda>x. (\<gamma> x) \<bullet> b)"
    using differentiable_imp_continuous_on
    by auto
  have gamma2_cont:"continuous_on {0..1} \<gamma>"
    using assms(1) C1_differentiable_imp_continuous_on
    by (auto simp add: valid_path_def)
  have iii: "continuous_on {0..1} (\<lambda>x. F (\<gamma> x) \<bullet> b * (vector_derivative \<gamma> (at x within {0..1}) \<bullet> b))"
  proof-
    have 0: "continuous_on {0..1} (\<lambda>x. F (\<gamma> x) \<bullet> b)"
      using assms(2) continuous_on_compose[OF gamma2_cont]
      by (auto simp add: path_image_def) 
    obtain D where D: "(\<forall>x\<in>{0..1}. (\<gamma> has_vector_derivative D x) (at x)) \<and> continuous_on {0..1} D"
      using assms(1)
      by (auto simp add: C1_differentiable_on_def)
    then have *:"\<forall>x\<in>{0..1}. vector_derivative \<gamma> (at x within{0..1}) = D x"
      using vector_derivative_at vector_derivative_at_within_ivl
      by fastforce
    then have "continuous_on {0..1} (\<lambda>x. vector_derivative \<gamma> (at x within{0..1}))"
      using continuous_on_eq D by force
    then have 1: "continuous_on {0..1} (\<lambda>x. (vector_derivative \<gamma> (at x within{0..1})) \<bullet> b)"
      by(auto intro!: continuous_intros)
    show ?thesis
      using continuous_on_mult[OF 0 1]  by auto
  qed
  then have "(\<lambda>x. F (\<gamma> x) \<bullet> b * (vector_derivative \<gamma> (at x within {0..1}) \<bullet> b)) integrable_on {0..1}"
    using integrable_continuous_real
    by auto
  then show "line_integral_exists F {b} \<gamma>"
    by(auto simp add: line_integral_exists_def)
qed

lemma line_integral_primitive_lemma:
  fixes f :: "complex \<Rightarrow> complex" and g :: "real \<Rightarrow> complex"
  assumes "a \<le> b"
      and "\<And>x. x \<in> s \<Longrightarrow> (f has_field_derivative f' x) (at x within s)"
      and "g piecewise_differentiable_on {a..b}"  "\<And>x. x \<in> {a..b} \<Longrightarrow> g x \<in> s"
    shows "((\<lambda>x. f'(g x) * vector_derivative g (at x within {a..b}))
             has_integral (f(g b) - f(g a))) {a..b}"
proof -
  obtain k where k: "finite k" "\<forall>x\<in>{a..b} - k. g differentiable (at x within {a..b})" and cg: "continuous_on {a..b} g"
    using assms by (auto simp: piecewise_differentiable_on_def)
  have cfg: "continuous_on {a..b} (\<lambda>x. f (g x))"
    apply (rule continuous_on_compose [OF cg, unfolded o_def])
    using assms
    apply (metis field_differentiable_def field_differentiable_imp_continuous_at continuous_on_eq_continuous_within continuous_on_subset image_subset_iff)
    done
  { fix x::real
    assume a: "a < x" and b: "x < b" and xk: "x \<notin> k"
    then have "g differentiable at x within {a..b}"
      using k by (simp add: differentiable_at_withinI)
    then have "(g has_vector_derivative vector_derivative g (at x within {a..b})) (at x within {a..b})"
      by (simp add: vector_derivative_works has_field_derivative_def scaleR_conv_of_real)
    then have gdiff: "(g has_derivative (\<lambda>u. u * vector_derivative g (at x within {a..b}))) (at x within {a..b})"
      by (simp add: has_vector_derivative_def scaleR_conv_of_real)
    have "(f has_field_derivative (f' (g x))) (at (g x) within g ` {a..b})"
      using assms by (metis a atLeastAtMost_iff b DERIV_subset image_subset_iff less_eq_real_def)
    then have fdiff: "(f has_derivative (*) (f' (g x))) (at (g x) within g ` {a..b})"
      by (simp add: has_field_derivative_def)
    have "((\<lambda>x. f (g x)) has_vector_derivative f' (g x) * vector_derivative g (at x within {a..b})) (at x within {a..b})"
      using diff_chain_within [OF gdiff fdiff]
      by (simp add: has_vector_derivative_def scaleR_conv_of_real o_def mult_ac)
  } note * = this
  show ?thesis
    apply (rule fundamental_theorem_of_calculus_interior_strong)
    using k assms cfg *
    apply (auto simp: at_within_Icc_at)
    done
qed

lemma contour_integral_primitive_lemma: (*Only for real normed fields, i.e. vectors with products*)
  fixes f :: "'a::{euclidean_space,real_normed_field} \<Rightarrow> 'a::{euclidean_space,real_normed_field}" and
        g :: "real \<Rightarrow> 'a"
  assumes "\<And>(a::'a). a \<in> s \<Longrightarrow> (f has_field_derivative (f' a)) (at a within s)"
      and "g piecewise_differentiable_on {0::real..1}"  "\<And>x. x \<in> {0..1} \<Longrightarrow> g x \<in> s"
      and "base_vec \<in> Basis"
    shows "((\<lambda>x. ((f'(g x)) * (vector_derivative g (at x within {0..1}))) \<bullet> base_vec)
             has_integral (((f(g 1)) \<bullet> base_vec - (f(g 0)) \<bullet> base_vec))) {0..1}"
proof -
  obtain k where k: "finite k" "\<forall>x\<in>{0..1} - k. g differentiable (at x within {0..1})" and cg: "continuous_on {0..1} g"
    using assms by (auto simp: piecewise_differentiable_on_def)
  have cfg: "continuous_on {0..1} (\<lambda>x. f (g x))"
    apply (rule continuous_on_compose [OF cg, unfolded o_def])
    using assms
    apply (metis field_differentiable_def field_differentiable_imp_continuous_at continuous_on_eq_continuous_within continuous_on_subset image_subset_iff)
    done
  { fix x::real
    assume a: "0 < x" and b: "x < 1" and xk: "x \<notin> k"
    then have "g differentiable at x within {0..1}"
      using k by (simp add: differentiable_at_withinI)
    then have "(g has_vector_derivative vector_derivative g (at x within {0..1})) (at x within {0..1})"
      by (simp add: vector_derivative_works has_field_derivative_def scaleR_conv_of_real)
    then have gdiff: "(g has_derivative (\<lambda>u. of_real u * vector_derivative g (at x within {0..1}))) (at x within {0..1})"
      by (simp add: has_vector_derivative_def scaleR_conv_of_real)
    have "(f has_field_derivative (f' (g x))) (at (g x) within g ` {0..1})"
      using assms by (metis a atLeastAtMost_iff b DERIV_subset image_subset_iff less_eq_real_def)
    then have fdiff: "(f has_derivative (*) (f' (g x))) (at (g x) within g ` {0..1})"
      by (simp add: has_field_derivative_def)
    have "((\<lambda>x. f (g x)) has_vector_derivative f' (g x) * vector_derivative g (at x within {0..1})) (at x within {0..1})"
      using diff_chain_within [OF gdiff fdiff]
      by (simp add: has_vector_derivative_def scaleR_conv_of_real o_def mult_ac)
  }
  then have *: "\<forall>x\<in>{0<..<1} - k.  ((\<lambda>x. f (g x)) has_vector_derivative f' (g x) * vector_derivative g (at x within {0..1})) (at x within {0..1})"
       by auto
  have "((\<lambda>x. ((f'(g x))) * ((vector_derivative g (at x within {0..1}))))
             has_integral (((f(g 1)) - (f(g 0))))) {0..1}"
    using fundamental_theorem_of_calculus_interior_strong[OF k(1) zero_le_one _ cfg]
    using k assms cfg * by (auto simp: at_within_Icc_at)
  then have "((\<lambda>x. (((f'(g x))) * ((vector_derivative g (at x within {0..1})))) \<bullet> base_vec)
             has_integral (((f(g 1)) - (f(g 0)))) \<bullet> base_vec) {0..1}"
       using has_integral_componentwise_iff assms(4)
       by fastforce
  then show ?thesis using inner_mult_left'
       by (simp add: inner_mult_left' inner_diff_left)
qed

lemma fundamental_theorem_of_line_integrals_gen:
  fixes f::"'a::euclidean_space \<Rightarrow> real" and
    g::"real \<Rightarrow> 'a"
  assumes "\<forall>a \<in> s. (has_gradient f f' a)"
    and "\<forall>x \<in> {0..1}. \<gamma> x \<in> s"
    and "\<forall>x \<in> {0..1}. (\<gamma> has_vector_derivative (\<gamma>' x)) (at x within {0..1})"
  shows "\<integral>\<^bsub>\<gamma>\<^esub> f' \<downharpoonright>\<^bsub>Basis\<^esub> = f(\<gamma> 1) - f(\<gamma> 0)"
  unfolding line_integral_Basis
proof-
  {fix x assume ass: "x\<in>{0::real..1}"
  have "((f o \<gamma>) has_vector_derivative (f' (\<gamma> x) \<bullet> (\<gamma>' x))) (at x within {0..1})"
    apply(rule vector_derivative_diff_chain_within[where ?f = \<gamma> and ?g = f and ?f' = "\<gamma>' x"])
    subgoal using assms(3) ass by auto
    subgoal using assms(1) unfolding has_gradient_def using ass assms(2) has_derivative_at_withinI
      by blast
    done
  }
  then have "((%x. (f' (\<gamma> x) \<bullet> (\<gamma>' x))) has_integral ((f o \<gamma>) 1 - (f o \<gamma>) 0)) {0..1}"
    using fundamental_theorem_of_calculus
    by smt
  then show "integral {0..1} (\<lambda>x. f' (\<gamma> x) \<bullet> vector_derivative \<gamma> (at x within {0..1})) = f (\<gamma> 1) - f (\<gamma> 0)"
    by (smt Henstock_Kurzweil_Integration.integral_cong assms(3) comp_apply has_integral_integrable has_integral_unique integrable_integral sum.cong vector_derivative_within_closed_interval)
qed

lemma fundamental_theorem_of_line_integrals:
  fixes f :: "'a::euclidean_space \<Rightarrow> real" and
    g :: "real \<Rightarrow> 'a"
  assumes "\<forall>a \<in> s. (has_gradient f f' a)"
    and "\<forall>x \<in> {0..1}. \<gamma> x \<in> s"
    and "\<gamma> C1_differentiable_on {0..1}"
  shows "\<integral>\<^bsub>\<gamma>\<^esub> f' \<downharpoonright>\<^bsub>Basis\<^esub> = f(\<gamma> 1) - f(\<gamma> 0)"
proof-
  obtain \<gamma>' where "(\<gamma> has_vector_derivative \<gamma>' x) (at x)" if "x\<in>{0..1}" for x 
    using assms(3)
    unfolding C1_differentiable_on_def
    by blast
  then show ?thesis using fundamental_theorem_of_line_integrals_gen[OF assms(1)]
       assms(2)
    by (metis has_vector_derivative_at_within)
qed

(*path reparam_weaketrization*)

definition reparam_weak where
  "reparam_weak \<gamma>1 \<gamma>2 \<equiv> \<exists>\<phi>. (\<forall>x\<in>{0..1}. \<gamma>1 x = (\<gamma>2 o \<phi>) x) \<and> \<phi> piecewise_C1_differentiable_on {0..1} \<and> \<phi>(0) = 0 \<and> \<phi>(1) = 1 \<and> \<phi> ` {0..1} = {0..1}"

lemma reparam_weak_eq_refl:
  shows "reparam_weak \<gamma>1 \<gamma>1"
  unfolding reparam_weak_def
  apply (rule_tac x="id" in exI)
  by (auto simp add: id_def piecewise_C1_differentiable_on_def C1_differentiable_on_def continuous_on_id)

lemma reparam_weak_eq_line_integrals:
  assumes "reparam_weak \<gamma>1 \<gamma>2"
    "\<gamma>2 C1_differentiable_on {0..1}" (*To generalise this to valid_path we need a version of has_integral_substitution_strong that allows finite discontinuities of f*)
    "continuous_on (path_image \<gamma>2) (\<lambda>x. F x \<bullet> b)"
  shows "line_integral F {b} \<gamma>1 = line_integral F {b} \<gamma>2"
    "line_integral_exists F {b} \<gamma>1"
proof-
  obtain \<phi> where phi: "(\<forall>x\<in>{0..1}. \<gamma>1 x = (\<gamma>2 o \<phi>) x)"" \<phi> piecewise_C1_differentiable_on {0..1}"" \<phi>(0) = 0"" \<phi>(1) = 1"" \<phi> ` {0..1} = {0..1}"
    using assms(1)
    by (auto simp add: reparam_weak_def)
  obtain s where s: "finite s" "\<phi> C1_differentiable_on {0..1} - s"
    using phi
    by(auto simp add: reparam_weak_def piecewise_C1_differentiable_on_def)
  have cont_phi: "continuous_on {0..1} \<phi>"
    using phi
    by(auto simp add: reparam_weak_def piecewise_C1_differentiable_on_imp_continuous_on)
  have gamma2_differentiable: "(\<forall>x \<in> {0 .. 1}. \<gamma>2 differentiable at x)"
    using assms(2) 
    by (auto simp add: valid_path_def C1_differentiable_on_eq)
  then have gamma2_b_component_differentiable: "(\<forall>x \<in> {0 .. 1}. (\<lambda>x. (\<gamma>2 x) \<bullet> b) differentiable at x)"
    by auto
  then have "(\<lambda>x. (\<gamma>2 x) \<bullet> b) differentiable_on {0..1}"
    using differentiable_at_withinI
    by (auto simp add: differentiable_on_def)
  then have gama2_cont_comp: "continuous_on {0..1} (\<lambda>x. (\<gamma>2 x) \<bullet> b)"
    using differentiable_imp_continuous_on
    by auto
  have gamma2_cont:"continuous_on {0..1} \<gamma>2"
    using assms(2) C1_differentiable_imp_continuous_on
    by (auto simp add: valid_path_def)
  have iii: "continuous_on {0..1} (\<lambda>x. F (\<gamma>2 x) \<bullet> b * (vector_derivative \<gamma>2 (at x within {0..1}) \<bullet> b))"
  proof-
    have 0: "continuous_on {0..1} (\<lambda>x. F (\<gamma>2 x) \<bullet> b)"
      using assms(3) continuous_on_compose[OF gamma2_cont]
      by (auto simp add: path_image_def) 
    obtain D where D: "(\<forall>x\<in>{0..1}. (\<gamma>2 has_vector_derivative D x) (at x)) \<and> continuous_on {0..1} D"
      using assms(2)  by (auto simp add: C1_differentiable_on_def)
    then have *:"\<forall>x\<in>{0..1}. vector_derivative \<gamma>2 (at x within{0..1}) = D x"
      using vector_derivative_at vector_derivative_at_within_ivl
      by fastforce
    then have "continuous_on {0..1} (\<lambda>x. vector_derivative \<gamma>2 (at x within{0..1}))"
      using continuous_on_eq D  by force
    then have 1: "continuous_on {0..1} (\<lambda>x. (vector_derivative \<gamma>2 (at x within{0..1})) \<bullet> b)"
      by (auto intro!: continuous_intros)
    show ?thesis
      using continuous_on_mult[OF 0 1]  by auto
  qed
  have iv: "\<phi>(0) \<le> \<phi>(1)"
    using phi(3) phi(4)  by (simp add: reparam_weak_def)
  have v: "\<phi>`{0..1} \<subseteq> {0..1}"
    using phi(5)  by (simp add: reparam_weak_def)
  obtain D where D_props: "(\<forall>x\<in>{0..1} - s. (\<phi> has_vector_derivative D x) (at x))"
    using s
    by (auto simp add: C1_differentiable_on_def)
  then have "(\<And>x. x \<in> ({0..1} -s) \<Longrightarrow> (\<phi> has_vector_derivative D x) (at x within {0..1}))"
    using has_vector_derivative_at_within  by blast
  then have vi: "(\<And>x. x \<in> ({0..1} - s) \<Longrightarrow> (\<phi> has_real_derivative D x) (at x within {0..1}))"
    using has_field_derivative_iff_has_vector_derivative
    by blast
  have a:"((\<lambda>x. D x * (F (\<gamma>2 (\<phi> x)) \<bullet> b * (vector_derivative \<gamma>2 (at (\<phi> x) within {0..1}) \<bullet> b))) has_integral
              integral {\<phi> 0..\<phi> 1} (\<lambda>x. F (\<gamma>2 x) \<bullet> b * (vector_derivative \<gamma>2 (at x within {0..1}) \<bullet> b)))
              {0..1}"
    using has_integral_substitution_strong[OF s(1) zero_le_one iv v iii cont_phi vi]
    by simp
  then have b: "integral {0..1} (\<lambda>x. D x * (F (\<gamma>2 (\<phi> x)) \<bullet> b * (vector_derivative \<gamma>2 (at (\<phi> x) within {0..1}) \<bullet> b))) = 
                       integral {\<phi> 0..\<phi> 1} (\<lambda>x. F (\<gamma>2 x) \<bullet> b * (vector_derivative \<gamma>2 (at x within {0..1}) \<bullet> b))"
    by auto
  have gamma2_vec_diffable: "\<And>x::real. x \<in> {0..1} -s \<Longrightarrow> ((\<gamma>2 o \<phi>) has_vector_derivative vector_derivative (\<gamma>2 \<circ> \<phi>) (at x)) (at x)"
  proof-
    fix x::real 
    assume ass: "x \<in> {0..1} -s"
    have zer_le_x_le_1:"0\<le> x \<and> x \<le> 1" using ass by auto
    show "((\<gamma>2 o \<phi>) has_vector_derivative vector_derivative (\<gamma>2 \<circ> \<phi>) (at x)) (at x)"
    proof-
      have **: "\<gamma>2 differentiable at (\<phi> x)"
        using phi gamma2_differentiable
        by (auto simp add: zer_le_x_le_1)
      have ***: " \<phi> differentiable at x"
        using s ass
        by (auto simp add: C1_differentiable_on_eq)
      then show "((\<gamma>2 o \<phi>) has_vector_derivative vector_derivative (\<gamma>2 \<circ> \<phi>) (at x)) (at x)"
        using differentiable_chain_at[OF *** **] 
        by (auto simp add: vector_derivative_works)
    qed
  qed
  then have gamma2_vec_deriv_within: "\<And>x::real. x \<in> {0..1} -s \<Longrightarrow> vector_derivative (\<gamma>2 \<circ> \<phi>) (at x) = vector_derivative (\<gamma>2 \<circ> \<phi>) (at x within {0..1})"
    using vector_derivative_at_within_ivl[OF gamma2_vec_diffable]
    by auto
  have "\<forall>x\<in>{0..1} - s. D x * (vector_derivative \<gamma>2 (at (\<phi> x) within {0..1}) \<bullet> b) = (vector_derivative (\<gamma>2 \<circ> \<phi>) (at x within {0..1}) \<bullet> b)"
  proof
    fix x::real
    assume ass: "x \<in> {0..1} -s"
    then have 0: "\<phi> differentiable (at x)"
      using s
      by (auto simp add: C1_differentiable_on_def differentiable_def has_vector_derivative_def)
    obtain D2 where "(\<phi> has_vector_derivative D2) (at x)"
      using D_props ass
      by blast
    have "\<phi> x \<in> {0..1}"
      using phi(5) ass 
      by (auto simp add: reparam_weak_def)
    then have 1: "\<gamma>2 differentiable (at (\<phi> x))"
      using gamma2_differentiable
      by auto
    have 3:" vector_derivative \<gamma>2 (at (\<phi> x)) =  vector_derivative \<gamma>2 (at (\<phi> x) within {0..1})"
    proof-
      have *:"0\<le> \<phi> x \<and> \<phi> x \<le> 1" using phi(5) ass by auto
      then have **:"(\<gamma>2 has_vector_derivative (vector_derivative \<gamma>2 (at (\<phi> x)))) (at (\<phi> x))"
        using 1 vector_derivative_works
        by auto
      show ?thesis
        using * vector_derivative_at_within_ivl[OF **]
        by auto
    qed
    show "D x * (vector_derivative \<gamma>2 (at (\<phi> x) within {0..1}) \<bullet> b) = vector_derivative (\<gamma>2 \<circ> \<phi>) (at x within {0..1}) \<bullet> b"
      using vector_derivative_chain_at[OF 0 1]
      apply (auto simp add: gamma2_vec_deriv_within[OF ass, symmetric] 3[symmetric])
      using D_props ass vector_derivative_at
      by fastforce
  qed
  then have c:"\<And>x.  x\<in>({0..1} -s) \<Longrightarrow> D x * (F (\<gamma>2 (\<phi> x)) \<bullet> b * (vector_derivative \<gamma>2 (at (\<phi> x) within {0..1}) \<bullet> b)) = 
                                    F (\<gamma>2 (\<phi> x)) \<bullet> b * (vector_derivative (\<gamma>2 \<circ> \<phi>) (at x within {0..1}) \<bullet> b)"
    by auto
  then have d: "integral ({0..1}) (\<lambda>x. D x * (F (\<gamma>2 (\<phi> x)) \<bullet> b * (vector_derivative \<gamma>2 (at (\<phi> x) within {0..1}) \<bullet> b))) = 
                                            integral ({0..1}) (\<lambda>x. F (\<gamma>2 (\<phi> x)) \<bullet> b * (vector_derivative (\<gamma>2 \<circ> \<phi>) (at x within {0..1}) \<bullet> b))"
  proof-
    have "negligible s" using s(1) by auto
    then show ?thesis
      using c integral_spike  by (metis(no_types,lifting))
  qed
  have phi_in_int: "(\<And>x. x \<in> {0..1} \<Longrightarrow> \<phi> x \<in> {0..1})" using phi
    by(auto simp add:)
  then have e: "((\<lambda>x. F (\<gamma>2 (\<phi> x)) \<bullet> b * (vector_derivative (\<gamma>2 \<circ> \<phi>) (at x within {0..1}) \<bullet> b)) has_integral
                             integral {\<phi> 0..\<phi> 1} (\<lambda>x. F (\<gamma>2 x) \<bullet> b * (vector_derivative \<gamma>2 (at x within {0..1}) \<bullet> b))){0..1}"
  proof-
    have 0:"negligible s" using s(1) by auto
    have c':"\<forall> x\<in> {0..1} -s. D x * (F (\<gamma>2 (\<phi> x)) \<bullet> b * (vector_derivative \<gamma>2 (at (\<phi> x) within {0..1}) \<bullet> b)) = 
                                    F (\<gamma>2 (\<phi> x)) \<bullet> b * (vector_derivative (\<gamma>2 \<circ> \<phi>) (at x within {0..1}) \<bullet> b)"
      using c by auto
    have has_integral_spike_eq': "\<And>s t f g y. negligible s \<Longrightarrow>
                                               \<forall>x\<in>t - s. g x = f x \<Longrightarrow> (f has_integral y) t = (g has_integral y) t"
      using has_integral_spike_eq by metis
    show ?thesis
      using a has_integral_spike_eq'[OF 0 c']  by blast
  qed
  then have f: "((\<lambda>x. F (\<gamma>1 x) \<bullet> b * (vector_derivative  \<gamma>1 (at x within {0..1}) \<bullet> b)) has_integral
                     integral {\<phi> 0..\<phi> 1} (\<lambda>x. F (\<gamma>2 x) \<bullet> b * (vector_derivative \<gamma>2 (at x within {0..1}) \<bullet> b)))
                       {0..1}"
  proof-
    assume ass: "((\<lambda>x. F (\<gamma>2 (\<phi> x)) \<bullet> b * (vector_derivative (\<gamma>2 \<circ> \<phi>) (at x within {0..1}) \<bullet> b)) has_integral
                            integral {\<phi> 0..\<phi> 1} (\<lambda>x. F (\<gamma>2 x) \<bullet> b * (vector_derivative \<gamma>2 (at x within {0..1}) \<bullet> b)))
                             {0..1}"
    have *:"\<forall>x\<in>{0..1} - (s\<union>{0,1}). (\<lambda>x. F (\<gamma>2 (\<phi> x)) \<bullet> b * (vector_derivative  (\<gamma>2 \<circ> \<phi>) (at x within {0..1}) \<bullet> b)) x =
                                                  (\<lambda>x. F (\<gamma>1 x) \<bullet> b * (vector_derivative \<gamma>1 (at x within {0..1}) \<bullet> b)) x"
    proof-
      have "\<forall>x\<in>{0<..<1} - s. (vector_derivative  (\<gamma>2 \<circ> \<phi>) (at x within {0..1}) \<bullet> b) = (vector_derivative  (\<gamma>1) (at x within {0..1}) \<bullet> b)"
      proof
        have i: "open ({0<..<1} - s)" using open_diff s open_greaterThanLessThan by blast
        have ii: "\<forall>x\<in>{0<..<1::real} - s. (\<gamma>2 \<circ> \<phi>) x = \<gamma>1 x" using phi(1)
          by auto                              
        fix x::real
        assume ass: " x \<in> {0<..<1::real} - s"
        then have iii: "(\<gamma>2 \<circ> \<phi> has_vector_derivative vector_derivative (\<gamma>2 \<circ> \<phi>) (at x within {0..1})) (at x)"
          using has_vector_derivative_at_within gamma2_vec_deriv_within gamma2_vec_diffable
          by auto
            (*Most crucial but annoying step*)
        then have iv:"(\<gamma>1 has_vector_derivative vector_derivative (\<gamma>2 \<circ> \<phi>) (at x within {0..1})) (at x)"
          using has_derivative_transform_within_open i ii ass
          apply(auto simp add: has_vector_derivative_def)
          by force
        have v: "0 \<le> x" "x \<le> 1" using ass by auto
        have 0: "vector_derivative \<gamma>1 (at x within {0..1}) = vector_derivative (\<gamma>2 \<circ> \<phi>) (at x within {0..1})"
          using vector_derivative_at_within_ivl[OF iv v(1) v(2) zero_less_one]
          by force
        have 1: "vector_derivative (\<gamma>2 \<circ> \<phi>) (at x within {0..1}) = vector_derivative (\<gamma>2 \<circ> \<phi>) (at x within {0..1})"
          using vector_derivative_at_within_ivl[OF iii v(1) v(2) zero_less_one]
          by force
        then have "vector_derivative (\<gamma>2 \<circ> \<phi>) (at x within {0..1}) = vector_derivative \<gamma>1 (at x within {0..1})"
          using 0 1 by auto
        then show "vector_derivative (\<gamma>2 \<circ> \<phi>) (at x within {0..1}) \<bullet> b = vector_derivative \<gamma>1 (at x within {0..1}) \<bullet> b" by auto
      qed
      then have i: "\<forall>x\<in>{0..1} - (s\<union>{0,1}). (vector_derivative  (\<gamma>2 \<circ> \<phi>) (at x within {0..1}) \<bullet> b) = (vector_derivative  (\<gamma>1) (at x within {0..1}) \<bullet> b)"
        by auto
      have ii: "\<forall>x\<in>{0..1} - (s\<union>{0,1}).  F (\<gamma>1 x) \<bullet> b =  F (\<gamma>2 (\<phi> x)) \<bullet> b"
        using phi(1)  by auto
      show ?thesis 
        using i ii by auto
    qed
    have **: "negligible (s\<union>{0,1})" using s(1) by auto
    have has_integral_spike_eq': "\<And>s t g f y. negligible s \<Longrightarrow>
                                \<forall>x\<in>t - s. g x = f x \<Longrightarrow> (f has_integral y) t = (g has_integral y) t"
      using has_integral_spike_eq by metis
    show ?thesis
      using has_integral_spike_eq'[OF ** *] ass
      by blast
  qed
  then show "line_integral_exists F {b} \<gamma>1"
    using phi  by(auto simp add: line_integral_exists_def)
  have "integral ({0..1}) (\<lambda>x. F (\<gamma>2 (\<phi> x)) \<bullet> b * (vector_derivative (\<gamma>2 \<circ> \<phi>) (at x within {0..1}) \<bullet> b)) =
                      integral ({0..1}) (\<lambda>x. F (\<gamma>1 x) \<bullet> b * (vector_derivative \<gamma>1  (at x within {0..1}) \<bullet> b))"
    using integral_unique[OF e] integral_unique[OF f]
    by metis
  moreover have "integral ({0..1}) (\<lambda>x. F (\<gamma>2 (\<phi> x)) \<bullet> b * (vector_derivative (\<gamma>2 \<circ> \<phi>) (at x within {0..1}) \<bullet> b)) =
                         integral ({0..1}) (\<lambda>x. F (\<gamma>2 x) \<bullet> b * (vector_derivative \<gamma>2 (at x within {0..1}) \<bullet> b))"
    using b d phi  by (auto simp add:)
  ultimately show "line_integral F {b} \<gamma>1 = line_integral F {b} \<gamma>2"
    using phi by(auto simp add: line_integral_def)
qed

find_theorems "?P \<Longrightarrow> ?f piecewise_C1_differentiable_on ?s"

definition reparam where
  "reparam \<gamma>1 \<gamma>2 \<equiv> \<exists>\<phi>. (\<forall>x\<in>{0..1}. \<gamma>1 x = (\<gamma>2 o \<phi>) x) \<and> \<phi> piecewise_C1_differentiable_on {0..1} \<and> \<phi>(0) = 0 \<and> \<phi>(1) = 1 \<and> bij_betw \<phi> {0..1} {0..1} \<and> \<phi> -` {0..1} \<subseteq> {0..1}"

(*The definition of reparam can be changed to not have the condition "\<phi> -` {0..1} \<subseteq> {0..1}", if the assumption
  1) "(\<forall>x\<in>{0..1}. \<gamma>1 x = (\<gamma>2 o \<phi>) x)" would be changed to "\<exists>d. (\<forall>x\<in>{0..1}.\<forall>y. dist x y < d \<longrightarrow> \<gamma>1 y = (\<gamma>2 o \<phi>) y)".
  2) In reparam_eq_line_integrals the assumption pw_smooth would be changed to "\<gamma>1 piecewise_C1_differentiable_on {0..1}".
  Then the theorem 
  Derivs.eq_pw_smooth: 0 < ?d \<Longrightarrow> \<forall>x\<in>?s. \<forall>y. dist x y < ?d \<longrightarrow> ?f y = ?g y \<Longrightarrow> \<forall>x\<in>?s. ?f x = ?g x \<Longrightarrow> ?f piecewise_C1_differentiable_on ?s \<Longrightarrow> ?g piecewise_C1_differentiable_on ?s
  can be used to derive reparam_eq_line_integrals.
 *)

lemma reparam_eq_line_integrals:
  assumes reparam: "reparam \<gamma>1 \<gamma>2" and
    pw_smooth: "\<gamma>2 piecewise_C1_differentiable_on {0..1}" and (*To generalise this to valid_path we need a version of has_integral_substitution_strong that allows finite discontinuities of f*)
    cont: "continuous_on (path_image \<gamma>2) (\<lambda>x. F x \<bullet> b)" and
    line_integral_ex: "line_integral_exists F {b} \<gamma>2" (*We need to remove this and work on special cases like conservative fields and field/line combinations that satisfy the improper integrals theorem assumptions*)
  shows "line_integral F {b} \<gamma>1 = line_integral F {b} \<gamma>2"
    "line_integral_exists F {b} \<gamma>1"
proof-
  obtain \<phi> where phi: "(\<forall>x\<in>{0..1}. \<gamma>1 x = (\<gamma>2 o \<phi>) x)"" \<phi> piecewise_C1_differentiable_on {0..1}"" \<phi>(0) = 0"" \<phi>(1) = 1" "bij_betw \<phi> {0..1} {0..1}" "\<phi> -` {0..1} \<subseteq> {0..1}" (*"\<forall>x\<in>{0..1}. finite (\<phi> -` {x})"*)
    using reparam
    by (auto simp add: reparam_def)
  obtain s where s: "finite s" "\<phi> C1_differentiable_on {0..1} - s"
    using phi
    by(auto simp add: reparam_def piecewise_C1_differentiable_on_def)
  let ?s = "s \<inter> {0..1}"
  have s_inter: "finite ?s" "\<phi> C1_differentiable_on {0..1} - ?s"
    using s
     apply blast
    by (metis Diff_Compl Diff_Diff_Int Diff_eq inf.commute s(2))
  have cont_phi: "continuous_on {0..1} \<phi>"
    using phi
    by(auto simp add: reparam_def piecewise_C1_differentiable_on_imp_continuous_on)
  obtain s' D where s'_D: "finite s'" "(\<forall>x \<in> {0 .. 1} - s'. \<gamma>2 differentiable at x)" "(\<forall>x\<in>{0..1} - s'. (\<gamma>2 has_vector_derivative D x) (at x)) \<and> continuous_on ({0..1} - s') D"
    using pw_smooth 
    apply (auto simp add: valid_path_def piecewise_C1_differentiable_on_def C1_differentiable_on_eq)
    by (simp add: vector_derivative_works)
  let ?s' = "s' \<inter> {0..1}"
  have gamma2_differentiable: "finite ?s'" "(\<forall>x \<in> {0 .. 1} - ?s'. \<gamma>2 differentiable at x)" "(\<forall>x\<in>{0..1} - ?s'. (\<gamma>2 has_vector_derivative D x) (at x)) \<and> continuous_on ({0..1} - ?s') D"
    using s'_D
      apply blast
    using s'_D(2) apply auto[1]
    by (metis Diff_Int2 inf_top.left_neutral s'_D(3))
  then have gamma2_b_component_differentiable: "(\<forall>x \<in> {0 .. 1} - ?s'. (\<lambda>x. (\<gamma>2 x) \<bullet> b) differentiable at x)"
    using differentiable_inner by force
  then have "(\<lambda>x. (\<gamma>2 x) \<bullet> b) differentiable_on {0..1} - ?s'"
    using differentiable_at_withinI
    by (auto simp add: differentiable_on_def)
  then have gama2_cont_comp: "continuous_on ({0..1} - ?s') (\<lambda>x. (\<gamma>2 x) \<bullet> b)"
    using differentiable_imp_continuous_on
    by auto
      (**********Additional needed assumptions ************)
  have s_in01: "?s \<subseteq> {0..1}" by auto
  have s'_in01: "?s' \<subseteq> {0..1}" by auto
  have phi_backimg_s': "\<phi> -` {0..1} \<subseteq> {0..1}" using phi by auto
  have "inj_on \<phi> {0..1}" using phi(5) by (auto simp add: bij_betw_def)
  then have finite_bck_img_single_s': "\<forall>x\<in>?s'. finite ({0..1} \<inter> (\<phi> -` {x}))"
    by (metis finite.emptyI finite_insert finite_vimage_IntI inf_commute)
  have bij_phi: "bij_betw \<phi> {0..1} {0..1}" using phi(5) by auto
  have gamma2_line_integrable: "(\<lambda>x. F (\<gamma>2 x) \<bullet> b * (vector_derivative \<gamma>2 (at x within {0..1}) \<bullet> b)) integrable_on {0..1}"
    using line_integral_ex
    by (simp add: line_integral_exists_def)
      (****************************************************************)
  have finite_neg_img: "finite ({0..1} \<inter>  \<phi> -` ?s')"
    using finite_bck_img_single_s'
    by (metis \<open>inj_on \<phi> {0..1}\<close> finite_vimage_IntI gamma2_differentiable(1) inf_commute)
  have gamma2_cont:"continuous_on ({0..1} - ?s') \<gamma>2"
    using  gamma2_differentiable
    by (meson continuous_at_imp_continuous_on differentiable_imp_continuous_within)
  have iii: "continuous_on ({0..1} - ?s') (\<lambda>x. F (\<gamma>2 x) \<bullet> b * (vector_derivative \<gamma>2 (at x within {0..1}) \<bullet> b))"
  proof-
    have 0: "continuous_on ({0..1} - ?s') (\<lambda>x. F (\<gamma>2 x) \<bullet> b)"
      using cont continuous_on_compose[OF gamma2_cont] continuous_on_compose2 gamma2_cont
      unfolding path_image_def  by fastforce
    have D: "(\<forall>x\<in>{0..1} - ?s'. (\<gamma>2 has_vector_derivative D x) (at x)) \<and> continuous_on ({0..1} - ?s') D"
      using gamma2_differentiable
      by auto                       
    then have *:"\<forall>x\<in>{0..1} - ?s'. vector_derivative \<gamma>2 (at x within{0..1}) = D x"
      using vector_derivative_at vector_derivative_at_within_ivl
      by fastforce
    then have "continuous_on ({0..1} - ?s') (\<lambda>x. vector_derivative \<gamma>2 (at x within{0..1}))"
      using continuous_on_eq D
      by metis
    then have 1: "continuous_on ({0..1} - ?s') (\<lambda>x. (vector_derivative \<gamma>2 (at x within{0..1})) \<bullet> b)"
      by (auto intro!: continuous_intros)
    show ?thesis
      using continuous_on_mult[OF 0 1]
      by auto
  qed
  have iv: "\<phi>(0) \<le> \<phi>(1)"
    using phi(3) phi(4)
    by (simp add: reparam_def)
  have v: "\<phi>`{0..1} \<subseteq> {0..1}"
    using phi
    by (auto simp add: reparam_def bij_betw_def)
  obtain D where D_props: "(\<forall>x\<in>{0..1} - ?s. (\<phi> has_vector_derivative D x) (at x))"
    using s
    by (auto simp add: C1_differentiable_on_def)
  then have "(\<And>x. x \<in> ({0..1} - ?s) \<Longrightarrow> (\<phi> has_vector_derivative D x) (at x within {0..1}))"
    using has_vector_derivative_at_within
    by blast
  then have vi: "(\<And>x. x \<in> ({0..1} - ?s) \<Longrightarrow> (\<phi> has_real_derivative D x) (at x within {0..1}))"
    using has_field_derivative_iff_has_vector_derivative
    by blast
  have a:"((\<lambda>x. D x * (F (\<gamma>2 (\<phi> x)) \<bullet> b * (vector_derivative \<gamma>2 (at (\<phi> x) within {0..1}) \<bullet> b))) has_integral
              integral {\<phi> 0..\<phi> 1} (\<lambda>x. F (\<gamma>2 x) \<bullet> b * (vector_derivative \<gamma>2 (at x within {0..1}) \<bullet> b)))
              ({0..1})"
  proof-
    have a: "integral {\<phi> 1..\<phi> 0} (\<lambda>x. F (\<gamma>2 x) \<bullet> b * (vector_derivative \<gamma>2 (at x within {0..1}) \<bullet> b)) = 0" using integral_singleton integral_empty iv
      by (simp add: phi(3) phi(4))
    have b: "((\<lambda>x. D x *\<^sub>R (F (\<gamma>2 (\<phi> x)) \<bullet> b * (vector_derivative \<gamma>2 (at (\<phi> x) within {0..1}) \<bullet> b))) has_integral
                       integral {\<phi> 0..\<phi> 1} (\<lambda>x. F (\<gamma>2 x) \<bullet> b * (vector_derivative \<gamma>2 (at x within {0..1}) \<bullet> b)) - integral {\<phi> 1..\<phi> 0} (\<lambda>x. F (\<gamma>2 x) \<bullet> b * (vector_derivative \<gamma>2 (at x within {0..1}) \<bullet> b)))
                           {0..1}"
      apply(rule has_integral_substitution_general_'[OF s_inter(1) zero_le_one gamma2_differentiable(1) v gamma2_line_integrable iii cont_phi finite_bck_img_single_s'])
    proof-
      have "surj_on {0..1} \<phi>"
        using bij_phi
        by (metis (full_types) bij_betw_def image_subsetI rangeI)
      then show "surj_on ?s' \<phi>" using bij_phi s'_in01
        by blast
      show "inj_on \<phi> ({0..1} \<union> (?s \<union> ({0..1} \<inter> \<phi> -` ?s')))"
      proof-
        have i: "inj_on \<phi> {0..1}" using  bij_phi 
          using bij_betw_def by blast
        have ii: "({0..1} \<union> (?s \<union> ({0..1} \<inter> \<phi> -` ?s'))) = {0..1}" using phi_backimg_s' s_in01 s'_in01
          by blast
        show ?thesis using i ii by auto
      qed
      show "\<And>x. x \<in> {0..1} - ?s \<Longrightarrow> (\<phi> has_real_derivative D x) (at x within {0..1})"
        using vi by blast
      show "\<phi> -` (s' \<inter> {0..1}) \<subseteq> {0..1}" using phi by auto
    qed
    show ?thesis using a b by auto
  qed
  then have b: "integral {0..1} (\<lambda>x. D x * (F (\<gamma>2 (\<phi> x)) \<bullet> b * (vector_derivative \<gamma>2 (at (\<phi> x) within {0..1}) \<bullet> b))) = 
                       integral {\<phi> 0..\<phi> 1} (\<lambda>x. F (\<gamma>2 x) \<bullet> b * (vector_derivative \<gamma>2 (at x within {0..1}) \<bullet> b))"
    by auto
  have gamma2_vec_diffable: "\<And>x::real. x \<in> {0..1} - ((\<phi> -` ?s') \<union> ?s) \<Longrightarrow> ((\<gamma>2 o \<phi>) has_vector_derivative vector_derivative (\<gamma>2 \<circ> \<phi>) (at x)) (at x)"
  proof-
    fix x::real 
    assume ass: "x \<in> {0..1} -   ((\<phi> -` ?s') \<union> ?s)"
    have zer_le_x_le_1:"0\<le> x \<and> x \<le> 1" using ass
      by simp
    show "((\<gamma>2 o \<phi>) has_vector_derivative vector_derivative (\<gamma>2 \<circ> \<phi>) (at x)) (at x)"
    proof-
      have **: "\<gamma>2 differentiable at (\<phi> x)"
        using  gamma2_differentiable(2) ass v 
        by blast
      have ***: " \<phi> differentiable at x"
        using s ass
        by (auto simp add: C1_differentiable_on_eq)
      then show "((\<gamma>2 o \<phi>) has_vector_derivative vector_derivative (\<gamma>2 \<circ> \<phi>) (at x)) (at x)"
        using differentiable_chain_at[OF *** **] 
        by (auto simp add: vector_derivative_works)
    qed
  qed
  then have gamma2_vec_deriv_within: "\<And>x::real. x \<in> {0..1} - ((\<phi> -` ?s') \<union> ?s) \<Longrightarrow> vector_derivative (\<gamma>2 \<circ> \<phi>) (at x) = vector_derivative (\<gamma>2 \<circ> \<phi>) (at x within {0..1})"
    using vector_derivative_at_within_ivl[OF gamma2_vec_diffable]
    by auto
  have "\<forall>x\<in>{0..1} - ((\<phi> -` ?s') \<union> ?s). D x * (vector_derivative \<gamma>2 (at (\<phi> x) within {0..1}) \<bullet> b) = (vector_derivative (\<gamma>2 \<circ> \<phi>) (at x within {0..1}) \<bullet> b)"
  proof
    fix x::real
    assume ass: "x \<in> {0..1} -((\<phi> -` ?s') \<union> ?s)"
    then have 0: "\<phi> differentiable (at x)"
      using s by (auto simp add: C1_differentiable_on_def differentiable_def has_vector_derivative_def)
    obtain D2 where "(\<phi> has_vector_derivative D2) (at x)"
      using D_props ass  by blast
    have "\<phi> x \<in> {0..1} - ?s'"
      using phi(5) ass by (metis Diff_Un Diff_iff Int_iff bij_betw_def image_eqI vimageI)
    then have 1: "\<gamma>2 differentiable (at (\<phi> x))"
      using gamma2_differentiable
      by auto
    have 3:" vector_derivative \<gamma>2 (at (\<phi> x)) =  vector_derivative \<gamma>2 (at (\<phi> x) within {0..1})"
    proof-
      have *:"0\<le> \<phi> x \<and> \<phi> x \<le> 1" using phi(5) ass
        using \<open>\<phi> x \<in> {0..1} - s' \<inter> {0..1}\<close> by auto
      then have **:"(\<gamma>2 has_vector_derivative (vector_derivative \<gamma>2 (at (\<phi> x)))) (at (\<phi> x))"
        using 1 vector_derivative_works  by auto
      show ?thesis
        using * vector_derivative_at_within_ivl[OF **]  by auto
    qed
    show "D x * (vector_derivative \<gamma>2 (at (\<phi> x) within {0..1}) \<bullet> b) = vector_derivative (\<gamma>2 \<circ> \<phi>) (at x within {0..1}) \<bullet> b"
      using vector_derivative_chain_at[OF 0 1]
      apply (auto simp add: gamma2_vec_deriv_within[OF ass, symmetric] 3[symmetric])
      using D_props ass vector_derivative_at
      by fastforce
  qed
  then have c:"\<And>x.  x \<in> {0..1} -({0..1} \<inter> (\<phi> -` ?s') \<union> ?s) \<Longrightarrow> D x * (F (\<gamma>2 (\<phi> x)) \<bullet> b * (vector_derivative \<gamma>2 (at (\<phi> x) within {0..1}) \<bullet> b)) = 
                                    F (\<gamma>2 (\<phi> x)) \<bullet> b * (vector_derivative (\<gamma>2 \<circ> \<phi>) (at x within {0..1}) \<bullet> b)"
    by auto
  then have d: "integral ({0..1}) (\<lambda>x. D x * (F (\<gamma>2 (\<phi> x)) \<bullet> b * (vector_derivative \<gamma>2 (at (\<phi> x) within {0..1}) \<bullet> b))) = 
                                            integral ({0..1}) (\<lambda>x. F (\<gamma>2 (\<phi> x)) \<bullet> b * (vector_derivative (\<gamma>2 \<circ> \<phi>) (at x within {0..1}) \<bullet> b))"
  proof-
    have "negligible (({0..1} \<inter> (\<phi> -` ?s')) \<union> ?s)" using finite_neg_img s(1) by auto
    then show ?thesis
      using c integral_spike  by (metis(no_types,lifting))
  qed
  have phi_in_int: "(\<And>x. x \<in> {0..1} \<Longrightarrow> \<phi> x \<in> {0..1})" using phi
    using v by blast
  then have e: "((\<lambda>x. F (\<gamma>2 (\<phi> x)) \<bullet> b * (vector_derivative (\<gamma>2 \<circ> \<phi>) (at x within {0..1}) \<bullet> b)) has_integral
                             integral {\<phi> 0..\<phi> 1} (\<lambda>x. F (\<gamma>2 x) \<bullet> b * (vector_derivative \<gamma>2 (at x within {0..1}) \<bullet> b))){0..1}"
  proof-
    have "negligible ?s" using s_inter(1) by auto
    have 0: "negligible (({0..1} \<inter> (\<phi> -` ?s')) \<union> ?s)" using finite_neg_img s(1) by auto
    have c':"\<forall> x\<in> {0..1} - ({0..1} \<inter> (\<phi> -` ?s') \<union> ?s). D x * (F (\<gamma>2 (\<phi> x)) \<bullet> b * (vector_derivative \<gamma>2 (at (\<phi> x) within {0..1}) \<bullet> b)) = 
                                    F (\<gamma>2 (\<phi> x)) \<bullet> b * (vector_derivative (\<gamma>2 \<circ> \<phi>) (at x within {0..1}) \<bullet> b)"
      using c  by blast
    have has_integral_spike_eq': "\<And>s t f g y. negligible s \<Longrightarrow>
                                                       \<forall>x\<in>t - s. g x = f x \<Longrightarrow> (f has_integral y) t = (g has_integral y) t"
      using has_integral_spike_eq by metis
    show ?thesis
      using a has_integral_spike_eq'[OF 0 c']  by blast
  qed
  then have f: "((\<lambda>x. F (\<gamma>1 x) \<bullet> b * (vector_derivative  \<gamma>1 (at x within {0..1}) \<bullet> b)) has_integral
                     integral {\<phi> 0..\<phi> 1} (\<lambda>x. F (\<gamma>2 x) \<bullet> b * (vector_derivative \<gamma>2 (at x within {0..1}) \<bullet> b)))
                       {0..1}"
  proof-
    assume ass: "((\<lambda>x. F (\<gamma>2 (\<phi> x)) \<bullet> b * (vector_derivative (\<gamma>2 \<circ> \<phi>) (at x within {0..1}) \<bullet> b)) has_integral
                            integral {\<phi> 0..\<phi> 1} (\<lambda>x. F (\<gamma>2 x) \<bullet> b * (vector_derivative \<gamma>2 (at x within {0..1}) \<bullet> b)))
                             {0..1}"
    have *:"\<forall>x\<in>{0..1} - ((\<phi> -` ?s') \<union> ?s \<union> {0,1}). (\<lambda>x. F (\<gamma>2 (\<phi> x)) \<bullet> b * (vector_derivative  (\<gamma>2 \<circ> \<phi>) (at x within {0..1}) \<bullet> b)) x =
                                                  (\<lambda>x. F (\<gamma>1 x) \<bullet> b * (vector_derivative \<gamma>1 (at x within {0..1}) \<bullet> b)) x"
    proof-
      have "\<forall>x\<in>{0<..<1}  - ({0..1} \<inter> \<phi> -` ?s' \<union> ?s). (vector_derivative  (\<gamma>2 \<circ> \<phi>) (at x within {0..1}) \<bullet> b) = (vector_derivative  (\<gamma>1) (at x within {0..1}) \<bullet> b)"
      proof
        have i: "open ({0<..<1}  - ({0..1} \<inter> (\<phi> -` ?s') \<union> ?s))" using open_diff s(1) open_greaterThanLessThan finite_neg_img
          by (simp add: open_diff)
        have ii: "\<forall>x\<in>{0<..<1::real}  - ({0..1} \<inter> \<phi> -` ?s' \<union> ?s). (\<gamma>2 \<circ> \<phi>) x = \<gamma>1 x" using phi(1)
          by auto                              
        fix x::real
        assume ass: " x \<in> {0<..<1::real} -  ({0..1} \<inter> (\<phi> -` ?s') \<union> ?s)"
        then have iii: "(\<gamma>2 \<circ> \<phi> has_vector_derivative vector_derivative (\<gamma>2 \<circ> \<phi>) (at x within {0..1})) (at x)"
          by (smt DiffD1 DiffD2 DiffI Diff_Diff_Int Int_Un_distrib Int_absorb1 atLeastAtMost_iff gamma2_vec_deriv_within gamma2_vec_diffable greaterThanLessThan_iff s_in01)
            (*Most crucial but annoying step*)
        then have iv:"(\<gamma>1 has_vector_derivative vector_derivative (\<gamma>2 \<circ> \<phi>) (at x within {0..1})) (at x)"
          using has_derivative_transform_within_open i ii ass
          apply(auto simp add: has_vector_derivative_def)
          by (meson ass has_derivative_transform_within_open i ii)+
        have v: "0 \<le> x" "x \<le> 1" using ass by auto
        have 0: "vector_derivative \<gamma>1 (at x within {0..1}) = vector_derivative (\<gamma>2 \<circ> \<phi>) (at x within {0..1})"
          using vector_derivative_at_within_ivl[OF iv v(1) v(2) zero_less_one]
          by force
        have 1: "vector_derivative (\<gamma>2 \<circ> \<phi>) (at x within {0..1}) = vector_derivative (\<gamma>2 \<circ> \<phi>) (at x within {0..1})"
          using vector_derivative_at_within_ivl[OF iii v(1) v(2) zero_less_one]
          by force
        then have "vector_derivative (\<gamma>2 \<circ> \<phi>) (at x within {0..1}) = vector_derivative \<gamma>1 (at x within {0..1})"
          using 0 1 by auto
        then show "vector_derivative (\<gamma>2 \<circ> \<phi>) (at x within {0..1}) \<bullet> b = vector_derivative \<gamma>1 (at x within {0..1}) \<bullet> b" by auto
      qed
      then have i: "\<forall>x\<in>{0..1} - ((({0..1} \<inter> (\<phi> -` ?s')) \<union> ?s) \<union> {0,1}). (vector_derivative  (\<gamma>2 \<circ> \<phi>) (at x within {0..1}) \<bullet> b) = (vector_derivative  (\<gamma>1) (at x within {0..1}) \<bullet> b)"
        by auto
      have ii: "\<forall>x\<in>{0..1} - ({0..1} \<inter> (\<phi> -` ?s') \<union> ?s \<union> {0,1}).  F (\<gamma>1 x) \<bullet> b =  F (\<gamma>2 (\<phi> x)) \<bullet> b"
        using phi(1)
        by auto
      have rw: "{0..1} - ({0..1} \<inter> \<phi> -` ?s' \<union> ?s \<union> {0, 1}) =
            {0..1} - (\<phi> -` ?s' \<union> ?s \<union> {0, 1})"
        by auto
      show ?thesis 
        using i[unfolded rw] ii[unfolded rw]  by metis
    qed
    have **: "negligible ({0..1} \<inter> ((\<phi> -` ?s') \<union> ?s \<union> {0, 1}))"
      using s(1) finite_neg_img
      by (simp add: Int_Un_distrib negligible_finite)
    then have has_integral_spike_eq': "(f has_integral y) t = (g has_integral y) t"
      if "negligible (t \<inter> s)" "\<forall>x\<in>t - s. g x = f x"
      for s::"'b::euclidean_space set" and t and g::"'b \<Rightarrow> 'c :: real_normed_vector" and f y
    proof-
      have "\<forall>x\<in>t - (t \<inter> s). g x = f x \<Longrightarrow> (f has_integral y) t = (g has_integral y) t"
        using has_integral_spike_eq that by metis
      then show ?thesis using that by blast
    qed
    show ?thesis
      using has_integral_spike_eq'[OF ** *] ass
      by blast
  qed
  then show "line_integral_exists F {b} \<gamma>1"
    using phi  by(auto simp add: line_integral_exists_def)
  have "integral ({0..1}) (\<lambda>x. F (\<gamma>2 (\<phi> x)) \<bullet> b * (vector_derivative (\<gamma>2 \<circ> \<phi>) (at x within {0..1}) \<bullet> b)) =
                      integral ({0..1}) (\<lambda>x. F (\<gamma>1 x) \<bullet> b * (vector_derivative \<gamma>1  (at x within {0..1}) \<bullet> b))"
    using integral_unique[OF e] integral_unique[OF f]
    by metis
  moreover have "integral ({0..1}) (\<lambda>x. F (\<gamma>2 (\<phi> x)) \<bullet> b * (vector_derivative (\<gamma>2 \<circ> \<phi>) (at x within {0..1}) \<bullet> b)) =
                         integral ({0..1}) (\<lambda>x. F (\<gamma>2 x) \<bullet> b * (vector_derivative \<gamma>2 (at x within {0..1}) \<bullet> b))"
    using b d phi by (auto simp add:)
  ultimately show "line_integral F {b} \<gamma>1 = line_integral F {b} \<gamma>2"
    using phi  by(auto simp add: line_integral_def)
qed

lemma line_integral_sum_basis:
  assumes "finite (basis::('a::euclidean_space) set)"  "\<forall>b\<in>basis. line_integral_exists F {b} \<gamma>"
  shows "line_integral F basis \<gamma> = (\<Sum>b\<in>basis. line_integral F {b} \<gamma>)"
        "line_integral_exists F basis \<gamma>"
  using assms
proof(induction basis)
  show "line_integral F {} \<gamma> = (\<Sum>b\<in>{}. line_integral F {b} \<gamma>)"
    by(auto simp add: line_integral_def)
  show "\<forall>b\<in>{}. line_integral_exists F {b} \<gamma> \<Longrightarrow> line_integral_exists F {} \<gamma>"
    by(simp add: line_integral_exists_def integrable_0)
next
  fix basis::"('a::euclidean_space) set"
  fix x::"'a::euclidean_space"
  fix  \<gamma>
  assume ind_hyp: "(\<forall>b\<in>basis. line_integral_exists F {b} \<gamma> \<Longrightarrow> line_integral_exists F basis \<gamma>)"
    "(\<forall>b\<in>basis. line_integral_exists F {b} \<gamma> \<Longrightarrow> line_integral F basis \<gamma> = (\<Sum>b\<in>basis. line_integral F {b} \<gamma>))"
  assume step: "finite basis"
    "x \<notin> basis"
    "\<forall>b\<in>insert x basis. line_integral_exists F {b} \<gamma>"
  then have 0: "line_integral_exists F {x} \<gamma>" by auto
  have 1:"line_integral_exists F basis \<gamma>"
    using ind_hyp step by auto
  then show "line_integral_exists F (insert x basis) \<gamma>"
    using step(1) step(2) line_integral_sum_gen(2)[OF _ 0 1] by simp
  have 3: "finite (insert x basis)" using step(1) by auto
  have "line_integral F basis \<gamma> = (\<Sum>b\<in>basis. line_integral F {b} \<gamma>)"
    using ind_hyp step by auto
  then show "line_integral F (insert x basis) \<gamma> = (\<Sum>b\<in>insert x basis. line_integral F {b} \<gamma>)"
    using step(1) step(2) line_integral_sum_gen(1)[OF 3 0 1]
    by force
qed

lemma reparam_weak_eq_line_integrals_basis:
  assumes "reparam_weak \<gamma>1 \<gamma>2"
    "\<gamma>2 C1_differentiable_on {0..1}" (*To generalise this to valid_path we need a version of has_integral_substitution_strong that allows finite discontinuities*)
    "\<forall>b\<in>basis. continuous_on (path_image \<gamma>2) (\<lambda>x. F x \<bullet> b)"
    "finite basis"
  shows "line_integral F basis \<gamma>1 = line_integral F basis \<gamma>2"
    "line_integral_exists F basis \<gamma>1"
proof- 
  show "line_integral_exists F basis \<gamma>1"
    using reparam_weak_eq_line_integrals(2)[OF assms(1) assms(2)] assms(3-4) line_integral_sum_basis(2)[OF assms(4)]
    by(simp add: subset_iff)
  show "line_integral F basis \<gamma>1 = line_integral F basis \<gamma>2"
    using reparam_weak_eq_line_integrals[OF assms(1) assms(2)] assms(3-4) line_integral_sum_basis(1)[OF assms(4)]
      line_integral_exists_smooth_one_base[OF assms(2)]
    by(simp add: subset_iff)
qed

lemma reparam_eq_line_integrals_basis:
  assumes "reparam \<gamma>1 \<gamma>2"
    "\<gamma>2 piecewise_C1_differentiable_on {0..1}" 
    "\<forall>b\<in>basis. continuous_on (path_image \<gamma>2) (\<lambda>x. F x \<bullet> b)"
    "finite basis"
    "\<forall>b\<in>basis. line_integral_exists F {b} \<gamma>2" (*We need to remove this and work on special cases like conservative fields and field/line combinations that satisfy the improper integrals theorem assumptions*)
  shows "line_integral F basis \<gamma>1 = line_integral F basis \<gamma>2"
    "line_integral_exists F basis \<gamma>1"
proof- 
  show "line_integral_exists F basis \<gamma>1"
    using reparam_eq_line_integrals(2)[OF assms(1) assms(2)] assms(3-5) line_integral_sum_basis(2)[OF assms(4)]
    by(simp add: subset_iff)
  show "line_integral F basis \<gamma>1 = line_integral F basis \<gamma>2"
    using reparam_eq_line_integrals[OF assms(1) assms(2)] assms(3-5) line_integral_sum_basis(1)[OF assms(4)]
    by(simp add: subset_iff)
qed

lemma line_integral_exists_smooth:
  assumes "\<gamma> C1_differentiable_on {0..1}" (*To generalise this to valid_path we need a version of has_integral_substitution_strong that allows finite discontinuities*)
    "\<forall>(b::'a::euclidean_space) \<in>basis. continuous_on (path_image \<gamma>) (\<lambda>x. F x \<bullet> b)"
    "finite basis"
  shows "line_integral_exists F basis \<gamma>"
  using reparam_weak_eq_line_integrals_basis(2)[OF reparam_weak_eq_refl[where ?\<gamma>1.0 = \<gamma>]] assms
  by fastforce

lemma smooth_path_imp_reverse:
  assumes "g C1_differentiable_on {0..1}"
  shows "(reversepath g) C1_differentiable_on {0..1}"
  using assms continuous_on_const
  apply (auto simp: reversepath_def)
  apply (rule C1_differentiable_compose [of "\<lambda>x::real. 1-x" _ g, unfolded o_def])
    apply (auto simp: C1_differentiable_on_eq)
  apply (simp add: finite_vimageI inj_on_def)
  done

lemma piecewise_smooth_path_imp_reverse:
  assumes "g piecewise_C1_differentiable_on {0..1}"
  shows "(reversepath g) piecewise_C1_differentiable_on {0..1}"
  using assms valid_path_reversepath
  using valid_path_def by blast

definition chain_reparam_weak_chain where
  "chain_reparam_weak_chain one_chain1 one_chain2 \<equiv>
      \<exists>f. bij f \<and> f ` one_chain1 = one_chain2 \<and> (\<forall>(k,\<gamma>)\<in>one_chain1. if k = fst (f(k,\<gamma>)) then reparam_weak \<gamma> (snd (f(k,\<gamma>))) else reparam_weak \<gamma> (reversepath (snd (f(k,\<gamma>)))))"

lemma chain_reparam_weak_chain_line_integral:
  assumes "chain_reparam_weak_chain one_chain1 one_chain2"
    "\<forall>(k2,\<gamma>2)\<in>one_chain2. \<gamma>2 C1_differentiable_on {0..1}"
    "\<forall>(k2,\<gamma>2)\<in>one_chain2.\<forall>b\<in>basis. continuous_on (path_image \<gamma>2) (\<lambda>x. F x \<bullet> b)"
    "finite basis"                                                                     
  and bound1: "boundary_chain one_chain1"
  and bound2: "boundary_chain one_chain2"
  shows "one_chain_line_integral F basis one_chain1 = one_chain_line_integral F basis one_chain2"
    "\<forall>(k, \<gamma>)\<in>one_chain1. line_integral_exists F basis \<gamma>"
proof-
  obtain f where f: "bij f"
    "(\<forall>(k,\<gamma>)\<in>one_chain1. if k = fst (f(k,\<gamma>)) then reparam_weak \<gamma> (snd (f(k,\<gamma>))) else reparam_weak \<gamma> (reversepath (snd (f(k,\<gamma>)))))"
    "f ` one_chain1 = one_chain2"
    using assms(1)
    by (auto simp add: chain_reparam_weak_chain_def)
  have 0:" \<forall>x\<in>one_chain1. (case x of (k, \<gamma>) \<Rightarrow> (real_of_int k * line_integral F basis \<gamma>) = (case f x of (k, \<gamma>) \<Rightarrow> real_of_int k * line_integral F basis \<gamma>) \<and>
                                                        line_integral_exists F basis \<gamma>)"
  proof
    {fix k1 \<gamma>1
      assume ass1: "(k1,\<gamma>1) \<in>one_chain1"
      have "real_of_int k1 * line_integral F basis \<gamma>1 = (case (f (k1,\<gamma>1)) of (k2, \<gamma>2) \<Rightarrow> real_of_int k2 * line_integral F basis \<gamma>2) \<and>
                      line_integral_exists F basis \<gamma>1"
      proof(cases)
        assume ass2: "k1 = 1"
        let ?k2 = "fst (f (k1, \<gamma>1))"
        let ?\<gamma>2 = "snd (f (k1, \<gamma>1))"
        have "real_of_int k1 * line_integral F basis \<gamma>1 = real_of_int ?k2 * line_integral F basis ?\<gamma>2\<and>
                              line_integral_exists F basis \<gamma>1"
        proof(cases)
          assume ass3: "?k2 = 1"
          then have 0: "reparam_weak \<gamma>1 ?\<gamma>2"
            using ass1 ass2 f(2)
            by auto
          have 1: "?\<gamma>2 C1_differentiable_on {0..1}"
            using f(3) assms(2) ass1
            by force
          have 2: "\<forall>b\<in>basis. continuous_on (path_image ?\<gamma>2) (\<lambda>x. F x \<bullet> b)"
            using f(3) assms(3) ass1
            by force
          show "real_of_int k1 * line_integral F basis \<gamma>1 = real_of_int ?k2 * line_integral F basis ?\<gamma>2 \<and>
                                     line_integral_exists F basis \<gamma>1"
            using assms reparam_weak_eq_line_integrals_basis[OF 0 1 2 assms(4)]
              ass2 ass3
            by auto
        next
          assume "?k2 \<noteq> 1"
          then have ass3: "?k2 = -1"
            using bound2 ass1 f(3) unfolding boundary_chain_def by force
          then have 0: "reparam_weak \<gamma>1 (reversepath ?\<gamma>2)"
            using ass1 ass2 f(2)
            by auto
          have 1: "(reversepath ?\<gamma>2) C1_differentiable_on {0..1}"
            using f(3) assms(2) ass1 smooth_path_imp_reverse
            by force
          have 2: "\<forall>b\<in>basis. continuous_on (path_image (reversepath ?\<gamma>2)) (\<lambda>x. F x \<bullet> b)"
            using f(3) assms(3) ass1 path_image_reversepath
            by force
          have 3: "line_integral F basis ?\<gamma>2 = - line_integral F basis (reversepath ?\<gamma>2)"
          proof-
            have i:"valid_path (reversepath ?\<gamma>2) "
              using 1 C1_differentiable_imp_piecewise
              by (auto simp add: valid_path_def)
            show ?thesis 
              using line_integral_on_reverse_path(1)[OF i line_integral_exists_smooth[OF 1 2 ]] assms
              by auto
          qed
          show "real_of_int k1 * line_integral F basis \<gamma>1 = real_of_int ?k2 * line_integral F basis ?\<gamma>2 \<and>
                                     line_integral_exists F basis \<gamma>1"
            using assms reparam_weak_eq_line_integrals_basis[OF 0 1 2 assms(4)]
              ass2 ass3 3
            by (auto  simp del: one_chain_line_integral_mult_left line_integral_mult_left line_integral_empty_basis one_chain_line_integral_empty_basis one_chain_line_integral_empty_chain)
        qed
        then show "real_of_int k1 * line_integral F basis \<gamma>1 = (case f (k1, \<gamma>1) of (k2, \<gamma>2) \<Rightarrow> real_of_int k2 * line_integral F basis \<gamma>2) \<and>
                                   line_integral_exists F basis \<gamma>1"
          by (simp add: case_prod_beta')
      next
        assume "k1 \<noteq> 1"
        then have ass2: "k1 = -1"
          using bound1 ass1 f(3) unfolding boundary_chain_def by force
        let ?k2 = "fst (f (k1, \<gamma>1))"
        let ?\<gamma>2 = "snd (f (k1, \<gamma>1))"
        have "real_of_int k1 * line_integral F basis \<gamma>1 = real_of_int ?k2 * line_integral F basis ?\<gamma>2 \<and>
                              line_integral_exists F basis \<gamma>1"
        proof(cases)
          assume ass3: "?k2 = 1"
          then have 0: "reparam_weak \<gamma>1 (reversepath ?\<gamma>2)"
            using ass1 ass2 f(2)
            by auto
          have 1: "(reversepath ?\<gamma>2) C1_differentiable_on {0..1}"
            using f(3) assms(2) ass1 smooth_path_imp_reverse
            by force
          have 2: "\<forall>b\<in>basis. continuous_on (path_image (reversepath ?\<gamma>2)) (\<lambda>x. F x \<bullet> b)"
            using f(3) assms(3) ass1 path_image_reversepath
            by force
          have 3: "line_integral F basis ?\<gamma>2 = - line_integral F basis (reversepath ?\<gamma>2)"
          proof-
            have i:"valid_path (reversepath ?\<gamma>2) "
              using 1 C1_differentiable_imp_piecewise
              by (auto simp add: valid_path_def)
            show ?thesis 
              using line_integral_on_reverse_path(1)[OF i line_integral_exists_smooth[OF 1 2 assms(4)]]
              by auto
          qed
          show "real_of_int k1 * line_integral F basis \<gamma>1 = real_of_int ?k2 * line_integral F basis ?\<gamma>2 \<and>
                                     line_integral_exists F basis \<gamma>1"
            using assms reparam_weak_eq_line_integrals_basis[OF 0 1 2 assms(4)]
              ass2 ass3 3
            by (auto  simp del: one_chain_line_integral_mult_left line_integral_mult_left line_integral_empty_basis one_chain_line_integral_empty_basis one_chain_line_integral_empty_chain)
        next
          assume "?k2 \<noteq> 1"
          then have ass3: "?k2 = -1"
            using bound2 ass1 f(3) unfolding boundary_chain_def by force
          then have 0: "reparam_weak \<gamma>1 ?\<gamma>2"
            using ass1 ass2 f(2)  by auto
          have 1: "?\<gamma>2 C1_differentiable_on {0..1}"
            using f(3) assms(2) ass1  by force
          have 2: "\<forall>b\<in>basis. continuous_on (path_image ?\<gamma>2) (\<lambda>x. F x \<bullet> b)"
            using f(3) assms(3) ass1  by force
          show "real_of_int k1 * line_integral F basis \<gamma>1 = real_of_int ?k2 * line_integral F basis ?\<gamma>2 \<and>
                                     line_integral_exists F basis \<gamma>1"
            using assms reparam_weak_eq_line_integrals_basis[OF 0 1 2 assms(4)]  ass2 ass3
            by auto
        qed
        then show "real_of_int k1 * line_integral F basis \<gamma>1 = (case f (k1, \<gamma>1) of (k2, \<gamma>2) \<Rightarrow> real_of_int k2 * line_integral F basis \<gamma>2) \<and>
                                   line_integral_exists F basis \<gamma>1"
          by (simp add: case_prod_beta')
      qed
    }
    then show "\<And>x. x \<in> one_chain1 \<Longrightarrow>
                                (case x of (k, \<gamma>) \<Rightarrow> (real_of_int k * line_integral F basis \<gamma>) = (case f x of (k, \<gamma>) \<Rightarrow> real_of_int k * line_integral F basis \<gamma>) \<and>
                                                        line_integral_exists F basis \<gamma>)"
      by (auto simp add: case_prod_beta')
  qed
  show "one_chain_line_integral F basis one_chain1 = one_chain_line_integral F basis one_chain2"
    using 0 by(simp add: one_chain_line_integral_def sum_bij[OF f(1) _ f(3)] split_beta)
  show "\<forall>(k, \<gamma>)\<in>one_chain1. line_integral_exists F basis \<gamma>"
    using 0 by blast
qed

definition chain_reparam_path where
   "chain_reparam_path \<gamma> one_chain \<equiv> \<exists>\<gamma>'. chain_subdiv_path \<gamma>' one_chain \<and> reparam \<gamma> \<gamma>'"
                                                   
lemma chain_reparam_path_finite:
  assumes "chain_reparam_path \<gamma> one_chain"
  shows "finite one_chain"
  using assms
  unfolding chain_reparam_path_def using chain_subdiv_path_finite by auto

lemma continuous_on_closed_UN:
  assumes "finite S"
  shows "((\<And>s. s \<in> S \<Longrightarrow> closed s) \<Longrightarrow> (\<And>s. s \<in> S \<Longrightarrow> continuous_on s f) \<Longrightarrow> continuous_on (\<Union>S) f)"
  using assms
proof(induction S)
  case empty
  then show ?case by auto
next
  case (insert x F)
  then show ?case using continuous_on_closed_Un closed_Union
    by (simp add: closed_Union continuous_on_closed_Un)
qed

lemma reparam_path_image:
  assumes "reparam \<gamma>1 \<gamma>2"
  shows "path_image \<gamma>1 = path_image \<gamma>2"
  using assms
  apply (auto simp add: reparam_def path_image_def image_def bij_betw_def)
  apply (force dest!: equalityD2)
  done

lemma path_image_coeff_cube_to_path: "path_image (coeff_cube_to_path (k, \<gamma>)) = (path_image \<gamma>)"
  by auto

lemma chain_subdiv_path_pathimg_subset_1:
  assumes "chain_subdiv_path \<gamma> subdiv"
  shows "\<forall>(k',\<gamma>')\<in>subdiv. (path_image \<gamma>') \<subseteq> path_image \<gamma>"
  using assms
proof(induct rule: chain_subdiv_path.induct)
  case (singleton \<gamma>)
  then show ?case by auto
next
  case (insert \<gamma> s \<gamma>0)
  then show ?case
    apply(subst path_image_join)
    using path_image_coeff_cube_to_path
    by auto
qed

lemma chain_subdiv_path_pathimg_subset_2:
  assumes "chain_subdiv_path \<gamma> subdiv"
  shows "path_image \<gamma> \<subseteq> (\<Union>(k',\<gamma>')\<in>subdiv. (path_image \<gamma>'))"
  using assms
proof(induct rule: chain_subdiv_path.induct)
  case (singleton \<gamma>)
  then show ?case 
    using path_image_coeff_cube_to_path by auto
next
  case (insert \<gamma> s \<gamma>0)
  show ?case
    apply(subst path_image_join)
    subgoal using insert by auto
    subgoal unfolding UN_insert
      apply(rule Un_mono)
      subgoal using path_image_coeff_cube_to_path by auto
      subgoal using insert by auto
      done
    done
qed

lemma chain_subdiv_path_eq_img:
  assumes "chain_subdiv_path \<gamma> subdiv"
      shows "path_image \<gamma> = (\<Union>(k',\<gamma>')\<in>subdiv. (path_image \<gamma>'))"
  using chain_subdiv_path_pathimg_subset_1[OF assms] chain_subdiv_path_pathimg_subset_2[OF assms]
  by blast

lemma chain_reparam_path_line_integral':
  assumes path_eq_chain: "chain_reparam_path \<gamma> one_chain" and
    boundary_chain: "boundary_chain one_chain" and
    line_integral_exists: "\<forall>b\<in>basis. \<forall>(k::int, \<gamma>) \<in> one_chain. line_integral_exists F {b} \<gamma>" and
    valid_path: "\<forall>(k::int, \<gamma>) \<in> one_chain. valid_path \<gamma>" and
    finite_basis: "finite basis" and 
    cont: "\<forall>b\<in>basis. \<forall>(k,\<gamma>2)\<in>one_chain. continuous_on (path_image \<gamma>2) (F\<^bsub>b\<^esub>)"
  shows "\<^sup>H\<integral>\<^bsub>one_chain\<^esub> F \<downharpoonright>\<^bsub>basis\<^esub>  = \<integral>\<^bsub>\<gamma>\<^esub> F \<downharpoonright>\<^bsub>basis\<^esub>"
    "line_integral_exists F basis \<gamma>"
    (*"valid_path \<gamma>" This cannot be proven, see the crappy assumption of derivs/eq_pw_smooth :( *)
proof-
  have integral_exists: "line_integral_exists F basis \<gamma>" if "(k, \<gamma>)\<in>one_chain" for k \<gamma>
      apply(rule line_integral_summation)
      using assms that by auto
  moreover obtain \<gamma>' where gamma': "chain_subdiv_path \<gamma>' one_chain" "reparam \<gamma> \<gamma>'"
    using path_eq_chain
    unfolding chain_reparam_path_def
    by auto
  ultimately have "\<^sup>H\<integral>\<^bsub>one_chain\<^esub> F \<downharpoonright>\<^bsub>basis\<^esub>  = \<integral>\<^bsub>\<gamma>'\<^esub> F \<downharpoonright>\<^bsub>basis\<^esub>"
    apply(intro line_integral_on_path_eq_line_integral_on_equiv_chain_1')
    using assms by auto
  moreover have "\<integral>\<^bsub>\<gamma>\<^esub> F \<downharpoonright>\<^bsub>basis\<^esub> = \<integral>\<^bsub>\<gamma>'\<^esub> F \<downharpoonright>\<^bsub>basis\<^esub>" (is ?g1) "line_integral_exists F basis \<gamma>" (is ?g2)
  proof-
    have "reparam \<gamma> \<gamma>'" using gamma' by auto
    moreover have "\<gamma>' piecewise_C1_differentiable_on {0..1}" using chain_subdiv_path_valid_path[OF gamma'(1) valid_path]
      unfolding valid_path_def by simp
    moreover have "\<forall>b\<in>basis. continuous_on (path_image \<gamma>') (F\<^bsub>b\<^esub>)"
    proof-
      have cont_forall: "\<forall>b\<in>basis. continuous_on (\<Union>(k, \<gamma>)\<in>one_chain. path_image \<gamma>) (\<lambda>x. F x \<bullet> b)"
      proof
        fix b
        assume ass: "b \<in> basis"
        have "continuous_on (\<Union>((path_image \<circ> snd) ` one_chain))  (\<lambda>x. F x \<bullet> b)"
          apply(rule continuous_on_closed_UN[where ?S = "(path_image o snd) ` one_chain" ])
        proof
          show "finite one_chain" using chain_reparam_path_finite[OF path_eq_chain] by auto
          show "\<And>s. s \<in> (path_image \<circ> snd) ` one_chain \<Longrightarrow> closed s"
            using  closed_valid_path_image[OF] valid_path  
            by fastforce
          show "\<And>s. s \<in> (path_image \<circ> snd) ` one_chain \<Longrightarrow> continuous_on s (\<lambda>x. F x \<bullet> b)"
            using cont ass by force
        qed
        then show "continuous_on (\<Union>(k, \<gamma>)\<in>one_chain. path_image \<gamma>) (\<lambda>x. F x \<bullet> b)"
          by (auto simp add: Union_eq case_prod_beta)
      qed
      then show "\<forall>b\<in>basis. continuous_on (path_image \<gamma>') (F\<^bsub>b\<^esub>)"
        using chain_subdiv_path_eq_img[OF gamma'(1)] cont continuous_on_subset
        by auto
    qed
    moreover have "\<forall>b\<in>basis. line_integral_exists F {b} \<gamma>'"
    proof(rule ballI)
      fix b assume ass: "b \<in> basis"
      show "line_integral_exists F {b} \<gamma>'"
        apply(rule line_integral_on_path_eq_line_integral_on_equiv_chain_1')
      proof-
        show "\<forall>(k, \<gamma>)\<in>one_chain. line_integral_exists F {b} \<gamma>"
          using assms ass
          by blast
      qed(auto simp add: assms gamma')
    qed
    ultimately show ?g1 ?g2
      using reparam_eq_line_integrals_basis assms integral_exists
      by auto
  qed
  ultimately show "\<^sup>H\<integral>\<^bsub>one_chain\<^esub> F \<downharpoonright>\<^bsub>basis\<^esub> = \<integral>\<^bsub>\<gamma>\<^esub> F \<downharpoonright>\<^bsub>basis\<^esub>" "line_integral_exists F basis \<gamma>"
    by auto
qed

definition chain_reparam_chain where
  "chain_reparam_chain one_chain1 subdiv
       \<equiv> \<exists>f. ((\<Union>(f ` one_chain1)) = subdiv) \<and>
              (\<forall>cube \<in> one_chain1. chain_reparam_path (coeff_cube_to_path cube) (f cube)) \<and>
              pairwise (\<lambda>p p'. f p \<inter> f p' = {}) one_chain1"

lemma chain_reparam_chain_imp_finite_subdiv:
  assumes "finite one_chain1"
    "chain_reparam_chain one_chain1 subdiv"
  shows "finite subdiv"
  using assms chain_reparam_path_finite
  by(auto simp add: chain_reparam_chain_def)

lemma chain_reparam_chain_line_integral:
  assumes chain1_eq_chain2: "chain_reparam_chain one_chain1 subdiv" and
    boundary_chain1: "boundary_chain one_chain1" and
    boundary_chain2: "boundary_chain subdiv" and
    line_integral_exists_on_chain2: "\<forall>b\<in>basis. \<forall>(k::int, \<gamma>) \<in> subdiv. line_integral_exists F {b} \<gamma>" and
    valid_path: "\<forall>(k, \<gamma>) \<in> subdiv. valid_path \<gamma>" and
    valid_path_2: "\<forall>(k, \<gamma>) \<in> one_chain1. valid_path \<gamma>" and
    finite_chain1: "finite one_chain1" and
    finite_basis: "finite basis" and
    cont_field: " \<forall>b\<in>basis. \<forall>(k, \<gamma>2)\<in>subdiv. continuous_on (path_image \<gamma>2) (\<lambda>x. F x \<bullet> b)"
  shows "one_chain_line_integral F basis one_chain1 = one_chain_line_integral F basis subdiv"
    "\<forall>(k, \<gamma>) \<in> one_chain1. line_integral_exists F basis \<gamma>"
proof -
  obtain f where f_props:
    "((\<Union>(f ` one_chain1)) = subdiv)"
    "(\<forall>cube \<in> one_chain1. chain_reparam_path (coeff_cube_to_path cube) (f cube))"
    "(\<forall>p\<in>one_chain1. \<forall>p'\<in>one_chain1. p \<noteq> p' \<longrightarrow> f p \<inter> f p' = {})"
    using chain1_eq_chain2 
    by (auto simp add: chain_reparam_chain_def pairwise_def)
  then have 0: "one_chain_line_integral F basis subdiv = one_chain_line_integral F basis (\<Union>(f ` one_chain1))"
    by auto
  {fix k \<gamma>
    assume ass:"(k, \<gamma>) \<in> one_chain1"
    have "line_integral_exists F basis \<gamma> \<and>
               one_chain_line_integral F basis (f (k, \<gamma>)) = k * line_integral F basis \<gamma>"
    proof(cases "k = 1")
      assume k_eq_1: "k = 1"
      then have i:"chain_reparam_path \<gamma> (f(k,\<gamma>))"
        using f_props(2) ass by auto
      have ii:"boundary_chain (f(k,\<gamma>))"
        using f_props(1) ass assms  unfolding boundary_chain_def
        by blast
      have iii:"\<forall>b\<in>basis. \<forall>(k, \<gamma>)\<in>f (k, \<gamma>). line_integral_exists F {b} \<gamma>"
        using f_props(1) ass assms
        by blast
      have "iv": " \<forall>(k, \<gamma>)\<in>f (k, \<gamma>). valid_path \<gamma>"
        using f_props(1) ass assms
        by blast
      have v: "\<forall>b\<in>basis. \<forall>(k, \<gamma>2)\<in>f (k, \<gamma>). continuous_on (path_image \<gamma>2) (\<lambda>x. F x \<bullet> b)"
        using f_props(1) ass assms
        by blast        
      have "line_integral_exists F basis \<gamma> \<and> one_chain_line_integral F basis (f (k, \<gamma>)) = line_integral F basis \<gamma>"
        using chain_reparam_path_line_integral'[OF i ii iii iv finite_basis v] ass 
        by auto
      then show "line_integral_exists F basis \<gamma> \<and> one_chain_line_integral F basis (f (k, \<gamma>)) = k * line_integral F basis \<gamma>"
        using k_eq_1  by auto
    next
      assume "k \<noteq> 1"
      then have k_eq_neg1: "k = -1"
        using ass boundary_chain1
        by (auto simp add: boundary_chain_def) 
      then have i:"chain_reparam_path (reversepath \<gamma>) (f(k,\<gamma>))"
        using f_props(2) ass by auto
      have ii:"boundary_chain (f(k,\<gamma>))"
        using f_props(1) ass assms unfolding boundary_chain_def
        by blast
      have iii:"\<forall>b\<in>basis. \<forall>(k, \<gamma>)\<in>f (k, \<gamma>). line_integral_exists F {b} \<gamma>"
        using f_props(1) ass assms
        by blast
      have "iv": " \<forall>(k, \<gamma>)\<in>f (k, \<gamma>). valid_path \<gamma>"
        using f_props(1) ass assms by blast
      have v: "\<forall>b\<in>basis. \<forall>(k, \<gamma>2)\<in>f (k, \<gamma>). continuous_on (path_image \<gamma>2) (\<lambda>x. F x \<bullet> b)"
        using f_props(1) ass assms  by blast        
      have x:"line_integral_exists F basis (reversepath \<gamma>) \<and> one_chain_line_integral F basis (f (k, \<gamma>)) = line_integral F basis (reversepath \<gamma>)"
        using chain_reparam_path_line_integral'[OF i ii iii iv finite_basis v] ass
        by auto
      have "valid_path (reversepath \<gamma>)"
        using f_props(1) ass assms  by auto
      then have "line_integral_exists F basis \<gamma> \<and> one_chain_line_integral F basis (f (k, \<gamma>)) = - (line_integral F basis \<gamma>)"
        using line_integral_on_reverse_path reversepath_reversepath x ass
        by metis
      then show "line_integral_exists F basis \<gamma> \<and> one_chain_line_integral F basis (f (k::int, \<gamma>)) = k * line_integral F basis \<gamma>"
        using k_eq_neg1  by (auto simp del: one_chain_line_integral_mult_left line_integral_mult_left line_integral_empty_basis one_chain_line_integral_empty_basis one_chain_line_integral_empty_chain)
    qed}
  note cube_line_integ = this
  have finite_chain2: "finite subdiv" 
    using chain_reparam_chain_imp_finite_subdiv chain1_eq_chain2 finite_chain1  by auto
  show line_integral_exists_on_chain1: "\<forall>(k, \<gamma>) \<in> one_chain1. line_integral_exists F basis \<gamma>"
    using cube_line_integ by auto
  have 1: "one_chain_line_integral F basis (\<Union>(f ` one_chain1)) = one_chain_line_integral F basis one_chain1"
  proof -
    have 0:"one_chain_line_integral F basis (\<Union>(f ` one_chain1)) = 
                           (\<Sum>one_chain \<in> (f ` one_chain1). one_chain_line_integral F basis one_chain)"
    proof -
      have finite: "\<forall>chain \<in> (f ` one_chain1). finite chain"
        using f_props(1) finite_chain2 
        by (meson Sup_upper finite_subset)
      have disj: " \<forall>A\<in>f ` one_chain1. \<forall>B\<in>f ` one_chain1. A \<noteq> B \<longrightarrow> A \<inter> B = {}"
        apply(simp add:image_def)
        using f_props(3)
        by metis
      show "one_chain_line_integral F basis (\<Union>(f ` one_chain1)) = 
                                (\<Sum>one_chain \<in> (f ` one_chain1). one_chain_line_integral F basis one_chain)"
        using Groups_Big.comm_monoid_add_class.sum.Union_disjoint[OF finite disj]
          one_chain_line_integral_def
        by auto
    qed
    have 1:"(\<Sum>one_chain \<in> (f ` one_chain1). one_chain_line_integral F basis one_chain) = 
                            one_chain_line_integral F basis one_chain1"
    proof -
      have "(\<Sum>one_chain \<in> (f ` one_chain1). one_chain_line_integral F basis one_chain) = 
                                     (\<Sum>(k,\<gamma>)\<in>one_chain1. k*(line_integral F basis \<gamma>))"
      proof -
        have i:"(\<Sum>one_chain \<in> (f ` (one_chain1 - {p. f p = {}})). one_chain_line_integral F basis one_chain) = 
                                       (\<Sum>(k,\<gamma>)\<in>one_chain1 - {p. f p = {}}. k*(line_integral F basis \<gamma>))"
        proof -
          have "inj_on f (one_chain1 - {p. f p = {}})"
            unfolding inj_on_def
            using f_props(3) by force
          then have 0: "(\<Sum>one_chain \<in> (f ` (one_chain1 - {p. f p = {}})). one_chain_line_integral F basis one_chain)
                                                       = (\<Sum>(k, \<gamma>) \<in> (one_chain1 - {p. f p = {}}). one_chain_line_integral F basis (f (k, \<gamma>)))"
            using Groups_Big.comm_monoid_add_class.sum.reindex
            by auto
          have "\<And> k \<gamma>. (k, \<gamma>) \<in> (one_chain1 - {p. f p = {}}) \<Longrightarrow>
                                                     one_chain_line_integral F basis (f(k, \<gamma>)) = k* (line_integral F basis \<gamma>)"
            using cube_line_integ by auto
          then have "(\<Sum>(k, \<gamma>) \<in> (one_chain1 - {p. f p = {}}). one_chain_line_integral F basis (f (k, \<gamma>)))
                   = (\<Sum>(k, \<gamma>) \<in> (one_chain1 - {p. f p = {}}). k* (line_integral F basis \<gamma>))"
            by (auto intro!: Finite_Cartesian_Product.sum_cong_aux)
          then show "(\<Sum>one_chain \<in> (f ` (one_chain1 - {p. f p = {}})). one_chain_line_integral F basis one_chain) =
                                                     (\<Sum>(k, \<gamma>) \<in> (one_chain1 - {p. f p = {}}). k* (line_integral F basis \<gamma>))"
            using 0  by auto
        qed
        have "\<And> (k::int) \<gamma>. (k, \<gamma>) \<in> one_chain1 \<Longrightarrow> (f (k, \<gamma>) = {}) \<Longrightarrow> (k, \<gamma>) \<in> {(k',\<gamma>'). k'* (line_integral F basis \<gamma>') = 0}"
        proof-
          fix k::"int"
          fix \<gamma>::"real\<Rightarrow>real*real"
          assume ass:"(k, \<gamma>) \<in> one_chain1"
            "(f (k, \<gamma>) = {})"
          then have zero_line_integral:"one_chain_line_integral F basis (f (k, \<gamma>)) = 0"
            using one_chain_line_integral_def
            by auto
          show "(k, \<gamma>) \<in> {(k'::int, \<gamma>'). k' * line_integral F basis \<gamma>' = 0}"
            using zero_line_integral cube_line_integ ass
            by force
        qed
        then have ii:"(\<Sum>one_chain \<in> (f ` (one_chain1 - {p. f p = {}})). one_chain_line_integral F basis one_chain) =
                                                          (\<Sum>one_chain \<in> (f ` (one_chain1)). one_chain_line_integral F basis one_chain)"
        proof -
          have "\<And>one_chain. one_chain \<in> (f ` (one_chain1)) - (f ` (one_chain1 - {p. f p = {}})) \<Longrightarrow>
                                                         one_chain_line_integral F basis one_chain = 0"
          proof -
            fix one_chain
            assume "one_chain \<in> (f ` (one_chain1)) - (f ` (one_chain1 - {p. f p = {}}))"
            then have "one_chain = {}"
              by auto
            then show "one_chain_line_integral F basis one_chain = 0"
              by (auto simp add: one_chain_line_integral_def)
          qed
          then have 0:"(\<Sum>one_chain \<in> f ` (one_chain1) - (f ` (one_chain1 - {p. f p = {}})). one_chain_line_integral F basis one_chain) 
                                                       = 0"
            using Groups_Big.comm_monoid_add_class.sum.neutral
            by auto
          then have "(\<Sum>one_chain \<in> f ` (one_chain1). one_chain_line_integral F basis one_chain)
                                                      - (\<Sum>one_chain \<in> (f ` (one_chain1 - {p. f p = {}})). one_chain_line_integral F basis one_chain) 
                                                       = 0"
          proof -
            have finte: "finite (f ` one_chain1)" using finite_chain1 by auto
            show ?thesis
              using Groups_Big.sum_diff[OF finte, of " (f ` (one_chain1 - {p. f p = {}}))"
                  "one_chain_line_integral F basis"]
                0
              by auto
          qed                                           
          then show "(\<Sum>one_chain \<in> (f ` (one_chain1 - {p. f p = {}})). one_chain_line_integral F basis one_chain) =
                                                          (\<Sum>one_chain \<in> (f ` (one_chain1)). one_chain_line_integral F basis one_chain)"
            by auto
        qed
        have "\<And> (k::int) \<gamma>. (k, \<gamma>) \<in> one_chain1 \<Longrightarrow> (f (k, \<gamma>) = {}) \<Longrightarrow> f (k, \<gamma>) \<in> {chain. one_chain_line_integral F basis chain = 0}"
        proof-
          fix k::"int"
          fix \<gamma>::"real\<Rightarrow>real*real"
          assume ass:"(k, \<gamma>) \<in> one_chain1"  "(f (k, \<gamma>) = {})"
          then have "one_chain_line_integral F basis (f (k, \<gamma>)) = 0"
            using one_chain_line_integral_def
            by auto
          then show "f (k, \<gamma>) \<in> {p'. (one_chain_line_integral F basis p' = 0)}"
            by auto
        qed
        then have iii:"(\<Sum>(k::int,\<gamma>)\<in>one_chain1 - {p. f p = {}}. k*(line_integral F basis \<gamma>))
                                                 = (\<Sum>(k::int,\<gamma>)\<in>one_chain1. k*(line_integral F basis \<gamma>))"
        proof-
          have "\<And> k \<gamma>. (k,\<gamma>)\<in>one_chain1 - (one_chain1 - {p. f p = {}})
                                                    \<Longrightarrow> k* (line_integral F basis \<gamma>) = 0"
          proof-
            fix k \<gamma>
            assume ass: "(k,\<gamma>)\<in>one_chain1 - (one_chain1 - {p. f p = {}})"
            then have "f(k, \<gamma>) = {}"
              by auto
            then have "one_chain_line_integral F basis (f(k, \<gamma>)) = 0"
              by (auto simp add: one_chain_line_integral_def)
            then have zero_line_integral:"one_chain_line_integral F basis (f (k, \<gamma>)) = 0"
              using one_chain_line_integral_def
              by auto
            then show "k* (line_integral F basis \<gamma>) = 0"
              using zero_line_integral cube_line_integ ass
              by force
          qed
          then have "\<forall>(k::int,\<gamma>)\<in>one_chain1 - (one_chain1 - {p. f p = {}}).
                       k* (line_integral F basis \<gamma>) = 0" by auto
          then have "(\<Sum>(k::int,\<gamma>)\<in>one_chain1 - (one_chain1 - {p. f p = {}}). k*(line_integral F basis \<gamma>)) = 0"
            using Groups_Big.comm_monoid_add_class.sum.neutral
              [of "one_chain1 - (one_chain1 - {p. f p = {}})" "(\<lambda>(k::int,\<gamma>). k* (line_integral F basis \<gamma>))"]
            by (simp add: split_beta)
          then have "(\<Sum>(k::int,\<gamma>)\<in>one_chain1. k*(line_integral F basis \<gamma>)) -
                                                    (\<Sum>(k::int,\<gamma>)\<in> (one_chain1 - {p. f p = {}}). k*(line_integral F basis \<gamma>))  = 0"
            using Groups_Big.sum_diff[OF finite_chain1]
            by (metis (no_types) Diff_subset \<open>(\<Sum>(k, \<gamma>)\<in>one_chain1 - (one_chain1 - {p. f p = {}}). k * line_integral F basis \<gamma>) = 0\<close> \<open>\<And>f B. B \<subseteq> one_chain1 \<Longrightarrow> sum f (one_chain1 - B) = sum f one_chain1 - sum f B\<close>)
          then show ?thesis by auto
        qed
        show ?thesis using i ii iii by auto
      qed
      then show ?thesis using one_chain_line_integral_def by auto
    qed                    
    show ?thesis using 0 1 by auto
  qed
  show "one_chain_line_integral F basis one_chain1 = one_chain_line_integral F basis subdiv" using 0 1 by auto
qed

lemma chain_reparam_chain_line_integral_smooth_cubes:
  assumes "chain_reparam_chain one_chain1 one_chain2"
    "\<forall>(k2,\<gamma>2)\<in>one_chain2. \<gamma>2 C1_differentiable_on {0..1}"
    "\<forall>b\<in>basis.\<forall>(k2,\<gamma>2)\<in>one_chain2. continuous_on (path_image \<gamma>2) (\<lambda>x. F x \<bullet> b)"
    "finite basis"                                                                     
    "finite one_chain1"
    "boundary_chain one_chain1"
    "boundary_chain one_chain2"
    "\<forall>(k,\<gamma>)\<in>one_chain1. valid_path \<gamma>"
  shows "one_chain_line_integral F basis one_chain1 = one_chain_line_integral F basis one_chain2"
    "\<forall>(k, \<gamma>)\<in>one_chain1. line_integral_exists F basis \<gamma>"
proof-
  {fix b
    assume "b \<in> basis"
    fix k \<gamma>
    assume "(k, \<gamma>)\<in>one_chain2"
    have "line_integral_exists F {b} \<gamma>"
      apply(rule line_integral_exists_smooth)
      using \<open>(k, \<gamma>) \<in> one_chain2\<close> assms(2) apply blast
      using assms
      using \<open>(k, \<gamma>) \<in> one_chain2\<close> \<open>b \<in> basis\<close> apply blast
      using \<open>b \<in> basis\<close> by blast}
  then have a: "\<forall>b\<in>basis. \<forall>(k, \<gamma>)\<in>one_chain2. line_integral_exists F {b} \<gamma>"
    by auto
  have b: "\<forall>(k2,\<gamma>2)\<in>one_chain2. valid_path \<gamma>2"
    using assms(2)
    by (simp add: C1_differentiable_imp_piecewise case_prod_beta valid_path_def)              
  show "one_chain_line_integral F basis one_chain1 = one_chain_line_integral F basis one_chain2"
       "\<forall>(k, \<gamma>)\<in>one_chain1. line_integral_exists F basis \<gamma>"
    by (rule chain_reparam_chain_line_integral[OF assms(1) assms(6) assms(7) a b assms(8) assms(5) assms(4) assms(3)])+
qed

lemma chain_reparam_path_pathimg_subset:
  assumes "chain_reparam_path \<gamma> subdiv"
  shows "\<forall>(k',\<gamma>')\<in>subdiv. (path_image \<gamma>') \<subseteq> path_image \<gamma>"
  using assms
  apply(auto simp add: chain_reparam_path_def)
  using reparam_path_image chain_subdiv_path_pathimg_subset_1
  by fastforce

lemma chain_reparam_chain_pathimg_subset':
  assumes "chain_reparam_chain one_chain subdiv"
  assumes "(k,\<gamma>)\<in> subdiv"
  shows " \<exists>k' \<gamma>'. (k',\<gamma>')\<in> one_chain \<and> path_image \<gamma> \<subseteq> path_image \<gamma>'"
  using assms chain_reparam_path_pathimg_subset
  apply(auto simp add: chain_reparam_chain_def set_eq_iff)
  using  path_image_reversepath case_prodE case_prodD old.prod.exhaust
  apply (simp add: list.distinct(1) list.inject)
  by (smt case_prodD coeff_cube_to_path.simps reversepath_simps(2) surj_pair)

lemma chain_subdiv_chain_pathimg_subset':
  assumes "chain_subdiv_chain one_chain subdiv"
  assumes "(k,\<gamma>)\<in> subdiv"
  shows " \<exists>k' \<gamma>'. (k',\<gamma>')\<in> one_chain \<and> path_image \<gamma> \<subseteq> path_image \<gamma>'"
  using assms unfolding chain_subdiv_chain_def  pairwise_def
  apply auto
  using chain_subdiv_path_pathimg_subset_1 path_image_coeff_cube_to_path by fastforce

lemma chain_subdiv_chain_pathimg_subset:
  assumes "chain_subdiv_chain one_chain subdiv"
  shows "\<Union> (path_image ` {\<gamma>. \<exists>k. (k,\<gamma>)\<in> subdiv}) \<subseteq> \<Union> (path_image ` {\<gamma>. \<exists>k. (k,\<gamma>)\<in> one_chain})"
  using assms unfolding chain_subdiv_chain_def pairwise_def
  apply auto
  by (metis UN_iff assms chain_subdiv_chain_pathimg_subset' subsetCE case_prodI2) 

 definition common_reparam_exists:: "(int \<times> (real \<Rightarrow> real \<times> real)) set \<Rightarrow> (int \<times> (real \<Rightarrow> real \<times> real)) set \<Rightarrow> bool" where
    "common_reparam_exists one_chain1 one_chain2 \<equiv>
    (\<exists>subdiv ps1 ps2.
    chain_reparam_chain (one_chain1 - ps1) subdiv \<and>
    chain_reparam_chain (one_chain2 - ps2) subdiv \<and>
    (\<forall>(k, \<gamma>)\<in>subdiv.  \<gamma> C1_differentiable_on {0..1}) \<and>
    boundary_chain subdiv \<and>
    (\<forall>(k, \<gamma>)\<in>ps1. point_path \<gamma>) \<and>
    (\<forall>(k, \<gamma>)\<in>ps2. point_path \<gamma>))"

lemma common_reparam_exists_imp_eq_line_integral:
  assumes finite_basis: "finite basis" and 
    "finite one_chain1"
    "finite one_chain2"
    "boundary_chain (one_chain1::(int \<times> (real \<Rightarrow> real \<times> real)) set)"
    "boundary_chain (one_chain2::(int \<times> (real \<Rightarrow> real \<times> real)) set)"
    " \<forall>(k2, \<gamma>2)\<in>one_chain2. \<forall>b\<in>basis. continuous_on (path_image \<gamma>2) (\<lambda>x. F x \<bullet> b)"
    "(common_reparam_exists one_chain1 one_chain2)"
    "(\<forall>(k, \<gamma>)\<in>one_chain1.  valid_path \<gamma>)"
    "(\<forall>(k, \<gamma>)\<in>one_chain2.  valid_path \<gamma>)"
  shows "one_chain_line_integral F basis one_chain1 = one_chain_line_integral F basis one_chain2"
    "\<forall>(k, \<gamma>)\<in>one_chain1. line_integral_exists F basis \<gamma>"
proof-
  obtain subdiv ps1 ps2 where props:
    "chain_reparam_chain (one_chain1 - ps1) subdiv"
    "chain_reparam_chain (one_chain2 - ps2) subdiv "
    "(\<forall>(k, \<gamma>)\<in>subdiv.  \<gamma> C1_differentiable_on {0..1})"
    "boundary_chain subdiv"
    "(\<forall>(k, \<gamma>)\<in>ps1. point_path \<gamma>)"
    "(\<forall>(k, \<gamma>)\<in>ps2. point_path \<gamma>)"
    using assms
    by (auto simp add: common_reparam_exists_def)
  have subdiv_valid: "(\<forall>(k, \<gamma>)\<in>subdiv.  valid_path \<gamma>)"
    apply(simp add: valid_path_def)
    using props(3)
    using C1_differentiable_imp_piecewise by blast
  have onechain_boundary1: "boundary_chain (one_chain1 - ps1)" using assms(4) by (auto simp add: boundary_chain_def)
  have onechain_boundary2: "boundary_chain (one_chain2 - ps1)" using assms(5) by (auto simp add: boundary_chain_def)
  {fix k2 \<gamma>2 b
    assume ass: "(k2, \<gamma>2)\<in>subdiv "" b\<in>basis"
    have "\<And> k \<gamma>. (k, \<gamma>) \<in> subdiv \<Longrightarrow> \<exists>k' \<gamma>'. (k', \<gamma>') \<in> one_chain2 \<and> path_image \<gamma> \<subseteq> path_image \<gamma>'"
      by (meson chain_reparam_chain_pathimg_subset' props Diff_subset subsetCE)
    then have "continuous_on (path_image \<gamma>2) (\<lambda>x. F x \<bullet> b)"
      using assms(6) continuous_on_subset[where ?f = " (\<lambda>x. F x \<bullet> b)"] ass
      apply(auto simp add:  subset_iff)
      by (metis (mono_tags, lifting) case_prodD)}
  then have cont_field: "\<forall>b\<in>basis. \<forall>(k2, \<gamma>2)\<in>subdiv. continuous_on (path_image \<gamma>2) (\<lambda>x. F x \<bullet> b)"
    by auto
  have one_chain1_ps_valid: "(\<forall>(k, \<gamma>)\<in>one_chain1 - ps1.  valid_path \<gamma>)" using assms by auto
  have one_chain2_ps_valid: "(\<forall>(k, \<gamma>)\<in>one_chain2 - ps1.  valid_path \<gamma>)" using assms by auto
  have 0: "one_chain_line_integral F basis (one_chain1 - ps1) = one_chain_line_integral F basis subdiv"
    apply(rule chain_reparam_chain_line_integral_smooth_cubes[OF props(1) props(3) cont_field finite_basis])
    using props assms
    apply blast
    using props assms
    using onechain_boundary1 apply blast
    using props assms
    apply blast
    using one_chain1_ps_valid by blast
  have 1:"\<forall>(k, \<gamma>)\<in>(one_chain1 - ps1). line_integral_exists F basis \<gamma>"
    apply(rule chain_reparam_chain_line_integral_smooth_cubes[OF props(1) props(3) cont_field finite_basis])
    using props assms
    apply blast
    using props assms
    using onechain_boundary1 apply blast
    using props assms
    apply blast
    using one_chain1_ps_valid by blast
  have 2: "one_chain_line_integral F basis (one_chain2 - ps2) = one_chain_line_integral F basis subdiv"
    apply(rule chain_reparam_chain_line_integral_smooth_cubes[OF props(2) props(3) cont_field finite_basis])
    using props assms
    apply blast
    apply (simp add: assms(5) boundary_chain_diff)
    apply (simp add: props(4))
    by (simp add: assms(9))
  have 3:"\<forall>(k, \<gamma>)\<in>(one_chain2 - ps2). line_integral_exists F basis \<gamma>"
    apply(rule chain_reparam_chain_line_integral_smooth_cubes[OF props(2) props(3) cont_field finite_basis])
    using props assms
    apply blast
    apply (simp add: assms(5) boundary_chain_diff)
    apply (simp add: props(4))
    by (simp add: assms(9))
  show line_int_ex_chain1: "\<forall>(k, \<gamma>)\<in>one_chain1. line_integral_exists F basis \<gamma>"
    using 0 
    using "1" finite_basis line_integral_exists_point_path props(5) by fastforce
  then show "one_chain_line_integral F basis one_chain1 = one_chain_line_integral F basis one_chain2"
    using 0 1 2 3
    using assms(2-3) finite_basis one_chain_line_integral_point_paths props(5) props(6) by auto
qed

definition subcube :: "real \<Rightarrow> real \<Rightarrow> (int \<times> (real \<Rightarrow> real \<times> real)) \<Rightarrow> (int \<times> (real \<Rightarrow> real \<times> real))" where
  "subcube a b cube = (fst cube, subpath a b (snd cube))"

lemma subcube_valid_path:
  assumes "valid_path (snd cube)" "a \<in> {0..1}" "b \<in> {0..1}"
  shows "valid_path (snd (subcube a b cube))"
  using valid_path_subpath[OF assms] by (auto simp add: subcube_def)

lemma line_integral_neg_base:
   "\<integral>\<^bsub>\<gamma>\<^esub> F \<downharpoonright>\<^bsub>{i::'a::euclidean_space}\<^esub> = \<integral>\<^bsub>\<gamma>\<^esub> F \<downharpoonright>\<^bsub>{-1 *\<^sub>R i}\<^esub>"
   "line_integral_exists F {i} \<gamma> = line_integral_exists F {-1 *\<^sub>R i} \<gamma>"
  unfolding line_integral_def line_integral_exists_def
  by(auto intro!: integral_cong)
  
lemma one_chain_line_integral_neg_base:
   "\<^sup>H\<integral>\<^bsub>\<gamma>\<^esub> F \<downharpoonright>\<^bsub>{i}\<^esub> = \<^sup>H\<integral>\<^bsub>\<gamma>\<^esub> F \<downharpoonright>\<^bsub>{-1 *\<^sub>R i}\<^esub>"
  unfolding one_chain_line_integral_def
  apply(auto intro!: Finite_Cartesian_Product.sum_cong_aux)
  using line_integral_neg_base by force

lemma common_boundary_sudivision_exists_refl_1:
  assumes "\<forall>(k,\<gamma>)\<in>C. valid_path \<gamma>"
    "boundary_chain (C::(int \<times> (real \<Rightarrow> real \<times> real)) set)"
  shows "common_boundary_sudivision_exists (C) (C)"
  using assms chain_subdiv_chain_refl common_boundary_sudivision_exists_def by blast

lemma reparam_refl: "reparam \<gamma> \<gamma>"
  unfolding reparam_def
proof(intro exI conjI)
  show "\<forall>x\<in>{0..1}. \<gamma> x = (\<gamma> \<circ> id) x"
    by auto
  show "id piecewise_C1_differentiable_on {0..1}"
    by (metis C1_differentiable_imp_piecewise C1_differentiable_on_ident eq_id_iff smooth_path_imp_reverse subpath_reversepath valid_path_def valid_path_reversepath)
  show "bij_betw id {0..1} {0..1}"
    by auto
  show "id -` {0..1} \<subseteq> {0..1}"
    by auto
  show "id 0 = 0" by auto
  show " id 1 = 1" by auto
qed

lemma chain_subdiv_path_imp_chain_reparam_path:
  assumes "chain_subdiv_path x y"
  shows "chain_reparam_path x y"
  unfolding chain_reparam_path_def
proof(intro exI conjI)
  show "chain_subdiv_path x y"
    using assms
    by auto
  show "reparam x x" using reparam_refl by auto
qed

lemma chain_subdiv_chain_imp_chain_reparam_chain:
  assumes "chain_subdiv_chain x y"
  shows " chain_reparam_chain x y"
  using assms
  unfolding chain_reparam_chain_def chain_subdiv_chain_def
  using chain_subdiv_path_imp_chain_reparam_path by auto

end