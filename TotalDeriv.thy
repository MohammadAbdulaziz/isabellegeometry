theory TotalDeriv
  imports PartialDerivs
begin

definition
 "C1_differentiable_on' C s = 
  (\<exists>C'. (\<forall>x\<in> s. (C has_derivative (C' x)) (at x)) \<and> continuous_on s C')"

lemma C1_differentiable_on'_has_derivative:
  "C1_differentiable_on' C s \<Longrightarrow> (\<exists>C'. (\<forall>x\<in> s. (C has_derivative (C' x)) (at x)))"
  unfolding C1_differentiable_on'_def
  by auto

definition "C1_differentiable_on'' C s = (\<exists>C'. (\<forall>x\<in> s. (C has_derivative (C' x)) (at x)) \<and> (\<forall>b. continuous_on s (%x. C' x b)))"

lemma C1_diff'_subset:
  assumes "C1_differentiable_on' C s" "t \<subseteq> s"
  shows "C1_differentiable_on' C t"
  using assms
  unfolding C1_differentiable_on'_def
  apply auto
  using continuous_on_subset by blast

lemma smooth_on_every_component_eq_smooth: "C1_differentiable_on' (C::'a::real_normed_vector \<Rightarrow> 'b::real_normed_vector) s = C1_differentiable_on'' C s"
  unfolding C1_differentiable_on'_def C1_differentiable_on''_def
  using continuous_on_product_coordinatewise_iff by metis

lemma C1_diff'_path_eq_C1_diff_old:
  shows "(C C1_differentiable_on s) = C1_differentiable_on' (C::real \<Rightarrow> 'a :: euclidean_space) s"
proof(auto)
  assume ass: "C C1_differentiable_on s"
  obtain D where D: "(\<forall>x\<in>s. (C has_derivative (\<lambda>xa. xa *\<^sub>R D x)) (at x))" "continuous_on s D"
    using ass unfolding C1_differentiable_on_def has_vector_derivative_def by auto
  let ?C' = "(%(x::real) \<delta>::real.  \<delta> *\<^sub>R D x)"
  have "continuous_on s ((%x. ?C' x \<delta>))" for \<delta>
    by(auto intro: D(2) continuous_on_product_then_coordinatewise' continuous_intros) 
  then show "C1_differentiable_on' C s" 
    unfolding C1_differentiable_on''_def smooth_on_every_component_eq_smooth
    using D  by fastforce 
next
  assume ass: "C1_differentiable_on' C s"
  obtain C' where C': "\<forall>x\<in>s. (C has_derivative (C' x)) (at x)" "continuous_on s C'"
    using ass unfolding C1_differentiable_on'_def by auto
  then have "\<forall>x\<in>s. has_partial_vector_derivative C (1::real) (C' x 1) x" 
    by (simp add: has_derivative_to_has_partial_deriv_real)
  then have "\<forall>x\<in>s. (C has_vector_derivative (%x. C' x 1) x) (at x)"
    using partial_deriv_one_d by blast
  moreover have "continuous_on s (%x. C' x 1)" using C'(2) continuous_on_product_then_coordinatewise' by metis
  ultimately show "C C1_differentiable_on s" 
    unfolding C1_differentiable_on_def
    by(auto intro!: exI conjI) 
qed

lemma C1_diff''_imp_components:
  assumes "C1_differentiable_on'' C s"
  shows "C1_differentiable_on'' (C\<^bsub>i\<^esub>) s"
proof-
  obtain C' where C': "(\<forall>x\<in> s. (C has_derivative (C' x)) (at x))" "(\<forall>b. continuous_on s (%x. C' x b))"
    using assms unfolding C1_differentiable_on''_def by auto
  then have "(\<forall>x\<in> s. ((C\<^bsub>i\<^esub>) has_derivative (%x b. C' x b \<bullet> i) x) (at x))" 
    using has_derivative_inner_left by metis
  moreover have  "\<forall>b. continuous_on s (%x. C' x b \<bullet> i)"
    using continuous_on_inner C'(2)  continuous_on_const[of _ i] by fastforce
  ultimately show ?thesis unfolding C1_differentiable_on''_def by auto
qed

lemma C1_diff'_imp_components:
  assumes "C1_differentiable_on' C s"
  shows "C1_differentiable_on' (C\<^bsub>i\<^esub>) s"
  using smooth_on_every_component_eq_smooth C1_diff''_imp_components assms by metis

lemma C_diff'_imp_differntiable: 
  "C1_differentiable_on' C s \<Longrightarrow> \<forall>x\<in>s. C differentiable (at x)"
  "C1_differentiable_on' C s \<Longrightarrow> C differentiable_on s"
  unfolding C1_differentiable_on'_def differentiable_on_def differentiable_def
  using has_derivative_at_withinI by blast+

lemma C_diff'_imp_cont: "C1_differentiable_on' C s \<Longrightarrow> continuous_on s C"
  using differentiable_imp_continuous_on C_diff'_imp_differntiable
  by blast

lemma C1_smooth''_imp_C1_smooth_boundaries:
  assumes "(b::'a::euclidean_space) \<bullet> b = 1"
          "C1_differentiable_on'' (C::'a::euclidean_space \<Rightarrow> 'b::euclidean_space) s"
          "a \<bullet> b = 0" "\<forall>x\<in>((%x. x \<bullet> b) ` s). (a + x *\<^sub>R b) \<in> s"
      shows "C1_differentiable_on'' (%x. C (a + x *\<^sub>R b)) ((%x. x \<bullet> b) ` s)"
  proof-
    obtain C' where C': "\<And>x. x\<in>s \<Longrightarrow> (C has_derivative (C' x)) (at x)" "\<forall>b. continuous_on s (%x. C' x b)"
      using assms unfolding C1_differentiable_on''_def
      by (metis all_not_in_conv continuous_on_empty)
    let ?C' = "(%(x::real) \<delta>::real.  \<delta> *\<^sub>R C' (a + x *\<^sub>R b) b)"
    have i: "((\<lambda>x. C (a - (a \<bullet> b) *\<^sub>R b + x *\<^sub>R b)) has_derivative (\<lambda>x. x *\<^sub>R C' a b)) (at (a \<bullet> b))" if "a \<in> s" for a
      using has_derivative_to_has_partial_deriv'[where F = C and F' = C' and  a = a] C'(1)[OF that] by auto
    {
      fix x assume "x\<in>(\<lambda>x. x \<bullet> b) ` s"
      then obtain p where "p \<in> s" "x = p \<bullet> b" by auto
      then have a_p_x: "a + x *\<^sub>R b \<in> s" using assms by auto
      have "((\<lambda>x. C (a + x *\<^sub>R b)) has_derivative ?C' x) (at x)"
        using i[OF a_p_x]
        by (auto simp add: i assms inner_left_distrib)
     }note * = this
   have ii: "continuous_on ((%p. (p \<bullet> b)) ` s) ((%x. ?C' x \<delta>))" for \<delta>
     apply(rule continuous_on_compose2[where ?f = "%x. a + x *\<^sub>R b" and s = "(%p. (p \<bullet> b)) ` s" and ?t =  s])
       apply (auto intro!: continuous_intros)
     using assms C'(2) by auto
   then show ?thesis
    unfolding C1_differentiable_on''_def
    using *
    by fastforce
qed

lemma C1_smooth'_imp_C1_smooth_boundaries:
  assumes "(b::'a::euclidean_space) \<bullet> b = 1" "(C1_differentiable_on' (C::'a::euclidean_space \<Rightarrow> 'b::euclidean_space) s)" "a \<bullet> b = 0"  "\<forall>x\<in>((%x. x \<bullet> b) ` s). (a + x *\<^sub>R b) \<in> s"
  shows "C1_differentiable_on' (%x. C (a + x *\<^sub>R b)) ((%x. x \<bullet> b) ` s)"
  using C1_smooth''_imp_C1_smooth_boundaries[OF assms(1) _ assms(3) assms(4)] assms(2)
  unfolding smooth_on_every_component_eq_smooth 
  by force

definition C1_partially_differentiable_on where
  "C1_partially_differentiable_on C s b = (\<exists>C'. (\<forall>x\<in> s. (has_partial_vector_derivative C b (C' x) x)) \<and> continuous_on s C')"

lemma C1_partially_differentiable_on_imp_cont_part_deriv:
  assumes "C1_partially_differentiable_on C s b"
  shows "continuous_on s (\<partial>C/\<partial>b)"
proof-
  obtain C_b' where C_b': "(\<forall>x\<in> s. (has_partial_vector_derivative C b (C_b' x) x))" "continuous_on s C_b'"
    using assms unfolding C1_partially_differentiable_on_def
    by auto
  then have "\<forall>x\<in>s. \<partial>C/ \<partial>b |\<^bsub>x\<^esub> = C_b' x"
    using partial_vector_derivative_works_2
    by force
  then show ?thesis
    using continuous_on_eq C_b'(2)
    by auto
qed

lemma C1_smooth_gen_C1_smooth_old':
  assumes "C1_differentiable_on' C s"
  shows "(C1_partially_differentiable_on C s b)"
proof-
  obtain C' where C': "\<forall>x\<in>s. (C has_derivative (C' x)) (at x)" "continuous_on s C'"
    using assms unfolding C1_differentiable_on'_def by auto
  then have "\<forall>x\<in>s. has_partial_vector_derivative C (b) (C' x b) x" 
    using has_derivative_to_has_partial_deriv 
    by force
  moreover have "continuous_on s (%x. C' x b)"
    using C'(2) continuous_on_product_then_coordinatewise'
    by metis
  ultimately show ?thesis 
    unfolding C1_partially_differentiable_on_def
    by(auto intro!: exI conjI) 
qed

lemma cont_pair_to_line:
  "continuous_on A (\<lambda>a. f (a, b))"
  if "continuous_on (A \<times> B) f" "b \<in> B"
  thm continuous_on_compose2
  apply (rule continuous_on_compose2[OF that(1)])
  by (auto intro!: continuous_intros that)

lemma C1_partially_diff_then_C1_diff_on_component:
  assumes  "a \<bullet> b = 0" "b \<bullet> b = 1" 
           "(C1_partially_differentiable_on C s b)"
           "\<forall>x\<in>((%x. x \<bullet> b) ` s). (a + x *\<^sub>R b) \<in> s"
  shows "(%x. C (a + x *\<^sub>R b)) C1_differentiable_on ((%x. x \<bullet> b) ` s)"
proof- 
  obtain C' where C': "(\<And>x. x \<in> s \<Longrightarrow> ((\<lambda>xa. C (x - (x \<bullet> b) *\<^sub>R b + xa *\<^sub>R b)) has_vector_derivative C' x) (at (x \<bullet> b)))" "continuous_on s C'"
    using assms(3) unfolding C1_partially_differentiable_on_def has_partial_vector_derivative_def by blast
  {
    fix x assume "x\<in>(\<lambda>x. x \<bullet> b) ` s"
    then obtain p where "p \<in> s" "x = p \<bullet> b" by auto
    then have a_p_x: "a + x *\<^sub>R b \<in> s" using assms(4) by auto
    have "((\<lambda>x. C (a + x *\<^sub>R b)) has_vector_derivative (%x. C' (a + x *\<^sub>R b)) x) (at x)"
      using C'(1)[OF a_p_x]
      unfolding has_partial_vector_derivative_def
      by (simp add: assms(1) assms(2) inner_left_distrib)
  }note * = this
  have ii: "continuous_on ((%p. (p \<bullet> b)) ` s) ((%x. C' (a + x *\<^sub>R b)))"
    apply(rule continuous_on_compose2[where ?f = "%x. a + x *\<^sub>R b" and s = "(%p. (p \<bullet> b)) ` s"])
     apply (auto intro!: continuous_intros C'(2))
    using assms by auto
  then show ?thesis
    unfolding C1_differentiable_on_def
    using *
    by metis
qed

(*lemma C1_diff_on_component_then_C1_partially_diff:
  assumes  "a \<bullet> b = 0" "b \<bullet> b = 1" "\<forall>a\<in>s. (%x. C (a - (a \<bullet> b) *\<^sub>R b + x *\<^sub>R b)) C1_differentiable_on ((%x. x \<bullet> b) ` s)"
  shows "(C1_partially_differentiable_on C s b)" 
  proof-
  {
    fix p assume ass: "p \<in> s"
    then have i: "p\<bullet>b \<in>((%x. x \<bullet> b) ` s)" by auto
    obtain C' where C': "(\<And>x. x\<in>(\<lambda>x. x \<bullet> b) ` s \<Longrightarrow> ((\<lambda>x. C (p - (p \<bullet> b) *\<^sub>R b + x *\<^sub>R b)) has_vector_derivative C' x) (at x)) " "continuous_on ((\<lambda>x. x \<bullet> b) ` s) C'"
      using assms(3) ass unfolding C1_differentiable_on_def by blast
    have "continuous_on s (C' o (%p.(p \<bullet> b)))"
      apply(rule continuous_on_compose)
      using C'(2) by (auto intro!: continuous_intros)
    moreover have "((\<lambda>xa. C (p - (p \<bullet> b) *\<^sub>R b + xa *\<^sub>R b)) has_vector_derivative  C' (p \<bullet> b)) (at (p \<bullet> b))"
      using C'(1)[OF i]
      by (auto simp add: assms(1) assms(2))
    ultimately have "\<exists>C'. (((\<lambda>xa. C (p - (p \<bullet> b) *\<^sub>R b + xa *\<^sub>R b)) has_vector_derivative  C' p) (at (p \<bullet> b))) \<and> continuous_on s C'"
      by auto
  }
  then obtain C' where "(\<forall>x\<in>s. ((\<lambda>xa. C (x - (x \<bullet> b) *\<^sub>R b + xa *\<^sub>R b)) has_vector_derivative (C' x x)) (at (x \<bullet> b)) \<and> continuous_on s (C' x))"
    by metis
  then show ?thesis 
    unfolding C1_partially_differentiable_on_def has_partial_vector_derivative_def
    sorry
qed*)

lemma C1_partially_differentiable_on_mult:
  assumes "C1_partially_differentiable_on (F::'a::{euclidean_space} \<Rightarrow> 'b::{real_normed_algebra, euclidean_space}) s b"
          "C1_partially_differentiable_on G s b"
          "continuous_on s F" "continuous_on s G"
  shows "C1_partially_differentiable_on (%x. F x * G x) s b"
  proof-
  obtain F' where F': "\<And>x. x\<in>s \<Longrightarrow> (((\<lambda>xa. F (x - (x \<bullet> b) *\<^sub>R b + xa *\<^sub>R b)) has_vector_derivative F' x) (at (x \<bullet> b)))" "continuous_on s F'"
    using assms(1) unfolding C1_partially_differentiable_on_def has_partial_vector_derivative_def by auto
  obtain G' where G': "\<And>x. x\<in>s \<Longrightarrow> (((\<lambda>xa. G (x - (x \<bullet> b) *\<^sub>R b + xa *\<^sub>R b)) has_vector_derivative G' x) (at (x \<bullet> b)))" "continuous_on s G'"
    using assms(2) unfolding C1_partially_differentiable_on_def has_partial_vector_derivative_def by auto
  let ?C' = "(%x. F (x - (x \<bullet> b) *\<^sub>R b + (x \<bullet> b) *\<^sub>R b) * G' x + F' x * G (x - (x \<bullet> b) *\<^sub>R b + (x \<bullet> b) *\<^sub>R b))"
  have "continuous_on s ?C'"
    by(auto intro!: continuous_intros F'(2) G'(2) assms(3,4))
  moreover have "((\<lambda>xa. F (x - (x \<bullet> b) *\<^sub>R b + xa *\<^sub>R b) * G (x - (x \<bullet> b) *\<^sub>R b + xa *\<^sub>R b)) has_vector_derivative ?C' x) (at (x \<bullet> b))" if "x\<in>s" for x
    using has_vector_derivative_mult[OF F'(1)[OF that] G'(1)[OF that]]
    by auto
  ultimately show ?thesis
    unfolding C1_partially_differentiable_on_def has_partial_vector_derivative_def
    by force
qed

lemma C1_differentiable_compose:
    "\<lbrakk>f C1_differentiable_on s; g C1_differentiable_on (f ` s)\<rbrakk>
      \<Longrightarrow> (g o f) C1_differentiable_on s"
  apply (simp add: C1_differentiable_on_eq, safe)
   using differentiable_chain_at apply blast
  apply (rule continuous_on_eq [of _ "\<lambda>x. vector_derivative f (at x) *\<^sub>R vector_derivative g (at (f x))"])
   apply (rule Limits.continuous_on_scaleR, assumption)
   apply (metis (mono_tags, lifting) continuous_on_eq continuous_at_imp_continuous_on continuous_on_compose differentiable_imp_continuous_within o_def)
  by (simp add: vector_derivative_chain_at)

lemma C1_differentiable'_compose':
  assumes "C1_differentiable_on'' (f::'a::{euclidean_space,real_normed_vector} \<Rightarrow> 'b::{euclidean_space,real_normed_vector}) s"  "C1_differentiable_on' g (f ` s)"
  shows "C1_differentiable_on' (g \<circ> f) s"
proof-
  obtain f' where f': "(\<And>x. x\<in> s \<Longrightarrow> (f has_derivative (f' x)) (at x))" "\<And>b. continuous_on s (%x. f' x b)"
    using assms(1) unfolding C1_differentiable_on''_def by auto
  obtain g' where g': "(\<And>x. x\<in> (f ` s) \<Longrightarrow> (g has_derivative (g' x)) (at x))" "\<And>b. continuous_on (f ` s) g'"
    using assms(2) unfolding C1_differentiable_on'_def by auto
  then have "\<forall>x\<in> f ` s. bounded_linear (g' x)" unfolding has_derivative_def bounded_linear_def by auto
  then have "\<exists>bl_g'. blinfun_apply (bl_g') = (g' x)" if "x\<in> f ` s" for x
    using bounded_linearE by (metis that) 
  then obtain bl_g' where "blinfun_apply (bl_g' x) = (g' x)" if "x\<in> f ` s" for x
    by (metis) 
  then have bl_g': "blinfun_apply ((bl_g' o f) x) = (g' (f x))" if "x\<in> s" for x
    by(auto simp add: image_def, metis that) 
  have "continuous_on s (%x. g' (f x))"
    using g'(2)
    by (smt has_derivative_at_withinI comp_def continuous_on_compose continuous_on_eq f'(1) has_derivative_continuous_on)
  then have "\<And>b. continuous_on s (%x. g' (f x) b)"
    by (simp add: General_Utils.continuous_on_product_coordinatewise_iff)
  then have "\<forall>b. continuous_on s (%x. blinfun_apply ((bl_g' o f) x) b)"
    using continuous_on_eq bl_g'
    by auto
  then have "continuous_on s (%x. (bl_g' o f) x)"
    using Bounded_Linear_Function.continuous_on_blinfun_componentwise
    by blast     
  then have i: "continuous_on s (%x. (blinfun_apply (bl_g' (f x))) o (f' x))"
    by(auto intro!: continuous_intros f'(2))
  have "(g \<circ> f has_derivative g' (f x) \<circ> f' x) (at x)" if "x\<in>s" for x
    using diff_chain_at[OF f'(1)[OF that] g'(1)] that
    by auto
  then have ii: "(g \<circ> f has_derivative (blinfun_apply (bl_g' (f x))) o (f' x)) (at x)" if "x\<in>s" for x
    using bl_g'
    by (simp add: that)
  show ?thesis
    unfolding C1_differentiable_on'_def
    using i ii
    by fastforce
qed

lemma C1_differentiable'_compose:
  assumes "C1_differentiable_on' (f::'a::{euclidean_space,real_normed_vector} \<Rightarrow> 'b::{euclidean_space,real_normed_vector}) s"  "C1_differentiable_on' g (f ` s)"
  shows "C1_differentiable_on' (g \<circ> f) s"
  using C1_differentiable'_compose' assms
  using smooth_on_every_component_eq_smooth by blast

lemma  C1_differentiable'_mult:
  assumes "C1_differentiable_on' (f::'a::{euclidean_space,real_normed_vector} \<Rightarrow> 'b::{euclidean_space,real_normed_algebra}) s "" C1_differentiable_on' (g::'a::{euclidean_space,real_normed_vector} \<Rightarrow> 'b::{euclidean_space,real_normed_algebra,times}) s "
  shows  " C1_differentiable_on' (\<lambda>x. f x * g x) s"
proof-
  obtain f' where f': "(\<And>x. x\<in> s \<Longrightarrow> (f has_derivative (f' x)) (at x))" "continuous_on s f'"
    using assms C1_differentiable_on'_def by blast
  obtain g' where g': "(\<And>x. x\<in> s \<Longrightarrow> (g has_derivative (g' x)) (at x))" "continuous_on s g'"
    using assms C1_differentiable_on'_def by blast
  let ?C' = "(\<lambda>x h. f x * g' x h + f' x h * g x)"
  have "((\<lambda>x. f x * g x) has_derivative ?C' x) (at x)" if "x\<in>s" for x
    using has_derivative_mult[OF f'(1)[OF that] g'(1)[OF that]]
    by auto
  moreover have "continuous_on s ?C'"
    apply(auto intro!: continuous_intros)
    subgoal using  assms C_diff'_imp_cont by blast
    subgoal using g'(2) by (auto simp add: General_Utils.continuous_on_product_coordinatewise_iff)
    subgoal  using f'(2) by (auto simp add: General_Utils.continuous_on_product_coordinatewise_iff)
    subgoal using  assms C_diff'_imp_cont by blast
    done
  ultimately show ?thesis
    unfolding C1_differentiable_on'_def by force
qed

lemma C1_partially_diff_imp_partially_diff: "C1_partially_differentiable_on F s i \<Longrightarrow> p \<in> s \<Longrightarrow> partially_vector_differentiable F i p"
  unfolding C1_partially_differentiable_on_def partially_vector_differentiable_def by auto

lemma indicator_2d_to_1d:
  assumes "Basis = {i,j}" "i \<noteq> j"
  shows "(%y. indicator s (y *\<^sub>R i + x *\<^sub>R j) *\<^sub>R f y) = (%y. indicator ((%p. (p \<bullet> i)) ` (s \<inter> {p. p \<bullet> j = x})) (y) *\<^sub>R f y)"
  unfolding indicator_def image_def
proof
  fix y 
  have ass: "i \<bullet> j = 0" "i \<bullet> i = 1" "j \<bullet> j = 1"
    by (auto simp add: assms inner_Basis)
  {assume "y *\<^sub>R i + x *\<^sub>R j \<in> s"
    moreover have "(y *\<^sub>R i + x *\<^sub>R j) \<bullet> i = y" "(y *\<^sub>R i + x *\<^sub>R j) \<bullet> j = x" using ass by (auto simp add: inner_add_left inner_commute inner_add_right)
    ultimately have "\<exists>p. (y = p \<bullet> i \<and> (p\<in>s \<inter> {p. p \<bullet> j = x}))"     
     apply auto  by force
  } note * = this
  {fix p assume "(p \<bullet> i) *\<^sub>R i + (p \<bullet> j) *\<^sub>R j \<notin> s "" y = p \<bullet> i "" p \<in> s"
   then have "x \<noteq> p \<bullet> j" using ass assms(1) euclidean_representation[of p, symmetric] apply (auto)
     by (smt add.right_neutral finite.emptyI finite.insertI insert_absorb insert_commute singleton_insert_inj_eq sum.empty sum.insert)
  } note ** = this
  show "(if y *\<^sub>R i + x *\<^sub>R j \<in> s then 1 else 0) *\<^sub>R f y = (if y \<in> {y. \<exists>x\<in>s \<inter> {p. p \<bullet> j = x}. y = x \<bullet> i} then 1 else 0) *\<^sub>R f y"
    using * **
    by (auto simp add: inter_iff split: if_splits)
qed  

lemma indicator_2d_to_1d':
  "indicat_real (s \<times> t) (x,y) = indicat_real s x * indicat_real t y"
  unfolding indicator_def image_def
  by(auto split: if_splits)

lemma indicator_2d_to_1d'':
  assumes "Basis = {i,j}" "i \<noteq> j"
  shows "(%y. indicat_real s (y *\<^sub>R i + x *\<^sub>R j) *\<^sub>R f y) = (%y. indicat_real ((%p. (p \<bullet> j)) ` (s \<inter> {p. p \<bullet> i = y})) x * indicat_real ((%p. (p \<bullet> i)) ` (s \<inter> {p. p \<bullet> j = x})) y *\<^sub>R f y)"
  unfolding indicator_def image_def
proof
  fix y 
  have ass: "i \<bullet> j = 0" "i \<bullet> i = 1" "j \<bullet> j = 1"
    by (auto simp add: assms inner_Basis)
  {assume "y *\<^sub>R i + x *\<^sub>R j \<in> s"
    moreover have "(y *\<^sub>R i + x *\<^sub>R j) \<bullet> i = y" "(y *\<^sub>R i + x *\<^sub>R j) \<bullet> j = x" using ass by (auto simp add: inner_add_left inner_commute inner_add_right)
    ultimately have "\<exists>p. (y = p \<bullet> i \<and> (p\<in>s \<inter> {p. p \<bullet> j = x}))"     
     apply auto  by force
  } note * = this
  {fix p assume "(p \<bullet> i) *\<^sub>R i + (p \<bullet> j) *\<^sub>R j \<notin> s "" y = p \<bullet> i "" p \<in> s"
   then have "x \<noteq> p \<bullet> j" using ass assms(1) euclidean_representation[of p, symmetric] apply (auto)
     by (smt add.right_neutral finite.emptyI finite.insertI insert_absorb insert_commute singleton_insert_inj_eq sum.empty sum.insert)
  } note ** = this
  show "(if y *\<^sub>R i + x *\<^sub>R j \<in> s then 1 else 0) *\<^sub>R f y = 
         (if x \<in> {ya. \<exists>x\<in>s \<inter> {p. p \<bullet> i = y}. ya = x \<bullet> j} then 1 else 0) * (if y \<in> {y. \<exists>x\<in>s \<inter> {p. p \<bullet> j = x}. y = x \<bullet> i} then 1 else 0) *\<^sub>R f y"
    using * **
    by (auto simp add: inter_iff split: if_splits)
qed  

end
