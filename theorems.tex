\input{ringpartfigColourlessImg}
\section{More General Structures}
\label{sec:thms}
Now that we described some of the basic definitions and how to derive Green's theorem for elementary regions, the remaining question is how to prove the theorem for more general regions.
As we stated earlier in the introduction, a lot of the text book proofs of Green's theorem are given for regions that can be divided into elementary regions.
It can be shown that any \emph{regular} region can be divided into elementary regions \cite{protter2006basic,zorich2004mathematical}.
Regular regions (see their definition in \cite[page 235]{protter2006basic}), are enough for a lot of applications, especially practical applications in physics and engineering.

In this section we describe how we prove Green's theorem for regions that can be divided both into type I regions and type II regions \emph{only} using vertical and horizontal edges, respectively.
%\footnote{In some references in the literature, Green's theorem is proven for regions that can be divided into type I and type II regions, without imposing}
We believe that for most practical purposes, the additional assumption that the division is done only by vertical and horizontal edges is equivalent to assuming just the existence of type I and type II divisions.
Indeed, we conjecture that the additional constraints do not lead to any loss of generality, however, we would not pursue the proof of this claim in the current paper.

Figure \ref{fig:ringpartiotioning} shows an example of a region and its type I and type II partitions.
In this example, some of the elementary regions appear to have a missing edge.
This is because the type I or the type II partitioning induced a \textit{one-point path}: a function mapping the interval $[0,1]$ to a single point in $\mathbb{R}^2$.
For instance, the left edge in the left 1-chain in \ref{fig:typeI1} is a point on the $x$-axis.

\subsection{Chains and cubes}
\label{subsec:chainscubes}

For tackling more general regions and their boundaries we use the concepts of cubes and chains \cite[chapter 8]{spivak1981comprehensive}.
One use of cubes is to represent parameterisable (sub)surfaces (regions in $\mathbb{R}^2$ and paths in our case).
A $k$-dimensional such surface embedded in $\mathbb{R}^n$ is represented by a function whose domain is a space homeomorphic to $\mathbb{R}^k$ and whose codomain is $\mathbb{R}^n$.
Roughly speaking, we model cubes as functions and chains as sets of cubes.
We use the existing Isabelle/HOL formalisation of paths, where we model 1-cubes as functions defined on the interval \isa{\{0..1\}}.
We model a 1-chain as a set of pairs of \isa{int} (coefficients) and 1-cubes.
For example, the following definition shows the lifting of the line integrals to 1-chains.
\begin{isabelle}
\isacommand{definition}\ one\_chain\_line\_integral\ ::\isanewline
\ "((real*real)\ =>\ (real\ *real))\ =>\ ((real*real)\ set)\ \isasymRightarrow \isanewline
\ \ \ (int\ *\ (real\ \isasymRightarrow real*real))\ set\ \isasymRightarrow \ real"\ \isakeyword{where}\isanewline
"one\_chain\_line\_integral\ F\ b\ C\ =\isanewline
\ \ \ \ \ (\isasymSum (k,{\isasymgamma})\isasymin C.\ k\ *\ (line\_integral\ F\ b\ {\isasymgamma}))"
\end{isabelle}
We extend the way we model 1-cubes to model 2-cubes, which we model as functions of type \isa{(real \isacharasterisk\ real \isasymRightarrow\ real \isacharasterisk\ real)} defined on the interval \isa{\{(0,0)..(1,1)\}}.
\begin{isabelle}
\isacommand{definition}\ cubeImage::\isanewline
\ "(real*real\ \isasymRightarrow \ real*real)\ \isasymRightarrow \ ((real*real)\ set)"\ \isakeyword{where}\isanewline
"cubeImage\ twoC\ =\ (twoC\ `\ (cbox\ (0,0)\ (1,1)))"
\end{isabelle}
\noindent The orientation of the boundary of a 2-cube (a 1-chain) is taken to be counter-clockwise.
A 1-cube is given the coefficient $-1$ if the path's direction is against the counter-clockwise boundary traversal order, otherwise it is given the coefficient~$1$.
Formally this is defined as follows:
\begin{isabelle}
\isacommand{definition}\ horizontal\_boundary\ ::\isanewline
\ "(real*real\ \isasymRightarrow \ real*real)\ \isasymRightarrow \ (real*\ (real\ \isasymRightarrow \ real*real))\ set"\isanewline
\ \isakeyword{where}\ "horizontal\_boundary\ twoC\ =\isanewline
\ \ \ \ \ \ \ \isacharbraceleft (1,\ ({\isasymlambda}x.\ twoC(x,0))),\ (-1,\ ({\isasymlambda}x.\ twoC(x,1)))\isacharbraceright "\isanewline
\isanewline
\isacommand{definition}\ vertical\_boundary\ ::\isanewline
\ "(real*real\ \isasymRightarrow \ real*real)\ \isasymRightarrow \ (real*\ (real\ \isasymRightarrow \ real*real))\ set"\isanewline
\ \isakeyword{where} "vertical\_boundary\ twoC\ =\isanewline
\ \ \ \ \ \ \ \isacharbraceleft (-1,\ ({\isasymlambda}y.\ twoC(0,y))),\ (1,\ ({\isasymlambda}y.\ twoC(1,y)))\isacharbraceright "\isanewline
\isanewline
\isacommand{definition}\ boundary\ \isakeyword{where}\isanewline
"boundary\ twoC\ =\isanewline
\ \ \ \ \ \  horizontal\_boundary\ twoC\ \isasymunion \ vertical\_boundary\ twoC"
\end{isabelle}
We follow the convention and define the 2-cubes in such a way that the top and left edges are against the counter-clockwise orientation (e.g. see the 2-cube in Figure~\ref{fig:typeI2}).
Accordingly both the left and top edges take a $-1$ coefficient in the 1-cube representation.
Defining 2-cubes in that way makes it easier to define predicates identifying type I and type II 2-cubes, as follows.
\begin{isabelle}
\isacommand{definition}\ typeI\_twoCube\ ::\isanewline
\ "((real\ *\ real)\ \isasymRightarrow \ (real\ *\ real))\ \isasymRightarrow \ bool"\ \isakeyword{where}\isanewline
"typeI\_twoCube\ twoC\ \isasymlongleftrightarrow \isanewline
\ \ (\isasymexists \ (a::real)\ (b::real)\ ({g\isadigit{1}}::real\isasymRightarrow real)\ ({g\isadigit{2}}::real\ \isasymRightarrow \ real).\isanewline
\ \ \ \ a\ <\ b\ \isasymand \ (\isasymforall x\ \isasymin \ \isacharbraceleft a..b\isacharbraceright .\ {g\isadigit{2}}\ x\ \isasymle \ {g\isadigit{1}}\ x)\ \isasymand \ \isanewline
\ \ \ \ twoC\ =\isanewline
\ \ \ \ \ \ (\isasymlambda (t1,t2).\ ((1-t1)*a\ +\ t1*b,\isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ (1\ -\ t2)\ *\ ({g\isadigit{2}}\ ((1-t1)*a\ +\ x*b))\ +\isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ t2\ *\ ({g\isadigit{1}}\ ((1-t1)*a\ +\ t1*b))))\isasymand \isanewline
\ \ \ \ {g\isadigit{1}}\ C1\_differentiable\_on\ \isacharbraceleft a\ ..\ b\isacharbraceright \ \isasymand \isanewline
\ \ \ \ {g\isadigit{2}}\ C1\_differentiable\_on\ \isacharbraceleft a\ ..\ b\isacharbraceright )"
\end{isabelle}
\noindent Although we do not render it here, an analogous predicate is defined for type II 2-cubes.
We also require that all 2-cubes conform to the following predicate:
\begin{isabelle}
\isacommand{definition}\isamarkupfalse%
\ valid{\isacharunderscore}two{\isacharunderscore}cube\ \isakeyword{where}\isanewline
{\isachardoublequoteopen}valid{\isacharunderscore}two{\isacharunderscore}cube\ twoC\ {\isacharequal}\ {\isacharparenleft}card\ {\isacharparenleft}boundary\ twoC{\isacharparenright}\ {\isacharequal}\ {\isadigit{4}}{\isacharparenright}{\isachardoublequoteclose}
\end{isabelle}
\noindent This predicate filters out cases where 2-cubes have either: i) right and top edges that are both one point paths, or ii) left and bottom edges that are both one point paths.
Although this assumption potentially leaves our theorems less general regarding some corner cases, it makes our computations much smoother.
After defining these concepts on 2-cubes, we derive the following statement of $x$-axis Green's theorem (and its equivalent $y$-axis Green's theorem) in terms of 2-cubes.
\begin{isabelle}
\ GreenThm\_typeI\_twoCube:\isanewline
\ \ \isakeyword{assumes}\ "typeI\_twoCube\ twoC"\isanewline
\ \ \ \ "valid\_two\_cube\ twoC"\isanewline
\ \ \ \ "analytically\_valid\ (cubeImage\ twoC)\ ({\isasymlambda}x.\ (F\ x)\ \isasymbullet \ i)\ j"\isanewline
\ \ \isakeyword{shows}\ "integral\ (cubeImage\ twoC)\isanewline
\ \ \ \ \ \ \ \ \ \ (\isasymlambda a.\ -\ partial\_vector\_derivative\ (\isasymlambda p.\ F\ p\ \isasymbullet \ i)\ j\ \ a)\isanewline
\ \ \ \ \ \ \ \ \ \ =\ one\_chain\_line\_integral\ F\ \isacharbraceleft i\isacharbraceright \ (boundary\ twoC)"
\end{isabelle}
\noindent Although we anticipated that proving this theorem would be a straightforward unfolding of definitions and usage of \isa{GreenThm{\isacharunderscore}typeI}, it was a surprisingly long and tedious proof that took a few hundred lines.

For 2-chains, we model them as sets of 2-cubes, which suits our needs in working in the context of Green's theorem.
We define the boundary of a 2-chain as follows:
\begin{isabelle}
\isacommand{definition}\ two\_chain\_boundary::\isanewline
\ "((((real*real)\ \isasymRightarrow \ (real*real)))\ set)\ \isasymRightarrow \isanewline
\ \ ((real*\ (real\ \isasymRightarrow \ (real*real)))\ set)"\ \isakeyword{where}\isanewline
"two\_chain\_boundary\ twoChain\ \ =\ \isasymUnion (boundary\ `\ twoChain)"
\end{isabelle}
\noindent We similarly defined the functions \isa{two\_chain\_horizontal\_boundary} and \isa{two\_} \isa{chain\_vertical\_boundary}.
We also lift the double integral to 2-chains as follows.
\begin{isabelle}
\isacommand{definition}\ two\_chain\_integral::\isanewline
\ "((((real*real)\ \isasymRightarrow \ (real*real)))\ set)\ \isasymRightarrow \isanewline
\ ((real*real)\isasymRightarrow (real))\ \isasymRightarrow \ real"\ \isakeyword{where}\isanewline
"two\_chain\_integral\ twoChain\ F\ =\isanewline
\ \ \ \ \ (\isasymSum (C)\isasymin twoChain.\ (integral\ (cubeImage\ C)\ F))"
\end{isabelle}
\noindent Lastly, to smoothe our computations on integrals over 2-chains and their boundaries, we require that a 2-chain: i) only has valid 2-cubes members, ii) edges of different 2-cubes only coincide if they have opposite orientations, and iii) different 2-cubes have different images.
These requirements are formally defined in the following predicate:
\begin{isabelle}
\isacommand{definition}\ valid\_two\_chain\ \isakeyword{where}\isanewline
"valid\_two\_chain\ twoChain\ =\isanewline
\ ((\isasymforall twoCube\ \isasymin \ twoChain.\ valid\_two\_cube\ twoCube)\ \isasymand \isanewline
\ \ (\isasymforall twoCube1\ \isasymin \ twoChain.\ \isanewline
\ \ \ \ \ \isasymforall twoCube2\ \isasymin \ twoChain.\ \isanewline
\ \ \ \ \ \ \ \ \ twoCube1\ \isasymnoteq \ twoCube2\ \isasymlongrightarrow \isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ boundary\ twoCube1\ \isasyminter \ boundary\ twoCube2\ =\ \isacharbraceleft \isacharbraceright )\ \isasymand \isanewline
\ \ inj\_on\ cubeImage\ twoChain)"
\end{isabelle}

Given these definitions on 2-chains, we lift our $x$-axis Green's theorem from 2-cubes to 2-chains, as shown in the following statement.
\begin{isabelle}
\isacommand{lemma}\ GreenThm\_typeI\_twoChain:\isanewline
\ \isakeyword{assumes}\ "\isasymforall twoCube\ \isasymin \ twoChain.\ typeI\_twoCube\ twoCube"\isanewline
\ \ \ \ \ \ \ \ "valid\_two\_chain\ twoChain"\isanewline
\ \ \ \ \ \ \ \ "\isasymforall twoC\ \isasymin \ twoChain.\isanewline
\ \ \ \ \ \ \ \ \ \ analytically\_valid\ (cubeImage\ twoC)\ ({\isasymlambda}x.\ (F\ x)\ \isasymbullet \ i)\ j"\isanewline
\ \isakeyword{shows}\ "two\_chain\_integral\ twoChain\isanewline
\ \ \ \ \ \ \ \ \ \ (\isasymlambda a.\ -\ partial\_vector\_derivative\isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ ({\isasymlambda}x.\ (F\ x)\ \isasymbullet \ i)\ j\ a)\ =\isanewline
\ \ \ \ \ \ \ \ one\_chain\_line\_integral\ F\ \isacharbraceleft i\isacharbraceright \isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ (two\_chain\_boundary\ twoChain)"
\end{isabelle}
\noindent A similar lemma can be derived for a 2-chain whose members are all of Type II (with the obvious consequences of orientation asymmetry).


After proving the $x$-axis and $y$-axis Green's theorems, the next step is, after algebraic and analytic manipulation, to add the line integral sides of the $x$-axis Green's theorem to its counterpart in the $y$-axis theorem and similarly add the double integrals of both theorems.
Given \isa{GreenThm{\isacharunderscore}typeI{\isacharunderscore}twoChain} and its type II equivalent, we can sum up both sides of the equalities in the conclusion and get Green's theorem in terms of 2-chains and their boundaries.
However, the main goal of the paper is to obtain the theorem directly for a region and its boundary, given that the region can be \emph{vertically} sliced intro regions of type I and \emph{horizontally} sliced into regions of type II.

The first (and easier) part in proving this is to prove the equivalence of the double integral on a region and the integral on a 2-chain that divides that region.
Before deriving such a theorem we generalised the notion of \isa{division{\isacharunderscore}of}, defined in Isabelle/HOL's multivariate analysis library, to work when the division is not constituted of rectangles.
\begin{isabelle}
\isacommand{definition}\ gen\_division\ \isakeyword{where}\isanewline
"gen\_division\ s\ S\ =\isanewline
\ \ \ \ \ \ \ \ \ \ \ (finite\ S\ \isasymand \ (\isasymUnion S\ =\ s)\ \isasymand \isanewline
\ \ \ \ \ \ \ \ \ \ \ \ (\isasymforall u\ \isasymin \ S.\ \isasymforall t\ \isasymin \ S.\ u\ \isasymnoteq \ t\ \isasymlongrightarrow \ negligible\ (u\ \isasyminter \ t)))"
\end{isabelle}
\noindent Then we show the following equivalence:
\begin{isabelle}
\isacommand{lemma}\ two\_chain\_integral\_eq\_integral\_divisable:\isanewline
\isakeyword{assumes}\ "\isasymforall \ twoCube\ \isasymin \ twoChain.\ F\ integrable\_on\ cubeImage\ twoCube"\isanewline
\ \ \ \ \ \ \ "gen\_division\ s\ (cubeImage\ `\ twoChain)"\isanewline
\ \ \ \ \ \ \ "valid\_two\_chain\ twoChain"\isanewline
\isakeyword{shows}\ "integral\ s\ F\ =\ two\_chain\_integral\ twoChain\ F"
\end{isabelle}

The other part concerning the line integrals, proved to be trickier, and we will explain it in the next section.

\subsection{Dealing with Boundaries}

What remains now is to prove an equivalence between the line integral on the 1-chain boundary of the region under consideration and the line integral on the 1-chain boundary of the region's elementary division (i.e. the 2-chain division of the region).
The classical approach depends on that the line integrals on the introduced boundaries will cancel each other, leaving out the line integral on the region's original boundary.
For example, the vertical-straight-line paths in Figures \ref{fig:typeI1}, \ref{fig:typeI2} and \ref{fig:typeI3}, are the introduced boundaries to obtain the type I division of the annulus.
In this example, the line integrals on the introduced vertical-straight-line paths will cancel each other because of their opposite orientations, thus, leaving out the integral on the original boundary.

To prove this formally, the classical approach needs to define what is a positively oriented boundary, which requires an explicit definition of the boundary of a region, and also defining the exterior normal of the region (examples of this approach can be found in \cite{protter2006basic,federer2014geometric}).
However, we use a different approach that does not depend on these definitions and avoids a lot of the resulting geometrical and analytic complications.
Our approach depends on two observations:
\begin{itemize}
  \item[O1:] if a path $\gamma$ is straight along a vector $x$, then $\work{ F}{\gamma}{\{x\}} = 0$, for an $F$ continuous on $\gamma$.
  \item[O2:] partitioning the region in type I/type II regions was done by introducing only vertical/horizontal boundaries.
\end{itemize}


For a type I 2-chain division of a region, consider the 1-chain $\gamma_{x}$, that: i) includes \emph{all} the horizontal boundaries of the dividing 2-chain, and ii) includes \emph{some} subpaths of the vertical boundaries of the dividing 2-chain (call this condition Cx).
%(Call the analogous, but symmetrically different, condition for a type II division, call it Cy, asserts that $\gamma$ has to contain all the vertical boundaries of the dividing 2-chain.)
Based on O1, the line integral on the vertical edges in the 1-chain boundary of the type I division and accordingly $\gamma_x$, projected on $i$, will be zero.
Accordingly we can prove the $x$-axis Green's theorem for $\gamma_x$.
Formal statements of Cx (formally: \isa{only\_vertical\_division}), and the consequence of a 1-chain conforming to it are:
\begin{isabelle}
\isacommand{definition}\ only\_vertical\_division\ \isakeyword{where}\isanewline
"only\_vertical\_division\ one\_chain\ two\_chain\ =\isanewline
 (\isasymexists \isasymV .\ finite\ \isasymV \ \isasymand \isanewline
\ \ \ (\isasymforall (k,\ \isasymgamma )\ \isasymin \ \isasymV .\isanewline
\ \ \ \ \ (\isasymexists \ (k',\ \isasymgamma ')\ \isasymin \ two\_chain\_vertical\_boundary\ two\_chain.\isanewline
\ \ \ \ \ \ (\isasymexists \ a\ \isasymin \ \isacharbraceleft 0\ ..\ 1\isacharbraceright .\isanewline
\ \ \ \ \ \ \ \ \isasymexists \ b\ \isasymin \ \isacharbraceleft 0..1\isacharbraceright .\ \isanewline
\ \ \ \ \ \ \ \ \ a\ \isasymle \ b\ \isasymand \ subpath\ a\ b\ \isasymgamma '\ =\ \isasymgamma )))\ \isasymand \isanewline
\ \ \ one\_chain\ =\ \isasymV \ \isasymunion \ (two\_chain\_horizontal\_boundary\ two\_chain))"\isanewline
\isanewline
\isacommand{lemma}\ GreenThm\_typeI\_divisible\_region\_boundary\_gen:\isanewline
\ \isakeyword{assumes}\ "valid\_typeI\_division\ s\ twoChain"\isanewline
\ \ \ \ \ \ \ "\isasymforall twoC\ \isasymin \ twoChain.\isanewline
\ \ \ \ \ \ \ \ \ analytically\_valid\ (cubeImage\ twoC)\ ({\isasymlambda}a.\ F(a)\ \isasymbullet \ i)\ j"\isanewline
\ \ \ \ \ \ \ "only\_vertical\_division\ {\isasymgamma}\_x \ twoChain"\isanewline
\ \isakeyword{shows}\ "integral\ s\isanewline
\ \ \ \ \ \ \ \ \ \ ({\isasymlambda}x.\ -\ partial\_vector\_derivative\ ({\isasymlambda}a.\ F(a)\ \isasymbullet \ i)\ j\ x)\isanewline
\ \ \ \ \ \ \ \ \ \ \ =\ one\_chain\_line\_integral\ F\ \isacharbraceleft i\isacharbraceright \ {\isasymgamma}\_x "
\end{isabelle}
\noindent In the previous lemma, \isa{valid\_typeI\_division} is an abbreviation that, for a region and a 2-chain, means that the 2-chain constitutes only valid type I cubes and that this 2-chain is a division of the given region.

An analogous condition, Cy, for a type II partitioning asserts that the 1-chain  includes \emph{all} the vertical boundaries of the dividing 2-chain, and includes \emph{some} subpaths of the horizontal boundaries of the dividing 2-chain.
For Cy, we formally define the predicate (\isa{only\_horizontal\_division}) and prove an analogous theorem for type II partitions, with the obvious changes in the conclusion.

From the second observation, O2, we can conclude that there will always be
\begin{itemize}
  \item a 1-chain, $\gamma_x$, whose image is the boundary of the region under consideration and that satisfies Cx for the type I division.
  \item a 1-chain, $\gamma_y$, whose image is the boundary of the region under consideration and that satisfies Cy for the type II division, where it is \emph{not} necessary that $\gamma_x = \gamma_y$.
\end{itemize}
Figure~\ref{fig:typeIboundarychain} and Figure~\ref{fig:typeIIboundarychain} show two 1-chains that satisfy Cx and Cy for the type I and type II divisions of the annulus.
Notice that in this example, those two 1-chains are \emph{not equal} even though they have the same \emph{orientation} and \emph{image}.

Now, if we can state and formalise the equivalence between $\gamma_x$ and $\gamma_y$, and that this equivalence lifts to equal line integrals, we can obtain Green's theorem in terms of the region, which is our goal.
One way to formalise path equivalence is to explicitly define the notion of orientation.
Then the equivalence between $\gamma_x$ and $\gamma_y$ can be characterised by their having similar orientations and images.
An advantage of this approach is that it can capture equivalence in path orientations regardless of the path image.

However, we do not need this generality in the context of proving the equivalence of 1-chains that have the same image and orientation, especially that this generality will cost a lot of analytic and geometric complexities to be formalised.
Instead we choose to foramlise the notion of equivalence in terms of having a \textit{common subdivision}.
For example the 1-chain shown in Figure~\ref{fig:commonsubdiv} is a subdivision of each of the 1-chains in Figure~\ref{fig:typeIboundarychain} and Figure~\ref{fig:typeIIboundarychain} as well as the original boundary 1-chain in Figure~\ref{fig:region}, i.e. a common subdivision between the three 1-chains.
We now formally define the concept of a common subdivision between 1-chains, where we mainly focus on ``boundary'' 1-chains, defined as follows.
\begin{isabelle}
\ \ \isacommand{definition}\ boundary\_chain\ \isakeyword{where}\isanewline
\ \ "boundary\_chain\ s\ =\ (\isasymforall (k,\ \isasymgamma )\ \isasymin \ s.\ k\ =\ 1\ \isasymor \ k\ =\ -1)"
\end{isabelle}

First, we lift the \isa{path{\isacharunderscore}join} operator defined in the Isabelle/HOL multivariate analysis library, to act on 1-chains ordered into lists as follows.
\begin{isabelle}
\ \isacommand{fun}\ rec\_join\ ::\isanewline
\ "(int\ *(real\ \isasymRightarrow \ (real*real)))\ list\ \isasymRightarrow \ (real\ \isasymRightarrow \ (real\ *\ real))"\isanewline
\ \isakeyword{where}\isanewline
\ \ "rec\_join\ []\ =\ ({\isasymlambda}x.0)"\ |\isanewline
\ \ "rec\_join\ ((k,\ \isasymgamma )\ \#\ xs)\ =\isanewline
\ \ \ \ \ \ (if\ (k\ =\ 1)\ then\ \isasymgamma \ else\ reversepath\ \isasymgamma )\ \ +++\ (rec\_join\ xs)"
\end{isabelle}
To use the theory for paths developed in the multivariate analysis library, we need the joined chains to be piece-wise $C^1$ smooth in the sense that is defined in that library (\isa{valid\_path}).
A necessary condition for a path to be valid, is that the ending point of every piece of that path to be the starting point of the next.
Accordingly we define the following predicate for the validity 1-chains ordered into lists.
\begin{isabelle}
\isacommand{fun}\ valid\_chain\_list\ \isakeyword{where}\isanewline
\ "valid\_chain\_list\ []\ =\ True"\ |\isanewline
\ "valid\_chain\_list\ ((k,\ \isasymgamma )\#l)\ =\isanewline
\ \ ((\ if\ (k\ =\ 1)\ then\ (pathfinish\ \isasymgamma \ =\ pathstart\ (rec\_join\ l))\isanewline
\ \ \ \ \ else\ (pathfinish\ (reversepath\ \isasymgamma )\ =\ pathstart\ (rec\_join\ l)))\isanewline
\ \ \ \isasymand \ valid\_chain\_list\ l)"
\end{isabelle}
Based on those concepts we now define what it means for a 1-chain to be a subdivision of a path, which is a straightforward definition.
\begin{isabelle}
\isacommand{definition}\ chain\_subdiv\_path::\isanewline
\ "(real\ \isasymRightarrow \ real\ *\ real)\ \isasymRightarrow \ ((int\ *\ (real\ \isasymRightarrow \ real\ *\ real))\ set)\isanewline
\ \isasymRightarrow\ bool"\ \isakeyword{where}\isanewline
\ "chain\_subdiv\_path\ \isasymgamma \ one\_chain\isanewline
\ \ \ \ \ \ \ =\ (\isasymexists l.\ set\ l\ =\ one\_chain\ \isasymand \ distinct\ l\ \isasymand \ rec\_join\ l\ =\ \isasymgamma \ \isasymand \isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ \  valid\_chain\_list\ l)"
\end{isabelle}
We call a 1-chain~$\gamma$, a subdivision of another 1-chain~$\eta$, if one can map every cube in $\eta$ to a sub-chain of $\gamma$ that is a subdivision of it.
Formally this is defined as follows:
\begin{isabelle}
\ \isacommand{definition}\ chain\_subdiv\_chain\ \isakeyword{where}\isanewline
\ "chain\_subdiv\_chain\ one\_chain1\ subdiv\ =\isanewline
\ \ (\isasymexists f.\ ((\isasymUnion (f\ `\ one\_chain1))\ =\ subdiv)\ \isasymand \isanewline
\ \ \ \ \ \ \ \ (\isasymforall (k,\isasymgamma )\isasymin one\_chain1.\isanewline
\ \ \ \ \ \ \ \ \ \ \ if\ k\ =\ 1\ then\isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ chain\_subdiv\_path\ \isasymgamma \ (f(k,\ \isasymgamma ))\isanewline
\ \ \ \ \ \ \ \ \ \ \ else\isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ chain\_subdiv\_path\ (reversepath\ \isasymgamma )\ (f(k,\isasymgamma )))\ \isasymand \isanewline
\ \ \ \ \ \ \ \ (\isasymforall p\isasymin one\_chain1.\isanewline
\ \ \ \ \ \ \ \ \ \ \ \isasymforall p'\isasymin one\_chain1.\ p\ \isasymnoteq \ p'\ \isasymlongrightarrow \ f\ p\ \isasyminter \ f\ p'\ =\ \isacharbraceleft \isacharbraceright )\ \isasymand \isanewline
\ \ \ \ \ \ \ \ (\isasymforall x\ \isasymin \ one\_chain1.\ finite\ (f\ x)))"
\end{isabelle}
After proving that each of the previous notions of equivalence implies equality of line integrals, we define equivalence of 1-chains in terms of having a common subdivision, and prove that it implies equal line integrals.
We define it as having a boundary 1-chain that is a subdivision for each of the 1-chains under consideration.
Formally this definition and the equality of line integrals that it implies are as follows:

\begin{isabelle}
\isacommand{definition}\ common\_boundary\_sudivision\_exists\ \isakeyword{where}\isanewline
"common\_boundary\_sudivision\_exists\ one\_chain1\ one\_chain2\ =\ \isanewline
\ \ \ (\isasymexists subdiv.\ chain\_subdiv\_chain\ one\_chain1\ subdiv\ \isasymand \ \isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ chain\_subdiv\_chain\ one\_chain2\ subdiv\ \isasymand \isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ (\isasymforall (k,\ \isasymgamma )\ \isasymin \ subdiv.\ valid\_path\ \isasymgamma )\isasymand \isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ (boundary\_chain\ subdiv))"\isanewline
\isanewline
\isacommand{lemma}\ common\_subdivision\_imp\_eq\_line\_integral:\isanewline
\ \isakeyword{assumes}\ "(common\_boundary\_sudivision\_exists\isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ one\_chain1\ one\_chain2)"\isanewline
\ \ \ \ \ \ \ \ "boundary\_chain\ one\_chain1"\isanewline
\ \ \ \ \ \ \ \ "boundary\_chain\ one\_chain2"\isanewline
\ \ \ \ \ \ \ \ "\isasymforall (k,\ \isasymgamma )\isasymin one\_chain1.\ line\_integral\_exists\ F\ B\ \isasymgamma "\isanewline
\ \ \ \ \ \ \ \ "finite\ one\_chain1"\isanewline
\ \ \ \ \ \ \ \ "finite\ one\_chain2"\isanewline
\ \isakeyword{shows}\ "one\_chain\_line\_integral\ F\ B\ one\_chain1\ =\isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ one\_chain\_line\_integral\ F\ B\ one\_chain2"\ \ \ \ \ \ \isanewline
\ \ \ \ \ \ \ "\isasymforall (k,\ \isasymgamma )\isasymin one\_chain2.\ line\_integral\_exists\ F\ B\ \isasymgamma "
\end{isabelle}
\noindent Based on this lemma, finally, we prove the following statement of Green's theorem.
\begin{isabelle}
\ \isacommand{lemma}\ GreenThm\_typeI\_typeII\_divisible\_region:\isanewline
\ \isakeyword{assumes}\ "valid\_typeI\_division\ s\ twoChain\_typeI"\isanewline
\ \ \ \ \ \ "valid\_typeII\_division\ s\ twoChain\_typeII"\isanewline
\ \ \ \ \ \ "\isasymforall twoC\ \isasymin \ twoChain\_typeI.\isanewline
\ \ \ \ \ \ \ \ \ analytically\_valid\ (cubeImage\ twoC)\ ({\isasymlambda}x.\ (F\ x)\ \isasymbullet \ i)\ j"\isanewline
\ \ \ \ \ \ "\isasymforall twoC\ \isasymin \ twoChain\_typeII.\isanewline
\ \ \ \ \ \ \ \ \ analytically\_valid\ (cubeImage\ twoC)\ ({\isasymlambda}x.\ (F\ x)\ \isasymbullet \ j)\ i"\isanewline
\ \ \ \ \ \ "only\_vertical\_division\ \isasymgamma \_x\ twoChain\_typeI"\isanewline
\ \ \ \ \ \ "boundary\_chain\ \isasymgamma \_x"\isanewline
\ \ \ \ \ \ "only\_horizontal\_division\ \isasymgamma \_y\ twoChain\_typeII"\isanewline
\ \ \ \ \ \ "boundary\_chain\ \isasymgamma \_y"\isanewline
\ \ \ \ \ \ "common\_boundary\_sudivision\_exists\ \isasymgamma \_x\ \isasymgamma \_y"\ \ \ \ \ \ \ \ \ \ \ \ \ \ \isanewline
\ \isakeyword{shows}\ "integral\ s\ \isanewline
\ \ \ \ \ \ \ \ \ \ ({\isasymlambda}x.\ partial\_vector\_derivative\ ({\isasymlambda}x.\ (F\ x)\ \isasymbullet \ j)\ i\ x\ -\isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ partial\_vector\_derivative\ ({\isasymlambda}x.\ (F\ x)\ \isasymbullet \ i)\ j\ x)\isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ =\ one\_chain\_line\_integral\ F\ \isacharbraceleft i,\ j\isacharbraceright \ {\isasymgamma\_x}"\isanewline
\ \ \ \ \ \ "integral\ s\isanewline
\ \ \ \ \ \ \ \ \ \ ({\isasymlambda}x.\ partial\_vector\_derivative\ ({\isasymlambda}x.\ (F\ x)\ \isasymbullet \ j)\ i\ x\ -\isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ partial\_vector\_derivative\ ({\isasymlambda}x.\ (F\ x)\ \isasymbullet \ i)\ j\ x)\isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ =\ one\_chain\_line\_integral\ F\ \isacharbraceleft i,\ j\isacharbraceright \ {\isasymgamma\_y}"
\end{isabelle}
This theorem does not require the 1-chains \isa{{\isasymgamma}{\isacharunderscore}x} and \isa{{\isasymgamma}{\isacharunderscore}y} to have as their image exactly the boundary of the region.
However, of course it applies to the 1-chains if their image is the boundary of the region.
Accordingly it fits as Green's theorem for a region that can be divided into elementary regions just by vertical and horizontal slicing.

It is worth noting that although this statement seems to have a lot of assumptions, its analytic assumptions regarding the field are strictly more general than the those in \cite{zorich2004mathematical,protter2006basic}, where they retuire the field and both of its partial derivatives to be continuous in the region.
For the geometric assumptions, on the other hand, we have two extra requirements: the type I and type II divisions should be obtained using only vertical slicing and only horizontal slicing, respectively.
