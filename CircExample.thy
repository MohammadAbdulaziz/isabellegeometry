section \<open>The Disk Example\<close>

theory CircExample
  imports Green SymmetricR2Shapes

begin

locale disk = R2 +
  fixes d::real
  assumes d_gt_0: "0 < d"
begin

definition disk_y where
  "disk_y t = sqrt (1/4 - t * t)"

definition disk_cube where
  "disk_cube = (\<lambda>(t1,t2). let x = (t1 - 1/2) in (x * d, (2 * t2 - 1) * d * sqrt (1/4 - x * x)))"

lemma diff_divide_cancel: 
  fixes z::real shows  "z \<noteq> 0  \<Longrightarrow> (a * z - a * (b * z)) / z = (a - a * b)" 
  by (auto simp: field_simps)

lemma disk_cube_is_type_I:
  shows "typeI_twoCube (1::int, disk_cube) i j"
  unfolding typeI_twoCube.simps
proof (intro exI conjI ballI)
  have f01: "finite{-d/2,d/2}"
    by simp
  show "-d/2 \<noteq> d/2"
    using d_gt_0 by simp
  have "(\<lambda>x. c * sqrt (1/4 - (x/d) * (x/d))) piecewise_C1_differentiable_on {- d / 2..d / 2} \<union> {d / 2..- d / 2}" for c
    using d_gt_0 unfolding piecewise_C1_differentiable_on_def   
    apply (intro exI conjI)
      apply (rule ballI refl f01 derivative_intros continuous_intros | simp)+
    apply (auto simp: field_simps)
    by sos
  then show "(\<lambda>x. d * sqrt (1/4 - (x/d) * (x/d))) piecewise_C1_differentiable_on {- d / 2..d / 2} \<union> {d / 2..- d / 2}"
    "(\<lambda>x. -d * sqrt (1/4 - (x/d) * (x/d))) piecewise_C1_differentiable_on {- d / 2..d / 2} \<union> {d / 2..- d / 2}"
    by auto
  have "- d * sqrt (1/4 - x / d * (x / d)) \<le> d * sqrt (1/4 - x / d * (x / d))"
    if "x \<in> {- d / 2..d / 2} \<union> {d / 2..- d / 2}" for x
  proof -
    have *: "x^2 \<le> (d/2)^2"
      using real_sqrt_le_iff that by fastforce
    show ?thesis
      apply (rule mult_right_mono)
      using d_gt_0 * apply (simp_all add: divide_simps power2_eq_square)
      done
  qed
  then show "(\<forall>x\<in>{- d / 2..d / 2} \<union> {d / 2..- d / 2}. - d * sqrt (1 / 4 - x / d * (x / d)) \<le> d * sqrt (1 / 4 - x / d * (x / d))) \<or>
    (\<forall>x\<in>{- d / 2..d / 2} \<union> {d / 2..- d / 2}. d * sqrt (1 / 4 - x / d * (x / d)) \<le> - d * sqrt (1 / 4 - x / d * (x / d)))"
    by auto
  have " d \<noteq> 0 \<Longrightarrow> - d \<le> x * 2 \<Longrightarrow> x * 2 \<le> d \<Longrightarrow> 0 \<le> ((d * d - x * (x * 4)) / (d * (d * 4)))" for x by sos
  then show "(1::int) = (if - d / 2 < d / 2 then 1 else - 1) * (if \<forall>x\<in>{- d / 2..d / 2} \<union> {d / 2..- d / 2}. - d * sqrt (1 / 4 - x / d * (x / d)) \<le> d * sqrt (1 / 4 - x / d * (x / d)) then 1 else - 1)"
    by (auto simp add: disk_cube_def divide_simps algebra_simps diff_divide_cancel linepath_def Let_def i_is_x_axis j_is_y_axis d_gt_0)
qed (auto simp add: disk_cube_def divide_simps algebra_simps diff_divide_cancel linepath_def Let_def i_is_x_axis j_is_y_axis d_gt_0)

lemma disk_cube_valid_two_cube: "valid_two_cube disk_cube"
proof- 
  have "(- 1::int, \<lambda>y::real. (- (d/2), 0)) \<noteq> (- 1, \<lambda>x. ((x - 1/2) * d, d * sqrt (1/4 - (x - 1/2) * (x - 1/2))))"
       "(1::int, \<lambda>y. (d/2, 0)) \<noteq> (1, \<lambda>x. ((x - 1/2) * d, - (d * sqrt (1/4 - (x - 1/2) * (x - 1/2)))))"
    using d_gt_0  apply (auto simp add: algebra_simps)
    by (smt prod.simps(1) real_mult_less_iff1)+
  then show ?thesis
    by (auto simp add: valid_two_cube_def boundary_def horizontal_boundary.simps vertical_boundary.simps disk_cube_def Let_def)
qed

lemma typeII_disk_cube_valid_two_cube:
  shows "valid_two_cube (typeI_to_typeII disk_cube)"
proof- 
  have " (1::int, \<lambda>y. (0, - (d / 2))) \<noteq> (1::int, \<lambda>x. (- (d * sqrt (1 / 4 - (1 / 2 - x) * (1 / 2 - x))), (1 / 2 - x) * d))"
       "(- 1::int, \<lambda>y. (0, d / 2)) \<noteq> (- 1::int, \<lambda>x. (d * sqrt (1 / 4 - (1 / 2 - x) * (1 / 2 - x)), (1 / 2 - x) * d))"
    using d_gt_0  apply (auto simp add: algebra_simps)
    by (smt prod.simps(1) real_mult_less_iff1)+
  then show ?thesis
    by (auto simp add: valid_two_cube_def boundary_def horizontal_boundary.simps vertical_boundary.simps disk_cube_def typeI_to_typeII_def Let_def)
qed

lemma disk_bot_edges_neq_disk_top_edges: 
  "bot_edge disk_cube \<noteq>  top_edge disk_cube"
  "right_edge disk_cube \<noteq>  top_edge disk_cube"
  "left_edge disk_cube \<noteq>  top_edge disk_cube"
  "bot_edge disk_cube \<noteq>  left_edge disk_cube"
  "right_edge disk_cube \<noteq>  left_edge disk_cube"
  "bot_edge disk_cube \<noteq>  right_edge disk_cube"
  unfolding disk_cube_def bot_edge_def right_edge_def left_edge_def top_edge_def i_is_x_axis j_is_y_axis Let_def
  using d_gt_0 by(auto simp add: fun_eq_iff intro: exI[where x = "1/2"])

lemma disk_bot_edges_neq_disk_top_edges_typeII'':
  "bot_edge (typeI_to_typeII disk_cube) \<noteq>  top_edge (typeI_to_typeII disk_cube)"
  "right_edge (typeI_to_typeII disk_cube) \<noteq>  top_edge (typeI_to_typeII disk_cube)"
  "left_edge (typeI_to_typeII disk_cube) \<noteq>  top_edge (typeI_to_typeII disk_cube)"
  "bot_edge (typeI_to_typeII disk_cube) \<noteq>  left_edge (typeI_to_typeII disk_cube)"
  "right_edge (typeI_to_typeII disk_cube) \<noteq>  left_edge (typeI_to_typeII disk_cube)"
  "bot_edge (typeI_to_typeII disk_cube) \<noteq>  right_edge (typeI_to_typeII disk_cube)"
  unfolding disk_cube_def bot_edge_def right_edge_def left_edge_def top_edge_def i_is_x_axis j_is_y_axis typeI_to_typeII_def Let_def
  using d_gt_0 by(auto simp add: fun_eq_iff intro: exI[where x = 0])

definition disk_polar where
  "disk_polar t = ((d/2)  * cos (2 * pi * t), (d/2) * sin (2 * pi * t))"

lemma disk_polar_smooth: "(disk_polar) C1_differentiable_on {0..1}"
  unfolding disk_polar_def
  by simp

abbreviation "custom_arccos \<equiv> (\<lambda>x. (if -1 \<le> x \<and> x \<le> 1 then arccos x else (if x < -1 then -x + pi else 1 -x )))"

lemma cont_custom_arccos: 
  assumes "S \<subseteq> {-1..1}"
  shows "continuous_on S custom_arccos"
proof -
  have "continuous_on ({-1..1} \<union> {}) custom_arccos"
    by (auto intro!: continuous_on_cases continuous_intros)
  with assms show ?thesis
    using continuous_on_subset by auto 
qed

lemma custom_arccos_has_deriv: 
  assumes "- 1 < x" "x < 1"
  shows "DERIV custom_arccos x :> inverse (- sqrt (1 - x\<^sup>2))"
proof -
  have x1: "\<bar>x\<bar>\<^sup>2 < 1\<^sup>2"
    by (simp add: abs_less_iff abs_square_less_1 assms)
  show ?thesis
    apply (rule DERIV_inverse_function [where f=cos and a="-1" and b=1])
         apply (rule x1 derivative_eq_intros | simp add: sin_arccos)+
    using assms x1 cont_custom_arccos [of "{-1<..<1}"] 
         apply (auto simp: continuous_on_eq_continuous_at greaterThanLessThan_subseteq_atLeastAtMost_iff)
    done
qed

declare 
  custom_arccos_has_deriv[THEN DERIV_chain2, derivative_intros]
  custom_arccos_has_deriv[THEN DERIV_chain2, unfolded has_field_derivative_def, derivative_intros]

lemma sin_plus_half_pi: "sin (x + pi / 2) =  cos x"
  using sin_minus[where x = "x + pi/2"] sin_cos_eq[where x = "- (x + pi/2)"]
   by auto

lemma disk_boundary_reparams:
  shows rot_circ_left_edge_reparam_polar_circ_split:
    "reparam (coeff_cube_to_path (bot_edge(typeI_to_typeII disk_cube))) (coeff_cube_to_path (subcube (1 / 4)  (1 / 2) (1, disk_polar)) +++ (coeff_cube_to_path (subcube (1 / 2) (3 / 4) (1, disk_polar))))"
    (is ?P1)
    and circ_top_edge_reparam_polar_circ_split:
    "reparam (coeff_cube_to_path (top_edge(disk_cube))) (coeff_cube_to_path (subcube 0 (1 / 4) (1, disk_polar)) +++ (coeff_cube_to_path (subcube (1 / 4) (1 / 2) (1, disk_polar))))"
    (is ?P2)
    and circ_bot_edge_reparam_polar_circ_split:
    "reparam (coeff_cube_to_path (bot_edge(disk_cube))) (coeff_cube_to_path (subcube (1 / 2)  (3 / 4) (1, disk_polar)) +++ (coeff_cube_to_path (subcube (3 / 4) 1 (1, disk_polar))))"
    (is ?P3)
    and rot_circ_right_edge_reparam_polar_circ_split:
    "reparam (coeff_cube_to_path (top_edge(typeI_to_typeII disk_cube))) (coeff_cube_to_path (subcube (3/4) 1 (1, disk_polar)) +++ (coeff_cube_to_path (subcube 0 (1 / 4) (1, disk_polar))))"
    (is ?P4)
proof -
  define \<phi> where phi: "\<phi> = ((*) (1/pi) \<circ> custom_arccos \<circ> (\<lambda>t. 2 * x_coord (1 - t)))" 
  have phi_diff: "\<phi> piecewise_C1_differentiable_on {0..1}"
    unfolding piecewise_C1_differentiable_on_def
  proof
    show "continuous_on {0..1} \<phi>"
      unfolding x_coord_def phi
      by (intro continuous_on_compose cont_custom_arccos continuous_intros) auto
    have "\<phi> C1_differentiable_on {0..1} - {0,1}"
      unfolding x_coord_def piecewise_C1_differentiable_on_def C1_differentiable_on_def valid_path_def phi
      by (simp | rule has_vector_derivative_pair_within DERIV_image_chain derivative_eq_intros continuous_intros exI ballI conjI 
          | force simp add: field_simps | sos)+
    then show "\<exists>s. finite s \<and> \<phi> C1_differentiable_on {0..1} - s"
      by force
  qed
  have inj: "inj \<phi>"
    unfolding phi
    apply (intro comp_inj_on inj_on_cases inj_on_arccos)
          apply (auto simp add: inj_on_def x_coord_def)
    using pi_ge_zero apply auto[1]
     apply (smt arccos)
    by (smt arccos_lbound)
  have "\<phi> ` {0..1} = {0..1}"
  proof
    show "\<phi> ` {0..1} \<subseteq> {0..1}"
      unfolding phi
      by (auto simp add: x_coord_def divide_simps arccos_lbound arccos_bounded)
    have "arccos (1 - 2 * ((1 - cos (x * pi))/2)) = x * pi" if "0 \<le> x" "x \<le> 1" for x
      using that by (simp add: field_simps arccos_cos)
    then show "{0..1} \<subseteq> \<phi> ` {0..1}"
      unfolding phi
      apply (auto simp: x_coord_def o_def image_def)
      by (rule_tac x="(1 - cos (x * pi))/2" in bexI) auto
  qed
  then have bij_phi: "bij_betw \<phi> {0..1} {0..1}"
    unfolding bij_betw_def using inj inj_on_subset by blast 
  have phi01: "\<phi> -` {0..1} \<subseteq> {0..1}"
    unfolding phi
    by (auto simp add: subset_iff x_coord_def divide_simps)
  {
    fix x::real assume x: "x \<in> {0..1}"
    then have i: "- 1 \<le> (2 * x - 1)" "(2 * x - 1) \<le> 1"  by auto
    have ii: "cos (pi / 2 + arccos (1 - x * 2)) = -sin (arccos (1 - x * 2))"
      using minus_sin_cos_eq[symmetric, where ?x = "arccos (1 - x * 2)"]
      by (auto simp add: add.commute)
    have "2 * sqrt (x - x * x) = sqrt (4*x - 4*x * x)"
      by (metis real_sqrt_mult  mult.assoc real_sqrt_four  right_diff_distrib)
    also have "... = sqrt (1  - (2 * x - 1) * (2 * x - 1))"
      by (simp add: algebra_simps)
    finally have iii: "2 * sqrt (x - x * x) = cos (arcsin (2 * x - 1)) "" 2 * sqrt (x - x * x) =  sin (arccos (1 - 2 * x))"
      using arccos_minus[OF i]  unfolding minus_diff_eq sin_pi_minus
      by (simp add: cos_arcsin i power2_eq_square sin_arccos)+
    have rw: "- c = (b::real) \<Longrightarrow> c = -b" for b c by auto
    have 1: "(coeff_cube_to_path (bot_edge(typeI_to_typeII disk_cube))) x = ((coeff_cube_to_path (subcube (1/4) (1 / 2) (1, disk_polar)) +++ (coeff_cube_to_path (subcube (1 / 2) (3 / 4) (1, disk_polar)))) o \<phi>) x"
      unfolding phi bot_edge_def disk_cube_def subcube_def disk_polar_def subpath_def typeI_to_typeII_def prod.swap_def
      using d_gt_0 i x apply(auto simp add: joinpaths_def reversepath_def x_coord_def diff_divide_distrib algebra_simps)
      subgoal using ii iii by (metis mult.commute mult_minus_right)
      subgoal unfolding cos_sin_eq[where ?x = "- arccos (1 - x * 2)", simplified, symmetric] by (subst cos_arccos, auto simp add: algebra_simps)
      subgoal unfolding cos_minus[where x = "pi/2 -  arccos (1 - x * 2)", simplified] sin_cos_eq[where x = "arccos (1 - x * 2)", simplified, symmetric] by(metis iii mult.commute)
      subgoal unfolding sin_minus[where x = "pi / 2 - arccos (1 - x * 2)", simplified] cos_sin_eq[where x = "arccos (1 - x * 2)", simplified, symmetric] by (subst cos_arccos, auto simp add: algebra_simps divide_simps)
      done
    have 2: "coeff_cube_to_path (top_edge disk_cube) x = ((coeff_cube_to_path (subcube 0 (1/4) (1, disk_polar)) +++ (coeff_cube_to_path (subcube (1 / 4) (1 / 2) (1, disk_polar)))) o \<phi>) x"
      unfolding phi top_edge_def disk_cube_def subcube_def disk_polar_def subpath_def 
      using x iii by (auto simp add: joinpaths_def reversepath_def x_coord_def diff_divide_distrib algebra_simps)
    have 3: "coeff_cube_to_path (bot_edge disk_cube) x = ((coeff_cube_to_path (subcube (1/2) (3/4) (1, disk_polar)) +++ (coeff_cube_to_path (subcube (3 / 4) 1 (1, disk_polar)))) \<circ> \<phi>) x"
      unfolding phi bot_edge_def disk_cube_def subcube_def disk_polar_def subpath_def 
      using x  iii
      by (auto simp add: x_coord_def algebra_simps diff_divide_distrib power2_eq_square joinpaths_def reversepath_def)
    have 4: "coeff_cube_to_path (top_edge(typeI_to_typeII disk_cube)) x = ((coeff_cube_to_path (subcube (3/4) 1 (1, disk_polar)) +++ (coeff_cube_to_path (subcube 0 (1 / 4) (1, disk_polar)))) o \<phi>) x"
      unfolding phi top_edge_def disk_cube_def subcube_def disk_polar_def subpath_def typeI_to_typeII_def prod.swap_def
      using d_gt_0 i x apply(auto simp add: joinpaths_def reversepath_def x_coord_def diff_divide_distrib algebra_simps)
      subgoal using cos_periodic_pi[where x  = "arccos (1 - x * 2) + pi/2"] minus_sin_cos_eq[where x  = "arccos (1 - x * 2)", symmetric] iii by (simp add: ac_simps)
      subgoal using sin_periodic_pi[where x = "arccos (1 - x * 2) + pi/2"] using  sin_plus_half_pi[where x = "arccos (1 - x * 2)"] by (simp add: ac_simps right_diff_distrib)
      subgoal using sin_cos_eq[where x = "arccos (1 - x * 2)"] cos_minus[where x = "arccos (1 - x * 2) - pi/2"] iii by (simp add: ac_simps )
      subgoal using cos_sin_eq[where x = "arccos (1 - x * 2)"] rw[OF sin_minus[where x = "arccos (1 - x * 2) - pi/2", symmetric, simplified]] by (simp add: ac_simps right_diff_distrib)
      done
    note 1 2 3 4
  } note * = this
  show ?P1 ?P2 ?P3 ?P4
    unfolding reparam_def
    by(rule exI conjI ballI impI phi_diff bij_phi phi01 * | force simp add: x_coord_def phi)+
qed

definition disk_cube_boundary_to_polardisk where
  "disk_cube_boundary_to_polardisk \<gamma> \<equiv>
     if (\<gamma> = (top_edge disk_cube)) then
           {subcube 0 (1/4) (1, disk_polar), subcube (1/4) (1/2) (1, disk_polar)}
     else if (\<gamma> = (bot_edge disk_cube)) then
           {subcube (1/2) (3/4) (1, disk_polar), subcube (3/4) 1 (1, disk_polar)}
     else {}"

definition rot_disk_cube_boundary_to_polardisk where
  "rot_disk_cube_boundary_to_polardisk \<gamma> \<equiv>
     if (\<gamma> = bot_edge (typeI_to_typeII disk_cube))  then
           {subcube (1/4)  (1/2) (1, disk_polar), subcube (1/2) (3/4) (1, disk_polar)}
     else if (\<gamma> = top_edge (typeI_to_typeII disk_cube)) then
           {subcube (3/4) 1 (1, disk_polar), subcube 0 (1/4) (1, disk_polar)}
     else {}"

lemma disk_arcs_neq:
  assumes "0 \<le> k" "k \<le> 1" "0 \<le> n" "n \<le> 1" "n < k" "k + n < 1"
  shows "subcube k m (1, disk_polar) \<noteq> subcube n q (1, disk_polar)"
proof -
  have "cos (2 * pi * k) \<noteq> cos(2 * pi * n)"
    unfolding cos_eq
  proof safe
    show False if "2 * pi * k = 2 * pi * n + 2 * m * pi" "m \<in> \<int>" for m
    proof -
      have "2 * pi * (k - n ) = 2 * m * pi"
        using distrib_left that by (simp add: left_diff_distrib mult.commute)
      then have a: "m = (k - n)" by auto
      have "\<lfloor>k - n\<rfloor> = 0 "
        using assms by (simp add: floor_eq_iff)
      then have "k - n \<notin> \<int>"
        using assms by (auto simp only: frac_eq_0_iff[symmetric] frac_def)
      then show False using that a by auto
    qed
    show False if "2 * pi * k = - (2 * pi * n) + 2 * m * pi" "m \<in> \<int>" for m
    proof -
      have "2 * pi * (k + n ) = 2 * m * pi"
        using that by (auto simp add: distrib_left)
      then have a: "m = (k + n)" by auto
      have "\<lfloor>k + n\<rfloor> = 0 "
        using assms by (simp add: floor_eq_iff)
      then have "k + n \<notin> \<int>"
        using Ints_def assms by force
      then show False using that a by auto
    qed
  qed
  then have *: "(\<lambda>x. (d * cos (2 * pi * ((m - k) * x + k))/2, d * sin (2 * pi * ((m - k) * x + k))/2)) 0  \<noteq> (\<lambda>x. (d * cos (2 * pi * ((q - n) * x + n))/2, d * sin (2 * pi * ((q - n) * x + n))/2)) 0"
    using d_gt_0 by auto
  then show ?thesis
    apply (simp add: subcube_def subpath_def disk_polar_def)
    by (metis *)
qed

lemma disk_arcs_neq_2:
  assumes "0 \<le> k" "k \<le> 1" "0 \<le> n" "n \<le> 1" "n < k" "0 < n" and kn12: "1/2 < k + n" and "k + n < 3/2"
  shows "subcube k m (1, disk_polar) \<noteq> subcube n q (1, disk_polar)"
proof (simp add: subcube_def subpath_def disk_polar_def)
  have "sin (2 * pi * k) \<noteq> sin(2 * pi * n)"
    unfolding sin_eq
  proof safe
    show False if "m \<in> \<int>" "2 * pi * k = 2 * pi * n + 2 * m * pi" for m
    proof -
      have "2 * pi * (k - n) = 2 * m * pi"
        using that by (simp add: left_diff_distrib mult.commute)                      
      then have a: "m = (k - n)" by auto
      have "\<lfloor>k - n\<rfloor> = 0 "
        using assms by (simp add: floor_eq_iff)
      then have "k - n \<notin>  \<int>"
        using assms by (auto simp only: frac_eq_0_iff[symmetric] frac_def )
      then show False using that a by auto
    qed
    show False if "2 * pi * k = - (2 * pi * n) + (2 * m + 1) * pi" "m \<in> \<int>" for m
    proof -
      have i: "\<And>pi. 0 < pi \<Longrightarrow> 2 * pi * (k + n ) = 2 * m * pi + pi \<Longrightarrow> m = (k + n) - 1/2"
        by (sos "(((((A<0 * A<1) * R<1) + ([1/2] * A=0))) & ((((A<0 * A<1) * R<1) + ([~1/2] * A=0))))")
      have "2 * pi * (k + n) = 2 * m * pi + pi"
        using that by (simp add: algebra_simps)
      then have a: "m = (k + n) - 1/2" using i[OF pi_gt_zero] by fastforce
      have "\<lfloor>k + n - 1/2\<rfloor> = 0 "
        using assms by (auto simp add: floor_eq_iff)
      then have "k + n - 1/2 \<notin> \<int>"
        by (metis Ints_cases add.commute add.left_neutral add_diff_cancel_left' add_diff_eq kn12 floor_of_int of_int_0 order_less_irrefl)
      then show False using that a by auto
    qed
  qed
  then have "(\<lambda>x. (d * cos (2 * pi * ((m - k) * x + k))/2, d * sin (2 * pi * ((m - k) * x + k))/2)) 0  \<noteq> (\<lambda>x. (d * cos (2 * pi * ((q - n) * x + n))/2, d * sin (2 * pi * ((q - n) * x + n))/2)) 0"
    using d_gt_0 by auto
  then show "(\<lambda>x. (d * cos (2 * pi * ((m - k) * x + k))/2, d * sin (2 * pi * ((m - k) * x + k))/2)) \<noteq> (\<lambda>x. (d * cos (2 * pi * ((q - n) * x + n))/2, d * sin (2 * pi * ((q - n) * x + n))/2))" 
    by metis
qed

lemma disk_cube_is_only_vertical_div_of_rot:
  shows "only_vertical_division' (\<partial>disk_cube) {(1, typeI_to_typeII disk_cube)}"
  unfolding only_vertical_division'_def
proof (rule exI [of _ "{}"], simp, intro conjI ballI)
  show "finite (\<partial>disk_cube)"
    unfolding boundary_edges by auto
  show "boundary_chain (\<partial>disk_cube)"
    by (simp add: two_cube_boundary_is_boundary)
  show "\<And>x. x \<in> \<partial>disk_cube \<Longrightarrow> case x of (k, x) \<Rightarrow> valid_path x"
    using typeI_edges_are_valid_paths_prod[OF disk_cube_is_type_I] by blast
  let ?\<V> = "\<partial>disk_cube"
  show "common_subdiv_exists (two_chain_horizontal_boundary {(1, typeI_to_typeII disk_cube)}) (\<partial> disk_cube) \<or>
               common_reparam_exists (\<partial> disk_cube) (two_chain_horizontal_boundary {(1, typeI_to_typeII disk_cube)})"
    unfolding common_reparam_exists_def
  proof (intro exI conjI HOL.disjI2)
    let ?pi = "{left_edge disk_cube, right_edge disk_cube}"
    let ?subdiv = "{(subcube 0 (1/4) (1, disk_polar)),
                    (subcube (1/4) (1/2) (1, disk_polar)),
                    (subcube (1/2) (3/4) (1, disk_polar)),
                    (subcube (3/4) 1 (1, disk_polar))}"
    show "(\<forall>(k, \<gamma>)\<in>?subdiv. \<gamma> C1_differentiable_on {0..1})"
      using subpath_smooth[OF disk_polar_smooth] by (auto simp add: subcube_def)
    have 1: "finite ?subdiv" by auto
    show "boundary_chain ?subdiv"
      by (simp add: boundary_chain_def subcube_def)
    show "chain_reparam_chain (\<partial>disk_cube - ?pi) ?subdiv"
      unfolding chain_reparam_chain_def
    proof (intro exI conjI impI)
      have 1: "(\<partial>disk_cube) - ?pi = {top_edge disk_cube, bot_edge disk_cube}"
        unfolding boundary_edges apply auto using disk_bot_edges_neq_disk_top_edges by auto
      let ?f = "disk_cube_boundary_to_polardisk"
      show "UNION (\<partial>disk_cube - ?pi) ?f = ?subdiv"
        unfolding 1
        by (auto simp add: disk_bot_edges_neq_disk_top_edges disk_cube_boundary_to_polardisk_def boundary_edges)
      show "\<forall>cube\<in>\<partial>disk_cube - ?pi. chain_reparam_path (coeff_cube_to_path cube) (disk_cube_boundary_to_polardisk cube)"
        unfolding 1 disk_cube_boundary_to_polardisk_def chain_reparam_path_def
      proof (simp, intro conjI exI impI)
        let ?e1 = "(coeff_cube_to_path (subcube 0 (1 / 4) (1, disk_polar)) +++ (coeff_cube_to_path (subcube (1 / 4) (1 / 2) (1, disk_polar))))"
        let ?e2 = "(coeff_cube_to_path (subcube (1 / 2) (3 / 4) (1, disk_polar)) +++ coeff_cube_to_path (subcube (3 / 4) 1 (1, disk_polar)))"
        show "chain_subdiv_path ?e1 {subcube 0 (1 / 4) (1, disk_polar), subcube (1 / 4) (1 / 2) (1, disk_polar)}"
             "chain_subdiv_path ?e2 {subcube (1 / 2) (3 / 4) (1, disk_polar), subcube (3 / 4) 1 (1, disk_polar)}"
          by (intro chain_subdiv_path.intros, (simp, subst neq_commute, simp add: disk_arcs_neq disk_arcs_neq_2) | (simp add: pathfinish_def pathstart_def subcube_def subpath_def))+
        then show "chain_subdiv_path ?e1 {subcube 0 (1 / 4) (1, disk_polar), subcube (1 / 4) (1 / 2) (1, disk_polar)}" by simp          
        show "reparam (coeff_cube_to_path (top_edge disk_cube)) ?e1"
             "reparam (coeff_cube_to_path (bot_edge disk_cube)) ?e2"
          using disk_boundary_reparams
          unfolding top_edge_def bot_edge_def
          by auto
        then show "reparam (coeff_cube_to_path (top_edge disk_cube)) ?e1"
          by simp
      qed
      show "pairwise (\<lambda>p p'. disk_cube_boundary_to_polardisk p \<inter> disk_cube_boundary_to_polardisk p' = {}) (\<partial> disk_cube - {left_edge disk_cube, right_edge disk_cube})"
        unfolding 1 pairwise_def
        using disk_arcs_neq disk_arcs_neq_2
        apply (auto simp add: disk_cube_boundary_to_polardisk_def boundary_edges)
        by (smt add_divide_distrib divide_self divide_cancel_left half_bounded_equal le_divide_eq_1_pos zero_le_divide_iff divide_pos_pos le_divide_eq_1_pos divide_pos_pos le_divide_eq_1)+
    qed
    let ?pj = "{left_edge (typeI_to_typeII disk_cube), right_edge (typeI_to_typeII disk_cube)}"
    show "chain_reparam_chain (two_chain_horizontal_boundary {(1, typeI_to_typeII disk_cube)} - ?pj) ?subdiv"
      unfolding chain_reparam_chain_def
    proof (intro exI conjI impI)
      let ?f = "rot_disk_cube_boundary_to_polardisk"
      have 1: "(two_chain_horizontal_boundary {(1, typeI_to_typeII disk_cube)}) - ?pj = {top_edge (typeI_to_typeII disk_cube), bot_edge (typeI_to_typeII disk_cube)}"
        unfolding two_chain_horizontal_boundary_def horizontal_boundary_edges rot_disk_cube_boundary_to_polardisk_def
        apply auto
        using disk_bot_edges_neq_disk_top_edges_typeII'' by auto
      show "UNION ((two_chain_horizontal_boundary {(1, typeI_to_typeII disk_cube)}) - ?pj) ?f = ?subdiv"
        unfolding 1
        apply (auto simp add: rot_disk_cube_boundary_to_polardisk_def two_chain_horizontal_boundary_def horizontal_boundary_edges)
        using disk_bot_edges_neq_disk_top_edges_typeII'' by auto
      show "\<forall>cube\<in>((two_chain_horizontal_boundary {(1, typeI_to_typeII disk_cube)}) - ?pj). chain_reparam_path (coeff_cube_to_path cube) (rot_disk_cube_boundary_to_polardisk cube)"
        unfolding 1 rot_disk_cube_boundary_to_polardisk_def chain_reparam_path_def
      proof (simp, intro conjI exI impI)
        let ?e1 = "(coeff_cube_to_path (subcube (1 / 4) (1 / 2) (1, disk_polar)) +++ coeff_cube_to_path (subcube (1 / 2) (3 / 4) (1, disk_polar)))"
        show "chain_subdiv_path ?e1 {subcube (1 / 4) (1 / 2) (1, disk_polar), subcube (1 / 2) (3 / 4) (1, disk_polar)}"
          by (intro chain_subdiv_path.intros, (simp, subst neq_commute, simp add: disk_arcs_neq disk_arcs_neq_2) | (simp add: pathfinish_def pathstart_def subcube_def subpath_def))+
        then show "chain_subdiv_path ?e1 {subcube (1 / 4) (1 / 2) (1, disk_polar), subcube (1 / 2) (3 / 4) (1, disk_polar)}"
          .
        let ?e2 = "(coeff_cube_to_path (subcube (3 / 4) 1 (1, disk_polar)) +++ coeff_cube_to_path (subcube 0 (1 / 4) (1, disk_polar)))"
        show "chain_subdiv_path ?e2 {subcube (3 / 4) 1 (1, disk_polar), subcube 0 (1 / 4) (1, disk_polar)}"
          by (intro chain_subdiv_path.intros, (simp add: disk_arcs_neq disk_arcs_neq_2) | (simp add: pathfinish_def pathstart_def subcube_def subpath_def disk_polar_def))+
        show "reparam (coeff_cube_to_path (bot_edge (typeI_to_typeII disk_cube))) ?e1"
             "reparam (coeff_cube_to_path (top_edge (typeI_to_typeII disk_cube))) ?e2"
          using disk_boundary_reparams
          unfolding top_edge_def bot_edge_def
          by auto
        then show "reparam (coeff_cube_to_path (bot_edge (typeI_to_typeII disk_cube))) ?e1" by simp
      qed
      show "pairwise (\<lambda>p p'. rot_disk_cube_boundary_to_polardisk p \<inter> rot_disk_cube_boundary_to_polardisk p' = {})  (two_chain_horizontal_boundary {(1, typeI_to_typeII disk_cube)} - ?pj)"
        unfolding 1 pairwise_def unfolding rot_disk_cube_boundary_to_polardisk_def two_chain_horizontal_boundary_def horizontal_boundary_edges
        using disk_arcs_neq disk_arcs_neq_2 apply auto
        using disk_arcs_neq_2 [symmetric] by auto
    qed
    show "\<forall>(k, \<gamma>)\<in>{left_edge disk_cube, right_edge disk_cube}. point_path \<gamma>"
      "\<forall>(k, \<gamma>)\<in>{left_edge (typeI_to_typeII disk_cube), right_edge (typeI_to_typeII disk_cube)}. point_path \<gamma>"
      unfolding left_edge_def disk_cube_def right_edge_def point_path_def typeI_to_typeII_def
      by auto
  qed 
qed

lemma disk_swap_eq_img: "cubeImage (disk_cube) = cubeImage (prod.swap o disk_cube)"
proof-
  have " (\<lambda>(x, y). ((x - 1 / 2) * d, (2 * y - 1) * d * sqrt (1 / 4 - (x - 1 / 2) * (x - 1 / 2)))) ` Green.unit_cube =
    (prod.swap \<circ> (\<lambda>(x, y). ((x - 1 / 2) * d, (2 * y - 1) * d * sqrt (1 / 4 - (x - 1 / 2) * (x - 1 / 2))))) ` Green.unit_cube"
    unfolding prod.swap_def image_def
    using d_gt_0
    apply (auto simp add: case_prod_unfold)
  proof-
    fix a b::real
    define x where "x \<equiv> (2 * b - 1) * sqrt (1 / 4 - (a - 1 / 2) * (a - 1 / 2)) + 1/2"
    assume ass: "0 \<le> a" "a \<le> 1" "0 \<le> b" "b \<le> 1"
    then have x_sound: "(2 * b - 1) * sqrt (1 / 4 - (a - 1 / 2) * (a - 1 / 2)) = (x - 1 / 2)"
      unfolding x_def by auto
    define y where "y = (((a - 1 / 2)) / sqrt (1 / 4 - (x - 1 / 2) * (x - 1 / 2)) + 1) / 2"
    have y_sound: "a - 1 / 2 = (2 * y - 1) * sqrt (1 / 4 - (x - 1 / 2) * (x - 1 / 2))"
      unfolding y_def unfolding x_def
      using ass by sos
    have x: "x \<in> {0..1}"
    proof-
      have rw: "sqrt (1/4) =  ((1/2)::real)" using real_sqrt_abs2[of "(1/2) ::real"] by auto
      have "(1 / 4 - (a - 1 / 2) * (a - 1 / 2)) \<le> 1/2 * 1/2"
        using ass
        by auto
      then have "sqrt (1 / 4 - (a - 1 / 2) * (a - 1 / 2)) \<le> 1/2"
        unfolding rw[symmetric] real_sqrt_le_iff by auto
      moreover have "0 \<le> (1 / 4 - (a - 1 / 2) * (a - 1 / 2))" using ass by sos
      moreover have "(2 * b - 1) * k + 1/2 \<ge> 0" "(2 * b - 1) * k + 1/2 \<le> 1" if "k \<le> 1/2" "0 \<le> k" for k
        using that apply(auto simp add: algebra_simps) using ass by sos+
      ultimately show ?thesis unfolding x_def by auto
    qed
    have y: "y \<in> {0..1}"
    proof-
      have rw: "\<bar>a - a * a\<bar> = a - a * a" using ass
        by (simp add: mult_left_le_one_le)
      have rw2: "sqrt ((1/2 - a) * (1/2 - a)) = abs (1/2 - a)"
        "sqrt ((1/2 - a) * (1/2 - a)) = abs (a - 1/2)"
        using real_sqrt_abs2[of "1/2 - a"] by auto
      {assume "x \<noteq> 0 "" x \<noteq> 1 "
        then have rw3: " (1/2 - a) * (1/2 - a) \<le> ((x * 4 - x * (x * 4)) / 4)"
          unfolding x_def
          using ass
          apply(auto simp add: algebra_simps rw)
          by (smt affine_ineq mult_left_le_one_le ordered_comm_semiring_class.comm_mult_left_mono)
        have 0: "(1/2 - a) \<le> sqrt ((x * 4 - x * (x * 4)) / 4)"
          "(a - 1/2) \<le> sqrt ((x * 4 - x * (x * 4)) / 4)"
          by(rule ordered_ab_group_add_abs_class.abs_le_D1, simp only: rw3 real_sqrt_le_iff rw2[symmetric])+}
      then show ?thesis
        unfolding y_def
        using x
        by (auto simp add: algebra_simps divide_simps)
    qed
    then show "\<exists>x\<in>Green.unit_cube. a - 1 / 2 = (2 * snd x - 1) * sqrt (1 / 4 - (fst x - 1 / 2) * (fst x - 1 / 2)) \<and> (2 * b - 1) * sqrt (1 / 4 - (a - 1 / 2) * (a - 1 / 2)) = fst x - 1 / 2"
              "\<exists>x\<in>Green.unit_cube. (2 * b - 1) * sqrt (1 / 4 - (a - 1 / 2) * (a - 1 / 2)) = fst x - 1 / 2 \<and> a - 1 / 2 = (2 * snd x - 1) * sqrt (1 / 4 - (fst x - 1 / 2) * (fst x - 1 / 2))"
      using x x_sound y_sound
      by (metis box_real(2) cbox_Pair_iff fst_conv snd_conv)+
  qed
  then show ?thesis
    unfolding cubeImage_def disk_cube_def typeI_to_typeII_def 
    by metis
qed

lemma GreenThm_disk:
  assumes "analytically_valid (cubeImage (disk_cube)) (F\<^bsub>i\<^esub>) j i"
    "analytically_valid (cubeImage (disk_cube)) (F\<^bsub>j\<^esub>) i j"
  shows "integral (cubeImage (disk_cube)) (\<lambda>x. \<partial> F\<^bsub>j\<^esub> / \<partial> i |\<^bsub>x\<^esub> - \<partial> F\<^bsub>i\<^esub> / \<partial> j |\<^bsub>x\<^esub> ) =  \<^sup>H\<integral>\<^bsub>\<partial>disk_cube\<^esub> F \<downharpoonright>\<^bsub>{i, j}\<^esub>"
proof -
  have "integral (cubeImage (disk_cube)) (\<lambda>x. real_of_int(chain_orientation {(1, typeI_to_typeII disk_cube)}) * \<partial> F\<^bsub>j\<^esub> / \<partial> i |\<^bsub>x\<^esub> - real_of_int(chain_orientation {(1, disk_cube)}) * \<partial> F\<^bsub>i\<^esub> / \<partial> j |\<^bsub>x\<^esub> ) =  \<^sup>H\<integral>\<^bsub>\<partial>disk_cube\<^esub> F \<downharpoonright>\<^bsub>{i, j}\<^esub>"
  proof(intro green_typeI_typeII_chain.GreenThm_typeI_typeII_divisible_region_2'(1))
    show "green_typeI_typeII_chain (cubeImage disk_cube) i j F {(1, disk_cube)} {(1, typeI_to_typeII disk_cube)}"
      unfolding green_typeI_typeII_chain_def
    proof(intro conjI)
      show *: " i_j_orthonorm i j" using R2.i_j_orthonorm[OF R2_axioms] by auto
      show "green_typeI_chain i j F {(1, disk_cube)} (cubeImage disk_cube)"
        unfolding green_typeI_chain_def
      proof(intro conjI * )
        show "green_typeI_chain_axioms i j F {(1, disk_cube)} (cubeImage disk_cube)"
          unfolding green_typeI_chain_axioms_def gen_division_def valid_two_chain_def
          by(auto intro: conjI assms(1) disk_cube_is_type_I disk_cube_valid_two_cube)
      qed
      show "green_typeI_chain (- j) i F {(1, typeI_to_typeII disk_cube)} (cubeImage disk_cube)"
        unfolding green_typeI_chain_def
      proof(intro conjI R2.neg_j_i_orthonorm'[OF R2_axioms])
        show "green_typeI_chain_axioms (- j) i F {(1, typeI_to_typeII disk_cube)} (cubeImage disk_cube)"
          unfolding green_typeI_chain_axioms_def gen_division_def valid_two_chain_def
        proof(intro conjI)
          show "\<forall>twoCube\<in>{(1, typeI_to_typeII disk_cube)}. typeI_twoCube twoCube (- j) i"
            using typeI_to_typeII_works[OF disk_cube_is_type_I] by simp
          show "UNION {(1, typeI_to_typeII disk_cube)} (cubeImage \<circ> snd) = cubeImage disk_cube"
            using disk_swap_eq_img rev_orient_same_img[OF swap_neg_typeI_is_typeII[OF disk_cube_is_type_I]]
            unfolding typeI_to_typeII_def by simp
          show "\<forall>(oreint, C)\<in>{(1, typeI_to_typeII disk_cube)}. valid_two_cube C"
            unfolding valid_two_cube_def boundary_edges
            using disk_bot_edges_neq_disk_top_edges_typeII''
            by simp
          show "\<forall>(orient, C)\<in>{(1, typeI_to_typeII disk_cube)}. analytically_valid (cubeImage C) ((F\<^bsub>- j\<^esub>)) i (- j)"
            using anal_valid_j_neg_j[OF assms(2)] disk_swap_eq_img
            using rev_orient_same_img[OF swap_typeI_is_typeII[OF disk_cube_is_type_I]]
            unfolding typeI_to_typeII_def
            by simp
        qed auto
      qed
    qed
    show "only_vertical_division' (\<partial>disk_cube) {(1, disk_cube)}"
      apply(rule twoChainVertDiv_of_itself'[where C = "{(1, disk_cube)}", unfolded two_chain_boundary_def , simplified, OF _ disk_cube_is_type_I])
      using typeI_edges_are_valid_paths[OF disk_cube_is_type_I]
      by auto
    show "only_vertical_division' (\<partial>disk_cube) {(1, typeI_to_typeII disk_cube)}"
      using disk_cube_is_only_vertical_div_of_rot
      by auto
    show "common_subdiv_exists (\<partial>disk_cube) (\<partial>disk_cube)"
      using typeI_edges_are_valid_paths_prod[OF disk_cube_is_type_I] gen_common_boundary_sudivision_exists_refl_twochain_boundary[OF _ two_cube_boundary_is_boundary]
      by blast
  qed (auto simp add: two_cube_boundary_is_boundary)
  moreover have "chain_orientation {(1::int, R2.typeI_to_typeII disk_cube)} = (1::int)"
    apply(rule valid_two_chain_orient'[symmetric])
    unfolding valid_two_chain_def valid_two_cube_def boundary_edges
    using disk_bot_edges_neq_disk_top_edges_typeII''
    by (auto simp: card_insert_if  split: if_split_asm)
  moreover have "chain_orientation {(1::int, disk_cube)} = (1::int)"
    apply(rule valid_two_chain_orient'[symmetric])
    using disk_cube_valid_two_cube
    unfolding valid_two_chain_def R2.typeI_to_typeII_def by (auto simp add: o_def)
  ultimately show ?thesis by auto
qed
end
end