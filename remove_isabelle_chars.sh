#!/bin/bash

sed -i 's/{\\isacharunderscore}/\\_/g' $1
sed -i 's/{\\isachardoublequoteopen}/"/g' $1
sed -i 's/{\\isachardoublequoteclose}/"/g' $1
sed -i 's/{\\isacharparenright}/)/g' $1
sed -i 's/{\\isacharparenleft}/(/g' $1
sed -i 's/{\\isacharcomma}/,/g' $1
sed -i 's/{\\isacharequal}/=/g' $1
sed -i 's/{\\isachardot}/./g' $1
sed -i 's/{\\isacharcolon}/:/g' $1
sed -i 's/{\\isacharbraceleft}/\\{/g' $1
sed -i 's/{\\isacharbraceright}/\\}/g' $1
sed -i 's/{\\isacharminus}/-/g' $1
sed -i 's/{\\isacharplus}/+/g' $1
sed -i 's/{\\isacharasterisk}/*/g' $1
sed -i 's/{\\isacharslash}/\//g' $1
