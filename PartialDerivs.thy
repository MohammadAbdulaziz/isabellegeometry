theory PartialDerivs
  imports Derivs General_Utils
begin

definition has_partial_derivative:: "(('a::euclidean_space) \<Rightarrow> 'b::euclidean_space) \<Rightarrow> 'a \<Rightarrow> ('a \<Rightarrow> 'b) \<Rightarrow> ('a) \<Rightarrow> bool" where
  "has_partial_derivative F b F' a
        \<equiv> ((\<lambda>x::'a::euclidean_space. F( (a - ((a \<bullet> b) *\<^sub>R b)) + (x \<bullet> b) *\<^sub>R b))
                has_derivative F') (at a)"

definition has_partial_vector_derivative:: "('a::euclidean_space \<Rightarrow> 'b::euclidean_space) \<Rightarrow> 'a \<Rightarrow> 'b \<Rightarrow> 'a \<Rightarrow> bool" where
  "has_partial_vector_derivative F b F' a
        \<equiv> ((\<lambda>x. F(a - (a \<bullet> b) *\<^sub>R b + x *\<^sub>R b ))
                has_vector_derivative F') (at (a \<bullet> b))"

lemma has_partial_vector_derivative_def_2:
  "has_partial_vector_derivative F base_vec F' a =
            ((\<lambda>x. F(a + x *\<^sub>R base_vec )) has_vector_derivative F') (at 0)"
  unfolding has_partial_vector_derivative_def
proof
  define f where "f = (%x. x + a \<bullet> base_vec)"
  have *: "(f has_derivative (\<lambda>x. x *\<^sub>R 1)) (at 0)"
    unfolding f_def has_vector_derivative_def
    apply(intro derivative_eq_intros)
    by auto
  assume "((\<lambda>x. F (a - (a \<bullet> base_vec) *\<^sub>R base_vec + x *\<^sub>R base_vec)) has_vector_derivative F') (at (a \<bullet> base_vec))"
  then have **: "((\<lambda>x. F (a - (a \<bullet> base_vec) *\<^sub>R base_vec + x *\<^sub>R base_vec)) has_derivative (\<lambda>x. x *\<^sub>R F')) (at (f 0))"
    unfolding has_vector_derivative_def f_def
    by auto
  show " ((\<lambda>x. F (a + x *\<^sub>R base_vec)) has_vector_derivative F') (at 0)"
    using diff_chain_at[OF * **]
    unfolding f_def has_vector_derivative_def o_def by (auto simp add: algebra_simps)
next
  define f where "f = (%x. x - a \<bullet> base_vec)"
  have *: "(f has_derivative (\<lambda>x. x *\<^sub>R 1)) (at (a \<bullet> base_vec))"
    unfolding f_def has_vector_derivative_def
    apply(intro derivative_eq_intros)
    by auto
  assume "((\<lambda>x. F (a + x *\<^sub>R base_vec)) has_vector_derivative F') (at 0) "
  then have **: "((\<lambda>x. F (a + x *\<^sub>R base_vec)) has_derivative (\<lambda>x. x *\<^sub>R F')) (at (f (a \<bullet> base_vec)))"
    unfolding has_vector_derivative_def f_def
    by auto
  show "((\<lambda>x. F (a - (a \<bullet> base_vec) *\<^sub>R base_vec + x *\<^sub>R base_vec)) has_vector_derivative F') (at (a \<bullet> base_vec))"
    using diff_chain_at[OF * **]
    unfolding f_def has_vector_derivative_def o_def by (auto simp add: algebra_simps)
qed

definition partially_vector_differentiable where
  "partially_vector_differentiable F b p \<equiv> (\<exists>F'. has_partial_vector_derivative F b F' p)"

lemma partially_differentiable_imp_differentiable: "partially_vector_differentiable f i x \<Longrightarrow> ((\<lambda>xa. f (x - (x \<bullet> i) *\<^sub>R i + xa *\<^sub>R i)) differentiable at (x \<bullet> i))"
  unfolding partially_vector_differentiable_def has_partial_vector_derivative_def has_vector_derivative_def differentiable_def
  by metis

lemma linear_form: "linear f \<Longrightarrow> \<exists>m c. (\<forall>x. f x = x *\<^sub>R m)"
  unfolding linear_iff Modules.additive_def
  apply auto 
  by (metis mult.commute scaleR_one)

lemma differentiable_imp_partially_differentiable: assumes " ((\<lambda>xa. f (x - (x \<bullet> i) *\<^sub>R i + xa *\<^sub>R i)) differentiable at (x \<bullet> i))" shows "partially_vector_differentiable f i x"
  unfolding partially_vector_differentiable_def has_partial_vector_derivative_def has_vector_derivative_def differentiable_def
  using assms differentiable_iff_scaleR by blast 

lemma partially_differentiable_eq_differentiable: "partially_vector_differentiable f i x = ((\<lambda>xa. f (x - (x \<bullet> i) *\<^sub>R i + xa *\<^sub>R i)) differentiable at (x \<bullet> i))"
  using differentiable_imp_partially_differentiable partially_differentiable_imp_differentiable by auto

definition partial_vector_derivative:: "('a::euclidean_space \<Rightarrow> 'b::euclidean_space) \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> 'b" where
  "partial_vector_derivative F b a
        \<equiv> (vector_derivative (\<lambda>x. F((a - ((a \<bullet> b) *\<^sub>R b)) + x *\<^sub>R b)) (at (a \<bullet> b)))"

abbreviation "partial_vector_derivative2 F base_vec \<equiv> (partial_vector_derivative F base_vec)"

notation (input) partial_vector_derivative2 ("\<partial> _ '/ \<partial> _" 300)
notation (output) partial_vector_derivative2 ("'(\<partial> _ '/ \<partial> _')" 300)

notation (input) partial_vector_derivative2 ("\<partial> _ '/ \<partial> _ '|\<^bsub>_\<^esub>" 300)
notation (output) partial_vector_derivative2 ("'(\<partial> _ '/ \<partial> _ '|\<^bsub>_\<^esub>')" 300)

lemma partial_vector_derivative_works:
  assumes "partially_vector_differentiable F base_vec a"
  shows "has_partial_vector_derivative F base_vec (partial_vector_derivative F base_vec a) a"
proof -
  obtain F' where F'_prop: "((\<lambda>x. F( (a - ((a \<bullet> base_vec) *\<^sub>R base_vec)) + x *\<^sub>R base_vec ))
                            has_vector_derivative F') (at (a \<bullet> base_vec))"
    using assms partially_vector_differentiable_def has_partial_vector_derivative_def
    by blast
  show ?thesis
    using Derivative.differentiableI_vector[OF F'_prop]
    by(simp add: vector_derivative_works partial_vector_derivative_def[symmetric]
        has_partial_vector_derivative_def[symmetric])
qed

named_theorems partial_derivative_intros 

lemma partial_vector_derivative_works_2[partial_derivative_intros]:
  "has_partial_vector_derivative F base_vec F' a \<Longrightarrow> (partial_vector_derivative F base_vec a) = F'"
  unfolding has_partial_vector_derivative_def partial_vector_derivative_def
  using vector_derivative_at
  by fastforce

lemma fundamental_theorem_of_calculus_partial_vector:
  fixes a b:: "real" and
    F:: "'a::euclidean_space \<Rightarrow> 'b::euclidean_space" and
    i:: "'a" and
    j:: "'b" and
    F_j_i:: "'a \<Rightarrow> real"
  assumes a_leq_b: "a \<le> b" and
    Base_vecs: "i \<in> Basis" "j \<in> Basis" and
    no_i_component: "c \<bullet> i = 0 " and
    has_partial_deriv: "\<forall>p \<in> D. has_partial_vector_derivative (\<lambda>x. (F x) \<bullet> j) i (F_j_i p) p" and
    domain_subset_of_D: "{x *\<^sub>R i + c |x. a \<le> x \<and> x \<le> b} \<subseteq> D"
  shows "((\<lambda>x. F_j_i( x *\<^sub>R i + c)) has_integral
          F(b *\<^sub>R i + c) \<bullet> j - F(a *\<^sub>R i + c) \<bullet> j) (cbox a b)"
proof -
  let ?domain = "{v. \<exists>x. a \<le> x \<and> x \<le> b \<and> v = x *\<^sub>R i + c}"
  have "\<forall>x\<in> ?domain.  has_partial_vector_derivative (\<lambda>x. (F x) \<bullet> j) i (F_j_i x) x"
    using has_partial_deriv domain_subset_of_D
    by auto
  then have "\<forall>x\<in> (cbox a b).  ((\<lambda>x. F(x *\<^sub>R i + c) \<bullet> j) has_vector_derivative (F_j_i( x *\<^sub>R i + c))) (at x)"
  proof(clarsimp simp add: has_partial_vector_derivative_def)
    fix x::real
    assume range_of_x: "a \<le> x" "x \<le> b"
    assume ass2: "\<forall>x. (\<exists>z\<ge>a. z \<le> b \<and> x = z *\<^sub>R i + c) \<longrightarrow>
                     ((\<lambda>z. F(x - (x \<bullet> i) *\<^sub>R i + z *\<^sub>R i) \<bullet> j) has_vector_derivative F_j_i x) (at (x \<bullet> i))"
    have "((\<lambda>z. F((x *\<^sub>R i + c) - ((x *\<^sub>R i + c) \<bullet> i) *\<^sub>R i + z *\<^sub>R i) \<bullet> j) has_vector_derivative F_j_i (x *\<^sub>R i + c)) (at ((x *\<^sub>R i + c) \<bullet> i)) "
      using range_of_x ass2 by auto
    then have 0: "((\<lambda>x. F( c + x *\<^sub>R i) \<bullet> j) has_vector_derivative F_j_i (x *\<^sub>R i + c)) (at x)"
      by (simp add: assms(2) inner_left_distrib no_i_component)
    have 1: "(\<lambda>x. F(x *\<^sub>R i + c) \<bullet> j) = (\<lambda>x. F(c + x *\<^sub>R i) \<bullet> j)"
      by (simp add: add.commute)
    then show "((\<lambda>x. F(x *\<^sub>R i + c) \<bullet> j) has_vector_derivative F_j_i (x *\<^sub>R i + c)) (at x)" 
      using 0 and 1 by auto
  qed
  then have "\<forall>x\<in> (cbox a b).  ((\<lambda>x. F(x *\<^sub>R i + c) \<bullet> j) has_vector_derivative (F_j_i( x *\<^sub>R i + c))) (at_within x (cbox a b))"
    using has_vector_derivative_at_within
    by blast
  then show "( (\<lambda>x. F_j_i( x *\<^sub>R i + c)) has_integral
             F(b *\<^sub>R i + c) \<bullet> j - F(a *\<^sub>R i + c) \<bullet> j) (cbox a b)"
    using fundamental_theorem_of_calculus[of "a" "b" "(\<lambda>x::real.  F(x *\<^sub>R i + c) \<bullet> j)" "(\<lambda>x::real. F_j_i( x *\<^sub>R i + c))"] and
      a_leq_b
    by auto
qed

lemma fundamental_theorem_of_calculus_partial_vector_gen:
  fixes k1 k2::"real" and
    F::"'a::euclidean_space \<Rightarrow> 'b::euclidean_space" and
    i::"'a" and
    F_i::"'a \<Rightarrow> 'b"
  assumes a_leq_b: "k1 \<le> k2" and
    unit_len: "i \<bullet> i = 1" and
    no_i_component: "c \<bullet> i = 0 " and
    has_partial_deriv: "\<forall>p \<in> D. has_partial_vector_derivative F i (F_i p) p" and
    domain_subset_of_D: "{v. \<exists>x. k1 \<le> x \<and> x \<le> k2 \<and> v = x *\<^sub>R i + c} \<subseteq> D"
  shows "((\<lambda>x. F_i( x *\<^sub>R i + c)) has_integral
                                        F(k2 *\<^sub>R i + c) - F(k1 *\<^sub>R i + c)) {k1 .. k2}"
proof -
  let ?domain = "{v. \<exists>x. k1 \<le> x \<and> x \<le> k2 \<and> v = x *\<^sub>R i + c}"
  have "\<forall>x\<in> ?domain.  has_partial_vector_derivative F i (F_i x) x"
    using has_partial_deriv domain_subset_of_D
    by auto
  then have "\<forall>x\<in> (cbox k1 k2).  ((\<lambda>x. F(x *\<^sub>R i + c)) has_vector_derivative (F_i( x *\<^sub>R i + c))) (at x)"
  proof (clarsimp simp add: has_partial_vector_derivative_def)
    fix x::real
    assume range_of_x: "k1 \<le> x" "x \<le> k2"
    assume ass2: "\<forall>x. (\<exists>z\<ge>k1. z \<le> k2 \<and> x = z *\<^sub>R i + c) \<longrightarrow>
                     ((\<lambda>z. F(x - (x \<bullet> i) *\<^sub>R i + z *\<^sub>R i)) has_vector_derivative F_i x) (at (x \<bullet> i))"
    have "((\<lambda>z. F((x *\<^sub>R i + c) - ((x *\<^sub>R i + c) \<bullet> i) *\<^sub>R i + z *\<^sub>R i)) has_vector_derivative F_i (x *\<^sub>R i + c)) (at ((x *\<^sub>R i + c) \<bullet> i)) "
      using range_of_x ass2 by auto
    then have 0: "((\<lambda>x. F( c + x *\<^sub>R i)) has_vector_derivative F_i (x *\<^sub>R i + c)) (at x)"
      by (simp add: inner_commute inner_right_distrib no_i_component unit_len)
    have 1: "(\<lambda>x. F(x *\<^sub>R i + c)) = (\<lambda>x. F(c + x *\<^sub>R i))"
      by (simp add: add.commute)
    then show "((\<lambda>x. F(x *\<^sub>R i + c)) has_vector_derivative F_i (x *\<^sub>R i + c)) (at x)" using 0 and 1 by auto
  qed
  then have "\<forall>x\<in> (cbox k1 k2).  ((\<lambda>x. F(x *\<^sub>R i + c) ) has_vector_derivative (F_i( x *\<^sub>R i + c))) (at_within x (cbox k1 k2))"
    using has_vector_derivative_at_within
    by blast
  then show "( (\<lambda>x. F_i( x *\<^sub>R i + c)) has_integral
                                        F(k2 *\<^sub>R i + c) - F(k1 *\<^sub>R i + c)) {k1 .. k2}"
    using fundamental_theorem_of_calculus[of "k1" "k2" "(\<lambda>x::real.  F(x *\<^sub>R i + c))" "(\<lambda>x::real. F_i( x *\<^sub>R i + c))"] and
      a_leq_b
    by auto
qed

lemma has_vector_partial_derivative_mult:
  assumes "(has_partial_vector_derivative (f::('a::euclidean_space \<Rightarrow> 'b::{euclidean_space,real_normed_algebra})) i f_i' x)" "(has_partial_vector_derivative g i g_i' x)"
  shows "(has_partial_vector_derivative (\<lambda>x. f x * g x) i (f x * g_i' + f_i' * g x) x )"
  using assms
  unfolding has_partial_vector_derivative_def
  using has_vector_derivative_mult[where ?f = "%xa::real. f (x - (x \<bullet> i) *\<^sub>R i + xa *\<^sub>R i)" and ?g = "%xa. g (x - (x \<bullet> i) *\<^sub>R i + xa *\<^sub>R i)" and ?x = "(x \<bullet> i)" and ?s = "UNIV"]
  by auto

definition has_gradient:: "(('a::euclidean_space) \<Rightarrow> real) \<Rightarrow> ('a \<Rightarrow> 'a) \<Rightarrow> 'a \<Rightarrow> bool" where
  "has_gradient F F' a = (F has_derivative (%x. (F' a) \<bullet> x)) (at a)"

lemma bounded_linear_sum: "bounded_linear (\<lambda>x. \<Sum>b\<in>Basis. F' a \<bullet> b * (x \<bullet> b))"
  unfolding bounded_linear_def linear_iff Modules.additive_def
  apply(rule conjI)+
  subgoal by (metis (full_types) euclidean_inner inner_commute inner_left_distrib)
  subgoal by (auto; smt inner_real_def inner_sum_right mult.left_commute sum.cong)
  subgoal unfolding bounded_linear_axioms_def  
    by (auto; metis Cauchy_Schwarz_ineq2 euclidean_inner mult.commute)
  done

(*the directional derivative needs to have the following property!!!!*)
(*lemma has_total_deriv_imp_has_grad: 
  assumes "(\<forall>b. has_partial_vector_derivative F b ((F' a) \<bullet> b) a)"
  assumes "continuous_on UNIV F'"
  shows "has_gradient F F' a"
  unfolding has_gradient_def has_partial_vector_derivative_def has_vector_derivative_def has_derivative_def
  apply auto 
  subgoal using bounded_linear_sum sorry
proof-
  let ?x_dir = id
  assume "\<forall>b.
             (\<lambda>y. inverse \<bar>y - netlimit (at (a \<bullet> b))\<bar> * (F (a - (a \<bullet> b) *\<^sub>R b + y *\<^sub>R b) - F (a - (a \<bullet> b) *\<^sub>R b + netlimit (at (a \<bullet> b)) *\<^sub>R b) - (y - netlimit (at (a \<bullet> b))) * (F' a \<bullet> b))) \<midarrow>a \<bullet> b\<rightarrow> 0"
  then have "(\<lambda>y. inverse (norm (y - netlimit (at a))) * (F y - F (netlimit (at a)) - (\<Sum>b\<in>Basis. F' a \<bullet> b * ((y - netlimit (at a)) \<bullet> b)))) \<midarrow>a\<rightarrow> 0"
    unfolding Topology_Euclidean_Space.Lim_at netlimit_at
    apply auto
    oops
*)

lemma has_derivative_to_has_partial_deriv[partial_derivative_intros]:
  assumes differentiable: "((F::'a::euclidean_space \<Rightarrow> 'b::euclidean_space) has_derivative F') (at a)"
  shows  "(has_partial_vector_derivative F b (F' b) a)"
proof-
  have *: "(\<lambda>x. F (a - (a \<bullet> b) *\<^sub>R b + x *\<^sub>R b)) = F o (\<lambda>x. (a - (a \<bullet> b) *\<^sub>R b + x *\<^sub>R b))"
    by auto
  have "F' (x *\<^sub>R c) = x *\<^sub>R (F' c)" for x c
    using differentiable
    by (auto intro: linear_simps)
  then show ?thesis
    using differentiable
    by (auto intro!: linear_simps derivative_eq_intros simp: has_partial_vector_derivative_def has_vector_derivative_def *)
qed

lemma has_derivative_to_has_partial_deriv':
  "((F::'a::euclidean_space \<Rightarrow> 'b::euclidean_space) has_derivative (F' a)) (at a) \<Longrightarrow>
   ((\<lambda>x. F (a - (a \<bullet> b) *\<^sub>R b + x *\<^sub>R b)) has_derivative (\<lambda>x. x *\<^sub>R F' a b)) (at (a \<bullet> b))"
  using has_derivative_to_has_partial_deriv[unfolded has_partial_vector_derivative_def has_vector_derivative_def] .

lemma has_derivative_to_has_partial_deriv_real:
  assumes differentiable: "((F::real \<Rightarrow> 'b::euclidean_space) has_derivative (F' a)) (at a)"
  shows  "(has_partial_vector_derivative F b (F' a b) a)"
  using has_derivative_to_has_partial_deriv assms by blast

lemma partial_deriv_one_d: "\<partial>f / \<partial>1 = (%a. vector_derivative f (at a))"
  "(has_partial_vector_derivative f 1 f' a) = (f has_vector_derivative f') (at a)"
  unfolding vector_derivative_def partial_vector_derivative_def has_partial_vector_derivative_def by auto

(*lemma has_derivative_to_has_partial_deriv: 
  assumes b_neq_zero: "norm b \<noteq> 0" and differentiable: "((F::'a::euclidean_space \<Rightarrow> 'b::euclidean_space) has_derivative (F' a)) (at a)"
  shows  "(has_partial_vector_derivative F b (F' a b) a)"
  unfolding has_gradient_def has_partial_vector_derivative_def has_vector_derivative_def has_derivative_def
  apply auto 
    subgoal unfolding bounded_linear_sum bounded_linear_def linear_def linear_axioms_def bounded_linear_axioms_def Real_Vector_Spaces.additive_def by (auto simp add: scaleR_add_left)
  unfolding Topology_Euclidean_Space.Lim_at netlimit_at
proof(auto)
  have sqrt_b: " inverse (sqrt (b \<bullet> b)) * (inverse (sqrt (b \<bullet> b))) = (inverse (b \<bullet> b)) " using real_inv_sqrt_pow2 b_neq_zero sorry
    let ?pt2vec = "%x. (a + inverse (norm b) *\<^sub>R (x *\<^sub>R b - (a \<bullet> b) *\<^sub>R b))"
    fix e::real assume ass: "0 < e"
    have i: "\<And>x.( x *\<^sub>R b - (a \<bullet> b) *\<^sub>R b) = (x *\<^sub>R b + (- (a \<bullet> b)) *\<^sub>R b)" by auto
    have "\<And>x. (?pt2vec x - a) \<bullet> (?pt2vec x - a) = (x - a \<bullet> b) * (x - a \<bullet> b)"
      using assms
      by (auto simp add: norm_eq_sqrt_inner sqrt_b mult.assoc[symmetric], auto simp add: algebra_simps right_inverse[OF b_neq_zero])
    then have "\<And>x. \<bar>(x - a \<bullet> b) \<bar> = norm (?pt2vec x - a)"
      by (auto simp add: norm_eq_sqrt_inner real_sqrt_mult)
    then have "\<And>x. \<bar>(x - a \<bullet> b) \<bar>  = norm (?pt2vec x - a)"
      by (auto simp add: norm_eq_sqrt_inner real_sqrt_mult)
    moreover have "\<And>x. dist x (a \<bullet> b)  =  dist (?pt2vec x) a" 
      apply (simp add: dist_norm)
      using b_neq_zero calculation by (auto simp add: algebra_simps, auto simp add: norm_scaleR) 
    moreover have "\<And>x. (x - a \<bullet> b) *\<^sub>R (F' a b) = (norm b) *\<^sub>R (F' a (((?pt2vec x) - a)))"
      apply (auto simp add:)
      using assms
      unfolding has_derivative_def bounded_linear_def linear_def Real_Vector_Spaces.additive_def linear_axioms_def bounded_linear_axioms_def
      by (auto simp add: , simp only: i, auto simp add: algebra_simps)
    moreover obtain d where d: "d > 0 \<and> (\<forall>x. x \<noteq> a \<bullet> b \<and> dist x (a \<bullet> b) < d \<longrightarrow> norm (inverse \<bar>x - a \<bullet> b\<bar> *\<^sub>R (F (a - (a \<bullet> b) *\<^sub>R b + x *\<^sub>R b) - F a - (norm b) *\<^sub>R (F' a (((?pt2vec x) - a))))) < e)"
      using assms(2)
      unfolding Topology_Euclidean_Space.Lim_at netlimit_at has_gradient_def has_derivative_def
      using dist_not_less_zero ass apply auto sledgehammer
    ultimately have "d > 0 \<and> (\<forall>x. x \<noteq> a \<bullet> b \<and> dist x (a \<bullet> b) < d \<longrightarrow> inverse \<bar>x - a \<bullet> b\<bar> * norm (F (a - (a \<bullet> b) *\<^sub>R b + x *\<^sub>R b) - F a - (x - a \<bullet> b) *\<^sub>R F' a b) < e)"
      by force
    moreover have "\<And>x v. norm(inverse (abs x) *\<^sub>R v) = inverse (abs x) * norm(v)" by auto
    ultimately show "\<exists>d>0. \<forall>x. x \<noteq> a \<bullet> b \<and> dist x (a \<bullet> b) < d \<longrightarrow> inverse \<bar>x - a \<bullet> b\<bar> * norm (F (a - (a \<bullet> b) *\<^sub>R b + x *\<^sub>R b) - F a - (x - a \<bullet> b) *\<^sub>R F' a b) < e"
      by metis
  qed*)

lemma has_derivative_to_has_partial_deriv'': 
  assumes "((F::'a::euclidean_space \<Rightarrow> 'b::euclidean_space) has_derivative (F')) (at a)"
  shows  "(has_partial_vector_derivative F b (F' b) a)"
  using has_derivative_to_has_partial_deriv assms by fast

lemma has_gradient_imp_has_directional_derivs: 
  "has_gradient F F' a \<Longrightarrow> has_partial_vector_derivative F b ((F' a) \<bullet> b) a"
  unfolding has_gradient_def   using has_derivative_to_has_partial_deriv
  by metis

(*Generalised from Immler's ODE AFP entry*)

lemma symmetric_second_derivative_aux:
  assumes first_fderiv[derivative_intros]:
    "\<And>a. a \<in> G \<Longrightarrow> (f has_derivative (f' a)) (at a within G)"
  assumes second_fderiv[derivative_intros]:
    "\<And>i. P i \<Longrightarrow> ((\<lambda>x. f' x i) has_derivative (f'' i)) (at a within G)"
  assumes "i \<noteq> j" "i \<noteq> 0" "j \<noteq> 0" "P i" "P j"
  assumes "a \<in> G"
  assumes "\<And>s t. s \<in> {0..1} \<Longrightarrow> t \<in> {0..1} \<Longrightarrow> a + s *\<^sub>R i + t *\<^sub>R j \<in> G"
  shows "f'' j i = f'' i j"
proof -
  let ?F = "at_right (0::real)"
  define B where "B i j = {a + s *\<^sub>R i + t *\<^sub>R j |s t. s \<in> {0..1} \<and> t \<in> {0..1}}" for i j
  have "B i j \<subseteq> G" using assms by (auto simp: B_def)
  {
    fix e::real and i j::'a
    assume "e > 0"
    assume ass: "i \<noteq> j" "i \<noteq> 0" "j \<noteq> 0"  "P i" "P j"
    assume "B i j \<subseteq> G"
    let ?ij' = "\<lambda>s t. \<lambda>u. a + (s * u) *\<^sub>R i + (t * u) *\<^sub>R j"
    let ?ij = "\<lambda>t. \<lambda>u. a + (t * u) *\<^sub>R i + u *\<^sub>R j"
    let ?i = "\<lambda>t. \<lambda>u. a + (t * u) *\<^sub>R i"
    let ?g = "\<lambda>u t. f (?ij t u) - f (?i t u)"
    have filter_ij'I: "\<And>P. P a \<Longrightarrow> eventually P (at a within G) \<Longrightarrow>
      eventually (\<lambda>x. \<forall>s\<in>{0..1}. \<forall>t\<in>{0..1}. P (?ij' s t x)) ?F"
    proof -
      fix P
      assume "P a"
      assume "eventually P (at a within G)"
      hence "eventually P (at a within B i j)" by (rule filter_leD[OF at_le[OF \<open>B i j \<subseteq> G\<close>]])
      then obtain d where d: "d > 0" and "\<And>x d2. x \<in> B i j \<Longrightarrow> x \<noteq> a \<Longrightarrow> dist x a < d \<Longrightarrow> P x"
        by (auto simp: eventually_at)
      with \<open>P a\<close> have P: "\<And>x d2. x \<in> B i j \<Longrightarrow> dist x a < d \<Longrightarrow> P x" by (case_tac "x = a") auto
      let ?d = "min (min (d/norm i) (d/norm j) / 2) 1"
      show "eventually (\<lambda>x. \<forall>s\<in>{0..1}. \<forall>t\<in>{0..1}. P (?ij' s t x)) (at_right 0)"
        unfolding eventually_at
      proof (rule exI[where x="?d"], safe)
        show "0 < ?d" using \<open>0 < d\<close> \<open>i \<noteq> 0\<close> \<open>j \<noteq> 0\<close> by simp
        fix x s t :: real assume *: "s \<in> {0..1}" "t \<in> {0..1}" "0 < x" "dist x 0 < ?d"
        show "P (?ij' s t x)"
        proof (rule P)
          have "\<And>x y::real. x \<in> {0..1} \<Longrightarrow> y \<in> {0..1} \<Longrightarrow> x * y \<in> {0..1}"
            by (auto intro!: order_trans[OF mult_left_le_one_le])
          hence "s * x \<in> {0..1}" "t * x \<in> {0..1}" using * by (auto simp: dist_norm)
          thus "?ij' s t x \<in> B i j" by (auto simp: B_def)
          have "norm (s *\<^sub>R x *\<^sub>R i + t *\<^sub>R x *\<^sub>R j) \<le> norm (s *\<^sub>R x *\<^sub>R i) + norm (t *\<^sub>R x *\<^sub>R j)"
            by (rule norm_triangle_ineq)
          also have "\<dots> < d / 2 + d / 2" using * \<open>i \<noteq> 0\<close> \<open>j \<noteq> 0\<close>
            by (intro add_strict_mono) (auto simp: ac_simps dist_norm
                pos_less_divide_eq le_less_trans[OF mult_left_le_one_le])
          finally show "dist (?ij' s t x) a < d" by (simp add: dist_norm)
        qed
      qed
    qed
    have filter_ijI: "eventually (\<lambda>x. \<forall>t\<in>{0..1}. P (?ij t x)) ?F"
      if "P a" "eventually P (at a within G)" for P
      using filter_ij'I[OF that]
      by eventually_elim (force dest: bspec[where x=1])
    have filter_iI: "eventually (\<lambda>x. \<forall>t\<in>{0..1}. P (?i t x)) ?F"
      if "P a" "eventually P (at a within G)" for P
      using filter_ij'I[OF that] by eventually_elim force
    {
      from second_fderiv[OF ass(4), simplified has_derivative_iff_norm, THEN conjunct2,
          THEN tendstoD, OF \<open>0 < e\<close>]
      have "eventually (\<lambda>x. norm (f' x i - f' a i - f'' i (x - a)) / norm (x - a) \<le> e)
          (at a within G)"
        by eventually_elim (simp add: dist_norm)
      from filter_ijI[OF _ this] filter_iI[OF _ this] \<open>0 < e\<close>
      have
        "eventually (\<lambda>ij. \<forall>t\<in>{0..1}. norm (f' (?ij t ij) i - f' a i - f'' i (?ij t ij - a)) /
          norm (?ij t ij - a) \<le> e) ?F"
        "eventually (\<lambda>ij. \<forall>t\<in>{0..1}. norm (f' (?i t ij) i - f' a i - f'' i (?i t ij - a)) /
          norm (?i t ij - a) \<le> e) ?F"
        by auto
      moreover
      have "eventually (\<lambda>x. x \<in> G) (at a within G)" unfolding eventually_at_filter by simp
      hence eventually_in_ij: "eventually (\<lambda>x. \<forall>t\<in>{0..1}. ?ij t x \<in> G) ?F" and
        eventually_in_i: "eventually (\<lambda>x. \<forall>t\<in>{0..1}. ?i t x \<in> G) ?F"
        using \<open>a \<in> G\<close> by (auto dest: filter_ijI filter_iI)
      ultimately
      have "eventually (\<lambda>u. norm (?g u 1 - ?g u 0 - (u * u) *\<^sub>R f'' i j) \<le>
          u * u * e * (2 * norm i + 3 * norm j)) ?F"
      proof eventually_elim
        case (elim u)
        hence ijsub: "(\<lambda>t. ?ij t u) ` {0..1} \<subseteq> G" and isub: "(\<lambda>t. ?i t u) ` {0..1} \<subseteq> G" by auto
        note has_derivative_subset[OF _ ijsub, derivative_intros]
        note has_derivative_subset[OF _ isub, derivative_intros]
        let ?g' = "\<lambda>t. (\<lambda>ua. u *\<^sub>R ua *\<^sub>R (f' (?ij t u) i - (f' (?i t u) i)))"
        have g': "((?g u) has_derivative ?g' t) (at t within {0..1})" if "t \<in> {0..1}" for t::real
        proof -
          from elim that have linear_f': "\<And>c x. f' (?ij t u) (c *\<^sub>R x) = c *\<^sub>R f' (?ij t u) x"
            "\<And>c x. f' (?i t u) (c *\<^sub>R x) = c *\<^sub>R f' (?i t u) x"
            using linear_cmul[OF has_derivative_linear, OF first_fderiv] by auto
          show ?thesis
            using elim \<open>t \<in> {0..1}\<close>
            by (auto intro!: derivative_eq_intros has_derivative_in_compose[of  "\<lambda>t. ?ij t u" _ _ _ f]
                has_derivative_in_compose[of  "\<lambda>t. ?i t u" _ _ _ f]
                simp: linear_f' scaleR_diff_right mult.commute)
        qed
        from elim(1) \<open>i \<noteq> 0\<close> \<open>j \<noteq> 0\<close> \<open>0 < e\<close> have f'ij: "\<And>t. t \<in> {0..1} \<Longrightarrow>
            norm (f' (a + (t * u) *\<^sub>R i + u *\<^sub>R j) i - f' a i - f'' i ((t * u) *\<^sub>R i + u *\<^sub>R j)) \<le>
            e * norm ((t * u) *\<^sub>R i + u *\<^sub>R j)"
          using  linear_0[OF has_derivative_linear, OF second_fderiv[OF ass(4)]]
          by (case_tac "u *\<^sub>R j + (t * u) *\<^sub>R i = 0") (auto simp: field_simps
              simp del: pos_divide_le_eq simp add: pos_divide_le_eq[symmetric])
        from elim(2) have f'i: "\<And>t. t \<in> {0..1} \<Longrightarrow> norm (f' (a + (t * u) *\<^sub>R i) i - f' a i -
          f'' i ((t * u) *\<^sub>R i)) \<le> e * abs (t * u) * norm i"
          using \<open>i \<noteq> 0\<close> \<open>j \<noteq> 0\<close> linear_0[OF has_derivative_linear, OF second_fderiv[OF ass(4)]]
          by (case_tac "t * u = 0") (auto simp: field_simps simp del: pos_divide_le_eq
              simp add: pos_divide_le_eq[symmetric])
        have "norm (?g u 1 - ?g u 0 - (u * u) *\<^sub>R f'' i j) =
          norm ((?g u 1 - ?g u 0 - u *\<^sub>R (f' (a + u *\<^sub>R j) i - (f' a i)))
            + u *\<^sub>R (f' (a + u *\<^sub>R j) i - f' a i - u *\<^sub>R f'' i j))"
          (is "_ = norm (?g10 + ?f'i)")
          by (simp add: algebra_simps linear_cmul[OF has_derivative_linear, OF second_fderiv]
              linear_add[OF has_derivative_linear, OF second_fderiv])
        also have "\<dots> \<le> norm ?g10 + norm ?f'i"
          by (blast intro: order_trans add_mono norm_triangle_le)
        also
        have "0 \<in> {0..1::real}" by simp
        have "\<forall>t \<in> {0..1}. onorm ((\<lambda>ua. (u * ua) *\<^sub>R (f' (?ij t u) i - f' (?i t u) i)) -
              (\<lambda>ua. (u * ua) *\<^sub>R (f' (a + u *\<^sub>R j) i - f' a i)))
            \<le> 2 * u * u * e * (norm i + norm j)" (is "\<forall>t \<in> _. onorm (?d t) \<le> _")
        proof
          fix t::real assume "t \<in> {0..1}"
          show "onorm (?d t) \<le> 2 * u * u * e * (norm i + norm j)"
          proof (rule onorm_le)
            fix x
            have "norm (?d t x) =
                norm ((u * x) *\<^sub>R (f' (?ij t u) i - f' (?i t u) i - f' (a + u *\<^sub>R j) i + f' a i))"
              by (simp add: algebra_simps)
            also have "\<dots> =
                abs (u * x) * norm (f' (?ij t u) i - f' (?i t u) i - f' (a + u *\<^sub>R j) i + f' a i)"
              by simp
            also have "\<dots> = abs (u * x) * norm (
                 f' (?ij t u) i - f' a i - f'' i ((t * u) *\<^sub>R i + u *\<^sub>R j)
               - (f' (?i t u) i - f' a i - f'' i ((t * u) *\<^sub>R i))
               - (f' (a + u *\<^sub>R j) i - f' a i - f'' i (u *\<^sub>R j)))"
              (is "_ = _ * norm (?dij - ?di - ?dj)")
              using \<open>a \<in> G\<close>
              by (simp add: algebra_simps
                  linear_add[OF has_derivative_linear[OF second_fderiv[OF ass(4)]]])
            also have "\<dots> \<le> abs (u * x) * (norm ?dij + norm ?di + norm ?dj)"
              by (rule mult_left_mono[OF _ abs_ge_zero]) norm
            also have "\<dots> \<le> abs (u * x) *
              (e * norm ((t * u) *\<^sub>R i + u *\<^sub>R j) + e * abs (t * u) * norm i + e * (\<bar>u\<bar> * norm j))"
              using f'ij f'i f'ij[OF \<open>0 \<in> {0..1}\<close>] \<open>t \<in> {0..1}\<close>
              by (auto intro!: add_mono mult_left_mono)
            also have "\<dots> = abs u * abs x * abs u *
              (e * norm (t *\<^sub>R i + j) + e * norm (t *\<^sub>R i) + e * (norm j))"
              by (simp add: algebra_simps norm_scaleR[symmetric] abs_mult del: norm_scaleR)
            also have "\<dots> =
                u * u * abs x * (e * norm (t *\<^sub>R i + j) + e * norm (t *\<^sub>R i) + e * (norm j))"
              by (simp add: ac_simps)
            also have "\<dots> = u * u * e * abs x * (norm (t *\<^sub>R i + j) + norm (t *\<^sub>R i) + norm j)"
              by (simp add: algebra_simps)
            also have "\<dots> \<le> u * u * e * abs x * ((norm (1 *\<^sub>R i) + norm j) + norm (1 *\<^sub>R i) + norm j)"
              using \<open>t \<in> {0..1}\<close> \<open>0 < e\<close>
              by (intro mult_left_mono add_mono) (auto intro!: norm_triangle_le add_right_mono
                  mult_left_le_one_le zero_le_square)
            finally show "norm (?d t x) \<le> 2 * u * u * e * (norm i + norm j) * norm x"
              by (simp add: ac_simps)
          qed
        qed
        with differentiable_bound_linearization[where f="?g u" and f'="?g'", of 0 1 _ 0, OF _ g']
        have "norm ?g10 \<le> 2 * u * u * e * (norm i + norm j)" by simp
        also have "norm ?f'i \<le> abs u *
          norm ((f' (a + (u) *\<^sub>R j) i - f' a i - f'' i (u *\<^sub>R j)))"
          using linear_cmul[OF has_derivative_linear, OF second_fderiv[OF ass(4)]]
          by simp
        also have "\<dots> \<le> abs u * (e * norm ((u) *\<^sub>R j))"
          using f'ij[OF \<open>0 \<in> {0..1}\<close>] by (auto intro: mult_left_mono)
        also have "\<dots> = u * u * e * norm j" by (simp add: algebra_simps abs_mult)
        finally show ?case by (simp add: algebra_simps)
      qed
    }
  } note wlog = this
  have e': "norm (f'' i j - f'' j i ) \<le> e * (5 * norm j + 5 * norm i)" if "0 < e" for e t::real
  proof -
    have "B i j = B j i" using \<open>i \<noteq> j\<close> by (force simp: B_def)+
    with assms \<open>B i j \<subseteq> G\<close> have "j \<noteq> i" "B j i \<subseteq> G" by (auto simp:)
    from wlog[OF \<open>0 < e\<close> \<open>i \<noteq> j\<close> \<open>i \<noteq> 0\<close> \<open>j \<noteq> 0\<close> \<open>P i\<close> \<open>P j\<close> \<open>B i j \<subseteq> G\<close>]
      wlog[OF \<open>0 < e\<close> \<open>j \<noteq> i\<close> \<open>j \<noteq> 0\<close> \<open>i \<noteq> 0\<close> \<open>P j\<close> \<open>P i\<close> \<open>B j i \<subseteq> G\<close>]
    have "eventually (\<lambda>u. norm ((u * u) *\<^sub>R f'' i j - (u * u) *\<^sub>R f'' j i)
         \<le> u * u * e * (5 * norm j + 5 * norm i)) ?F"
    proof eventually_elim
      case (elim u)
      have "norm ((u * u) *\<^sub>R f'' i j - (u * u) *\<^sub>R f'' j i) =
        norm (f (a + u *\<^sub>R j + u *\<^sub>R i) - f (a + u *\<^sub>R j) -
         (f (a + u *\<^sub>R i) - f a) - (u * u) *\<^sub>R f'' j i
         - (f (a + u *\<^sub>R i + u *\<^sub>R j) - f (a + u *\<^sub>R i) -
         (f (a + u *\<^sub>R j) - f a) -
         (u * u) *\<^sub>R f'' i j))" by (simp add: field_simps)
      also have "\<dots> \<le> u * u * e * (2 * norm j + 3 * norm i) + u * u * e * (3 * norm j + 2 * norm i)"
        using elim by (intro order_trans[OF norm_triangle_ineq4]) (auto simp: ac_simps intro: add_mono)
      finally show ?case by (simp add: algebra_simps)
    qed
    hence "eventually (\<lambda>u. norm ((u * u) *\<^sub>R (f'' i j - f'' j i)) \<le>
        u * u * e * (5 * norm j + 5 * norm i)) ?F"
      by (simp add: algebra_simps)
    hence "eventually (\<lambda>u. (u * u) * norm ((f'' i j - f'' j i)) \<le>
        (u * u) * (e * (5 * norm j + 5 * norm i))) ?F"
      by (simp add: ac_simps)
    hence "eventually (\<lambda>u. norm ((f'' i j - f'' j i)) \<le> e * (5 * norm j + 5 * norm i)) ?F"
      unfolding mult_le_cancel_left eventually_at_filter
      by eventually_elim auto
    then show ?thesis
      by (auto simp add:eventually_at dist_norm dest!: bspec[where x="d/2" for d])
  qed
  have e: "norm (f'' i j - f'' j i) < e" if "0 < e" for e::real
  proof -
    let ?e = "e/2/(5 * norm j + 5 * norm i)"
    have "?e > 0" using \<open>0 < e\<close> \<open>i \<noteq> 0\<close> \<open>j \<noteq> 0\<close> by (auto intro!: divide_pos_pos add_pos_pos)
    from e'[OF this] have "norm (f'' i j - f'' j i) \<le> ?e * (5 * norm j + 5 * norm i)" .
    also have "\<dots> = e / 2" using \<open>i \<noteq> 0\<close> \<open>j \<noteq> 0\<close> by (auto simp: ac_simps add_nonneg_eq_0_iff)
    also have "\<dots> < e" using \<open>0 < e\<close> by simp
    finally show ?thesis .
  qed
  have "norm (f'' i j - f'' j i) = 0"
  proof (rule ccontr)
    assume "norm (f'' i j - f'' j i) \<noteq> 0"
    hence "norm (f'' i j - f'' j i) > 0" by simp
    from e[OF this] show False by simp
  qed
  thus ?thesis by simp
qed

lemma symmetric_second_derivative_aux':
  assumes first_fderiv[derivative_intros]:
    "\<And>a. a \<in> G \<Longrightarrow> (f has_derivative (f' a)) (at a within G)"
  assumes second_fderiv[derivative_intros]:
    "((\<lambda>x. f' x i) has_derivative (f'_i)) (at a within G)"
    "((\<lambda>x. f' x j) has_derivative (f'_j)) (at a within G)"
  assumes "i \<noteq> j" "i \<noteq> 0" "j \<noteq> 0"
  assumes "a \<in> G"
  assumes "\<And>s t. s \<in> {0..1} \<Longrightarrow> t \<in> {0..1} \<Longrightarrow> a + s *\<^sub>R i + t *\<^sub>R j \<in> G"
  shows "f'_j i = f'_i j"
  using assms(2-3) assms(4) symmetric_second_derivative_aux[where ?P = "%a. a \<in> {i,j}" and ?f'' = "(\<lambda>v. if v = i then f'_i else f'_j)", OF assms(1) _ assms(4 - 6) _ _ assms(7) assms(8)]
  apply auto
  by metis

(*We can just this statement for now, but when we will need to apply it to practical examples everything will need to have (at a within s),
  in which case I will need to carry over the assumptions of symmetric_second_derivative_aux' everywhere.*)
lemma Youngs_thm:
  assumes "norm i = 1"  "norm j = 1" "i \<noteq> j"
    "\<And>a. ((F::'a::euclidean_space \<Rightarrow> 'b::euclidean_space) has_derivative (F' a)) (at a)"
    "((\<lambda>x. F' x i) has_derivative F'_i) (at a)" 
    "((\<lambda>x. F' x j) has_derivative F'_j) (at a)" 
  shows "(\<partial> (\<partial> F / \<partial> i) / \<partial> j |\<^bsub>a\<^esub>) = (\<partial> (\<partial> F / \<partial> j) / \<partial> i |\<^bsub>a\<^esub>)"
proof-
  have ij: "i \<noteq> 0" "j \<noteq> 0"
    using assms by auto
  have "partial_vector_derivative F i = (%a. F' a i) \<and> partial_vector_derivative F j = (%a. F' a j)"
    by (metis has_derivative_to_has_partial_deriv assms partial_vector_derivative_works_2 norm_eq_1)
  then have "partial_vector_derivative (partial_vector_derivative F i) j a = F'_i j \<and> partial_vector_derivative (partial_vector_derivative F j) i a = F'_j i"
    using has_derivative_to_has_partial_deriv'' assms partial_vector_derivative_works_2 norm_eq_1 partial_vector_derivative_works_2 by metis
  then show ?thesis
    using symmetric_second_derivative_aux'[where ?G = UNIV, OF assms(4 - 6) assms(3) ij]
    by (auto) 
qed

lemma partial_vector_derivative_mult_at:
  fixes f g :: "'a::euclidean_space \<Rightarrow> real"
  assumes "partially_vector_differentiable f i x" "partially_vector_differentiable g i x"
  shows "\<partial> (%x. f x * g x) / \<partial> i |\<^bsub>x\<^esub> = (\<partial> f / \<partial> i |\<^bsub>x\<^esub>) * g x + (\<partial> g / \<partial> i |\<^bsub>x\<^esub>) * f x"
  unfolding partial_vector_derivative_def
  using vector_derivative_mult_at[where ?f = "(\<lambda>xa. f (x - (x \<bullet> i) *\<^sub>R i + xa *\<^sub>R i))" and ?g = "(\<lambda>xa. g (x - (x \<bullet> i) *\<^sub>R i + xa *\<^sub>R i))" and ?a = "x \<bullet> i"]
    partially_differentiable_imp_differentiable[OF assms(1)] partially_differentiable_imp_differentiable[OF assms(2)]
  by auto

lemma assumes "p\<in>s"
  "\<forall>p\<in>s. has_partial_vector_derivative F i (Fi p) p"
  "has_partial_vector_derivative Fi j Fij p"
  "\<forall>p\<in>s. has_partial_vector_derivative F j (Fj p) p"
shows "has_partial_vector_derivative Fj i Fij p"
  unfolding has_partial_vector_derivative_def
proof-
  have "((\<lambda>x. Fj (p - (p \<bullet> i) *\<^sub>R i + x *\<^sub>R i)) has_vector_derivative Fij) (at (p \<bullet> i))"
    using assms
    unfolding has_vector_derivative_def has_derivative_def netlimit_at Lim_at has_partial_vector_derivative_def
    apply auto
    oops

abbreviation "i_component F u \<equiv> (%x. F x \<bullet> u)"

notation(*(input)*)  i_component ("_\<^bsub>_\<^esub>" 45)
  (*notation(output)  i_component ("'(_\<^bsub>_\<^esub>')" 45)*)

lemma partial_deriv_add_direction:
  assumes Fd: "(F has_derivative F') (at p)" and
    unit_i: "i \<bullet> i = 1" and unit_j: "j \<bullet> j = 1"
  shows "\<partial>F / \<partial>(i + j) |\<^bsub>p\<^esub> = \<partial>F / \<partial>i |\<^bsub>p\<^esub> + \<partial>F / \<partial>j |\<^bsub>p\<^esub>"
proof-
  let ?pd_fun = "(%b x. p - (p \<bullet> b) *\<^sub>R b + x *\<^sub>R b)"
  have "(\<lambda>x. F (p - (p \<bullet> i) *\<^sub>R i + x *\<^sub>R i)) = F o ?pd_fun i" 
    "(\<lambda>x. F (p - (p \<bullet> (i + j)) *\<^sub>R (i + j) + x *\<^sub>R (i + j))) = F o ?pd_fun (i + j)" by auto
  from Fd have F'_b: "((F o (?pd_fun b)) has_vector_derivative (F' b)) (at (p \<bullet> b))" for b
    apply(auto intro!: vector_derivative_diff_chain_within[where g = "F" and f = "?pd_fun b" and S = UNIV and f' = b and g' = "F'" and x = "p \<bullet> b"])
    subgoal by(auto intro!: derivative_eq_intros)
    subgoal using has_derivative_at_withinI by auto
    done
  have lin_F': "linear F'" using Fd
    using has_derivative_linear by blast
  have "has_partial_vector_derivative F (i + j) (F' i + F' j) p"
    using F'_b[of "i+j"] lin_F'
    unfolding has_partial_vector_derivative_def linear_iff Modules.additive_def
    by (auto simp add: o_def)
  moreover have "has_partial_vector_derivative F (i) (F' i) p" "has_partial_vector_derivative F (j) (F' j) p"
    using Fd unit_i unit_j
    by (simp add: has_derivative_to_has_partial_deriv'')+
  ultimately show "(\<partial> F / \<partial> i + j |\<^bsub>p\<^esub>) = (\<partial> F / \<partial> i |\<^bsub>p\<^esub>) + (\<partial> F / \<partial> j |\<^bsub>p\<^esub>)"
    by (metis partial_vector_derivative_works_2)
qed

section \<open>Different chain rules for partial derivatives\<close>

lemma part_deriv_ch: fixes i::"'a::{euclidean_space}" and p::"'a" and C::"'a \<Rightarrow> ('b::{euclidean_space})" and f :: "'b \<Rightarrow> real" and f'::"'b \<Rightarrow> real"
  assumes C': "(C has_derivative C') (at p)" and f': "(f has_derivative f') (at (C p))"
  shows "\<partial> f / \<partial>(\<partial>C/ \<partial>i|\<^bsub>p\<^esub>) |\<^bsub>(C p)\<^esub>  = \<partial>(f o C) / \<partial>i |\<^bsub>p\<^esub>"
proof-
  let ?pd_fun = "(%b x. p - (p \<bullet> b) *\<^sub>R b + x *\<^sub>R b)"
  have "((f o (\<lambda>x.(C p - (C\<^bsub>b\<^esub>) p *\<^sub>R b + x *\<^sub>R b))) has_vector_derivative (f' b)) (at ((C\<^bsub>b\<^esub>) p))" for b
    apply(auto intro!: vector_derivative_diff_chain_within[where g = "f" and f = "(\<lambda>x.(C p - (C\<^bsub>b\<^esub>) p *\<^sub>R b + x *\<^sub>R b))" and S = UNIV and f' = b and g' = "f'" and x = "(C\<^bsub>b\<^esub>) p"])
    subgoal by (auto intro!: derivative_eq_intros)
    subgoal using f' has_derivative_at_withinI by blast
    done
  then have 2: "vector_derivative (f o (\<lambda>x.(C p - (C\<^bsub>b\<^esub>) p *\<^sub>R b + x *\<^sub>R b))) (at ((C\<^bsub>b\<^esub>) p)) = f' b" for b
    using vector_derivative_at by blast
  have "vector_derivative (\<lambda>x. (f \<circ> C) (p - (p \<bullet> i) *\<^sub>R i + x *\<^sub>R i)) (at (p \<bullet> i)) = vector_derivative ((f \<circ> C) o (?pd_fun i)) (at (p \<bullet> i))" by (auto simp add: o_def)
  have FoC_deriv: "(f \<circ> C has_derivative f' \<circ> C' ) (at p)"
    using  diff_chain_at[OF C' f'] by auto
  have "(((f \<circ> C) o (?pd_fun i)) has_vector_derivative ((f' \<circ> C') i)) (at (p \<bullet> i))"
    apply(rule vector_derivative_diff_chain_within[where g = "f o C" and f = "?pd_fun i" and S = UNIV and f' = i and g' = "f' \<circ> C'" and x = "p \<bullet> i"])
    subgoal by(auto intro!: derivative_eq_intros)
    subgoal using FoC_deriv by (simp add: has_derivative_at_withinI)
    done
  then have 3: "vector_derivative ((f \<circ> C) o (?pd_fun i)) (at (p \<bullet> i)) = ((f' \<circ> C') i)"
    using vector_derivative_at by blast
  have "\<partial> f / \<partial>(C' i) |\<^bsub>(C p)\<^esub>  = \<partial>(f o C) / \<partial>i |\<^bsub>p\<^esub>"
    unfolding partial_vector_derivative_def
    using 2[where b = "C' i"] 3
    by(auto simp add: o_def)
  moreover have "\<partial>C/ \<partial>i |\<^bsub>p\<^esub> = C' i"
    using has_derivative_to_has_partial_deriv'' C' partial_vector_derivative_works_2
    by blast
  ultimately show "\<partial> f / \<partial>(\<partial>C/ \<partial>i|\<^bsub>p\<^esub>) |\<^bsub>(C p)\<^esub>  = \<partial>(f o C) / \<partial>i |\<^bsub>p\<^esub>" by auto
qed

(*lemma partial_deriv_compose:
  assumes Df: "has_partial_vector_derivative F (g' i) Fi (g p)" and 
          Dg: "(g has_derivative g') (at p)"
        shows  "has_partial_vector_derivative (F o g) i (g Fi) p"
  using assms
  unfolding has_partial_vector_derivative_def
proof-
  have i: "((\<lambda>x. F (g p - (g p \<bullet> (\<partial>(g\<^bsub>i\<^esub>)/\<partial>i |\<^bsub>p\<^esub>)) *\<^sub>R (\<partial>(g\<^bsub>i\<^esub>)/\<partial>i |\<^bsub>p\<^esub>) + x *\<^sub>R (\<partial>(g\<^bsub>i\<^esub>)/\<partial>i |\<^bsub>p\<^esub>))) has_derivative (\<lambda>x. x *\<^sub>R Fi)) (at ( g p \<bullet> (\<partial>(g\<^bsub>i\<^esub>)/\<partial>i |\<^bsub>p\<^esub>)))"
    using Df[unfolded has_partial_vector_derivative_def has_vector_derivative_def] by auto
  have ii: "((%x. g x \<bullet> i)  has_derivative (%v. g' v \<bullet> i)) (at p)" apply(rule inner_left_has_derivative) using Dg by auto
  have "has_partial_vector_derivative (F o g) i (g' Fi) p"
       unfolding has_partial_vector_derivative_def has_vector_derivative_def
       using diff_chain_at[OF ii i] 
       apply (auto simp add: o_def)
  using assms
  unfolding has_partial_vector_derivative_def
  sorry
  oops
*)

lemma part_deriv_ch': fixes i::"'a::{euclidean_space}" and p::"'a" and C::"'a \<Rightarrow> 'a" and f :: "'a \<Rightarrow> real" and f'::"'a \<Rightarrow> real"
  assumes unit_i: "i \<bullet> i = 1" and
          C': "(C has_derivative C') (at p)" and
          f': "(f has_derivative f') (at (C p))" and
          C'_scaling: "C' j = a *\<^sub>R i" (*I basically need to guarantee that C v \<bullet> i *\<^sub>R i = C v*)
  shows "\<partial>(C\<^bsub>i\<^esub>) / \<partial>j |\<^bsub>p\<^esub> * \<partial> f / \<partial>i |\<^bsub>(C p)\<^esub>  = \<partial>(f o C) / \<partial>j |\<^bsub>p\<^esub>"
proof-
  let ?pd_fun = "(%b x. p - (p \<bullet> b) *\<^sub>R b + x *\<^sub>R b)"
  have "((C\<^bsub>i\<^esub>) has_derivative (C'\<^bsub>i\<^esub>)) (at p)" for i
    using has_derivative_inner_left[OF C', where b = i ] by auto
  then have "(((C\<^bsub>i\<^esub>) o (?pd_fun b)) has_vector_derivative ((C'\<^bsub>i\<^esub>) b)) (at (p \<bullet> b))" for i b
    apply(auto intro!: vector_derivative_diff_chain_within[where g = "(C\<^bsub>i\<^esub>)" and f = "?pd_fun b" and S = UNIV and f' = b and g' = "(C'\<^bsub>i\<^esub>)" and x = "p \<bullet> b"])
    subgoal by(auto intro!: derivative_eq_intros)
    subgoal using has_derivative_at_withinI by blast
    done
  then have 1: "vector_derivative ((C\<^bsub>i\<^esub>) o (?pd_fun b)) (at (p \<bullet> b)) = (C'\<^bsub>i\<^esub>) b" for i b
    using vector_derivative_at by blast
  have "((f o (\<lambda>x.(C p - (C\<^bsub>b\<^esub>) p *\<^sub>R b2 + x *\<^sub>R b2))) has_vector_derivative (f' b2)) (at ((C\<^bsub>b\<^esub>) p))" for b b2
    apply(auto intro!: vector_derivative_diff_chain_within[where g = "f" and f = "(\<lambda>x.(C p - (C\<^bsub>b\<^esub>) p *\<^sub>R b2 + x *\<^sub>R b2))" and S = UNIV and f' = b2 and g' = "f'" and x = "(C\<^bsub>b\<^esub>) p"])
    subgoal by(auto intro!: derivative_eq_intros)
    subgoal using f' has_derivative_at_withinI by blast
    done
  then have 2: "vector_derivative (f o (\<lambda>x.(C p - (C\<^bsub>b\<^esub>) p *\<^sub>R b2 + x *\<^sub>R b2))) (at ((C\<^bsub>b\<^esub>) p)) = f' b2" for b b2
    using vector_derivative_at by blast
  have FoC_deriv: "(f \<circ> C has_derivative f' \<circ> C') (at p)"
    using diff_chain_at[OF C' f'] by auto
  have "(((f \<circ> C) o (?pd_fun j)) has_vector_derivative ((f' \<circ> C') j)) (at (p \<bullet> j))"
    apply(rule vector_derivative_diff_chain_within[where g = "f o C" and f = "?pd_fun j" and S = UNIV and f' = j and g' = "f' \<circ> C'" and x = "p \<bullet> j"])
    subgoal by(auto intro!: derivative_eq_intros)
    subgoal using FoC_deriv by (simp add: has_derivative_at_withinI)
    done
  then have 3: "vector_derivative ((f \<circ> C) o (?pd_fun j)) (at (p \<bullet> j)) = ((f' \<circ> C') j)"
    using vector_derivative_at by blast
  have "linear f'" using f'
    using has_derivative_linear by auto
  then show "\<partial>(C\<^bsub>i\<^esub>) / \<partial>j |\<^bsub>p\<^esub> * \<partial> f / \<partial> i |\<^bsub>(C p)\<^esub>  = \<partial>(f o C) / \<partial>j |\<^bsub>p\<^esub>"
    using unit_i
    unfolding partial_vector_derivative_def linear_iff Modules.additive_def
    using 1[where b = j and i = i  ] 2[where b = "i" and ?b2.0 = i] 3 C'_scaling
    by(auto simp add: o_def)
qed

lemma vector_derivative_diff_chain_within:
  assumes Df: "(f has_vector_derivative f') (at x within s)"
    and Dg: "(g has_derivative g') (at (f x) within f`s)"
  shows "((g \<circ> f) has_vector_derivative (g' f')) (at x within s)"
  using diff_chain_within[OF Df[unfolded has_vector_derivative_def] Dg]
    linear.scaleR[OF has_derivative_linear[OF Dg]]
  unfolding has_vector_derivative_def o_def
  by (auto simp: o_def mult.commute has_vector_derivative_def)

lemma partial_vector_derivative_cong:
  shows "(\<And>x. f x = g x) \<Longrightarrow> \<partial> f / \<partial> i |\<^bsub>x\<^esub> = \<partial> g / \<partial> i |\<^bsub>x\<^esub>"
  unfolding partial_vector_derivative_def
  by simp

lemma partial_deriv_minus:
  assumes "partially_vector_differentiable f i p"
  shows "\<partial>(%x. - f x) / \<partial>i |\<^bsub>p\<^esub> = - \<partial>f / \<partial>i |\<^bsub>p\<^esub>"
  unfolding partial_vector_derivative_def
  apply(rule vector_derivative_minus_at)
  using assms
  unfolding differentiable_def partially_vector_differentiable_def has_partial_vector_derivative_def has_vector_derivative_def
  by fastforce

lemma has_partially_vec_deriv_minus:
  assumes "has_partial_vector_derivative f i f' p"
  shows "has_partial_vector_derivative (%x. - f x) i (- f') p"
  using assms has_vector_derivative_minus unfolding has_partial_vector_derivative_def
  by auto

lemma partially_vec_diff_minus:
  assumes "partially_vector_differentiable f i p"
  shows "partially_vector_differentiable (%x. - f x) i p"
  using has_partially_vec_deriv_minus assms
  unfolding partially_vector_differentiable_def
  by force

proposition has_derivative_Pair_2:
  assumes f: "(f has_derivative f') (at x1 within (fst ` s))"
    and g: "(g has_derivative g') (at x2 within (snd ` s))"
  shows "((\<lambda>x. (f (fst x), g  (snd x))) has_derivative (\<lambda>h. (f' (fst h), g' (snd h)))) (at (x1,x2) within s)"
proof-
  have "((f o fst) has_derivative (f' o fst)) (at (x1, x2) within s)"
    unfolding o_def
    apply(rule has_derivative_in_compose[where g = f and f = fst and g' = f' and f' = fst])
     apply(intro derivative_intros)
    using f
    by auto
  moreover have "((g o snd) has_derivative (g' o snd)) (at (x1, x2) within s)"
    unfolding o_def
    apply(rule has_derivative_in_compose[where g = g and f = snd and g' = g' and f' = snd])
     apply(intro derivative_intros)
    using g
    by auto
  ultimately show ?thesis
    apply(intro derivative_intros)
    by (simp add: o_def)+
qed

lemma f'_comp_pair:
  fixes S and f::"(real \<times> 'a::banach) \<Rightarrow> 'b::{real_normed_vector}" and f':: "real \<times> 'a \<Rightarrow> (real \<times> 'a) \<Rightarrow> 'b"
  assumes f_deriv: "\<And>tx. tx \<in> S \<Longrightarrow> (f has_derivative (f' tx)) (at tx within S)" and
          g_deriv: "(g has_derivative g') (at s1 within (fst ` S2))" and
          h_deriv: "(h has_derivative h') (at s2 within (snd ` S2))" and
          img_sub: "(\<And>x1 x2. (x1, x2) \<in> S2 \<Longrightarrow> (g x1, h x2) \<in> S)" and
          point_in: "(s1,s2) \<in> S2"
        shows "((\<lambda>(x,y). f (g x, h y)) has_derivative (\<lambda>(x,y). f' (g s1, h s2) (g' x, h' y))) (at (s1,s2) within S2)"
  unfolding split_beta'
  apply(intro has_derivative_in_compose2[OF f_deriv, where f = "\<lambda>(x, y). (g x, h y)" and f' = "\<lambda>(x,y). (g' x, h' y)" and x = "(s1, s2)", unfolded split_beta' fst_conv snd_conv])
  by (auto intro: has_derivative_Pair_2 assms)

lemma has_partial_vector_derivative_inner:
  "\<lbrakk>has_partial_vector_derivative (f::'a::euclidean_space \<Rightarrow> 'b::euclidean_space) u (f_u') p\<rbrakk>
   \<Longrightarrow> has_partial_vector_derivative (f\<^bsub>v\<^esub>) u (f_u' \<bullet> v) p"
  by(auto intro!: derivative_eq_intros
          simp add: has_partial_vector_derivative_def has_vector_derivative_def)

lemma partial_derivative_component_sum:
  assumes G_deriv: "partially_vector_differentiable G v p"
  shows "\<partial>G / \<partial>v |\<^bsub>p\<^esub> = (\<Sum>i\<in>Basis. ((\<partial>(G\<^bsub>i\<^esub>) / \<partial>v |\<^bsub>p\<^esub>) *\<^sub>R i))"
proof-
  obtain G_v where G_v: "has_partial_vector_derivative G v G_v p"
    using G_deriv
    unfolding partially_vector_differentiable_def
    by auto
  then have "G_v = (\<Sum>i\<in>Basis. (G_v \<bullet> i) *\<^sub>R i)"
    using euclidean_representation by metis
  moreover have "\<partial>G / \<partial>v |\<^bsub>p\<^esub> = G_v"
    using G_v partial_vector_derivative_works_2
    by auto
  moreover have "\<partial>(G\<^bsub>i\<^esub>) / \<partial>v |\<^bsub>p\<^esub> = G_v \<bullet> i" for i
    using has_partial_vector_derivative_inner[OF G_v] partial_vector_derivative_works_2
    by blast
  ultimately show ?thesis
    by auto
qed

lemma sum_sing:
  "sum f {a} = f a"
  by auto

text \<open>@{term Basis} below is that of 'a\<close>
lemma partial_derivative_chain:
  fixes S :: "'a::euclidean_space set" and T :: "'a set" and 
        F :: "'a \<Rightarrow> 'b::euclidean_space"
  assumes F_deriv: "\<And>x. x \<in> T \<Longrightarrow> (F has_derivative (F' x)) (at x)" and
          G_deriv: "\<And>x. x \<in> S \<Longrightarrow> (G has_derivative (G' x)) (at x)" and
          sub: "G ` S \<subseteq> T" and
          point_in: "p \<in> S" and
          unit_v: "v \<bullet> v = 1"
  shows "\<partial>(F o G) / \<partial>v |\<^bsub>p\<^esub> = (\<Sum>i\<in>Basis. \<partial>(G\<^bsub>i\<^esub>) / \<partial>v |\<^bsub>p\<^esub> *\<^sub>R \<partial>F / \<partial>i |\<^bsub>G p\<^esub>)"
proof-
  have "((F o G) has_derivative (\<lambda>x. (F' (G p) (G' p x)))) (at p)"
    apply(intro derivative_eq_intros)
  proof- 
    show "(G has_derivative (G' p)) (at p)"
      using G_deriv[OF point_in].
    show "(F has_derivative (F' (G p))) (at (G p) within (range G))"
      apply(intro has_derivative_subset[OF F_deriv])
      using sub point_in
      by auto
  qed auto
  then have "has_partial_vector_derivative (F o G) v (F' (G p) (G' p v)) p"
    apply(intro has_derivative_to_has_partial_deriv unit_v)
    using point_in by blast
  then have "\<partial>(F o G) / \<partial>v |\<^bsub>p\<^esub> = (F' (G p) (G' p v))"
    by (simp add: partial_vector_derivative_works_2 point_in)
  moreover have "(G' p v) = \<partial>G / \<partial>v |\<^bsub>p\<^esub>"
    by (intro partial_vector_derivative_works_2[symmetric] has_derivative_to_has_partial_deriv unit_v G_deriv point_in)
  moreover have "\<partial>G / \<partial>v |\<^bsub>p\<^esub> = (\<Sum>i\<in>Basis. (\<partial>(G\<^bsub>i\<^esub>) / \<partial>v |\<^bsub>p\<^esub>) *\<^sub>R i)"
    apply(rule partial_derivative_component_sum)
    apply(simp add: partially_vector_differentiable_def)
    using G_deriv has_derivative_to_has_partial_deriv'' point_in
    by blast
  ultimately have "\<partial>(F o G) / \<partial>v |\<^bsub>p\<^esub> = F' (G p) (\<Sum>i\<in>Basis. (\<partial>(G\<^bsub>i\<^esub>) / \<partial>v |\<^bsub>p\<^esub>) *\<^sub>R i)"
    by auto
  moreover have "linear (F' (G p))"
    using has_derivative_linear[OF F_deriv, where ?x1 = "G p"] point_in sub
    by auto
  moreover have "F' (G p) (x *\<^sub>R y) = x *\<^sub>R (F' (G p) y)" for x y
    apply(intro linear_scale)
    using has_derivative_linear[OF F_deriv, where ?x1 = "G p"] point_in sub
    by auto
  ultimately have "\<partial>(F o G) / \<partial>v |\<^bsub>p\<^esub> = (\<Sum>i\<in>Basis. (\<partial>(G\<^bsub>i\<^esub>) / \<partial>v |\<^bsub>p\<^esub>) *\<^sub>R F' (G p) i)"
    by(simp add: real_vector.linear_sum)
  moreover have "\<partial>F / \<partial>k |\<^bsub>q\<^esub> = F' q k" if "q \<in> T" and "k \<bullet> k = 1" for q k
    by (intro partial_vector_derivative_works_2 has_derivative_to_has_partial_deriv that F_deriv)
  ultimately show ?thesis
    using point_in sub
    by (simp add: subset_eq)
qed

lemma partial_derivative_chain_pair:
  (*This could very easily be generalised to non-pairs*)
  fixes S :: "(real \<times> real) set" and T :: "(real \<times> real) set" and 
        F :: "(real \<times> real) \<Rightarrow> 'a::euclidean_space"
  assumes F_deriv: "\<And>x. x \<in> T \<Longrightarrow> (F has_derivative (F' x)) (at x)" and
          G_deriv: "\<And>x. x \<in> S \<Longrightarrow> (G has_derivative (G' x)) (at x)" and
          sub: "G ` S \<subseteq> T" and
          point_in: "p \<in> S" and
          i_is_x_axis: "i = (1::real,0)" and 
          j_is_y_axis: "j = (0,1)" and
          unit_v: "v \<bullet> v = 1"
        shows "\<partial>(F o G) / \<partial>v |\<^bsub>p\<^esub> = (\<partial>(G\<^bsub>i\<^esub>) / \<partial>v |\<^bsub>p\<^esub>) *\<^sub>R \<partial>F / \<partial>i |\<^bsub>G p\<^esub> + (\<partial>(G\<^bsub>j\<^esub>) / \<partial>v |\<^bsub>p\<^esub>) *\<^sub>R \<partial>F / \<partial>j |\<^bsub>G p\<^esub>"
proof-
  have *: "Basis = {i, j}"
    by (simp add: i_is_x_axis j_is_y_axis real_pair_basis)
  have **: "(\<Sum>k\<in>(insert i {j}). f k) = f i + (\<Sum>j\<in>{j} - {i}. f j)" for f
      apply(intro sum.insert_remove)
      by auto
  have "(\<partial> F \<circ> G / \<partial> v |\<^bsub>p\<^esub>) = (\<Sum>i\<in>Basis. (\<partial> G\<^bsub>i\<^esub> / \<partial> v |\<^bsub>p\<^esub>) *\<^sub>R (\<partial> F / \<partial> i |\<^bsub>G p\<^esub>))" 
    using partial_derivative_chain[OF assms(1,2,3,4,7)]
    by simp
  then show ?thesis
    by (auto simp add: * ** i_is_x_axis j_is_y_axis)
qed

lemma partial_vector_derivative_inner:
  assumes "has_partial_vector_derivative F v F_v p"
  shows "(\<partial> (F\<^bsub>b\<^esub>) / \<partial> v |\<^bsub>p\<^esub>) = (\<partial> F / \<partial> v |\<^bsub>p\<^esub>) \<bullet> b "
  using has_partial_vector_derivative_inner partial_vector_derivative_works_2
  using assms
  by metis

lemma has_partial_vector_derivative_id[partial_derivative_intros,simp]:
  "has_partial_vector_derivative (id\<^bsub>v\<^esub>) u (u \<bullet> v) p"
  "(\<partial> (id\<^bsub>v\<^esub>) / \<partial> u |\<^bsub>p\<^esub>) = u \<bullet> v"
  "(\<partial> (\<lambda>x. x \<bullet> v) / \<partial> u |\<^bsub>p\<^esub>) = u \<bullet> v"
  by(auto intro!: derivative_eq_intros partial_derivative_intros
          simp add: has_partial_vector_derivative_def has_vector_derivative_def)

lemma has_partial_vector_derivative_add[partial_derivative_intros]:
  "\<lbrakk>has_partial_vector_derivative (f::'a::euclidean_space \<Rightarrow> 'b::euclidean_space) u (f_u') p;
    has_partial_vector_derivative (g::'a::euclidean_space \<Rightarrow> 'b::euclidean_space) u (g_u') p\<rbrakk>
   \<Longrightarrow> has_partial_vector_derivative (\<lambda>x. f x + g x) u (f_u' + g_u') p"
  by(auto intro!: derivative_eq_intros
          simp add: has_partial_vector_derivative_def has_vector_derivative_def algebra_simps)

lemma has_partial_vector_derivative_scale[partial_derivative_intros]:
  "\<lbrakk>has_partial_vector_derivative (f::'a::euclidean_space \<Rightarrow> 'b::euclidean_space) u (f_u') p\<rbrakk>
   \<Longrightarrow> has_partial_vector_derivative (\<lambda>x. c *\<^sub>R f x) u (c *\<^sub>R f_u') p"
  by(auto intro!: derivative_eq_intros
          simp add: has_partial_vector_derivative_def has_vector_derivative_def)

lemma partial_derivative_eq[partial_derivative_intros]:
  "\<lbrakk>has_partial_vector_derivative F u F_u x; has_partial_vector_derivative G u F_u x\<rbrakk> \<Longrightarrow> 
       (\<partial>F / \<partial>u |\<^bsub>x\<^esub>) = (\<partial>G / \<partial>u |\<^bsub>x\<^esub>)"
  by (auto dest!: partial_vector_derivative_works_2)

lemma partial_derivative_eq_inner[partial_derivative_intros]:
  assumes "has_partial_vector_derivative F u F_u x" "has_partial_vector_derivative G u F_u x"
  shows "(\<partial>(F\<^bsub>v\<^esub>) / \<partial>u |\<^bsub>x\<^esub>) = (\<partial>(G\<^bsub>v\<^esub>) / \<partial>u |\<^bsub>x\<^esub>)"
proof-
  have "(\<partial>F / \<partial>u |\<^bsub>x\<^esub>) = (\<partial>G / \<partial>u |\<^bsub>x\<^esub>)"
    using partial_derivative_eq[OF assms] .
  thus ?thesis
    using assms
    by (auto simp add: partial_vector_derivative_inner)
qed

end