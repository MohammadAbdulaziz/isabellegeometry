\section{Basic Concepts and Lemmas}
\label{sec:defs}
In this section we discuss the basic lemmas we need to prove Green's theorem.
However, we need to firstly discuss two basic definitions needed to state the theorem statement: \emph{line integrals} and \emph{partial derivatives}.
Definitions of both of those two concepts are ubiquitous in the literature, nonetheless, we had to adapt them to be defined on the Euclidean spaces type class in the Isabelle multivariate analysis library.

We define the line integral of a function $F$ on the parameterised path $\gamma$ as follows:
% it is usually defined in the literature  \cite[chapter X]{zorich2004mathematical}.
\begin{mydef}{Line Integral}
\[\work{F}{\gamma}{B} = \int_{0}^{1} \Sigma_{b \epsilon B}( (F(\gamma(t)) \cdot b) (\gamma'(t) \cdot b)) dt  \]
\end{mydef}
A difference in our definition is that we add the argument $B$, a set of vectors, to which $F$ and $\gamma$, and accordingly the line integral are projected.
The reasons for adding the $B$ argument will become evident later.
\noindent Above, $\cdot$ denotes the inner product of two vectors.
Formally, the definition is:
\begin{isabelle}
\isacommand{definition}\isamarkupfalse%
\ line\_integral::\isanewline
"({\isacharprime}a::euclidean\_space\ {\isasymRightarrow}\ {\isacharprime}a\ )\ {\isasymRightarrow}\ {\isacharprime}a\ set\ {\isasymRightarrow}\ (real\ {\isasymRightarrow}\ {\isacharprime}a)\ {\isasymRightarrow}\ real"\isanewline
\isakeyword{where}\ "line\_integral\ F\ B\ {\isasymgamma}\ =\isanewline
\ \ integral\ {{\isadigit{0}} ..\ {\isadigit{1}}}\isanewline
\ \ \ ({\isasymlambda}t.\ {\isasymSum}b{\isasymin}B.\ (F({\isasymgamma}\ t)\ {\isasymbullet}\ b)\ *\isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ (vector\_derivative\ {\isasymgamma}\ (at\ t\ within\ {{\isadigit{0}}..{\isadigit{1}}})\ {\isasymbullet}\ b))"\isanewline
\isacommand{definition}\isamarkupfalse%
\ line\_integral\_exists\ \isakeyword{where}\isanewline
\ \ "line\_integral\_exists\ F\ B\ {\isasymgamma}\ =\isanewline
\ \ \ \ (({\isasymlambda}t.\ {\isasymSum}b{\isasymin}B.\ F\ ({\isasymgamma}\ t)\ {\isasymbullet}\ b\ *\isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ (vector\_derivative\ {\isasymgamma}\ (at\ t\ within\ {{\isadigit{0}}..{\isadigit{1}}})\ {\isasymbullet}\ b))\isanewline
\ \ \ \ \ integrable\_on\ {{\isadigit{0}}..{\isadigit{1}}})"
\end{isabelle}
\noindent 
Note that \isa{integral} refers to the Henstock-Kurzweil gauge integral implementation in Isabelle/HOL library.
As one would expect, the line integral distributes over unions of sets of vectors and path joins.

\begin{isabelle}
\isacommand{lemma}\isamarkupfalse%
\ line\_integral\_sum\_gen:\isanewline
\ \ \ \ \isakeyword{assumes}\ "finite\ B"\isanewline
\ \ \ \ \ \ \ \ \ \ \ \ "line\_integral\_exists\ F\ B{\isadigit{1}}\ {\isasymgamma}"\isanewline
\ \ \ \ \ \ \ \ \ \ \ \ "line\_integral\_exists\ F\ B{\isadigit{2}}\ {\isasymgamma}"\isanewline
\ \ \ \ \ \ \ \ \ \ \ \ "B{\isadigit{1}}\ {\isasymunion}\ B{\isadigit{2}}\ =\ B"\ "B{\isadigit{1}}\ {\isasyminter}\ B{\isadigit{2}}\ =\ {}"\ \isanewline
\ \ \ \ \isakeyword{shows}\ "line\_integral\ F\ B\ {\isasymgamma}\ =\isanewline
\ \ \ \ \ \ \ \ \ \ (line\_integral\ F\ \ B{\isadigit{1}}\ {\isasymgamma})\ +\ (line\_integral\ F\ B{\isadigit{2}}\ {\isasymgamma})"\isanewline\isanewline
\isacommand{lemma}\isamarkupfalse%
\ line\_integral\_distrib:\isanewline
\ \ \ \ \isakeyword{assumes}\ "line\_integral\_exists\ f\ B\ {\isasymgamma}{\isadigit{1}}"\isanewline
\ \ \ \ \ \ \ \ \ \ \ \ "line\_integral\_exists\ f\ B\ {\isasymgamma}{\isadigit{2}}"\isanewline
\ \ \ \ \ \ \ \ \ \ \ \ "valid\_path\ {\isasymgamma}{\isadigit{1}}"\ "valid\_path\ {\isasymgamma}{\isadigit{2}}"\isanewline
\ \ \ \ \isakeyword{shows}\ "line\_integral\ f\ B\ ({\isasymgamma}{\isadigit{1}}\ +++\ {\isasymgamma}{\isadigit{2}})\ ={\isanewline}
\ \ \ \ \ \ \ \ \ \ line\_integral\ f\ B\ {\isasymgamma}{\isadigit{1}}\ +\ line\_integral\ f\ B\ {\isasymgamma}{\isadigit{2}}"
\end{isabelle}
\noindent The line integral also admits a transformation equivalent to integration by substitution.
This lemma applies to paths where all components are defined as a function in terms of one component orthogonal to all of them.
It is a critical lemma for proving Green's theorem for ``elementary'' regions (to be defined later).
\begin{isabelle}
\isacommand{lemma}\isamarkupfalse%
\ line\_integral\_on\_pair\_path:\isanewline
\ \isakeyword{fixes}\ F::"({\isacharprime}a::euclidean\_space)\ {\isasymRightarrow}\ ({\isacharprime}a)"\isanewline
\ \ \ \ \ g::"real\ {\isasymRightarrow}\ {\isacharprime}a"\isanewline
\ \ \ \ \ {\isasymgamma}::"(real\ {\isasymRightarrow}\ {\isacharprime}a)"\isanewline
\ \ \ \ \ b::{\isacharprime}a\ \isanewline
\ \isakeyword{assumes}\ "b\ {\isasymin}\ Basis"\isanewline
\ \ \ \ \ \ \ \ "{\isasymforall}x.\ g(x)\ {\isasymbullet}\ b\ =\ {\isadigit{0}}"\ \ \isanewline
\ \ \ \ \ \ \ \ "\isasymgamma\ =\ ({\isasymlambda}t.\ f(t)\ *\isactrlsub R\ b\ +\ g(f(t)))"\isanewline
\ \ \ \ \ \ \ \ "{\isasymgamma}\ C{\isadigit{1}}\_differentiable\_on\ {{\isadigit{0}}\ ..\ {\isadigit{1}}}"\isanewline
\ \ \ \ \ \ \ \ "continuous\_on\ (f\ {\isacharbackquote}\ {{\isadigit{0}}..{\isadigit{1}}})\ g"\isanewline
\ \ \ \ \ \ \ \ "(pathstart\ {\isasymgamma})\ {\isasymbullet}\ b\ {\isasymle}\ (pathfinish\ {\isasymgamma})\ {\isasymbullet}\ b"\isanewline
\ \ \ \ \ \ \ \ "continuous\_on\ (path\_image\ {\isasymgamma})\ ({\isasymlambda}x.\ F\ x\ {\isasymbullet}\ b)"\isanewline
\ \isakeyword{shows}\ "(line\_integral\ F\ {b}\ {\isasymgamma})\ \isanewline
\ \ \ \ \ \ \ \ \ =\ integral\ (cbox\ ((pathstart\ {\isasymgamma})\ {\isasymbullet}\ b)\isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ ((pathfinish\ {\isasymgamma})\ {\isasymbullet}\ b))\isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ ({\isasymlambda}f\_var.\ (F\ (f\_var\ *\isactrlsub R\ b\ +\ g(f\_var))\ {\isasymbullet}\ b))"
\end{isabelle}



\textit{Partial derivatives} are defined on the Euclidean space type class implemented in Isabelle/HOL.
For a function $F$ defined on a Euclidean space, we define its partial derivative to be w.r.t. the change in the magnitude of a component vector $b$ of its input.
At a point $a$, the partial derivative is defined as:
\begin{mydef}{Partial Derivative}
\[\left. \frac{\partial F(v)}{\partial b}\right\rvert_{v = a} = \left. \frac{dF(a + (x - a \cdot b) b)}{dx}\right \rvert_{x = a \cdot b}\]
\end{mydef}
\noindent
Again, this definition is different from the classical definition in that the partial derivative is w.r.t. the change of magnitude of a vector rather than the change in one of the variables on which $F$ is defined.
However, our definition is similar to the classical definition of a partial derivative, when $b$ is a base vector.
Formally we define it as:
\begin{isabelle}
\isacommand{definition}\isamarkupfalse%
\ has\_partial\_vector\_derivative::\isanewline
"({\isacharprime}a::euclidean\_space\ {\isasymRightarrow}\ {\isacharprime}b::euclidean\_space)\ {\isasymRightarrow}\ {\isacharprime}a\ {\isasymRightarrow}\ {\isacharprime}b\ {\isasymRightarrow}\ {\isacharprime}a\isanewline
{\isasymRightarrow}\ bool"\ \isakeyword{where}\isanewline
"has\_partial\_vector\_derivative\ F\ b\ F{\isacharprime}\ a\ \isanewline
\ \ =\ (({\isasymlambda}x.\ F(\ (a\ -\ ((a\ {\isasymbullet}\ b)\ *\isactrlsub R\ b))\ +\ x\ *\isactrlsub R\ b\ ))\isanewline
\ \ \ \ \ has\_vector\_derivative\ F{\isacharprime})\ (at\ (a\ {\isasymbullet}\ b))"\isanewline
\isanewline
\isacommand{definition}\isamarkupfalse%
\ partially\_vector\_differentiable\ \isakeyword{where}\isanewline
"partially\_vector\_differentiable\ F\ b\ p\isanewline
\ \ \ =\ ({\isasymexists}F{\isacharprime}.\ has\_partial\_vector\_derivative\ F\ b\ F{\isacharprime}\ p)"\isanewline
\isanewline
\isacommand{definition}\isamarkupfalse%
\ partial\_vector\_derivative::\isanewline
"({\isacharprime}a::euclidean\_space\ {\isasymRightarrow}\ {\isacharprime}b::euclidean\_space)\ {\isasymRightarrow}\ {\isacharprime}a\ {\isasymRightarrow}\ {\isacharprime}a\ {\isasymRightarrow}\ {\isacharprime}b"\isanewline
\isakeyword{where}\ "partial\_vector\_derivative\ F\ b\ a\ \isanewline
\ \ \ \ \ =\ (vector\_derivative\isanewline
\ \ \ \ \ \ \ \ \ \ ({\isasymlambda}x.\ F(\ (a\ -\ ((a\ {\isasymbullet}\ b)\ *\isactrlsub R\ b))\ +\ x\ *\isactrlsub R\ b))\ (at\ (a\ {\isasymbullet}\ b)))"
\end{isabelle}
\noindent The following FTC for the partial derivative follows from the FTC for the vector derivative that is proven in Isabelle/HOL analysis library.
\begin{isabelle}
\isacommand{lemma}\isamarkupfalse%
\ fundamental\_theorem\_of\_calculus\_partial\_vector\_gen:\ \isanewline
\ \isakeyword{fixes}\ k{\isadigit{1}}\ k{\isadigit{2}}::"real"\isanewline
\ \ \ \ \ \ \ F::"({\isacharprime}a::euclidean\_space\ {\isasymRightarrow}\ {\isacharprime}b::euclidean\_space)"\isanewline
\ \ \ \ \ \ \ b::"{\isacharprime}a"\isanewline
\ \ \ \ \ \ \ F\_b::"({\isacharprime}a::euclidean\_space\ {\isasymRightarrow}\ {\isacharprime}b)"\isanewline
\ \isakeyword{assumes}\ "k{\isadigit{1}}\ {\isasymle}\ k{\isadigit{2}}"\isanewline
\ \ \ \ \ \ \ \ "b\ {\isasymbullet}\ b\ =\ {\isadigit{1}}"\isanewline
\ \ \ \ \ \ \ \ "c\ {\isasymbullet}\ b\ =\ {\isadigit{0}}\ "\isanewline
\ \ \ \ \ \ \ \ "{\isasymforall}\ p\ {\isasymin}\ D.\ has\_partial\_vector\_derivative\ F\ b\ (F\_b\ p)\ p"\isanewline
\ \ \ \ \ \ \ \ "{v.\ {\isasymexists}x.\ k{\isadigit{1}}\ {\isasymle}\ x\ {\isasymand}\ x\ {\isasymle}\ k{\isadigit{2}}\ {\isasymand}\ v\ =\ x\ *\isactrlsub R\ b\ +\ c}\ {\isasymsubseteq}\ D"\isanewline
\ \isakeyword{shows}\ "(\ ({\isasymlambda}x.\ F\_b(\ x\ *\isactrlsub R\ b\ +\ c))\ has\_integral\ \isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ F(k{\isadigit{2}}\ *\isactrlsub R\ b\ +\ c)\ -\ F(k{\isadigit{1}}\ *\isactrlsub R\ b\ +\ c))\ (cbox\ k{\isadigit{1}}\ k{\isadigit{2}})"
\end{isabelle}

Given these definitions and basic lemmas, we can now start elaborating on our formalisation of Green's theorem.
The first issue is how to formalise $\mathbb{R}^2$.
We use pairs of \isa{real} to refer to members of $\mathbb{R}^2$, where we define the following \isa{locale} that fixes the base vector names:
\begin{isabelle}
\isacommand{locale}\isamarkupfalse%
\ R{\isadigit{2}}\ =\isanewline
\ \ \ \ \ \isakeyword{fixes}\ i\ j\isanewline
\ \ \ \ \ \isakeyword{assumes}\ i\_is\_x\_axis:\ "i\ =\ ({\isadigit{1}}::real,{\isadigit{0}}::real)"\ \isakeyword{and}\isanewline
\ \ \ \ \ \ \ \ \ \ \ j\_is\_y\_axis:\ "j\ =\ ({\isadigit{0}}::real,\ {\isadigit{1}}::real)"
\end{isabelle}

Proofs of Green's theorem usually start by proving ``half'' of the theorem statement for every type of ``elementary regions'' in $\mathbb{R}^2$.
These regions are referred to as Type I, Type II or Type III regions, defined below.%
\footnote{Using elementary regions that are bounded by $C^1$ smooth functions is as general as using piece-wise smooth functions because it can be shown that the latter can be divided into regions of the former type (see \cite{protter2006basic}).}
\begin{mydef}{Elementary Regions}

\noindent A region $D$ (modelled as a set of $\isa{real}$ pairs) is Type I iff there are $C^1$ smooth functions $g_1$ and $g_2$ such that for two constants $a$ and $b$:
\[D = \{(x,y) \mid a \leq x \leq b \wedge g_2(x) \leq y \leq g_1(x) \}.\]
Similarly D would be called type II iff for $g_1$, $g_2$, $a$ and $b$
\[D = \{(x,y) \mid a \leq y \leq b \wedge g_2(y) \leq x \leq g_1(y) \}.\]
Finally, a region is of type III if it is both of type I and type II.
\end{mydef}



To prove Green's theorem a common approach is to prove the following two ``half'' Green's theorems, for any regions $D_x$ and $D_y$ that are type I and type II, respectively, and their positively oriented boundaries:
\[ \underset{ D_x }{\int } - \frac{\partial(F_i)}{\partial j}\ dxdy = \work{ F}{\partial D_x}{\{i\}},\]and
\[ \underset{ D_y }{\int } \frac{\partial(F_j)}{\partial i}\ dxdy = \work{ F}{\partial D_y}{\{j\}}.\]
Here $i$ and $j$ are the base vectors while $F_i$ and $F_j$ are the $x$-axis and $y$-axis components, respectively, of $F$.
However, the difference in the expressions for the type I and type II regions is because of the asymmetry of the $x$-axis and the $y$-axis w.r.t. the orientation.
We refer to the top expression as the \emph{$x$-axis} Green's theorem, and the bottom one as the \textit{$y$-axis} Green's theorem.
Below is the statement of the $x$-axis Green's theorem for type I regions as we have formalised it in Isabelle/HOL.
For the boundary, we model its paths explicitly as functions of type \isa{ real \isasymRightarrow\ (real \isacharasterisk\ real)}, where \isa{\isasymgamma 1}, \isa{\isasymgamma 2}, \isa{\isasymgamma 3} and \isa{\isasymgamma 4} are the bottom, right, top and left sides, respectively.
\begin{isabelle}
\isacommand{lemma}\isamarkupfalse%
Greens\_thm\_type\_I:\isanewline
\isakeyword{fixes}\ F::"((real*real)\ {\isasymRightarrow}\ (real\ *\ real))"\isanewline
\ \ \ \ \ {\isasymgamma}{\isadigit{1}}\ {\isasymgamma}{\isadigit{2}}\ {\isasymgamma}{\isadigit{3}}\ {\isasymgamma}{\isadigit{4}}\ ::"(real\ {\isasymRightarrow}\ (real\ *\ real))"\isanewline
\ \ \ \ \ a::"real"\ b::"real"\ \isanewline
\ \ \ \ \ g{\isadigit{1}}::"(real\ {\isasymRightarrow}\ real)"\ g{\isadigit{2}}::"(real\ {\isasymRightarrow}\ real)"\isanewline
 \isakeyword{assumes}\ "Dx\ =\ {(x,y)\ .\ x\ {\isasymin}\ cbox\ a\ b\ {\isasymand}\ y\ {\isasymin}\ cbox\ (g{\isadigit{2}}\ x)\ (g{\isadigit{1}}\ x)}"\isanewline
\ \ \ \ \ \ \ "{\isasymgamma}{\isadigit{1}}\ =\ ({\isasymlambda}t.\ (a\ +\ (b\ -\ a)\ *\ t,\ g{\isadigit{2}}(a\ +\ (b\ -\ a)\ *\ t)))"\isanewline
\ \ \ \ \ \ \ "{\isasymgamma}{\isadigit{1}}\ C{\isadigit{1}}\_differentiable\_on\ {{\isadigit{0}}..{\isadigit{1}}}"\ \isanewline
\ \ \ \ \ \ \ "{\isasymgamma}{\isadigit{2}}\ =\ ({\isasymlambda}t.\ (b,\ g{\isadigit{2}}(b)\ +\ t\ \ *\isactrlsub R\ (g{\isadigit{1}}(b)\ -\ g{\isadigit{2}}(b))))"\isanewline
\ \ \ \ \ \ \ "{\isasymgamma}{\isadigit{3}}\ =\ ({\isasymlambda}t.\ (a\ +\ (b\ -\ a)\ *\ t,\ g{\isadigit{1}}(a\ +\ (b\ -\ a)\ *\ t)))"\ \isanewline
\ \ \ \ \ \ \ "{\isasymgamma}{\isadigit{3}}\ C{\isadigit{1}}\_differentiable\_on\ {{\isadigit{0}}..{\isadigit{1}}}"\isanewline
\ \ \ \ \ \ \ "{\isasymgamma}{\isadigit{4}}\ =\ ({\isasymlambda}t.\ (a,\ \ g{\isadigit{2}}(a)\ +\ t\ *\isactrlsub R\ (g{\isadigit{1}}(a)\ -\ g{\isadigit{2}}(a))))"\isanewline
\ \ \ \ \ \ \ "analytically\_valid\ Dx\ ({\isasymlambda}p.\ F(p)\ {\isasymbullet}\ i)\ j"\isanewline
\ \ \ \ \ \ \ "{\isasymforall}x\ {\isasymin}\ cbox\ a\ b.\ (g{\isadigit{2}}\ x)\ {\isasymle}\ (g{\isadigit{1}}\ x)"\ \isanewline
\ \ \ \ \ \ \ "a\ {\isacharless}\ b"\isanewline
\isakeyword{shows}\ "(line\_integral\ F\ {i}\ {\isasymgamma}{\isadigit{1}})\ +\ (line\_integral\ F\ {i}\ {\isasymgamma}{\isadigit{2}})\ -\isanewline
\ \ \ \ \ \ (line\_integral\ F\ {i}\ {\isasymgamma}{\isadigit{3}})\ -\ (line\_integral\ F\ {i}\ {\isasymgamma}{\isadigit{4}})\ =\isanewline
\ \ \ \ \ \ \ (integral\ Dx\isanewline
\ \ \ \ \ \ \ \ ({\isasymlambda}a.\ -\ (partial\_vector\_derivative\ ({\isasymlambda}p.\ F(p)\ {\isasymbullet}\ i)\ j\ a)))"
\end{isabelle}
Proving the lemma above depends on the observation that for a path $\gamma$ (e.g. \isa{\isasymgamma 1} above) that is straight along a vector $x$ (e.g. \isa{i}), $\work{ F}{\gamma}{\{x\}} = 0$, for an $F$ continuous on $\gamma$.
(Formally, this observation follows immediately from theorem \isa{work\_on\_pair\_path}.)
The rest of the proof boils down to an application of Fubini's theorem and the FTC to the double integral, the integral by substitution to the line integrals and some algebraic manipulation (\cite[p.\ts238]{zorich2004mathematical}).
Nonetheless, this algebraic manipulation proved to be quite tedious when done formally in Isabelle/HOL.

However, we did not discuss the predicate \isa{analytically\_valid}, which represents the analytic assumptions of our statement of Green's theorem, to which an ``appropriate'' field has to conform.
Firstly let $1_s$ be the indicator function for a set $s$.
Then, for the $x$-axis Green's theorem, our analytic assumptions are that
\begin{enumerate}
  \item $F_i$ is continuous on $D_x$
  \item $\frac{\partial(F_i)}{\partial j}$ exists everywhere in $D_x$
  \item the product $1_{D_x}(x,y) \frac{\partial(F_i)}{\partial j}(x,y)$ is Lebesgue integrable
  \item the product $1_{[a,b]}(x) \int_{g_1(x)}^{g_2(x)} F(x,y) dy$, where the integral is a Henstock-Kurzweil gauge integral, is a borel measurable function
\end{enumerate}
These assumptions vary symmetrically for the $y$-axis Green's theorem, so to avoid having two symmetrical definitions, we defined the predicate \isa{analytically\_valid} to take the axis (as a base vector) as an argument.
\begin{isabelle}
\isacommand{definition}\isamarkupfalse%
\ analytically\_valid::\isanewline
\ \ "{\isacharprime}a::euclidean\_space\ set\ {\isasymRightarrow}\isanewline
\ \ \ ({\isacharprime}a\ {\isasymRightarrow}\ {\isacharprime}b::{euclidean\_space,\ times,\ one,\ zero})\ {\isasymRightarrow}\ {\isacharprime}a\ {\isasymRightarrow}\ bool"\isanewline
\ \isakeyword{where}\isanewline
\ "analytically\_valid\ s\ F\ b\ =\isanewline
\ \ (({\isasymforall}\ a\ {\isasymin}\ s.\ partially\_vector\_differentiable\ F\ b\ a)\ {\isasymand}\isanewline
\ \ continuous\_on\ s\ F\ {\isasymand}\isanewline
\ \ integrable\ lborel\isanewline
\ \ \ \ ({\isasymlambda}p.\ (partial\_vector\_derivative\ F\ i)\ p\ *\ indicator\ s\ p)\ {\isasymand}\isanewline
\ \ ({\isasymlambda}x.\ integral\ UNIV\isanewline
\ \ \ \ \ \ \ \ ({\isasymlambda}y.\ (partial\_vector\_derivative\ F\ i\isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ (y\ *\isactrlsub R\ b\ +\ x\ *\isactrlsub R\ ({\isasymSum}\ b\ {\isasymin}(Basis\ -\ {i}).\ b)))\ *\isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ \ (indicator\ s\isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ (y\ *\isactrlsub R\ b\ +\ x\ *\isactrlsub R\ ({\isasymSum}\ b\ {\isasymin}(Basis\ -\ {i}).\ b))\ )))\isanewline
\ \ \ {\isasymin}\ borel\_measurable\ lborel)"
\end{isabelle}

%Although these assumptions are not the most general possible for Green's theorem, they are still more general than the assumptions of the most basic theorem statement, which requires the field and all of its partial derivatives to be continuous in the region.
These conditions refer to Lebesgue integrability and to measurability because we use Fubini's theorem for the Lebesgue integral in Isabelle/HOL's probability library to derive a Fubini like result for the Henstock-Kurzweil integral.
Proving Fubini's theorem for the gauge integral
% or using an altogether different integral (e.g. the integral introduced by W.B. Jurkat et al.\ \cite{jurkat1990general}, or Pfeffer's integral \cite{pfeffer1992riemann})
would allow for more general analytic assumptions.
%\footnote{Some of the most exotic integrals admit general analytic assumptions by definition, rather that it being a theorem derived from the definition.}
However, the rest of our approach would still be valid.

We prove the $y$-axis Green's theorem for type II regions similarly, where this is its conclusion.
\begin{isabelle}
\isakeyword{shows}\ "(line\_integral\ F\ {j}\ {\isasymgamma}{\isadigit{1}})\ +\ (line\_integral\ F\ {j}\ {\isasymgamma}{\isadigit{2}})\ -\isanewline
\ \ \ \ \ \ (line\_integral\ F\ {j}\ {\isasymgamma}{\isadigit{3}})\ -\ (line\_integral\ F\ {j}\ {\isasymgamma}{\isadigit{4}})\ =\isanewline
\ \ \ \ \ \ \ (integral\ Dy\isanewline
\ \ \ \ \ \ \ \ \ (partial\_vector\_derivative\ ({\isasymlambda}p.\ F(p)\ {\isasymbullet}\ j)\ i))"
\end{isabelle}
