theory Green
  imports Paths TotalDeriv Integrals
begin

definition analytically_valid:: "'a::euclidean_space set \<Rightarrow> ('a \<Rightarrow> 'b::{euclidean_space,times,one}) \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool" where
  "analytically_valid s F i j \<equiv>
       continuous_on s F \<and> 
       (\<forall>a \<in> s. partially_vector_differentiable F i a) \<and>
       set_integrable lborel s (\<partial> F / \<partial> i) \<and>
       (let p = (%x y. (y *\<^sub>R i + x *\<^sub>R j)) in
            ((\<lambda>x. integral UNIV (\<lambda>y. (indicator s (p x y)) *\<^sub>R (\<partial> F / \<partial> i |\<^bsub>p x y\<^esub>)))
                  \<in> borel_measurable lborel))"

lemma analytically_valid_imp_cont: "analytically_valid s F i j \<Longrightarrow> continuous_on s F"
  unfolding analytically_valid_def
  by auto

lemma analytically_valid_cong: "(\<And>x. F x = G x) \<Longrightarrow> analytically_valid s F i j \<Longrightarrow> analytically_valid s G i j"
  using HOL.ext
  by blast

lemma analytically_valid_imp_part_deriv_integrable_on:
  assumes "analytically_valid (s::(real*real) set) (f::(real*real)\<Rightarrow> real) i j"
  shows "(partial_vector_derivative f i) integrable_on s"
proof -
  have "integrable lborel (\<lambda>p. indicator s p * partial_vector_derivative f i p)"
    using assms
    by(simp add: analytically_valid_def set_integrable_def)
  then have "integrable lborel (\<lambda>p::(real*real). if p \<in> s then partial_vector_derivative f i p else 0)"
    using indic_ident[of "partial_vector_derivative f i"]
    by (smt \<open>\<And>s. (\<lambda>x. (\<partial> f / \<partial> i |\<^bsub>x\<^esub>) * indicat_real s x) = (\<lambda>x. if x \<in> s then (\<partial> f / \<partial> i |\<^bsub>x\<^esub>) else 0)\<close> integrable_cong mult_commute_abs)
  then have "(\<lambda>x. if x \<in> s then partial_vector_derivative f i x else 0) integrable_on UNIV"
    using Equivalence_Lebesgue_Henstock_Integration.integrable_on_lborel
    by auto
  then show "(partial_vector_derivative f i) integrable_on s"
    using integrable_restrict_UNIV
    by auto
qed

(*******************************************************************************)

lemma ite_add: "(if P x then (a::'a::euclidean_space) + b else c + d) = (if P x then b else d) + (if P x then a else c)" by (auto split: if_splits)

locale i_j_orthonorm =
  fixes i j::"'a::euclidean_space"
  assumes i_neq_j: "i \<noteq> j" and
    i_j_inner_zero[simp]: "i \<bullet> j = 0" and
    i_inner_unit[simp]: "i \<bullet> i = 1" and 
    j_inner_unit[simp]: "j \<bullet> j = 1" and
    bases_have_fubini: "\<And>f (s::'a set) g g1 g2 (a::real) (b::real). 
                    ((\<forall>x\<in>{a .. b} \<union> {b .. a}. g1 x \<le> g2 x) \<or> (\<forall>x\<in>{a .. b} \<union> {b .. a}. g2 x \<le> g1 x)) \<Longrightarrow>
                    ((integrable lborel f) \<Longrightarrow>
                    (\<And>x. (%y. f(y *\<^sub>R j + x *\<^sub>R i)) integrable_on UNIV) \<Longrightarrow>
                    ((%x. integral UNIV (%y. f(y *\<^sub>R j + x *\<^sub>R i))) \<in> borel_measurable lborel) \<Longrightarrow>
                    (f = (%x. if x \<in> s then g x else (0::real))) \<Longrightarrow>
                    (s = {point. \<exists>y x. point = (y *\<^sub>R j + x *\<^sub>R i) \<and> x \<in> {a .. b} \<union> {b .. a} \<and> y \<in> ({(g1 x) .. (g2 x)} \<union> {(g2 x) .. (g1 x)})}) \<Longrightarrow>
                    (integral s g = integral {a .. b} (\<lambda>x. integral ({(g1 x) .. (g2 x)} \<union> {(g2 x) .. (g1 x)}) (\<lambda>y. g(y *\<^sub>R j + x *\<^sub>R i))) +
                                       integral {b .. a} (\<lambda>x. integral ({(g1 x) .. (g2 x)} \<union> {(g2 x) .. (g1 x)}) (\<lambda>y. g(y *\<^sub>R j + x *\<^sub>R i)))))"
begin


lemma finite_ij: "finite {i,j}" by auto 

lemma i_norm_1: "norm i = 1" using i_inner_unit norm_eq_1 by blast
lemma j_norm_1: "norm j = 1" using j_inner_unit norm_eq_1 by blast

lemma analytically_valid_y:
  assumes "analytically_valid s F i j"
  shows "(\<lambda>x. integral UNIV (\<lambda>y. (indicat_real s (y*\<^sub>R i + x *\<^sub>R j)) * (\<partial>F/ \<partial>i |\<^bsub>y*\<^sub>R i + x *\<^sub>R j\<^esub>))) \<in> borel_measurable lborel"
    using assms  by(simp add: real_pair_basis analytically_valid_def)

lemma analytically_valid_x:
  assumes "analytically_valid s F j i"
  shows "(\<lambda>x. integral UNIV (\<lambda>y.  indicat_real s (y*\<^sub>R j + x *\<^sub>R i) *(\<partial> F/ \<partial> j |\<^bsub>(y*\<^sub>R j + x *\<^sub>R i)\<^esub>))) \<in> borel_measurable lborel"
    using assms  by(simp add: real_pair_basis analytically_valid_def)

lemma GreenThm_type_I:
  fixes F and
    \<gamma>1 \<gamma>2 \<gamma>3 \<gamma>4 and
    a::"real" and b::"real" and
    g1::"real \<Rightarrow> real" and g2::"real \<Rightarrow> real"
  assumes Dy_def: "Dy_pair = {p. \<exists>x y. p = x *\<^sub>R i + y *\<^sub>R j \<and> x \<in> {a .. b} \<union> {b .. a} \<and> y \<in> {(g2 x) .. (g1 x)} \<union> {(g1 x) .. (g2 x)}}" and
    gamma1_def: "\<gamma>1 = (\<lambda>x. let c = (linepath a b x) in c *\<^sub>R i + g2 c *\<^sub>R j)" and
    gamma1_smooth: "\<gamma>1 piecewise_C1_differentiable_on {0..1}" and (*TODO: This should be piecewise smooth*)
    gamma2_def: "\<gamma>2 = (\<lambda>x. b *\<^sub>R i + (linepath (g2 b) (g1 b) x) *\<^sub>R j)" and
    gamma3_def: "\<gamma>3 = (\<lambda>x. let c = (linepath a b x) in c *\<^sub>R i + g1 c *\<^sub>R j)" and
    gamma3_smooth: "\<gamma>3 piecewise_C1_differentiable_on {0..1}" and
    gamma4_def: "\<gamma>4 = (\<lambda>x. a *\<^sub>R i + (linepath (g2 a) (g1 a) x) *\<^sub>R j)" and
    F_i_analytically_valid: "analytically_valid Dy_pair (F\<^bsub>i\<^esub>) j i" and
    g2_leq_g1: "(\<forall>x \<in> {a .. b} \<union> {b .. a}. (g2 x) \<le> (g1 x)) \<or> (\<forall>x \<in> {a .. b} \<union> {b .. a}. (g1 x) \<le> (g2 x))" and (*This is needed otherwise what would Dy be?*)
    a_lt_b: "a \<noteq> b"
  shows "\<integral>\<^bsub>\<gamma>1\<^esub> F \<downharpoonright>\<^bsub>{i}\<^esub> + \<integral>\<^bsub>\<gamma>2\<^esub> F \<downharpoonright>\<^bsub>{i}\<^esub> - \<integral>\<^bsub>\<gamma>3\<^esub> F \<downharpoonright>\<^bsub>{i}\<^esub> - \<integral>\<^bsub>\<gamma>4\<^esub> F \<downharpoonright>\<^bsub>{i}\<^esub>
              = (if a < b then 1::int else -1) *
                (if (\<forall>x \<in> {a .. b} \<union> {b .. a}. (g2 x) \<le> (g1 x)) then 1::int else -1) *
                integral Dy_pair (\<lambda>a. - (\<partial> (F\<^bsub>i\<^esub>)/ \<partial>j |\<^bsub>a\<^esub>))" (is ?g)
    "line_integral_exists F {i} \<gamma>4" 
    "line_integral_exists F {i} \<gamma>3" 
    "line_integral_exists F {i} \<gamma>2" 
    "line_integral_exists F {i} \<gamma>1" 
proof -
  let ?F_b' = "\<partial> (F\<^bsub>i\<^esub>)/ \<partial>j"
  have F_first_is_continuous: "continuous_on Dy_pair (F\<^bsub>i\<^esub>)"
    using F_i_analytically_valid
    by (auto simp add: analytically_valid_def)
  let ?f = "(\<lambda>x. if x \<in> (Dy_pair) then (\<partial>(F\<^bsub>i\<^esub>)/ \<partial>j) x else 0)"
  have f_lesbegue_integrable: "integrable lborel ?f"
    using F_i_analytically_valid
    apply (auto simp add: analytically_valid_def set_integrable_def)
    by (smt indic_ident integrable_cong mult_commute_abs)
  have partially_vec_diff: "\<forall>a \<in> Dy_pair. partially_vector_differentiable (F\<^bsub>i\<^esub>) j a"
    using F_i_analytically_valid
    by (auto simp add: analytically_valid_def)
  have F_partially_differentiable: "\<forall>a\<in>Dy_pair. has_partial_vector_derivative (F\<^bsub>i\<^esub>) j (?F_b' a) a"
    using partial_vector_derivative_works partially_vec_diff
    by fastforce
  have g1_g2_continuous: "continuous_on ((cbox a b) \<union> (cbox b a)) g1" (is ?P1)
    "continuous_on ((cbox a b) \<union> (cbox b a)) g2" (is ?P2)
  proof -
    have "continuous_on {0..1} (g1 o (linepath a b))"
         "continuous_on {0..1} (g2 o (linepath a b))"
      using continuous_on_inner[OF piecewise_C1_differentiable_on_imp_continuous_on[OF gamma1_smooth], of "(\<lambda>x. j)", OF continuous_on_const]
            continuous_on_inner[OF piecewise_C1_differentiable_on_imp_continuous_on[OF gamma3_smooth], of "(\<lambda>x. j)", OF continuous_on_const]
      unfolding gamma1_def gamma3_def
      by(auto simp add: algebra_simps i_j_inner_zero j_inner_unit linepath_def)
    then show ?P1 ?P2 using continuous_on_linepath_compose'[OF a_lt_b] by blast+
  qed
  have g2_scale_j_contin: "continuous_on ((cbox a b) \<union> (cbox b a)) (\<lambda>x. g2 x *\<^sub>R j)" and
       g1_scale_j_contin: "continuous_on ((cbox a b) \<union> (cbox b a)) (\<lambda>x. g1 x *\<^sub>R j)"
    by (intro continuous_intros g1_g2_continuous)+
  let ?Dg1 =  "{p. \<exists>x. x \<in> ((cbox a b) \<union> (cbox b a)) \<and> p = (x *\<^sub>R i + g1(x) *\<^sub>R j)}"
  let ?Dg2 =  "{p. \<exists>x. x \<in> ((cbox a b) \<union> (cbox b a)) \<and> p = x *\<^sub>R i + g2(x) *\<^sub>R j}"
  have line_is_pair_img:
        "?Dg1 = (\<lambda>x. x *\<^sub>R i + g1(x) *\<^sub>R j) ` ((cbox a b) \<union> (cbox b a))"
        "?Dg2 = (\<lambda>x. x *\<^sub>R i + g2(x) *\<^sub>R j) ` ((cbox a b) \<union> (cbox b a))"
    using image_def by auto
  have 
    g1_path_continuous: "continuous_on ((cbox a b) \<union> (cbox b a)) (\<lambda>x. x *\<^sub>R i + g1(x) *\<^sub>R j)" and
    g2_path_continuous: "continuous_on ((cbox a b) \<union> (cbox b a)) (\<lambda>x. x *\<^sub>R i + g2(x) *\<^sub>R j)"
    by (intro continuous_intros g1_g2_continuous)+
  have field_cont_on_gamma1_image:
    "continuous_on ?Dg2  (F\<^bsub>i\<^esub>)"
    apply (rule continuous_on_subset [OF F_first_is_continuous])
    apply (auto simp add: Dy_def)
    by fastforce+
  have field_cont_on_gamma3_image:
    "continuous_on ?Dg1  (F\<^bsub>i\<^esub>)"
    apply (intro continuous_on_subset [OF F_first_is_continuous])
    apply (auto simp add: Dy_def)
    by fastforce+
  have gamma1_is_compos_of_scal_and_g2: "\<gamma>1 = (\<lambda>x. x *\<^sub>R i + g2(x) *\<^sub>R j) \<circ> (linepath a b)" and
    gamma3_is_compos_of_scal_and_g2: "\<gamma>3 = (\<lambda>x. x *\<^sub>R i + g1(x) *\<^sub>R j) \<circ> (linepath a b)"
    unfolding gamma1_def gamma3_def by (auto simp add: o_def Let_def)
  have add_scale_img:
    "(linepath a b) ` {0 .. 1} = ((cbox a b) \<union> (cbox b a))" using linepath_img' and a_lt_b by auto
  then have Dg2_is_gamma1_pathimg: "path_image \<gamma>1 = ?Dg2" and Dg1_is_gamma3_pathimg: "path_image \<gamma>3 = ?Dg1"
    by (metis (no_types, lifting) box_real(2) gamma1_is_compos_of_scal_and_g2 gamma3_is_compos_of_scal_and_g2 image_comp line_is_pair_img path_image_def)+
  have gamma1_as_euclid_space_fun: "\<gamma>1 = (\<lambda>x. let c = linepath a b x in c *\<^sub>R i + g2 c *\<^sub>R j)" and
       gamma3_as_euclid_space_fun: "\<gamma>3 = (\<lambda>x. let c = linepath a b x in c *\<^sub>R i + g1 c *\<^sub>R j)"
    using gamma1_def gamma3_def by auto
  have 0: "\<integral>\<^bsub>\<gamma>1\<^esub> F \<downharpoonright>\<^bsub>{i}\<^esub> = integral (cbox a b) (\<lambda>x. F(x *\<^sub>R i + g2(x) *\<^sub>R j) \<bullet> i ) - integral (cbox b a) (\<lambda>x. F(x *\<^sub>R i + g2(x) *\<^sub>R j) \<bullet> i )"
          "line_integral_exists F {i} \<gamma>1"
    using line_integral_on_pair_path_strong' [OF i_norm_1 _ gamma1_as_euclid_space_fun[unfolded Let_def], of "F"]
    using gamma1_def gamma1_smooth
    using g2_scale_j_contin add_scale_img
    using Dg2_is_gamma1_pathimg
    using field_cont_on_gamma1_image
     by (auto simp: pathstart_def pathfinish_def inner_commute algebra_simps Let_def linepath_def)
    then show "(line_integral_exists F {i} \<gamma>1)" by metis
  have 2: "\<integral>\<^bsub>\<gamma>3\<^esub> F \<downharpoonright>\<^bsub>{i}\<^esub> = integral (cbox a b) (\<lambda>x. F(x *\<^sub>R i + g1(x) *\<^sub>R j) \<bullet> i ) - integral (cbox b a) (\<lambda>x. F(x *\<^sub>R i + g1(x) *\<^sub>R j) \<bullet> i )"
          "line_integral_exists F {i} \<gamma>3"
    using line_integral_on_pair_path_strong' [OF i_norm_1 _ gamma3_as_euclid_space_fun[unfolded Let_def], of "F"] 
    using gamma3_def gamma3_smooth
    using g1_scale_j_contin add_scale_img
    using Dg1_is_gamma3_pathimg
    using field_cont_on_gamma3_image
     by (auto simp: pathstart_def pathfinish_def inner_commute algebra_simps Let_def linepath_def)
  then show "(line_integral_exists F {i} \<gamma>3)" by metis
  have gamma2_x_const: "\<forall>x. \<gamma>2 x \<bullet> i = b" and
       gamma4_x_const: "\<forall>x. \<gamma>4 x \<bullet> i = a"
    by (simp add: algebra_simps i_j_inner_zero i_inner_unit inner_commute gamma2_def gamma4_def)+
  have 1: "(\<integral>\<^bsub>\<gamma>2\<^esub> F\<downharpoonright>\<^bsub>{i}\<^esub>) = 0" "(line_integral_exists F {i} \<gamma>2)" and
       3: "(\<integral>\<^bsub>\<gamma>4\<^esub> F\<downharpoonright>\<^bsub>{i}\<^esub>) = 0" "(line_integral_exists F {i} \<gamma>4)"
    using line_integral_on_pair_straight_path[OF gamma2_x_const] line_integral_on_pair_straight_path[OF gamma4_x_const] straight_path_diffrentiable_x gamma2_def gamma4_def
    by (auto simp add: mult.commute Let_def linepath_def)
  then show "(line_integral_exists F {i} \<gamma>2)" and "(line_integral_exists F {i} \<gamma>4)"
    by metis+      
  have contin: "continuous_on ((cbox a b) \<union> (cbox b a)) (\<lambda>x. F(x *\<^sub>R i + g2(x) *\<^sub>R j)  \<bullet> i)"
       "continuous_on ((cbox a b) \<union> (cbox b a)) (\<lambda>x. F(x *\<^sub>R i + g1(x) *\<^sub>R j)  \<bullet> i)" 
    using line_is_pair_img and g2_path_continuous and g1_path_continuous and field_cont_on_gamma1_image field_cont_on_gamma3_image
      Topological_Spaces.continuous_on_compose
    by auto
  have 6: "(\<lambda>x. F(x *\<^sub>R i + g2(x) *\<^sub>R j)  \<bullet> i) integrable_on (cbox a b)" "(\<lambda>x. F(x *\<^sub>R i + g2(x) *\<^sub>R j)  \<bullet> i) integrable_on (cbox b a)" and 
       7: "(\<lambda>x. F(x *\<^sub>R i + g1(x) *\<^sub>R j)  \<bullet> i) integrable_on (cbox a b)" "(\<lambda>x. F(x *\<^sub>R i + g1(x) *\<^sub>R j)  \<bullet> i) integrable_on (cbox b a)"
    by(intro integrable_continuous; auto intro: integrable_continuous contin continuous_on_subset)+
  have partial_deriv_one_d_integrable:
    "((\<lambda>y. ?F_b'(x *\<^sub>R i + y *\<^sub>R j)) has_integral 
            (if g2 x \<le> g1 x then F(x *\<^sub>R i + g1(x) *\<^sub>R j) \<bullet> i - F(x *\<^sub>R i + g2(x) *\<^sub>R j) \<bullet> i
     else (F(x *\<^sub>R i + g2(x) *\<^sub>R j) \<bullet> i - F(x *\<^sub>R i + g1(x) *\<^sub>R j) \<bullet> i))) ({(g2 x) .. (g1 x)} \<union> {(g1 x) .. (g2 x)})"
     if "x \<in> {a .. b} \<union> {b .. a}" for x
  proof (cases "g2 x \<le> g1 x")
    case True
    then have *: "{(g2 x) .. (g1 x)} \<union> {(g1 x) .. (g2 x)} = cbox (g2 x) (g1 x)" by auto
    have **: "(x *\<^sub>R i) \<bullet> j = 0" using i_j_inner_zero by auto
    have "{p. \<exists>x' y. p = x' *\<^sub>R i + y *\<^sub>R j \<and> y \<in> (cbox (g2 x) (g1 x) \<union> cbox (g1 x) (g2 x)) \<and> x' = x} \<subseteq> Dy_pair"
      using that by (auto simp add: Dy_def)
    then have i: "{v. \<exists>xa\<ge>g2 x. xa \<le> g1 x \<and> v = xa *\<^sub>R j + x *\<^sub>R i} \<subseteq> Dy_pair"
      by (auto simp add:  Groups.ab_semigroup_add_class.add.commute)
    show ?thesis
      using fundamental_theorem_of_calculus_partial_vector_gen[OF True j_inner_unit ** F_partially_differentiable i] 
      unfolding *
      by (auto simp add:  Groups.ab_semigroup_add_class.add.commute)
  next
    case False
    then have *: "cbox (g2 x) (g1 x) \<union> cbox (g1 x) (g2 x) = cbox (g1 x) (g2 x)" by auto
    have **: "(x *\<^sub>R i) \<bullet> j = 0" using i_j_inner_zero by auto
    have ***: "g1 x \<le> g2 x" using False by auto
    have "{p. \<exists>x' y. p = x' *\<^sub>R i + y *\<^sub>R j \<and> y \<in> (cbox (g2 x) (g1 x) \<union> cbox (g1 x) (g2 x)) \<and> x' = x} \<subseteq> Dy_pair"
      using that by (auto simp add: Dy_def)
    then have i: "{v. \<exists>xa\<ge>g1 x. xa \<le> g2 x \<and> v = xa *\<^sub>R j + x *\<^sub>R i} \<subseteq> Dy_pair"
      by (auto simp add:  Groups.ab_semigroup_add_class.add.commute)
    show ?thesis
      using fundamental_theorem_of_calculus_partial_vector_gen[OF *** j_inner_unit ** F_partially_differentiable i] ***
      unfolding * 
      by (auto simp add: Groups.ab_semigroup_add_class.add.commute)
  qed
  have partial_deriv_integrable: "(?F_b') integrable_on Dy_pair"
    using f_lesbegue_integrable integrable_on_lborel integrable_restrict_UNIV by blast
  have 4: "integral Dy_pair ?F_b'
           = integral {a .. b} (\<lambda>x. integral ({(g2 x) .. (g1 x)} \<union> {(g1 x) .. (g2 x)}) (\<lambda>y. ?F_b' (x *\<^sub>R i + y *\<^sub>R j)))
             + integral {b .. a} (\<lambda>x. integral ({(g2 x) .. (g1 x)} \<union> {(g1 x) .. (g2 x)}) (\<lambda>y. ?F_b' (x *\<^sub>R i + y *\<^sub>R j)))"
  proof -
    have x_axis_gauge_integrable: "(\<lambda>y. ?f(y *\<^sub>R j + x *\<^sub>R i)) integrable_on UNIV" for x
    proof -
      have "\<forall>x. x \<notin> {a .. b} \<union> {b .. a} \<longrightarrow>  (\<lambda>y. ?f (x *\<^sub>R i + y *\<^sub>R j)) = (\<lambda>y. 0)"
        apply (auto simp add: Dy_def)
        using i_inner_unit j_inner_unit i_j_inner_zero sum_on_base_2 by fastforce+
      then have f_integrable_x_not_in_range:
        "\<forall>x. x \<notin> (cbox a b \<union> cbox b a) \<longrightarrow>  (\<lambda>y. ?f (x *\<^sub>R i + y *\<^sub>R j)) integrable_on UNIV"
        by (simp add: integrable_0)
      let ?F_b'_oneD = "(\<lambda>x. (\<lambda>y. if y \<in> (cbox (g2 x) ( g1 x)) \<union> (cbox (g1 x) ( g2 x)) then ?F_b' (x *\<^sub>R i + y *\<^sub>R j) else 0))"
      have f_value_x_in_range: "\<forall>x \<in> (cbox a b \<union> cbox b a). ?F_b'_oneD x = (\<lambda>y. ?f(x *\<^sub>R i + y *\<^sub>R j))"
        apply (auto simp add: Dy_def)
        using sum_on_base_2[OF _ i_j_inner_zero i_inner_unit j_inner_unit] by fastforce+
      have "\<forall>x \<in> {a .. b} \<union> {b .. a}. ?F_b'_oneD x integrable_on UNIV"
      (*This is the output of sledgehammer, NEEDS OPTIMISATION!*)
      proof -
        { fix rr :: real
          { assume "(\<lambda>r. (\<partial> F\<^bsub>i\<^esub> / \<partial> j |\<^bsub>rr *\<^sub>R i + r *\<^sub>R j\<^esub>)) integrable_on {g2 rr..g1 rr} \<union> {g1 rr..g2 rr}"
            then have "(\<lambda>r. if r \<in> {g2 rr..g1 rr} \<union> {g1 rr..g2 rr} then (\<partial> F\<^bsub>i\<^esub> / \<partial> j |\<^bsub>rr *\<^sub>R i + r *\<^sub>R j\<^esub>) else 0) integrable_on UNIV"
              using integrable_restrict_UNIV by fastforce
            then have "rr \<notin> {a..b} \<union> {b..a} \<or> (\<lambda>r. if r \<in> cbox (g2 rr) (g1 rr) \<union> cbox (g1 rr) (g2 rr) then (\<partial> F\<^bsub>i\<^esub> / \<partial> j |\<^bsub>rr *\<^sub>R i + r *\<^sub>R j\<^esub>) else 0) integrable_on UNIV"
              using box_real(2) by presburger }
          then have "rr \<notin> {a..b} \<union> {b..a} \<or> (\<lambda>r. if r \<in> cbox (g2 rr) (g1 rr) \<union> cbox (g1 rr) (g2 rr) then (\<partial> F\<^bsub>i\<^esub> / \<partial> j |\<^bsub>rr *\<^sub>R i + r *\<^sub>R j\<^esub>) else 0) integrable_on UNIV"
            using partial_deriv_one_d_integrable by blast }
        then show ?thesis
          by blast
      qed
      then have f_integrable_x_in_range:
        "\<forall>x. x \<in> (cbox a b \<union> cbox b a) \<longrightarrow>  (\<lambda>y. ?f (x *\<^sub>R i + y *\<^sub>R j)) integrable_on UNIV"
        using f_value_x_in_range by auto
      have *:"(\<lambda>y. ?f(x *\<^sub>R i + y *\<^sub>R j)) integrable_on UNIV" for x
        using f_integrable_x_not_in_range and f_integrable_x_in_range by auto
      show ?thesis
        apply(rule integrable_spike_finite[where f = "%y. ?f(x *\<^sub>R i + y *\<^sub>R j)" and S = "{}"], auto simp add: add.commute)
        using * apply(subst add.commute) by auto
    qed      
    have x_axis_integral_measurable: "(\<lambda>x. integral UNIV (\<lambda>y. ?f(y *\<^sub>R j + x *\<^sub>R i))) \<in> borel_measurable lborel"
    proof -
      have "(\<lambda>p. (?F_b' p) * indicator (Dy_pair) p) = (\<lambda>x. if x \<in> (Dy_pair) then (?F_b') x else 0)"
        using indic_ident[of "?F_b'"] by auto
      then have "\<And>x y. indicat_real (Dy_pair) (y *\<^sub>R j + x *\<^sub>R i) * ?F_b' (y *\<^sub>R j + x *\<^sub>R i) = (\<lambda>x. if x \<in> (Dy_pair) then (?F_b') x else 0) (y *\<^sub>R j + x *\<^sub>R i)"
        by auto
      then show ?thesis
        using analytically_valid_x[OF F_i_analytically_valid]
        by (auto simp add: indicator_def)
    qed
    have arg: "(\<lambda>a. if a \<in> Dy_pair then partial_vector_derivative (\<lambda>a. F a \<bullet> i) j a else 0) =
               (\<lambda>x. if x \<in> Dy_pair then if x \<in> Dy_pair then partial_vector_derivative (\<lambda>a. F a \<bullet> i) j x else 0 else 0)"
      by auto
    have arg2: " Dy_pair = {y *\<^sub>R j + x *\<^sub>R i |y x. x \<in> {a .. b} \<union> {b .. a} \<and> y \<in> {(g2 x) .. (g1 x)} \<union> {(g1 x) .. (g2 x)}}"
      unfolding Dy_def apply (auto simp add: add.commute) by force+
    have arg3: "\<And> x. x \<in> Dy_pair \<Longrightarrow> (\<lambda>x. if x \<in> Dy_pair then partial_vector_derivative (\<lambda>a. F a \<bullet> i) j x else 0) x
                           = (\<lambda>x. partial_vector_derivative (\<lambda>a. F a \<bullet> i) j x) x"
      by auto
    have arg4: "(\<lambda>x. integral ({(g2 x) .. (g1 x)} \<union> {(g1 x) .. (g2 x)}) (\<lambda>y. if (y *\<^sub>R j + x *\<^sub>R i) \<in> Dy_pair then partial_vector_derivative (\<lambda>a. F a \<bullet> i) j (y *\<^sub>R j + x *\<^sub>R i) else 0)) x =
                                      (\<lambda>x. integral ({(g2 x) .. (g1 x)} \<union> {(g1 x) .. (g2 x)}) (\<lambda>y. partial_vector_derivative (\<lambda>a. F a \<bullet> i) j (y *\<^sub>R j + x *\<^sub>R i))) x" if "x \<in> {a .. b}" for x
      apply(rule integral_cong)
      using atLeastAtMost_iff that apply (auto simp add: Dy_def)
      using pth_c(1) by blast+
    have arg5: "(\<lambda>x. integral ({(g2 x) .. (g1 x)} \<union> {(g1 x) .. (g2 x)}) (\<lambda>y. if (y *\<^sub>R j + x *\<^sub>R i) \<in> Dy_pair then partial_vector_derivative (\<lambda>a. F a \<bullet> i) j (y *\<^sub>R j + x *\<^sub>R i) else 0)) x =
                                      (\<lambda>x. integral ({(g2 x) .. (g1 x)} \<union> {(g1 x) .. (g2 x)}) (\<lambda>y. partial_vector_derivative (\<lambda>a. F a \<bullet> i) j (y *\<^sub>R j + x *\<^sub>R i))) x" if "x \<in> {b .. a}" for x
      apply(rule integral_cong)
      using atLeastAtMost_iff that apply (auto simp add: Dy_def)
      using pth_c(1) by blast+
    show ?thesis
      using integral_cong[OF arg3, of "Dy_pair" "(\<lambda>x. x)"] integral_cong[OF arg4, of "{a .. b}" "(\<lambda>x. x)"] integral_cong[OF arg5, of "{b .. a}" "(\<lambda>x. x)"]
      using bases_have_fubini[OF g2_leq_g1 f_lesbegue_integrable x_axis_gauge_integrable x_axis_integral_measurable arg arg2]
      by (smt Henstock_Kurzweil_Integration.integral_cong pth_c(1))
  qed
  have "(integral Dy_pair (\<lambda>a. (?F_b' a))
        = (integral {a .. b} (\<lambda>x. (if g2 x \<le> g1 x then ((F\<^bsub>i\<^esub>)) (x *\<^sub>R i + g1 x *\<^sub>R j) - ((F\<^bsub>i\<^esub>)) (x *\<^sub>R i + g2 x *\<^sub>R j) else ((F\<^bsub>i\<^esub>)) (x *\<^sub>R i + g2 x *\<^sub>R j) - ((F\<^bsub>i\<^esub>)) (x *\<^sub>R i + g1 x *\<^sub>R j))))
          + integral {b .. a} (\<lambda>x. (if g2 x \<le> g1 x then ((F\<^bsub>i\<^esub>)) (x *\<^sub>R i + g1 x *\<^sub>R j) - ((F\<^bsub>i\<^esub>)) (x *\<^sub>R i + g2 x *\<^sub>R j) else ((F\<^bsub>i\<^esub>)) (x *\<^sub>R i + g2 x *\<^sub>R j) - ((F\<^bsub>i\<^esub>)) (x *\<^sub>R i + g1 x *\<^sub>R j))))"
    unfolding 4
    using partial_deriv_one_d_integrable 
    by (blast intro: add_mono_thms_linordered_semiring(4) conjI integral_cong)+ 
  moreover have "(integral (cbox a b) (\<lambda>x. (if g2 x \<le> g1 x then ((F\<^bsub>i\<^esub>)) (x *\<^sub>R i + g1 x *\<^sub>R j) - ((F\<^bsub>i\<^esub>)) (x *\<^sub>R i + g2 x *\<^sub>R j) else ((F\<^bsub>i\<^esub>)) (x *\<^sub>R i + g2 x *\<^sub>R j) - ((F\<^bsub>i\<^esub>)) (x *\<^sub>R i + g1 x *\<^sub>R j))))
        = (if (\<forall>x \<in> (cbox a b \<union> cbox b a). (g2 x) \<le> (g1 x)) then integral (cbox a b) (\<lambda>x. ((F\<^bsub>i\<^esub>)) (x *\<^sub>R i + g1 x *\<^sub>R j) - ((F\<^bsub>i\<^esub>)) (x *\<^sub>R i + g2 x *\<^sub>R j)) else integral (cbox a b) (\<lambda>x. ((F\<^bsub>i\<^esub>)) (x *\<^sub>R i + g2 x *\<^sub>R j) - ((F\<^bsub>i\<^esub>)) (x *\<^sub>R i + g1 x *\<^sub>R j)))"
          "integral (cbox b a) (\<lambda>x. (if g2 x \<le> g1 x then ((F\<^bsub>i\<^esub>)) (x *\<^sub>R i + g1 x *\<^sub>R j) - ((F\<^bsub>i\<^esub>)) (x *\<^sub>R i + g2 x *\<^sub>R j) else ((F\<^bsub>i\<^esub>)) (x *\<^sub>R i + g2 x *\<^sub>R j) - ((F\<^bsub>i\<^esub>)) (x *\<^sub>R i + g1 x *\<^sub>R j))) = 
           (if (\<forall>x \<in> (cbox a b \<union> cbox b a). (g2 x) \<le> (g1 x)) then integral (cbox b a) (\<lambda>x. (F\<^bsub>i\<^esub>) (x *\<^sub>R i + g1 x *\<^sub>R j) - ((F\<^bsub>i\<^esub>)) (x *\<^sub>R i + g2 x *\<^sub>R j)) else integral (cbox b a) (\<lambda>x. ((F\<^bsub>i\<^esub>)) (x *\<^sub>R i + g2 x *\<^sub>R j) - ((F\<^bsub>i\<^esub>)) (x *\<^sub>R i + g1 x *\<^sub>R j)))"
    apply(auto split: if_splits)
    using g2_leq_g1
    by(intro integral_cong conjI; auto; metis Un_iff antisym_conv atLeastAtMost_iff)+
  ultimately have "(integral Dy_pair (\<lambda>a. (?F_b' a))
        = (if (\<forall>x \<in> (cbox a b \<union> cbox b a). (g2 x) \<le> (g1 x)) then integral (cbox a b) (\<lambda>x. ((F\<^bsub>i\<^esub>)) (x *\<^sub>R i + g1 x *\<^sub>R j) - ((F\<^bsub>i\<^esub>)) (x *\<^sub>R i + g2 x *\<^sub>R j)) else integral (cbox a b) (\<lambda>x. ((F\<^bsub>i\<^esub>)) (x *\<^sub>R i + g2 x *\<^sub>R j) - ((F\<^bsub>i\<^esub>)) (x *\<^sub>R i + g1 x *\<^sub>R j)))
          + (if (\<forall>x \<in> (cbox a b \<union> cbox b a). (g2 x) \<le> (g1 x)) then integral (cbox b a) (\<lambda>x. ((F\<^bsub>i\<^esub>)) (x *\<^sub>R i + g1 x *\<^sub>R j) - ((F\<^bsub>i\<^esub>)) (x *\<^sub>R i + g2 x *\<^sub>R j)) else integral (cbox b a) (\<lambda>x. ((F\<^bsub>i\<^esub>)) (x *\<^sub>R i + g2 x *\<^sub>R j) - ((F\<^bsub>i\<^esub>)) (x *\<^sub>R i + g1 x *\<^sub>R j))))"
    by auto
  then have 5: "integral Dy_pair (\<lambda>a. (?F_b' a))
        = (if (\<forall>x \<in> (cbox a b \<union> cbox b a). (g2 x) \<le> (g1 x)) then ((integral (cbox a b) (\<lambda>x. ((F\<^bsub>i\<^esub>)) (x *\<^sub>R i + g1 x *\<^sub>R j) - ((F\<^bsub>i\<^esub>)) (x *\<^sub>R i + g2 x *\<^sub>R j))) + integral (cbox b a) (\<lambda>x. ((F\<^bsub>i\<^esub>)) (x *\<^sub>R i + g1 x *\<^sub>R j) - ((F\<^bsub>i\<^esub>)) (x *\<^sub>R i + g2 x *\<^sub>R j)))
           else ((integral (cbox a b) (\<lambda>x. ((F\<^bsub>i\<^esub>)) (x *\<^sub>R i + g2 x *\<^sub>R j) - ((F\<^bsub>i\<^esub>)) (x *\<^sub>R i + g1 x *\<^sub>R j))) + integral (cbox b a) (\<lambda>x. ((F\<^bsub>i\<^esub>)) (x *\<^sub>R i + g2 x *\<^sub>R j) - ((F\<^bsub>i\<^esub>)) (x *\<^sub>R i + g1 x *\<^sub>R j))))"
    by auto
  have "\<integral>\<^bsub>\<gamma>1\<^esub> F \<downharpoonright>\<^bsub>{i}\<^esub> + \<integral>\<^bsub>\<gamma>2\<^esub> F \<downharpoonright>\<^bsub>{i}\<^esub> - \<integral>\<^bsub>\<gamma>3\<^esub> F \<downharpoonright>\<^bsub>{i}\<^esub> - \<integral>\<^bsub>\<gamma>4\<^esub> F \<downharpoonright>\<^bsub>{i}\<^esub>
           = (if a < b \<and> (\<forall>x \<in> ((cbox a b) \<union> (cbox b a)). (g2 x) \<le> (g1 x)) then integral Dy_pair (\<lambda>a. - (\<partial> (F\<^bsub>i\<^esub>)/ \<partial>j |\<^bsub>a\<^esub>))
              else if a < b \<and> (\<forall>x \<in> ((cbox a b) \<union> (cbox b a)). (g1 x) \<le> (g2 x)) then - integral Dy_pair (\<lambda>a. - (\<partial> (F\<^bsub>i\<^esub>)/ \<partial>j |\<^bsub>a\<^esub>)) 
              else if b < a \<and> (\<forall>x \<in> ((cbox a b) \<union> (cbox b a)). (g2 x) \<le> (g1 x)) then - integral Dy_pair (\<lambda>a. - (\<partial> (F\<^bsub>i\<^esub>)/ \<partial>j |\<^bsub>a\<^esub>)) 
              else integral Dy_pair (\<lambda>a. - (\<partial> (F\<^bsub>i\<^esub>)/ \<partial>j |\<^bsub>a\<^esub>)))"
        using "0"(1) "1"(1) "2"(1) "3"(1) 5 "6" "7"
        apply (simp add: Henstock_Kurzweil_Integration.integral_diff)
        using g2_leq_g1
        by (smt Henstock_Kurzweil_Integration.integral_cong Un_iff a_lt_b atLeastatMost_empty box_real(2) empty_iff)
  then show ?g 
    using g2_leq_g1 a_lt_b
    by(auto split: if_splits)
qed

end

type_synonym two_cube = "(real * real \<Rightarrow> real * real)"
type_synonym two_chain = "(int * two_cube) set"
fun typeI_twoCube where
"typeI_twoCube (orient::int, C) v1 v2 =
           (\<exists>a b g1 g2. a \<noteq> b \<and> ((\<forall>x \<in> ({a..b} \<union> {b..a}). g2 x \<le> g1 x) \<or> (\<forall>x \<in> ({a..b} \<union> {b..a}). g1 x \<le> g2 x)) \<and>
                       C = (\<lambda>(t1,t2). let c = (linepath a b t1) in (c *\<^sub>R v1 + linepath (g2 c) (g1 c) t2 *\<^sub>R v2)) \<and>
                       g1 piecewise_C1_differentiable_on ({a..b} \<union> {b..a}) \<and>
                       g2 piecewise_C1_differentiable_on ({a..b} \<union> {b..a}) \<and>
                       orient = (if a < b then 1 else -1) * (if \<forall>x\<in>{a..b} \<union> {b..a}. g2 x \<le> g1 x then 1 else - 1))"

declare typeI_twoCube.simps [simp del]

abbreviation unit_cube where "unit_cube \<equiv> cbox (0,0) (1::real,1::real)" 

definition cubeImage where
  "cubeImage twoC \<equiv> (twoC ` unit_cube)"

lemma typeI_twoCubeImg':
  assumes "typeI_twoCube (orient, C) v1 v2"
  shows "\<exists>a b g1 g2. a \<noteq> b \<and> ((\<forall>x \<in> ({a..b} \<union> {b..a}). g2 x \<le> g1 x) \<or> (\<forall>x \<in> ({a..b} \<union> {b..a}). g1 x \<le> g2 x)) \<and>
                      cubeImage C = {p. \<exists>x y. p = x *\<^sub>R v1 + y *\<^sub>R v2 \<and> x \<in> ({a..b} \<union> {b..a}) \<and> y \<in> ({g2 x .. g1 x} \<union> {g1 x .. g2 x})}
                      \<and> C = (\<lambda>(t1,t2). let c = (linepath a b t1) in (c *\<^sub>R v1 + linepath (g2 c) (g1 c) t2 *\<^sub>R v2))
                      \<and> g1 piecewise_C1_differentiable_on ({a..b} \<union> {b..a}) \<and> g2 piecewise_C1_differentiable_on ({a..b} \<union> {b..a}) \<and>
                      orient = (if a < b then 1 else -1) * (if \<forall>x\<in>{a..b} \<union> {b..a}. g2 x \<le> g1 x then 1 else - 1)" (is ?g)
proof-
  obtain a b g1 g2 where
    twoCisTypeII: "a \<noteq> b"
    "((\<forall>x \<in> ({a..b} \<union> {b..a}). g2 x \<le> g1 x) \<or> (\<forall>x \<in> ({a..b} \<union> {b..a}). g1 x \<le> g2 x))"
    "C = (\<lambda>(t1,t2). let c = (linepath a b t1) in (c *\<^sub>R v1 + linepath (g2 c) (g1 c) t2 *\<^sub>R v2))"
    "g1 piecewise_C1_differentiable_on ({a..b} \<union> {b..a})"
    "g2 piecewise_C1_differentiable_on ({a..b} \<union> {b..a})"
    "orient = (if a < b then 1 else -1) * (if \<forall>x\<in>{a..b} \<union> {b..a}. g2 x \<le> g1 x then 1 else - 1)"
    using assms
    by (auto simp add: cubeImage_def image_def typeI_twoCube.simps)
  define g2_g1 where "g2_g1 = (\<lambda>x. {g2 x..g1 x} \<union> {g1 x..g2 x})"
  have "cubeImage C = {p.\<exists>x y. p = x *\<^sub>R v1 + y *\<^sub>R v2 \<and>  x \<in> {a''..b''} \<and> y \<in> g2_g1 x}" if a_lt_b: "a'' < b''"
    and g2_le_g1: "((\<forall>x \<in> ({a''..b''} \<union> {b''..a''}). g2 x \<le> g1 x) \<or> (\<forall>x \<in> ({a''..b''} \<union> {b''..a''}). g1 x \<le> g2 x))"
    and twoc_spec: "C = (\<lambda>(t1,t2). let c = (linepath a'' b'' t1) in (c *\<^sub>R v1 + linepath (g2 c) (g1 c) t2 *\<^sub>R v2))" for a'' b''
  proof-
    have ab1: "a'' - x * a'' + x * b'' \<le> b''" if a1: "0 \<le> x" "x \<le> 1" for x
      using that apply (simp add: algebra_simps) using a_lt_b
      by (metis affine_ineq less_eq_real_def mult.commute )
    show ?thesis
      apply (auto simp add: twoc_spec ab1 left_diff_distrib linepath_def Let_def prod_cases cubeImage_def)
      subgoal  for a' b'
      proof-
        assume ass: "0 \<le> a'" "a' \<le> 1 " "0 \<le> b'" "b' \<le> 1"
        define pt where "pt = (a'' - a' * a'' + a' * b'')"
        have *: "a'' \<le> pt \<and> pt \<le> b''" using a_lt_b ass unfolding pt_def by sos+
        moreover have "(g2 pt - b' * g2 pt + b' * g1 pt) \<in> g2_g1 pt"
          unfolding g2_g1_def
          apply simp
          using ass
          by (smt affine_ineq mult_less_cancel_left_disj)+
        then show ?thesis using g2_le_g1
          using calculation pt_def by blast
      qed
      subgoal for c d proof-
        assume c_bounds: "a'' \<le> c" "c \<le> b''" and d_bounds: "d \<in> g2_g1 c"
        have b_minus_a_nzero: "b'' - a'' \<noteq> 0" using a_lt_b by auto
        have x_witness: "\<exists>k1. c = (1 - k1)*a'' + k1 * b'' \<and> 0 \<le> k1 \<and> k1 \<le> 1"
          apply (rule_tac x="(c - a'')/(b'' - a'')" in exI)
          using c_bounds a_lt_b apply (simp add: divide_simps algebra_simps)
          using sum_sqs_eq by blast
        have y_witness: "\<exists>k2. d = (1 - k2)*(g2 c) + k2 * (g1 c) \<and> 0 \<le> k2 \<and> k2 \<le> 1"
        proof (cases "g1 c - g2 c = 0")
          case True
          with d_bounds show ?thesis unfolding g2_g1_def by (fastforce simp add: algebra_simps)
        next
          case False
          let ?k2 = "(d - g2 c)/(g1 c - g2 c)"
          have k2_in_bounds: "0 \<le> ?k2 \<and> ?k2 \<le> 1" 
            using twoCisTypeII(2) c_bounds d_bounds False unfolding g2_g1_def
            by (auto simp add: divide_nonpos_nonpos)
          have "d = (1 - ?k2) * g2 c + ?k2 * g1 c" 
            using False sum_sqs_eq by (fastforce simp add: divide_simps algebra_simps)
          with k2_in_bounds show ?thesis 
            by fastforce
        qed
        show ?thesis
          using x_witness y_witness by (force simp add: left_diff_distrib linepath_def Let_def)
      qed
      done
  qed
  then have cube_img: "cubeImage C = {p.\<exists>x y. p = x *\<^sub>R v1 + y *\<^sub>R v2 \<and>  x \<in> {a''..b''}\<union>{b''..a''} \<and> y \<in> g2_g1 x}" if a_lt_b: "a'' < b''"
    and g2_le_g1: "(\<forall>x\<in>{a''..b''}\<union>{b''..a''}. g2 x \<le> g1 x) \<or> (\<forall>x\<in>{a''..b''}\<union>{b''..a''}. g1 x \<le> g2 x)"
    and twoc_spec: "C = (\<lambda>(t1,t2). let c = (linepath a'' b'' t1) in (c *\<^sub>R v1 + linepath (g2 c) (g1 c) t2 *\<^sub>R v2))" for a'' b''
    using a_lt_b g2_le_g1 twoc_spec
    by simp
  have cube_img2: "cubeImage C = {p.\<exists>x y. p = x *\<^sub>R v1 + y *\<^sub>R v2 \<and>  x \<in> {a''..b''} \<and> y \<in> g2_g1 x}" if a_lt_b: "a'' < b''"
    and g2_le_g1: "((\<forall>x \<in> ({a''..b''} \<union> {b''..a''}). g2 x \<le> g1 x) \<or> (\<forall>x \<in> ({a''..b''} \<union> {b''..a''}). g1 x \<le> g2 x))"
    and twoc_spec: "C = (\<lambda>(t1,t2). let c = (linepath b'' a'' t1) in (c *\<^sub>R v1 + linepath (g2 c) (g1 c) t2 *\<^sub>R v2))" for a'' b''
  proof-
    have ab1: "a'' - x * a'' + x * b'' \<le> b''" if a1: "0 \<le> x" "x \<le> 1" for x
      using that apply (simp add: algebra_simps) using a_lt_b
      by (metis affine_ineq less_eq_real_def mult.commute )
    show ?thesis
      apply (auto simp add: twoc_spec ab1 left_diff_distrib linepath_def Let_def prod_cases cubeImage_def)
      subgoal  for a' b'
      proof-
        assume ass: "0 \<le> a'" "a' \<le> 1 " "0 \<le> b'" "b' \<le> 1"
        define pt where "pt = (b'' - a' * b'' + a' * a'')"
        have *: "a'' \<le> pt \<and> pt \<le> b''" using a_lt_b ass unfolding pt_def by sos+
        moreover have "(g2 pt - b' * g2 pt + b' * g1 pt) \<in> g2_g1 pt"
          unfolding g2_g1_def
          apply simp
          using ass
          by (smt affine_ineq mult_less_cancel_left_disj)+
        then show ?thesis using g2_le_g1
          using calculation pt_def by blast
      qed
      subgoal for c d proof-
        assume c_bounds: "a'' \<le> c" "c \<le> b''" and d_bounds: "d \<in> g2_g1 c"
        have b_minus_a_nzero: "b'' - a'' \<noteq> 0" using a_lt_b by auto
        have x_witness: "\<exists>k1. c = (1 - k1)*b'' + k1 * a'' \<and> 0 \<le> k1 \<and> k1 \<le> 1"
          apply (rule_tac x="(c - b'')/(a'' - b'')" in exI)
          using c_bounds a_lt_b apply (simp add: divide_simps algebra_simps)
          using sum_sqs_eq by blast
        have y_witness: "\<exists>k2. d = (1 - k2)*(g2 c) + k2 * (g1 c) \<and> 0 \<le> k2 \<and> k2 \<le> 1"
        proof (cases "g1 c - g2 c = 0")
          case True
          with d_bounds show ?thesis unfolding g2_g1_def by (fastforce simp add: algebra_simps)
        next
          case False
          let ?k2 = "(d - g2 c)/(g1 c - g2 c)"
          have k2_in_bounds: "0 \<le> ?k2 \<and> ?k2 \<le> 1" 
            using twoCisTypeII(2) c_bounds d_bounds False unfolding g2_g1_def
            by (auto simp add: divide_nonpos_nonpos)
          have "d = (1 - ?k2) * g2 c + ?k2 * g1 c" 
            using False sum_sqs_eq by (fastforce simp add: divide_simps algebra_simps)
          with k2_in_bounds show ?thesis 
            by fastforce
        qed
        show ?thesis
          using x_witness y_witness by (force simp add: left_diff_distrib linepath_def Let_def)
      qed
      done
  qed
  then have cube_img2: "cubeImage C = {p.\<exists>x y. p = x *\<^sub>R v1 + y *\<^sub>R v2 \<and>  x \<in> {a''..b''}\<union>{b''..a''} \<and> y \<in> g2_g1 x}" if a_lt_b: "a'' < b''"
    and g2_le_g1: "(\<forall>x\<in>{a''..b''}\<union>{b''..a''}. g2 x \<le> g1 x) \<or> (\<forall>x\<in>{a''..b''}\<union>{b''..a''}. g1 x \<le> g2 x)"
    and twoc_spec: "C = (\<lambda>(t1,t2). let c = (linepath b'' a'' t1) in (c *\<^sub>R v1 + linepath (g2 c) (g1 c) t2 *\<^sub>R v2))" for a'' b''
    using a_lt_b g2_le_g1 twoc_spec
    by simp
  show ?thesis
  proof(cases "a < b")
    case True
    show ?thesis
      using cube_img[unfolded g2_g1_def, OF True twoCisTypeII(2,3)] using twoCisTypeII(1,2,3,4,5,6) by blast
  next
    case False
    then have b_lt_a: "b < a" and a_b_emty: "{a ..b} = {}" using twoCisTypeII(1) by auto
    show ?thesis
      using cube_img2[unfolded g2_g1_def, OF b_lt_a twoCisTypeII(2,3)[unfolded Un_commute[of "{a ..b}" "{b ..a}"]]] twoCisTypeII(1,2,3,4,5,6) by blast
  qed
qed

lemma typeI_cube_explicit_spec':
  assumes "typeI_twoCube (orient, C) v1 v2"
  shows "\<exists>a b g1 g2. a \<noteq> b \<and>  ((\<forall>x\<in>{a..b} \<union> {b..a}. g2 x \<le> g1 x) \<or> (\<forall>x\<in>{a..b} \<union> {b..a}. g1 x \<le> g2 x)) \<and>
                     cubeImage C = {p. \<exists>x y. p = x *\<^sub>R v1 + y *\<^sub>R v2 \<and> x \<in> {a..b} \<union> {b..a} \<and> y \<in> {g2 x..g1 x} \<union> {g1 x..g2 x}}
                  \<and> C = (\<lambda>(t1, t2). let c = linepath a b t1 in c *\<^sub>R v1 + linepath (g2 c) (g1 c) t2 *\<^sub>R v2)
                  \<and> g1 piecewise_C1_differentiable_on {a..b} \<union> {b..a} \<and> g2 piecewise_C1_differentiable_on {a..b} \<union> {b..a}
                  \<and> (\<lambda>x. C(x, 0)) = (\<lambda>x. let c = linepath a b x in c *\<^sub>R v1 + (g2 c) *\<^sub>R v2)
                  \<and> (\<lambda>y. C(1, y)) = (\<lambda>x. b *\<^sub>R v1 + (linepath (g2 b) (g1 b) x) *\<^sub>R v2)
                  \<and> (\<lambda>x. C(x, 1)) = (\<lambda>x. let c = linepath a b x in c *\<^sub>R v1  + g1 c *\<^sub>R v2)
                  \<and> (\<lambda>y. C(0, y)) = (\<lambda>x. a *\<^sub>R v1 + (linepath (g2 a) (g1 a) x) *\<^sub>R v2)
                  \<and> orient = (if a < b then 1 else - 1) * (if \<forall>x\<in>{a..b} \<union> {b..a}. g2 x \<le> g1 x then 1 else - 1)" (is ?g1)
        "orient = 1 \<or> orient = -1" (is ?g2)
proof-
  obtain a b g2 g1 where C:
       "a \<noteq> b"
       "((\<forall>x\<in>{a..b} \<union> {b..a}. g2 x \<le> g1 x) \<or> (\<forall>x\<in>{a..b} \<union> {b..a}. g1 x \<le> g2 x))"
       "cubeImage C = {p. \<exists>x y. p = x *\<^sub>R v1 + y *\<^sub>R v2 \<and> x \<in> {a..b} \<union> {b..a} \<and> y \<in> {g2 x..g1 x} \<union> {g1 x..g2 x}}"
       "C = (\<lambda>(t1, t2). let c = linepath a b t1 in c *\<^sub>R v1 + linepath (g2 c) (g1 c) t2 *\<^sub>R v2)"
       "g1 piecewise_C1_differentiable_on {a..b} \<union> {b..a} \<and> g2 piecewise_C1_differentiable_on {a..b} \<union> {b..a}"
       "orient = (if a < b then 1 else - 1) * (if \<forall>x\<in>{a..b} \<union> {b..a}. g2 x \<le> g1 x then 1 else - 1)"
       "(\<lambda>y. C(0, y)) = (\<lambda>x. a *\<^sub>R v1 + (linepath (g2 a) (g1 a) x) *\<^sub>R v2)"
       "(\<lambda>x. C(x, 0)) = (\<lambda>x. let c = linepath a b x in c *\<^sub>R v1 + (g2 c) *\<^sub>R v2)"
       "(\<lambda>y. C(1, y)) = (\<lambda>x. b *\<^sub>R v1 + (linepath (g2 b) (g1 b) x) *\<^sub>R v2)"
       "(\<lambda>x. C(x, 1)) = (\<lambda>x. let c = linepath a b x in c *\<^sub>R v1  + g1 c *\<^sub>R v2)"
  using typeI_twoCubeImg'[OF assms] 
  by (auto simp add:  linepath_def algebra_simps)
  then show ?g1 by blast
  show ?g2 using C(1,2,6) by simp 
qed

lemma typeI_cube_explicit_spec'_prod:
  assumes "typeI_twoCube twoC v1 v2"
  shows "\<exists>a b g1 g2. a \<noteq> b \<and>  ((\<forall>x\<in>{a..b} \<union> {b..a}. g2 x \<le> g1 x) \<or> (\<forall>x\<in>{a..b} \<union> {b..a}. g1 x \<le> g2 x)) \<and>
                     cubeImage (snd twoC) = {p. \<exists>x y. p = x *\<^sub>R v1 + y *\<^sub>R v2 \<and> x \<in> {a..b} \<union> {b..a} \<and> y \<in> {g2 x..g1 x} \<union> {g1 x..g2 x}}
                  \<and> snd twoC = (\<lambda>(t1, t2). let c = linepath a b t1 in c *\<^sub>R v1 + linepath (g2 c) (g1 c) t2 *\<^sub>R v2)
                  \<and> g1 piecewise_C1_differentiable_on {a..b} \<union> {b..a} \<and> g2 piecewise_C1_differentiable_on {a..b} \<union> {b..a}
                  \<and> (\<lambda>x. snd twoC(x, 0)) = (\<lambda>x. let c = linepath a b x in c *\<^sub>R v1 + (g2 c) *\<^sub>R v2)
                  \<and> (\<lambda>y. snd twoC(1, y)) = (\<lambda>x. b *\<^sub>R v1 + (linepath (g2 b) (g1 b) x) *\<^sub>R v2)
                  \<and> (\<lambda>x. snd twoC(x, 1)) = (\<lambda>x. let c = linepath a b x in c *\<^sub>R v1  + g1 c *\<^sub>R v2)
                  \<and> (\<lambda>y. snd twoC(0, y)) = (\<lambda>x. a *\<^sub>R v1 + (linepath (g2 a) (g1 a) x) *\<^sub>R v2)
                  \<and> fst twoC = (if a < b then 1 else - 1) * (if \<forall>x\<in>{a..b} \<union> {b..a}. g2 x \<le> g1 x then 1 else - 1)" (is ?g1)
  using assms
  apply(cases twoC, simp only: snd_def case_prod_conv)
  subgoal for orient C using typeI_cube_explicit_spec'[of orient C] by force
  done

lemma imp_andI: "(\<exists>x. P \<longrightarrow> R \<and> Q x) \<Longrightarrow> (P \<longrightarrow> R \<and> (\<exists>x. Q x))" by auto

fun horizontal_boundary::"two_cube \<Rightarrow> one_chain" where
  "horizontal_boundary C = {(1, (\<lambda>x. C(x,0))), (-1, (\<lambda>x. C(x,1)))}"

lemma horizontal_boundary_prod: "horizontal_boundary twoC = {(1, (\<lambda>x. twoC(x,0))), (-1, (\<lambda>x. twoC(x,1)))}"
  by (metis snd_conv horizontal_boundary.elims)

fun vertical_boundary::"two_cube \<Rightarrow> one_chain" where
  "vertical_boundary C = {(-1, (\<lambda>y. C(0,y))), (1, (\<lambda>y. C(1,y)))}"

lemma vertical_boundary_prod: "vertical_boundary twoC = {((-1::int), (\<lambda>y. twoC(0,y))), (1, (\<lambda>y. twoC(1,y)))}"
  by (metis snd_conv vertical_boundary.elims)

definition boundary::"two_cube \<Rightarrow> one_chain" where
  "boundary C \<equiv> horizontal_boundary C \<union> vertical_boundary C"

notation boundary ("\<partial> _" 200)

lemma typeI_twoCube_smooth_edges:
  assumes "typeI_twoCube (orient, C) v1 v2" "(k,\<gamma>) \<in> (\<partial> C)"
  shows "\<gamma> piecewise_C1_differentiable_on {0..1}"
proof -
  let ?bottom_edge = "(\<lambda>x. C(x, 0))"
  let ?right_edge = "(\<lambda>y. C(1, y))"
  let ?top_edge = "(\<lambda>x. C(x, 1))"
  let ?left_edge = "(\<lambda>y. C(0, y))"
  obtain a b g1 g2 where
    twoCisTypeII: "a \<noteq> b"
      "((\<forall>x\<in>{a..b} \<union> {b..a}. g2 x \<le> g1 x) \<or> (\<forall>x\<in>{a..b} \<union> {b..a}. g1 x \<le> g2 x))"
      "cubeImage C = {x *\<^sub>R v1 + y *\<^sub>R v2 |x y. x \<in> {a..b} \<union> {b..a} \<and> y \<in> {g2 x..g1 x} \<union> {g1 x..g2 x}}"
      "C = (\<lambda>(t1, t2). let c = linepath a b t1 in c *\<^sub>R v1 + linepath (g2 c) (g1 c) t2 *\<^sub>R v2)"
      "g1 piecewise_C1_differentiable_on {a..b} \<union> {b..a} "
      "g2 piecewise_C1_differentiable_on {a..b} \<union> {b..a} "
      "(\<lambda>x. C(x, 0)) = (\<lambda>x. let c = linepath a b x in c *\<^sub>R v1 + (g2 c) *\<^sub>R v2)"
      "(\<lambda>y. C(1, y)) = (\<lambda>x. b *\<^sub>R v1 + (linepath (g2 b) (g1 b) x) *\<^sub>R v2)"
      "(\<lambda>x. C(x, 1)) = (\<lambda>x. let c = linepath a b x in c *\<^sub>R v1  + g1 c *\<^sub>R v2)"
      "(\<lambda>y. C(0, y)) = (\<lambda>x. a *\<^sub>R v1 + (linepath (g2 a) (g1 a) x) *\<^sub>R v2)"
      "orient = (if a < b then 1 else - 1) * (if \<forall>x\<in>{a..b} \<union> {b..a}. g2 x \<le> g1 x then 1 else - 1)"
    using assms and typeI_cube_explicit_spec'[of orient C v1 v2]
    by blast
  have bottom_top_edge_smooth: "?bottom_edge piecewise_C1_differentiable_on {0..1}" (is ?g1) "?top_edge piecewise_C1_differentiable_on {0..1}" (is ?g2)
  proof -
    have finite_vimg: "\<And>x. finite({0..1} \<inter> (linepath a b)-` {x})" using linepath_vimage_singleton'[OF twoCisTypeII(1)] by auto
    have g2_smooth: "(%x. g2 x *\<^sub>R v2) piecewise_C1_differentiable_on (linepath a b) ` {0..1}"
                    "(%x. g1 x *\<^sub>R v2) piecewise_C1_differentiable_on (linepath a b) ` {0..1}"
      unfolding linepath_img'[OF twoCisTypeII(1)] by (auto intro!: piecewise_C1_differentiable_on_scaleR twoCisTypeII(6) twoCisTypeII(5))
    have "(\<lambda>x. g2 (linepath a b x) *\<^sub>R v2) piecewise_C1_differentiable_on {0..1}"
         "(\<lambda>x. g1 (linepath a b x) *\<^sub>R v2) piecewise_C1_differentiable_on {0..1}"
      using piecewise_C1_differentiable_compose[OF linepath_C1_diff _ finite_vimg] g2_smooth
      by (auto simp add: o_def)
    then show ?g1 ?g2 unfolding twoCisTypeII(7,9)
      by (auto simp add: Let_def algebra_simps intro!: derivative_eq_intros piecewise_C1_differentiable_add piecewise_C1_differentiable_on_scaleR linepath_C1_diff)
  qed
  moreover have right_left_edge_smooth: "?right_edge piecewise_C1_differentiable_on {0..1}" (is ?g1) "?left_edge piecewise_C1_differentiable_on {0..1}" (is ?g2)
      unfolding twoCisTypeII(8) twoCisTypeII(10)
      by(auto intro!: derivative_eq_intros linepath_C1_diff piecewise_C1_differentiable_add piecewise_C1_differentiable_on_scaleR)
  moreover have "\<gamma> = ?bottom_edge \<or> \<gamma> = ?right_edge \<or> \<gamma> = ?top_edge \<or> \<gamma> = ?left_edge"
    using assms by (auto simp add: boundary_def)
  ultimately show ?thesis
    by auto
qed

lemma typeI_twoCube_smooth_edges_prod:
  assumes "typeI_twoCube twoCube v1 v2" "(k,\<gamma>) \<in> \<partial> (snd twoCube)"
  shows "\<gamma> piecewise_C1_differentiable_on {0..1}"
  using assms typeI_twoCube_smooth_edges
  by (metis eq_snd_iff)

lemma two_cube_hor_boundary_is_boundary: "boundary_chain (horizontal_boundary C)"
  using typeI_cube_explicit_spec'(2)
  by (auto simp add: boundary_chain_def boundary_def)

lemma two_cube_vert_boundary_is_boundary: "boundary_chain (vertical_boundary C)"
  using typeI_cube_explicit_spec'(2)
  by (auto simp add: boundary_chain_def boundary_def)

lemma two_cube_boundary_is_boundary: "boundary_chain (\<partial> C)"
  using typeI_cube_explicit_spec'(2)
  by (auto simp add: boundary_chain_def boundary_def)

lemma finite_boundary: "finite (\<partial> C)"
  unfolding boundary_def by auto

definition valid_two_cube where
  "valid_two_cube twoC \<equiv> card (boundary twoC) = 4"

lemma common_boundary_sudivision_exists_refl_2:
  assumes "\<forall>(k,\<gamma>)\<in>boundary twoC. valid_path \<gamma>"
  shows "common_boundary_sudivision_exists (boundary twoC) (boundary twoC)"
  by (metis assms common_boundary_sudivision_exists_refl_1 two_cube_boundary_is_boundary horizontal_boundary.cases)+

lemma gen_common_boundary_sudivision_exists_refl_twochain_boundary:
  assumes "\<forall>(k,\<gamma>)\<in>C. valid_path \<gamma>"
    "boundary_chain (C::(int \<times> (real \<Rightarrow> real \<times> real)) set)"
  shows "common_subdiv_exists (C) (C)"
  using assms chain_subdiv_chain_refl common_boundary_sudivision_exists_def common_subdiv_imp_gen_common_subdiv' by blast

lemma typeI_edges_are_valid_paths:
  assumes "typeI_twoCube twoC v1 v2" "(k,\<gamma>) \<in> (\<partial> snd twoC)"
  shows "valid_path \<gamma>"
  using typeI_twoCube_smooth_edges_prod[OF assms] C1_differentiable_imp_piecewise
  by (auto simp: valid_path_def)

lemma typeI_edges_are_valid_paths_prod:
  assumes "typeI_twoCube (orient,C) v1 v2" "(k,\<gamma>) \<in> \<partial> C"
  shows "valid_path \<gamma>"
  using typeI_twoCube_smooth_edges[OF assms] C1_differentiable_imp_piecewise
  by (auto simp: valid_path_def)

lemma boundary_image_subset_cube_image:
  assumes "(k,\<gamma>) \<in> \<partial> C"
  shows "(path_image \<gamma>) \<subseteq> cubeImage C"
  using assms
  unfolding boundary_def cubeImage_def path_image_def image_def subset_iff
  by auto

lemma field_cont_on_region_cont_on_edges:
  assumes
    field_cont:
    "continuous_on (cubeImage C) F" and
    member_of_boundary:
    "(k,\<gamma>) \<in> \<partial> C"
  shows "continuous_on (path_image \<gamma>) F"
  using boundary_image_subset_cube_image assms continuous_on_subset
  by metis

locale green_typeI_cube =  i_j_orthonorm + 
  fixes C and orient and F::"real * real \<Rightarrow> real * real" (*If these real pairs are to be generalised to higher dimensions, then we need to generalise the boundary operator to take cubes of higher dimensions.*)
  assumes 
    two_cube: "typeI_twoCube (orient, C) i j" and
    valid_two_cube: "valid_two_cube C" and
    f_analytically_valid: "analytically_valid (cubeImage C) (F\<^bsub>i\<^esub>) j i"
    (*
      To generalise this locale to take vectors i j that are euclidean space we need to generalise:
      - boundaries to take arbitrary dimensions
      *)
begin

lemma GreenThm_typeI_twoCube:
  shows "orient * integral (cubeImage C) (%p. - \<partial> F\<^bsub>i\<^esub> / \<partial> j |\<^bsub>p\<^esub>) = \<^sup>H\<ointegral>\<^bsub>\<partial>C\<^esub> F\<downharpoonright>\<^bsub>{i}\<^esub>" (is ?g1)
    "\<forall>(k,\<gamma>) \<in> \<partial>C. line_integral_exists F {i} \<gamma>" (is ?g2)
proof -
  let ?bot_edge = "(\<lambda>x. C(x, 0))"
  let ?right_edge = "(\<lambda>y. C(1, y))"
  let ?top_edge = "(\<lambda>x. C(x, 1))"
  let ?left_edge = "(\<lambda>y. C(0, y))"
  obtain a b g1 g2 where a_b_g1_g2: "
       cubeImage C = {x *\<^sub>R i + y *\<^sub>R j |x y. x \<in> {a .. b} \<union> {b .. a} \<and> y \<in> {(g2 x) .. (g1 x)} \<union> {(g1 x) .. (g2 x)}} "
       "a \<noteq> b""
       ((\<forall>x\<in>{a..b} \<union> {b..a}. g2 x \<le> g1 x) \<or> (\<forall>x\<in>{a..b} \<union> {b..a}. g1 x \<le> g2 x))""
       C = (\<lambda>(t1, t2). let c = linepath a b t1 in c *\<^sub>R i + linepath (g2 c) (g1 c) t2 *\<^sub>R j) ""
       g1 piecewise_C1_differentiable_on ({a..b} \<union> {b..a})""
       g2 piecewise_C1_differentiable_on ({a..b} \<union> {b..a}) " and
       bot_edge: " (\<lambda>x. C (x, 0)) = (\<lambda>x. let c = linepath a b x in c *\<^sub>R i + g2 c *\<^sub>R j)" and
       right_edge: "(\<lambda>y. C (1, y)) = (\<lambda>x. b *\<^sub>R i + linepath (g2 b) (g1 b) x *\<^sub>R j)" and
       top_edge: "(\<lambda>x. C (x, 1)) = (\<lambda>x. let c = linepath a b x in c *\<^sub>R i + g1 c *\<^sub>R j)" and
       left_edge: "(\<lambda>y. C (0, y)) = (\<lambda>x. a *\<^sub>R i + linepath (g2 a) (g1 a) x *\<^sub>R j)" and
       orientation: "orient = (if a < b then 1 else - 1) * (if \<forall>x\<in>{a..b} \<union> {b..a}. g2 x \<le> g1 x then 1 else - 1)"
    using typeI_cube_explicit_spec'[OF two_cube] by auto
  moreover have line_integral_around_boundary: 
    "\<^sup>H\<integral>\<^bsub>\<partial>C\<^esub> F \<downharpoonright>\<^bsub>b\<^esub> =  \<integral>\<^bsub>?bot_edge\<^esub> F \<downharpoonright>\<^bsub>b\<^esub>  + \<integral>\<^bsub>?right_edge\<^esub> F \<downharpoonright>\<^bsub>b\<^esub> - \<integral>\<^bsub>?top_edge\<^esub> F \<downharpoonright>\<^bsub>b\<^esub> - \<integral>\<^bsub>?left_edge\<^esub> F \<downharpoonright>\<^bsub>b\<^esub>" for b
    unfolding one_chain_line_integral_def
    using valid_two_cube
    by (auto simp: card_insert_if boundary_def valid_two_cube_def split: if_split_asm)
  moreover have bot_top_edges_smooth: "?bot_edge piecewise_C1_differentiable_on {0..1}" (is ?p1) "?top_edge piecewise_C1_differentiable_on {0..1}" (is ?p2)
    using typeI_twoCube_smooth_edges[OF two_cube]
    by (auto simp add: boundary_def)
  show ?g1 ?g2
    using GreenThm_type_I[OF a_b_g1_g2(1) bot_edge bot_top_edges_smooth(1) right_edge top_edge bot_top_edges_smooth(2) left_edge f_analytically_valid ] a_b_g1_g2(3) a_b_g1_g2(2)
         line_integral_around_boundary orientation
    by (auto simp add: boundary_def)
qed

lemma line_integral_exists_on_typeI_Cube_boundaries:
  assumes "(k,\<gamma>) \<in> \<partial> C"
  shows "line_integral_exists F {i} \<gamma>"
  using assms GreenThm_typeI_twoCube(2) by blast
                              
end

definition two_chain_integral:: "two_chain \<Rightarrow> (real * real \<Rightarrow> real) \<Rightarrow> real" where
  "two_chain_integral twoChain F \<equiv> \<Sum>(orient, C)\<in>twoChain. orient * (integral (cubeImage C) F)"

definition valid_two_chain where
  "valid_two_chain twoChain \<equiv> (\<forall>(orient, C) \<in> twoChain. valid_two_cube C)
                                  \<and> pairwise (\<lambda>c1 c2. (\<partial> (snd c1)) \<inter> (\<partial> (snd c2)) = {}) twoChain
                                  \<and> inj_on (cubeImage o snd) twoChain
                                  \<and> (\<exists>orient. \<forall>(orient',C) \<in> twoChain. orient' = orient)"

definition "chain_orientation twoChain = (THE orient. \<forall>(orient',C) \<in> twoChain. orient' = orient)"

lemma valid_two_chain_orient:
  "valid_two_chain twoChain \<Longrightarrow> twoCube\<in>twoChain \<Longrightarrow> fst twoCube = chain_orientation twoChain "
  unfolding valid_two_chain_def chain_orientation_def
  by (smt fst_conv prod.case_eq_if snd_conv theI)

lemma valid_two_chain_orient': 
  "valid_two_chain twoChain \<Longrightarrow> (orient, C)\<in>twoChain \<Longrightarrow> orient = chain_orientation twoChain "
  unfolding valid_two_chain_def chain_orientation_def
  by (smt fst_conv prod.case_eq_if snd_conv theI)

definition two_chain_boundary::"two_chain \<Rightarrow> one_chain" where
  "two_chain_boundary twoChain = \<Union>((boundary o snd) ` twoChain)"

notation two_chain_boundary ("\<partial>\<^sup>H _" 200)

definition gen_division where
  "gen_division s S \<equiv> (finite S \<and> (\<Union>S = s) \<and> (pairwise (\<lambda>u t. negligible (u \<inter> t))) S)"

definition two_chain_horizontal_boundary:: "two_chain \<Rightarrow> one_chain" where
  "two_chain_horizontal_boundary twoChain  \<equiv> \<Union>((horizontal_boundary o snd) ` twoChain)"

definition two_chain_vertical_boundary:: "two_chain \<Rightarrow> one_chain" where
  "two_chain_vertical_boundary twoChain  \<equiv> \<Union>((vertical_boundary o snd) ` twoChain)"

abbreviation "valid_typeI_division s twoChain v1 v2 \<equiv> ((\<forall>twoCube \<in> twoChain. typeI_twoCube twoCube v1 v2) \<and>
                                                (gen_division s ((cubeImage o snd) ` twoChain)) \<and>
                                                (valid_two_chain twoChain))"

lemma two_chain_vertical_boundary_is_boundary_chain:
  assumes "(\<forall>twoCube \<in> twoChain. typeI_twoCube twoCube v1 v2)"
  shows "boundary_chain (two_chain_vertical_boundary twoChain)"
  using assms two_cube_boundary_is_boundary
  by(simp add: boundary_chain_def two_chain_vertical_boundary_def)

lemma two_chain_horizontal_boundary_is_boundary_chain:
  assumes "(\<forall>twoCube \<in> twoChain. typeI_twoCube twoCube v1 v2)"
  shows "boundary_chain (two_chain_horizontal_boundary twoChain)"
  using assms two_cube_boundary_is_boundary
  by(simp add: boundary_chain_def two_chain_horizontal_boundary_def)

lemma two_chain_integral_eq_integral_divisable:
  assumes f_integrable: "\<forall>(orient, C) \<in> twoChain. F integrable_on cubeImage C" and
    gen_division: "gen_division s ((cubeImage o snd) ` twoChain)" and
    valid_two_chain: "valid_two_chain twoChain"
  shows "(chain_orientation twoChain) * (integral s F) = two_chain_integral twoChain F"
proof-
    have partial_deriv_integrable:
      "\<forall>(orient,C) \<in> twoChain. ((F) has_integral (integral (cubeImage C) (F))) (cubeImage C)"
      using f_integrable by auto
    then have partial_deriv_integrable:
      "\<And>twoCubeImg. twoCubeImg \<in> (cubeImage o snd) ` twoChain \<Longrightarrow> (F has_integral (integral (twoCubeImg) F)) (twoCubeImg)"
      using Henstock_Kurzweil_Integration.integrable_neg by force
    have finite_images: "finite ((cubeImage o snd) ` twoChain)"
      using gen_division gen_division_def by auto
    have negligible_images: "pairwise (\<lambda>S S'. negligible (S \<inter> S')) ((cubeImage o snd) ` twoChain)"
      using gen_division  by (auto simp add: pairwise_def gen_division_def)
    have inj: "inj_on (cubeImage o snd) twoChain"
      using valid_two_chain by (simp add: inj_on_def valid_two_chain_def)
    have *: "integral s F = (\<Sum>twoCubeImg\<in>(cubeImage o snd) ` twoChain. integral twoCubeImg F)"
      using has_integral_Union[OF finite_images partial_deriv_integrable negligible_images] gen_division
      by (auto simp add: gen_division_def)
    have **: "chain_orientation twoChain * (\<Sum>twoCubeImg\<in>(cubeImage o snd) ` twoChain. integral twoCubeImg F) = (\<Sum>(orient,C)\<in>twoChain. orient * integral (cubeImage C) F)"
      using sum.reindex inj valid_two_chain_orient valid_two_chain_orient'
      by (smt comp_apply prod.case_eq_if sum.cong sum_distrib_left valid_two_chain)
    show ?thesis
      using ** unfolding *[symmetric] two_chain_integral_def .
  qed

definition only_vertical_division' where
  "only_vertical_division' one_chain two_chain \<equiv>
       \<exists>\<V> \<H>. finite \<H> \<and> finite \<V> \<and>
               (\<forall>(k,\<gamma>) \<in> \<V>.
                 (\<exists>(k',\<gamma>') \<in> two_chain_vertical_boundary two_chain.
                     (\<exists>a \<in> {0..1}. \<exists>b \<in> {0..1}. a \<le> b \<and> subpath a b \<gamma>' = \<gamma>))) \<and>
               (common_subdiv_exists (two_chain_horizontal_boundary two_chain) \<H>
                \<or> common_reparam_exists \<H> (two_chain_horizontal_boundary two_chain)) \<and>
               boundary_chain \<H> \<and> one_chain = \<V> \<union> \<H> \<and>
               (\<forall>(k,\<gamma>)\<in>\<H>. valid_path \<gamma>)"

lemma two_chain_boundary_is_boundary_chain:
  shows "boundary_chain (two_chain_boundary twoChain)"
  by (auto simp add: boundary_chain_def two_chain_boundary_def boundary_def)

lemma finite_two_chain_vertical_boundary:
  assumes "finite two_chain"
  shows "finite (two_chain_vertical_boundary two_chain)"
  using assms  by (auto simp add: two_chain_vertical_boundary_def)

lemma finite_two_chain_horizontal_boundary:
  assumes "finite two_chain"
  shows "finite (two_chain_horizontal_boundary two_chain)"
  using assms  by (auto simp add: two_chain_horizontal_boundary_def)

lemma boundary_is_vert_hor:
  "two_chain_boundary two_chain =
      (two_chain_vertical_boundary two_chain) \<union>
      (two_chain_horizontal_boundary two_chain)"
  by(auto simp add: two_chain_boundary_def two_chain_vertical_boundary_def two_chain_horizontal_boundary_def boundary_def)

locale green_typeI_chain =  i_j_orthonorm +
  fixes F::"(real * real) \<Rightarrow> (real * real)" and two_chain s
  assumes valid_typeI_div: "valid_typeI_division s two_chain i j" and
          F_anal_valid: "\<forall>(orient,C) \<in> two_chain. analytically_valid (cubeImage C) (F\<^bsub>i\<^esub>) j i"
begin

lemma is_valid_two_chain: "valid_two_chain two_chain"
  using valid_typeI_div by auto

lemma two_chain_valid_valid_cubes: "\<forall>(orient, C) \<in> two_chain. valid_two_cube C" using valid_typeI_div
  by (auto simp add: valid_two_chain_def)

lemma two_chain_typeI_cubes: "two_cube \<in> two_chain \<Longrightarrow> typeI_twoCube two_cube i j"
                              "(orient, C) \<in> two_chain \<Longrightarrow> typeI_twoCube (orient, C) i j"
using valid_typeI_div by auto

lemma finite_two_chain: "finite two_chain"
  using valid_typeI_div [unfolded gen_division_def valid_two_chain_def]
        finite_imageD
  by metis

lemma two_chian_boundary_is_finite: "finite (two_chain_boundary two_chain)"
  unfolding two_chain_boundary_def
  apply(rule finite_UN_I)
  subgoal by (simp add: finite_two_chain)
  by (simp add: finite_boundary)

lemma typeI_chain_line_integral_exists_boundary:
  shows  
        "\<forall>(k,\<gamma>) \<in> two_chain_vertical_boundary two_chain. line_integral_exists F {i} \<gamma>" (is ?g1)
        "\<forall>(k,\<gamma>) \<in> two_chain_horizontal_boundary two_chain. line_integral_exists F {i} \<gamma>" (is ?g2)
        "\<forall>(k,\<gamma>) \<in> two_chain_boundary two_chain. line_integral_exists F {i} \<gamma>" (is ?g3)
proof -
  show ?g3
    using green_typeI_cube.line_integral_exists_on_typeI_Cube_boundaries[of i j] F_anal_valid valid_typeI_div 
    apply(auto simp add:  two_chain_boundary_def)
    using i_j_orthonorm_axioms green_typeI_cube_axioms_def green_typeI_cube_def two_chain_valid_valid_cubes by fastforce
  then show ?g1 ?g2
    by (simp add: two_chain_boundary_def two_chain_vertical_boundary_def two_chain_horizontal_boundary_def boundary_def)+
qed

lemma type_II_chain_bound_valid:
     "\<forall>(k,\<gamma>) \<in> two_chain_horizontal_boundary two_chain. valid_path \<gamma>"
     "\<forall>(k,\<gamma>) \<in> two_chain_vertical_boundary two_chain. valid_path \<gamma>"
     "\<forall>(k,\<gamma>) \<in> two_chain_boundary two_chain. valid_path \<gamma>"
  using typeI_edges_are_valid_paths[OF two_chain_typeI_cubes(2)]
  unfolding two_chain_boundary_def two_chain_horizontal_boundary_def two_chain_vertical_boundary_def boundary_def
  by auto+

lemma members_of_only_vertical_div'_line_integrable:
  assumes "only_vertical_division' one_chain two_chain"
    "(k::int, \<gamma>)\<in>one_chain"
    "(k::int, \<gamma>)\<in>one_chain"
    "finite two_chain"
    "\<forall>(orient, C) \<in> two_chain. valid_two_cube C"
  shows "line_integral_exists F {i} \<gamma>"
proof -
  have "line_integral_exists F {i} (subpath a b \<gamma>')" if "(k', \<gamma>')\<in>two_chain_vertical_boundary two_chain" "a\<in>{0..1}" "b\<in>{0..1}" "a \<le> b" for k' \<gamma>' a b
    apply(rule line_integral_exists_subpath[of "F" "{i}"])
    subgoal using typeI_chain_line_integral_exists_boundary(1)  that by auto
    subgoal using that typeI_edges_are_valid_paths[OF two_chain_typeI_cubes(2)]
      unfolding two_chain_vertical_boundary_def boundary_def
      by fastforce
    using that by auto
  then have integ_exis_vert:
    "(\<And>k \<gamma>. (\<exists>(k', \<gamma>')\<in>two_chain_vertical_boundary two_chain. \<exists>a\<in>{0..1}. \<exists>b\<in>{0..1}. a \<le> b \<and> subpath a b \<gamma>' = \<gamma>) \<Longrightarrow>
                            line_integral_exists F {i} \<gamma>)"
      by auto
  obtain \<V> \<H> where hv_props: "finite \<V>"
        "(\<forall>(k,\<gamma>) \<in> \<V>. (\<exists>(k', \<gamma>') \<in> two_chain_vertical_boundary two_chain.
            (\<exists>a \<in> {0 .. 1}. \<exists>b \<in> {0..1}. a \<le> b \<and> subpath a b \<gamma>' = \<gamma>)))"
        "one_chain = \<V> \<union> \<H>"
        "((common_subdiv_exists (two_chain_horizontal_boundary two_chain) \<H>)
          \<or> common_reparam_exists \<H> (two_chain_horizontal_boundary two_chain))"
    "finite \<H>"
    "boundary_chain \<H>"
    "\<forall>(k,\<gamma>)\<in>\<H>. valid_path \<gamma>"
    using assms(1) unfolding only_vertical_division'_def  by blast
  have finite_i: "finite {i}" by auto
  show "line_integral_exists F {i} \<gamma>"
  proof(cases "common_subdiv_exists (two_chain_horizontal_boundary two_chain) \<H>")
    case True
    show ?thesis
      using integ_exis_vert
      using gen_common_subdivision_imp_eq_line_integral(2)[OF True two_chain_horizontal_boundary_is_boundary_chain
                                                              hv_props(6) typeI_chain_line_integral_exists_boundary(2)
                                                              finite_two_chain_horizontal_boundary[OF assms(4)]
                                                              hv_props(5) finite_i]
            assms(3) hv_props(2) hv_props(3)
      by (smt UnE Un_commute Un_def case_prod_conv integ_exis_vert singleton_conv two_chain_horizontal_boundary_def valid_typeI_div)
  next
    case False
    have ii: " \<forall>(k2, \<gamma>2)\<in>two_chain_horizontal_boundary two_chain. \<forall>b\<in>{i}. continuous_on (path_image \<gamma>2) (\<lambda>x. F x \<bullet> b)"
      using F_anal_valid field_cont_on_region_cont_on_edges valid_typeI_div
      by (fastforce simp add: analytically_valid_def two_chain_horizontal_boundary_def boundary_def path_image_def)
    show "line_integral_exists F {i} \<gamma>"
      using common_reparam_exists_imp_eq_line_integral(2)[OF finite_i hv_props(5) 
           finite_two_chain_horizontal_boundary[OF assms(4)] hv_props(6) two_chain_horizontal_boundary_is_boundary_chain ii _ hv_props(7) type_II_chain_bound_valid(1)]
        integ_exis_vert[of "\<gamma>"] assms(3) hv_props False 
      by (smt UnE Un_commute Un_def case_prod_conv integ_exis_vert singleton_conv valid_typeI_div singleton_conv2)
  qed
qed

lemma meb_two_cubes_typeI: "green_typeI_cube i j C orient F" if  "(orient,C)\<in>two_chain"
  using valid_typeI_div F_anal_valid  valid_two_chain_def that 
  by (fastforce simp add: i_j_orthonorm_axioms green_typeI_cube_axioms_def green_typeI_cube_def)+

lemma boundary_inj: "inj_on (boundary o snd) two_chain"
    using is_valid_two_chain
    unfolding valid_two_cube_def valid_two_chain_def pairwise_def inj_on_def
    by (metis (mono_tags) card_0_eq comp_apply finite.emptyI inf.idem prod.case_eq_if zero_neq_numeral)

lemma GreenThm_typeI_twoChain:
  shows "two_chain_integral two_chain (\<lambda>p. - \<partial> F\<^bsub>i\<^esub> / \<partial>j |\<^bsub>p\<^esub>) = \<^sup>H\<ointegral>\<^bsub>(two_chain_boundary two_chain)\<^esub> F \<downharpoonright>\<^bsub>{i}\<^esub>"
proof-
  let ?F_a' = "(\<lambda>p. - \<partial> F\<^bsub>i\<^esub> / \<partial>j |\<^bsub>p\<^esub>)"
  have "(\<Sum>(k,g)\<in> boundary (C). k * line_integral F {i} g) = orient * integral (cubeImage C) (\<lambda>a. ?F_a' a)"
    if "(orient, C) \<in> two_chain" for orient C
    using green_typeI_cube.GreenThm_typeI_twoCube(1)[OF meb_two_cubes_typeI[OF that]]
    by (auto simp add: one_chain_line_integral_def)
  then have double_sum_eq_sum:
    "(\<Sum>(orient,C)\<in>(two_chain).(\<Sum>(k,g)\<in> boundary (C). k * line_integral F {i} g))
                     =  (\<Sum>(orient,C)\<in>(two_chain). orient * integral (cubeImage C) (\<lambda>a. ?F_a' a))"
    apply(intro Finite_Cartesian_Product.sum_cong_aux) by auto
  have pairwise_disjoint_boundaries: "\<forall>x\<in> ((boundary o snd) ` two_chain). (\<forall>y\<in> ((boundary o snd) ` two_chain). (x \<noteq> y \<longrightarrow> (x \<inter> y = {})))"
    using valid_typeI_div by (fastforce simp add:  image_def valid_two_chain_def pairwise_def)
  have finite_boundaries: "\<forall>B \<in> ((boundary o snd)` two_chain). finite B"
    using valid_typeI_div image_iff by (fastforce simp add: valid_two_cube_def valid_two_chain_def)
  have boundary_inj: "inj_on (boundary o snd) two_chain"
    using is_valid_two_chain
    unfolding valid_two_cube_def valid_two_chain_def pairwise_def inj_on_def
    by (metis (mono_tags) card_0_eq comp_apply finite.emptyI inf.idem prod.case_eq_if zero_neq_numeral)
  have "(\<Sum>x\<in>(\<Union>(orient, C)\<in>two_chain. boundary C). case x of (k, g) \<Rightarrow> k * line_integral F {i} g) =
                      (\<Sum>(orient, C)\<in>(two_chain).(\<Sum>(k,g)\<in> boundary C. k * line_integral F {i} g))"
    using sum.reindex[OF boundary_inj,  of "\<lambda>x. (\<Sum>(k,g)\<in> x. k * line_integral F {i} g)"]
    using sum.Union_disjoint[OF finite_boundaries
        pairwise_disjoint_boundaries,
        of "\<lambda>x. case x of (k, g) \<Rightarrow> (k::int) * line_integral F {i} g"]
    by (metis (mono_tags, lifting) Sup.SUP_cong comp_apply prod.case_eq_if sum.cong)
  then show ?thesis
    using double_sum_eq_sum
  by (smt Sup.SUP_cong comp_apply one_chain_line_integral_def prod.case_eq_if sum.cong two_chain_boundary_def two_chain_integral_def)
qed

lemma GreenThm_typeI_divisible:
  assumes 
    gen_division: "gen_division s ((cubeImage o snd) ` two_chain)"    (*This should follow from the assumption that images are not negligible*)
  shows "chain_orientation two_chain * integral s (\<lambda>p. - \<partial> F\<^bsub>i\<^esub> / \<partial>j |\<^bsub>p\<^esub>) =
           \<^sup>H\<ointegral>\<^bsub>(\<partial>\<^sup>H two_chain)\<^esub> F \<downharpoonright>\<^bsub>{i}\<^esub>"
proof -
  let ?F_a' = "(\<lambda>p. - \<partial> F\<^bsub>i\<^esub> / \<partial>j |\<^bsub>p\<^esub>)"
  have "chain_orientation two_chain * integral s (\<lambda>x. ?F_a' x) = two_chain_integral two_chain (\<lambda>a. ?F_a' a)"
  proof-
    have partial_deriv_integrable:
      "(?F_a' has_integral (integral (cubeImage C) ?F_a')) (cubeImage C)"
      if "(orient,C) \<in> two_chain" for orient C
      using F_anal_valid analytically_valid_imp_part_deriv_integrable_on has_integral_neg that by blast
    then have partial_deriv_integrable:
      "\<And>twoCubeImg. twoCubeImg \<in> (cubeImage o snd) ` two_chain \<Longrightarrow> ((\<lambda>x. ?F_a' x) has_integral (integral (twoCubeImg) (\<lambda>x. ?F_a' x))) (twoCubeImg)"
      using integrable_neg by force
    have finite_images: "finite ((cubeImage o snd) ` two_chain)"
      using gen_division gen_division_def by auto
    have negligible_images: "pairwise (\<lambda>S S'. negligible (S \<inter> S')) ((cubeImage o snd) ` two_chain)"
      using gen_division by (auto simp add: pairwise_def gen_division_def)
    have "inj_on (cubeImage o snd) two_chain" using valid_typeI_div valid_two_chain_def by auto
    then have "(\<Sum>twoCubeImg\<in> (cubeImage o snd) ` two_chain. chain_orientation two_chain * integral twoCubeImg (\<lambda>x. ?F_a' x))
             = (\<Sum>(orient,C)\<in>two_chain. chain_orientation two_chain * integral (cubeImage C) (\<lambda>a. ?F_a' a))"
      using sum.reindex
      by (simp add: case_prod_beta' sum.reindex_cong)
    then show ?thesis
      unfolding two_chain_integral_def
      using has_integral_Union[OF finite_images partial_deriv_integrable negligible_images]
             gen_division[unfolded gen_division_def] valid_two_chain_orient[OF is_valid_two_chain]
      by (smt has_integral_iff prod.case_eq_if sum.cong sum_distrib_left)
  qed
  then show ?thesis
    using GreenThm_typeI_twoChain F_anal_valid
    by auto
qed

lemma tracing_vertical_boundary_only_vertical_div':
  assumes one_chain_traces_boundary:
    "two_chain_horizontal_boundary two_chain \<subseteq> one_chain" and
    boundary_of_region_is_subset_of_partition_boundaries:
    "one_chain \<subseteq> two_chain_boundary two_chain"
  shows "only_vertical_division' one_chain two_chain"
  unfolding  only_vertical_division'_def
proof(intro exI conjI)
  show "finite (two_chain_horizontal_boundary two_chain)"
       "finite (one_chain - (two_chain_horizontal_boundary two_chain))"
    using boundary_of_region_is_subset_of_partition_boundaries two_chian_boundary_is_finite finite_two_chain finite_two_chain_horizontal_boundary
    by (meson finite_Diff finite_subset)+
  have "one_chain - two_chain_horizontal_boundary two_chain \<subseteq> two_chain_vertical_boundary two_chain"
    using boundary_of_region_is_subset_of_partition_boundaries unfolding boundary_is_vert_hor two_chain_horizontal_boundary_def two_chain_vertical_boundary_def
    by auto 
  moreover have  "\<forall>(k, \<gamma>)\<in>two_chain_vertical_boundary two_chain. \<exists>(k', \<gamma>')\<in>two_chain_vertical_boundary two_chain. \<exists>a\<in>{0..1}. \<exists>b\<in>{0..1}. a \<le> b \<and> subpath a b \<gamma>' = \<gamma>"
    using subpath_trivial
    using atLeastAtMost_iff le_numeral_extra(1) by blast
  ultimately show " \<forall>(k, \<gamma>)\<in>one_chain - two_chain_horizontal_boundary two_chain. \<exists>(k', \<gamma>')\<in>two_chain_vertical_boundary two_chain. \<exists>a\<in>{0..1}. \<exists>b\<in>{0..1}. a \<le> b \<and> subpath a b \<gamma>' = \<gamma>"
    using one_chain_traces_boundary by auto
  show "common_subdiv_exists (two_chain_horizontal_boundary two_chain) (two_chain_horizontal_boundary two_chain) \<or>
          common_reparam_exists (two_chain_horizontal_boundary two_chain) (two_chain_horizontal_boundary two_chain)"
       "boundary_chain (two_chain_horizontal_boundary two_chain)"
    using gen_common_boundary_sudivision_exists_refl_twochain_boundary two_chain_horizontal_boundary_is_boundary_chain type_II_chain_bound_valid(1) valid_typeI_div by blast+
  show "one_chain = one_chain - two_chain_horizontal_boundary two_chain \<union> two_chain_horizontal_boundary two_chain"
    using one_chain_traces_boundary by auto
  show "\<forall>(k, \<gamma>)\<in>two_chain_horizontal_boundary two_chain. valid_path \<gamma>"
    using type_II_chain_bound_valid(1) by blast
qed

lemma hor_vert_finite:
  "finite (two_chain_vertical_boundary two_chain)"
  "finite (two_chain_horizontal_boundary two_chain)"
  using two_chian_boundary_is_finite boundary_is_vert_hor by auto

lemma horiz_verti_disjoint:
  "(two_chain_vertical_boundary two_chain) \<inter>
     (two_chain_horizontal_boundary two_chain) = {}"
proof-
  have "(\<Union>twoC\<in>two_chain. vertical_boundary (snd twoC)) \<inter> (\<Union>twoC\<in>two_chain. horizontal_boundary (snd twoC)) = {}"
  proof -
    have "vertical_boundary C1 \<inter> horizontal_boundary C2 = {}"
      if "(orient1, C1)\<in>two_chain" "(orient2, C2)\<in>two_chain" for orient1 C1 orient2 C2
    proof (cases "C1 = C2")
      case True
      have card_4: "card (\<partial>C2) = 4"
        using valid_typeI_div valid_two_chain_def that(2)
        using valid_two_cube_def by fastforce
      then show ?thesis
        by (auto simp: boundary_def True card_insert_if split: if_split_asm)
    next
      case False
      then show ?thesis
        using valid_typeI_div
        using False that(1) that(2) case_prodE snd_conv snd_eqD
        unfolding boundary_def valid_two_chain_def pairwise_def
        apply auto
        by (metis (no_types, hide_lams) False snd_conv snd_eqD that(1) that(2))+
    qed
    then show ?thesis by (fastforce simp add: Union_disjoint)
  qed
  then show ?thesis
    by(simp add: two_chain_vertical_boundary_def two_chain_horizontal_boundary_def)
qed

lemma horiz_line_integral_zero:
  assumes "V \<subseteq> (two_chain_vertical_boundary two_chain)"
  shows "one_chain_line_integral F {i} V = 0"
proof- 
  have "line_integral F {i} (snd oneCube) = 0"
    if "oneCube \<in> two_chain_vertical_boundary(two_chain)" for oneCube
  proof -
    obtain orient C where twoC: "(orient, C)\<in>two_chain" "oneCube \<in> vertical_boundary C" "typeI_twoCube (orient, C) i j" using two_chain_typeI_cubes
      using \<open>oneCube \<in> two_chain_vertical_boundary two_chain\<close> two_chain_vertical_boundary_def by auto
    then obtain a b g1 g2 where "(\<lambda>y. C (1, y)) = (\<lambda>x. b *\<^sub>R i + linepath (g2 b) (g1 b) x *\<^sub>R j)"
      "(\<lambda>y. C (0, y)) = (\<lambda>x. a *\<^sub>R i + linepath (g2 a) (g1 a) x *\<^sub>R j)"
      using typeI_cube_explicit_spec'_prod[OF twoC(3)]
      by auto
    then obtain x y1 y2 k 
      where horiz_edge_def: "oneCube = (k, (\<lambda>t::real. x *\<^sub>R i + (linepath y2 y1 t) *\<^sub>R j))"
      using twoC(2) vertical_boundary_prod by auto
    let ?vert_edge = "(snd oneCube)"
    have "?vert_edge = (\<lambda>t. x *\<^sub>R i + (y2 + t * (y1 - y2)) *\<^sub>R j)"
      by (auto simp: horiz_edge_def algebra_simps linepath_def)
    then have "\<forall>x. ?vert_edge differentiable at x"
      by(auto intro!: derivative_intros)
    moreover have vert_edge_x_const: "\<forall>t. (?vert_edge t) \<bullet> i = x"
      by (auto simp add: horiz_edge_def inner_commute algebra_simps)
    ultimately show ?thesis
      using line_integral_on_pair_straight_path(1)
      by blast
  qed
  then show ?thesis
    using assms
    apply (simp add: one_chain_line_integral_def)
    by (force intro: sum.neutral)
qed

lemma GreenThm_typeI_divisible_region_boundary:
  assumes
    two_cubes_trace_vertical_boundaries: 
    "two_chain_horizontal_boundary two_chain \<subseteq> \<gamma>" and
    boundary_of_region_is_subset_of_partition_boundary:
    "\<gamma> \<subseteq> two_chain_boundary two_chain"
  shows "chain_orientation two_chain * integral s (\<lambda>p. - (\<partial> (F\<^bsub>i\<^esub>) / \<partial> j |\<^bsub>p\<^esub>)) = \<^sup>H\<integral>\<^bsub>\<gamma>\<^esub> F \<downharpoonright>\<^bsub>{i}\<^esub>"
proof -
  let ?F_a' = "(partial_vector_derivative (F\<^bsub>j\<^esub>) i)"
    (*Proving that line_integral on the x axis is 0 for the vertical 1-cubes*)
    (*then, the x axis line_integral on the boundaries of the twoCube is equal to the line_integral on the horizontal boundaries of the twoCube \<longrightarrow> 1*)
  have boundary_is_finite:
    "finite (two_chain_boundary two_chain)"
    by (simp add: two_chian_boundary_is_finite)
  have boundary_is_vert_hor:
    "(two_chain_boundary two_chain) =
      (two_chain_vertical_boundary two_chain) \<union> (two_chain_horizontal_boundary two_chain)"
    by(auto simp add: two_chain_boundary_def two_chain_vertical_boundary_def two_chain_horizontal_boundary_def
        boundary_def)
  then have hor_vert_finite:
    "finite (two_chain_vertical_boundary two_chain)"
    "finite (two_chain_horizontal_boundary two_chain)"
    using boundary_is_finite by auto
  have horiz_verti_disjoint:
    "(two_chain_vertical_boundary two_chain) \<inter> (two_chain_horizontal_boundary two_chain) = {}"
    using horiz_verti_disjoint by auto
  have "one_chain_line_integral F {i} (two_chain_boundary two_chain)
        = one_chain_line_integral F {i} (two_chain_vertical_boundary two_chain) +
          one_chain_line_integral F {i} (two_chain_horizontal_boundary two_chain)"
    using boundary_is_vert_hor horiz_verti_disjoint
    by (auto simp add: one_chain_line_integral_def hor_vert_finite sum.union_disjoint)
  then have y_axis_line_integral_is_only_vertical:
    "one_chain_line_integral F {i} (two_chain_boundary two_chain)
     = one_chain_line_integral F {i} (two_chain_horizontal_boundary two_chain)"
    using horiz_line_integral_zero by auto
      (*Since \<gamma> \<subseteq> the boundary of the 2-chain and the horizontal boundaries are \<subseteq> \<gamma>, then there is some \<V> \<subseteq> \<partial>\<^sub>V(2-\<C>) such that \<gamma> = \<V> \<union> \<partial>\<^sub>H(2-\<C>)*)
  have "\<exists>\<V>. \<V> \<subseteq> (two_chain_vertical_boundary two_chain) \<and>
             \<gamma> = \<V> \<union> (two_chain_horizontal_boundary two_chain)"
  proof-
    let ?\<H> = "\<gamma> - (two_chain_vertical_boundary two_chain)"
    show ?thesis
      using two_cubes_trace_vertical_boundaries
        boundary_of_region_is_subset_of_partition_boundary boundary_is_vert_hor
      by blast
  qed
  then obtain \<V> where
    h_props: "\<V> \<subseteq> (two_chain_vertical_boundary two_chain)"
    "\<gamma> = \<V> \<union> (two_chain_horizontal_boundary two_chain)"
    by auto
  have h_vert_disj: "\<V> \<inter> (two_chain_horizontal_boundary two_chain) = {}"
    using horiz_verti_disjoint h_props(1) by auto 
  have h_finite: "finite \<V>"
    using hor_vert_finite h_props(1) Finite_Set.rev_finite_subset by force
  have line_integral_on_path:
    "one_chain_line_integral F {i} \<gamma> =
     one_chain_line_integral F {i} \<V> + one_chain_line_integral F {i} (two_chain_horizontal_boundary two_chain)"
    unfolding one_chain_line_integral_def
    using sum.union_disjoint[OF h_finite hor_vert_finite(2) h_vert_disj] h_props 
    by auto
      (*Since \<V> \<subseteq> \<partial>\<^sub>V(2-\<C>), then the line_integral on the x axis along \<V> is 0, and from 1 Q.E.D. *)
  have "one_chain_line_integral F {i} \<V> = 0"
    using h_props horiz_line_integral_zero by auto
  then have "one_chain_line_integral F {i} \<gamma> =
                           one_chain_line_integral F {i} (two_chain_horizontal_boundary two_chain)"
    using line_integral_on_path by auto
  then have "one_chain_line_integral F {i} \<gamma> =
             one_chain_line_integral F {i} (two_chain_boundary two_chain)"
    using y_axis_line_integral_is_only_vertical by auto
  then show ?thesis
    using valid_typeI_div GreenThm_typeI_divisible by auto
qed

lemma GreenThm_typeI_divisible_region_boundary_gen:
  assumes only_vertical_division: "only_vertical_division' \<gamma> two_chain"
  shows "(chain_orientation two_chain) * integral s (\<lambda>p. - (\<partial> F\<^bsub>i\<^esub> / \<partial> j |\<^bsub>p\<^esub>)) = \<^sup>H\<integral>\<^bsub>\<gamma>\<^esub> F \<downharpoonright>\<^bsub>{i}\<^esub>"
proof -
  let ?F_a' = "(\<lambda>p. - (\<partial> F\<^bsub>i\<^esub> / \<partial> j |\<^bsub>p\<^esub>))"
    (*Proving that line_integral on the x axis is 0 for the vertical 1-cubes*)
    (*then, the x axis line_integral on the boundaries of the twoCube is equal to the line_integral on the horizontal boundaries of the twoCube \<longrightarrow> 1*)
  have "one_chain_line_integral F {i} (two_chain_boundary two_chain)
       = one_chain_line_integral F {i} (two_chain_vertical_boundary two_chain) +
         one_chain_line_integral F {i} (two_chain_horizontal_boundary two_chain)"
    using boundary_is_vert_hor horiz_verti_disjoint
    by (simp add: hor_vert_finite sum.union_disjoint one_chain_line_integral_def)
  then have x_axis_line_integral_is_only_horizontal:
    "one_chain_line_integral F {i} (two_chain_boundary two_chain)
                         = one_chain_line_integral F {i} (two_chain_horizontal_boundary two_chain)"
    using horiz_line_integral_zero
    by auto
      (*Since \<gamma> \<subseteq> the boundary of the 2-chain and the horizontal boundaries are \<subseteq> \<gamma>, then there is some \<V> \<subseteq> \<partial>\<^sub>V(2-\<C>) such that \<gamma> = \<V> \<union> \<partial>\<^sub>H(2-\<C>)*)
  obtain \<H> \<V> where hv_props: "finite \<V>"
    "(\<forall>(k,\<gamma>) \<in> \<V>. (\<exists>(k', \<gamma>') \<in> two_chain_vertical_boundary two_chain.
        (\<exists>a \<in> {0 .. 1}. \<exists>b \<in> {0..1}. a \<le> b \<and> subpath a b \<gamma>' = \<gamma>)))" "\<gamma> = \<H> \<union> \<V>"
    "(common_subdiv_exists (two_chain_horizontal_boundary two_chain) \<H>)
     \<or> common_reparam_exists \<H> (two_chain_horizontal_boundary two_chain)"
    "finite \<H>"
    "boundary_chain \<H>"
    "\<forall>(k,\<gamma>)\<in>\<H>. valid_path \<gamma>"
    using only_vertical_division by (auto simp add:  only_vertical_division'_def)
  have "finite {i}" by auto
  then have eq_integrals: " one_chain_line_integral F {i} \<H> = one_chain_line_integral F {i} (two_chain_horizontal_boundary two_chain)"
  proof(cases "common_subdiv_exists (two_chain_horizontal_boundary two_chain) \<H>")
    case True
    then show ?thesis
      by (metis \<open>finite {i}\<close> gen_common_subdivision_imp_eq_line_integral(1) hor_vert_finite(2) hv_props(5) hv_props(6) two_chain_horizontal_boundary_is_boundary_chain typeI_chain_line_integral_exists_boundary(2) valid_typeI_div)
  next
    case False
    have integ_exis_horiz:
      "\<forall>(k,\<gamma>) \<in> two_chain_horizontal_boundary two_chain. line_integral_exists F {i} \<gamma>"
      using typeI_chain_line_integral_exists_boundary(2) assms
      by (fastforce simp add: valid_two_chain_def)
    have integ_exis: "\<forall>(k,\<gamma>) \<in> two_chain_boundary two_chain. line_integral_exists F {i} \<gamma>"
      using typeI_chain_line_integral_exists_boundary(3) by blast
    have valid_paths: "\<forall>(k,\<gamma>) \<in> two_chain_vertical_boundary two_chain. valid_path \<gamma>"
      using type_II_chain_bound_valid(2) by blast
    have integ_exis_vert:
      "(\<And>k \<gamma>. (\<exists>(k', \<gamma>') \<in> two_chain_vertical_boundary two_chain. \<exists>a\<in>{0..1}. \<exists>b\<in>{0..1}. a \<le> b \<and> subpath a b \<gamma>' = \<gamma>) \<Longrightarrow>
                                    line_integral_exists F {i} \<gamma>)"
      using integ_exis valid_paths line_integral_exists_subpath[of "F" "{i}"]
      by (fastforce simp add: two_chain_boundary_def two_chain_vertical_boundary_def
          two_chain_horizontal_boundary_def boundary_def)
    have finite_i: "finite {i}" by auto
    have ii: " \<forall>(k2, \<gamma>2)\<in>two_chain_horizontal_boundary two_chain. \<forall>b\<in>{i}. continuous_on (path_image \<gamma>2) (\<lambda>x. F x \<bullet> b)"
      using F_anal_valid 
      apply (simp add: two_chain_horizontal_boundary_def)
      apply(subst ball_pair)
      by (smt Un_iff analytically_valid_imp_cont boundary_def field_cont_on_region_cont_on_edges horizontal_boundary_prod insert_iff prod.case_eq_if)
    have *: "common_reparam_exists \<H> (two_chain_horizontal_boundary two_chain)"
      using hv_props(4) False by auto
    show "one_chain_line_integral F {i} \<H> = one_chain_line_integral F {i} (two_chain_horizontal_boundary two_chain)"
      using common_reparam_exists_imp_eq_line_integral(1)[OF finite_i hv_props(5) hor_vert_finite(2) hv_props(6) two_chain_horizontal_boundary_is_boundary_chain ii * hv_props(7) type_II_chain_bound_valid(1)]
      using valid_typeI_div by blast
  qed
    (*Since \<V> \<subseteq> \<partial>\<^sub>H(2-\<C>), then the line_integral on the x axis along \<V> is 0, and from 1 Q.E.D. *)
  have line_integral_on_path:
    "one_chain_line_integral F {i} \<gamma> =
     one_chain_line_integral F {i} (two_chain_horizontal_boundary two_chain)"
  proof (auto simp add: one_chain_line_integral_def simp del: one_chain_line_integral_mult_left line_integral_mult_left line_integral_empty_basis one_chain_line_integral_empty_basis one_chain_line_integral_empty_chain)
    have "line_integral F {i} (snd oneCube) = 0" if oneCube: "oneCube \<in> \<V>" for oneCube
    proof -
      obtain k \<gamma> where k_gamma: "(k,\<gamma>) = oneCube"
        by (metis coeff_cube_to_path.cases)
      then obtain k' \<gamma>' a b where kp_gammap:
        "(k'::int, \<gamma>') \<in> two_chain_vertical_boundary two_chain"
        "a \<in> {0 .. 1}"
        "b \<in> {0..1}"
        "subpath a b \<gamma>' = \<gamma>"
        using hv_props oneCube
        by (smt case_prodE split_conv)
      then obtain orient C where twoC: "(orient, C)\<in>two_chain" "(k'::int, \<gamma>') \<in> vertical_boundary C" "typeI_twoCube (orient, C) i j" using two_chain_typeI_cubes
        using two_chain_vertical_boundary_def
        by auto
      then obtain a' b' g1 g2 where "(\<lambda>y. C (1, y)) = (\<lambda>x. b' *\<^sub>R i + linepath (g2 b') (g1 b') x *\<^sub>R j)"
                                    "(\<lambda>y. C (0, y)) = (\<lambda>x. a' *\<^sub>R i + linepath (g2 a') (g1 a') x *\<^sub>R j)"
        using typeI_cube_explicit_spec'_prod[OF twoC(3)]
        by auto
      then obtain x y1 y2 
        where vert_edge_def: "\<gamma>' = (\<lambda>t::real. x *\<^sub>R i + (linepath y2 y1 t) *\<^sub>R j)"
        using twoC(2) vertical_boundary_prod by auto
      show "line_integral F {i} (snd oneCube) = 0"
      proof -
        have *: "\<gamma> = (\<lambda>t::real. x *\<^sub>R i + ((1 - (b - a)*t - a) * (y2) + ((b-a)*t + a) * y1) *\<^sub>R j)"
          using vert_edge_def Product_Type.snd_conv Product_Type.fst_conv kp_gammap(4)
          by (simp add: subpath_def diff_diff_eq[symmetric] linepath_def)
        then have vert_edge_x_const: "\<forall>t. \<gamma> (t) \<bullet> i = x"
          by(auto simp add: algebra_simps inner_commute)
        have "\<forall>x. \<gamma> differentiable at x"
          unfolding *
          by(auto intro!: derivative_intros)
        then have "line_integral F {i} \<gamma> = 0"
          using line_integral_on_pair_straight_path(1) vert_edge_x_const by blast
        then show ?thesis
          using Product_Type.snd_conv k_gamma by auto
      qed
    qed
    then have "\<forall>x\<in>\<V>. (case x of (k, g) \<Rightarrow> (k::int) * line_integral F {i} g) = 0"
      by auto
    then show "(\<Sum>x\<in>\<gamma>. case x of (k, g) \<Rightarrow> real_of_int k * line_integral F {i} g) =
               (\<Sum>x\<in>two_chain_horizontal_boundary two_chain. case x of (k, g) \<Rightarrow> of_int k * line_integral F {i} g)"
      using hv_props(1) hv_props(3) hv_props(5) sum_zero_set hor_vert_finite(2) eq_integrals
      apply(auto simp add: one_chain_line_integral_def)
      by (smt Un_commute sum_zero_set)
  qed
  then have "one_chain_line_integral F {i} \<gamma> =
             one_chain_line_integral F {i} (two_chain_boundary two_chain)"
    using x_axis_line_integral_is_only_horizontal line_integral_on_path by auto
  then show ?thesis
    using GreenThm_typeI_divisible
          valid_typeI_div
    by simp
qed

end

locale green_typeI_typeII_chain = i_j_orthonorm: i_j_orthonorm i j + T1: green_typeI_chain i j F two_chain_typeI + T2: green_typeI_chain "-j" i F two_chain_typeII for i j neg_j F two_chain_typeI two_chain_typeII
begin

lemma one_chain_j_integrals':
  assumes 
    only_vertical_division:
    "only_vertical_division' one_chain_typeI two_chain_typeI"
    "boundary_chain one_chain_typeI" and
    only_horizontal_division:
    "only_vertical_division' one_chain_typeII two_chain_typeII"
    "boundary_chain one_chain_typeII" and
    typeI_and_typII_one_chains_have_gen_common_subdiv:
    "common_subdiv_exists one_chain_typeI one_chain_typeII"
  shows "\<^sup>H\<integral>\<^bsub>one_chain_typeII\<^esub> F \<downharpoonright>\<^bsub>{j}\<^esub> = \<^sup>H\<integral>\<^bsub>one_chain_typeI\<^esub> F \<downharpoonright>\<^bsub>{j}\<^esub>"
    "(\<forall>(k,\<gamma>)\<in>one_chain_typeII. line_integral_exists F {j} \<gamma>)"
    "(\<forall>(k,\<gamma>)\<in>one_chain_typeI. line_integral_exists F {j} \<gamma>)"
  proof-
    have "finite two_chain_typeII"
      using T2.finite_two_chain .
    then have "\<forall>(k,\<gamma>)\<in>one_chain_typeII. line_integral_exists F {-j} \<gamma>"
      using T2.members_of_only_vertical_div'_line_integrable assms T2.two_chain_valid_valid_cubes
      by blast
    then show ii: "\<forall>(k,\<gamma>)\<in>one_chain_typeII. line_integral_exists F {j} \<gamma>"      
      unfolding line_integral_neg_base[where ?i = j] by auto
    have iv: "finite one_chain_typeI"
      using only_vertical_division(1) only_vertical_division'_def by auto
    moreover have iv': "finite one_chain_typeII"
      using only_horizontal_division(1) only_vertical_division'_def by auto
    ultimately show "one_chain_line_integral F {j} one_chain_typeII =
                     one_chain_line_integral F {j} one_chain_typeI"
                    "\<forall>(k, \<gamma>)\<in>one_chain_typeI. line_integral_exists F {j} \<gamma>"
      using gen_common_subdivision_imp_eq_line_integral[OF _
          only_horizontal_division(2) only_vertical_division(2) ii] ii typeI_and_typII_one_chains_have_gen_common_subdiv common_subdiv_exists_comm'
      by auto
  qed

lemma neg_y_comp_double_integral:
"integral s (\<lambda>x. - (\<partial> F\<^bsub>-j\<^esub> / \<partial> i |\<^bsub>x\<^esub>)) = integral s (\<lambda>x. (\<partial> F\<^bsub>j\<^esub> / \<partial> i |\<^bsub>x\<^esub>))"
    proof-
      have "\<forall>(orient,C)\<in>two_chain_typeII. (\<forall>a\<in>cubeImage C. partially_vector_differentiable (F\<^bsub>-j\<^esub>) i a)"
        using T2.F_anal_valid
        unfolding analytically_valid_def by auto
      then have "(\<forall>a \<in> s. partially_vector_differentiable (F\<^bsub>-j\<^esub>) i a)"
        using T2.valid_typeI_div unfolding gen_division_def by auto
      then show ?thesis
        apply(intro integral_cong)
        using partial_deriv_minus[where f = "(F\<^bsub>-j\<^esub>)"]
        unfolding inner_minus_right scaleR_minus1_left
        by auto
    qed

lemma GreenThm_typeI_typeII_divisible_region_2':
  assumes 
    only_vertical_division:
    "only_vertical_division' one_chain_typeI two_chain_typeI"
    "boundary_chain one_chain_typeI" and
    only_horizontal_division:
    "only_vertical_division' one_chain_typeII two_chain_typeII"
    "boundary_chain one_chain_typeII" and
    typeI_and_typII_one_chains_have_gen_common_subdiv:
    "common_subdiv_exists one_chain_typeI one_chain_typeII"
  shows "integral s (\<lambda>x. chain_orientation two_chain_typeII * \<partial> F\<^bsub>j\<^esub>/ \<partial>i |\<^bsub>x\<^esub> - chain_orientation two_chain_typeI * \<partial> F\<^bsub>i\<^esub>/ \<partial>j |\<^bsub>x\<^esub>) = \<^sup>H\<integral>\<^bsub>one_chain_typeI\<^esub> F \<downharpoonright>\<^bsub>{i, j}\<^esub>" (is ?g1)
        "integral s (\<lambda>x. chain_orientation two_chain_typeII * \<partial> F\<^bsub>j\<^esub>/ \<partial>i |\<^bsub>x\<^esub> - chain_orientation two_chain_typeI * \<partial> F\<^bsub>i\<^esub>/ \<partial>j |\<^bsub>x\<^esub>) = \<^sup>H\<integral>\<^bsub>one_chain_typeII\<^esub> F \<downharpoonright>\<^bsub>{i, j}\<^esub>" (is ?g2)
proof -
  let ?F_b' = "\<partial> F\<^bsub>i\<^esub>/ \<partial>j"
  let ?F_a' = "\<partial> F\<^bsub>-j\<^esub>/ \<partial>i"
  have one_chain_i_integrals:
    "\<^sup>H\<integral>\<^bsub>one_chain_typeI\<^esub> F \<downharpoonright>\<^bsub>{i}\<^esub> = \<^sup>H\<integral>\<^bsub>one_chain_typeII\<^esub> F \<downharpoonright>\<^bsub>{i}\<^esub> \<and>
              (\<forall>(k,\<gamma>)\<in>one_chain_typeI. line_integral_exists F {i} \<gamma>) \<and>
              (\<forall>(k,\<gamma>)\<in>one_chain_typeII. line_integral_exists F {i} \<gamma>)"
  proof-
    have "finite two_chain_typeI"
      using T1.finite_two_chain .
    then have ii: "line_integral_exists F {i} \<gamma>" if "(k, \<gamma>)\<in>one_chain_typeI" for k \<gamma>         
      by(auto intro!: T1.members_of_only_vertical_div'_line_integrable assms(1) that T1.two_chain_valid_valid_cubes)
    have "finite (two_chain_horizontal_boundary two_chain_typeI)"
      by (meson T1.valid_typeI_div finite_imageD finite_two_chain_horizontal_boundary gen_division_def valid_two_chain_def)
    then have "finite one_chain_typeI"
      using only_vertical_division(1)
      using only_vertical_division'_def by auto
    moreover have "finite one_chain_typeII"
      using only_horizontal_division(1) only_vertical_division'_def by auto
    ultimately show ?thesis
      using gen_common_subdivision_imp_eq_line_integral[OF typeI_and_typII_one_chains_have_gen_common_subdiv
          only_vertical_division(2) only_horizontal_division(2)] ii 
      by blast
  qed
  have one_chain_j_integrals:
    "\<^sup>H\<integral>\<^bsub>one_chain_typeII\<^esub> F \<downharpoonright>\<^bsub>{j}\<^esub> = \<^sup>H\<integral>\<^bsub>one_chain_typeI\<^esub> F \<downharpoonright>\<^bsub>{j}\<^esub>"
    "(\<forall>(k,\<gamma>)\<in>one_chain_typeII. line_integral_exists F {j} \<gamma>)"
    "(\<forall>(k,\<gamma>)\<in>one_chain_typeI. line_integral_exists F {j} \<gamma>)"
  proof-
    have "finite two_chain_typeII"
      using T2.finite_two_chain .
    then have "\<forall>(k,\<gamma>)\<in>one_chain_typeII. line_integral_exists F {-j} \<gamma>"
      using T2.members_of_only_vertical_div'_line_integrable assms(3) T2.two_chain_valid_valid_cubes
      by blast
    then show ii: "\<forall>(k,\<gamma>)\<in>one_chain_typeII. line_integral_exists F {j} \<gamma>"      
      unfolding line_integral_neg_base[where ?i = j] by auto
    have typeII_and_typI_one_chains_have_common_subdiv: "common_subdiv_exists one_chain_typeII one_chain_typeI"
      by (simp add: common_subdiv_exists_comm' typeI_and_typII_one_chains_have_gen_common_subdiv)
    have iv: "finite one_chain_typeI"
      using only_vertical_division(1) only_vertical_division'_def by auto
    moreover have iv': "finite one_chain_typeII"
      using only_horizontal_division(1) only_vertical_division'_def by auto
    ultimately show "one_chain_line_integral F {j} one_chain_typeII =
                     one_chain_line_integral F {j} one_chain_typeI"
                    "\<forall>(k, \<gamma>)\<in>one_chain_typeI. line_integral_exists F {j} \<gamma>"
      using gen_common_subdivision_imp_eq_line_integral[OF typeII_and_typI_one_chains_have_common_subdiv
          only_horizontal_division(2) only_vertical_division(2) ii] ii 
      by auto
  qed
  have typeI_regions_integral:
    "chain_orientation two_chain_typeI * integral s (\<lambda>x. - ?F_b' x) = \<^sup>H\<integral>\<^bsub>one_chain_typeI\<^esub> F \<downharpoonright>\<^bsub>{i}\<^esub> "
    apply(rule T1.GreenThm_typeI_divisible_region_boundary_gen)
    using  T1.valid_typeI_div
          T1.F_anal_valid  only_vertical_division(1)
    by blast
  have typeII_regions_integral: "chain_orientation two_chain_typeII * integral s (\<lambda>x. (\<partial> F\<^bsub>j\<^esub> / \<partial> i |\<^bsub>x\<^esub>)) = \<^sup>H\<integral>\<^bsub>one_chain_typeII\<^esub> F\<downharpoonright>\<^bsub>{j}\<^esub>"
  proof-
    have "chain_orientation two_chain_typeII * integral s (\<lambda>x. - ?F_a' x) = \<^sup>H\<integral>\<^bsub>one_chain_typeII\<^esub> F\<downharpoonright>\<^bsub>{-j}\<^esub> "
      apply(rule T2.GreenThm_typeI_divisible_region_boundary_gen)
      using T2.valid_typeI_div
        T2.F_anal_valid  only_horizontal_division(1)
      by blast
    moreover have "... = \<^sup>H\<integral>\<^bsub>one_chain_typeII\<^esub> F\<downharpoonright>\<^bsub>{j}\<^esub>"
      unfolding one_chain_line_integral_neg_base[where ?i = "j"] by auto
    ultimately show ?thesis
      using neg_y_comp_double_integral
      by metis
  qed
  have line_integral_dist:
    "one_chain_line_integral F {i, j} one_chain_typeI = one_chain_line_integral F {i} one_chain_typeI + one_chain_line_integral F {j} one_chain_typeI \<and>
     one_chain_line_integral F {i, j} one_chain_typeII = one_chain_line_integral F {i} one_chain_typeII + one_chain_line_integral F {j} one_chain_typeII"
  proof (simp add: one_chain_line_integral_def del: one_chain_line_integral_mult_left line_integral_mult_left line_integral_empty_basis one_chain_line_integral_empty_basis one_chain_line_integral_empty_chain)
    have line_integral_distrib:
      "(\<Sum>(k,g)\<in>one_chain_typeI. k * line_integral F {i, j} g) =
       (\<Sum>(k,g)\<in>one_chain_typeI. k * line_integral F {i} g +  k * line_integral F {j} g) \<and>
       (\<Sum>(k,g)\<in>one_chain_typeII. k * line_integral F {i, j} g) =
       (\<Sum>(k,g)\<in>one_chain_typeII. k * line_integral F {i} g +  k * line_integral F {j} g)"
    proof -
      have 0: "k * line_integral F {i, j} g = k * line_integral F {i} g +  k * line_integral F {j} g"
        if  "(k,g) \<in> one_chain_typeII" for k g
      proof -
        have "line_integral_exists F {i} g" "line_integral_exists F {j} g" "finite {i, j}"
          using one_chain_i_integrals one_chain_j_integrals that by fastforce+
        moreover have "{i} \<inter> {j} = {}"
          using local.i_j_orthonorm.i_neq_j by blast
        ultimately have "line_integral F {i, j} g = line_integral F {i} g + line_integral F {j} g"
          by (metis insert_is_Un line_integral_sum_gen(1))
        then show "k * line_integral F {i, j} g = k * line_integral F {i} g + k * line_integral F {j} g"
          by (simp add: distrib_left)
      qed
      have "k * line_integral F {i, j} g = k * line_integral F {i} g +  k * line_integral F {j} g"
        if "(k,g) \<in> one_chain_typeI" for k g
      proof -
        have "line_integral F {i, j} g = line_integral F {i} g + line_integral F {j} g"
          by (smt that i_j_orthonorm.i_j_inner_zero i_j_orthonorm.i_neq_j disjoint_insert(2) finite.emptyI finite.insertI inf_bot_right insert_absorb insert_commute insert_is_Un line_integral_sum_gen(1) one_chain_i_integrals one_chain_j_integrals prod.case_eq_if singleton_inject snd_conv)
        then show "k * line_integral F {i, j} g = k * line_integral F {i} g + k * line_integral F {j} g"
          by (simp add: distrib_left)
      qed
      then show ?thesis
        using 0 by (smt sum.cong split_cong)
    qed
    show "(\<Sum>(k::int, g)\<in>one_chain_typeI. k * line_integral F {i, j} g) =
          (\<Sum>(k, g)\<in>one_chain_typeI. k * line_integral F {i} g) + (\<Sum>(k::int, g)\<in>one_chain_typeI. k * line_integral F {j} g) \<and>
          (\<Sum>(k::int, g)\<in>one_chain_typeII. k * line_integral F {i, j} g) =
          (\<Sum>(k, g)\<in>one_chain_typeII. k * line_integral F {i} g) + (\<Sum>(k::int, g)\<in>one_chain_typeII. k * line_integral F {j} g)"
    proof -
      have 0: "(\<lambda>x. (case x of (k::int, g) \<Rightarrow> k * line_integral F {i} g) + (case x of (k::int, g) \<Rightarrow> k * line_integral F {j} g)) =
                                  (\<lambda>x. (case x of (k::int, g) \<Rightarrow> (k * line_integral F {i} g) +  k * line_integral F {j} g))"
        using comm_monoid_add_class.sum.distrib by auto
      then have 1: "(\<Sum>x\<in>one_chain_typeI. (case x of (k::int, g) \<Rightarrow> k * line_integral F {i} g) + (case x of (k::int, g) \<Rightarrow> k * line_integral F {j} g)) =
                    (\<Sum>x\<in>one_chain_typeI. (case x of (k::int, g) \<Rightarrow>( k * line_integral F {i} g + k * line_integral F {j} g)))"
        by presburger
      have "(\<Sum>x\<in>one_chain_typeII. (case x of (k, g) \<Rightarrow> k * line_integral F {i} g) + (case x of (k, g) \<Rightarrow> k * line_integral F {j} g)) =
            (\<Sum>x\<in>one_chain_typeII. (case x of (k, g) \<Rightarrow>( k * line_integral F {i} g + k * line_integral F {j} g)))"
        using 0 by presburger
      then show ?thesis
        using sum.distrib[of "(\<lambda>(k, g).  k * line_integral F {i} g)" "(\<lambda>(k, g).  k * line_integral F {j} g)" "one_chain_typeI"]
          sum.distrib[of "(\<lambda>(k, g).  k * line_integral F {i} g)" "(\<lambda>(k, g).  k * line_integral F {j} g)" "one_chain_typeII"]
          line_integral_distrib 1
        by auto
    qed
  qed
  have integral_dis: "integral s (\<lambda>x. c * (\<partial> F\<^bsub>j\<^esub>/ \<partial>i |\<^bsub>x\<^esub>) - d * (\<partial> F\<^bsub>i\<^esub>/ \<partial>j |\<^bsub>x\<^esub>)) = integral s (\<lambda>x. c * (\<partial> F\<^bsub>j\<^esub>/ \<partial>i |\<^bsub>x\<^esub>)) + integral s (\<lambda>x. - d * (\<partial> F\<^bsub>i\<^esub>/ \<partial>j |\<^bsub>x\<^esub>))" for c d
  proof -
    have F_ji_integrable: "(\<partial> F\<^bsub>j\<^esub>/ \<partial>i) integrable_on s"
    proof-
      have "\<forall>(orient,C)\<in>two_chain_typeII. (\<forall>a\<in>cubeImage C. partially_vector_differentiable (F\<^bsub>-j\<^esub>) i a)"
        using T2.F_anal_valid
        unfolding analytically_valid_def by auto
      then have *: "(\<forall>a \<in> s. partially_vector_differentiable (F\<^bsub>-j\<^esub>) i a)"
        using T2.valid_typeI_div unfolding gen_division_def by auto
      have "(?F_a' has_integral integral (cubeImage (snd twoC)) ?F_a') (cubeImage (snd twoC))"
        if "twoC \<in> two_chain_typeII" for twoC
        using analytically_valid_imp_part_deriv_integrable_on T2.F_anal_valid has_integral_integrable_integral that
        by (metis (mono_tags, lifting) split_beta')
      then have "\<And>u. u \<in> (cubeImage o snd) ` two_chain_typeII \<Longrightarrow> (?F_a' has_integral integral (u) ?F_a') (u)"
        by auto
      then have "(?F_a' has_integral (\<Sum>img\<in>(cubeImage o snd)` two_chain_typeII. integral img ?F_a')) s"
        by (smt T2.valid_typeI_div gen_division_def has_integral_Union pairwise_def)
      then have F_a'_integrable:
        "(?F_a' integrable_on s)" by auto
      then have "((%x. - ?F_a' x) integrable_on s)"
        by (simp add: integrable_neg)
      then show ?thesis
        apply(rule integrable_spike[where S = "{}"])
        using partial_deriv_minus[where f = "(F\<^bsub>-j\<^esub>)"] *
        unfolding inner_minus_right scaleR_minus1_left
        by auto
    qed
    have "\<forall>(orient, C) \<in> two_chain_typeI. (?F_b' has_integral integral (cubeImage C) ?F_b') (cubeImage C)"
      using analytically_valid_imp_part_deriv_integrable_on T1.F_anal_valid by blast
    then have "\<And>u. u \<in> (cubeImage o snd) ` two_chain_typeI \<Longrightarrow> (?F_b' has_integral integral (u) ?F_b') (u)"
      by auto
    then have *: "(?F_b' has_integral (\<Sum>img\<in>(cubeImage o snd) ` two_chain_typeI. integral img ?F_b')) s"
      by (smt T1.valid_typeI_div gen_division_def has_integral_Union pairwise_def)
    show ?thesis
      using has_integral_mult_right[OF *]  integrable_on_cmult_left[OF F_ji_integrable]
      by (simp add: Henstock_Kurzweil_Integration.integral_diff has_integral_iff)
  qed
  show ?g1
    using  one_chain_j_integrals(2,3) integral_dis line_integral_dist typeI_regions_integral typeII_regions_integral
    unfolding integral_mult_right[symmetric] mult_minus_right one_chain_j_integrals
    by simp
  show ?g2
    using one_chain_i_integrals integral_dis line_integral_dist typeI_regions_integral typeII_regions_integral
    by simp
qed

lemma GreenThm_typeI_typeII_divisible_region_eq_orient':
  assumes 
    only_vertical_division:
    "only_vertical_division' one_chain_typeI two_chain_typeI"
    "boundary_chain one_chain_typeI" and
    only_horizontal_division:
    "only_vertical_division' one_chain_typeII two_chain_typeII"
    "boundary_chain one_chain_typeII" and
    typeI_and_typII_one_chains_have_gen_common_subdiv:
    "common_subdiv_exists one_chain_typeI one_chain_typeII" and
    same_orientation:
    "chain_orientation two_chain_typeI = chain_orientation two_chain_typeII"
  shows "chain_orientation two_chain_typeI * integral s (\<lambda>x. \<partial> F\<^bsub>j\<^esub>/ \<partial>i |\<^bsub>x\<^esub> - \<partial> F\<^bsub>i\<^esub>/ \<partial>j |\<^bsub>x\<^esub>) = \<^sup>H\<integral>\<^bsub>one_chain_typeI\<^esub> F \<downharpoonright>\<^bsub>{i, j}\<^esub>" (is ?g1)
        "chain_orientation two_chain_typeII * integral s (\<lambda>x. \<partial> F\<^bsub>j\<^esub>/ \<partial>i |\<^bsub>x\<^esub> - \<partial> F\<^bsub>i\<^esub>/ \<partial>j |\<^bsub>x\<^esub>) = \<^sup>H\<integral>\<^bsub>one_chain_typeII\<^esub> F \<downharpoonright>\<^bsub>{i, j}\<^esub>" (is ?g2)
  using GreenThm_typeI_typeII_divisible_region_2'[OF assms(1-5)]
  unfolding assms(6) integral_mult_right[symmetric]
  by (auto simp add: algebra_simps)

lemma GreenThm_typeI_typeII_divisible_region_anti_cwise':
  assumes 
    only_vertical_division:
    "only_vertical_division' one_chain_typeI two_chain_typeI"
    "boundary_chain one_chain_typeI" and
    only_horizontal_division:
    "only_vertical_division' one_chain_typeII two_chain_typeII"
    "boundary_chain one_chain_typeII" and
    typeI_and_typII_one_chains_have_gen_common_subdiv:
    "common_subdiv_exists one_chain_typeI one_chain_typeII" and
    same_orientation:
    "chain_orientation two_chain_typeI = chain_orientation two_chain_typeII"
    "chain_orientation two_chain_typeII = 1"
  shows "integral s (\<lambda>x. \<partial> F\<^bsub>j\<^esub>/ \<partial>i |\<^bsub>x\<^esub> - \<partial> F\<^bsub>i\<^esub>/ \<partial>j |\<^bsub>x\<^esub>) = \<^sup>H\<integral>\<^bsub>one_chain_typeI\<^esub> F \<downharpoonright>\<^bsub>{i, j}\<^esub>" (is ?g1)
        "integral s (\<lambda>x. \<partial> F\<^bsub>j\<^esub>/ \<partial>i |\<^bsub>x\<^esub> - \<partial> F\<^bsub>i\<^esub>/ \<partial>j |\<^bsub>x\<^esub>) = \<^sup>H\<integral>\<^bsub>one_chain_typeII\<^esub> F \<downharpoonright>\<^bsub>{i, j}\<^esub>" (is ?g2)
  using GreenThm_typeI_typeII_divisible_region_eq_orient'[OF assms(1-6)]
  unfolding integral_mult_right[symmetric]
  by (auto simp add: algebra_simps assms(6,7))

lemma GreenThm_typeI_typeII_divisible_region':
  assumes only_vertical_division:
    "only_vertical_division' one_chain_typeI two_chain_typeI"
    "boundary_chain one_chain_typeI" and
    only_horizontal_division:
    "only_vertical_division' one_chain_typeII two_chain_typeII"
    "boundary_chain one_chain_typeII" and
    typeI_and_typII_one_chains_have_common_subdiv:
    "common_boundary_sudivision_exists one_chain_typeI one_chain_typeII"
  shows "integral s (\<lambda>x. chain_orientation two_chain_typeII * (\<partial> (F\<^bsub>j\<^esub>) / \<partial> i |\<^bsub>x\<^esub>) - chain_orientation two_chain_typeI * (\<partial> (F\<^bsub>i\<^esub>) / \<partial> j |\<^bsub>x\<^esub>)) = \<^sup>H\<integral>\<^bsub>one_chain_typeI\<^esub> F \<downharpoonright>\<^bsub>{i, j}\<^esub>"
    "integral s (\<lambda>x. chain_orientation two_chain_typeII * (\<partial> F\<^bsub>j\<^esub> / \<partial> i |\<^bsub>x\<^esub>) - chain_orientation two_chain_typeI * (\<partial> F\<^bsub>i\<^esub> / \<partial> j |\<^bsub>x\<^esub>)) = \<^sup>H\<integral>\<^bsub>one_chain_typeII\<^esub> F \<downharpoonright>\<^bsub>{i, j}\<^esub>"
  apply(auto intro!: GreenThm_typeI_typeII_divisible_region_2'[OF assms(1-4)])
  using common_subdiv_imp_gen_common_subdiv'[OF typeI_and_typII_one_chains_have_common_subdiv]
  by blast+

lemma GreenThm_typeI_typeII_divisible_region_eq_orient_2:
  assumes only_vertical_division:
    "only_vertical_division' one_chain_typeI two_chain_typeI"
    "boundary_chain one_chain_typeI" and
    only_horizontal_division:
    "only_vertical_division' one_chain_typeII two_chain_typeII"
    "boundary_chain one_chain_typeII" and
    typeI_and_typII_one_chains_have_common_subdiv:
    "common_boundary_sudivision_exists one_chain_typeI one_chain_typeII" and
    same_orientation:
    "chain_orientation two_chain_typeI = chain_orientation two_chain_typeII"
  shows "chain_orientation two_chain_typeI * integral s (\<lambda>x. \<partial> F\<^bsub>j\<^esub>/ \<partial>i |\<^bsub>x\<^esub> - \<partial> F\<^bsub>i\<^esub>/ \<partial>j |\<^bsub>x\<^esub>) = \<^sup>H\<integral>\<^bsub>one_chain_typeI\<^esub> F \<downharpoonright>\<^bsub>{i, j}\<^esub>" (is ?g1)
        "chain_orientation two_chain_typeII * integral s (\<lambda>x. \<partial> F\<^bsub>j\<^esub>/ \<partial>i |\<^bsub>x\<^esub> - \<partial> F\<^bsub>i\<^esub>/ \<partial>j |\<^bsub>x\<^esub>) = \<^sup>H\<integral>\<^bsub>one_chain_typeII\<^esub> F \<downharpoonright>\<^bsub>{i, j}\<^esub>" (is ?g2)
  using GreenThm_typeI_typeII_divisible_region'[OF assms(1-5)]
  unfolding assms(6) integral_mult_right[symmetric]
  by (auto simp add: algebra_simps)

lemma GreenThm_typeI_typeII_divisible_region_finite_holes:
  assumes valid_cube_boundary: "\<forall>(k,\<gamma>)\<in>boundary C. valid_path \<gamma>" and
    only_vertical_division:
    "only_vertical_division' (\<partial>C) two_chain_typeI" and
    only_horizontal_division:
    "only_vertical_division' (\<partial>C) two_chain_typeII" and
    s_is_oneCube: "s = cubeImage (C)" and
    "two_chain_typeI = {(orient, C)}" "two_chain_typeII = {(orient, C)}"
  shows "orient * integral (cubeImage C) (\<lambda>x. (\<partial> F\<^bsub>j\<^esub> / \<partial> i |\<^bsub>x\<^esub>) - (\<partial> F\<^bsub>i\<^esub> / \<partial> j |\<^bsub>x\<^esub>)) = \<^sup>H\<ointegral>\<^bsub>\<partial>C\<^esub> F \<downharpoonright>\<^bsub>{i, j}\<^esub>"
proof-
  have "orient = chain_orientation {(orient, C)}" unfolding chain_orientation_def using valid_two_chain_orient by force
  then show ?thesis
    using GreenThm_typeI_typeII_divisible_region_eq_orient_2(1)[OF only_vertical_division two_cube_boundary_is_boundary only_horizontal_division two_cube_boundary_is_boundary common_boundary_sudivision_exists_refl_2[OF assms(1)]]
      s_is_oneCube assms(5,6)
    by auto
qed

lemma GreenThm_typeI_typeII_divisible_region_equivallent_boundary:
  assumes 
    gen_divisions: "gen_division s ((cubeImage o snd) ` two_chain_typeI)"
    "gen_division s ((cubeImage o snd) ` two_chain_typeII)" and
    typeI_two_cubes_trace_horizontal_boundaries:
    "two_chain_horizontal_boundary two_chain_typeI \<subseteq> one_chain_typeI" and
    typeII_two_cubes_trace_vertical_boundaries:
    "two_chain_horizontal_boundary two_chain_typeII \<subseteq> one_chain_typeII" and
    boundary_of_region_is_subset_of_partition_boundaries:
    "one_chain_typeI \<subseteq> two_chain_boundary two_chain_typeI"
    "one_chain_typeII \<subseteq> two_chain_boundary two_chain_typeII" and
    typeI_and_typII_one_chains_have_common_subdiv:
    "common_boundary_sudivision_exists one_chain_typeI one_chain_typeII"
  shows "integral s (\<lambda>x. chain_orientation two_chain_typeII * (\<partial> F\<^bsub>j\<^esub> / \<partial> i |\<^bsub>x\<^esub>) - chain_orientation two_chain_typeI * (\<partial> F\<^bsub>i\<^esub> / \<partial> j |\<^bsub>x\<^esub>)) = \<^sup>H\<integral>\<^bsub>one_chain_typeI\<^esub> F \<downharpoonright>\<^bsub>{i, j}\<^esub>"
    "integral s (\<lambda>x. chain_orientation two_chain_typeII * (\<partial> F\<^bsub>j\<^esub> / \<partial> i |\<^bsub>x\<^esub>) - chain_orientation two_chain_typeI * (\<partial> F\<^bsub>i\<^esub> / \<partial> j |\<^bsub>x\<^esub>)) = \<^sup>H\<integral>\<^bsub>one_chain_typeII\<^esub> F \<downharpoonright>\<^bsub>{i, j}\<^esub>"
  using GreenThm_typeI_typeII_divisible_region'[OF
        T1.tracing_vertical_boundary_only_vertical_div'[OF typeI_two_cubes_trace_horizontal_boundaries boundary_of_region_is_subset_of_partition_boundaries(1)]
        subset_boundary_is_boundary[OF boundary_of_region_is_subset_of_partition_boundaries(1)]
        T2.tracing_vertical_boundary_only_vertical_div'[OF  typeII_two_cubes_trace_vertical_boundaries boundary_of_region_is_subset_of_partition_boundaries(2)]
        subset_boundary_is_boundary[OF boundary_of_region_is_subset_of_partition_boundaries(2)]
        typeI_and_typII_one_chains_have_common_subdiv]
  by (auto simp add: two_chain_boundary_is_boundary_chain)

lemma GreenThm_typeI_typeII_divisible_region_equivallent_boundary_eq_orient:
  assumes 
    gen_divisions: "gen_division s ((cubeImage o snd) ` two_chain_typeI)"
    "gen_division s ((cubeImage o snd) ` two_chain_typeII)" and
    typeI_two_cubes_trace_horizontal_boundaries:
    "two_chain_horizontal_boundary two_chain_typeI \<subseteq> one_chain_typeI" and
    typeII_two_cubes_trace_vertical_boundaries:
    "two_chain_horizontal_boundary two_chain_typeII \<subseteq> one_chain_typeII" and
    boundary_of_region_is_subset_of_partition_boundaries:
    "one_chain_typeI \<subseteq> two_chain_boundary two_chain_typeI"
    "one_chain_typeII \<subseteq> two_chain_boundary two_chain_typeII" and
    typeI_and_typII_one_chains_have_common_subdiv:
    "common_boundary_sudivision_exists one_chain_typeI one_chain_typeII" and
    same_orientation:
    "chain_orientation two_chain_typeI = chain_orientation two_chain_typeII"
  shows "chain_orientation two_chain_typeI * integral s (\<lambda>x.  (\<partial> F\<^bsub>j\<^esub> / \<partial> i |\<^bsub>x\<^esub>) - (\<partial> F\<^bsub>i\<^esub> / \<partial> j |\<^bsub>x\<^esub>)) = \<^sup>H\<integral>\<^bsub>one_chain_typeI\<^esub> F \<downharpoonright>\<^bsub>{i, j}\<^esub>"
    "chain_orientation two_chain_typeII * integral s (\<lambda>x. (\<partial> F\<^bsub>j\<^esub> / \<partial> i |\<^bsub>x\<^esub>) - (\<partial> F\<^bsub>i\<^esub> / \<partial> j |\<^bsub>x\<^esub>)) = \<^sup>H\<integral>\<^bsub>one_chain_typeII\<^esub> F \<downharpoonright>\<^bsub>{i, j}\<^esub>"
  using GreenThm_typeI_typeII_divisible_region_equivallent_boundary[OF assms(1-7)]
  unfolding assms(8) integral_mult_right[symmetric]
  by (auto simp add: algebra_simps)

lemma GreenThm_typeI_typeII_divisible_region_equivallent_boundary_eq_orient_anticwise:
  assumes 
    gen_divisions: "gen_division s ((cubeImage o snd) ` two_chain_typeI)"
    "gen_division s ((cubeImage o snd) ` two_chain_typeII)" and
    typeI_two_cubes_trace_horizontal_boundaries:
    "two_chain_horizontal_boundary two_chain_typeI \<subseteq> one_chain_typeI" and
    typeII_two_cubes_trace_vertical_boundaries:
    "two_chain_horizontal_boundary two_chain_typeII \<subseteq> one_chain_typeII" and
    boundary_of_region_is_subset_of_partition_boundaries:
    "one_chain_typeI \<subseteq> two_chain_boundary two_chain_typeI"
    "one_chain_typeII \<subseteq> two_chain_boundary two_chain_typeII" and
    typeI_and_typII_one_chains_have_common_subdiv:
    "common_boundary_sudivision_exists one_chain_typeI one_chain_typeII" and
    same_orientation:
    "chain_orientation two_chain_typeI = chain_orientation two_chain_typeII"
    "chain_orientation two_chain_typeI = 1"
  shows "integral s (\<lambda>x.  (\<partial> F\<^bsub>j\<^esub> / \<partial> i |\<^bsub>x\<^esub>) - (\<partial> F\<^bsub>i\<^esub> / \<partial> j |\<^bsub>x\<^esub>)) = \<^sup>H\<integral>\<^bsub>one_chain_typeI\<^esub> F \<downharpoonright>\<^bsub>{i, j}\<^esub>"
    " integral s (\<lambda>x. (\<partial> F\<^bsub>j\<^esub> / \<partial> i |\<^bsub>x\<^esub>) - (\<partial> F\<^bsub>i\<^esub> / \<partial> j |\<^bsub>x\<^esub>)) = \<^sup>H\<integral>\<^bsub>one_chain_typeII\<^esub> F \<downharpoonright>\<^bsub>{i, j}\<^esub>"
  using GreenThm_typeI_typeII_divisible_region_equivallent_boundary_eq_orient[OF assms(1-8)]
  using same_orientation
  by (auto simp add: algebra_simps)

end

(*Instantiating the theorems for concrete x and y axes*)

lemma absolutely_integral_minusarg2_iff:
  fixes g :: "real \<times> real \<Rightarrow> real"
  shows "(\<lambda>(x,y). g (x,-y)) absolutely_integrable_on s \<and> integral s (\<lambda>(x,y). g (x,-y)) = b \<longleftrightarrow> g absolutely_integrable_on (\<lambda>(x,y). (x,-y)) ` s \<and> integral ((\<lambda>(x,y). (x,-y)) ` s) g = b"
proof-
  have "(\<lambda>x. case x of (x, y) \<Rightarrow> g (x, - y)) = g o (\<lambda>(x,y). (x, -y))" by auto
  have "orthogonal_transformation (\<lambda>h. (fst h, - snd h))"
    unfolding orthogonal_transformation_def linear_iff Modules.additive_def embed_vec_def embed_pair_def
    by auto
  then have "orthogonal_transformation (embed_vec o (\<lambda>h. (fst h, - snd h)) o embed_pair)"
    using orthogonal_embed_vec_compose orthogonal_compose_embed_pair by auto
  then have det_1: "\<bar>det (matrix (embed_vec o (\<lambda>h. (fst h, - snd h)) o embed_pair))\<bar> = 1" using orthogonal_transformation_det by auto
  have deriv: "((\<lambda>(x,y). (x, -y)) has_derivative (\<lambda>p h. (fst h, - snd h)) x) (at x within s)" for x
    by(auto intro!: derivative_eq_intros)
  have rw: "((\<lambda>x. 1 *\<^sub>R f (case x of (x, y) \<Rightarrow> (x, - y))) absolutely_integrable_on s) = ((\<lambda>(x,y). f (x, - y)) absolutely_integrable_on s)" for f s
    unfolding set_integrable_def
    by (metis (mono_tags, lifting) integrable_cong prod.case_distrib scaleR_one split_cong)
  have rw2: "(\<lambda>x. g (case x of (x, y) \<Rightarrow> (x, - y))) = (\<lambda>(x,y). g (x, - y))" by auto
  have cont: "continuous_on s (\<lambda>p::real\<times>real. (fst p, - (snd p)))" for s
    by(auto intro!: continuous_on_Pair continuous_on_fst continuous_on_snd continuous_on_minus continuous_on_id )
  then have cont: "continuous_on s (\<lambda>(x::real, y::real). (x, - y))" for s
    by (simp add: cont case_prod_beta)
  then have "((\<lambda>x. g (case x of (x, y) \<Rightarrow> (x, - y))) absolutely_integrable_on s \<and> integral s (\<lambda>x. g (case x of (x, y) \<Rightarrow> (x, - y))) = b) = (g absolutely_integrable_on (\<lambda>(x, y). (x, - y)) ` s \<and> integral ((\<lambda>(x, y). (x, - y)) ` s) g = b)"
    apply(intro has_absolute_integral_change_of_variables_invertible_pair[OF deriv, unfolded det_1 rw[of s]  pth_1[symmetric], where h = "\<lambda>(x::real,y::real). (x, -y)" and f = g])
    by auto
  then show ?thesis
    by (metis (mono_tags, lifting) integrable_cong rw2)
qed

lemma absolutely_integral_minusarg2_iff':
  fixes g :: "real \<times> real \<Rightarrow> real"
  shows "(\<lambda>(x,y). g (x,-y)) absolutely_integrable_on s \<longleftrightarrow> g absolutely_integrable_on (\<lambda>(x,y). (x,-y)) ` s"
  using absolutely_integral_minusarg2_iff by blast

lemma flip_y_is_borel_measurable: "(\<lambda>(x,y). (x, - y)) \<in> lborel \<rightarrow>\<^sub>M lborel"
proof-
  have rw: "(%(x,y). (x, -y)) = (\<lambda>p. (fst p, - snd p))" by auto
  show ?thesis
    unfolding rw
  apply(auto intro!: borel_measurable_uminus_eq borel_measurable_Pair split_comp_eq)
  by (metis borel_prod measurable_fst measurable_snd)+
qed

lemma integral_minusarg2_iff_1:
  fixes g :: "real \<times> real \<Rightarrow> real"
  assumes "integrable lborel g"
  shows "integrable lborel (\<lambda>(x,y). g (x,-y))"
proof-
  have range_UNIV: "range (\<lambda>(x::real, y::real). (x, - y)) = UNIV" apply (auto simp add: image_def) 
    by (metis add.inverse_inverse)
  have rw: "(\<lambda>x. case x of (x, y) \<Rightarrow> g (x, - y)) = (\<lambda>x. g (case x of (x, y) \<Rightarrow> (x, - y)))" by auto
  have "(((\<lambda>(x,y). g (x, -y))) absolutely_integrable_on UNIV)"
    using lborel_integrable_imp_absolutely_integrable[OF assms] unfolding absolutely_integral_minusarg2_iff'
    by (auto simp add: range_UNIV)
  then show ?thesis
    using absolutely_integrable_imp_lborel_integrable[OF borel_measurable_integrable'[OF assms flip_y_is_borel_measurable]]
    by (auto simp add: rw)
qed

lemma integral_minusarg2_iff_2:
  fixes g :: "real \<times> real \<Rightarrow> real"
  assumes "integrable lborel (\<lambda>(x,y). g (x,-y))"
  shows "integrable lborel g"
proof-
  have rw:"(\<lambda>x. case case x of (x, y) \<Rightarrow> (x, - y) of (x, y) \<Rightarrow> g (x, - y)) = g" by auto
  have range_UNIV: "range (\<lambda>(x::real, y::real). (x, - y)) = UNIV" apply (auto simp add: image_def) 
    by (metis add.inverse_inverse)
  have "g absolutely_integrable_on UNIV"
    using lborel_integrable_imp_absolutely_integrable[OF assms] unfolding absolutely_integral_minusarg2_iff'
    by (auto simp add: range_UNIV)
  then show ?thesis
    using absolutely_integrable_imp_lborel_integrable[OF borel_measurable_integrable'[OF assms flip_y_is_borel_measurable]]
    by (auto simp add: rw)
qed

lemma integral_minusarg2_iff:
  fixes g :: "real \<times> real \<Rightarrow> real"
  shows "integrable lborel (\<lambda>(x,y). g (x,-y)) \<longleftrightarrow> integrable lborel g"
  using integral_minusarg2_iff_1 integral_minusarg2_iff_2 by auto

lemma eqimage: "{(x, - y) |y x. a \<le> x \<and> x \<le> b \<and> g1 x \<le> y \<and> y \<le> g2 x} =
       (\<lambda>(x,y). (x,-y)) ` {(x::real, y::real). a \<le> x \<and> x \<le> b \<and> g1 x \<le> y \<and> y \<le> g2 x}"
  "{(y, -x) |y x. a \<le> x \<and> x \<le> b \<and> g1 x \<le> y \<and> y \<le> g2 x} =
       (\<lambda>(x,y). (x,-y)) ` {(y::real, x::real). a \<le> x \<and> x \<le> b \<and> g1 x \<le> y \<and> y \<le> g2 x}"
  by auto

lemma eqimage': "{(x, - y) |y x. (a \<le> x \<and> x \<le> b \<or> b \<le> x \<and> x \<le> a) \<and> g1 x \<le> y \<and> y \<le> g2 x} =
       (\<lambda>(x,y). (x,-y)) ` {(x::real, y::real). (a \<le> x \<and> x \<le> b \<or> b \<le> x \<and> x \<le> a) \<and> g1 x \<le> y \<and> y \<le> g2 x}"
  "{(y, -x) |y x. (a \<le> x \<and> x \<le> b \<or> b \<le> x \<and> x \<le> a) \<and> g1 x \<le> y \<and> y \<le> g2 x} =
       (\<lambda>(x,y). (x,-y)) ` {(y::real, x::real). (a \<le> x \<and> x \<le> b \<or> b \<le> x \<and> x \<le> a) \<and> g1 x \<le> y \<and> y \<le> g2 x}"
  by auto


locale R2 =
  fixes i j
  assumes i_is_x_axis: "i = (1::real,0::real)" and
    j_is_y_axis: "j = (0::real, 1::real)"
begin

lemma i_neq_j: "i \<noteq> j" using i_is_x_axis j_is_y_axis by auto

lemma anal_valid_j_neg_j:
  assumes anal_valid: "analytically_valid s (F\<^bsub>j\<^esub>) i j " 
  shows "analytically_valid s (F\<^bsub>(- j)\<^esub>) i (-j)" 
  unfolding analytically_valid_def
proof(safe)
  show " partially_vector_differentiable ((F\<^bsub>- j\<^esub>)) i (a',b')" if "(a', b') \<in> s" for a' b'
    using anal_valid apply (auto simp add: analytically_valid_def) 
    using that partially_vec_diff_minus[where p = "(a',b')"]
    by blast
  show "continuous_on s ((F\<^bsub>- j\<^esub>))" using anal_valid continuous_on_minus by (auto simp add: analytically_valid_def)
  show "set_integrable lborel s ((\<partial> (F\<^bsub>- j\<^esub>) / \<partial> i))"
    unfolding set_integrable_def
    apply(rule iffD1[OF integrable_cong[where M = lborel and f = "\<lambda>x. - indicat_real s x *\<^sub>R (\<partial> (F\<^bsub>j\<^esub>) / \<partial> i |\<^bsub>x\<^esub>)"]])
    using anal_valid
    by (auto simp add: analytically_valid_def partial_deriv_minus indicator_def set_integrable_def)
  have *: "(\<lambda>x. integral UNIV (\<lambda>y. (if y *\<^sub>R i - x *\<^sub>R j \<in> s then 1 else 0) * (\<partial> \<lambda>x. - ((F\<^bsub>j\<^esub>)) x / \<partial> i |\<^bsub>y *\<^sub>R i - x *\<^sub>R j\<^esub>)))
         = (\<lambda>x. integral UNIV (\<lambda>y. (if y *\<^sub>R i - x *\<^sub>R j \<in> s then 1 else 0) * - (\<partial> \<lambda>x. ((F\<^bsub>j\<^esub>)) x / \<partial> i |\<^bsub>y *\<^sub>R i - x *\<^sub>R j\<^esub>)))"
    apply(rule HOL.ext, rule integral_cong)
    using anal_valid
    by (simp add: analytically_valid_def partial_deriv_minus indicator_def)
  have **: "(\<lambda>x. integral UNIV (\<lambda>y. (if y *\<^sub>R i + x *\<^sub>R j \<in> s then 1 else 0) * (f (y *\<^sub>R i + x *\<^sub>R j)))) \<in> borel_measurable lborel
         \<Longrightarrow> (\<lambda>x. integral UNIV (\<lambda>y. (if y *\<^sub>R i - x *\<^sub>R j \<in> s then 1 else 0) * (f (y *\<^sub>R i - x *\<^sub>R j)))) \<in> borel_measurable lborel" for s and f::"(real \<times> real) \<Rightarrow> real"
    using [[simp_trace]] [[simp_trace_depth_limit=10]]
    by (simp add: i_is_x_axis j_is_y_axis)
  show "let p = \<lambda>x y. y *\<^sub>R i + x *\<^sub>R - j in (\<lambda>x. integral UNIV (\<lambda>y. indicat_real s (p x y) *\<^sub>R (\<partial> (F\<^bsub>- j\<^esub>) / \<partial> i |\<^bsub>p x y\<^esub>))) \<in> borel_measurable lborel"
    using anal_valid 
    apply (simp add: analytically_valid_def partial_deriv_minus indicator_def)
    unfolding * mult_minus_right integral_neg
    apply(intro borel_measurable_uminus)
    using ** by force
qed

definition "g2_ge_g1 g1 g2 a b = ((\<forall>x\<in>(cbox a b \<union> cbox b a). g1 x \<le> g2 x) \<or> (\<forall>x\<in>(cbox a b \<union> cbox b a). g2 x \<le> g1 x))"

definition P :: "real \<times> real \<Rightarrow> real \<times> real \<Rightarrow> bool"
  where "P base1 base2 \<equiv> (\<forall>f s g g1 g2 a b. 
                    g2_ge_g1 g1 g2 a b \<longrightarrow>
                    (integrable lborel f) \<longrightarrow>
                    (\<forall>x. (%y. f(y *\<^sub>R base1 + x *\<^sub>R base2)) integrable_on UNIV) \<longrightarrow>
                    ((%x. integral UNIV (%y. f(y *\<^sub>R base1 + x *\<^sub>R base2))) \<in> borel_measurable lborel) \<longrightarrow>
                    (f = (%x. if x \<in> s then g x else (0::real))) \<longrightarrow>
                    (s = {point. \<exists>y x. point = (y *\<^sub>R base1 + x *\<^sub>R base2) \<and> ((a \<le> x \<and> x \<le> b) \<or> (b \<le> x \<and> x \<le> a)) \<and> ((g1 x) \<le> y \<and> y \<le> (g2 x) \<or> (g2 x) \<le> y \<and> y \<le> (g1 x))}) \<longrightarrow>
                    (integral s g = integral (cbox a b) (\<lambda>x. integral (cbox (g1 x) (g2 x) \<union> cbox (g2 x) (g1 x)) (\<lambda>y. g(y *\<^sub>R base1 + x *\<^sub>R base2)))
                                    + integral (cbox b a) (\<lambda>x. integral (cbox (g1 x) (g2 x) \<union> cbox (g2 x) (g1 x)) (\<lambda>y. g(y *\<^sub>R base1 + x *\<^sub>R base2)))))"

lemma i_j_have_Fubini: "P j i"
  unfolding P_def i_is_x_axis j_is_y_axis
proof (clarsimp)
  fix g::"real*real \<Rightarrow> real" and g1 g2:: "real\<Rightarrow>real" and a b::real
  assume intf: "integrable lborel (\<lambda>x. if \<exists>y xa. x = (xa, y) \<and> (a \<le> xa \<and> xa \<le> b \<or> b \<le> xa \<and> xa \<le> a) \<and> (g1 xa \<le> y \<and> y \<le> g2 xa \<or> g2 xa \<le> y \<and> y \<le> g1 xa) then g x else 0)"
    and int_y: "\<forall>x. (\<lambda>y. if (a \<le> x \<and> x \<le> b \<or> b \<le> x \<and> x \<le> a) \<and> (g1 x \<le> y \<and> y \<le> g2 x \<or> g2 x \<le> y \<and> y \<le> g1 x) then g (x, y) else 0) integrable_on UNIV"
    and meas_y: "(\<lambda>x. integral UNIV (\<lambda>y. if (a \<le> x \<and> x \<le> b \<or> b \<le> x \<and> x \<le> a) \<and> (g1 x \<le> y \<and> y \<le> g2 x \<or> g2 x \<le> y \<and> y \<le> g1 x) then g (x, y) else 0)) \<in> borel_measurable borel"
    and "g2_ge_g1 g1 g2 a b"
  then have g2_g1: "((\<forall>x\<in>(cbox a b \<union> cbox b a). g1 x \<le> g2 x) \<or> (\<forall>x\<in>(cbox a b \<union> cbox b a). g2 x \<le> g1 x))" 
    unfolding g2_ge_g1_def by auto
  have **:"{(x, y) |y x. (a \<le> x \<and> x \<le> b) \<and> (g1 x \<le> y \<and> y \<le> g2 x \<or> g2 x \<le> y \<and> y \<le> g1 x)} = {(x, y). (a \<le> x \<and> x \<le> b) \<and> (g1 x \<le> y \<and> y \<le> g2 x)}"
        "(a \<le> x \<and> x \<le> b) \<longrightarrow>(g1 x \<le> y \<and> y \<le> g2 x \<or> g2 x \<le> y \<and> y \<le> g1 x) = (g1 x \<le> y \<and> y \<le> g2 x)" if "a \<le> b" and "\<forall>x\<in>{a..b}. g1 x \<le> g2 x" for x y a b:: real and g1 g2
        using that by force+
  have ***: "integral {a..b} (\<lambda>x. integral ({g1 x..g2 x} \<union> {g2 x..g1 x}) (\<lambda>y. g (x, y))) = integral {a..b} (\<lambda>x. integral {g1 x..g2 x} (\<lambda>y. g (x, y)))"
    if "a \<le> b" and "\<forall>x\<in>{a..b}. g1 x \<le> g2 x" for a b g1 g2
        apply(intro integral_cong)
        subgoal for x apply(rule HOL.arg_cong[where y = "{g1 x..g2 x}"])
          using that by force
        done
  show "integral {(x, y) |y x. (a \<le> x \<and> x \<le> b \<or> b \<le> x \<and> x \<le> a) \<and> (g1 x \<le> y \<and> y \<le> g2 x \<or> g2 x \<le> y \<and> y \<le> g1 x)} g =
       integral {a..b} (\<lambda>x. integral ({g1 x..g2 x} \<union> {g2 x..g1 x}) (\<lambda>y. g (x, y))) + integral {b..a} (\<lambda>x. integral ({g1 x..g2 x} \<union> {g2 x..g1 x}) (\<lambda>y. g (x, y)))"
  proof(cases "a\<le>b")
    case T1:True
    then have *:"(a \<le> x \<and> x \<le> b \<or> b \<le> x \<and> x \<le> a) = (a \<le> x \<and> x \<le> b)" for x
      by auto
    show ?thesis
    proof(cases "\<forall>x\<in>{a..b}. g1 x \<le> g2 x")
      case True
      show ?thesis
        unfolding ***[OF T1 True] **(1)[OF T1 True] * a_leq_q_int_0[OF T1] add_0_right
        apply(intro gauge_integral_Fubini_curve_bounded_region_x(1)[where g = g, unfolded cbox_interval, OF intf])
        subgoal using int_y unfolding * by auto
        subgoal using  meas_y unfolding * case_prod_conv
          by force
        subgoal unfolding * apply(rule HOL.ext, auto)
          using **(2)[OF T1 True] antisym_conv by auto
        by auto
    next
      case False
      then have False: "(\<forall>x\<in>{a..b}. g2 x \<le> g1 x)" using g2_g1 by auto
      show ?thesis
        apply(subst Un_commute)
        unfolding ***[OF T1 False] **(1)[OF T1 False] * a_leq_q_int_0[OF T1] add_0_right
        apply(intro gauge_integral_Fubini_curve_bounded_region_x(1)[where g = g, unfolded cbox_interval, OF intf])
        subgoal using int_y unfolding * by auto
        subgoal using  meas_y unfolding * case_prod_conv
          by force
        subgoal unfolding * by(rule HOL.ext, auto)
          using **(2)[OF T1 False] antisym_conv
        by auto
    qed
  next
    case False
    then have *:"(a \<le> x \<and> x \<le> b \<or> b \<le> x \<and> x \<le> a) = (b \<le> x \<and> x \<le> a)" for x
      by auto
    have F1:"b \<le> a" using False by auto
    show ?thesis
    proof(cases "\<forall>x\<in>{b..a}. g1 x \<le> g2 x")
      case True
      show ?thesis
        unfolding ***[OF F1 True] **(1)[OF F1 True] * a_leq_q_int_0[OF F1] add_0_left
        apply(intro gauge_integral_Fubini_curve_bounded_region_x(1)[where g = g, unfolded cbox_interval, OF intf])
        subgoal using int_y unfolding * by auto
        subgoal using  meas_y unfolding * case_prod_conv
          by force
        subgoal unfolding * apply(rule HOL.ext, auto)
          using **(2)[OF F1 True] antisym_conv by auto
        by auto
    next
      case False
      then have False: "(\<forall>x\<in>{b..a}. g2 x \<le> g1 x)" using g2_g1 by auto
      show ?thesis
        apply(rewrite in "_ = _ + \<hole>" subst Un_commute)
        unfolding ***[OF F1 False] **(1)[OF F1 False] * a_leq_q_int_0[OF F1] add_0_left
        apply(intro gauge_integral_Fubini_curve_bounded_region_x(1)[where g = g, unfolded cbox_interval, OF intf])
        subgoal using int_y unfolding * by auto
        subgoal using  meas_y unfolding * case_prod_conv
          by force
        subgoal unfolding * by(rule HOL.ext, auto)
          using **(2)[OF F1 False] antisym_conv
        by auto
    qed
  qed
qed

lemma i_neg_j_have_Fubini: "P i (-j)"
proof-
  {define le_x where "le_x = (%(l::real) u x. l \<le> x \<and> x \<le> u \<or> u \<le> x \<and> x \<le> l)"
    have le_x_comm: "le_x l u x = le_x u l x" for l u x unfolding le_x_def by auto
    fix g :: "real \<times> real \<Rightarrow> real" and g1 :: "real \<Rightarrow> real" and g2 :: "real \<Rightarrow> real" and a :: "real" and b :: "real"
    assume intf: "integrable lborel (\<lambda>x. if \<exists>y xa. x = (y, - xa) \<and> le_x a b xa \<and> le_x (g1 xa)  (g2  xa) y then g x else 0)" 
      and int_x: "\<forall>x. (\<lambda>y. if le_x a b x \<and> le_x (g1 x)  (g2  x) y then g (y, - x) else 0) integrable_on UNIV" 
      and meas_x: "(\<lambda>x. integral UNIV (\<lambda>y. if le_x a b x \<and> le_x (g1 x)  (g2  x) y then g (y, - x) else 0)) \<in> borel_measurable borel"
      and g2_g1: "g2_ge_g1 g1 g2 a b"  
    have intf': "integrable lborel
         (\<lambda>p. if \<exists>y x. p = (y, x) \<and> le_x a b x \<and> le_x (g1 x)  (g2  x) y then (\<lambda>(x,y). g(x, -y)) p
              else 0)"
      apply (subst integral_minusarg2_iff [symmetric])
      apply (rule iffD1[OF integrable_cong, OF _ _ intf])
      by auto
    have **:"{(y, - x) |y x. (a \<le> x \<and> x \<le> b) \<and> le_x (g1 x)  (g2  x) y} = {(y, - x) |y x. (a \<le> x \<and> x \<le> b) \<and> (g1 x \<le> y \<and> y \<le> g2 x)}"
      "(a \<le> x \<and> x \<le> b) \<longrightarrow> le_x (g1 x)  (g2  x) y = (g1 x \<le> y \<and> y \<le> g2 x)" if "a \<le> b" and "\<forall>x\<in>{a..b}. g1 x \<le> g2 x" for x y a b:: real and g1 g2
      unfolding le_x_def
      using that by force+
    have ***: "integral {a..b} (\<lambda>x. integral ({g1 x..g2 x} \<union> {g2 x..g1 x}) (\<lambda>y. g (y, -x))) = integral {a..b} (\<lambda>x. integral {g1 x..g2 x} (\<lambda>y. g (y, -x)))"
      if "a \<le> b" and "\<forall>x\<in>{a..b}. g1 x \<le> g2 x" for a b g1 g2
      apply(intro integral_cong)
      subgoal for x apply(rule HOL.arg_cong[where y = "{g1 x..g2 x}"])
        using that by force
      done
    have "integral {(y, - x) |y x. le_x a b x \<and> le_x (g1 x)  (g2  x) y} g =
       integral {a..b} (\<lambda>x. integral ({g1 x..g2 x} \<union> {g2 x..g1 x}) (\<lambda>y. g (y, - x))) + integral {b..a} (\<lambda>x. integral ({g1 x..g2 x} \<union> {g2 x..g1 x}) (\<lambda>y. g (y, - x)))"
    proof(cases "a \<le>b")
      case T1: True
      then have *:"le_x a b x = (a \<le> x \<and> x \<le> b)" for x
        unfolding le_x_def
        by auto
      show ?thesis
      proof(cases "\<forall>x\<in>{a..b}. g1 x \<le> g2 x")
        case True
        show ?thesis
          unfolding * a_leq_q_int_0[OF T1] **[OF T1 True] ***[OF T1 True] add_0_right
          apply(subst gauge_integral_Fubini_curve_bounded_region_y(1) [OF intf', of _ "\<lambda>(x,y). g(x, -y)", simplified, symmetric])
          subgoal for x using int_x by auto
          subgoal using meas_x by auto
        proof-
          have "set_integrable lborel {(y, x). a \<le> x \<and> x \<le> b \<and> g1 x \<le> y \<and> y \<le> g2 x} (%(x,y). g (x, -y))"
            unfolding set_integrable_def
            apply(rule iffD1[OF integrable_cong intf'])
             apply(auto simp add: indicator_def algebra_simps * split: if_splits)
            using **[OF T1 True] by fastforce+
          then have i: "(%(x,y). g (x, -y)) absolutely_integrable_on {(y, x). a \<le> x \<and> x \<le> b \<and> g1 x \<le> y \<and> y \<le> g2 x}"
            unfolding set_integrable_def
            using integrable_completion by blast
          show "integral {(y, - x) |y x. (a \<le> x \<and> x \<le> b) \<and> g1 x \<le> y \<and> y \<le> g2 x} g = integral {(y, x). a \<le> x \<and> x \<le> b \<and> g1 x \<le> y \<and> y \<le> g2 x} (\<lambda>(x, y). g (x, - y))"
            apply (simp add: eqimage)
            using absolutely_integral_minusarg2_iff i by blast      
          show "(\<lambda>p. if \<exists>y x. p = (y, x) \<and> le_x a b x \<and> le_x (g1 x)  (g2  x) y then case p of (x, y) \<Rightarrow> g (x, - y) else 0) =
                    (\<lambda>x. if x \<in> {(y, x). a \<le> x \<and> x \<le> b \<and> g1 x \<le> y \<and> y \<le> g2 x} then case x of (x, y) \<Rightarrow> g (x, - y) else 0)"
            apply(rule HOL.ext)
            using "*" **(2)[OF T1 True] case_prodI2 by auto
        qed (auto)
      next
        case False
        then have False: "\<forall>x\<in>{a..b}. g2 x \<le> g1 x" using g2_g1 unfolding g2_ge_g1_def by auto
        then show ?thesis
          apply(rewrite in "_ (_ \<and> \<hole>)" le_x_comm, subst Un_commute)
          unfolding * a_leq_q_int_0[OF T1] **[OF T1 False] ***[OF T1 False] add_0_right 
          apply(subst gauge_integral_Fubini_curve_bounded_region_y(1) [OF intf', of _ "\<lambda>(x,y). g(x, -y)", simplified, symmetric])
          subgoal for x using int_x by auto
          subgoal using meas_x by auto
        proof-
          have "set_integrable lborel {(y, x). a \<le> x \<and> x \<le> b \<and> g2 x \<le> y \<and> y \<le> g1 x} (%(x,y). g (x, -y))"
            unfolding set_integrable_def
            apply(rule iffD1[OF integrable_cong intf'])
             apply(auto simp add: indicator_def algebra_simps * split: if_splits)
            using **[OF T1 False] le_x_comm by fastforce+
          then have i: "(%(x,y). g (x, -y)) absolutely_integrable_on {(y, x). a \<le> x \<and> x \<le> b \<and> g2 x \<le> y \<and> y \<le> g1 x}"
            unfolding set_integrable_def
            using integrable_completion by blast
          show "integral {(y, - x) |y x. (a \<le> x \<and> x \<le> b) \<and> g2 x \<le> y \<and> y \<le> g1 x} g = integral {(y, x). a \<le> x \<and> x \<le> b \<and> g2 x \<le> y \<and> y \<le> g1 x} (\<lambda>(x, y). g (x, - y))"
            apply (simp add: eqimage)
            using absolutely_integral_minusarg2_iff i by blast      
          show "(\<lambda>p. if \<exists>y x. p = (y, x) \<and> le_x a b x \<and> le_x (g1 x)  (g2  x) y then case p of (x, y) \<Rightarrow> g (x, - y) else 0) =
                    (\<lambda>x. if x \<in> {(y, x). a \<le> x \<and> x \<le> b \<and> g2 x \<le> y \<and> y \<le> g1 x} then case x of (x, y) \<Rightarrow> g (x, - y) else 0)"
            apply(rule HOL.ext)
            using "*" **(2)[OF T1 False] case_prodI2 le_x_comm by auto
        qed (auto)
      qed
    next
      case False
      then have *:"le_x a b x = (b \<le> x \<and> x \<le> a)" for x
        unfolding le_x_def
        by auto
      have F1: "b \<le> a" using False by auto
      show ?thesis
      proof(cases "\<forall>x\<in>{b..a}. g1 x \<le> g2 x")
        case True
        show ?thesis
          unfolding * a_leq_q_int_0[OF F1] **[OF F1 True] ***[OF F1 True] add_0_left
          apply(subst gauge_integral_Fubini_curve_bounded_region_y(1) [OF intf', of _ "\<lambda>(x,y). g(x, -y)", simplified, symmetric])
          subgoal for x using int_x by auto
          subgoal using meas_x by auto
        proof-
          have "set_integrable lborel {(y, x). b \<le> x \<and> x \<le> a \<and> g1 x \<le> y \<and> y \<le> g2 x} (%(x,y). g (x, -y))"
            unfolding set_integrable_def
            apply(rule iffD1[OF integrable_cong intf'])
             apply(auto simp add: indicator_def algebra_simps * split: if_splits)
            using **[OF F1 True] by fastforce+
          then have i: "(%(x,y). g (x, -y)) absolutely_integrable_on {(y, x). b \<le> x \<and> x \<le> a \<and> g1 x \<le> y \<and> y \<le> g2 x}"
            unfolding set_integrable_def
            using integrable_completion by blast
          show "integral {(y, - x) |y x. (b \<le> x \<and> x \<le> a) \<and> g1 x \<le> y \<and> y \<le> g2 x} g = integral {(y, x). b \<le> x \<and> x \<le> a \<and> g1 x \<le> y \<and> y \<le> g2 x} (\<lambda>(x, y). g (x, - y))"
            apply (simp add: eqimage)
            using absolutely_integral_minusarg2_iff i by blast      
          show "(\<lambda>p. if \<exists>y x. p = (y, x) \<and> le_x a b x \<and> le_x (g1 x)  (g2  x) y then case p of (x, y) \<Rightarrow> g (x, - y) else 0) =
                    (\<lambda>x. if x \<in> {(y, x). b \<le> x \<and> x \<le> a \<and> g1 x \<le> y \<and> y \<le> g2 x} then case x of (x, y) \<Rightarrow> g (x, - y) else 0)"
            apply(rule HOL.ext)
            using "*" **(2)[OF F1 True] case_prodI2 by auto
        qed (auto)
      next
        case False
        then have False: "\<forall>x\<in>{b..a}. g2 x \<le> g1 x" using g2_g1 unfolding g2_ge_g1_def by auto
        show ?thesis
          apply(rewrite in "_ (_ \<and> \<hole>)" le_x_comm, rewrite in "_ + _ (\<hole>)" Un_commute)
          unfolding * a_leq_q_int_0[OF F1] **[OF F1 False] ***[OF F1 False] add_0_left 
          apply(subst gauge_integral_Fubini_curve_bounded_region_y(1) [OF intf', of _ "\<lambda>(x,y). g(x, -y)", simplified, symmetric])
          subgoal for x using int_x by auto
          subgoal using meas_x by auto
        proof-
          have "set_integrable lborel {(y, x). b \<le> x \<and> x \<le> a \<and> g2 x \<le> y \<and> y \<le> g1 x} (%(x,y). g (x, -y))"
            unfolding set_integrable_def
            apply(rule iffD1[OF integrable_cong intf'])
             apply(auto simp add: indicator_def algebra_simps * split: if_splits)
            using **[OF F1 False] le_x_comm by fastforce+
          then have i: "(%(x,y). g (x, -y)) absolutely_integrable_on {(y, x). b \<le> x \<and> x \<le> a \<and> g2 x \<le> y \<and> y \<le> g1 x}"
            unfolding set_integrable_def
            using integrable_completion by blast
          show "integral {(y, - x) |y x. (b \<le> x \<and> x \<le> a) \<and> g2 x \<le> y \<and> y \<le> g1 x} g = integral {(y, x). b \<le> x \<and> x \<le> a \<and> g2 x \<le> y \<and> y \<le> g1 x} (\<lambda>(x, y). g (x, - y))"
            apply (simp add: eqimage)
            using absolutely_integral_minusarg2_iff i by blast      
          show "(\<lambda>p. if \<exists>y x. p = (y, x) \<and> le_x a b x \<and> le_x (g1 x) (g2 x) y then case p of (x, y) \<Rightarrow> g (x, - y) else 0) =
                     (\<lambda>x. if x \<in> {(y, x). b \<le> x \<and> x \<le> a \<and> g2 x \<le> y \<and> y \<le> g1 x} then case x of (x, y) \<Rightarrow> g (x, - y) else 0)"
            apply(rule HOL.ext)
            using "*" **(2)[OF F1 False] case_prodI2 le_x_comm by auto
        qed (auto)
      qed
    qed}
  then show ?thesis
    unfolding P_def lex_def  i_is_x_axis j_is_y_axis
    by(clarsimp)
qed

lemma i_j_orthonorm: "i_j_orthonorm i j"
  using i_j_have_Fubini
  unfolding i_j_orthonorm_def i_is_x_axis j_is_y_axis
            P_def g2_ge_g1_def
  by auto

lemma neg_j_i_orthonorm': "i_j_orthonorm (- j) i"
  using i_neg_j_have_Fubini
  unfolding i_j_orthonorm_def i_is_x_axis j_is_y_axis
            P_def g2_ge_g1_def
  by auto

lemma C1_imp_analytically_valid_ij: assumes "continuous_on (s\<times>t) (F::(real\<times>real)\<Rightarrow>'a::{euclidean_space,banach,one,times})" "C1_partially_differentiable_on F (s\<times>t) i" "compact t" "s = {a .. b}"
  shows "analytically_valid (s\<times>t) F i j"
  unfolding analytically_valid_def partially_vector_differentiable_def  using assms C1_partially_diff_imp_partially_diff   apply auto
proof-
  obtain F_i where F_i: "\<forall>x\<in> (s\<times>t). (has_partial_vector_derivative F i (F_i x) x)" "continuous_on (s\<times>t) F_i"
    using assms(2) unfolding C1_partially_differentiable_on_def by auto
  have cont_F_i: "continuous_on (s\<times>t) (\<partial>F/ \<partial>i)"
    apply(rule continuous_on_eq[where f = F_i], rule F_i)
    using F_i partial_vector_derivative_works_2
    by force
  then show "set_integrable lborel ({a..b}\<times>t) ((\<partial> F / \<partial> i))"
    unfolding set_integrable_def
    using borel_integrable_compact compact_cbox compact_Times assms(3,4)
    apply auto by blast
  have rw2: "set_integrable lborel t (\<lambda>x. integral UNIV (\<lambda>y. (indicat_real s y) *\<^sub>R (\<partial> F / \<partial> i |\<^bsub>(y, x)\<^esub>)))
          = integrable lborel (\<lambda>x. integral UNIV (\<lambda>y. (indicat_real s y * indicat_real t x) *\<^sub>R (\<partial> F / \<partial> i |\<^bsub>(y, x)\<^esub>)))"
    unfolding set_integrable_def
    apply(auto intro!: Bochner_Integration.integrable_cong)
    by (smt Henstock_Kurzweil_Integration.integral_cong integral_cmul pth_5 real_vector.scale_left_commute)
  have rw3: "set_integrable lborel t (\<lambda>x. integral UNIV (\<lambda>y. (indicat_real s y) *\<^sub>R (\<partial> F / \<partial> i |\<^bsub>(y, x)\<^esub>)))
          = set_integrable lborel t (\<lambda>x. integral s (\<lambda>y. (\<partial> F / \<partial> i |\<^bsub>(y, x)\<^esub>)))"
    by(auto intro!: Bochner_Integration.integrable_cong simp add: integral_indicator_Int)
  have rw4: "prod.swap ` (t \<times> s) =  (s \<times> t)" by auto
  have rw5: "(\<lambda>(x, t). (\<partial> F / \<partial> i |\<^bsub>(t, x)\<^esub>)) = ((\<partial> F / \<partial> i) o prod.swap)" by auto
  have rw6: "s = cbox a b" using assms(4) by auto
  have "(\<lambda>x. integral UNIV (\<lambda>y. indicator (s\<times>t) (y, x) *\<^sub>R (\<partial> F / \<partial> i |\<^bsub>(y, x)\<^esub>))) \<in> borel_measurable lborel"
    apply (intro Bochner_Integration.borel_measurable_integrable)
    unfolding indicator_2d_to_1d' rw2[symmetric] rw3
    unfolding set_integrable_def
    apply (intro borel_integrable_compact assms(3) integral_continuous_on_param, simp only: rw6 rw5)+
    apply(rule continuous_on_compose)
    using cont_F_i rw4 by(auto intro!: continuous_intros simp: assms(4))
  then show "(\<lambda>x. integral UNIV (\<lambda>y. indicat_real ({a..b} \<times> t) (y *\<^sub>R i + x *\<^sub>R j) *\<^sub>R (\<partial> F / \<partial> i |\<^bsub>y *\<^sub>R i + x *\<^sub>R j\<^esub>))) \<in> borel_measurable borel"
    by (auto simp add: i_is_x_axis j_is_y_axis assms(4))
  show "\<And>x y. y \<in> t \<Longrightarrow> a \<le> x \<Longrightarrow> x \<le> b \<Longrightarrow> \<exists>F'. has_partial_vector_derivative F i F' (x, y)"
    using F_i(1)
    by (auto simp add: assms(4), force)
qed

lemma C1_imp_analytically_valid_ji: assumes "continuous_on (s\<times>t) (F::(real\<times>real)\<Rightarrow>'a::{euclidean_space,banach,one,times})" "C1_partially_differentiable_on F (s\<times>t) j" "compact s" "t = {a .. b}"
  shows "analytically_valid (s\<times>t) F j i"
  unfolding analytically_valid_def partially_vector_differentiable_def  using assms C1_partially_diff_imp_partially_diff   apply auto
proof-
  obtain F_i where F_i: "\<forall>x\<in> (s\<times>t). (has_partial_vector_derivative F j (F_i x) x)" "continuous_on (s\<times>t) F_i"
    using assms(2) unfolding C1_partially_differentiable_on_def by auto
  have cont_F_i: "continuous_on (s\<times>t) (\<partial>F/ \<partial>j)"
    apply(rule continuous_on_eq[where f = F_i], rule F_i)
    using F_i partial_vector_derivative_works_2
    by force
  then show "set_integrable lborel (s\<times>{a..b}) ((\<partial> F / \<partial> j))"
    unfolding set_integrable_def
    using borel_integrable_compact compact_cbox compact_Times assms(3,4)
    apply auto by blast
  have rw2: "set_integrable lborel s (\<lambda>x. integral UNIV (\<lambda>y. (indicat_real t y) *\<^sub>R (\<partial> F / \<partial> j |\<^bsub>(x, y)\<^esub>)))
          = integrable lborel (\<lambda>x. integral UNIV (\<lambda>y. (indicat_real s x * indicat_real t y) *\<^sub>R (\<partial> F / \<partial> j |\<^bsub>(x, y)\<^esub>)))"
    unfolding set_integrable_def
    apply(auto intro!: Bochner_Integration.integrable_cong)
    by (smt Henstock_Kurzweil_Integration.integral_cong integral_cmul pth_5 real_vector.scale_left_commute)
  have rw3: "set_integrable lborel s (\<lambda>x. integral UNIV (\<lambda>y. (indicat_real t y) *\<^sub>R (\<partial> F / \<partial> j |\<^bsub>(x, y)\<^esub>)))
          = set_integrable lborel s (\<lambda>x. integral t (\<lambda>y. (\<partial> F / \<partial> j |\<^bsub>(x, y)\<^esub>)))"
    by(auto intro!: Bochner_Integration.integrable_cong simp add: integral_indicator_Int)
  have rw4: "prod.swap ` (t \<times> s) =  (s \<times> t)" by auto
  have rw5: "t = cbox a b" using assms(4) by auto
  have "(\<lambda>x. integral UNIV (\<lambda>y. indicator (s\<times>t) (x, y) *\<^sub>R (\<partial> F / \<partial> j |\<^bsub>(x, y)\<^esub>))) \<in> borel_measurable lborel"
    apply (intro Bochner_Integration.borel_measurable_integrable)
    unfolding indicator_2d_to_1d' rw2[symmetric] rw3
    unfolding set_integrable_def
    apply (intro borel_integrable_compact assms(3) integral_continuous_on_param, simp only: rw5, rule integral_continuous_on_param)
    using cont_F_i rw4 by(auto intro!: continuous_intros simp: rw5)
  then show "(\<lambda>x. integral UNIV (\<lambda>y. indicat_real (s \<times> {a..b}) (y *\<^sub>R j + x *\<^sub>R i) *\<^sub>R (\<partial> F / \<partial> j |\<^bsub>y *\<^sub>R j + x *\<^sub>R i\<^esub>))) \<in> borel_measurable borel"
    by (auto simp add: i_is_x_axis j_is_y_axis assms(4))
  show "\<And>x y. x \<in> s \<Longrightarrow> a \<le> y \<Longrightarrow> y \<le> b \<Longrightarrow>  \<exists>F'. has_partial_vector_derivative F j F' (x, y)"
    using F_i(1)
    by (auto simp add: assms(4), force)
qed
end
end